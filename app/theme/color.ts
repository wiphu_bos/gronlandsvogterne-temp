import { palette } from "./palette"

/**
 * Roles for colors.  Prefer using these over the palette.  It makes it easier
 * to change things.
 *
 * The only roles we need to place in here are the ones that span through the app.
 *
 * If you have a specific use-case, like a spinner color.  It makes more sense to
 * put that in the <Spinner /> component.
 */
export const color = {
  /**
   * The palette is available to use, but prefer using the name.
   */
  palette,
  /**
   * A helper for making something see-thru. Use sparingly as many layers of transparency
   * can cause older Android devices to slow down due to the excessive compositing required
   * by their under-powered GPUs.
   */
  transparent: "rgba(0, 0, 0, 0)",
  /**
   * The screen background.
   */
  background: palette.white,
  /**
   * The main tinting color.
   */
  primary: palette.orange,
  /**
   * The main tinting color, but darker.
   */
  primaryDarker: palette.orangeDarker,
  /**
   * A subtle color used for borders and lines.
   */
  line: palette.offWhite,
  /**
   * The default color of text in many components.
   */
  text: palette.white,
  /**
   * Secondary information.
   */
  dim: palette.lightGrey,

  blackDim: 'rgba(0,0,0,0.1)',

  darkDim: 'rgba(0,0,0,0.6)',

  whiteDim: 'rgba(255,255,255,0.6)',

  menuWhiteDim: 'rgba(255,255,255,0.8)',

  hardWhiteDim: 'rgba(255,255,255,0.9)',

  bodyPearl: 'rgba(251,243,248,1)',

  superWhiteDim: 'rgba(255,255,255,0.13)',

  brightDim: 'rgba(255,255,255,0.3)',

  boder: '#E3E3E3',

  highlight: '#F08B17',

  slideBar: '#FFC9E0',
  /**
   * Error messages and icons.
   */
  error: palette.angry,
  darkRedDim: 'rgba(217,73,107,0.46)',
  disabled: '#B8B8B8',
  disabledTextField: '#D4D4D4',
  mainBackground: 'rgba(206,134,157,1)',
  grayPinkGradient: ['#C0BEBF', '#DA819E'],
  blueWhiteGradient: ['#81BCF9', '#FFFFFF'],
  pinkBlueGradient: ['#FE60A2', '#81BCF9'],
  whiteClearGradient: ['#FFFFFF00', '#FFFFFF'],
  clear: 'rgba(0,0,0,0)',
  redTRC: 'rgba(217,73,107,0.7)',
  redFleshGradient: ['#FF6589', '#EBACA5'],
  tabPageBackground: ['rgba(228,143,187,1)', 'rgba(122,187,250,1)'],
  edgeRainBow: ['#81BCF9', '#FC9596'],
  highlightDude: ['#AED6FFE6', '#FFFFFF00']
}
