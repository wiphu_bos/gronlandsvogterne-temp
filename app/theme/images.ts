export const images = {
    close: require('../assets/Icons/ic-close.png'),
    splashLogo: require('../animate-assets/images/img-heartgif.gif'),
    back: require('../assets/Icons/ic-back.png'),
    next: require('../assets/Icons/ic-next.png'),
    waring: require('../assets/Icons/ic-warning.png'),
    down: require('../assets/Icons/ic-down-arrow.png'),
    manageProfile: require('../assets/Icons/ic-manage-profile.png'),
    upload: require('../assets/Icons/ic-upload.png'),
    search: require('../assets/Icons/ic-search.png'),
    favourite: require('../assets/Icons/ic-fav.png'),
    envelope: require('../assets/Icons/ic-envelope.png'),
    heart: require('../assets/Icons/ic-heart.png'),
    info: require('../assets/Icons/ic-info.png'),
    logout: require('../assets/Icons/ic-logout.png'),
    contact: require('../assets/Icons/ic-contact.png'),
    hamMenu: require('../assets/Icons/ic-ham-menu.png'),
    bell: require('../assets/Icons/ic-bell.png'),
    dude: require('../assets/Icons/ic-dude.png'),
    addDude: require('../assets/Icons/ic-add-dude.png'),
    dudette: require('../assets/Icons/ic-dudette.png'),
    recommendedMySelf: require('../assets/Icons/ic-recommended-myself.png'),
    createDudeTitle: require('../assets/images/create-dude-title.png'),
    waitingDudeTitle: require('../assets/images/wait-dude-title.png'),
    edit: require('../assets/Icons/ic-edit.png'),
    threeDotsMenu: require('../assets/Icons/ic-three-dots-menu.png'),
    avatarDude: require('../assets/images/img-avatar-dude.png'),
    profileImagePlacholder: require('../assets/images/img-avatar-dude-white.png'),
    dudetteProfileImagePlacholder: require('../assets/images/img-avatar-dudette-white.png'),
    chatPink: require('../assets/Icons/ic-chat-pink.png'),
    chatGrey: require('../assets/Icons/ic-chat-grey.png'),
    view: require('../assets/Icons/ic-view.png'),
    infomation: require('../assets/Icons/ic-infomation.png'),
    userTabMenu: require('../assets/Icons/ic-tab-menu-user.png'),
    editTabMenu: require('../assets/Icons/ic-tab-menu-edit.png'),
    imageTabMenu: require('../assets/Icons/ic-tab-menu-image.png'),
    starTabMenu: require('../assets/Icons/ic-tab-menu-star.png'),
    threeDotsMenuWhite: require('../assets/Icons/ic-three-dots-menu-white.png'),
    plus: require('../assets/Icons/ic-plus.png'),
    selected: require('../assets/Icons/ic-selected.png'),
    unSelected: require('../assets/Icons/ic-unselected.png'),
    userTag: require('../assets/Icons/ic-user-tag.png'),
    checkedCircle: require('../assets/Icons/ic-checked-circle.png'),
    uncheckCircle: require('../assets/Icons/ic-uncheck-circle.png'),
    dudeWhite: require('../assets/Icons/ic-dude-white.png'),
    dudetteWhite: require('../assets/Icons/ic-dudette-white.png'),
    remove: require('../assets/Icons/ic-remove.png'),
    addWhite: require('../assets/Icons/ic-add-white.png'),
    waitDudeApproveProfilePublishTitle: require('../assets/images/wait-dude-approve-profile-publish-title.png'),
    pinkPin: require('../assets/Icons/ic-pink-pin.png'),
    whitePin: require('../assets/Icons/ic-white-pin.png'),
    doc: require('../assets/Icons/ic-doc.png'),
    imagePlaceholder: require('../assets/Icons/ic-image-placeholder.png'),
    chat: require('../assets/Icons/ic-chat.png'),
    starUnFav: require('../assets/Icons/ic-star-un-fav.png'),
    starFav: require('../assets/Icons/ic-star-fav.png'),
    filter: require('../assets/Icons/ic-filter.png'),
    userInfoTabMenu: require('../assets/Icons/ic-tab-menu-user-info.png'),
    userTagTabMenu: require('../assets/Icons/ic-tab-menu-user-tag.png'),
    chatTabMenu: require('../assets/Icons/ic-tab-menu-chat.png'),
    thumbsDown: require('../assets/Icons/ic-thumbs-down.png'),
    thumbsUp: require('../assets/Icons/ic-thumbs-up.png'),
    chatCirclePink: require('../assets/Icons/ic-chat-circle-pink.png'),
    flag: require('../assets/Icons/ic-flag.png'),
    sendMessage: require('../assets/Icons/ic-send-message.png'),
    markHighlight: require('../assets/Icons/ic-mark-highlight.png'),
    home: require('../assets/Icons/ic-home.png'),
}