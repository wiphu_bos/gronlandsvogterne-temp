import { StackNavigationOptions, CardStyleInterpolators } from "@react-navigation/stack"
import { Dimensions, PixelRatio, Platform, StyleProp, ViewStyle } from "react-native"
import { DesignDimension, newIphoneDeviceIds } from "../constants/app.constant"
import { color } from "./color"
import { SafeAreaViewProps } from "react-navigation"
import DeviceInfo from 'react-native-device-info'

export const isIPhone: boolean = Platform.OS === 'ios'

export const baseStackNavigaitonOptions: StackNavigationOptions = {
    gestureEnabled: true,
    gestureDirection: 'horizontal',
    cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
    cardStyle: {
        backgroundColor: color.clear
    },
    cardOverlayEnabled: false
}

const sceneContainerOptions = {
    sceneContainerStyle: {
        backgroundColor: color.clear
    }
}

export const drawerOptions = {
    ...sceneContainerOptions,
    screenOptions: {
        swipeEnabled: false
    }
}

export const tabNavigationOptions = {
    ...sceneContainerOptions,
    timingConfig: {
        duration: 500,
        useNativeDriver: true
    }
}

export const tabNavigationDisableSwipeOptions = {
    swipeEnabled: false,
}

export const disableGesture = {
    gestureEnabled: false
}

export const clearBackgroundOption = {
    cardStyle: {
        backgroundColor: color.clear
    }
}

export const modalOverFullScreenOptions = {
    ...disableGesture,
    cardStyleInterpolator: CardStyleInterpolators.forFadeFromBottomAndroid,
    transitionSpec: {
        open: {
            animation: 'spring',
            config: {
                speed: 0,
                bounciness: 1000
            }
        },
        close: {
            animation: 'spring',
            config: {
                speed: 500,
            }
        }
    } as any
}

const forFade = ({ current, closing }) => ({
    cardStyle: {
        opacity: current.progress,
        backgroundColor: color.clear
    },
})

export const firstStackNavigaitonOptions: StackNavigationOptions = {
    ...baseStackNavigaitonOptions,
    cardStyleInterpolator: forFade
}

export const screenWidth: number = Dimensions.get('window').width
export const screenHeight: number = Dimensions.get('window').height

export const ratioWidth = (value: number): number => {
    const widthPercentage = (value * 100) / DesignDimension.Width
    return widthPercentageToDP(widthPercentage)
}

export const ratioHeight = (value: number): number => {
    const heightPercentage = (value * 100) / DesignDimension.Height
    return heightPercentageToDP(heightPercentage)
}

const widthPercentageToDP = (widthPercent: string | number) => {
    const elemWidth = typeof widthPercent === "number" ? widthPercent : parseFloat(widthPercent)
    return PixelRatio.roundToNearestPixel(screenWidth * elemWidth / 100)
}
const heightPercentageToDP = (heightPercent: string | number) => {
    const elemHeight = typeof heightPercent === "number" ? heightPercent : parseFloat(heightPercent)
    return PixelRatio.roundToNearestPixel(screenHeight * elemHeight / 100)
}

export const keyboardBehavior: any = isIPhone ? "padding" : "height"

export const solidScrollView: any = { bounces: false, showsVerticalScrollIndicator: false }

export const maxCharVerifyCode: number = 6

export const disableAutoCompleteTextField:any = {
    autoCompleteType: "off",
    autoCorrect: false,
}

const baseSecureTextField: any = {
    blurOnSubmit: true,
    selectTextOnFocus: false,
    contextMenuHidden: true,
}

export const secureTextField: any = {
    ...baseSecureTextField,
    secureTextEntry: true
}

export const verifyCodeTextFieldOptions: any = {
    ...baseSecureTextField,
    maxLength: maxCharVerifyCode
}

export const buttonSpringOptions: any = {
    tension: 250,
    duration: 50,
    useNativeDriver: false
}
export const buttonAnimatedOptions: any = {
    delay: 0,
    scale: 0.99,
    moveSlop: 15
}
export const animatedButtonDebounceOptions: any = {
    delay: 800,
}
export const isNewIPhone: boolean = newIphoneDeviceIds.includes(DeviceInfo.getDeviceId())
export const bouceButtonOptions: any = {
    buttonAnimatedOptions: buttonAnimatedOptions,
    buttonSpringOptions: buttonSpringOptions,
    animatedButtonDebounceOptions: animatedButtonDebounceOptions
}
export const keyboardVerticalOffSet: number = isIPhone ? ratioHeight(0) : ratioHeight(0)
export const keyboardVerticalOffSetForChat: number = isIPhone ? ratioHeight(-18) : ratioHeight(0)
export const backButtonHeaderMarginTop: number = !isIPhone ? ratioHeight(40) : 0
export const backButtonHeaderHeight: number = ratioHeight(35)
export const backButtonHeaderMargin: number = ratioHeight(8)
export const actualHeaderMargin: number = backButtonHeaderHeight + backButtonHeaderMargin
export const minAge: number = 18
export const starterMaxAge: number = 35
export const maxAge: number = 99
export const maxDescriptionChar: number = 1000
export const maxDudeProfileTabIndex: number = 3
export const DrawerStyleConfig = (isFirstLoadDone: boolean): StyleProp<ViewStyle> => {
    return {
        width: isFirstLoadDone ? ratioWidth(354.5) : 0,
        marginTop: ratioHeight(48 + 13),
        backgroundColor: color.clear
    }
}

export const ageRangeOptions = {
    min: minAge,
    max: maxAge,
    step: 1,
    gravity: 'center',
    selectionColor: color.palette.pink,
    blankColor: color.slideBar,
    thumbColor: color.palette.pink,
    thumbBorderColor: color.palette.pink,
    labelStyle: "none"
}

export const animatedViewOptions: any = {
    shouldRasterizeIOS: true,
    useNativeDriver: isIPhone,
    animation: 'fadeIn',
    duration: 300
}

export const safeAreaViewProps: SafeAreaViewProps = {
    forceInset: {
        bottom: 'never'
    }
}

export const flatlistViewAbilityConfig = {
    waitForInteraction: false,
    itemVisiblePercentThreshold: 0
}

export const flatlistResultAnimationConfig = {
    animation: "slideInDown",
    duration: 300,
} 