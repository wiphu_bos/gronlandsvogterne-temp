import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

const size = {
    header: RFPercentage(6),
    extraLarge: RFPercentage(4),
    large: RFPercentage(3),
    medium: RFPercentage(2.7),
    small: RFPercentage(2.5),
    tiny: RFPercentage(1),
    errorMessage: RFPercentage(2),
}

const type = {
    base: 'BaiJamjuree-Regular',
    bold: 'BaiJamjuree-Bold',
    // emphasis: 'Roboto-Italic'
}

export {
    size,
    type
}