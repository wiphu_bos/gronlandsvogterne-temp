import { NativeModules } from 'react-native'

import { NavigationKey } from "../constants/app.constant"

export const screenExitList = [NavigationKey.Home]

const { ReactNativeConfig } = NativeModules
export const environment = ReactNativeConfig.buildEnvironment
export const isDevelopment = (environment === "Development" || environment === "Alpha")

// export const packageName = () => {
//     switch (environment) {
//         case "Development":
//             return 'com.datemindude.dev'
//         case "Alpha":
//             return 'com.datemindude.alpha'
//         case "Staging":
//             return 'com.datemindude.staging'
//         default:
//             return 'com.datemindude'
//     }
// }

export const packageName = () => {
    switch (environment) {
        case "Development":
            return 'com.gronlandsvogterne.dev'
        case "Alpha":
            return 'com.gronlandsvogterne.dev'
        case "Staging":
            return 'com.gronlandsvogterne.dev'
        default:
            return 'com.gronlandsvogterne.dev'
    }
}