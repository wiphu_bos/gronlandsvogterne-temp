import * as Utils from "../utils"
import { StoreName } from "../constants/app.constant"
type IOptions = {
    jsonify?: boolean,
    readonly storeBlacklist?: Utils.StringToBooleanMap
}

const All = Object.values(StoreName)
const Default = [StoreName.Navigation, StoreName.Shared, StoreName.FetchingResourcesPropertyStore, StoreName.Filter]

const storeBlacklistStorageKeys: Array<string> = Default


export const storagePersistOptions: IOptions = {
    jsonify: true,
    storeBlacklist: Utils.arrayToDictionary(storeBlacklistStorageKeys)
}

export const ROOT_STATE_STORAGE_KEY: string = StoreName.Root