import { isDevelopment } from "./app.config"

export const BaseURL: string = isDevelopment ? "https://us-central1-gronlandsvogterne-app-dev.cloudfunctions.net/api/" : "https://us-central1-gronlandsvogterne-app-dev.cloudfunctions.net/api/"

export const ContentType: { [key: string]: any } = {
    URL_ENCODED: 'application/x-www-form-urlencoded',
    JSON: "application/json"
}
export const Timeout: number = 7000