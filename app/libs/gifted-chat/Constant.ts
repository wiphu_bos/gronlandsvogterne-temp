import { Platform } from 'react-native'
import * as metric from "../../theme"

export const MIN_COMPOSER_HEIGHT = Platform.select({
  ios: metric.ratioHeight(-44),
  android: metric.ratioHeight(-51),
  web: metric.ratioHeight(-44)
})
export const MAX_COMPOSER_HEIGHT = metric.ratioHeight(100)
export const DEFAULT_PLACEHOLDER = 'Type a message...'
export const DATE_FORMAT = 'll'
export const TIME_FORMAT = 'LT'
