import PropTypes from 'prop-types'
import React from 'react'
import { Platform, TextInput, TextInputProps, View, ViewStyle, TextStyle } from 'react-native'
import { MIN_COMPOSER_HEIGHT, DEFAULT_PLACEHOLDER } from '../../libs/gifted-chat/Constant'
import Color from '../../libs/gifted-chat/Color'
import { StylePropType } from '../../libs/gifted-chat/utils'
import * as Styles from "../../components/chat-input/chat-input.styles"
import * as metric from "../../theme"
import { color } from '../../theme'
import { TITLE } from '../../components/chat-input/chat-input.styles'
import { RFValue } from 'react-native-responsive-fontsize'

const lineHeight:number = metric.ratioHeight(25)

let TEXT_INPUT: ViewStyle & TextStyle = {
    flex:1,
    borderRadius: metric.ratioWidth(10),
    backgroundColor: color.palette.white,
    minHeight: metric.ratioHeight(50),
    ...TITLE,
    color: color.palette.black,
    marginHorizontal: metric.ratioWidth(15),
    fontSize: RFValue(12),
    lineHeight: lineHeight,
}
export interface ComposerProps {
  composerHeight?: number
  text?: string
  placeholder?: string
  placeholderTextColor?: string
  textInputProps?: Partial<TextInputProps>
  textInputStyle?: TextInputProps['style']
  textInputAutoFocus?: boolean
  keyboardAppearance?: TextInputProps['keyboardAppearance']
  multiline?: boolean
  disableComposer?: boolean
  onTextChanged?(text: string): void
  onInputSizeChanged?(contentSize: { width: number; height: number }): void
}

export default class ChatInputComposerWrapped extends React.Component<ComposerProps> {
  static defaultProps = {
    composerHeight: MIN_COMPOSER_HEIGHT,
    text: '',
    placeholderTextColor: Color.defaultColor,
    placeholder: DEFAULT_PLACEHOLDER,
    textInputProps: null,
    multiline: true,
    disableComposer: false,
    textInputStyle: {},
    textInputAutoFocus: false,
    keyboardAppearance: 'default',
    onTextChanged: () => { },
    onInputSizeChanged: () => { },
  }

  static propTypes = {
    composerHeight: PropTypes.number,
    text: PropTypes.string,
    placeholder: PropTypes.string,
    placeholderTextColor: PropTypes.string,
    textInputProps: PropTypes.object,
    onTextChanged: PropTypes.func,
    onInputSizeChanged: PropTypes.func,
    multiline: PropTypes.bool,
    disableComposer: PropTypes.bool,
    textInputStyle: StylePropType,
    textInputAutoFocus: PropTypes.bool,
    keyboardAppearance: PropTypes.string,
  }

  contentSize?: { width: number; height: number } = undefined

  onContentSizeChange = (e: any) => {
    const { contentSize } = e.nativeEvent

    // Support earlier versions of React Native on Android.
    if (!contentSize) {
      return
    }

    if (
      !this.contentSize ||
      (this.contentSize &&
        (this.contentSize.width !== contentSize.width ||
          this.contentSize.height !== contentSize.height))
    ) {
      this.contentSize = contentSize
      this.props.onInputSizeChanged!(this.contentSize!)
    }
  }

  onChangeText = (text: string) => {
    this.props.onTextChanged!(text)
  }

  render() {

    const containerHeight = (): number => {
      const calcComposerHeight = this.props.composerHeight
      const useAdjustedComposerHeight = this.contentSize && this.contentSize.height > lineHeight
      return useAdjustedComposerHeight ? calcComposerHeight + metric.ratioHeight(15) : calcComposerHeight
    }

    const textInputStyles = () => {
      const useDefaultTextInputStyles = this.contentSize && this.contentSize.height > lineHeight
      return useDefaultTextInputStyles ? TEXT_INPUT : 
      {...TEXT_INPUT, paddingTop: metric.ratioHeight(10)} as ViewStyle & TextStyle
    }

    return (
      <View style={
        [TEXT_INPUT,
          {
            height: containerHeight()
          }
        ]
      }>
      <TextInput
        testID={this.props.placeholder}
        autoCompleteType="off"
        autoCorrect={false}
        accessible
        accessibilityLabel={this.props.placeholder}
        placeholder={this.props.placeholder}
        placeholderTextColor={this.props.placeholderTextColor}
        multiline={this.props.multiline}
        editable={!this.props.disableComposer}
        onChange={this.onContentSizeChange}
        onContentSizeChange={this.onContentSizeChange}
        onChangeText={this.onChangeText}
        selectionColor={Styles.CURSOR_COLOR}
        style={[
          textInputStyles(),
          this.props.textInputStyle,
          {
            height: this.props.composerHeight,
            ...Platform.select({
              web: {
                outlineWidth: 0,
                outlineColor: 'transparent',
                outlineOffset: 0,
              },
            }),
          }
        ]}
        autoFocus={this.props.textInputAutoFocus}
        value={this.props.text}
        enablesReturnKeyAutomatically
        underlineColorAndroid='transparent'
        keyboardAppearance={this.props.keyboardAppearance}
        {...this.props.textInputProps}
      />
      </View>
    )
  }
}
