import PropTypes from 'prop-types'
import React from 'react'
import {
  Text,
  TouchableOpacity,
  View,
  StyleSheet,
  StyleProp,
  ImageStyle,
  TextStyle,
} from 'react-native'
import Color from './Color'
import { StylePropType } from './utils'
import * as metric from "../../theme"
import FastImage from 'react-native-fast-image'
import { UserStore } from '../../models/user-store/user.types'

const {
  carrot,
  emerald,
  peterRiver,
  wisteria,
  alizarin,
  turquoise,
  midnightBlue,
} = Color

const styles = StyleSheet.create({
  avatarStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    width: metric.ratioWidth(40),
    height: metric.ratioWidth(40),
    borderRadius: metric.ratioWidth(20)
  },
  avatarTransparent: {
    backgroundColor: Color.backgroundTransparent,
  },
  textStyle: {
    color: Color.white,
    fontSize: 16,
    backgroundColor: Color.backgroundTransparent,
    fontWeight: '100',
  },
})

export interface GiftedAvatarProps {
  user$?: UserStore
  avatarStyle?: StyleProp<ImageStyle>
  textStyle?: StyleProp<TextStyle>
  onPress?(props: any): void
  onLongPress?(props: any): void
}

export default class GiftedAvatar extends React.Component<GiftedAvatarProps> {
  static defaultProps = {
    user$: {
      full_name: null,
      profile_image: null,
    },
    onPress: undefined,
    onLongPress: undefined,
    avatarStyle: {},
    textStyle: {},
  }

  static propTypes = {
    user$: PropTypes.object,
    onPress: PropTypes.func,
    onLongPress: PropTypes.func,
    avatarStyle: StylePropType,
    textStyle: StylePropType,
  }

  avatarName?: string = undefined
  avatarColor?: string = undefined

  setAvatarColor() {
    const userName = (this.props.user$ && this.props.user$.uid) || ''
    const name = userName.toUpperCase().split(' ')
    if (name.length === 1) {
      this.avatarName = `${name[0].charAt(0)}`
    } else if (name.length > 1) {
      this.avatarName = `${name[0].charAt(0)}${name[1].charAt(0)}`
    } else {
      this.avatarName = ''
    }

    let sumChars = 0
    for (let i = 0; i < userName.length; i += 1) {
      sumChars += userName.charCodeAt(i)
    }

    // inspired by https://github.com/wbinnssmith/react-user-avatar
    // colors from https://flatuicolors.com/
    const colors = [
      carrot,
      emerald,
      peterRiver,
      wisteria,
      alizarin,
      turquoise,
      midnightBlue,
    ]

    this.avatarColor = colors[sumChars % colors.length]
  }

  renderAvatar() {
    const { user$ } = this.props
    if (user$) {
      return (
        <FastImage
          source={{ uri: user$.profile_image[0]?.url }}
          style={[styles.avatarStyle, this.props.avatarStyle]}
        />
      )
    }
    return null
  }

  renderInitials() {
    return (
      <Text style={[styles.textStyle, this.props.textStyle]}>
        {this.avatarName}
      </Text>
    )
  }

  handleOnPress = () => {
    const { onPress, ...other } = this.props
    if (this.props.onPress) {
      this.props.onPress(other)
    }
  }

  handleOnLongPress = () => { }

  render() {
    const profile_image = this.props.user$.profile_image
    if (
      !this.props.user$ ||
      (!this.props.user$.full_name && profile_image?.length > 0 && !profile_image[0]?.url)
    ) {
      // render placeholder
      return (
        <View
          style={[
            styles.avatarStyle,
            styles.avatarTransparent,
            this.props.avatarStyle,
          ]}
          accessibilityTraits='image'
        />
      )
    }
    if (profile_image?.length > 0 && profile_image[0]?.url) {
      return (
        <TouchableOpacity
          activeOpacity={1}
          disabled={!this.props.onPress}
          onPress={this.props.onPress}
          onLongPress={this.props.onLongPress}
          accessibilityTraits='image'
        >
          {this.renderAvatar()}
        </TouchableOpacity>
      )
    }

    this.setAvatarColor()

    return (
      <TouchableOpacity
        disabled={!this.props.onPress}
        onPress={this.props.onPress}
        onLongPress={this.props.onLongPress}
        style={[
          styles.avatarStyle,
          { backgroundColor: this.avatarColor },
          this.props.avatarStyle,
        ]}
        accessibilityTraits='image'
      >
        {this.renderInitials()}
      </TouchableOpacity>
    )
  }
}
