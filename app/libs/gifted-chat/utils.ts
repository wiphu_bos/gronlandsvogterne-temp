import PropTypes from 'prop-types'
import dayjs from 'dayjs'

import { IMessage } from './Models'

export const StylePropType = PropTypes.oneOfType([
  PropTypes.array,
  PropTypes.object,
  PropTypes.number,
  PropTypes.bool,
])

export function isSameDay(
  currentMessage: IMessage,
  diffMessage: IMessage | null | undefined,
) {
  if (!diffMessage || !diffMessage.created_date) {
    return false
  }

  const currentCreatedDate = dayjs(currentMessage.created_date)
  const diffCreatedDate = dayjs(diffMessage.created_date)

  if (!currentCreatedDate.isValid() || !diffCreatedDate.isValid()) {
    return false
  }

  return currentCreatedDate.isSame(diffCreatedDate, 'day')
}

export function isSameUser(
  currentMessage: IMessage,
  diffMessage: IMessage | null | undefined,
) {
  return !!(
    diffMessage &&
    diffMessage.sender &&
    currentMessage.sender &&
    diffMessage.sender.uid === currentMessage.sender.uid
  )
}

const styleString = (color: string) => `color: ${color}; font-weight: bold`

const headerLog = '%c[react-native-gifted-chat]'

export const warning = (...args: any) =>
  console.log(headerLog, styleString('orange'), ...args)

export const error = (...args: any) =>
  console.log(headerLog, styleString('red'), ...args)
