import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    Animated,
    TouchableOpacity,
} from 'react-native'
import * as Animatable from 'react-native-animatable'

import * as metric from "../../theme"
import { color } from "../../theme"
import { RFPercentage, RFValue } from "react-native-responsive-fontsize"
import { Icon, Button } from "../../components"
import { images } from '../../theme/images';

class FloatingLabel extends Component {
    constructor(props) {
        super(props);

        let initialPadding = 0;
        let initialOpacity = 0;

        if (this.props.visible) {
            initialPadding = 0;
            initialOpacity = 1;
        }

        this.state = {
            paddingAnim: new Animated.Value(initialPadding),
            opacityAnim: new Animated.Value(initialOpacity)
        }
    }

    UNSAFE_componentWillReceiveProps(newProps) {
        const animatedOptions = {
            useNativeDriver: false,
            duration: 230
        }
        Animated.timing(this.state.paddingAnim, {
            toValue: newProps.visible ? 0 : 1,
            ...animatedOptions
        }).start();

        return Animated.timing(this.state.opacityAnim, {
            toValue: newProps.visible ? 1 : 0,
            ...animatedOptions
        }).start();
    }

    render() {
        return (
            <Animated.View shouldRasterizeIOS useNativeDriver style={[styles.floatingLabel, { paddingTop: this.state.paddingAnim, opacity: this.state.opacityAnim }]}>
                {this.props.children}
            </Animated.View>
        );
    }
}

class FloatLabelTextField extends Component {
    constructor(props) {
        super(props);
        this.state = {
            focused: false,
            text: this.props.value
        };
    }

    UNSAFE_componentWillReceiveProps(newProps) {
        if (newProps.hasOwnProperty('value') && newProps.value !== this.state.text) {
            this.setState({ text: newProps.value })
        }
    }

    leftPadding(isDisabled) {
        return {
            width: this.props.leftPadding || metric.ratioWidth(20),
            backgroundColor: isDisabled ? color.disabledTextField : color.palette.white
        }
    }

    withBorder() {
        if (!this.props.noBorder) {
            return styles.withBorder;
        }
    }

    renderDropdownIconIfNeeded(onTouch) {
        return this.props.isDropDown &&
            <Button
                isAnimated={false}
                isSolid
                preset="none"
                imageSource={images.down}
                style={styles.iconModalContainer}
                imageStyle={styles.downArrow}
                onPress={onTouch} />
    }

    renderMultitpleSelectionIconIfNeeded(onTouch) {
        return this.props.isMultipleSelectionDropDown &&
            <Button
                isAnimated={false}
                isSolid
                preset="none"
                imageSource={images.plus}
                style={styles.iconModalContainer}
                imageStyle={styles.plus}
                onPress={onTouch} />
    }

    renderTouchableView(onTouch) {
        return (this.props.onTouchTextField || this.props.onTouchTextFieldXL)
            && <TouchableOpacity
                style={this.props.onTouchTextFieldXL ? styles.touchableViewXL : styles.touchableView}
                onPress={onTouch} activeOpacity={1} />
    }

    renderDescriptionViewIfNeeded() {
        return this.props.description &&
            <View shouldRasterizeIOS style={styles.descriptionContainer}>
                <Text {...this.props.testIDDescription} style={styles.descriptionText}>{this.props.description}</Text>
            </View> || null
    }

    renderErrorMessageIfNeeded() {
        return this.props.errorMessage &&
            <Animatable.View shouldRasterizeIOS useNativeDriver animation="fadeIn" style={{ ...styles.errorContainer, ...this.props.containerStyle }}
                {...this.props.testIDErrorMessage}
            >
                <Icon icon="warning" containerStyle={styles.warningContainer} style={styles.warning} />
                <Text style={styles.errorText}>{this.props.errorMessage}</Text>
            </Animatable.View> || null
    }
    renderMultipleSelectionViewIfNeeded(onTouch) {
        return this.props.multipleSelectionItems && <Animatable.View shouldRasterizeIOS useNativeDriver animation="fadeIn" duration={400} style={{ ...styles.multipleSelectionContainer, ...this.props.containerStyle }}
            {...this.props.testIDMultipleSelectionItems}
        >
            <View shouldRasterizeIOS style={styles.SEPARATOR_VIEW} />
            <TouchableOpacity onPress={onTouch} activeOpacity={1} style={styles.multitpleSelectiontouchableView}>
                <Text style={styles.multipleSelectionText}>{this.props.multipleSelectionItems}</Text>
            </TouchableOpacity>
        </Animatable.View> || null
    }
    render() {
        const textfieldOptions = this.props.textfieldOptions
        const paddingStyle = this.props.multipleSelectionItems ? styles.multitpleSelectionPaddingView : styles.paddingView
        const containerStyle = [paddingStyle, this.leftPadding(this.props.disabled),
            this.props.errorMessage && styles.paddingViewError || styles.valueTextNormal]
        const valueTextStyle = this.props.multipleSelectionItems ? styles.multitpleSelectionValueText : styles.valueText
        const textfieldStyle = [valueTextStyle,
            this.props.disabled === true ? styles.textFieldDisabled : styles.valueTextNormal,
            this.props.errorMessage && styles.valueTextError || styles.valueTextNormal]
        const onTouch = () => this.props.onTouchTextField && this.props.onTouchTextField() ||
            this.props.onTouchTextFieldXL && this.props.onTouchTextFieldXL()
        return (
            <View shouldRasterizeIOS style={{ ...styles.container, ...this.props.containerStyle }}>
                <View shouldRasterizeIOS style={styles.viewContainer}>
                    <FloatingLabel visible={this.state.text}>
                        <Text numberOfLines={1} style={[styles.fieldLabel, this.labelStyle()]}>{this.placeholderValue()}</Text>
                    </FloatingLabel>
                    <View shouldRasterizeIOS style={containerStyle} />
                    <View shouldRasterizeIOS style={[styles.fieldContainer, this.withBorder()]}>
                        <TextInput {...this.props}
                            autoCompleteType="off"
                            autoCorrect={false}
                            ref='input'
                            underlineColorAndroid={color.palette.clear}
                            style={textfieldStyle}
                            defaultValue={this.props.defaultValue}
                            value={this.props.value}
                            maxLength={this.props.maxLength}
                            onFocus={() => this.setFocus()}
                            onBlur={() => this.unsetFocus()}
                            onChangeText={(value) => this.setText(value)}
                            selectionColor={color.palette.pink}
                            numberOfLines={this.props.maxLength || 1}
                            allowFontScaling
                        />
                        {this.renderDropdownIconIfNeeded(onTouch)}
                        {this.renderMultitpleSelectionIconIfNeeded(onTouch)}
                        {this.renderTouchableView(onTouch)}
                    </View>
                </View>
                {this.renderDescriptionViewIfNeeded()}
                {this.renderErrorMessageIfNeeded(onTouch)}
                {this.renderMultipleSelectionViewIfNeeded(onTouch)}
            </View>
        );
    }

    clearErrorMessage() {
        // this.props.errorMessage = null
    }

    inputRef() {
        return this.refs.input;
    }

    focus() {
        this.inputRef().focus();
    }

    blur() {
        this.inputRef().blur();
    }

    isFocused() {
        return this.inputRef().isFocused();
    }

    clear() {
        this.inputRef().clear();
    }

    setFocus() {
        this.setState({
            focused: true
        });
        try {
            return this.props.onFocus();
        } catch (_error) { }
    }

    unsetFocus() {
        this.setState({
            focused: false
        });
        try {
            return this.props.onBlur();
        } catch (_error) { }
    }

    labelStyle() {
        if (this.state.focused) {
            return styles.focused;
        }
    }

    placeholderValue() {
        if (this.state.text) {
            return this.props.placeholder;
        }
    }

    setText(value) {
        this.setState({
            text: value
        });
        try {
            return this.props.onChangeTextValue(value);
        } catch (_error) { }
    }
}

const REGULAR_TEXT = {
    fontFamily: 'Montserrat',
    fontWeight: '400',
    //Android
    fontFamily: 'Montserrat-Regular',
}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        justifyContent: 'center',
        width: metric.ratioWidth(300)
    },
    viewContainer: {
        // flex: 1,
        flexDirection: 'row',

    },
    paddingView: {
        height: metric.ratioHeight(50),
        width: metric.ratioWidth(0),
        backgroundColor: 'white',
        borderTopLeftRadius: metric.ratioWidth(9),
        borderBottomLeftRadius: metric.ratioWidth(9),
        marginTop: metric.ratioHeight(30),
    },
    multitpleSelectionPaddingView: {
        height: metric.ratioHeight(50),
        width: metric.ratioWidth(0),
        backgroundColor: 'white',
        borderTopLeftRadius: metric.ratioWidth(9),
        marginTop: metric.ratioHeight(30),
    },
    paddingViewNormal: {
        borderLeftWidth: 0,
        borderTopWidth: 0,
        borderBottomWidth: 0,
    },
    paddingViewError: {
        //Error
        borderTopColor: color.palette.red,
        borderLeftColor: color.palette.red,
        borderBottomColor: color.palette.red,
        borderLeftWidth: metric.ratioWidth(2),
        borderTopWidth: metric.ratioWidth(2),
        borderBottomWidth: metric.ratioWidth(2),
    },
    floatingLabel: {
        position: 'absolute',
        top: metric.ratioHeight(6),
        left: metric.ratioWidth(6)
    },
    fieldLabel: {
        height: metric.ratioHeight(21),
        fontSize: RFValue(13),
        color: color.palette.white,
        fontFamily: 'Montserrat',
        fontWeight: '300',
        //Android
        fontFamily: 'Montserrat-Light',
    },
    fieldContainer: {
        flex: 1,
        justifyContent: 'center',
        position: 'relative',
    },
    withBorder: {
        borderBottomWidth: 1 / 2,
        borderColor: '#C8C7CC',
    },
    multitpleSelectionValueText: {
        ...REGULAR_TEXT,
        marginTop: metric.ratioHeight(30),
        height: metric.ratioHeight(50),
        fontSize: RFValue(17),
        color: color.palette.darkGrey,
        paddingVertical: !metric.isIPhone ? metric.ratioHeight(11) : 0,
        backgroundColor: color.palette.white,
        borderTopRightRadius: metric.ratioWidth(9),
        paddingRight: metric.ratioWidth(40),
    },
    valueText: {
        ...REGULAR_TEXT,
        marginTop: metric.ratioHeight(30),
        height: metric.ratioHeight(50),
        fontSize: RFValue(17),
        color: color.palette.darkGrey,
        paddingVertical: !metric.isIPhone ? metric.ratioHeight(11) : 0,
        backgroundColor: color.palette.white,
        borderBottomRightRadius: metric.ratioWidth(9),
        borderTopRightRadius: metric.ratioWidth(9),
        paddingRight: metric.ratioWidth(14),
    },
    valueTextWithIcon: {
        ...REGULAR_TEXT,
        marginTop: metric.ratioHeight(30),
        height: metric.ratioHeight(50),
        fontSize: RFValue(17),
        color: color.palette.darkGrey,
        paddingVertical: !metric.isIPhone ? metric.ratioHeight(11) : 0,
        backgroundColor: color.palette.white,
        borderBottomRightRadius: metric.ratioWidth(9),
        borderTopRightRadius: metric.ratioWidth(9),
        marginRight: 100
    },
    valueTextError: {
        //Error
        borderTopColor: color.palette.red,
        borderRightColor: color.palette.red,
        borderBottomColor: color.palette.red,
        borderRightWidth: metric.ratioWidth(2),
        borderTopWidth: metric.ratioWidth(2),
        borderBottomWidth: metric.ratioWidth(2),

    },

    valueTextNormal: {
        borderLeftWidth: 0,
        borderTopWidth: 0,
        borderBottomWidth: 0,
    },
    focused: {
        color: color.palette.white
    },
    descriptionContainer: {
        marginTop: metric.ratioHeight(6),
        width: metric.ratioWidth(300),
        marginBottom: metric.ratioHeight(7),
        paddingLeft: metric.ratioWidth(3),
    },
    descriptionText: {
        ...REGULAR_TEXT,
        fontSize: RFValue(11),
        color: color.palette.white,
        lineHeight: metric.ratioHeight(17),
    },
    errorContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: metric.ratioHeight(6),
        backgroundColor: color.palette.red,
        borderRadius: metric.ratioHeight(30 / 2),
        width: metric.ratioWidth(300),
        minHeight: metric.ratioHeight(30),
        paddingLeft: metric.ratioWidth(11.23),
        marginBottom: metric.ratioHeight(7),
    },
    errorText: {
        fontSize: RFValue(11),
        color: color.palette.white,
        fontFamily: 'Montserrat',
        fontWeight: '400',
        //Android
        fontFamily: 'Montserrat-Regular',
        marginLeft: metric.ratioWidth(9.23),
        width: metric.ratioWidth(270),
        marginVertical: metric.ratioHeight(5),
    },
    multipleSelectionContainer: {
        flexDirection: 'column',
        backgroundColor: color.palette.white,
        borderBottomRightRadius: metric.ratioHeight(30 / 2),
        borderBottomLeftRadius: metric.ratioHeight(30 / 2),
        width: metric.ratioWidth(300),
        minHeight: metric.ratioHeight(50),
        paddingHorizontal: metric.ratioWidth(21),
        justifyContent: 'center',
    },
    warningContainer: {

    },
    warning: {
        width: metric.ratioWidth(12.53),
        height: metric.ratioWidth(12.53),
    }, iconModalContainer: {
        position: 'absolute',
        bottom: metric.ratioHeight(14),
        right: metric.ratioWidth(20),
        zIndex: 1,
        justifyContent: 'center'
    },
    downArrow: {
        width: metric.ratioWidth(16),
        height: metric.ratioHeight(18),
    },
    plus: {
        width: metric.ratioWidth(18),
        height: metric.ratioWidth(18),
    },
    touchableView: {
        position: 'absolute',
        left: metric.ratioWidth(-19.8),
        right: 0,
        bottom: 0,
        width: metric.ratioWidth(300),
        height: metric.ratioHeight(50),
        backgroundColor: color.clear,
        zIndex: 2
    },
    touchableViewXL: {
        position: 'absolute',
        left: metric.ratioWidth(-19.8),
        right: 0,
        bottom: 0,
        width: metric.ratioWidth(344),
        height: metric.ratioHeight(50),
        backgroundColor: color.clear,
        zIndex: 2,
    },
    multitpleSelectiontouchableView: {
        flex: 1,
        justifyContent: 'center'
    },
    textFieldDisabled: {
        flex: 1,
        backgroundColor: color.disabledTextField
    },
    multipleSelectionText: {
        fontSize: RFValue(12),
        fontWeight: '400',

        //Android
        fontFamily: "Montserrat-Regular",
        color: color.palette.darkGrey,
    },
    SEPARATOR_VIEW: {
        height: metric.ratioHeight(1),
        backgroundColor: color.boder,
        width: metric.ratioWidth(344),
        marginLeft: -metric.ratioWidth(20)
    }
});

export default FloatLabelTextField;