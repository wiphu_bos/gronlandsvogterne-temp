/* eslint import/extensions:0 */
/* eslint import/no-unresolved:0 */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  View,
  FlatList,
  TouchableOpacity,
  Text,
  TextInput,
  KeyboardAvoidingView,
  Platform,
  SafeAreaView
} from 'react-native'
import Modal from '../../libs/react-native-modal'
import { Icon } from "../../components"
import styles from "./styles"
import * as metric from '../../theme'
import { color } from '../../theme'
import { images } from "../../theme/images"


export default class ModalFilterPicker extends Component {
  static keyExtractor(item, index) {
    return index
  }
  constructor(props, ctx) {
    super(props, ctx)

    this.state = {
      filter: "",
      ds: props.options || [],
      isMultipleSelectionDropDown: this.props.isMultipleSelectionDropDown || false,
      selectedOptions: this.props.selectedOptions || []
    }
  }

  UNSAFE_componentWillReceiveProps(newProps) {
    if (
      (!this.props.visible && newProps.visible) ||
      this.props.options !== newProps.options ||
      this.props.selectedOptions !== newProps.selectedOptions
    ) {
      this.setState({
        filter: "",
        ds: newProps.options || [],
        isMultipleSelectionDropDown: newProps.isMultipleSelectionDropDown || false,
        selectedOptions: newProps.selectedOptions || []
      })
    }
  }

  render() {
    const {
      title,
      titleTextStyle,
      overlayStyle,
      renderList,
      modal,
      visible,
      onCancel,
    } = this.props

    const renderedTitle = !title ? null : (
      <Text style={titleTextStyle || styles.titleTextStyle}>{title}</Text>
    )

    return (
      <Modal
        onShow={() => this.scrollToSelectedIndex()}
        animationIn="fadeIn"
        animationOut="fadeOut"
        useNativeDriver
        presentationStyle="overFullScreen"
        onRequestClose={onCancel}
        {...modal}
        isVisible={visible}
        supportedOrientations={['portrait']}
      >
        <KeyboardAvoidingView
          behavior={metric.keyboardBehavior}
          style={overlayStyle || styles.overlay}
        >
          <SafeAreaView shouldRasterizeIOS style={styles.modalContainer}>
            <View>{renderedTitle}</View>
            {(renderList || this.renderList)()}
          </SafeAreaView>
        </KeyboardAvoidingView>
      </Modal>
    )
  }

  renderList = () => {
    const {
      showFilter,
      autoFocus,
      listContainerStyle,
      androidUnderlineColor,
      placeholderText,
      placeholderTextColor,
      filterTextInputContainerStyle,
      filterTextInputStyle,
      keyboardType,
    } = this.props
    const filter = !showFilter ? null : (
      <View
        style={filterTextInputContainerStyle || styles.filterTextInputContainer}
      >
        <TextInput
          onChangeText={this.onFilterChange}
          autoCompleteType="off"
          autoCorrect={false}
          blurOnSubmit={true}
          autoFocus={autoFocus}
          autoCapitalize="none"
          underlineColorAndroid={androidUnderlineColor}
          placeholderTextColor={placeholderTextColor}
          placeholder={placeholderText}
          style={filterTextInputStyle || styles.filterTextInput}
          selectionColor={color.palette.pink}
          keyboardType={keyboardType}
        />
        {this.state.isMultipleSelectionDropDown ? this.renderClearButton() : null}
      </View>
    )

    return (
     <View shouldRasterizeIOS style={listContainerStyle || styles.listContainer} >
        {filter}
        {this.renderOptionList()}
        {this.renderBottomMenu()}
      </View>
    )
  }

  renderOptionList = () => {
    const {
      flatListViewProps,
      keyExtractor,
      keyboardShouldPersistTaps,
    } = this.props

    const { ds } = this.state

    if (!ds.length) {
      return (
        <FlatList
          style={styles.listContentStyle}
          data={ds}
          keyExtractor={keyExtractor || this.constructor.keyExtractor}
          extraData={this.state}
          {...flatListViewProps}
        />
      )
    }
    return (
    <FlatList
        ref='list'
        style={styles.listContentStyle}
        keyExtractor={keyExtractor || this.constructor.keyExtractor}
        {...flatListViewProps}
        data={ds}
        renderItem={this.renderOption}
        extraData={this.state}
        onScrollToIndexFailed={this.onScrollToIndexFailed()}
        keyboardShouldPersistTaps={keyboardShouldPersistTaps}
      />
    )
  }

  inputRef() {
    return this.refs.list
  }

  onOK() {
    const { selectedOptions } = this.state
    this.props.onOK(selectedOptions)
  }

  onScrollToIndexFailed() {
    return info => {
      const wait = new Promise(resolve => setTimeout(resolve, 500));
      wait.then(() => {
        this.inputRef()?.scrollToIndex({ index: info.index, animated: true, viewPosition: 0.5 });
      });
    }
  }

  scrollToSelectedIndex() {
    this.inputRef()?.scrollToIndex({ animated: true, index: this.props.flatListViewProps?.selectedIndex || 0, viewPosition: 0.5 });
  }

  onClearButtonPressed = () => {
    this.setState({ selectedOptions: [] })
  }

  onMultipleSelect = (item) => {
    const { selectedOptions } = this.state
    const { key } = item
    let selectedList = selectedOptions || []
    const foundItemIndex = selectedList?.findIndex(v => v.key === key)
    if (selectedList?.length > 0 && foundItemIndex !== -1) {
      selectedList.splice(foundItemIndex, 1)
    } else {
      selectedList.push(item)
    }
    this.setState({ selectedOptions: selectedList })

  }

  onDismissModal = () => {
    if (this.props.onClose) {
      return this.props.onClose()
    }
    return this.props.onCancel && this.props.onCancel()
  }

  renderOption = ({ item }) => {
    const {
      selectedOption,
      renderOption,
      optionTextStyle,
      selectedOptionTextStyle,
      iconSelectionStyle,
    } = this.props

    const { selectedOptions } = this.state
    const { key, label } = item
    let style = styles.optionStyle
    let textStyle = optionTextStyle || styles.optionTextStyle
    if (this.state.isMultipleSelectionDropDown) {
      return (
        <TouchableOpacity
          activeOpacity={1}
          style={style}
          onPress={() => this.onMultipleSelect(item)}
        >
          <Icon source={selectedOptions.length > 0 && selectedOptions?.filter(v => v.key == key).length > 0 ? images.selected : images.unSelected} style={iconSelectionStyle} />
          <Text style={textStyle}>{label}</Text>
        </TouchableOpacity>
      )
    } else {
      if (key === selectedOption) {
        style = styles.selectedOptionStyle
        textStyle = selectedOptionTextStyle || styles.selectedOptionTextStyle
      }
      if (renderOption) return renderOption(item, key === selectedOption)
      return (
        <TouchableOpacity
          activeOpacity={0.7}
          style={style}
          onPress={() => this.props.onSelect(item)}
        >
          <Text style={textStyle}>{label}</Text>
        </TouchableOpacity>
      )
    }
  }

  renderClearButton = () => {
    const {
      clearButtonTextStyle,
      clearButtonText,
      clearContainerStyle,
    } = this.props
    return (
      <TouchableOpacity
        onPress={() => this.onClearButtonPressed()}
        activeOpacity={1}
        style={clearContainerStyle || styles.clearContainer}
      >
        <Text style={clearButtonTextStyle || styles.clearButtonText}>
          {clearButtonText}
        </Text>
      </TouchableOpacity>
    )
  }

  renderBottomMenu = () => {
    const {
      cancelContainerStyle,
      renderCancelButton
    } = this.props

    if (this.state.isMultipleSelectionDropDown) {
      return (
       <View shouldRasterizeIOS style={styles.footerMultipleSelectionContainerStyle}>
         <View shouldRasterizeIOS style={cancelContainerStyle || styles.cancelContainerMultipleSelection}>
            {(renderCancelButton || this.renderCancelButton)()}
          </View>
          {(this.renderOKButton)()}
        </View >)

    } else {
      return (
       <View shouldRasterizeIOS style={styles.footerSingleSelectionContainerStyle}>
         <View shouldRasterizeIOS style={cancelContainerStyle || styles.cancelContainerSingleSelection}>
            {(renderCancelButton || this.renderCancelButton)()}
          </View>
        </View>
      )
    }
  }

  renderOKButton = () => {
    const {
      okButtonTextStyle,
      okButtonText,
      okContainerStyle,
    } = this.props
    return (
      <TouchableOpacity
        onPress={() => this.onOK()}
        activeOpacity={1}
        style={okContainerStyle || styles.okContainerStyle}
      >
        <Text style={okButtonTextStyle || styles.okButtonTextStyle}>
          {okButtonText}
        </Text>
      </TouchableOpacity>
    )
  }

  renderCancelButton = () => {
    const {
      cancelButtonStyle,
      cancelButtonTextStyle,
      cancelButtonText,
    } = this.props
    return (

      <TouchableOpacity
        onPress={() => this.onDismissModal()}
        activeOpacity={0.7}
        style={cancelButtonStyle || styles.cancelButton}
      >
        <Text style={cancelButtonTextStyle || styles.cancelButtonText}>
          {cancelButtonText}
        </Text>
      </TouchableOpacity>
    )
  }

  onFilterChange = (text) => {
    const { options } = this.props

    const filter = text.toLowerCase()

    // apply filter to incoming data
    const filtered = !filter.length
      ? options
      : options?.filter(
        ({ searchKey, label }) =>
          label.toLowerCase().indexOf(filter) >= 0 ||
          (searchKey && searchKey.toLowerCase().indexOf(filter) >= 0)
      )
    /* eslint react/no-unused-state:0 */
    this.setState({
      filter: text.toLowerCase(),
      ds: filtered
    })
  }
}

/* eslint react/forbid-prop-types:0 */

ModalFilterPicker.propTypes = {
  options: PropTypes.array,
  onSelect: PropTypes.func,
  onCancel: PropTypes.func,
  onClose: PropTypes.func,
  onOK: PropTypes.func,
  keyboardType: PropTypes.string,
  placeholderText: PropTypes.string,
  placeholderTextColor: PropTypes.string,
  androidUnderlineColor: PropTypes.string,
  cancelButtonText: PropTypes.string,
  title: PropTypes.string,
  noResultsText: PropTypes.string,
  visible: PropTypes.bool,
  showFilter: PropTypes.bool,
  modal: PropTypes.object,
  selectedOption: PropTypes.string,
  renderOption: PropTypes.func,
  renderCancelButton: PropTypes.func,
  renderList: PropTypes.func,
  flatListViewProps: PropTypes.object,
  filterTextInputContainerStyle: PropTypes.any,
  filterTextInputStyle: PropTypes.any,
  cancelContainerStyle: PropTypes.any,
  cancelButtonStyle: PropTypes.any,
  cancelButtonTextStyle: PropTypes.any,
  titleTextStyle: PropTypes.any,
  overlayStyle: PropTypes.any,
  listContainerStyle: PropTypes.any,
  optionTextStyle: PropTypes.any,
  selectedOptionTextStyle: PropTypes.any,
  keyExtractor: PropTypes.any,
  autoFocus: PropTypes.any,
  keyboardShouldPersistTaps: PropTypes.string
}

ModalFilterPicker.defaultProps = {
  placeholderText: "",
  placeholderTextColor: color.palette.darkGrey,
  androidUnderlineColor: color.clear,
  cancelButtonText: "",
  clearButtonText: "",
  okButtonText: "",
  noResultsText: "",
  visible: false,
  showFilter: true,
  keyboardShouldPersistTaps: 'never'
}