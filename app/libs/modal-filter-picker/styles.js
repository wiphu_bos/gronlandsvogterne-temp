import { StyleSheet, Dimensions } from 'react-native'
import { color } from "../../theme"
import * as metric from "../../theme"
import { RFValue } from 'react-native-responsive-fontsize'

const { width, height } = Dimensions.get('window')

const optionStyle = {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: metric.ratioHeight(18),
    paddingHorizontal: metric.ratioWidth(10),
    borderBottomWidth: 1,
    borderBottomColor: color.clear,
    color: 'red'
}

const baseViewStyle = {

}

const baseTextStyle = {
    fontFamily: 'Montserrat'
}

const lightTextStyle = {
    fontWeight: '300',

    //Android
    fontFamily: 'Montserrat-Light'
}

const regularTextStyle = {
    fontWeight: '400',

    //Android
    fontFamily: 'Montserrat-Regular'
}

const optionTextStyle = {
    ...baseTextStyle,
    ...regularTextStyle,
    flex: 1,
    textAlign: 'left',
    color: color.palette.darkGrey,
    fontSize: RFValue(18),
}

const filterTextStyle = {
    ...baseTextStyle,
    ...regularTextStyle,
    fontSize: RFValue(18),
}

export default StyleSheet.create({
    overlay: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: 'center',
        alignItems: 'center'
    },
    modalContainer: {
        ...baseViewStyle,
        flex: 1,
        marginTop: metric.ratioHeight(100),
        marginBottom: metric.ratioHeight(15)
    },
    titleTextStyle: {
        flex: 0,
        color: color.palette.pearl,
        fontSize: RFValue(23),
        marginBottom: metric.ratioHeight(15)
    },
    listContainer: {
        ...baseViewStyle,
        flex: 1,
        width: width * 0.8,
        maxHeight: height * 0.7,
        borderRadius: 0,
        marginBottom: metric.ratioHeight(15),

        backgroundColor: color.palette.pearl,
    },
    listContentStyle: {
        backgroundColor: color.whiteDim
    },
    footerSingleSelectionContainerStyle: {
        height: metric.ratioHeight(55),
        justifyContent: 'center',
        borderTopColor: color.boder,
        borderTopWidth: 1,
        backgroundColor: color.whiteDim
    },
    footerMultipleSelectionContainerStyle: {
        height: metric.ratioHeight(55),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderTopColor: color.boder,
        borderTopWidth: 1,
        backgroundColor: color.whiteDim
    },
    clearContainer: {
        height: metric.ratioHeight(60),
        width: metric.ratioWidth(60 + 10),
        justifyContent: 'center',
    },
    cancelContainerSingleSelection: {
        justifyContent: 'center',
        alignSelf: 'center',
        flex: 1
    },
    cancelContainerMultipleSelection: {
        paddingLeft: metric.ratioWidth(14)
    },
    okContainerStyle: {
        width: metric.ratioWidth(72),
        height: metric.ratioHeight(36),
        backgroundColor: color.palette.pink,
        borderRadius: metric.ratioWidth(75),
        marginRight: metric.ratioWidth(15),
        justifyContent: 'center'
    },
    cancelButton: {
        flex: 1,
        justifyContent:'center',
    },
    okButton: {
        flex: 0,
    },
    cancelButtonText: {
        ...regularTextStyle,
        textAlign: 'center',
        fontSize: RFValue(15),
        color: color.palette.pink,
    },
    clearButtonText: {
        ...regularTextStyle,
        textAlign: 'left',
        fontSize: RFValue(13),
        color: color.palette.pink,
    },
    okButtonTextStyle: {
        ...regularTextStyle,
        textAlign: 'center',
        fontSize: RFValue(15),
        color: color.palette.white,
    },
    filterTextInputContainer: {
        borderBottomWidth: 1,
        borderBottomColor: color.boder,
        backgroundColor: color.whiteDim,
        flexDirection: 'row',
    },
    filterTextInput: {
        ...filterTextStyle,
        paddingVertical: metric.ratioHeight(10),
        paddingHorizontal: metric.ratioWidth(15),
        flex: 1,
        height: metric.ratioHeight(60),
        color: color.palette.darkGrey
    },
    categoryStyle: {
        ...optionStyle
    },
    categoryTextStyle: {
        ...optionTextStyle,
        color: 'red',
        fontStyle: 'italic',
        fontSize: 16
    },
    optionStyle: {
        ...optionStyle
    },
    optionStyleLastChild: {
        borderBottomWidth: 0
    },
    optionTextStyle: {
        ...optionTextStyle
    },
    selectedOptionStyle: {
        ...optionStyle
    },
    selectedOptionStyleLastChild: {
        borderBottomWidth: 0
    },
    selectedOptionTextStyle: {
        ...optionTextStyle,
        ...regularTextStyle,
        color: color.palette.pink,
    }
})