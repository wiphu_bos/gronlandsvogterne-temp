import * as React from 'react'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs'
import { NavigationKey } from '../../../constants/app.constant'
import * as metric from "../../../theme"
import { CustomTabBar } from '../custom-tab/custom-tab'
import { PrimaryInformationFirstScreen } from '../../../modules/tabs/pages/primary-infomation-page/page-1/pi-first-page.screen'
import { PrimaryInformationSecondScreen } from '../../../modules/tabs/pages/primary-infomation-page/page-2/pi-second-page.screen'
import { PrimaryInformationThirdScreen } from '../../../modules/tabs/pages/primary-infomation-page/page-3/pi-third-page.screen'
import { PrimaryInformationFourthScreen } from '../../../modules/tabs/pages/primary-infomation-page/page-4/pi-fourth-page.screen'
import { TabControllerProps } from '../tab.props'

const Tab = createMaterialTopTabNavigator()

export const CreateEditProfileTapComponents: React.FunctionComponent<TabControllerProps> = (mainProps) => {
    return (
        <Tab.Navigator tabBar={props => CustomTabBar(mainProps?.tabController, NavigationKey.CreateEditProfileTab, props)}
            {...metric.tabNavigationOptions}>
            <Tab.Screen name={NavigationKey.CreateEditProfileFirst} component={PrimaryInformationFirstScreen} />
            <Tab.Screen name={NavigationKey.CreateEditProfileSecond} component={PrimaryInformationSecondScreen} />
            <Tab.Screen name={NavigationKey.CreateEditProfileThird} component={PrimaryInformationThirdScreen} />
            <Tab.Screen name={NavigationKey.CreateEditProfileFourth} component={PrimaryInformationFourthScreen} />
        </Tab.Navigator>
    )
}