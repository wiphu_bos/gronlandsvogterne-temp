import * as React from 'react'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs'
import { NavigationKey } from '../../../constants/app.constant'
import * as metric from "../../../theme"
import { CustomTabBar } from '../custom-tab/custom-tab'
import { UserProfileDetailFirstScreen } from '../../../modules/tabs/pages/user-profile-detail-page/page-1/user-profile-detail-first-page.screen'
import { UserProfileDetailSecondScreen } from '../../../modules/tabs/pages/user-profile-detail-page/page-2/user-profile-detail-second-page.screen'
import { UserProfileDetailThirdScreen } from '../../../modules/tabs/pages/user-profile-detail-page/page-3/user-profile-detail-third-page.screen'
import { TabControllerProps } from '../tab.props'

const Tab = createMaterialTopTabNavigator()

export const UserProfileDetailTapComponents: React.FunctionComponent<TabControllerProps> = (mainProps) => {
    return (
        <Tab.Navigator tabBar={props => CustomTabBar(mainProps?.tabController, NavigationKey.UserProfileDetailTab, props)}
            {...metric.tabNavigationOptions}>
            <Tab.Screen name={NavigationKey.UserProfileDetailFirst} component={UserProfileDetailFirstScreen} />
            <Tab.Screen name={NavigationKey.UserProfileDetailSecond} component={UserProfileDetailSecondScreen} />
            <Tab.Screen name={NavigationKey.UserProfileDetailThird} component={UserProfileDetailThirdScreen} />
        </Tab.Navigator>
    )
}