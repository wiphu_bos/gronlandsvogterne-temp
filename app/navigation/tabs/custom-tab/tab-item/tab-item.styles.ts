import { ViewStyle } from "react-native";
import * as metric from "../../../../theme"

export const CONTAINNER: ViewStyle = {
    flex: 1
}

export const TAB_ITEM: ViewStyle = {
    opacity: 1,
    height: metric.ratioHeight(50),
    borderTopRightRadius: metric.ratioWidth(16),
    borderTopLeftRadius: metric.ratioWidth(16),
    justifyContent: 'center'
}

export const TINT_COLOR_FOCUSED: any = ['rgba(233,140,183,1)', 'rgba(228,143,187,1)']
export const TINT_COLOR_BLURRED: any = ['rgba(0,0,0,0)', 'rgba(0,0,0,0)']

