import * as React from 'react'
import { MaterialTopTabBarProps } from "@react-navigation/material-top-tabs"
import { View } from "react-native"
import * as Styles from "./custom-tab.styles"
import { TabItem } from "./tab-item/tab-item"
import { TabItemProps } from "./tab-item/tab-item.props"
import { TabController } from '../../../modules/tab.controller'

export const CustomTabBar = (tabController: TabController, tabBarKey: string, props: MaterialTopTabBarProps) => {
    tabController.setupChildProps(props)
    const { state } = props

    return (
        <View shouldRasterizeIOS style={Styles.TAB_BAR_CONTAINER}>
            {state.routes?.map((route: any, index: number) => {
                const isFocused = state.index === index
                const tabItemProps: TabItemProps = {
                    onPress: () => tabController.onPress(route, index),
                    onLongPress: () => tabController.onLongPress(route),
                    iconProps: Styles.getTabMenuIconProps(index, tabBarKey),
                    isFocused: isFocused
                }
                return <TabItem {...tabItemProps} key={index} />
            })}
        </View>
    )
}