import * as React from 'react'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs'
import ManageProfileController from '../../../modules/tabs/manage-profile-tab/manage-profile.controllers'
import * as metric from "../../../theme"
import { MyDudeProfileScreen } from '../../../modules/tabs/pages/my-dude-profile-page/my-dude-profile.screen'
import { MyProfileScreen } from '../../../modules/tabs/pages/my-profile-page/my-profile.screen'
import { CurveCustomTabBar } from '../curve-custom-tabs/curve-custom-tab'
import { TabControllerProps } from '../tab.props'

const Tab = createMaterialTopTabNavigator()

export const ManageProfileTapComponents = (mainProps:TabControllerProps) => {
    return (
        <Tab.Navigator tabBar={props => CurveCustomTabBar(mainProps.tabController, props)}
            {...metric.tabNavigationOptions}>
            <Tab.Screen name={ManageProfileController.resourcesViewModel?.getResourceMyProfileTabMenuTitle() || " "} {...ManageProfileController.resourcesViewModel?.getTestIDMyProfileTabMenuTitle()} component={MyProfileScreen} />
            <Tab.Screen name={ManageProfileController.resourcesViewModel?.getResourceMyDudeProfileTabMenuTitle() || " "} {...ManageProfileController.resourcesViewModel?.getTestIDDudeProfileTabMenuTitle()} component={MyDudeProfileScreen} />
        </Tab.Navigator>
    )
}