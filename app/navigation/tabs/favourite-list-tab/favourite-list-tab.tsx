import * as React from 'react'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs'
import { CurveCustomTabBar } from "../curve-custom-tabs/curve-custom-tab"
import FavouriteController from '../../../modules/tabs/favourite-list-tab/favourite-list.controllers'
import * as metric from "../../../theme"
import { FavouriteDudeListScreen } from '../../../modules/tabs/pages/favourite-dude-list-page/favourite-dude-list.screen'
import { FavouriteDudetteListScreen } from '../../../modules/tabs/pages/favourite-dudette-list-page/favourite-dudette-list.screen'
import { TabControllerProps } from '../tab.props'

const Tab = createMaterialTopTabNavigator()

export const FavouriteListTapComponents = (mainProps: TabControllerProps) => {
    return (
        <Tab.Navigator tabBar={props => CurveCustomTabBar(mainProps.tabController, props)}
            {...metric.tabNavigationOptions}>
            <Tab.Screen name={FavouriteController.resourcesViewModel?.getResourceFavouriteDudesTabMenuTitle() || " "} {...FavouriteController.resourcesViewModel?.getTestIDFavouriteDudesTabMenuTitle()} component={FavouriteDudeListScreen} />
            <Tab.Screen name={FavouriteController.resourcesViewModel?.getResourceFavouriteDudettesTabMenuTitle() || " "} {...FavouriteController.resourcesViewModel?.getTestIDFavouriteDudettesTabMenuTitle()} component={FavouriteDudetteListScreen} />
        </Tab.Navigator>
    )
}