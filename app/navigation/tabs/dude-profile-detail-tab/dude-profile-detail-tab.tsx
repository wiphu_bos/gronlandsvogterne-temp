import * as React from 'react'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs'
import { NavigationKey } from '../../../constants/app.constant'
import * as metric from "../../../theme"
import { DudeProfileDetailFirstScreen } from '../../../modules/tabs/pages/dude-profile-page/page-1/dude-profile-first-page.screen'
import { DudeProfileDetailSecondScreen } from '../../../modules/tabs/pages/dude-profile-page/page-2/dude-profile-second-page.screen'
import { DudeProfileDetailThridScreen } from '../../../modules/tabs/pages/dude-profile-page/page-3/dude-profile-third-page.screen'
import { DudeProfileDetailFourthScreenStackComponents } from '../../stacks/dude-profile-fourth-screen-stack'
import { TabControllerProps } from '../tab.props'
import { StackCustomTabBar } from '../stack-custom-tab/custom-tab'

const Tab = createMaterialTopTabNavigator()

export const DudeProfileDetailTapComponents: React.FunctionComponent<TabControllerProps> = (mainProps) => {
    return (
        <Tab.Navigator tabBar={props => StackCustomTabBar(mainProps?.tabController, NavigationKey.DudeProfileDetailTab, mainProps.numberOfTabsEnabled, props)}
            {...metric.tabNavigationOptions} {...metric.tabNavigationDisableSwipeOptions}>
            <Tab.Screen name={NavigationKey.DudeProfileDetailFirst} component={DudeProfileDetailFirstScreen} initialParams={{ disable: false }} />
            <Tab.Screen name={NavigationKey.DudeProfileDetailSecond} component={DudeProfileDetailSecondScreen} initialParams={{ disable: true }} />
            <Tab.Screen name={NavigationKey.DudeProfileDetailThird} component={DudeProfileDetailThridScreen} initialParams={{ disable: true }} />
            <Tab.Screen name={NavigationKey.DudeProfileDetailFourthStack} component={DudeProfileDetailFourthScreenStackComponents} initialParams={{ disable: true }} />
        </Tab.Navigator>
    )
}

