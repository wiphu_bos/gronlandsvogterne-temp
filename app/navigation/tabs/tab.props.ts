import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { TabController } from "../../modules/tab.controller";

export interface TabControllerProps extends NavigationContainerProps<ParamListBase> {
    tabController: TabController,
    numberOfTabsEnabled?: number,
}