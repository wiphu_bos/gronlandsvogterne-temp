import { ViewStyle, ImageStyle } from "react-native"
import { images } from '../../../theme/images'
import * as metric from "../../../theme"
import { TabItemIconProps } from "./tab-item/tab-item.props"
import { NavigationKey } from "../../../constants/app.constant"

export const TAB_BAR_CONTAINER: ViewStyle = {
    flexDirection: 'row'
}

const CENTER: ImageStyle = {
    alignSelf: 'center'
}

export const getTabMenuIconProps = (index: number, key: string): TabItemIconProps => {

    if (key === NavigationKey.CreateEditProfileTab) {
        switch (index) {
            case 0:
                return {
                    iconSource: images.userTabMenu,
                    iconStyle: {
                        width: metric.ratioWidth(19.25),
                        height: metric.ratioHeight(22),
                        ...CENTER
                    }
                }
            case 1:
                return {
                    iconSource: images.editTabMenu,
                    iconStyle: {
                        width: metric.ratioWidth(22),
                        height: metric.ratioHeight(22),
                        ...CENTER
                    }
                }
            case 2:
                return {
                    iconSource: images.imageTabMenu,
                    iconStyle: {
                        width: metric.ratioWidth(22),
                        height: metric.ratioHeight(16.5),
                        ...CENTER
                    }
                }
            case 3:
                return {
                    iconSource: images.starTabMenu,
                    iconStyle: {
                        width: metric.ratioWidth(22.99),
                        height: metric.ratioHeight(22),
                        ...CENTER
                    }
                }
            default:
                return null
        }
    } else if (key === NavigationKey.DudeProfileDetailTab) {
        switch (index) {
            case 0:
                return {
                    iconSource: images.userTagTabMenu,
                    iconStyle: {
                        width: metric.ratioWidth(27.5),
                        height: metric.ratioHeight(22),
                        ...CENTER
                    }
                }
            case 1:
                return {
                    iconSource: images.userInfoTabMenu,
                    iconStyle: {
                        width: metric.ratioWidth(16.5),
                        height: metric.ratioHeight(22),
                        ...CENTER
                    }
                }
            case 2:
                return {
                    iconSource: images.imageTabMenu,
                    iconStyle: {
                        width: metric.ratioWidth(22),
                        height: metric.ratioHeight(16.5),
                        ...CENTER
                    }
                }
            case 3:
                return {
                    iconSource: images.chatTabMenu,
                    iconStyle: {
                        width: metric.ratioWidth(22),
                        height: metric.ratioHeight(19.5),
                        ...CENTER
                    }
                }
            default:
                return null
        }
    } else if (key === NavigationKey.UserProfileDetailTab) {
        switch (index) {
            case 0:
                return {
                    iconSource: images.userTagTabMenu,
                    iconStyle: {
                        width: metric.ratioWidth(27.5),
                        height: metric.ratioHeight(22),
                        ...CENTER
                    }
                }
            case 1:
                return {
                    iconSource: images.userInfoTabMenu,
                    iconStyle: {
                        width: metric.ratioWidth(16.5),
                        height: metric.ratioHeight(22),
                        ...CENTER
                    }
                }
            case 2:
                return {
                    iconSource: images.imageTabMenu,
                    iconStyle: {
                        width: metric.ratioWidth(22),
                        height: metric.ratioHeight(16.5),
                        ...CENTER
                    }
                }
            default:
                return null
        }
    }
    return null
}