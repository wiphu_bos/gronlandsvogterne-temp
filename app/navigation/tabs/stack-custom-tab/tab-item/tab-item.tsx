import * as React from 'react'
import { TouchableOpacity } from "react-native-gesture-handler"
import { TabItemProps } from "./tab-item.props"
import LinearGradient from "react-native-linear-gradient"
import { Icon } from "../../../../components"
import * as Style from "./tab-item.styles"
import { View } from 'react-native'

export const TabItem = (props: TabItemProps) => {
    return (
        <View shouldRasterizeIOS style={Style.CONTAINNER}>
            <TouchableOpacity
                onPress={props?.onPress}
                onLongPress={props?.onLongPress}
                activeOpacity={1}
            >
                <LinearGradient shouldRasterizeIOS colors={props?.isFocused ? Style.TINT_COLOR_FOCUSED : props?.isEnable? Style.TINT_COLOR_BLURRED : Style.TINT_COLOR_DISABLED} style={Style.TAB_ITEM}>
                    <Icon source={props.iconProps?.iconSource} style={props?.iconProps?.iconStyle} />
                </LinearGradient>
            </TouchableOpacity>
        </View>)
}

