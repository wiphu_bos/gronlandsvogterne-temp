import { ViewStyle } from "react-native"
import * as metric from "../../../theme"

export const TAB_BAR_CONTAINER: ViewStyle = {
    flexDirection: 'row'
}

export const OVERLAPPING_VIEW: ViewStyle = {
    width: metric.ratioWidth(20),
    height: metric.ratioHeight(50),
    borderTopRightRadius: metric.ratioWidth(16),
    position: 'absolute',
    bottom: 0,
    right: 0,
}

export const TINT_COLOR_FOCUSED: any = ['rgba(233,140,183,1)', 'rgba(228,143,187,1)']