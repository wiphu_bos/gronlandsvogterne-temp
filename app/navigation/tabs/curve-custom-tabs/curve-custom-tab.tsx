import * as React from 'react'
import { View } from "react-native"
import * as Styles from "./curve-custom-tab.styles"
import { CurveCustomTabItem } from "./tab-item/tab-item"
import { TabItemProps } from "./tab-item/tab-item.props"
import LinearGradient from 'react-native-linear-gradient'
import { TabController } from '../../../modules/tab.controller'
import { MaterialTopTabBarProps } from '@react-navigation/material-top-tabs'

export const CurveCustomTabBar = (tabController: TabController, props: MaterialTopTabBarProps) => {
    tabController.setupChildProps(props)
    const { state, descriptors } = props
    return (
        <View shouldRasterizeIOS style={Styles.TAB_BAR_CONTAINER}>
            {state?.routes?.map((route: any, index: number) => {
                const isFocused = state.index === index
                const tabItemProps: TabItemProps = {
                    onPress: () => tabController.onPress(route, index),
                    onLongPress: () => tabController.onLongPress(route),
                    title: tabController.getTapTitle(descriptors, route.key, route),
                    isFocused: isFocused,
                    currentIndex: state.index,
                    textStyle: tabController.tapTitleStyle
                }
                return <CurveCustomTabItem {...tabItemProps} key={index} />
            })}
            {state.index === 1 ? <LinearGradient shouldRasterizeIOS style={Styles.OVERLAPPING_VIEW} colors={Styles.TINT_COLOR_FOCUSED} /> : null}
        </View>
    )
}