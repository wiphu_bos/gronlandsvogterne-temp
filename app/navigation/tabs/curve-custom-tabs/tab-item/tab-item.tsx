import * as React from 'react'
import { TouchableOpacity } from "react-native-gesture-handler"
import { TabItemProps } from "./tab-item.props"
import LinearGradient from "react-native-linear-gradient"
import { Text } from "../../../../components"
import * as Style from "./tab-item.styles"
import { View } from 'react-native'

export const CurveCustomTabItem = (props: TabItemProps) => {
    return (
        <View shouldRasterizeIOS style={Style.CONTAINNER}>
            <TouchableOpacity
                onPress={props?.onPress}
                onLongPress={props?.onLongPress}
                activeOpacity={1}
            >
                <LinearGradient shouldRasterizeIOS colors={props?.isFocused ? Style.TINT_COLOR_FOCUSED : Style.TINT_COLOR_BLURRED} style={Style.TAB_ITEM}>
                    <Text style={{ ...Style.TITLE, ...props?.textStyle }}>{props?.title}</Text>
                </LinearGradient>
            </TouchableOpacity>

            {
                props.currentIndex === 0 ?
                    <View shouldRasterizeIOS style={Style.CURVE_JOINT_FIRST_INDEX_CONTAINER}>
                        <LinearGradient shouldRasterizeIOS colors={Style.TINT_COLOR_FOCUSED}
                            style={Style.CURVE_JOINT_FIRST_INDEX_FOCUSED} />
                        <LinearGradient shouldRasterizeIOS colors={Style.TINT_COLOR_JOINT_BLURRED}
                            style={Style.CURVE_JOINT_FIRST_INDEX_BLURRED} />
                    </View>
                    : <View shouldRasterizeIOS style={Style.CURVE_JOINT_SECOND_INDEX_CONTAINER}>
                        <LinearGradient shouldRasterizeIOS colors={Style.TINT_COLOR_FOCUSED}
                            style={Style.CURVE_JOINT_SECOND_INDEX_FOCUSED} />
                        <LinearGradient shouldRasterizeIOS colors={Style.TINT_COLOR_JOINT_BLURRED}
                            style={Style.CURVE_JOINT_SECOND_INDEX_BLURRED} />
                    </View>
            }
        </View >)
}

