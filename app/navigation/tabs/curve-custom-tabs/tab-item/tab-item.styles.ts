import { ViewStyle, TextStyle } from "react-native";
import * as metric from "../../../../theme"
import { color } from "../../../../theme"
import { RFValue } from "react-native-responsive-fontsize";

export const CONTAINNER: ViewStyle = {
    flex: 1
}

export const TAB_ITEM: ViewStyle = {
    opacity: 1,
    height: metric.ratioHeight(50),
    borderTopRightRadius: metric.ratioWidth(16),
    borderTopLeftRadius: metric.ratioWidth(16),
    justifyContent: 'center'
}


const BASE_CURVE_JOINT: ViewStyle = {
    width: metric.ratioWidth(20),
    height: metric.ratioHeight(50),
    position: 'absolute',
    bottom: 0,
    right: 0,
}

export const CURVE_JOINT_FIRST_INDEX_CONTAINER: ViewStyle = {
    ...BASE_CURVE_JOINT
}
export const CURVE_JOINT_FIRST_INDEX_FOCUSED: ViewStyle = {
    ...BASE_CURVE_JOINT,
    marginRight: -metric.ratioWidth(20),
}

export const CURVE_JOINT_FIRST_INDEX_BLURRED: ViewStyle = {
    ...BASE_CURVE_JOINT,
    marginRight: -metric.ratioWidth(20),
    borderBottomLeftRadius: metric.ratioWidth(16)
}

export const CURVE_JOINT_SECOND_INDEX_CONTAINER: ViewStyle = {
    ...BASE_CURVE_JOINT
}
export const CURVE_JOINT_SECOND_INDEX_FOCUSED: ViewStyle = {
    ...BASE_CURVE_JOINT,
}

export const CURVE_JOINT_SECOND_INDEX_BLURRED: ViewStyle = {
    ...BASE_CURVE_JOINT,
    borderBottomRightRadius: metric.ratioWidth(16)
}


const TEXT: TextStyle = {
    color: color.palette.white,
    textAlign: "center",
    fontFamily: "Montserrat",
}
export const TITLE: TextStyle = {
    ...TEXT,
    fontSize: RFValue(18),
    fontWeight: '400',

    //Android
    fontFamily: "Montserrat-Regular",
}

export const TINT_COLOR_FOCUSED: any = ['rgba(233,140,183,1)', 'rgba(228,143,187,1)']
export const TINT_COLOR_BLURRED: any = ['rgba(0,0,0,0)', 'rgba(0,0,0,0)']
export const TINT_COLOR_JOINT_BLURRED: any = ['rgba(237,109,174,1)', 'rgba(230,114,176,1)']
