import { TextStyle } from "react-native";

export interface TabItemProps {
    onPress?: () => void
    onLongPress?: () => void
    isFocused?: boolean
    title?: string,
    currentIndex: number,
    textStyle?: TextStyle
}