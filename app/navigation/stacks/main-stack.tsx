import React from "react"
import { NavigationKey } from "../../constants/app.constant"
import { createStackNavigator } from "@react-navigation/stack"
import * as metric from "../../theme/metric"
import { DrawerComponents } from "../drawers/main-drawer"
import { CreateDudeScreen } from "../../modules/create-dude/create-dude.screen"
import { HomeScreen } from "../../modules/home/home.screen"
import { EmailSendSuccesScreen } from "../../modules/email-send-success/email-send-success.screen"
import { CreateEditProfileScreen } from "../../modules/tabs/create-edit-profile-tab/create-edit-profile.screen"
import { NotificationListScreen } from "../../modules/notification-list/notification-list.screen"
import { WaitingDudePublishProfileScreen } from "../../modules/waiting-dude-approve-publish-profile/waiting-dude-approve-publish-profile.screen"
import { ManageProfileScreen } from "../../modules/tabs/manage-profile-tab/manage-profile.screen"
import { FilterScreen } from "../../modules/filter/filter.screen"
import { DudeProfileDetailScreen } from "../../modules/tabs/dude-profile-detail-tab/dude-profile-detail.screen"
import { DudetteProfileDetailScreen } from "../../modules/dudette-detail/dudette-detail.screen"
import { UserProfileDetailScreen } from "../../modules/tabs/user-profile-detail-tab/user-profile-detail.screen"
import { ChatRoomScreen } from "../../modules/chat-room/chat-room.screen"
import { SubscriptionConfirmationScreen } from "../../shared-modules/subscription-confirmation/subscription-confirmation.screen"
// import { ChatListComponents } from "../../modules/chat-list/chat-list.screen"
import { FeedNavigator } from "../../modules/feed/feed.navigator"
import { IndberetningNavigator } from "../../modules/indberetning/indberetning.navigator"
import { SOSNavigator } from "../../modules/sos/sos.navigator"
import { MenuNavigator } from "../../modules/menu/menu.navigator"

const disableGesture = {
    gestureEnabled: false
}

const MainStack = createStackNavigator()

export const MainStackComponents = () => {

    console.log('jwb : MainStackComponents')

    return (<MainStack.Navigator
        mode="modal"
        screenOptions={metric.baseStackNavigaitonOptions}
        headerMode="none"
        initialRouteName={NavigationKey.MainStack}
    >
        <MainStack.Screen name={NavigationKey.MainStack} component={DrawerComponents} options={metric.firstStackNavigaitonOptions} />

        {/**  Home Stack */}
        <MainStack.Screen name={NavigationKey.Home} component={HomeScreen} options={disableGesture} />
        <MainStack.Screen name={NavigationKey.CreateDude} component={CreateDudeScreen} />
        <MainStack.Screen name={NavigationKey.Feed} component={FeedNavigator} />
        <MainStack.Screen name={NavigationKey.Indberetning} component={IndberetningNavigator} />

        <MainStack.Screen name={NavigationKey.SOS} component={SOSNavigator} />
        <MainStack.Screen name={NavigationKey.Menu} component={MenuNavigator} />

        <MainStack.Screen name={NavigationKey.EmailSendSuccess} component={EmailSendSuccesScreen} />
        <MainStack.Screen name={NavigationKey.CreateEditProfileTab} component={CreateEditProfileScreen} />
        <MainStack.Screen name={NavigationKey.NotificationList} component={NotificationListScreen} />
        <MainStack.Screen name={NavigationKey.WaitingDudePublishProfile} component={WaitingDudePublishProfileScreen} />

        {/**  Manage Profile Stack */}
        <MainStack.Screen name={NavigationKey.ManageProfileTab} component={ManageProfileScreen} options={disableGesture} />


        {/**  SearchList Stack */}
        <MainStack.Screen name={NavigationKey.Filters} component={FilterScreen} options={metric.modalOverFullScreenOptions} />
        <MainStack.Screen name={NavigationKey.DudeProfileDetailTab} component={DudeProfileDetailScreen} />
        <MainStack.Screen name={NavigationKey.DudetteProfileDetails} component={DudetteProfileDetailScreen} />

        {/**  Update Profile Stack */}
        <MainStack.Screen name={NavigationKey.UserProfileDetailTab} component={UserProfileDetailScreen} options={disableGesture} />

        <MainStack.Screen name={NavigationKey.ChatRoom} component={ChatRoomScreen} />

        <MainStack.Screen name={NavigationKey.SubscriptionConfirmation} component={SubscriptionConfirmationScreen} options={{ ...metric.modalOverFullScreenOptions, ...metric.clearBackgroundOption }} />

    </MainStack.Navigator>)
}