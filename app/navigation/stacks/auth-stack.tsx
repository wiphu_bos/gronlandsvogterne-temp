import React from "react"
import { NavigationKey } from "../../constants/app.constant"
import { createStackNavigator } from "@react-navigation/stack"
import { IntroductionScreen } from "../../modules/introduction/introduction.screen"
import { CreateAccountScreen } from "../../modules/create-account/create-account.screen"
import { LoginScreen } from "../../modules/login/login.screen"
import { WelcomeScreen } from "../../modules/welcome/welcome.screen"
import { TermsAndConditionsScreen } from "../../modules/terms-conditions/terms-conditions.screen"
import { VerifyUserScreen } from "../../modules/verify-user/verify-user.screen"
import { ForgotPasswordScreen } from "../../modules/forgot-password/forgot-password.screen"
import * as metric from "../../theme/metric"

const disableGesture = {
    gestureEnabled: false
}
export const AuthStack = createStackNavigator()

export const AuthStackComponents = () => {
    return (<AuthStack.Navigator initialRouteName={NavigationKey.Welcome}
        screenOptions={metric.baseStackNavigaitonOptions}
        headerMode="none" >
        <AuthStack.Screen name={NavigationKey.Welcome} component={WelcomeScreen} options={metric.firstStackNavigaitonOptions} />
        <AuthStack.Screen name={NavigationKey.Introduction} component={IntroductionScreen} options={disableGesture} />
        <AuthStack.Screen name={NavigationKey.CreateAccount} component={CreateAccountScreen} options={disableGesture} />
        <AuthStack.Screen name={NavigationKey.TermsAndConditions} component={TermsAndConditionsScreen} />
        <AuthStack.Screen name={NavigationKey.Login} component={LoginScreen} options={disableGesture} />
        <AuthStack.Screen name={NavigationKey.VerifyUser} component={VerifyUserScreen} options={disableGesture} />
        <AuthStack.Screen name={NavigationKey.ForgotPassword} component={ForgotPasswordScreen} options={disableGesture} />
    </AuthStack.Navigator>)
}
