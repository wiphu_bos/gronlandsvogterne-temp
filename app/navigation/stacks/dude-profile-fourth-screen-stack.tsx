import React from "react"
import { NavigationKey } from "../../constants/app.constant"
import { createStackNavigator } from "@react-navigation/stack"
import * as metric from "../../theme/metric"
import { EmailSendSuccesScreen } from "../../modules/email-send-success/email-send-success.screen"
import { DudeProfileDetailFourthScreen } from "../../modules/tabs/pages/dude-profile-page/page-4/dude-profile-fourth-page.screen"
import { DudeProfileDetailSendEmailScreen } from "../../modules/tabs/pages/dude-profile-page/page-send-email/dude-profile-send-email-page.screen"

const disableGesture = {
    gestureEnabled: false
}
const DudeProfileDetailFourthScreenStack = createStackNavigator()

export const DudeProfileDetailFourthScreenStackComponents = () => {
    return (<DudeProfileDetailFourthScreenStack.Navigator
        screenOptions={metric.baseStackNavigaitonOptions}
        headerMode="none"
        initialRouteName={NavigationKey.ManageProfileTab}
    >
        <DudeProfileDetailFourthScreenStack.Screen name={NavigationKey.DudeProfileDetailFourth} component={DudeProfileDetailFourthScreen} options={disableGesture} />
        <DudeProfileDetailFourthScreenStack.Screen name={NavigationKey.DudeProfileDetailSendEmail} component={DudeProfileDetailSendEmailScreen} options={disableGesture} />
        <DudeProfileDetailFourthScreenStack.Screen name={NavigationKey.EmailSendSuccess} component={EmailSendSuccesScreen} />
    </DudeProfileDetailFourthScreenStack.Navigator>)
}