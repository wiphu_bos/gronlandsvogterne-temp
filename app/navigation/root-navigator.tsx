import React from "react"
import { NavigationKey } from "../constants/app.constant"
import { Splashscreen } from "../modules/splash/splash.screen"
import { useStores } from "../models/root-store"
import { AuthStackComponents } from "./stacks/auth-stack"
import { createStackNavigator } from "@react-navigation/stack"
import { useDynamicLinks, useInitialDynamicLinks } from "../utils/firebase/dynamic-links/use-dynamic-links"
import { observer } from "mobx-react-lite"
import { MainStackComponents } from "./stacks/main-stack"

export const IntroStack = createStackNavigator()

export const RootNavigator = observer((props) => {

  const rootStore = useStores()
  const { getIsFetchingResources } = rootStore
  const { getLoggedIn } = rootStore.getAuthStore

  // MARK: Dynamic Links

  console.log('jwb : RootNavigator')

  !getIsFetchingResources && useInitialDynamicLinks(rootStore)
  useDynamicLinks(rootStore)

  return getIsFetchingResources ?
    <IntroStack.Navigator headerMode="none" >
      <IntroStack.Screen name={NavigationKey.Splash} component={Splashscreen} />
    </IntroStack.Navigator> :
    !getLoggedIn ?
      <AuthStackComponents /> : <MainStackComponents />

})