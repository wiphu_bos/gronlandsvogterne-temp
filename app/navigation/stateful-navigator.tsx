import React, { useRef } from "react"
// @ts-ignore: until they update @type/react-navigation
import { useStores } from "../models/root-store"
import { RootNavigator } from "./root-navigator"
import { NavigationContainer } from '@react-navigation/native'
import { NetworkAwarenessComponent } from "../components/network-awareness/network-awareness"
import { observer } from "mobx-react-lite"
import { useNetworkAwareness } from "../custom-hooks/use-network-awareness"
import { useAuthentication } from "../utils/firebase/authentication"
import { LoadingIndicator } from "../components/loading-indicator"

export const StatefulNavigator: React.FunctionComponent<{}> = observer(() => {

  // MARK: Global Variables

  const rootStore = useStores()
  const stateRef: any = useRef()
  const navigationRef = useRef()

  useAuthentication(rootStore)
  useNetworkAwareness(rootStore)

  return (
    <NavigationContainer
      ref={navigationRef}
      onStateChange={state => {
        const previousState = stateRef.current
        const currentState = state
        rootStore.getNavigationStore?.setPreviousState(previousState)
        rootStore.getNavigationStore?.setCurrentState(state)
        if (previousState !== currentState) {
          // The line below uses the @react-native-firebase/analytics tracker
          // Change this line to use another Mobile analytics SDK
        }
        stateRef.current = currentState
      }}
    >
      <LoadingIndicator />
      <NetworkAwarenessComponent />
      <RootNavigator />
    </NavigationContainer>
  )
})
