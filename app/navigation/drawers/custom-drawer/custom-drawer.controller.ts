// MARK: Import
import { NavigationKey, SearchListType } from "../../../constants/app.constant"
import { CustomDrawerResourcesStoreModel, CustomDrawerStoreModel } from "./storemodels/custom-drawer.store"
import { CustomDrawerResourcesStore, CustomDrawerStore } from "./storemodels/custom-drawer.types"
import { RootStore, INavigationRoute } from "../../../models"
import FireBaseServices from "../../../utils/firebase/authentication/auth"
import { DrawerActions, StackActions } from "@react-navigation/native"
import { CustomDrawerContentProps } from "./custom-drawer.props"
import { HomeStore } from "../../../modules/home/storemodels/home.types"
import { HomeStoreModel } from "../../../modules/home/storemodels/home.store"
import { GeneralResources } from "../../../constants/firebase/remote-config"
import { FilterStoreModel } from "../../../models/filter-store"
import NotificationServices from "../../../utils/firebase/notification/notification"
import { BaseController } from "../../../modules/base.controller"
import { onSnapshot } from "mobx-state-tree"
import { FAQ_URL, CONTACT_URL } from "../../../constants/web-url/web-url.constant"

class CustomDrawerController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static resourcesViewModel: CustomDrawerResourcesStore
    public static viewModel: CustomDrawerStore
    public static homeViewModel: HomeStore

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, props?: CustomDrawerContentProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, props, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(props)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        CustomDrawerController.resourcesViewModel = CustomDrawerResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const data = this._rootStore?.getModuleStore(NavigationKey.Home)
        if (data) CustomDrawerController.homeViewModel = HomeStoreModel.create({ ...data })
        CustomDrawerController.viewModel = CustomDrawerStoreModel.create({})
    }

    private _setupSnapShot = () => {
        onSnapshot(CustomDrawerController.viewModel, (snap: CustomDrawerStore) => {
            this._setInitializedToPropsParams()
        })
    }

    /*
       Mark Data
   */

    /*
       Mark Event
   */
    public onSignOutPressed = async () => {
        this.closeDrawer()
        this._isGlobalLoading(true)
        await CustomDrawerController.viewModel?.signOut(this._rootStore)
        await NotificationServices.deleteToken()
        await FireBaseServices.signOut()
        this._isGlobalLoading(false)
        this._rootStore?.getUserStore?.forceLogoutWithoutAuth(this._rootStore)
    }


    public onDrawerItemPressed = (identifier: any) => {

        console.log("jwb onDrawerItemPressed")
        console.log(identifier)

        let actions: any[]
        switch (identifier) {

            case NavigationKey.Feed:
                    actions = [DrawerActions.jumpTo(NavigationKey.Home), StackActions.push(identifier)]
                    break
            case NavigationKey.CreateDude:
                actions = [DrawerActions.jumpTo(NavigationKey.Home), StackActions.push(identifier)]
                break
            case NavigationKey.Indberetning:
                actions = [DrawerActions.jumpTo(NavigationKey.Home), StackActions.push(identifier)]
                break
            case NavigationKey.SOS:
                actions = [DrawerActions.jumpTo(NavigationKey.Home), StackActions.push(identifier)]
                break
            case NavigationKey.Menu:
                actions = [DrawerActions.jumpTo(NavigationKey.Home), StackActions.push(identifier)]
                break
            case NavigationKey.ManageProfileTab:
                actions = [DrawerActions.jumpTo(identifier, {
                    screen: this._rootStore?.getGeneralResourcesStore(identifier, GeneralResources.ManageProfileTabScreen.myProfileTabMenuTitle),
                })]
            break
            case SearchListType.Dude:
            case SearchListType.Dudette:
                this._rootStore?.setFilterStore(FilterStoreModel.create({}), this._rootStore)
                actions = [DrawerActions.jumpTo(NavigationKey.SearchListScreen, {
                    type: identifier === SearchListType.Dude ? SearchListType.Dude : SearchListType.Dudette,
                    isShowFilter: true,
                    isFetchData: true
                })]
                break
            case NavigationKey.FavouriteListTab:
                actions = [DrawerActions.jumpTo(identifier, {
                    screen: identifier
                })]
                break
            case NavigationKey.ChatList:
                actions = [DrawerActions.jumpTo(identifier, {
                    screen: identifier
                })]
                break
            case NavigationKey.FAQ:
                actions = [DrawerActions.jumpTo(identifier, {
                    screen: identifier,
                    title: CustomDrawerController.resourcesViewModel?.getResourceFAQSideMenuBarTitle(),
                    uri: FAQ_URL
                })]
                break
            case NavigationKey.Contact:
                actions = [DrawerActions.jumpTo(identifier, {
                    screen: identifier,
                    title: CustomDrawerController.resourcesViewModel?.getResourceContactSideMenuBarTitle(),
                    uri: CONTACT_URL
                })]
                break
            default:
                actions = [DrawerActions.jumpTo(identifier)]
                break
        }
        Promise.all(actions.map(e => this._myProps?.navigation?.dispatch(e)))
        this.closeDrawer()
    }

    public closeDrawer = () => this._myProps?.navigation?.dispatch(DrawerActions.closeDrawer() as any)

    /*
       Mark Life cycle
   */

}

export default CustomDrawerController