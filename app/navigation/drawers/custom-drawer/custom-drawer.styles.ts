import { ViewStyle } from "react-native"
import { color } from "../../../theme/color"
import * as metric from "../../../theme"

export const CONTAINER_VIEW: ViewStyle = {
    backgroundColor: color.palette.white,
    width: metric.ratioWidth(321),
    height: '100%',
    borderTopRightRadius: metric.ratioWidth(40),
    marginTop: metric.ratioHeight(10),
}
export const GRADIENT_VIEW: ViewStyle = {
    width: metric.ratioWidth(240),
    height: '100%',
    position:'absolute'
}

export const SCROLL_VIEW: ViewStyle = {
    marginTop: metric.ratioHeight(48)
}

export const CLOSE_VIEW: ViewStyle = {
    backgroundColor: color.palette.pinkRed,
    width: metric.ratioWidth(67),
    height: metric.ratioWidth(67),
    borderRadius: metric.ratioWidth(67 / 2),
    position:'absolute',
    justifyContent:'center',
    right:0,
    top:0
}