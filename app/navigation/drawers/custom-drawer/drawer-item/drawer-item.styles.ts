import { ViewStyle, ImageStyle, TextStyle } from "react-native"
import * as metric from "../../../../theme"
import { color } from "../../../../theme"
import { RFValue } from "react-native-responsive-fontsize"

export const CONTAINNER_VIEW: ViewStyle = {
    flexDirection: 'row',
    width: metric.ratioWidth(332),
    height: metric.ratioHeight(65),
    paddingLeft: metric.ratioWidth(22),
    alignItems: 'center'
}

export const ICON_VIEW: ImageStyle = {
    width: metric.ratioWidth(18),
    height: metric.ratioWidth(18),
    resizeMode:'contain'
}

export const TEXT: TextStyle = {
    color: color.palette.pinkRed,
    fontFamily: "Montserrat",
}
export const TITLE: TextStyle = {
    ...TEXT,
    fontSize: RFValue(13),
    fontWeight: '400',

    //Android
    fontFamily: "Montserrat-Regular",
}
export const TEXT_VIEW: TextStyle = {
    ...TITLE,
    marginLeft: metric.ratioWidth(20)
}

export const SEPARATOR_VIEW: ViewStyle = {
    position: 'absolute',
    left: 0,
    bottom: 0,
    right: 0,
    height: metric.ratioHeight(1),
    backgroundColor: color.palette.white
}

export const BADGE_TITLE: TextStyle = {
    ...TEXT,
    fontSize: RFValue(11),
    fontWeight: '300',

    //Android
    fontFamily: "Montserrat-Light",
    color: color.palette.white
}

export const BADGE_VIEW: TextStyle = {
    marginLeft: metric.ratioWidth(10)
}