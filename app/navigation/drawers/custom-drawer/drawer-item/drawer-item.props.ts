export interface DrawerItemProps {
    iconSource: string
    text?: string
    onPress?: () => void
    badgeNumber?:number | null
}