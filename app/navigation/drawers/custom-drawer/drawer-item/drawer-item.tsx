import React from "react"
import { observer } from "mobx-react-lite"
import * as Styles from "./drawer-item.styles"
import { Icon, Text, Button } from "../../../../components"
import LinearGradient from "react-native-linear-gradient"
import { color } from "../../../../theme"
import { DrawerItemProps } from "./drawer-item.props"
import * as Uitls from "../../../../utils"


export const DrawerItemCompoenents: React.FunctionComponent<DrawerItemProps> = observer((props) => {
    return (
        <Button isAnimated={false} isSolid preset='none' style={Styles.CONTAINNER_VIEW} onPress={props.onPress}>
            <Icon source={props.iconSource} style={Styles.ICON_VIEW} />
            <Text text={props.text} style={Styles.TEXT_VIEW} />
            {props.badgeNumber ? <Button text={Uitls.getBadgeNumber(props.badgeNumber)} preset="sideMenuBarBadge"  /> : null}
            <LinearGradient shouldRasterizeIOS style={Styles.SEPARATOR_VIEW} colors={color.whiteClearGradient} />
        </Button>
    )
})
