import { types } from "mobx-state-tree"
import { CustomDrawerPropsModel } from "./custom-drawer.models"
import { GeneralResources } from "../../../../constants/firebase/remote-config"
import { NavigationKey } from "../../../../constants/app.constant"
import { TestIDResources } from "../../../../constants/test-key/test.constant"
import * as Utils from "../../../../utils"
import { GeneralResourcesStoreModel } from "../../../../models/general-resources-store"

export const CustomDrawerViews = types.model(CustomDrawerPropsModel)
    .views(self => ({
        get getExample() {
            return
        }
    }))

export const CustomDrawerResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State

    const { SideMenuBar } = GeneralResources
    const { homeSideMenuBarTitle,manageProfileSideMenuBarTitle,
        searchDudeSideMenuBarTitle,
        createDudeSideMenuBarTitle,
        activitySideMenuBarTitle,
        feedSideMenuBarTitle,
        indberetningSideMenuBarTitle,
        sosSideMenuBarTitle,
        menuSideMenuBarTitle,
        helpDudetteSideMenuBarTitle,
        favouriteDudeSideMenuBarTitle,
        inboxSideMenuBarTitle,
        contactSideMenuBarTitle,
        faqSideMenuBarTitle,
        logoutSideMenuBarTitle
    } = SideMenuBar

    //MARK: Views
    
    const getResources = (key: string, childKeyOrShareKey: string | boolean = false, isChildDotNotation: boolean = false) => self.getValues(childKeyOrShareKey ? key : NavigationKey.SideMenuBar, childKeyOrShareKey ? true : key, isChildDotNotation)
    const getResourceHomeSideMenuBarTitle = () => getResources(homeSideMenuBarTitle)
    const getResourceManageProfileSideMenuBarTitle = () => getResources(manageProfileSideMenuBarTitle)
    const getResourceSearchDudeSideMenuBarTitle = () => getResources(searchDudeSideMenuBarTitle)
    const getResourceCreateDudeSideMenuBarTitle = () => getResources(createDudeSideMenuBarTitle)
    const getResourceActivitySideMenuBarTitle = () => getResources(activitySideMenuBarTitle)
    const getResourceFeedSideMenuBarTitle = () => getResources(feedSideMenuBarTitle)
    const getResourceSOSSideMenuBarTitle = () => getResources(sosSideMenuBarTitle)
    const getResourceMenuSideMenuBarTitle = () => getResources(menuSideMenuBarTitle)
    const getResourceIndberetnigSideMenuBarTitle = () => getResources(indberetningSideMenuBarTitle)

    const getResourceHelpDudetteSideMenuBarTitle = () => getResources(helpDudetteSideMenuBarTitle)
    const getResourceFavDudeSideMenuBarTitle = () => getResources(favouriteDudeSideMenuBarTitle)
    const getResourceInboxSideMenuBarTitle = () => getResources(inboxSideMenuBarTitle)
    const getResourceContactSideMenuBarTitle = () => getResources(contactSideMenuBarTitle)
    const getResourceFAQSideMenuBarTitle = () => getResources(faqSideMenuBarTitle)
    const getResourceLogoutSideMenuBarTitle = () => getResources(logoutSideMenuBarTitle)


    return {
        getResourceHomeSideMenuBarTitle,
        getResourceManageProfileSideMenuBarTitle,
        getResourceSearchDudeSideMenuBarTitle,
        getResourceCreateDudeSideMenuBarTitle,
        getResourceActivitySideMenuBarTitle,
        getResourceFeedSideMenuBarTitle,
        getResourceSOSSideMenuBarTitle,
        getResourceIndberetnigSideMenuBarTitle,
        getResourceMenuSideMenuBarTitle,
        getResourceHelpDudetteSideMenuBarTitle,
        getResourceFavDudeSideMenuBarTitle,
        getResourceInboxSideMenuBarTitle,
        getResourceContactSideMenuBarTitle,
        getResourceFAQSideMenuBarTitle,
        getResourceLogoutSideMenuBarTitle
    }
})
    .views(self => {

        //MARK: Volatile State

        //MARK: Views
        const getTestIDHomeSideMenuBarTitle = () => Utils.getTestIDObject(TestIDResources.SideMenuBar.homeSideMenuBar)
        const getTestIDManageProfileSideMenuBarTitle = () => Utils.getTestIDObject(TestIDResources.SideMenuBar.manageProfileSideMenuBar)
        const getTestIDSearchDudeSideMenuBarTitle = () => Utils.getTestIDObject(TestIDResources.SideMenuBar.searchDudeSideMenuBar)
        const getTestIDCreateDudeSideMenuBarTitle = () => Utils.getTestIDObject(TestIDResources.SideMenuBar.createDudeSideMenuBar)
        const getTestIDActivitySideMenuBarTitle = () => Utils.getTestIDObject(TestIDResources.SideMenuBar.activitySideMenuBar)
        const getTestIDFeedSideMenuBarTitle = () => Utils.getTestIDObject(TestIDResources.SideMenuBar.feedSideMenuBar)
        const getTestIDIndberetnigSideMenuBarTitle = () => Utils.getTestIDObject(TestIDResources.SideMenuBar.inboxSideMenuBar)
        const getTestIDSOSSideMenuBarTitle = () => Utils.getTestIDObject(TestIDResources.SideMenuBar.sosSideMenuBar)
        const getTestIDMenuSideMenuBarTitle = () => Utils.getTestIDObject(TestIDResources.SideMenuBar.menuSideMenuBar)
        const getTestIDHelpDudetteSideMenuBarTitle = () => Utils.getTestIDObject(TestIDResources.SideMenuBar.createDudeSideMenuBar)
        const getTestIDFavDudeSideMenuBarTitle = () => Utils.getTestIDObject(TestIDResources.SideMenuBar.createDudeSideMenuBar)
        const getTestIDInboxSideMenuBarTitle = () => Utils.getTestIDObject(TestIDResources.SideMenuBar.createDudeSideMenuBar)
        const getTestIDContactSideMenuBarTitle = () => Utils.getTestIDObject(TestIDResources.SideMenuBar.createDudeSideMenuBar)
        const getTestIDFAQSideMenuBarTitle = () => Utils.getTestIDObject(TestIDResources.SideMenuBar.createDudeSideMenuBar)
        const getTestIDLogoutSideMenuBarTitle = () => Utils.getTestIDObject(TestIDResources.SideMenuBar.createDudeSideMenuBar)
        const getTestIDButtonCloseSideMenuBarTitle = () => Utils.getTestIDObject(TestIDResources.SideMenuBar.buttonCloseSideMenuBar)
        return {
            getTestIDHomeSideMenuBarTitle,
            getTestIDManageProfileSideMenuBarTitle,
            getTestIDSearchDudeSideMenuBarTitle,
            getTestIDCreateDudeSideMenuBarTitle,
            getTestIDActivitySideMenuBarTitle,
            getTestIDFeedSideMenuBarTitle,
            getTestIDSOSSideMenuBarTitle,
            getTestIDMenuSideMenuBarTitle,
            getTestIDIndberetnigSideMenuBarTitle,
            getTestIDHelpDudetteSideMenuBarTitle,
            getTestIDFavDudeSideMenuBarTitle,
            getTestIDInboxSideMenuBarTitle,
            getTestIDContactSideMenuBarTitle,
            getTestIDFAQSideMenuBarTitle,
            getTestIDLogoutSideMenuBarTitle,
            getTestIDButtonCloseSideMenuBarTitle
        }
    })
