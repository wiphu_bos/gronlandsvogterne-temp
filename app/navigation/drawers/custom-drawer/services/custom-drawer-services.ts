import { types, flow } from "mobx-state-tree"
import { RootStore } from "../../../../models"
import { CustomDrawerPropsModel } from "../viewmodels/custom-drawer.models"

export const CustomDrawerServiceActions = types.model(CustomDrawerPropsModel).actions(self => {
    const signOut = flow(function* (rootStore: RootStore) {
        try {
            yield rootStore?.getUserStore?.signOut()
        } catch (e) {
            return (e)
        }
    })
    return { signOut }
})