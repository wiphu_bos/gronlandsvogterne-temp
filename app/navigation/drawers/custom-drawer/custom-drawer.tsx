import React from "react"
import { View } from "react-native-animatable"
import { observer } from "mobx-react-lite"
import * as Styles from "./custom-drawer.styles"
import { Button, AnimatedScrollView } from "../../../components"
import LinearGradient from "react-native-linear-gradient"
import { color } from "../../../theme/color"
import * as metric from "../../../theme"
import { DrawerItemCompoenents } from "./drawer-item/drawer-item"
import { images } from "../../../theme/images"
import { useConfigurate } from "../../../custom-hooks/use-configure-controller"
import CustomDrawerController from "./custom-drawer.controller"
import { CustomDrawerContentProps } from "./custom-drawer.props"
import { NavigationKey, SearchListType } from "../../../constants/app.constant"

export const CustomDrawerContent: React.FunctionComponent<CustomDrawerContentProps> = observer((props) => {

    const controller = useConfigurate(CustomDrawerController, props) as CustomDrawerController

    return (
        <View>
            <View shouldRasterizeIOS style={Styles.CONTAINER_VIEW}>
                <LinearGradient shouldRasterizeIOS style={Styles.GRADIENT_VIEW}
                    colors={color.blueWhiteGradient}
                    start={{ x: -2, y: 1 }}
                    end={{ x: 1, y: 1 }} />
                <AnimatedScrollView
                    {...metric.solidScrollView}
                    contentContainerStyle={Styles.SCROLL_VIEW}>
                    <DrawerItemCompoenents text={CustomDrawerController.resourcesViewModel?.getResourceHomeSideMenuBarTitle()}
                        {...CustomDrawerController.resourcesViewModel?.getTestIDHomeSideMenuBarTitle()}
                        onPress={() => controller.onDrawerItemPressed(NavigationKey.Home)} iconSource={images.home} />
                    <DrawerItemCompoenents text={CustomDrawerController.resourcesViewModel?.getResourceManageProfileSideMenuBarTitle()}
                        {...CustomDrawerController.resourcesViewModel?.getTestIDManageProfileSideMenuBarTitle()}
                        onPress={() => controller.onDrawerItemPressed(NavigationKey.ManageProfileTab)} iconSource={images.manageProfile} />
                    
                    <DrawerItemCompoenents text={CustomDrawerController.resourcesViewModel?.getResourceCreateDudeSideMenuBarTitle()}
                        {...CustomDrawerController.resourcesViewModel?.getTestIDCreateDudeSideMenuBarTitle()}
                        onPress={() => controller.onDrawerItemPressed(NavigationKey.CreateDude)} iconSource={images.upload} />


                    <DrawerItemCompoenents text='Feed'
                    {...CustomDrawerController.resourcesViewModel?.getTestIDFeedSideMenuBarTitle}
                        onPress={() => controller.onDrawerItemPressed(NavigationKey.Feed)} iconSource={images.upload} />


                    <DrawerItemCompoenents text='Indberetning'
                    {...CustomDrawerController.resourcesViewModel?.getTestIDFeedSideMenuBarTitle}
                        onPress={() => controller.onDrawerItemPressed(NavigationKey.Indberetning)} iconSource={images.upload} />
                        
                    <DrawerItemCompoenents text='SOS'
                    {...CustomDrawerController.resourcesViewModel?.getTestIDSOSSideMenuBarTitle}
                        onPress={() => controller.onDrawerItemPressed(NavigationKey.SOS)} iconSource={images.upload} />

                    <DrawerItemCompoenents text='Menu'
                    {...CustomDrawerController.resourcesViewModel?.getTestIDMenuSideMenuBarTitle}
                        onPress={() => controller.onDrawerItemPressed(NavigationKey.Menu)} iconSource={images.upload} />

                    <DrawerItemCompoenents text={CustomDrawerController.resourcesViewModel?.getResourceSearchDudeSideMenuBarTitle()}
                        {...CustomDrawerController.resourcesViewModel?.getTestIDSearchDudeSideMenuBarTitle()}
                        onPress={() => controller.onDrawerItemPressed(SearchListType.Dude)} iconSource={images.search} />
                    <DrawerItemCompoenents text={CustomDrawerController.resourcesViewModel?.getResourceHelpDudetteSideMenuBarTitle()}
                        {...CustomDrawerController.resourcesViewModel?.getTestIDHelpDudetteSideMenuBarTitle()}
                        onPress={() => controller.onDrawerItemPressed(SearchListType.Dudette)} iconSource={images.heart} />
                    <DrawerItemCompoenents text={CustomDrawerController.resourcesViewModel?.getResourceFavDudeSideMenuBarTitle()}
                        {...CustomDrawerController.resourcesViewModel?.getTestIDFavDudeSideMenuBarTitle()}
                        onPress={() => controller.onDrawerItemPressed(NavigationKey.FavouriteListTab)} iconSource={images.favourite} />
                    <DrawerItemCompoenents text={CustomDrawerController.resourcesViewModel?.getResourceInboxSideMenuBarTitle()}
                        {...CustomDrawerController.resourcesViewModel?.getTestIDInboxSideMenuBarTitle()}
                        onPress={() => controller.onDrawerItemPressed(NavigationKey.ChatList)}
                        iconSource={images.envelope} badgeNumber={CustomDrawerController.rootStore?.getUserStore?.getUnReadChatCount} />
                    <DrawerItemCompoenents text={CustomDrawerController.resourcesViewModel?.getResourceFAQSideMenuBarTitle()}
                        {...CustomDrawerController.resourcesViewModel?.getTestIDFAQSideMenuBarTitle()}
                        onPress={() => controller.onDrawerItemPressed(NavigationKey.FAQ)}
                        iconSource={images.info} />
                    <DrawerItemCompoenents text={CustomDrawerController.resourcesViewModel?.getResourceContactSideMenuBarTitle()}
                        {...CustomDrawerController.resourcesViewModel?.getTestIDContactSideMenuBarTitle()}
                        onPress={() => controller.onDrawerItemPressed(NavigationKey.Contact)}
                        iconSource={images.contact} />
                    <DrawerItemCompoenents text={CustomDrawerController.resourcesViewModel?.getResourceLogoutSideMenuBarTitle()}
                        {...CustomDrawerController.resourcesViewModel?.getTestIDLogoutSideMenuBarTitle()}
                        onPress={controller.onSignOutPressed} iconSource={images.logout} />
                </AnimatedScrollView>
            </View>
            <Button
                isSolid
                isAnimated={false}
                preset='link'
                imagePreset="close"
                style={Styles.CLOSE_VIEW}
                {...CustomDrawerController.resourcesViewModel?.getTestIDButtonCloseSideMenuBarTitle()}
                onPress={controller.closeDrawer}
            />
        </View>

    )
})