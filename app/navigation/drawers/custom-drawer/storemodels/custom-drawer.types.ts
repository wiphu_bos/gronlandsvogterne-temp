import { Instance, SnapshotOut } from "mobx-state-tree"
import { CustomDrawerStoreModel, CustomDrawerResourcesStoreModel } from "./custom-drawer.store"

export type CustomDrawerStore = Instance<typeof CustomDrawerStoreModel>
export type CustomDrawerStoreSnapshot = SnapshotOut<typeof CustomDrawerStoreModel>

export type CustomDrawerResourcesStore = Instance<typeof CustomDrawerResourcesStoreModel>
export type CustomDrawerResourcesStoreSnapshot = SnapshotOut<typeof CustomDrawerResourcesStoreModel>