import { types } from "mobx-state-tree"
import { StoreName } from "../../../../constants/app.constant"
import { CustomDrawerPropsModel } from "../viewmodels/custom-drawer.models"
import { CustomDrawerActions } from "../viewmodels/custom-drawer.actions"
import { CustomDrawerViews, CustomDrawerResourcesViews } from "../viewmodels/custom-drawer.views"
import { GeneralResourcesStoreModel } from "../../../../models/general-resources-store"
import { CustomDrawerServiceActions } from "../services/custom-drawer-services"

const CustomDrawerModel = types.model(StoreName.CustomDrawer, CustomDrawerPropsModel)
export const CustomDrawerStoreModel = types.compose(CustomDrawerModel, CustomDrawerViews, CustomDrawerActions, CustomDrawerServiceActions)
export const CustomDrawerResourcesStoreModel = types.compose(GeneralResourcesStoreModel, CustomDrawerResourcesViews)

