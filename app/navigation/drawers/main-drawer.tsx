
import React from "react"
import { createDrawerNavigator } from "@react-navigation/drawer"
import { NavigationKey } from "../../constants/app.constant"
import * as metric from "../../theme"
import { CustomDrawerContent } from "./custom-drawer/custom-drawer"
import { HomeScreen } from "../../modules/home/home.screen"
import { ManageProfileScreen } from "../../modules/tabs/manage-profile-tab/manage-profile.screen"
import { SearchListScreen } from "../../modules/search-list/search-list.screen"
import { FavouriteListScreen } from "../../modules/tabs/favourite-list-tab/favourite-list.screen"
import { ChatListScreen } from "../../modules/chat-list/chat-list.screen"
import { WebViewScreen } from "../../modules/web-view/web-view.screen"
import { useDrawerInitialRender } from "../../custom-hooks/use-drawer-initial-render"

const Drawer = createDrawerNavigator()

export const DrawerComponents = () => {

    const isFirstLoadDone = useDrawerInitialRender()

    return (
        <Drawer.Navigator
            drawerStyle={metric.DrawerStyleConfig(isFirstLoadDone)}
            drawerContent={props => <CustomDrawerContent {...props} />}
            {...metric.drawerOptions}
            initialRouteName={NavigationKey.Home}
        >
            <Drawer.Screen name={NavigationKey.Home} component={HomeScreen} />
            <Drawer.Screen name={NavigationKey.ManageProfileTab} component={ManageProfileScreen} />
            <Drawer.Screen name={NavigationKey.SearchListScreen} component={SearchListScreen} />
            <Drawer.Screen name={NavigationKey.FavouriteListTab} component={FavouriteListScreen} />
            <Drawer.Screen name={NavigationKey.ChatList} component={ChatListScreen} />
            <Drawer.Screen name={NavigationKey.FAQ} component={WebViewScreen} />
            <Drawer.Screen name={NavigationKey.Contact} component={WebViewScreen} />
        </Drawer.Navigator>
    )
}