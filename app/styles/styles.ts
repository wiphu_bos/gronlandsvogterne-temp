import {
    ViewStyle,
    TextStyle
} from 'react-native';
import { color } from '../theme/color';
import { size, type } from '../theme/fonts';

const TEXT: TextStyle = {
    fontSize: size.medium,
    fontFamily: type.base,
    padding: 2,
}

const TEXT_BOLD: TextStyle = {
    fontFamily: type.bold,
    padding: 3
}

const DEFAULT_TEXT: TextStyle = {
    ...TEXT,
    color: color.palette.white
}

const ERROR_TEXT: TextStyle = {
    ...TEXT,
    color: color.palette.orange,
    fontSize: size.small
}

const VIEW_FULLFILL: ViewStyle = {
    flex: 1
}

const VIEW_CONTAINNER_STYLE: ViewStyle = {
    flex: 1,
    justifyContent: 'center'
}

export {
    TEXT,
    TEXT_BOLD,
    DEFAULT_TEXT,
    ERROR_TEXT,
    VIEW_CONTAINNER_STYLE,
    VIEW_FULLFILL
}