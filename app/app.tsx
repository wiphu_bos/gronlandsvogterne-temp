// Welcome to the main entry point of the app.
//
// In this file, we'll be kicking off our app or storybook.

import "./i18n"
import React, { useState, useEffect } from "react"
import { AppRegistry, YellowBox, StatusBar, Platform } from "react-native"
import { StatefulNavigator, GlobalBackButtonHandler } from "./navigation"
import { RootStore, RootStoreProvider, setupRootStore, useStores } from "./models/root-store"
import 'mobx-react-lite/batchingForReactNative'
import { enableScreens } from "react-native-screens"
import { AppStateHandler } from "./custom-hooks/use-app-state-handler"
import messaging from '@react-native-firebase/messaging'
import NotificationHandler from "./custom-hooks/use-notification/notification.handler"
// This puts screens in a native ViewController or Activity. If you want fully native
// stack navigation, use `createNativeStackNavigator` in place of `createStackNavigator`:
// https://github.com/kmagiera/react-native-screens#using-native-stack-navigator
enableScreens()

/**
 * Ignore some yellowbox warnings. Some of these are for deprecated functions
 * that we haven't gotten around to replacing yet.
 */
// console.disableYellowBox = true
YellowBox.ignoreWarnings([
  "componentWillMount is deprecated",
  "componentWillReceiveProps is deprecated",
])

/**
 * This is the root component of our app.
 */
export const App: React.FunctionComponent<{}> = () => {

  const [rootStore, setRootStore] = useState<RootStore | undefined>(undefined) // prettier-ignore

  useEffect(() => {
    setupRootStore().then(setRootStore)
  }, [])


  // Before we show the app, we have to wait for our state to be ready.
  // In the meantime, don't render anything. This will be the background
  // color set in native by rootView's background color.
  //
  // This step should be completely covered over by the splash screen though.
  //
  // You're welcome to swap in your own component to render if your boot up
  // sequence is too slow though.
  if (!rootStore) return null


  // otherwise, we're ready to render the app
  return (
    <RootStoreProvider value={rootStore}>
      <GlobalBackButtonHandler >
        <AppStateHandler>
          <StatusBar animated barStyle='light-content' showHideTransition='slide' translucent />
          <StatefulNavigator />
        </AppStateHandler>
      </GlobalBackButtonHandler>
    </RootStoreProvider>
  )
}

/**
 * This needs to match what's found in your app_delegate.m and MainActivity.java.
 */
const APP_NAME = "DateMyDude"
//handle background process when kill app
//https://rnfirebase.io/messaging/usage#background-application-state
messaging().setBackgroundMessageHandler(async remoteMessage => {
  const isIos = Platform.OS === "ios"
  if (!isIos)
    NotificationHandler.handleNotificationBadgeNumber(remoteMessage)
});

const headlessCheck = ({ isHeadless }) => {
  if (isHeadless) {
    // App has been launched in the background by iOS, ignore
    return null
  }
  return <App />
}


AppRegistry.registerComponent(APP_NAME, () => headlessCheck)
