import { BaseServicesController } from "./base-request.controller"
import * as RNIap from 'react-native-iap'
import { Product, Subscription, Purchase } from "react-native-iap"
import { Linking } from "react-native"
import { ServiceType } from "../../constants/service.constant"
import { APIRequestParams } from "../api/config/api.router-config.types"

class PaymentServices extends BaseServicesController {
    public purchaseHighlightDudeIAP = async (SKU: string[]) => {
        const planDetails: Product[] = await RNIap.getProducts(SKU)
        const highlightPlan = planDetails && planDetails[0]
        if (!highlightPlan) throw planDetails
        const result: Purchase = await RNIap.requestPurchase(highlightPlan.productId, false)
        if (!result?.transactionId) throw result
        return result
    }

    public subscribePremiumAccountIAP = async (SKU: string[]) => {
        const subscriptions: Subscription[] = await RNIap.getSubscriptions(SKU)
        const subscriptionsPlan = subscriptions && subscriptions[0]
        if (!subscriptionsPlan) throw subscriptions
        const result = await RNIap.requestSubscription(subscriptionsPlan.productId, false)
        if (!result?.transactionId) throw result
        return result
    }

    public confirmSubscribePremiumAccount = async (body: any) => {
        const requestParams: Partial<APIRequestParams> = {
            body
        }
        return await this.requestAPI(ServiceType.SubscribePremiumAccount, requestParams)
    }
    public invalidateDudeHighlight = async (queryParams: any) => {
        const requestParams: Partial<APIRequestParams> = {
            queryParams: queryParams?.uid
        }
        return await this.requestAPI(ServiceType.InvalidateDudeHighlight, requestParams)
    }
    public invalidatePremiumAccount = async () => {
        return await this.requestAPI(ServiceType.InvalidatePremiumAccount)
    }
    public cancelSubscribePremiumAccount = async (url: string) => {
        return await Linking.openURL(url)
    }
}

export default new PaymentServices()