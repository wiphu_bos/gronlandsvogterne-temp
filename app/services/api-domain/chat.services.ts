import { ServiceType } from "../../constants/service.constant"
import { BaseServicesController } from "./base-request.controller"
import { APIRequestParams } from "../api/config/api.router-config.types"

class ChatListServices extends BaseServicesController {
    public fetchChatList = async (params: any) => {
        const requestParams: Partial<APIRequestParams> = {
            params
        }
        return await this.requestAPI(ServiceType.GetChatList, requestParams)
    }
    public fetchMessageList = async (params: any) => {
        const requestParams: Partial<APIRequestParams> = {
            params
        }
        return await this.requestAPI(ServiceType.GetMessageList, requestParams)
    }

    public createChatRoomById = async (body: any) => {
        const requestParams: Partial<APIRequestParams> = {
            body
        }
        return await this.requestAPI(ServiceType.CreateChatRoomById, requestParams)
    }
    public createMessage = async (body: any) => {
        const requestParams: Partial<APIRequestParams> = {
            body
        }
        return await this.requestAPI(ServiceType.CreateMessage, requestParams)
    }

    public markReadChatById = (params: any) => {
        const requestParams: Partial<APIRequestParams> = {
            queryParams: params.chat_room_id
        }
        this.noWaitResponseFromRequestAPI(ServiceType.MarkReadChatById, requestParams)
    }
    public deleteChatRoomById = async (params: any) => {
        const requestParams: Partial<APIRequestParams> = {
            queryParams: params.chat_room_id
        }
        return await this.requestAPI(ServiceType.DeleteChatRoomById, requestParams)
    }
    public deleteMessageById = async (params: any) => {
        const requestParams: Partial<APIRequestParams> = {
            queryParams: params.message_id
        }
        return await this.requestAPI(ServiceType.DeleteMessageById, requestParams)
    }
}

export default new ChatListServices()