import { GeneralResources } from "../../constants/firebase/remote-config/remote-config.constant"
import { Validate } from "../../utils/local-validate/local-validate.types"
import { IAlertParams } from "../../modules/base.controller"
import { Alert } from "react-native"
import { FirebaseErrorResponseValidation } from "../../utils/firebase/authentication/auth.types"

export class BaseValidationServicesController {
    protected static _errorMessage = GeneralResources.ErrorMessage
    protected static _sharedKeys = GeneralResources.SharedKeys
    protected static _validateParams: Validate
    protected static _firebaseValidateParams: FirebaseErrorResponseValidation
    protected static _errorKey: string

    constructor(validateParams?: Validate | FirebaseErrorResponseValidation) {
        const apiErrors = (validateParams as Validate)?.error?.errors
        const firebaesError = (validateParams as FirebaseErrorResponseValidation)?.error
        BaseValidationServicesController._firebaseValidateParams = validateParams
        BaseValidationServicesController._validateParams = validateParams
        BaseValidationServicesController._errorKey = apiErrors && (apiErrors[0]?.msg) || (firebaesError)?.error
    }

    protected _getSharedResources = (key?: string) => {
        return BaseValidationServicesController?._validateParams?.rootStore?.getGeneralResourcesStore(key, true)
    }

    protected _generalAlertOS = (params: IAlertParams) => {
        setTimeout(() => {
            Alert.alert(params.title, params.message)
        }, params?.delay || 300)
    }
}