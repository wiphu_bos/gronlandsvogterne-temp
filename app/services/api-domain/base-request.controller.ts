import { ServiceType } from "../../constants/service.constant"
import { APIRequestParams } from "../api/config/api.router-config.types"
import { router } from "../api/config/api.router-config"
import { request, noWaitResponseFromRequest } from "../api/api"
import FireBaseServices from "../../utils/firebase/authentication/auth"
import { UserCredential } from "../../utils/firebase/authentication/auth.types"

export type AuthRequestParams = {
    email: string,
    password: string
}

export class BaseServicesController {

    protected noWaitResponseFromRequestAPI = (serviceType: ServiceType, params?: Partial<APIRequestParams>) => {
        noWaitResponseFromRequest(router(serviceType, params))
    }

    protected requestAPI = async (serviceType: ServiceType, params?: Partial<APIRequestParams>) => {
        const result = await request(router(serviceType, params))
        if (!result) throw result
        return result
    }

    protected requestFirebase = async (serviceType: ServiceType, params: Partial<AuthRequestParams>) => {
        switch (serviceType) {
            case ServiceType.SignInEmailAndPassword:
                const result = await FireBaseServices.signInEmailAndPassword(params.email, params.password)
                const firebaseUser: UserCredential = result
                const user = firebaseUser.user
                if (!user) throw result
                return user
            case ServiceType.SendPasswordResetEmail:
                await FireBaseServices.sendPasswordResetEmail(params.email)
                return true
            case ServiceType.FetchSignInMethodsForEmail:
                const methods = await FireBaseServices.fetchSignInMethodsForEmail(params.email)
                return methods
            default:
                return false
        }
    }
}