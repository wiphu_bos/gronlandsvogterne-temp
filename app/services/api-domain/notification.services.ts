import { APIRequestParams } from "../api/config/api.router-config.types"
import { ServiceType } from "../../constants/service.constant"
import { INotification } from "../../models/notification-store"
import { BaseServicesController } from "./base-request.controller"

class NotificationServices extends BaseServicesController {
    public getNotificationList = async (params: any) => {
        const requestParams: Partial<APIRequestParams> = {
            params
        }
        return await this.requestAPI(ServiceType.GetNotificationList, requestParams)
    }

    public markReadNotificationByIdWaitResponse = async (params: Partial<INotification>) => {
        const requestParams: Partial<APIRequestParams> = {
            queryParams: params.notification_id
        }
        return await this.requestAPI(ServiceType.MarkReadNotificationById, requestParams)
    }

    /*

        No need to use `async` and `await` because we don't care about any responses.

    */

    public markReadNotificationById = (params: Partial<INotification>) => {
        const requestParams: Partial<APIRequestParams> = {
            queryParams: params.notification_id
        }
        this.noWaitResponseFromRequestAPI(ServiceType.MarkReadNotificationById, requestParams)
    }

    public markReadCommentNotificationByTopic = (params: Partial<INotification>) => {
        const requestParams: Partial<APIRequestParams> = {
            queryParams: params.topic
        }
        this.noWaitResponseFromRequestAPI(ServiceType.MarkReadNotificationByTopic, requestParams)
    }
}

export default new NotificationServices()