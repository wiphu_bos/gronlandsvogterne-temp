import { RemoteConfigDictionary } from "../../constants/firebase/remote-config"
import { Validate } from "../../utils/local-validate/local-validate.types"
import { PaymentErrorResponse, FirebaseAuthErrorResponse } from "../../constants/firebase/auth/auth-error-response.constant"
import { BaseValidationServicesController } from "./base-validate.controller"
import { FirebaseErrorResponseValidation } from "../../utils/firebase/authentication/auth.types"
import { APIErrorResponse } from "../../constants/service.types"

export class ValidationServices extends BaseValidationServicesController {

    constructor(validateParams?: Validate | FirebaseErrorResponseValidation) {
        super(validateParams)
    }

    public verifyCode = (): void => {
        const { VerifyCodeInvalidTitle, VerifyCodeIsExpiredTitle } = RemoteConfigDictionary
        const errorKey = ValidationServices._errorKey
        let errorPath: string
        if (errorKey === VerifyCodeInvalidTitle) {
            errorPath = ValidationServices._errorMessage.verifyCodeInvalidTitle
        } else if (errorKey === VerifyCodeIsExpiredTitle) {
            errorPath = ValidationServices._errorMessage.verifyCodeIsExpiredTitle
        }
        const translatedErrorMessage = this._getSharedResources(errorPath)
        ValidationServices._validateParams?.localViewModel?.setCodeErrorMessage(translatedErrorMessage)
    }

    public premiumAccount = (): [boolean, string?] => {
        let isError: boolean = false
        const paymentErrorMessage = ValidationServices._sharedKeys.paymentStatusErrorMassage
        const paymentStatusErrorMassageObject = this._getSharedResources(paymentErrorMessage)
        const translatedErrorMessage = paymentStatusErrorMassageObject[ValidationServices._errorKey]
        isError = ValidationServices._errorKey === PaymentErrorResponse.NotPremiumAccount
        return [isError, translatedErrorMessage]
    }

    public purchaseDudeHighlight = (): [boolean, string?] => {
        let isError: boolean = false
        const paymentErrorMessage = ValidationServices._sharedKeys.paymentStatusErrorMassage
        const paymentStatusErrorMassageObject = this._getSharedResources(paymentErrorMessage)
        const translatedErrorMessage = paymentStatusErrorMassageObject[ValidationServices._errorKey]
        isError = ValidationServices._errorKey === PaymentErrorResponse.PurchaseExpired
        return [isError, translatedErrorMessage]
    }

    public generalAPIResponseValidation = (): string => {
        const rootStore = ValidationServices._firebaseValidateParams?.rootStore
        const localViewModel = ValidationServices._firebaseValidateParams?.localViewModel
        const getIsConnected = rootStore?.getSharedStore?.getIsConnected
        const firebaseErrorMessage = ValidationServices._sharedKeys.firebaseErrorMessage
        const firebaseErrorMessageObject = this._getSharedResources(firebaseErrorMessage)
        const internetErrorMessage = firebaseErrorMessageObject[FirebaseAuthErrorResponse.NetworkRequestFailed]
        const errorResponse = ValidationServices._firebaseValidateParams?.error
        const generalErrorMsgObj = (errorResponse as APIErrorResponse)?.errors
        const errorKey: string = errorResponse?.code || generalErrorMsgObj && generalErrorMsgObj[0]?.msg || null
        let errorMessage: string = this._getSharedResources(ValidationServices._errorMessage.somethingWentWrong)
        let shouldAlert: boolean = true

        if (getIsConnected) {
            if (errorKey) {
                errorMessage = firebaseErrorMessageObject[errorKey]
                rootStore?.getSharedStore?.setIsLoading(false)
                if (localViewModel) {
                    if (errorKey === FirebaseAuthErrorResponse.EmailInvalid ||
                        errorKey === FirebaseAuthErrorResponse.EmailAlreadyInUse ||
                        errorKey === FirebaseAuthErrorResponse.UserNotFound) {
                        localViewModel?.setEmailErrorMessage(errorMessage)
                        shouldAlert = false
                    } else if (errorKey === FirebaseAuthErrorResponse.WrongPassword) {
                        localViewModel?.setPasswordErrorMessage(errorMessage)
                        shouldAlert = false
                    }
                }
            }
        } else {
            errorMessage = internetErrorMessage
        }

        if (ValidationServices._firebaseValidateParams.goBackHandler) ValidationServices._firebaseValidateParams.goBackHandler()

        return (shouldAlert && typeof errorKey === 'string') ? errorKey || errorMessage : null
    }

}