import { APIRequestParams } from "../api/config/api.router-config.types"
import { ServiceType } from "../../constants/service.constant"
import { BaseServicesController } from "./base-request.controller"
import { Property } from "../../constants/firebase/fire-store.constant"

type ICreateComment = {
    [Property.UID]: string,
    [Property.MESSAGE]: string
}

type IFetchComment = {
    [Property.UID]: string,
    [Property.LAST_ID]: string
}

type IReportComment = {
    [Property.DUDE_ID]: string,
    [Property.COMMENT_ID]: string,
    [Property.DESCRIPTION]: string
}

class CommentServices extends BaseServicesController {
    public getCommentList = async (params: IFetchComment) => {
        const requestParams: Partial<APIRequestParams> = {
            params
        }
        return await this.requestAPI(ServiceType.DudeCommentList, requestParams)
    }

    public createComment = async (body: ICreateComment) => {
        const requestParams: Partial<APIRequestParams> = {
            body
        }
        return await this.requestAPI(ServiceType.CreateComment, requestParams)
    }

    public reportComment = async (body: IReportComment) => {
        const requestParams: Partial<APIRequestParams> = {
            body
        }
        return await this.requestAPI(ServiceType.ReportComment, requestParams)
    }
}
export default new CommentServices()