import { IUser } from "../../models/user-store/user.types"
import { ServiceType } from "../../constants/service.constant"
import { APIRequestParams } from "../api/config/api.router-config.types"
import { BaseServicesController } from "./base-request.controller"

class UserServices extends BaseServicesController {
    public askForPublishDude = async (params: Partial<IUser>) => {
        const requestParams: Partial<APIRequestParams> = {
            queryParams: params.uid,
            body: params
        }
        return await this.requestAPI(ServiceType.AskForPublishDude, requestParams)
    }

    public fetchMyProfile = async () => {
        return await this.requestAPI(ServiceType.GetMyProfile)
    }

    public updateDeviceToken = async (token: string) => {
        const requestParams: Partial<APIRequestParams> = {
            body: {
                token
            }
        }
        return await this.requestAPI(ServiceType.UpdateDeviceToken, requestParams)
    }

    public searchUserList = async (params: any) => {
        const requestParams: Partial<APIRequestParams> = {
            params
        }
        return await this.requestAPI(ServiceType.SearchUserList, requestParams)
    }

    public fetchMyDudeList = async (params: any) => {
        const requestParams: Partial<APIRequestParams> = {
            params
        }
        return await this.requestAPI(ServiceType.GetMyDudeList, requestParams)
    }

    public fetchFavouriteList = async (params: any) => {
        const requestParams: Partial<APIRequestParams> = {
            params
        }
        return await this.requestAPI(ServiceType.GetFavouriteList, requestParams)
    }

    public deleteMyUser = async () => {
        return await this.requestAPI(ServiceType.DeleteMyUser)
    }

    public getDudetteDetail = async (params: any) => {
        const requestParams: Partial<APIRequestParams> = {
            queryParams: params.uid
        }
        return await this.requestAPI(ServiceType.GetDudetteProfile, requestParams)
    }

    public doFavouriteDudette = async (body: any) => {
        const requestParams: Partial<APIRequestParams> = {
            body
        }
        return await this.requestAPI(ServiceType.FavouriteDudette, requestParams)
    }

    public patchSeekingGuy = async (body: any) => {
        const requestParams: Partial<APIRequestParams> = {
            body
        }
        return await this.requestAPI(ServiceType.PatchSeekingGuy, requestParams)
    }

    public putUser = async (body: any) => {
        const requestParams: Partial<APIRequestParams> = {
            body
        }
        return await this.requestAPI(ServiceType.PutUser, requestParams)
    }

    public increaseViewCount = async (body: any) => {
        const requestParams: Partial<APIRequestParams> = {
            body
        }
        return await this.requestAPI(ServiceType.IncreaseCount, requestParams)
    }

    public fetchChatList = async (params: any) => {
        const requestParams: Partial<APIRequestParams> = {
            params
        }
        return await this.requestAPI(ServiceType.GetChatList, requestParams)
    }

    public signOut = async (token: string) => {
        const params = {
            token
        }
        const requestParams: Partial<APIRequestParams> = {
            body: { ...params }
        }
        return await this.requestAPI(ServiceType.SignOut, requestParams)
    }

}

export default new UserServices()