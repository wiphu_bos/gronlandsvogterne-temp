import { ServiceType } from "../../constants/service.constant"
import { BaseServicesController } from "./base-request.controller"

class InformationServices extends BaseServicesController {
    public fetchServerDateTime = async () => {
        return await this.requestAPI(ServiceType.GetServerDateTime)
    }
}
export default new InformationServices()