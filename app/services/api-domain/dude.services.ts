import { IUser } from "../../models/user-store/user.types"
import { APIRequestParams } from "../api/config/api.router-config.types"
import { ServiceType } from "../../constants/service.constant"
import { BaseServicesController } from "./base-request.controller"

class DudeServices extends BaseServicesController {
    public createDude = async (body: Partial<IUser>) => {
        const requestParams: Partial<APIRequestParams> = {
            body
        }
        return await this.requestAPI(ServiceType.CreateDude, requestParams)
    }

    public searchDudeList = async (params: any) => {
        const requestParams: Partial<APIRequestParams> = {
            params
        }
        return await this.requestAPI(ServiceType.SearchDudeList, requestParams)
    }

    public getDudeProfile = async (params: any) => {
        const requestParams: Partial<APIRequestParams> = {
            queryParams: params.uid
        }
        return await this.requestAPI(ServiceType.DudeProfile, requestParams)
    }

    public sendEmailToDude = async (body: any) => {
        const requestParams: Partial<APIRequestParams> = {
            body
        }
        return await this.requestAPI(ServiceType.SendEmailToDude, requestParams)
    }

    public deleteDude = async (params: any) => {
        const requestParams: Partial<APIRequestParams> = {
            queryParams: params.uid
        }
        return await this.requestAPI(ServiceType.DeleteDude, requestParams)
    }

    public changeStatus = async (body: any) => {
        const requestParams: Partial<APIRequestParams> = {
            body
        }
        return await this.requestAPI(ServiceType.ChangeStatus, requestParams)
    }


    /*

           No need to use `async` and `await` because we don't care about any responses.

    */

    public doFavouriteDude = (body: any) => {
        const requestParams: Partial<APIRequestParams> = {
            body
        }
        this.noWaitResponseFromRequestAPI(ServiceType.FavouriteDude, requestParams)
    }

    public increaseViewCount = (body: any) => {
        const requestParams: Partial<APIRequestParams> = {
            body
        }
        this.noWaitResponseFromRequestAPI(ServiceType.IncreaseCount, requestParams)
    }

}

export default new DudeServices()