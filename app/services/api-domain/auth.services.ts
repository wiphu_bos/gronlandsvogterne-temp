import { IUser } from "../../models/user-store/user.types"
import { ServiceType } from "../../constants/service.constant"
import { APIRequestParams } from "../api/config/api.router-config.types"
import { BaseServicesController, AuthRequestParams } from "./base-request.controller"

class AuthServices extends BaseServicesController {
    public createFirebaseAuth = async (body: Partial<IUser>) => {
        const requestParams: Partial<APIRequestParams> = {
            body
        }
        return await this.requestAPI(ServiceType.CreateFirebaseAuth, requestParams)
    }

    public createUser = async (body: Partial<IUser>) => {
        const requestParams: Partial<APIRequestParams> = {
            body
        }
        return await this.requestAPI(ServiceType.CreateUser, requestParams)
    }

    public sendCodeToVerify = async (body: Partial<IUser>) => {
        const requestParams: Partial<APIRequestParams> = {
            body
        }
        return await this.requestAPI(ServiceType.SendCodeToVerify, requestParams)
    }

    public generateVerifyCode = async (body: Partial<IUser>) => {
        const requestParams: Partial<APIRequestParams> = {
            body
        }
        return await this.requestAPI(ServiceType.PostVerifyCode, requestParams)
    }

    public resendVerifyCode = async (body: Partial<IUser>) => {
        const requestParams: Partial<APIRequestParams> = {
            body
        }
        return await this.requestAPI(ServiceType.ResendVerifyCode, requestParams)
    }

    public signInWithEmailAndPassword = async (params: AuthRequestParams) => {
        console.log( 'jwb : email > ' + params.email )
        console.log( 'jwb : password > ' + params.password )

        return await this.requestFirebase(ServiceType.SignInEmailAndPassword, params)
    }

    public sendPasswordResetEmail = async (params: Partial<AuthRequestParams>) => {
        return await this.requestFirebase(ServiceType.SendPasswordResetEmail, params)
    }

    public fetchSignInMethodsForEmail = async (params: Partial<AuthRequestParams>) => {
        return await this.requestFirebase(ServiceType.FetchSignInMethodsForEmail, params)
    }
}

export default new AuthServices()