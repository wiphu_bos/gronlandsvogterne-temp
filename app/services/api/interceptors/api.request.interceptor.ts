import { AxiosRequestConfig, AxiosError } from "axios"
import * as APIConfig from "../../../config/api.config"
import { HeaderType } from "../../../constants/service.constant"
import FireBaseServices from "../../../utils/firebase/authentication/auth"
import * as Locale from "../../../utils/locale"


export const requestInterceptor = async (config: AxiosRequestConfig) => {
    const token = await FireBaseServices.getFirebaseToken() //Get Token from LocalStorage;
    if (token) config.headers[HeaderType.Authorization] = HeaderType.Bearer + ' ' + token
    config.headers[HeaderType.Language] = Locale.getLocaleDevice()
    config.headers[HeaderType.ContentType] = APIConfig.ContentType.JSON
    return config
}

export const requestInterceptorError = (error: AxiosError<any>) => {
    return Promise.reject(error)
}