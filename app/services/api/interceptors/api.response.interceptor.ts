import { AxiosResponse, AxiosError } from "axios"
import { IResponse } from "../api.types";
import { requestErrorHandler } from "../error-handler";

export const responseInterceptor = (response: AxiosResponse<IResponse>) => response
export const responseInterceptorError = (error: AxiosError<any>) => requestErrorHandler(error)