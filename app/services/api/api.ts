import { api } from "./config/api.config"
import { IResponse } from "./api.types"
import axios, { AxiosRequestConfig, CancelTokenSource } from "axios"

// let source: CancelTokenSource
export const request = async (config: AxiosRequestConfig): Promise<IResponse> => {
    // if (source) source.cancel()
    // source = axios.CancelToken.source()
    // config.cancelToken = source.token
    return await api.request<IResponse>(config)
}

// let noWaitResponseSource: CancelTokenSource
export const noWaitResponseFromRequest = (config: AxiosRequestConfig) => {
    // if (noWaitResponseSource) noWaitResponseSource.cancel()
    // noWaitResponseSource = axios.CancelToken.source()
    // config.cancelToken = noWaitResponseSource.token
    api.request<IResponse>(config)
}