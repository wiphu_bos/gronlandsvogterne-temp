import { AxiosError, AxiosRequestConfig, AxiosPromise } from "axios"
import { api } from "../config/api.config"
import { useStores } from "../../../models/root-store"
import { IAuthResponse } from "../../../models/auth-store/auth.types"
import { HeaderType } from "../../../constants/service.constant"
import FireBaseServices from "../../../utils/firebase/authentication/auth"

let isRefreshing = false
let failedQueue = []
let originalRequest: AxiosRequestConfig

const processQueue = (error: string, token: string = null) => {
    for (const promise of failedQueue) {
        error ?
            promise.reject(error) :
            promise.resolve(token)
    }
    failedQueue = []
}

const setRefreshedToken = (token: string) => originalRequest.headers[HeaderType.Authorization] = HeaderType.Bearer + ' ' + token

const pushToFailedQueue = async (): Promise<string> => await new Promise<string>((resolve, reject) => failedQueue.push({ resolve, reject }))

const refreshToken = async (): Promise<AxiosPromise<IAuthResponse>> => await new Promise<AxiosPromise<IAuthResponse>>(async (resolve, reject) => {

    const rootStore = useStores()
    const { getAuthStore } = rootStore

    //const localToken = getAuthStore?.getToken //Get Token from LocalStorage(old token) to send to api to refresh

    try {
        //Request API for Refreshing
        // const data: IAuthResponse = (await api.post<IAuthResponse>("refreshtokenAPI", { token: localToken })).data

        //Firebase Auth refresh token
        const token = await FireBaseServices.getFirebaseToken()
        const data = { token: token }

        //Save Token to LocalStorage
        // getAuthStore?.setToken(data)
        // setRefreshedToken(getAuthStore?.getToken)
        // processQueue(null, getAuthStore?.getToken)
        setRefreshedToken(token)
        processQueue(null, token)
        resolve(api(originalRequest))
    }
    catch (e) {
        processQueue(e, null)
        reject(e)
    }
    finally {
        isRefreshing = false
    }
})

export const refreshTokenManager = async (error: AxiosError<any>) => {
    originalRequest = error.config
    if (isRefreshing) {
        try {
            const token: string = await pushToFailedQueue()
            setRefreshedToken(token)
            return api(originalRequest)
        }
        catch (e) {
            return Promise.reject(e)
        }
    }
    originalRequest.headers._retry = true
    isRefreshing = true
    return await refreshToken()
}