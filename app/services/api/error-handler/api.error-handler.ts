import { AxiosError } from "axios"
import { HTTP_CODE } from "../../../constants/http.constant"
import { refreshTokenManager } from "./api.refresh-token"
import { FirebaseAuthErrorResponse, AXIOSErrorResponse } from "../../../constants/firebase/auth/auth-error-response.constant"
import { api } from "../config/api.config"

export const requestErrorHandler = async (error: AxiosError<any>) => {
    const errorKey: string = error?.code
    const request = error?.config
    const errorData = error?.response?.data
    if (
        (errorKey && (errorKey === FirebaseAuthErrorResponse.NetworkRequestFailed || errorKey === AXIOSErrorResponse.ECONNABORTED)) ||
        (typeof errorData === 'string' && errorData?.includes('could not handle the request'))
    ) {
        return api(request)
    } else {
        const status = error?.response?.status
        if (status === HTTP_CODE._401 && !error?.config?.headers._retry) {
            return refreshTokenManager(error)
        }
    }
    return Promise.resolve(error?.response?.data)
}