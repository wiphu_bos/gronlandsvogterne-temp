import { ServiceType } from "../../../constants/service.constant"
import { AxiosRequestConfig } from "axios"
import { APIRequestParams } from "./api.router-config.types"

export const router = (type: ServiceType, params?: Partial<APIRequestParams>): AxiosRequestConfig => {
    let router: AxiosRequestConfig = {}
    const queryParams: string = params?.queryParams
    const paramsObject = params?.params
    const data = params?.body && JSON.stringify(params?.body)
    const version = 'v1.0/mobile'
    switch (type) {
        case ServiceType.CreateUser:
            console.log('jwb CreateUser')
            router.method = 'post'
            router.url = version + '/users'
            router.data = data
            break
        case ServiceType.PostVerifyCode:
            router.method = 'post'
            router.url = version + '/users/generate-code'
            router.data = data
            break
        case ServiceType.SendCodeToVerify:
            router.method = 'post'
            router.url = version + '/users/verify-code'
            router.data = data
            break
        case ServiceType.CreateFirebaseAuth:
            router.method = 'post'
            router.url = version + '/users/create-auth'
            router.data = data
            break
        case ServiceType.GetHomeContent:
            router.method = 'get'
            router.url = version + '/dude/highlight'
            router.cache = false
            router.forceUpdate = true
            break
        case ServiceType.CreateDude:
            router.method = 'post'
            router.url = version + '/dude/ask-for-approve'
            router.data = data
            break
        case ServiceType.Activity:
            router.method = 'post'
            router.url = version + '/dude/ask-for-approve'
            router.data = data
            break
        case ServiceType.UpdateDeviceToken:
            router.method = 'patch'
            router.url = version + '/users/device-token'
            router.data = data
            break
        case ServiceType.GetMyProfile:
            router.method = 'get'
            router.url = version + '/users/me'
            break
        case ServiceType.AskForPublishDude:
            router.method = 'patch'
            router.url = version + '/dude/ask-for-publish/' + queryParams
            router.data = data
            break
        case ServiceType.ResendVerifyCode:
            router.method = 'post'
            router.url = version + '/users/re-generate-code'
            router.data = data
            break
        case ServiceType.DudeProfile:
            router.method = 'get'
            router.url = version + '/dude/profile/' + queryParams
            break
        case ServiceType.DudeCommentList:
            router.method = 'get'
            router.url = version + '/dudes/comments'
            router.params = paramsObject
            break
        case ServiceType.CreateComment:
            router.method = 'post'
            router.url = version + '/dudes/comment/createMessage'
            router.data = data
            break
        case ServiceType.FavouriteDude:
            router.method = 'put'
            router.url = version + '/favourite/dude'
            router.data = data
            break
        case ServiceType.IncreaseCount:
            router.method = 'patch'
            router.url = version + '/information/increase-count'
            router.data = data
            break
        case ServiceType.SendEmailToDude:
            router.method = 'post'
            router.url = version + '/dudes/send-email'
            router.data = data
            break
        case ServiceType.GetMyDudeList:
            router.method = 'get'
            router.url = version + '/users/mydudes'
            router.params = paramsObject
            break
        case ServiceType.SignOut:
            router.method = 'patch'
            router.url = version + '/users/signout'
            router.data = data
            break
        case ServiceType.ReportComment:
            router.method = 'post'
            router.url = version + '/dudes/report-comment'
            router.data = data
            break
        case ServiceType.GetNotificationList:
            router.method = 'get'
            router.url = version + '/notifications'
            router.params = paramsObject
            break
        case ServiceType.MarkReadNotificationById:
            router.method = 'patch'
            router.url = version + '/notifications/markread/id/' + queryParams
            break
        case ServiceType.MarkReadNotificationByTopic:
            router.method = 'patch'
            router.url = version + '/notifications/markread/topic/' + queryParams
            break
        case ServiceType.FavouriteDudette:
            router.method = 'put'
            router.url = version + '/favourite/dudette'
            router.data = data
            break
        case ServiceType.GetDudetteProfile:
            router.method = 'get'
            router.url = version + '/users/profile/' + queryParams
            break
        case ServiceType.DeleteDude:
            router.method = 'delete'
            router.url = version + '/dude/' + queryParams
            break
        case ServiceType.ChangeStatus:
            router.method = 'patch'
            router.url = version + '/dude/status'
            router.data = data
            break
        case ServiceType.PatchSeekingGuy:
            router.method = 'patch'
            router.url = version + '/users/seeking-guy'
            router.data = data
            break
        case ServiceType.PutUser:
            router.method = 'put'
            router.url = version + '/users'
            router.data = data
            break
        case ServiceType.DeleteMyUser:
            router.method = 'delete'
            router.url = version + '/users'
            break
        case ServiceType.GetFavouriteList:
            router.method = 'get'
            router.url = version + '/favourite/all'
            router.params = paramsObject
            break
        case ServiceType.SearchUserList:
            router.method = 'get'
            router.url = version + '/users/listFilter'
            router.params = paramsObject
            break
        case ServiceType.SearchDudeList:
            router.method = 'get'
            router.url = version + '/dude/list'
            router.params = paramsObject
            break
        case ServiceType.GetChatList:
            router.method = 'get'
            router.url = version + '/chat'
            router.params = paramsObject
            break
        case ServiceType.GetMessageList:
            router.method = 'get'
            router.url = version + '/chat/message'
            router.params = paramsObject
            break
        case ServiceType.CreateChatRoomById:
            router.method = 'post'
            router.url = version + '/chat/room'
            router.data = data
            break
        case ServiceType.CreateMessage:
            router.method = 'post'
            router.url = version + '/chat/message'
            router.data = data
            break
        case ServiceType.DeleteChatRoomById:
            router.method = 'delete'
            router.url = version + '/chat/room/' + queryParams
            break
        case ServiceType.DeleteMessageById:
            router.method = 'delete'
            router.url = version + '/chat/message/' + queryParams
            break
        case ServiceType.GetServerDateTime:
            router.method = 'get'
            router.url = version + '/server/datetime'
            break
        case ServiceType.SubscribePremiumAccount:
            router.method = 'post'
            router.url = version + '/payment/subscription'
            router.data = data
            break
        case ServiceType.InvalidateDudeHighlight:
            router.method = 'delete'
            router.url = version + '/payment/purchase/' + queryParams
            break
        case ServiceType.InvalidatePremiumAccount:
            router.method = 'delete'
            router.url = version + '/payment/subscription'
            break
        case ServiceType.MarkReadChatById:
            router.method = 'patch'
            router.url = version + '/chat/room/markread/' + queryParams
            break

        default: break
    }
    return router
}