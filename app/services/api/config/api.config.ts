import axios, { AxiosRequestConfig } from "axios"
import { BaseURL, Timeout } from "../../../config/api.config"
import { requestInterceptor, requestInterceptorError } from "../interceptors/api.request.interceptor"
import { responseInterceptor, responseInterceptorError } from "../interceptors/api.response.interceptor"
import { cacheAdapterEnhancer } from 'axios-extensions';

const config: AxiosRequestConfig = {
    baseURL: BaseURL,
    timeout: Timeout,
    headers: { 'Cache-Control': 'no-cache' },
    adapter: cacheAdapterEnhancer(axios.defaults.adapter,{ enabledByDefault: false})
}

export const api = axios.create(config)
api.interceptors.request.use(requestInterceptor, requestInterceptorError)
api.interceptors.response.use(responseInterceptor, responseInterceptorError)


