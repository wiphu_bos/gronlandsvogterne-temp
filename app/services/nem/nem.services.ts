import { CRIIPTO } from "../../constants/provider/criipto.constant"
import { authorize, AuthorizeResult } from 'react-native-app-auth';

export type NEMIDResponse = {
    data?: AuthorizeResult,
    error?: Error
}

export const verifyNEMID = async () => {
    let data: NEMIDResponse = null;
    try {
        const newAuthState: AuthorizeResult = await authorize(CRIIPTO());
        data = {
            data: newAuthState
        }
    } catch (error) {
        data = {
            error: error
        }
    } finally {
        return data
    }
}

