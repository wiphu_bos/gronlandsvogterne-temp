import { Platform } from "react-native";
import { packageName } from "../../config/app.config";

enum PaymentPackages {
  HighlightDudePackage = 'highlight_dude_7',
  PremiumAccountPackage = 'premium_account_30'
}

export const CANCEL_SUBSCRIPTION_URL = Platform.select({
  ios: Platform.Version <= 11 ? 'https://buy.itunes.apple.com/WebObjects/MZFinance.woa/wa/manageSubscriptions' : 'https://apps.apple.com/account/subscriptions',
  android: 'https://play.google.com/store/account/subscriptions?package=' + packageName + '&sku=' + PaymentPackages.PremiumAccountPackage
});

export const SKU_HIGHLIGT = Platform.select({
  ios: [
    PaymentPackages.HighlightDudePackage
  ],
  android: [
    PaymentPackages.HighlightDudePackage
  ]
});

export const SKU_PREMIUM_ACCOUNT = Platform.select({
  ios: [
    PaymentPackages.PremiumAccountPackage
  ],
  android: [
    PaymentPackages.PremiumAccountPackage
  ]
});