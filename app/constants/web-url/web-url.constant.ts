import { NativeModules } from 'react-native'
import * as LocaleUtils from "../../utils/locale"

const { ReactNativeConfig } = NativeModules

export const BASE_URL = (): string => {
  const environment = ReactNativeConfig.buildEnvironment
  switch (environment) {
      case "Development":
          return 'https://datemydude-dev.web.app/'
      case "Alpha":
          return 'https://datemydude-alpha.web.app/'
      case "Staging":
          return 'https://datemydude-staging.web.app/'
      default:
          return 'https://datemindude.dk/'
  }
}

export const FAQ_URL = BASE_URL() + 'faq?lang=' + LocaleUtils.getLocaleDevice()

export const CONTACT_URL = BASE_URL() + 'about-us?lang=' + LocaleUtils.getLocaleDevice()

export const PRIVACY_URL = BASE_URL() + 'assets/file/policy.pdf'