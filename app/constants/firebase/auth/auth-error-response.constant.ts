export enum FirebaseAuthErrorResponse {
    UserNotFound = "auth/user-not-found",
    WrongPassword = "auth/wrong-password",
    EmailAlreadyInUse = "auth/email-already-in-use",
    EmailInvalid = "auth/invalid-email",
    NetworkRequestFailed = "auth/network-request-failed",
    NoAuthorization = "auth/requires-recent-login"
}

export enum PaymentErrorResponse {
    NotPremiumAccount = "payment/not-premium-account",
    PurchaseExpired = 'payment/purchase-expired'
}

export enum AXIOSErrorResponse {
    ECONNABORTED = "ECONNABORTED"
}

export enum InAppPurchaseErrorResponse {
    E_UNKNOWN = "E_UNKNOWN",
    E_USER_CANCELLED = "E_USER_CANCELLED"
}

export enum FirebaseStorageErrorResponse {
    FileNotFound = "storage/file-not-found"
}