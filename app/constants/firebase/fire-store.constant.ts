export enum Collection {
    NOTIFICATIONS = "notifications",
    PURCHASES = "purchases",
    SUBSCRIPTIONS = "subscriptions",
    USERS = "users",
    DUDES = "dudes",
    CHATS = "chats",
    MESSAGES = "messages",
    COMMENTS = "comments",
    FAVOURITE_DUDE = "favorite_dudes",
    FAVOURITE_DUDETTE = "favorite_dudettes",
}

export enum MobileProperty {
    PASSWORD = "password",
    CONFIRM_PASSWORD = "confirmPassword",
    MY_DUDE_LIST = "myDudeList",
    NOTIFICATION_LIST = "notificationList",
    FAVOURITE_DUDE_LIST = "favDudeList",
    FAVOURITE_DUDETTE_LIST = "favDudetteList",
    COMPARE_PROFILE_IMAGE = "compare_profile_image",
    COMPARE_GALLERY_IMAGE_LIST = "compare_gallery_image_list",
    PROFILE_IMAGE_TO_REMOVE = "profile_image_to_remove",
    GALLERY_IMAGE_LIST_TO_REMOVE = "gallery_image_list_to_remove",
    TYPE = "type",
    FILE_PATH = "file_path",

    //Exists in Timer object.
    IS_TIMER_SET = "isTimerSet",
    EXPIRY_TIMER = "expiry_timer",
    TIMER = "timer",
    DATE_TIME_AGO = "dateTimeAgo",
    DATE_TIME = "dateTime",
    EXPIRY_DATE_COUNT = "expiryDateCount",
    SERVER_TIME = "serverTime",

    //Exists in ISelect object.
    KEY = "key",
    LABEL = "label"
}

export enum MediaProperty {
    STORAGE_PATH = "storage_path",
    FILE_NAME = "file_name",
    URL = "url"
}

export enum Property {
    UID = "uid",
    AGE = "age",
    IS_HIGHLIGHT = "is_highlight",
    FULL_NAME = "full_name",
    EMAIL = "email",
    DESCRIPTION = "description",
    STATUS = "status",
    CITY = "city",
    VIEW_COUNT = "view_count",
    COMMENT_COUNT = "comment_count",
    UNREAD_COMMENT_COUNT = "unread_comment_count",
    UNREAD_CHAT_COUNT = "unread_chat_count",
    HAVD_SOGER_HAN_LIST = "havd_soger_han_list",
    INTEREST_LIST = "interest_list",
    TAG_LIST = "tag_list",
    CREATED_DATE = "created_date",
    UPDATED_DATE = "updated_date",
    CHAT_COUNT = "chat_count",
    IS_FAVOURITE = "is_favourite",
    PROFILE_IMAGE = "profile_image",
    GALLERY_IMAGE_LIST = "gallery_image_list",
    //Exists in User role.
    COUNTRY = "country",
    IS_VERIFIED = "is_verified",
    ROLE = "role",
    DEVICE_TOKEN = "device_token",
    IS_SEEKING_GUY = "is_seeking_guy",
    //Exists in Dude role.
    USER_ID = "user_id",
    //Exists for payment.
    SKU_ID = "sku_id",
    TRANSACTION_ID = "transaction_id",
    TRANSACTION_RECEIPT = "transaction_receipt",
    EXPIRY_DATE = "expiry_date",
    IS_EXPIRED = "is_expired",
    IS_SUBSCRIPTION = "is_subscription",
    DUDE_DETAILS = "dude_details",
    PAYMENT_DETAILS = "payment_details",
    //Exists in Favourite object.
    DUDE_ID = "dude_id",
    DUDETTE_ID = "dudette_id",
    //Exists in Page object.
    PAGE_SIZE = "page_size",
    //Exists in Page object.
    SERVER_TIME = "server_time",
    IS_LAST_PAGE = "is_last_page",
    LAST_ID = "last_id",
    IS_LOAD_DONE = "is_load_done",
    PAGE = "page",
    //Exists in My dude list object.
    MY_DUDE_LIST = "my_dude_list",
    //Exists in Notification object.
    NOTIFICATION_ID = "notification_id",
    TITLE = "title",
    BODY = "body",
    DATA = "data",
    NAVIGATION = "navigation",
    MISC = "misc",
    TOPIC = "topic",
    IS_READ = "is_read",
    //Exists in Comment object,
    COMMENT_ID = "comment_id",
    MESSAGE = "message",
    //Exists in FavouriteList object.
    FAVOURITE_LIST = "favourite_list",
    //Exists in ChatList object.
    CHAT_LIST = "chat_list",
    MESSAGE_LIST = "message_list",
    CHAT_ROOM_ID = "chat_room_id",
    SENDER = "sender",
    RECEIVER = "receiver",
    USER = "user",
    INTERLOCUTOR_IDS = "interlocutor_ids",
    LAST_MESSAGE = "last_message",
    IS_REPLIED = "is_replied",
    TEXT = "text",
    INTERLOCUTOR_PROFILES = "interlocutor_profiles",
    //Exists in Message object.
    MESSAGE_ID = "message_id",
    IMAGE = "image",
    VIDEO = "video",
    AUDIO = "audio",
    IS_SENT = "sent",
    IS_RECEIVED = "received",
    IS_PENDING = "pending",
    SYSTEM = "system",
    QUICK_REPLIES = "quick_replies"

}

export enum SnapshotChangeType {
    ADDED = "added",
    MODIFIED = "modified",
    REMOVED = "removed"
}