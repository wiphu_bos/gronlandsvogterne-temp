import { RemoteConfigDictionary } from "./remote-config.dictionaries"

export enum RemoteConfigRootKey {
    GeneralResources = "dmd_rmcf_general_resources",
    Countries = "dmd_rmcf_countries",
    Introductions = "dmd_rmcf_introductions",
    Cities = "dmd_rmcf_denmark_cities",
    Regions = "dmd_rmcf_denmark_regions",
    RegionsCities = "dmd_rmcf_denmark_regions_cities",
    Interests = "dmd_rmcf_interests",
    HavdSogerHans = "dmd_rmcf_havd_soger_hans",
    Tags = "dmd_rmcf_tags",
    Notifications = "notification",
}

export enum RemoteConfigGroupedKey {
    NotificationTitle = "title",
    NotificationBody = "body",
    MenuTitle = "menu_title_object",
    SharedKeys = "share_keys",
    Action = "action",
    Modal = "modal",
    Subscription = "subscription_status",
    UserStatus = "user_status",
    GeneralErrorMessage = "general_error_message",
    FirebaseErrorMessage = "firebase_error_message",
    PaymentStatusErrorMassage = "payment_status_error_message",
    NEMIDeErrorMessage = "nemid_error_message",
    Alert = "alert",
    SeekingGuyStatus = "seeking_guy_status",
}

export class GeneralResources {
    static SharedKeys = {
        title: RemoteConfigDictionary.Title,
        subTitle: RemoteConfigDictionary.SubTitle,
        description: RemoteConfigDictionary.Description,
        fullNamePlaceholder: RemoteConfigDictionary.FullnamePlaceholder,
        passwordPlaceholder: RemoteConfigDictionary.PasswordPlaceholder,
        emailPlaceholder: RemoteConfigDictionary.EmailPlaceholder,
        appName: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigDictionary.AppName,
        slogan: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigDictionary.slogan,
        contactSupportTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigDictionary.ContactSupportTitle,
        buttonLoginTitle: RemoteConfigDictionary.ButtonLoginTitle,
        buttonNextTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Action + "." + RemoteConfigDictionary.NextTitle,
        buttonSubmitTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Action + "." + RemoteConfigDictionary.ButtonSubmitTitle,
        buttonSubmitCodeTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Action + "." + RemoteConfigDictionary.ButtonSubmitCodeTitle,
        ErrorMessage: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigDictionary.ErrorMessage,
        generalErrorMessage: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigDictionary.ErrorMessage + "." + RemoteConfigGroupedKey.GeneralErrorMessage,
        firebaseErrorMessage: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigDictionary.ErrorMessage + "." + RemoteConfigGroupedKey.FirebaseErrorMessage,
        paymentStatusErrorMassage: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigDictionary.ErrorMessage + "." + RemoteConfigGroupedKey.PaymentStatusErrorMassage,
        noInternetConnectionTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigDictionary.NoInternetConnectionTitle,
        buttonAcceptTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Action + "." + RemoteConfigDictionary.ButtonAcceptTitle,
        buttonContinueTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Action + "." + RemoteConfigDictionary.ContinueTitle,
        buttonCancelTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Action + "." + RemoteConfigDictionary.CancelTitle,
        buttonClearTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Action + "." + RemoteConfigDictionary.ClearTitle,
        buttonOKTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Action + "." + RemoteConfigDictionary.OKTitle,
        buttonSubscribeTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Action + "." + RemoteConfigDictionary.SubscribeTitle,
        statusTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigDictionary.StatusTitle,
        expireDateTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigDictionary.ExpireDateTitle,
        subscriptionActiveStatusTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Subscription + "." + RemoteConfigDictionary.ActiveStatusTitle,
        subscriptionDeactiveStatusTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Subscription + "." + RemoteConfigDictionary.DeactiveStatusTitle,
        cancelSubscriptionTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Action + "." + RemoteConfigDictionary.CancelSubscription,
        deleteTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Action + "." + RemoteConfigDictionary.DeleteTitle,
        makeSubscriptionTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Action + "." + RemoteConfigDictionary.MakeSubscriptionTitle,
        updateDudetteProfileTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigDictionary.UpdateDudetteProfileWarningTitle,
        updateDudeProfileTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigDictionary.UpdateDudeProfileWarningTitle,
        expireInTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigDictionary.ExpireInTitle,
        buttonSaveAndContinueTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Action + "." + RemoteConfigDictionary.SaveAndContinue,
        NEMIDErrorMessage: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigDictionary.ErrorMessage + "." + RemoteConfigGroupedKey.NEMIDeErrorMessage,
        selectPhoto: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigDictionary.SelectPhotoTitle,
        uploadProfilePhoto: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigDictionary.UploadProfilePhotoTitle,
        uploadPhotoToGallery: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigDictionary.UploadPhotoGalleryTitle,
        buttonGotoManageProfileTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Action + "." + RemoteConfigDictionary.ManageProfileTitle,
        buttonSelectTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Action + "." + RemoteConfigDictionary.SelectTitle,
        buttonSelectAllTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Action + "." + RemoteConfigDictionary.SelectAllTitle,
        buttonFiltersTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Action + "." + RemoteConfigDictionary.FiltersTitle,
        buttonResendVerifyCodeTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Action + "." + RemoteConfigDictionary.ButtonResendVerifyCodeTitle,
        verifyCodeSendToEmail: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigDictionary.VerifyCodeSendToEmail,
        enterTextPlaceholder: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigDictionary.EnterTextPlaceholder,
        buttonEmailToDudeTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Action + "." + RemoteConfigDictionary.EmailToTitle,
        buttonDirectMessageToCreatorTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Action + "." + RemoteConfigDictionary.DirectMessageToCreatorTitle,
        buttonSendEmailTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Action + "." + RemoteConfigDictionary.SendEmailTitle,
        writeYourMessageTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigDictionary.DudeProfileDetailWriteYourMessageTitle,
        buttonContactThisDudette: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Action + "." + RemoteConfigDictionary.ContactThisDudette,
        buttonPublishMyProfileTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Action + "." + RemoteConfigDictionary.PublishMyProfileTitle,
        buttonLaterTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Action + "." + RemoteConfigDictionary.LaterTitle,
        buttonPublishTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Action + "." + RemoteConfigDictionary.PublishTitle,
        buttonUnPublishTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Action + "." + RemoteConfigDictionary.UnPublishTitle,
        buttonAddTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Action + "." + RemoteConfigDictionary.AddTitle,
        addCommentPlaceholder: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigDictionary.AddCommentPlaceholder,
        buttonThumbsUpTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Action + "." + RemoteConfigDictionary.ThumbsUpTitle,
        buttonThumbsDownTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Action + "." + RemoteConfigDictionary.ThumbsDownTitle,
    }

    static UserStatus = {
        publishStatusTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.UserStatus + "." + RemoteConfigDictionary.PublishedStatusTitle,
        unPublishStatusTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.UserStatus + "." + RemoteConfigDictionary.UnPublishedStatusTitle,
        hightLightStatusTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.UserStatus + "." + RemoteConfigDictionary.HightLightStatusTitle,
        deletedStatusTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.UserStatus + "." + RemoteConfigDictionary.DeletedStatusTitle,
        approvedStatusTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.UserStatus + "." + RemoteConfigDictionary.ApprovedStatusTitle,
        waitingForApprovalStatusTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.UserStatus + "." + RemoteConfigDictionary.WaitingForApprovalStatusTitle,
        waitingForPublishStatusTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.UserStatus + "." + RemoteConfigDictionary.WaitingForPublishingStatusTitle,
        requestForEditingStatusTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.UserStatus + "." + RemoteConfigDictionary.RequestForEditingStatusTitle,
    }

    static Notification = {
        approveCreateDude: RemoteConfigDictionary.ApproveCreateDude,
        publishedDude: RemoteConfigDictionary.PublishedDude,
        commentDude: RemoteConfigDictionary.CommentDude,
        reportCommentDude: RemoteConfigDictionary.ReportCommentDude
    }

    static SeekingGuyStatus = {
        notSeekingGuyStatusTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.SeekingGuyStatus + "." + RemoteConfigDictionary.NotSeekingGuy,
        seekingGuyStatusTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.SeekingGuyStatus + "." + RemoteConfigDictionary.SeekingGuy
    }


    static ErrorMessage = {
        fullnameEmptyTitle: GeneralResources.SharedKeys.generalErrorMessage + "." + RemoteConfigDictionary.FullNameEmptyTitle,
        emailEmptyTitle: GeneralResources.SharedKeys.generalErrorMessage + "." + RemoteConfigDictionary.EmailEmptyTitle,
        emailInvalidTitle: GeneralResources.SharedKeys.generalErrorMessage + "." + RemoteConfigDictionary.EmailInvalidTitle,
        passwordEmptyTitle: GeneralResources.SharedKeys.generalErrorMessage + "." + RemoteConfigDictionary.PasswordEmptyTitle,
        passwordInvalidTitle: GeneralResources.SharedKeys.generalErrorMessage + "." + RemoteConfigDictionary.PasswordInvalidTitle,
        confirmPasswordInvalidTitle: GeneralResources.SharedKeys.generalErrorMessage + "." + RemoteConfigDictionary.ConfirmPasswordInvalidTitle,
        verifyCodeEmptyTitle: GeneralResources.SharedKeys.generalErrorMessage + "." + RemoteConfigDictionary.VerifyCodeEmptyTitle,
        verifyCodeIsExpiredTitle: GeneralResources.SharedKeys.generalErrorMessage + "." + RemoteConfigDictionary.VerifyCodeIsExpiredTitle,
        verifyCodeInvalidTitle: GeneralResources.SharedKeys.generalErrorMessage + "." + RemoteConfigDictionary.VerifyCodeInvalidTitle,
        verifyCodeFailedTitle: GeneralResources.SharedKeys.generalErrorMessage + "." + RemoteConfigDictionary.VerifyCodeFailedTitle,
        ageEmptyTitle: GeneralResources.SharedKeys.generalErrorMessage + "." + RemoteConfigDictionary.AgeEmptyTitle,
        countryEmptyTitle: GeneralResources.SharedKeys.generalErrorMessage + "." + RemoteConfigDictionary.CountryEmptyTitle,
        cityEmptyTitle: GeneralResources.SharedKeys.generalErrorMessage + "." + RemoteConfigDictionary.CityEmptyTitle,
        tagListInvalidTitle: GeneralResources.SharedKeys.generalErrorMessage + "." + RemoteConfigDictionary.TagListInvalidTitle,
        profileImageEmptyTitle: GeneralResources.SharedKeys.generalErrorMessage + "." + RemoteConfigDictionary.ProfileImageEmptyTitle,
        galleryImageEmptyTitle: GeneralResources.SharedKeys.generalErrorMessage + "." + RemoteConfigDictionary.GalleryImageEmptyTitle,
        interestListEmptyTitle: GeneralResources.SharedKeys.generalErrorMessage + "." + RemoteConfigDictionary.InterestListEmptyTitle,
        havdSogerHanEmptyTitle: GeneralResources.SharedKeys.generalErrorMessage + "." + RemoteConfigDictionary.HavdSogerHanListEmptyTitle,
        verifyNEMIDTitle: GeneralResources.SharedKeys.NEMIDErrorMessage + "." + RemoteConfigDictionary.NEMIDAlertTitle,
        verifyNEMID: GeneralResources.SharedKeys.NEMIDErrorMessage + "." + RemoteConfigDictionary.NEMIDVerifyError,
        grantPermissionTile: GeneralResources.SharedKeys.generalErrorMessage + "." + RemoteConfigDictionary.GrantPermissionTile,
        grantPermission: GeneralResources.SharedKeys.generalErrorMessage + "." + RemoteConfigDictionary.GrantPermission,
        somethingWentWrong: GeneralResources.SharedKeys.generalErrorMessage + "." + RemoteConfigDictionary.SomethingWentWrong,
        accessUnpublishedDudeProfile: GeneralResources.SharedKeys.generalErrorMessage + "." + RemoteConfigDictionary.AccessUnpublishedDudeProfile
    }

    static Modal = {
        modalCountryPlaceholder: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Modal + "." + RemoteConfigDictionary.ModalCountryPlaceholder,
        modalAgePlaceholder: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Modal + "." + RemoteConfigDictionary.ModalAgePlaceholder,
        modalCityPlaceholder: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Modal + "." + RemoteConfigDictionary.ModalCityPlaceholder,
        modalHavdSogerHanPlaceholder: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Modal + "." + RemoteConfigDictionary.ModalHavdSogerHanPlaceholder,
        modalInterestPlaceholder: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Modal + "." + RemoteConfigDictionary.ModalInterestPlaceholder,
        modalRegionPlaceholder: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Modal + "." + RemoteConfigDictionary.ModalRegionPlaceholder,
        modalConfirmDeleteDude: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Modal + "." + RemoteConfigDictionary.ModalConfirmDeleteDude,
        modalConfirmPublishProfile: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Modal + "." + RemoteConfigDictionary.ModalConfirmPublishProfile,
        modalConfirmDeleteChatThread: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Modal + "." + RemoteConfigDictionary.ModalConfirmDeleteChatThread,
        modalConfirmDeleteMessage: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Modal + "." + RemoteConfigDictionary.ModalConfirmDeleteMessage,
        modalConfirmChangeStatusDude: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Modal + "." + RemoteConfigDictionary.ModalConfirmChangeStatusDude,
        modalConfirmDeleteUser: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Modal + "." + RemoteConfigDictionary.ModalConfirmDeleteUser,
        modalConfirmSubscribeServices: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Modal + "." + RemoteConfigDictionary.ModalConfirmSubscribeServices,
        modalConfirmHilightDude: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Modal + "." + RemoteConfigDictionary.ModalConfirmHilightDude,
    }

    static Alert = {
        alertEmailSentTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Alert + "." + RemoteConfigDictionary.AlertEmailSentTitle,
        alertDeleteProfileImageTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Alert + "." + RemoteConfigDictionary.AlertDeleteProileImageTitle,
        alertDeleteProfileImageMessage: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Alert + "." + RemoteConfigDictionary.AlertDeleteProileImageMessage,
        alertDeleteGalleryImageTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Alert + "." + RemoteConfigDictionary.AlertDeleteImageInGalleryTitle,
        alertDeleteGalleryImageMessage: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Alert + "." + RemoteConfigDictionary.AlertDeleteImageInGalleryMessage,
        alertValidateUserTitle: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Alert + "." + RemoteConfigDictionary.AlertValidateUserTitle,
        alertValidateUserMessage: RemoteConfigGroupedKey.SharedKeys + "." + RemoteConfigGroupedKey.Alert + "." + RemoteConfigDictionary.AlertValidateUserMessage,
    }

    static WelcomeScreen = {
        buttonCreateAccountTitle: RemoteConfigDictionary.ButtonCreateAccountTitle,
        buttonLoginTitle: GeneralResources.SharedKeys.buttonLoginTitle,
        contactSupportTitle: GeneralResources.SharedKeys.contactSupportTitle
    }

    static CreateAccountScreen = {
        title: GeneralResources.SharedKeys.title,
        fullNameTitle: RemoteConfigDictionary.FullnameTitle,
        fullNamePlaceholder: GeneralResources.SharedKeys.fullNamePlaceholder,
        emailPlaceholder: GeneralResources.SharedKeys.emailPlaceholder,
        countryPlaceholder: RemoteConfigDictionary.CountryPlaceholder,
        ageTitle: RemoteConfigDictionary.AgeTitle,
        agePlaceholder: RemoteConfigDictionary.AgePlaceholder,
        passwordTitle: RemoteConfigDictionary.PasswordTitle,
        passwordPlaceholder: GeneralResources.SharedKeys.passwordPlaceholder,
        passwordDescription: RemoteConfigDictionary.PasswordDescription,
        confirmPasswordTitle: RemoteConfigDictionary.ConfirmPasswordTitle,
        confirmPasswordPlaceholder: RemoteConfigDictionary.ConfirmPasswordPlaceholder,
        buttonNextTitle: GeneralResources.SharedKeys.buttonNextTitle,
        alreadyHaveAnAccountTitle: RemoteConfigDictionary.AlreadyHaveAnAccountTitle,
        modalCountryPlaceholder: GeneralResources.Modal.modalCountryPlaceholder,
        modalAgePlaceholder: GeneralResources.Modal.modalAgePlaceholder,
        modalButtonCancelTitle: GeneralResources.SharedKeys.buttonCancelTitle,
        keyboardButtonNextTitle: GeneralResources.SharedKeys.buttonNextTitle,
        keyboardButtonSubmitTitle: GeneralResources.SharedKeys.buttonSubmitTitle
    }

    static LoginScreen = {
        title: GeneralResources.SharedKeys.title,
        subTitle: GeneralResources.SharedKeys.subTitle,
        emailPlaceholder: GeneralResources.SharedKeys.emailPlaceholder,
        passwordPlaceholder: GeneralResources.SharedKeys.passwordPlaceholder,
        buttonLoginTitle: GeneralResources.SharedKeys.buttonLoginTitle,
        forgotPasswordTitle: RemoteConfigDictionary.ForgotPasswordTitle,
        createNewAccountTitle: RemoteConfigDictionary.CreateNewAccountTitle,
        keyboardButtonNextTitle: GeneralResources.SharedKeys.buttonNextTitle,
        keyboardButtonSubmitTitle: GeneralResources.SharedKeys.buttonSubmitTitle
    }

    static VerifyUserScreen = {
        title: GeneralResources.SharedKeys.title,
        subTitle: GeneralResources.SharedKeys.subTitle,
        verifyCodePlaceholder: RemoteConfigDictionary.CodePlaceholder,
        buttonSubmitCodeTitle: GeneralResources.SharedKeys.buttonSubmitCodeTitle,
        buttonResendVerifyCodeTitle: GeneralResources.SharedKeys.buttonResendVerifyCodeTitle,
        verifyCodeSendToEmail: GeneralResources.SharedKeys.verifyCodeSendToEmail
    }

    static ForgotPasswordScreen = {
        title: GeneralResources.SharedKeys.title,
        subTitle: GeneralResources.SharedKeys.subTitle,
        emailPlaceholder: GeneralResources.SharedKeys.emailPlaceholder,
        alertEmailSentTitle: GeneralResources.Alert.alertEmailSentTitle,
        buttonSubmitTitle: GeneralResources.SharedKeys.buttonSubmitTitle
    }

    static TermsAndConditionScreen = {
        title: GeneralResources.SharedKeys.title,
        subTitle: GeneralResources.SharedKeys.subTitle,
        description: GeneralResources.SharedKeys.description,
        buttonAcceptTitle: GeneralResources.SharedKeys.buttonAcceptTitle,
        privacyTitle: RemoteConfigDictionary.PrivacyTitle,
        privacyLinkTitle: RemoteConfigDictionary.PrivacyLinkTitle
    }

    static HomeScreen = {
        highlightTitle: RemoteConfigDictionary.HighLightTitle,
        homeMenuTitle: RemoteConfigDictionary.HomeMenuTitle,
        searchDudeMenuTitle: RemoteConfigGroupedKey.MenuTitle + "." + RemoteConfigDictionary.SearchDudeMenuTitle,
        createDudeMenuTitle: RemoteConfigGroupedKey.MenuTitle + "." + RemoteConfigDictionary.CreateDudeMenuTitle,
        recommendedMySelfMenuTitle: RemoteConfigGroupedKey.MenuTitle + "." + RemoteConfigDictionary.RecommendedMySelfMenuTitle,
        helpDudetteMenuTitle: RemoteConfigGroupedKey.MenuTitle + "." + RemoteConfigDictionary.HelpDudetteMenuTitle
    }

    static AcceptanceFromDudeScreen = {
        title: GeneralResources.SharedKeys.title,
        subTitle: GeneralResources.SharedKeys.subTitle,
        dudeEmailPlaceholder: GeneralResources.SharedKeys.emailPlaceholder,
        dudeFullNamePlaceholder: GeneralResources.SharedKeys.fullNamePlaceholder,
        buttonContinueTitle: GeneralResources.SharedKeys.buttonContinueTitle,
        keyboardButtonNextTitle: GeneralResources.SharedKeys.buttonNextTitle,
        keyboardButtonSubmitTitle: GeneralResources.SharedKeys.buttonSubmitTitle,
    }

    static WaitingDudeApproveScreen = {
        successTitle: GeneralResources.SharedKeys.title,
        successSubTitle: GeneralResources.SharedKeys.subTitle,
        successDescription: GeneralResources.SharedKeys.description,
    }

    static SideMenuBar = {
        homeSideMenuBarTitle: RemoteConfigDictionary.HomeSideMenuBarTitle,
        manageProfileSideMenuBarTitle: RemoteConfigDictionary.ManageProfileSideMenuBarTitle,
        createDudeSideMenuBarTitle: RemoteConfigDictionary.CreateDudeSideMenuBarTitle,
        activitySideMenuBarTitle: RemoteConfigDictionary.ActivitySideMenuBarTitle,
        feedSideMenuBarTitle: RemoteConfigDictionary.FeedSideMenuBarTitle,
        indberetningSideMenuBarTitle: RemoteConfigDictionary.IndberetningSideMenuBarTitle,
        sosSideMenuBarTitle: RemoteConfigDictionary.SOSSideMenuBarTitle,
        menuSideMenuBarTitle: RemoteConfigDictionary.MenuSideMenuBarTitle,
        searchDudeSideMenuBarTitle: RemoteConfigDictionary.SearchDudeSideMenuBarTitle,
        helpDudetteSideMenuBarTitle: RemoteConfigDictionary.HelpDudetteSideMenuBarTitle,
        favouriteDudeSideMenuBarTitle: RemoteConfigDictionary.FavDudeSideMenuBarTitle,
        inboxSideMenuBarTitle: RemoteConfigDictionary.InboxSideMenuBarTitle,
        faqSideMenuBarTitle: RemoteConfigDictionary.FaqSideMenuBarTitle,
        contactSideMenuBarTitle: RemoteConfigDictionary.ContactSideMenuBarTitle,
        logoutSideMenuBarTitle: RemoteConfigDictionary.LogoutSideMenuBarTitle,
    }

    static ManageProfileTabScreen = {
        myProfileTabMenuTitle: RemoteConfigDictionary.MyProfileTabMenuTitle,
        myDudeProfileTabMenuTitle: RemoteConfigDictionary.MyDudeProfileTabMenuTitle,
        buttonAddTitle: GeneralResources.SharedKeys.buttonAddTitle
    }

    static MyProfileScreen = {
        subscriptionTitle: RemoteConfigDictionary.SubscriptionTitle,
        statusTitle: GeneralResources.SharedKeys.statusTitle,
        expireDateTitle: GeneralResources.SharedKeys.expireDateTitle,
        subscriptionActiveStatusTitle: GeneralResources.SharedKeys.subscriptionActiveStatusTitle,
        subscriptionDeactiveStatusTitle: GeneralResources.SharedKeys.subscriptionDeactiveStatusTitle,
        dropdownMakeSubscriptionTitle: GeneralResources.SharedKeys.makeSubscriptionTitle,
        dropdownCancelSubscriptionTitle: GeneralResources.SharedKeys.cancelSubscriptionTitle,
        updateDudetteProfileTitle: GeneralResources.SharedKeys.updateDudetteProfileTitle,
        publishMyProfileTitle: GeneralResources.SharedKeys.buttonPublishMyProfileTitle,
        modalButtonCancelTitle: GeneralResources.SharedKeys.buttonLaterTitle,
        modalButtonOKTitle: GeneralResources.SharedKeys.buttonPublishTitle,
        modalConfirmPublishProfile: GeneralResources.Modal.modalConfirmPublishProfile,
        alertValidateUserTitle: GeneralResources.Alert.alertValidateUserTitle,
        alertValidateUserMessage: GeneralResources.Alert.alertValidateUserMessage
    }

    static MyDudeProfileScreen = {
        publishStatusTitle: GeneralResources.UserStatus.publishStatusTitle,
        unPublishStatusTitle: GeneralResources.UserStatus.unPublishStatusTitle,
        hightLightStatusTitle: GeneralResources.UserStatus.hightLightStatusTitle,
        expireInTitle: GeneralResources.SharedKeys.expireInTitle,
        deleteTitle: GeneralResources.SharedKeys.deleteTitle,
        updateDudeProfileTitle: GeneralResources.SharedKeys.updateDudeProfileTitle,
        modalButtonOKTitle: GeneralResources.SharedKeys.buttonOKTitle,
        modalButtonCancelTitle: GeneralResources.SharedKeys.buttonCancelTitle,
        modalConfirmDeleteDude: GeneralResources.Modal.modalConfirmDeleteDude,
    }

    static CreateEditProfileScreen = {
        alertValidateUserTitle: GeneralResources.Alert.alertValidateUserTitle,
        alertValidateUserMessage: GeneralResources.Alert.alertValidateUserMessage
    }

    static PrimaryInformationFirstScreen = {
        statusTitle: GeneralResources.SharedKeys.statusTitle,
        fullNamePlaceholder: GeneralResources.SharedKeys.fullNamePlaceholder,
        emailPlaceholder: GeneralResources.SharedKeys.emailPlaceholder,
        agePlaceholder: RemoteConfigDictionary.AgePlaceholder,
        cityPlaceholder: RemoteConfigDictionary.CityPlaceholder,
        interestPlaceholder: RemoteConfigDictionary.InterestPlaceholder,
        havdSogerHanPlaceholder: RemoteConfigDictionary.HavdSogerHanPlaceholder,
        tagTitle: RemoteConfigDictionary.TagTitle,
        modalButtonCancelTitle: GeneralResources.SharedKeys.buttonCancelTitle,
        modalCityPlaceholder: GeneralResources.Modal.modalCityPlaceholder,
        modalAgePlaceholder: GeneralResources.Modal.modalAgePlaceholder,
        modalHavdSogerHandPlaceholder: GeneralResources.Modal.modalHavdSogerHanPlaceholder,
        buttonActivateTitle: RemoteConfigDictionary.ButtonActivateTitle,
        buttonDeactivateTitle: RemoteConfigDictionary.ButtonDeactivateTitle,
        buttonDeleteTitle: GeneralResources.SharedKeys.deleteTitle,
        modalButtonClearTitle: GeneralResources.SharedKeys.buttonClearTitle,
        modalButtonOKTitle: GeneralResources.SharedKeys.buttonOKTitle,
        buttonSaveAndContinueTitle: GeneralResources.SharedKeys.buttonSaveAndContinueTitle,
        modalInterestPlaceholder: GeneralResources.Modal.modalInterestPlaceholder,
        modalConfirmDeleteDude: GeneralResources.Modal.modalConfirmDeleteDude,
        modalConfirmChangeStatusDude: GeneralResources.Modal.modalConfirmChangeStatusDude
    }

    static PrimaryInformationSecondScreen = {
        fullDescriptionTitle: RemoteConfigDictionary.FullDescriptionTitle,
        buttonSaveAndContinueTitle: GeneralResources.SharedKeys.buttonSaveAndContinueTitle,
    }

    static NotificationScreen = {
        screenTitle: RemoteConfigDictionary.Title
    }
    static PrimaryInformationThirdScreen = {
        uploadProfilePhotoTitle: GeneralResources.SharedKeys.uploadProfilePhoto,
        updatePhotoToGalleryTitle: GeneralResources.SharedKeys.uploadPhotoToGallery,
        selectPhoto: GeneralResources.SharedKeys.selectPhoto,
        buttonSaveAndContinueTitle: GeneralResources.SharedKeys.buttonSaveAndContinueTitle,
        alertDeleteProfileImageTitle: GeneralResources.Alert.alertDeleteProfileImageTitle,
        alertDeleteProfileImageMessage: GeneralResources.Alert.alertDeleteProfileImageMessage,
        alertDeleteGalleryImageTitle: GeneralResources.Alert.alertDeleteGalleryImageTitle,
        alertDeleteGalleryImageMessage: GeneralResources.Alert.alertDeleteGalleryImageMessage,
        grantPermissionTitle: GeneralResources.ErrorMessage.grantPermissionTile,
        grantPermission: GeneralResources.ErrorMessage.grantPermission
    }
    static PrimaryInformationFourthScreen = {
        title: RemoteConfigDictionary.HighlightThisDudeQuestionTitle,
        description: RemoteConfigDictionary.HighlightThisDudeDescription,
        yesAnswer: RemoteConfigDictionary.HighlightThisDudeYesAnswerTitle,
        noAnswer: RemoteConfigDictionary.HighlightThisDudeNoAnswerTitle,
        buttonSubmitTitle: GeneralResources.SharedKeys.buttonSubmitTitle,
        modalButtonCancelTitle: GeneralResources.SharedKeys.buttonCancelTitle,
        buttonDeleteTitle: GeneralResources.SharedKeys.deleteTitle,
        modalButtonClearTitle: GeneralResources.SharedKeys.buttonClearTitle,
        modalButtonOKTitle: GeneralResources.SharedKeys.buttonOKTitle,
        modalConfirmHilightDude: GeneralResources.Modal.modalConfirmHilightDude
    }

    static WaitingDudePublishProfileScreen = {
        title: GeneralResources.SharedKeys.title,
        description: GeneralResources.SharedKeys.description,
        buttonGotoManageProfile: GeneralResources.SharedKeys.buttonGotoManageProfileTitle,
    }

    static SearchList = {
        searchDudeTitle: RemoteConfigDictionary.SearchDudeTitle,
        helpDudetteTitle: RemoteConfigDictionary.HelpDudetteTitle,
        buttonFiltersMenuTitle: GeneralResources.SharedKeys.buttonFiltersTitle,
        accessUnpublishedDudeProfile: GeneralResources.ErrorMessage.accessUnpublishedDudeProfile
    }

    static Filters = {
        screenTitle: RemoteConfigDictionary.Title,
        ageTitle: RemoteConfigDictionary.AgeTitle,
        regionTitle: RemoteConfigDictionary.RegionTitle,
        havdSogerHanTitle: RemoteConfigDictionary.HavdSogerHanTitle,
        interestTitle: RemoteConfigDictionary.InterestTitle,
        tagTitle: RemoteConfigDictionary.TagsTitle,
        buttonSelectTitle: GeneralResources.SharedKeys.buttonSelectTitle,
        buttonSelectAllTitle: GeneralResources.SharedKeys.buttonSelectAllTitle,
        buttonSaveAndContinueTitle: GeneralResources.SharedKeys.buttonSaveAndContinueTitle,
        modalButtonClearTitle: GeneralResources.SharedKeys.buttonClearTitle,
        modalButtonOKTitle: GeneralResources.SharedKeys.buttonOKTitle,
        modalButtonCancelTitle: GeneralResources.SharedKeys.buttonCancelTitle,
        modalInterestPlaceholder: GeneralResources.Modal.modalInterestPlaceholder,
        modalRegionPlaceholder: GeneralResources.Modal.modalRegionPlaceholder,
        seekingGuyTitle: RemoteConfigDictionary.FilterSeekingGuyTitle
    }

    static DudeProfileDetailScreen = {
        enterTextPlaceholder: GeneralResources.SharedKeys.addCommentPlaceholder,
        thumbsUpTitle: GeneralResources.SharedKeys.buttonThumbsUpTitle,
        thumbsDownTitle: GeneralResources.SharedKeys.buttonThumbsDownTitle
    }


    static DudeProfileFirstScreen = {

    }

    static DudeProfileSecondScreen = {
        interestTitle: RemoteConfigDictionary.InterestTitle,
        havdSogerHanTitle: RemoteConfigDictionary.SeekingTitle,
        detailTitle: RemoteConfigDictionary.DetailTitle,
    }

    static DudeProfileFourthScreen = {
        commentTitle: RemoteConfigDictionary.CommentTitle,
        menuDescription: RemoteConfigDictionary.DudeProfileDetailMenuDescription,
        enterTextPlaceholder: GeneralResources.SharedKeys.addCommentPlaceholder,
        buttonEmailToDudeTitle: GeneralResources.SharedKeys.buttonEmailToDudeTitle,
        buttonDirectMessageToCreatorTitle: GeneralResources.SharedKeys.buttonDirectMessageToCreatorTitle,
        buttonSubmitTitle: GeneralResources.SharedKeys.buttonSubmitTitle
    }

    static DudeProfileDetailSendEmailScreen = {
        buttonSendEmailTitle: GeneralResources.SharedKeys.buttonSendEmailTitle,
        sendEmailDescription: RemoteConfigDictionary.DudeProfileDetailSendEmailDescription,
        writeYourMessageTitle: GeneralResources.SharedKeys.writeYourMessageTitle
    }

    static EmailSendToUserSuccessScreen = {
        title: GeneralResources.SharedKeys.title,
        description: GeneralResources.SharedKeys.description
    }

    static DudetteProfileDetailScreen = {
        interestTitle: RemoteConfigDictionary.InterestTitle,
        havdSogerHanTitle: RemoteConfigDictionary.SeekingTitle,
        detailTitle: RemoteConfigDictionary.DetailTitle,
        contactThisDudetteTitle: GeneralResources.SharedKeys.buttonContactThisDudette,
        tagTitle: RemoteConfigDictionary.TagTitle,
    }

    static UserProfileDetailTabScreen = {
        modalButtonCancelTitle: GeneralResources.SharedKeys.buttonLaterTitle,
        modalButtonOKTitle: GeneralResources.SharedKeys.buttonPublishTitle,
        modalConfirmPublishProfile: GeneralResources.Modal.modalConfirmPublishProfile,
        alertValidateUserTitle: GeneralResources.Alert.alertValidateUserTitle,
        alertValidateUserMessage: GeneralResources.Alert.alertValidateUserMessage
    }

    static UserProfileDetailFirstScreen = {
        statusTitle: GeneralResources.SharedKeys.statusTitle,
        fullNamePlaceholder: GeneralResources.SharedKeys.fullNamePlaceholder,
        emailPlaceholder: GeneralResources.SharedKeys.emailPlaceholder,
        agePlaceholder: RemoteConfigDictionary.AgePlaceholder,
        cityPlaceholder: RemoteConfigDictionary.CityPlaceholder,
        interestPlaceholder: RemoteConfigDictionary.InterestPlaceholder,
        havdSogerHanPlaceholder: RemoteConfigDictionary.HavdSogerHanPlaceholder,
        tagTitle: RemoteConfigDictionary.TagTitle,
        modalButtonCancelTitle: GeneralResources.SharedKeys.buttonCancelTitle,
        modalCityPlaceholder: GeneralResources.Modal.modalCityPlaceholder,
        modalAgePlaceholder: GeneralResources.Modal.modalAgePlaceholder,
        modalHavdSogerHandPlaceholder: GeneralResources.Modal.modalHavdSogerHanPlaceholder,
        buttonActivateTitle: RemoteConfigDictionary.ButtonActivateTitle,
        buttonDeactivateTitle: RemoteConfigDictionary.ButtonDeactivateTitle,
        buttonDeleteTitle: GeneralResources.SharedKeys.deleteTitle,
        modalButtonClearTitle: GeneralResources.SharedKeys.buttonClearTitle,
        modalButtonOKTitle: GeneralResources.SharedKeys.buttonOKTitle,
        buttonSaveAndContinueTitle: GeneralResources.SharedKeys.buttonSaveAndContinueTitle,
        modalInterestPlaceholder: GeneralResources.Modal.modalInterestPlaceholder,
        buttonPublish: GeneralResources.SharedKeys.buttonPublishTitle,
        buttonUnPublish: GeneralResources.SharedKeys.buttonUnPublishTitle,
        modalConfirmDeleteUser: GeneralResources.Modal.modalConfirmDeleteUser,
    }
    static UserProfileDetailSecondScreen = {
        fullDescriptionTitle: RemoteConfigDictionary.FullDescriptionTitle,
        buttonSaveAndContinueTitle: GeneralResources.SharedKeys.buttonSaveAndContinueTitle,
    }
    static UserProfileDetailThirdScreen = {
        uploadProfilePhotoTitle: GeneralResources.SharedKeys.uploadProfilePhoto,
        updatePhotoToGalleryTitle: GeneralResources.SharedKeys.uploadPhotoToGallery,
        selectPhoto: GeneralResources.SharedKeys.selectPhoto,
        buttonSaveAndContinueTitle: GeneralResources.SharedKeys.buttonSaveAndContinueTitle,
        alertDeleteProfileImageTitle: GeneralResources.Alert.alertDeleteProfileImageTitle,
        alertDeleteProfileImageMessage: GeneralResources.Alert.alertDeleteProfileImageMessage,
        alertDeleteGalleryImageTitle: GeneralResources.Alert.alertDeleteGalleryImageTitle,
        alertDeleteGalleryImageMessage: GeneralResources.Alert.alertDeleteGalleryImageMessage,
        grantPermissionTitle: GeneralResources.ErrorMessage.grantPermissionTile,
        grantPermission: GeneralResources.ErrorMessage.grantPermission,
    }
    static FavouriteListTabScreen = {
        favouriteDudesTabMenuTitle: RemoteConfigDictionary.FavouriteDudesTabMenuTitle,
        favouriteDudettesTabMenuTitle: RemoteConfigDictionary.FavouriteDudettesTabMenuTitle,
    }
    static ChatListScreen = {
        title: RemoteConfigDictionary.Title,
        modalButtonOKTitle: GeneralResources.SharedKeys.buttonOKTitle,
        buttonDeleteTitle: GeneralResources.SharedKeys.deleteTitle,
        buttonCancelTitle: GeneralResources.SharedKeys.buttonCancelTitle,
        modalConfirmDeleteChatThread: GeneralResources.Modal.modalConfirmDeleteChatThread
    }
    static ChatRoomScreen = {
        enterTextPlaceholder: GeneralResources.SharedKeys.enterTextPlaceholder,
        modalButtonOKTitle: GeneralResources.SharedKeys.buttonOKTitle,
        buttonDeleteTitle: GeneralResources.SharedKeys.deleteTitle,
        buttonCancelTitle: GeneralResources.SharedKeys.buttonCancelTitle,
        modalConfirmDeleteMessage: GeneralResources.Modal.modalConfirmDeleteMessage
    }
    static SubscriptionConfirmationScreen = {
        modalButtonSubscribeTitle: GeneralResources.SharedKeys.buttonSubscribeTitle,
        modalButtonCancelTitle: GeneralResources.SharedKeys.buttonCancelTitle,
        modalConfirmSubscribeServices: GeneralResources.Modal.modalConfirmSubscribeServices,
    }

    static DudeProfileDetailFirst = {
        tagTitle: RemoteConfigDictionary.TagTitle
    }
}