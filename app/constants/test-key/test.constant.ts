import { TestIDConfigKey } from "./test.keys"

export class TestIDResources {
    static WelcomeScreen = {
        buttonCreateAccount: TestIDConfigKey.btnCreateAccount,
        buttonLogin: TestIDConfigKey.btnGoToLogin,
        buttonContactSupport: TestIDConfigKey.btnGoToContactSupport
    }

    static IntroductionScreen = {
        image: TestIDConfigKey.image,
        title: TestIDConfigKey.title,
        description: TestIDConfigKey.description
    }
    static CreateAccountScreen = {
        title: TestIDConfigKey.title,
        fullNamePlaceholder: TestIDConfigKey.inputFullName,
        emailPlaceholder: TestIDConfigKey.inputEmail,
        countryPlaceholder: TestIDConfigKey.ddlCountry,
        agePlaceholder: TestIDConfigKey.ddlAge,
        passwordPlaceholder: TestIDConfigKey.inputPassword,
        passwordDescription: TestIDConfigKey.description,
        confirmPasswordPlaceholder: TestIDConfigKey.inputConfirmPassword,
        alreadyHaveAnAccount: TestIDConfigKey.btnBackToLogin,
        buttonNext: TestIDConfigKey.btnNext,
        modalPicker: TestIDConfigKey.modalPicker,
        errorMessage: TestIDConfigKey.errorMessage,
        description: TestIDConfigKey.description
    }

    static LoginScreen = {
        title: TestIDConfigKey.title,
        subTitle: TestIDConfigKey.subTitle,
        emailPlaceholder: TestIDConfigKey.inputEmail,
        passwordPlaceholder: TestIDConfigKey.inputPassword,
        buttonLogin: TestIDConfigKey.btnBackToLogin,
        forgotPassword: TestIDConfigKey.btnForgotPassword,
        createNewAccount: TestIDConfigKey.btnCreateAccount,
        errorMessage: TestIDConfigKey.errorMessage
    }

    static VerifyUserScreen = {
        title: TestIDConfigKey.title,
        subTitle: TestIDConfigKey.subTitle,
        verifyCodePlaceholder: TestIDConfigKey.inputVerifyCode,
        emailExample: TestIDConfigKey.emailExample,
        buttonSubmitCode: TestIDConfigKey.btnSubmit,
        errorMessage: TestIDConfigKey.errorMessage,
        buttonResendCode: TestIDConfigKey.btnResendCode
    }

    static ForgotPasswordScreen = {
        title: TestIDConfigKey.title,
        subTitle: TestIDConfigKey.subTitle,
        emailPlaceholder: TestIDConfigKey.inputEmail,
        buttonSubmit: TestIDConfigKey.btnSubmit,
        errorMessage: TestIDConfigKey.errorMessage
    }

    static TermsAndConditionScreen = {
        title: TestIDConfigKey.title,
        subTitle: TestIDConfigKey.subTitle,
        description: TestIDConfigKey.description,
        buttonAccept: TestIDConfigKey.btnAccept,
        privacyTitle: TestIDConfigKey.lblPrivacyTitle,
        privacyLinkTitle: TestIDConfigKey.lblPrivacyLinkTitle
    }

    static Home = {
        highlightTitle: TestIDConfigKey.highlightTitle,
        homeMenuTitle: TestIDConfigKey.homeMenuTitle,
        bottonSearchDude: TestIDConfigKey.btnSearchDude,
        bottonCreateDude: TestIDConfigKey.btnCreateDude,
        bottonRecommendedMySelf: TestIDConfigKey.btnRecommendedMySelf,
        bottonHelpDudette: TestIDConfigKey.btnHelpDudette,
        highlightDudeList: TestIDConfigKey.highlightDudeList
    }

    static CreateDudeScreen = {
        title: TestIDConfigKey.title,
        subTitle: TestIDConfigKey.subTitle,
        emailPlaceholder: TestIDConfigKey.inputEmail,
        fullNamePlaceholder: TestIDConfigKey.inputFullName,
        buttonContinue: TestIDConfigKey.btnContinue,
        errorMessage: TestIDConfigKey.errorMessage
    }
    static ActivityScreen = {
        title: TestIDConfigKey.title,
        subTitle: TestIDConfigKey.subTitle,
        emailPlaceholder: TestIDConfigKey.inputEmail,
        fullNamePlaceholder: TestIDConfigKey.inputFullName,
        buttonContinue: TestIDConfigKey.btnContinue,
        errorMessage: TestIDConfigKey.errorMessage
    }

    static FeedScreen = {
        title: TestIDConfigKey.title,
        subTitle: TestIDConfigKey.subTitle,
        emailPlaceholder: TestIDConfigKey.inputEmail,
        fullNamePlaceholder: TestIDConfigKey.inputFullName,
        buttonContinue: TestIDConfigKey.btnContinue,
        errorMessage: TestIDConfigKey.errorMessage
    }

    static SOSScreen = {
        title: TestIDConfigKey.title,
        subTitle: TestIDConfigKey.subTitle,
        emailPlaceholder: TestIDConfigKey.inputEmail,
        fullNamePlaceholder: TestIDConfigKey.inputFullName,
        buttonContinue: TestIDConfigKey.btnContinue,
        errorMessage: TestIDConfigKey.errorMessage
    }
    static IndberetningScreen = {
        title: TestIDConfigKey.title,
        subTitle: TestIDConfigKey.subTitle,
        emailPlaceholder: TestIDConfigKey.inputEmail,
        fullNamePlaceholder: TestIDConfigKey.inputFullName,
        buttonContinue: TestIDConfigKey.btnContinue,
        errorMessage: TestIDConfigKey.errorMessage
    }
    
    static WaitingDudeApproveScreen = {
        title: TestIDConfigKey.title,
        subTitle: TestIDConfigKey.subTitle,
        description: TestIDConfigKey.description,
        gotoManageProfile: TestIDConfigKey.description
    }

    static SideMenuBar = {
        homeSideMenuBar: TestIDConfigKey.homeSideMenu,
        manageProfileSideMenuBar: TestIDConfigKey.manageProfileSideMenu,
        createDudeSideMenuBar: TestIDConfigKey.createDudeSideMenuBar,
        activitySideMenuBar: TestIDConfigKey.activitySideMenuBar,
        feedSideMenuBar: TestIDConfigKey.feedSideMenuBar,
        indberetnigSideMenuBar: TestIDConfigKey.indberetnigSideMenuBar,
        menuSideMenuBar: TestIDConfigKey.indberetnigSideMenuBar,
        sosSideMenuBar: TestIDConfigKey.indberetnigSideMenuBar,
        searchDudeSideMenuBar: TestIDConfigKey.searchDudeSideMenuBar,
        helpDudetteSideMenuBar: TestIDConfigKey.helpDudetteSideMenuBar,
        favouriteDudeSideMenuBar: TestIDConfigKey.favouriteDudeSideMenuBar,
        inboxSideMenuBar: TestIDConfigKey.inboxSideMenuBar,
        faqSideMenuBar: TestIDConfigKey.faqSideMenuBar,
        contactSideMenuBar: TestIDConfigKey.contactSideMenuBar,
        logoutSideMenuBar: TestIDConfigKey.logoutSideMenuBar,
        buttonCloseSideMenuBar: TestIDConfigKey.btnCloseSideMenuBar,
    }

    static MyProfileScreen = {
        subscription: TestIDConfigKey.subscription,
        status: TestIDConfigKey.status,
        expireDate: TestIDConfigKey.expireDate,
        dropdownMakeSubscription: TestIDConfigKey.ddlMakeSubscription,
        dropdownCancelSubscription: TestIDConfigKey.ddlCancelSubscription,
        subscriptionActiveStatus: TestIDConfigKey.subscriptionActiveStatus,
        subscriptionDeactiveStatus: TestIDConfigKey.subscriptionDeactiveStatus,
        publishStatus: TestIDConfigKey.publishStatus,
        unPublishStatus: TestIDConfigKey.unPublishStatus,
        updateDudetteProfile: TestIDConfigKey.updateDudetteProfile,
        imageProfile: TestIDConfigKey.imgProfile,
        fullName: TestIDConfigKey.fullName,
        highlightStatus: TestIDConfigKey.highlightTitle,
        buttonDelete: TestIDConfigKey.btnDelete,
        buttonEditProfile: TestIDConfigKey.btnEditProfile,
        expireIn: TestIDConfigKey.expireIn,
        viewCountTitle: TestIDConfigKey.viewCount,
        commentCountTitle: TestIDConfigKey.commentCount,
        unreadCommentCountTitle: TestIDConfigKey.unreadCommentCount,
        confirmModal: TestIDConfigKey.modalConfirm,
        buttonPublishMyProfile: TestIDConfigKey.btnPublishMyProfile,
        switchStatusSeekingGuy: TestIDConfigKey.swhStatusSeekingGuy
    }

    static ManageProfileTabScreen = {
        myProfileTabMenu: TestIDConfigKey.myProfileTabMenu,
        myDudeProfileTabMenu: TestIDConfigKey.myDudeProfileTabMenu
    }

    static MyDudeProfileScreen = {
        imageProfile: TestIDConfigKey.imgProfile,
        fullName: TestIDConfigKey.fullName,
        highlightStatus: TestIDConfigKey.highlightTitle,
        updateDudeProfile: TestIDConfigKey.updateDudeProfile,
        buttonDelete: TestIDConfigKey.btnDelete,
        buttonEditProfile: TestIDConfigKey.btnEditProfile,
        expireIn: TestIDConfigKey.expireIn,
        viewCountTitle: TestIDConfigKey.viewCount,
        commentCountTitle: TestIDConfigKey.commentCount,
        unreadCommentCountTitle: TestIDConfigKey.unreadCommentCount,
        confirmModal: TestIDConfigKey.modalConfirm
    }

    static PrimaryInformationFirstScreen = {
        status: TestIDConfigKey.status,
        fullNamePlaceholder: TestIDConfigKey.inputFullName,
        emailPlaceholder: TestIDConfigKey.inputEmail,
        approveStatus: TestIDConfigKey.approveStatus,
        dropdownAge: TestIDConfigKey.ddlAge,
        dropdownCity: TestIDConfigKey.ddlCity,
        pickerInterest: TestIDConfigKey.pickerInterest,
        pickerHavdSogerHan: TestIDConfigKey.pickerHavdSogerHan,
        tagTitle: TestIDConfigKey.tagTitle,
        buttonSaveAndContinue: TestIDConfigKey.btnSaveAndContinue,
        buttonThreeDotsMenu: TestIDConfigKey.btnThreeDotsMenu,
        buttonDeactivate: TestIDConfigKey.btnDeactivate,
        buttonDelete: TestIDConfigKey.btnDelete,
    }
    static PrimaryInformationSecondScreen = {
        title: TestIDConfigKey.title,
        description: TestIDConfigKey.inputDescription,
        buttonSaveAndContinue: TestIDConfigKey.btnSaveAndContinue,
    }

    static PrimaryInformationFourthScreen = {
        title: TestIDConfigKey.title,
        description: TestIDConfigKey.description,
        selectYesAnswer: TestIDConfigKey.selectYesAnswer,
        selectNoAnswer: TestIDConfigKey.selectNoAnswer,
        exampleHighlightGallery: TestIDConfigKey.exampleHighlightGallery,
        buttonSubmit: TestIDConfigKey.btnSubmit,
        confirmModal: TestIDConfigKey.modalConfirm
    }

    static NotificationListScreen = {
        title: TestIDConfigKey.title,
        description: TestIDConfigKey.description,
        timeAgo: TestIDConfigKey.timeAgo
    }
    static PrimaryInformationThirdScreen = {
        uploadProfilePhoto: TestIDConfigKey.uploadProfilePhoto,
        uploadPhotoGallerySection: TestIDConfigKey.uploadPhotoGallerySection,
        buttonSelectPhoto: TestIDConfigKey.btnSelectPhoto,
        buttonSelectPhotoInGallery: TestIDConfigKey.btnSelectPhotoInGallery,
        buttonDeleteProfilePhoto: TestIDConfigKey.btnDeleteProfilePhoto,
        imageProfile: TestIDConfigKey.imgProfile
    }

    static SearchList = {
        title: TestIDConfigKey.title,
        icon: TestIDConfigKey.icon,
        searchingDudeList: TestIDConfigKey.searchingDudeList
    }

    static Filter = {
        title: TestIDConfigKey.title,
        modalPicker: TestIDConfigKey.modalPicker,
        selectAll: TestIDConfigKey.selectAll,
        sliderBarAgeRange: TestIDConfigKey.sliderBarAgeRange,
        tagTitle: TestIDConfigKey.tagTitle,
        regionTitle: TestIDConfigKey.regionTitle,
        regionSelected: TestIDConfigKey.regionSelected,
        buttonTagItem: TestIDConfigKey.btnTagItem,
        ageTitle: TestIDConfigKey.ageTitle,
        interestTitle: TestIDConfigKey.interestTitle,
        interestSelected: TestIDConfigKey.interestSelected,
        pickerInterest: TestIDConfigKey.pickerInterest,
        pickerCity: TestIDConfigKey.pickerCity,
        pickerRegion: TestIDConfigKey.pickerRegion,
        buttonSaveAndContinue: TestIDConfigKey.btnSaveAndContinue,
        swhSeekingGuy: TestIDConfigKey.swhStatusSeekingGuy
    }

    static DudeProfileTabScreen = {
        fullNameWithAgeTitle: TestIDConfigKey.fullNameWithAgeTitle,
        cityTitle: TestIDConfigKey.cityTitle,
        imgProfile: TestIDConfigKey.imgProfile,
        buttonSubmit: TestIDConfigKey.btnSubmit,
        inputMessage: TestIDConfigKey.inputMessage,
        buttonThumbsUp: TestIDConfigKey.btnThumbsUp,
        buttonThumbsDown: TestIDConfigKey.btnThumbsDown,
        thumbsUpTitle: TestIDConfigKey.lblThumbsUp,
        thumbsDownTitle: TestIDConfigKey.lblThumbsDown,
    }

    static DudeProfileFirstScreen = {
        tagTitle: TestIDConfigKey.tagTitle
    }

    static DudeProfileSecondScreen = {
        interestTitle: TestIDConfigKey.interestTitle,
        interestContent: TestIDConfigKey.interestContent,
        havdSogerHanTitle: TestIDConfigKey.havdSogerHanTitle,
        havdSogerHanContent: TestIDConfigKey.havdSogerHanContent,
        descriptionTitle: TestIDConfigKey.descriptionTitle,
        descriptionContent: TestIDConfigKey.descriptionContent
    }


    static DudeProfileThirdScreen = {
        imageGallery: TestIDConfigKey.imgGallery
    }
    static DudeProfileFourthScreen = {
        title: TestIDConfigKey.title,
        commentTitle: TestIDConfigKey.commentTitle,
        buttonEmailToDude: TestIDConfigKey.btnEmailToDude,
        buttonDirectMessageToCreator: TestIDConfigKey.btnDirectMessageToCreator,
        commentList: TestIDConfigKey.commentList
    }

    static DudeProfileDetailSendEmailScreen = {
        title: TestIDConfigKey.title,
        writeYourMessageTitle: TestIDConfigKey.writeYourMessageTitle,
        buttonSendEmailTitle: TestIDConfigKey.btnSendEmail,
    }

    static DudetteProfileDetailScreen = {
        fullNameWithAgeTitle: TestIDConfigKey.fullNameWithAgeTitle,
        cityTitle: TestIDConfigKey.cityTitle,
        imgProfile: TestIDConfigKey.imgProfile,
        interestTitle: TestIDConfigKey.interestTitle,
        interestContent: TestIDConfigKey.interestContent,
        havdSogerHanTitle: TestIDConfigKey.havdSogerHanTitle,
        havdSogerHanContent: TestIDConfigKey.havdSogerHanContent,
        descriptionTitle: TestIDConfigKey.descriptionTitle,
        descriptionContent: TestIDConfigKey.descriptionContent,
        contactThisDudette: TestIDConfigKey.btnContactThisDudette,
        tagTitle: TestIDConfigKey.tagTitle
    }
    static UserProfileDetailFirstScreen = {
        status: TestIDConfigKey.status,
        fullNamePlaceholder: TestIDConfigKey.inputFullName,
        emailPlaceholder: TestIDConfigKey.inputEmail,
        approveStatus: TestIDConfigKey.approveStatus,
        dropdownAge: TestIDConfigKey.ddlAge,
        dropdownCity: TestIDConfigKey.ddlCity,
        pickerInterest: TestIDConfigKey.pickerInterest,
        pickerHavdSogerHan: TestIDConfigKey.pickerHavdSogerHan,
        tagTitle: TestIDConfigKey.tagTitle,
        buttonSaveAndContinue: TestIDConfigKey.btnSaveAndContinue,
        buttonThreeDotsMenu: TestIDConfigKey.btnThreeDotsMenu,
        buttonDeactivate: TestIDConfigKey.btnDeactivate,
        buttonDelete: TestIDConfigKey.btnDelete,
    }
    static UserProfileDetailSecondScreen = {
        title: TestIDConfigKey.title,
        description: TestIDConfigKey.inputDescription,
        buttonSaveAndContinue: TestIDConfigKey.btnSaveAndContinue,
    }
    static UserProfileDetailThirdScreen = {
        uploadProfilePhoto: TestIDConfigKey.uploadProfilePhoto,
        uploadPhotoGallerySection: TestIDConfigKey.uploadPhotoGallerySection,
        buttonSelectPhoto: TestIDConfigKey.btnSelectPhoto,
        buttonSelectPhotoInGallery: TestIDConfigKey.btnSelectPhotoInGallery,
        buttonDeleteProfilePhoto: TestIDConfigKey.btnDeleteProfilePhoto,
        imageProfile: TestIDConfigKey.imgProfile
    }
    static FavouriteListTabScreen = {
        favouriteDudesTabMenu: TestIDConfigKey.favouriteDudesTabMenu,
        favouriteDudettesTabMenu: TestIDConfigKey.favouriteDudettesTabMenu
    }

    static FavouriteListScreen = {
        imageProfile: TestIDConfigKey.imgProfile,
        fullName: TestIDConfigKey.fullName,
        viewCountTitle: TestIDConfigKey.viewCount,
        commentCountTitle: TestIDConfigKey.commentCount,
        unreadCommentCountTitle: TestIDConfigKey.unreadCommentCount
    }

    static ChatListScreen = {
        title: TestIDConfigKey.title,
        imageProfile: TestIDConfigKey.imgProfile,
        fullName: TestIDConfigKey.fullName,
        lastTextMessage: TestIDConfigKey.lastTextMessage,
        lastTextMessageTime: TestIDConfigKey.lastTextMessageTime,
        timeAgo: TestIDConfigKey.timeAgo,
        confirmModal: TestIDConfigKey.modalConfirm,
    }

    static ChatRoomScreen = {
        title: TestIDConfigKey.title,
        buttonSubmit: TestIDConfigKey.btnSubmit,
        inputMessage: TestIDConfigKey.inputMessage,
    }
    static SubscriptionConfirmationScreen = {
        title: TestIDConfigKey.title,
        btnOK: TestIDConfigKey.btnOK,
        btnCancel: TestIDConfigKey.btnCancel,
    }
}