export class TestIDConfigKey {
    static AppName = "lblAppTitle"
    static slogan = "lblSloganTitle"
    static errorMessage = "lblErrorMessage"
    static title = "lblTitle"
    static subTitle = "lblSubTitle"
    static timeAgo = "timeAgoTitle"
    static inputFullName = "inputFullName"
    static inputDescription = "inputDescription"
    static inputEmail = "inputEmail"
    static ddlCountry = "ddlCountry"
    static ddlAge = "ddlAge"
    static ddlCity = "ddlCity"
    static inputPassword = "inputPassword"
    static inputConfirmPassword = "inputConfirmPassword"
    static btnNext = "btnNext"
    static btnBackToLogin = "btnBackToLogin"
    static modalPicker = "modalPicker"
    static selectAll = "selectAll"
    static btnSubmit = "btnSubmit"
    static btnBack = "btnBack"
    static btnOK = "btnOK"
    static btnCancel = "btnCancel"
    static image = "image"
    static icon = "icon"
    static ageTitle = "lblAge"
    static description = "lblDescription"
    static viewPagerContainer = "viewPagerContainer"
    static btnForgotPassword = "btnForgotPassword"
    static btnCreateAccount = "btnCreateAccount"
    static btnAccept = "btnAccept"
    static btnGoToNEM = "btnGoToNEM"
    static emailExample = "lblEmailExample"
    static inputVerifyCode = "inputVerifyCode"
    static btnVerify = "btnVerify"
    static btnGoToIntroduction = "btnGoToIntroduction"
    static btnGoToLogin = "btnGoToLogin"
    static btnGoToContactSupport = "btnGoToContactSupport"
    static highlightTitle = "lblHighlightTitle"
    static homeMenuTitle = "lblHomeMenuTitle"
    static btnSearchDude = "btnSearchDude"
    static btnCreateDude = "btncreateDude"
    static btnRecommendedMySelf = "btnRecommendedMySelf"
    static btnHelpDudette = "btnHelpDudette"
    static btnContinue = "btnContinue"
    static approveStatus = "lblApproveStatus"
    static watingForApprovalStatus = "lblWatingForApprovalStatus"
    static watingForPublishingStatus = "lblWatingForPublishingStatus"
    static btnDelete = "btnDelete"
    static btnEditProfile = "btnEditProfile"
    static homeSideMenu = "lblHomeSideMenu"
    static manageProfileSideMenu = "lblManageProfileSideMenu"
    static createDudeSideMenuBar = "lblCreateDudeSideMenuBar"
    static activitySideMenuBar = "lblCreateDudeSideMenuBar"
    static feedSideMenuBar = "lblCreateDudeSideMenuBar"
    static indberetnigSideMenuBar = "lblCreateDudeSideMenuBar"
    static sosSideMenuBar = "lblSOSSideMenuBar"
    static menuSideMenuBar = "lblMenuSideMenuBar"

    static searchDudeSideMenuBar = "lblSearchDudeSideMenuBar"
    static helpDudetteSideMenuBar = "lblHelpDudetteSideMenuBar"
    static favouriteDudeSideMenuBar = "lblFavouriteDudeSideMenuBar"
    static inboxSideMenuBar = "lblInboxSideMenuBar"
    static faqSideMenuBar = "lblFAQSideMenuBar"
    static contactSideMenuBar = "lblContactSideMenu"
    static logoutSideMenuBar = "lblLogoutSideMenu"
    static btnCloseSideMenuBar = "btnCloseSideMenu"
    static sliderBarAgeRange = "sliderBarAgeRange"
    static subscription = "lblSubscription"
    static expireDate = "lblExpireDate"
    static expireIn = "lblExpireIn"
    static status = "lblStatus"
    static subscriptionActiveStatus = "lblSubscriptionActiveStatus"
    static subscriptionDeactiveStatus = "lblSubscriptionDeactiveStatus"
    static ddlMakeSubscription = "ddlMakeSubscription"
    static ddlCancelSubscription = "ddlCancelSubscription"
    static publishStatus = "lblPublishStatus"
    static unPublishStatus = "lblUnPublishStatus"
    static updateDudetteProfile = "lblUpdateDudetteProfile"
    static updateDudeProfile = "lblUpdateDudeProfile"
    static myProfileTabMenu = "tabMenuMyProfile"
    static myDudeProfileTabMenu = "tabMenuMyDudeProfile"
    static pickerInterest = "pickerInterest"
    static pickerHavdSogerHan = "pickerHavdSogerHan"
    static pickerCity = "pickerCity"
    static pickerRegion = "pickerRegion"
    static tagTitle = "lblTag"
    static btnSaveAndContinue = "btnSaveAndContinue"
    static btnThreeDotsMenu = "btnThreeDotsMenu"
    static btnDeactivate = "btnDeactivate"
    static selectYesAnswer = "selectYesAnswer"
    static selectNoAnswer = "selectNoAnswer"
    static exampleHighlightGallery = "exampleHighlightGallery"
    static uploadProfilePhoto = "lblUploadProfilePhoto"
    static favouriteDudesTabMenu = "FavouriteDudesTabMenu"
    static favouriteDudettesTabMenu = "lblFavouriteDudettesTabMenu"
    static uploadPhotoGallerySection = "lblUploadPhotoGallerySection"
    static btnSelectPhoto = "btnSelectPhoto"
    static btnSelectPhotoInGallery = "btnSelectPhotoInGallery"
    static btnDeleteProfilePhoto = "btnDeleteProfilePhoto"
    static imgProfile = "imgProfile"
    static cityTitle = "lblCity"
    static regionTitle = "lblRegion"
    static regionSelected = "lblSelectedRegion"
    static citySelected = "lblSelectedCity"
    static havdSogerHanTitle = "lblHavdSogerHand"
    static btnTagItem = "btnTagItem"
    static btnGotoManageProfile = "btnGotoManageProfile"
    static modalConfirm = "modalConfirm"
    static interestTitle = "lblInterest"
    static interestSelected = "lblSelectedInterest"
    static btnResendCode = "btnResendCode"
    static interestContent = "lblInterestContent"
    static havdSogerHanContent = "lblHavdSogerHanContent"
    static descriptionTitle = "lblDescriptionTitle"
    static descriptionContent = "lblDescriptionContent"
    static inputMessage = "inputMessage"
    static btnEmailToDude = "btnEmailToDude"
    static btnDirectMessageToCreator = "btnDirectMessageToCreator"
    static fullNameWithAgeTitle = "lblFullNameWithAgeTitle"
    static fullName = "lblFullName"
    static imgGallery = "imgGallery"
    static btnSendEmail = "btnSendEmail"
    static writeYourMessageTitle = "lblWriteYourMessage"
    static commentTitle = "lblCommentTitle"
    static highlightDudeList = "listDudeHighlight"
    static searchingDudeList = "listSearchingDude"
    static commentList = "listComment"
    static btnContactThisDudette = "btnContactThisDudette"
    static viewCount = "lblViewCount"
    static lastTextMessage = "lblLastTextMessage"
    static lastTextMessageTime = "lblLastTextMessageTime"
    static commentCount = "lblCommentCount"
    static unreadCommentCount = "lblUnreadCommentCount"
    static btnPublishMyProfile = "btnPublishMyProfile"
    static swhStatusSeekingGuy = "swhStatusSeekingGuy"
    static swhSeekingGuy = "swhSeekingGuy"

    static lblPrivacyTitle = "lblPrivacyTitle"
    static lblPrivacyLinkTitle = "ibiPrivacyLinkTitle"

    static btnThumbsUp = "btnThumbsUp"
    static btnThumbsDown = "btnThumbsUp"

    static lblThumbsUp = "lblThumbUp"
    static lblThumbsDown = "lblThumbDown"
}