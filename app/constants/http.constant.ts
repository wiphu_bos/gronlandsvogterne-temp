// HTTP CODE //
export enum HTTP_CODE {
    _200 = 200, // Standard response for successful HTTP requests.
    _400 = 400, // Bad Request.
    _401 = 401, //
    _403 = 403, // The request was valid, but the server is refusing action. The user might not have the necessary permissions for a resource, or may need an account of some sort.
    _404 = 404, // The requested URL was not found.
    _500 = 500 // Internal Server Error
}

export enum HTTP_ERROR_MESSAGE {
    UNAUTHORIZED = "Unauthorized",
    PERMISSION_DENIED = "Permission Denied",
    REQUEST_NOT_FOUND = "The requested URL was not found.",
    INTERNAL_SERVER_ERROR_CODE = "INTERNAL_SERVER_ERROR",
    INTERNAL_SERVER_ERROR = "Internal Server Error"
}