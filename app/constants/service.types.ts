import { AxiosRequestConfig } from "axios"

type ErrorResponse = {
    value?: number,
    msg?: string,
    code?: string
}

export type APIErrorResponse = {
    errors?: ErrorResponse[]
}

export type APISuccessParams = {
    data: [{
        msg: string
    }]
}

export type APISuccessResponse = {
    data?: any,
    status?: number,
    statusText: string,
    headers: any,
    config: AxiosRequestConfig,

}

export type APIResponse =
    | APISuccessResponse
    | APIErrorResponse