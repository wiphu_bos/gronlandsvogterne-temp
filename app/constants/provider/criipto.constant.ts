import { NativeModules } from 'react-native'
import { AuthConfiguration } from 'react-native-app-auth'

const { ReactNativeConfig } = NativeModules
export const CRIIPTO = (): AuthConfiguration => {
  const environment = ReactNativeConfig.buildEnvironment
  switch (environment) {
      case "Development":
          return CRIIPTO_DEV
      case "Alpha":
          return CRIIPTO_DEV
      case "Staging":
          return CRIIPTO_DEV
      default:
          return CRIIPTO_PORD
  }
}

const CRIIPTO_DEV = {
  issuer: 'https://datemindude-test.criipto.id',
  clientId: 'urn:datemindude:application:identifier:7856',
  redirectUrl: 'com.datemindude:/oauthredirect',
  clientSecret: 'qGDZ049zlAUIcIhmUUlAcQr+eSWetdMZa8BaS6kSjQc=',
  additionalParameters: {
    acr_values: 'urn:grn:authn:dk:nemid:poces',
    acr: 'default',
    token_endpoint_auth_method: 'client_secret_post',
  },
  scopes: ['openid', 'profile', 'email', 'offline_access'],
};


const CRIIPTO_PORD = {
  issuer: 'https://datemindude.criipto.id',
  clientId: 'urn:datemindude:application:identifier:2895',
  redirectUrl: 'com.datemindude:/oauthredirect',
  clientSecret: 'emj5MEZdHuzLGZYk3xQtN/6ur+qqy968oP1DWnoS16Q=',
  additionalParameters: {
    acr_values: 'urn:grn:authn:dk:nemid:poces',
    acr: 'default',
    token_endpoint_auth_method: 'client_secret_post',
  },
  scopes: ['openid', 'profile', 'email', 'offline_access'],
};