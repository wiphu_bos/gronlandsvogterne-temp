
export enum DesignDimension {
    Width = 414,
    Height = 896
}

export enum GeneralResourcesJSONPath {
    Path = "generalresource.json"
}

export enum SearchListType {
    Dude,
    Dudette
}

export enum UserType {
    Dude = "Dude",
    Dudette = "Dudette",
    Me = "Me"
}

export enum Locale {
    Denmark = "da",
    Default = "en"
}

export enum FileUploadType {
    GalleryImage = "gallery_image",
    ProfileImage = "profile_image",
}

export enum NavigationKey {
    Splash = "splash_screen",
    Home = "home_screen",
    Welcome = "welcome_screen",
    Introduction = "introduction_screen",
    CreateAccount = "create_account_screen",
    Login = "login_screen",
    TermsAndConditions = "terms_and_conditions_screen",
    VerifyUser = "verify_code_screen",
    ForgotPassword = "forgot_password_screen",
    CreateDude = "create_dude_screen",
    Indberetning = "indberetning_screen",
    SOS = "sos_screen",
    Feed = "feed_screen",
    Menu = "menu_screen",
    WaitingDudeApprove = "waiting_dude_approve_screen",
    SideMenuBar = "side_menu_bar_screen",
    FAQ = "faq_screen",
    Contact = "contact_screen",
    NemId = "nemid_screen",
    Inbox = "inbox_screen",
    Chat = "chat_screen",
    UserInfo = "user_info_screen",
    MyProfile = "my_profile_screen",
    MyDudeProfile = "my_dude_profile_screen",
    Filters = "filters_screen",
    EmailSent = "email_sent_screen",
    MainStack = "main_stack",
    ManageProfileTab = "manage_profile_screen_tab",
    FavouriteListTab = "favourite_list_tab",
    NotificationList = "notification_list_screen",
    WaitingDudePublishProfile = "waiting_dude_approve_publish_profile_screen",
    SearchListScreen = "search_list_screen",
    CreateEditProfileTab = "create_edit_profile_tab",
    PrimaryInformationScreen = "primary_information_screen",
    CreateEditProfileFirst = "create_edit_profile_first_screen",
    CreateEditProfileSecond = "create_edit_profile_second_screen",
    CreateEditProfileThird = "create_edit_profile_third_screen",
    CreateEditProfileFourth = "create_edit_profile_fourth_screen",
    UserProfileDetailTab = "user_profile_detail_tab",
    UserProfileDetailScreen = "user_profile_detail_screen",
    UserProfileDetailFirst = "user_profile_detail_first_screen",
    UserProfileDetailSecond = "user_profile_detail_second_screen",
    UserProfileDetailThird = "user_profile_detail_third_screen",
    DudeProfileDetailTab = "dude_profile_detail_tab",
    DudeProfileDetailScreen = "dude_profile_detail_screen",
    DudeProfileDetailFirst = "dude_profile_detail_first_screen",
    DudeProfileDetailSecond = "dude_profile_detail_second_screen",
    DudeProfileDetailThird = "dude_profile_detail_third_screen",
    DudeProfileDetailFourthStack = "dude_profile_detail_fourth_stack",
    DudeProfileDetailFourth = "dude_profile_detail_fourth_screen",
    DudeProfileDetailSendEmail = "dude_profile_detail_send_email_screen",
    DudetteProfileDetails = "dudette_detail_screen",
    EmailSendSuccess = "email_send_screen",
    EmailSendToUserSuccessScreen = "email_send_to_user_success_screen",
    FavouriteDudetteList = "favourite_dudette_list_screen",
    FavouriteDudeList = "favourite_dude_list_screen",
    ChatList = "inbox_list_screen",
    ChatRoom = "inbox_screen",
    SubscriptionConfirmation = "premium_account_confirmation_screen",
    WebView = "web_view_screen"
}

export enum StoreName {
    Root = "RootStore",
    Navigation = "NavigationStore",
    Splash = "SplashStore",
    Welcome = "WelComeStore",
    Home = "HomeStore",
    Introduction = "IntroductionStore",
    CreateAccount = "CreateAccountStore",
    Login = "LoginStore",
    VerifyUser = "VerifyUserStore",
    User = "UserStore",
    Auth = "AuthStore",
    Menu = "MenuStore",
    Shared = "SharedStore",
    GeneralResources = "GeneralResourcesStore",
    Module = "ModuleStore",
    NemID = "NemIdStore",
    ForgotPassword = "ForgotPasswordStore",
    ValidationStore = "ValidationStore",
    TermsAndConditions = "TermsAndConditionStore",
    CreateDude = "CreateDudeStore",
    Indberetning = "IndberetningStore",
    SOS = "SOSStore",
    Feed = "CreateDudeStore",
    WaitingDudeApprove = "WaitingDudeApproveStore",
    CustomDrawer = "CustomDrawerStore",
    ManageProfileTab = "ManageProfileTabStore",
    FavouriteListTab = "FavouriteListTabStore",
    MyProfile = "MyProfileStore",
    MyDudeProfile = "MyDudeProfileStore",
    MyDudeProfileCard = "MyDudeProfileCardStore",
    FavouriteDudetteList = "FavouriteDudetteListStore",
    FavouriteDudeList = "FavouriteDudeListStore",
    CreateEditProfileTab = "CreateEditProfileStore",
    PrimaryInformationFirst = "PrimaryInformationFirstStore",
    PrimaryInformationSecond = "PrimaryInformationSecondStore",
    PrimaryInformationThird = "PrimaryInformationThirdStore",
    PrimaryInformationFourth = "PrimaryInformationFourthStore",
    NotificationList = "NotificationListStore",
    FetchingResourcesPropertyStore = "isFetchingResourcesPropertyStore",
    WaitingDudePublishProfile = "WaitingDudePublishProfileStore",
    SearchList = "SearchListStore",
    Filter = "FilterStore",
    UserProfileDetailTab = "UserProfileDetailTabStore",
    UserProfileDetailFirst = "UserProfileDetailFirstStore",
    UserProfileDetailSecond = "UserProfileDetailSecondStore",
    UserProfileDetailThird = "UserProfileDetailThirdStore",

    DudeProfileDetailTab = "DudeProfileDetailTabStore",
    DudeProfileDetailFirst = "DudeProfileDetailFirstStore",
    DudeProfileDetailSecond = "DudeProfileDetailSecondStore",
    DudeProfileDetailThird = "DudeProfileDetailThirdStore",
    DudeProfileDetailFourth = "DudeProfileDetailFourthStore",
    DudeProfileDetailSendEmail = "DudeProfileDetailSendEmailStore",
    EmailSendSuccess = "EmailSendSuccess",
    DudetteDetailStore = "DudetteDetailStore",
    ChatList = "ChatListStore",
    ChatRoom = "ChatRoomStore",
    Chat = "ChatStore",
    Message = "MessageStore",
    SubscriptionConfirmation = "SubscriptionConfirmationStore",
    WebView = "WebViewStore",

    StackCustomTab = "StackCustomTabStore"
}

export enum DudeStatus {
    WaitingForApproval = 'waiting_for_approval',
    Approved = 'approved',
    WaitingForPublishing = 'waiting_for_publishing',
    Published = 'published',
    Unpublished = 'unpublished',
    Deleted = 'deleted',
    RequestForEditing = 'request_for_editing'
}

export enum EventName {
    NavigationEvents = "NavigationEvents"
}

export enum ListenerKey {
    HardwareBackPress = "hardwareBackPress"
}

export enum NotificationTopicKey {
    CreatedDude = "created_dude_",
    PublishDude = "publish_dude_",
    Comment = "comment_dude_",
    ReportComment = "report_comment_dude_",
    CreateMessage = "create_message_",
    RequestForEditing = "reqeust_for_editing_"
}

export const ageConstant = require('./constants.json')

export const newIphoneDeviceIds: string[] = ['iPhone10,3', 'iPhone10,6', 'iPhone11,2', 'iPhone11,4', 'iPhone11,6', 'iPhone11,8', 'iPhone12,1', 'iPhone12,3', 'iPhone12,5']
//https://gist.github.com/adamawolf/3048717