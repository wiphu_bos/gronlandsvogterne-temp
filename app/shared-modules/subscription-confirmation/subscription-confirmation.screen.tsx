// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { SubscriptionConfirmationProps } from "./subscription-confirmation.props"
import SubscriptionConfirmationController from "./subscription-confirmation.controllers"
import { INavigationRoute } from "../../models/navigation-store/navigation.types"
// MARK: Style Import

import { useConfigurate } from "../../custom-hooks/use-configure-controller"
import { ConfirmModal } from "../../components"

export const SubscriptionConfirmationScreen: React.FunctionComponent<SubscriptionConfirmationProps & Partial<INavigationRoute>> = observer((props) => {

    const controller = useConfigurate(SubscriptionConfirmationController, props) as SubscriptionConfirmationController

    // MARK: Render
    return (
        <ConfirmModal
            {...SubscriptionConfirmationController.resourcesViewModel?.getTestIDConfirmModal()}
            okButtonText={SubscriptionConfirmationController.resourcesViewModel?.getResourceModalButtonSubscribeTitle()}
            clearButtonText={SubscriptionConfirmationController.resourcesViewModel?.getResourceModalButtonCancelTitle()}
            message={SubscriptionConfirmationController.resourcesViewModel?.getResourceModalConfirmSubscribeServices()}
            isVisible={!props.skipConfirmation}
            onModalClosed={controller.onModalClosed}
            onCancel={controller.onCancel}
            onOK={controller.onOK} />
    )
})
