import { ViewStyle, TextStyle } from "react-native";
import * as metric from "../../../../theme"
import { color } from "../../../../theme"
import { RFValue } from "react-native-responsive-fontsize";

export const CONTAINNER_STYLE: ViewStyle = {
    width: metric.ratioWidth(113),
    height: metric.ratioHeight(47),
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingRight: metric.ratioWidth(15),
}

const TEXT: TextStyle = {
    color: color.palette.white,
    fontFamily: "Montserrat",
}

export const TITLE: TextStyle = {
    ...TEXT,
    lineHeight: metric.ratioHeight(22),
    fontSize: RFValue(12),
    fontWeight: '400',

    //Android
    fontFamily: "Montserrat-Regular",
}
