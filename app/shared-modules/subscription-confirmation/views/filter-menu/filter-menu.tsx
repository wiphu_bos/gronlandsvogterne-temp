// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { FilterMenuProps } from "./filter-menu.props"
import * as Styles from "./filter-menu.styles"
import { Icon, Button, Text } from "../../../../components"

export const FilterMenuComponent: React.FunctionComponent<FilterMenuProps> = observer((props) => {

    // MARK: Render

    return (
        <Button preset="none" isAnimated={false} style={Styles.CONTAINNER_STYLE} onPress={props.onPress}>
            <Text text={props.title} style={Styles.TITLE} />
            <Icon source={props.iconSource} style={props.iconStyle} />
        </Button>
    )
})