import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { MenuProps } from '../../../home/views/menu/menu-props'

export interface FilterMenuProps extends NavigationContainerProps<ParamListBase>, MenuProps {

}