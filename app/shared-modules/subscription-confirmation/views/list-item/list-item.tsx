// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { ListItemProps } from "./list-item.props"
import * as Styles from "./list-item.styles"
import { Icon, Button, Text } from "../../../../components"
import { images } from "../../../../theme/images"
import { View } from "react-native"
import LinearGradient from "react-native-linear-gradient"

export const ListItemComponent: React.FunctionComponent<ListItemProps> = observer((props) => {

    // MARK: Render
    
    return (
        <Button
        preset="none" 
        isSolid 
        isAnimated={false} 
        style={Styles.CONTAINNER_STYLE} onPress={() => props.onPress("")}>
            <LinearGradient
                colors={Styles.EDGE_HIGHLIGHT_COLORS}
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 1 }}
                style={Styles.EDGE_CONTAINER}
            />
            <View style={Styles.CONTENT_CONTAINER}>
                <Text text={[props.item?.full_name, props.item?.age?.toString()].join(', ')} style={Styles.TITLE} numberOfLines={1} />
                <View style={Styles.SUB_CONTENT_CONTAINER}>
                    <Icon source={images.pinkPin} style={Styles.PINK_PIN} />
                    <Text text={props.item?.city} style={Styles.DESCRIPTION} numberOfLines={1} />
                </View>
            </View>
        </Button>
    )
})