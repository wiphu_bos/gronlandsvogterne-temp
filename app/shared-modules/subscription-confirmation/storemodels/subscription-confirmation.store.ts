import { types } from "mobx-state-tree"
import { SubscriptionConfirmationViews, SubscriptionConfirmationActions, SubscriptionConfirmationResourcesViews, SubscriptionConfirmationPropsModel } from "../viewmodels"
import { SubscriptionConfirmationServiceActions } from "../services/subscription-confirmation.services"
import { StoreName } from "../../../constants/app.constant"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store/general-resources.store"

const SubscriptionConfirmationModel = types.model(StoreName.SubscriptionConfirmation, SubscriptionConfirmationPropsModel)

export const SubscriptionConfirmationStoreModel = types.compose(
    SubscriptionConfirmationModel,
    SubscriptionConfirmationViews,
    SubscriptionConfirmationActions,
    SubscriptionConfirmationServiceActions)

export const SubscriptionConfirmationResourcesStoreModel = types.compose(GeneralResourcesStoreModel, SubscriptionConfirmationResourcesViews)

