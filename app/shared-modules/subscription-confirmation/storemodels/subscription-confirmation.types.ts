import { Instance, SnapshotOut } from "mobx-state-tree"
import { SubscriptionConfirmationStoreModel, SubscriptionConfirmationResourcesStoreModel } from "./subscription-confirmation.store"

export type SubscriptionConfirmationStore = Instance<typeof SubscriptionConfirmationStoreModel>
export type SubscriptionConfirmationStoreSnapshot = SnapshotOut<typeof SubscriptionConfirmationStoreModel>

export type SubscriptionConfirmationResourcesStore = Instance<typeof SubscriptionConfirmationResourcesStoreModel>
export type SubscriptionConfirmationResourcesStoreSnapshot = SnapshotOut<typeof SubscriptionConfirmationResourcesStoreModel>
