import { types } from "mobx-state-tree"
import { SubscriptionConfirmationPropsModel } from "./subscription-confirmation.models"
export const SubscriptionConfirmationActions = types.model(SubscriptionConfirmationPropsModel).actions(self => {
    const setIsModalShowing = (value: boolean) => self.isModalShowing = value
    return {
        setIsModalShowing
    }
})