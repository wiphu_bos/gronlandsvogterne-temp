import { types } from "mobx-state-tree"

export const SubscriptionConfirmationPropsModel = {
    isModalShowing: types.optional(types.boolean, false)
} 