import { types } from "mobx-state-tree"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store/general-resources.store"
import { TestIDResources } from "../../../constants/test-key/test.constant"
import * as Utils from "../../../utils"
import { GeneralResources } from "../../../constants/firebase/remote-config/remote-config.constant"
import { NavigationKey } from "../../../constants/app.constant"
import { SubscriptionConfirmationPropsModel } from "./subscription-confirmation.models"
export const SubscriptionConfirmationViews = types.model(SubscriptionConfirmationPropsModel)
    .views(self => ({
        get getIsModalShowing(): boolean {
            return self.isModalShowing
        },
    }))

export const SubscriptionConfirmationResourcesViews = GeneralResourcesStoreModel
    .views(self => {

        //MARK: Volatile State

        const { SubscriptionConfirmationScreen } = GeneralResources
        const {
            modalButtonSubscribeTitle,
            modalButtonCancelTitle,
            modalConfirmSubscribeServices
        } = SubscriptionConfirmationScreen
        const { SubscriptionConfirmation } = NavigationKey

        //MARK: Views
        const getResources = (key: string, childKeyOrShareKey: string | boolean = false) => self.getValues(childKeyOrShareKey ? key : SubscriptionConfirmation, childKeyOrShareKey ? true : key)
        const getResourceModalButtonSubscribeTitle = () => getResources(modalButtonSubscribeTitle, true)
        const getResourceModalButtonCancelTitle = () => getResources(modalButtonCancelTitle, true)
        const getResourceModalConfirmSubscribeServices = () => getResources(modalConfirmSubscribeServices, true)

        return {
            getResourceModalButtonSubscribeTitle,
            getResourceModalButtonCancelTitle,
            getResourceModalConfirmSubscribeServices
        }
    })

    .views(self => {

        //MARK: Volatile State

        //MARK: Views

        const getTestIDConfirmModal = () => Utils.getTestIDObject(TestIDResources.SubscriptionConfirmationScreen.title)
        const getTestIDButtonCancel = () => Utils.getTestIDObject(TestIDResources.SubscriptionConfirmationScreen.btnCancel)
        const getTestIDButtonOK = () => Utils.getTestIDObject(TestIDResources.SubscriptionConfirmationScreen.btnOK)
        return {
            getTestIDConfirmModal,
            getTestIDButtonOK,
            getTestIDButtonCancel
        }
    })

