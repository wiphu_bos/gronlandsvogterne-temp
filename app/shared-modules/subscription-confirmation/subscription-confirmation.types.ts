import { Instance, SnapshotOut } from "mobx-state-tree"
import { SubscriptionConfirmationStoreModel } from "./storemodels"

export type SubscriptionConfirmationStore = Instance<typeof SubscriptionConfirmationStoreModel>
export type SubscriptionConfirmationStoreSnapshot = SnapshotOut<typeof SubscriptionConfirmationStoreModel>