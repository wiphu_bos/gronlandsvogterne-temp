// MARK: Import

import { NavigationKey } from "../../constants/app.constant"
import { SubscriptionConfirmationProps } from "./subscription-confirmation.props"
import { SubscriptionConfirmationResourcesStoreModel, SubscriptionConfirmationStoreModel } from "./storemodels/subscription-confirmation.store"
import { SubscriptionConfirmationResourcesStore } from "./storemodels/subscription-confirmation.types"
import { RootStore, INavigationRoute } from "../../models"
import { onSnapshot } from "mobx-state-tree"
import { SubscriptionConfirmationStore } from "./subscription-confirmation.types"
import { BaseController } from "../../modules/base.controller"
import { IPaymentResponse } from "../../models/user-store"

class SubscriptionConfirmationController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static resourcesViewModel: SubscriptionConfirmationResourcesStore
    public static viewModel: SubscriptionConfirmationStore
    public static myProps: SubscriptionConfirmationProps & Partial<INavigationRoute>

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, props?: SubscriptionConfirmationProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, props, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(props)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        SubscriptionConfirmationController.resourcesViewModel = SubscriptionConfirmationResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.SubscriptionConfirmation)
        SubscriptionConfirmationController.viewModel = localStore && SubscriptionConfirmationStoreModel.create({ ...localStore }) || SubscriptionConfirmationStoreModel.create({})
    }

    private _setupSnapShot = () => {
        onSnapshot(SubscriptionConfirmationController.viewModel, (snap: SubscriptionConfirmationStore) => {
            this._setInitializedToPropsParams()
        })
    }

    //@override
    _setupProps = (myProps: SubscriptionConfirmationProps & Partial<INavigationRoute>) => {
        super._setupProps && super._setupProps(myProps)
        SubscriptionConfirmationController.myProps = {
            ...myProps as object,
            ...(myProps as INavigationRoute)?.route?.params
        }
    }

    /*
       Mark Data
   */

    /*
       Mark Event
   */

    private _subscriptionProcess = async () => {
        this._isGlobalLoading(true)
        const payment: IPaymentResponse = await SubscriptionConfirmationController.viewModel?.subscribePremiumAccountIAP(this._rootStore)
        const transactionId: string = payment?.transactionId
        if (transactionId) await SubscriptionConfirmationController.viewModel?.confirmSubscribePremiumAccount(this._rootStore, payment)
        this._isGlobalLoading(false)
        if (transactionId) {
            const onSuccess = SubscriptionConfirmationController.myProps.onSuccess
            onSuccess && onSuccess()
        }
    }

    public onCancel = () => {
        const navigationPropsAfterCancel = SubscriptionConfirmationController.myProps.navigationPropsAfterCancel
        if (navigationPropsAfterCancel) {
            Promise.all([this.backProcess(), this._myProps?.navigation?.dispatch(navigationPropsAfterCancel)])
        } else {
            this.backProcess()
        }
    }

    private _onClose = () => {
        SubscriptionConfirmationController.viewModel?.setIsModalShowing(false)
    }

    public onModalClosed = () => {
    }

    public onOK = () => {
        this._onClose()
        this.onCancel()
        this._delayExecutor(() => this._subscriptionProcess())
    }

    /*
       Mark Life cycle
   */

    public viewDidAppearAfterFocus = async () => {
        const skipConfirmation = SubscriptionConfirmationController.myProps?.skipConfirmation
        if (skipConfirmation) {
            this.onOK()
        }
        SubscriptionConfirmationController.viewModel?.setIsModalShowing(!skipConfirmation)
        super.viewDidAppearAfterFocus && super.viewDidAppearAfterFocus()
    }

    public viewWillDisappear = () => {
        this._onClose()
        super.viewWillDisappear && super.viewWillDisappear()
    }
}

export default SubscriptionConfirmationController
