import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"

export interface SubscriptionConfirmationProps extends NavigationContainerProps<ParamListBase> {
    skipConfirmation?: boolean
    onSuccess?: () => void
    navigationPropsAfterCancel?: any
}