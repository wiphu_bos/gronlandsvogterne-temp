import { types, flow } from "mobx-state-tree"
import { SubscriptionConfirmationPropsModel } from "../viewmodels"
import { RootStore } from "../../../models/root-store"
import { IPaymentResponse } from "../../../models/user-store"

export const SubscriptionConfirmationServiceActions = types.model(SubscriptionConfirmationPropsModel).actions(self => {

    //MARK: Volatile State

    //MARK: Service Actions
    const subscribePremiumAccountIAP = flow(function* (rootStore?: RootStore) {
        try {
            return yield rootStore?.getUserStore?.subscribePremiumAccountIAP()
        } catch (e) {
            return e
        }
    })

    const confirmSubscribePremiumAccount = flow(function* (rootStore?: RootStore, payment?: IPaymentResponse) {
        try {
            return yield rootStore?.getUserStore?.confirmSubscribePremiumAccount(payment)
        } catch (e) {
            return e
        }
    })
    return {
        subscribePremiumAccountIAP,
        confirmSubscribePremiumAccount
    }
})