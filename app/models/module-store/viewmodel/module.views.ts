
import { types } from "mobx-state-tree"
import { ModulePropsModel } from "../module.model"

export const ModuleViews = types.model(ModulePropsModel)
    .views(self => ({
        getModule(key: string): any {
            return { ...self.data[key] }
        },
        get allModule(): any {
            return self.data
        }
    }))