import { types } from "mobx-state-tree"
import { ModulePropsModel } from "../module.model"
import { toJS } from "mobx"

export const ModuleActions = types.model(ModulePropsModel).actions(self => {
    const setModule = (key: string, value: any) => {
        let jsonData = toJS(self.data)
        if (!value) {
            delete jsonData[key]
        } else {
            jsonData = { ...jsonData, ...{ [key]: value } }
        }
        self.data = jsonData
    }
    return { setModule }
})