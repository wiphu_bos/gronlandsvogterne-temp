import { Instance, SnapshotOut } from "mobx-state-tree"
import { ModuleStoreModel } from "./module.store"

export type ModuleStore = Instance<typeof ModuleStoreModel>
export type ModuleStoreSnapshot = SnapshotOut<typeof ModuleStoreModel>