import { types } from "mobx-state-tree"

export const ModulePropsModel = {
    data: types.optional(types.frozen(), {})
}