import { Instance, SnapshotOut } from "mobx-state-tree"
import { FilterStoreModel } from "./filter-store.store"

export type FilterStore = Instance<typeof FilterStoreModel>
export type FilterStoreSnapshot = SnapshotOut<typeof FilterStoreModel>
