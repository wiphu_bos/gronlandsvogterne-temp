import { types } from "mobx-state-tree"
import { FilterPropsModel } from "../filter-store.model"
import { ISelect } from "../../user-store"
export const FilterViews = types.model(FilterPropsModel)
    .views(self => ({
        get getIsSelectedAllTags() {
            return self.isSelectedAllTags
        },
        get getIsSelectedAllCities() {
            return self.isSelectedAllCities
        },
        get getIsSelectedAllRegions() {
            return self.isSelectedAllRegions
        },
        get getIsSelectedAllInterests() {
            return self.isSelectedAllInterests
        },
        get getMinAge() {
            return self.minAge
        },
        get getMaxAge() {
            return self.maxAge
        },
        get getCityList() {
            return self.cityList
        },
        get getRegionList(): ISelect[] {
            return self.regionList
        }
    }))