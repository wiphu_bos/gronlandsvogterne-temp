import { types } from "mobx-state-tree"
import { ISelect, UserStoreModel } from "../../../models/user-store"
import { FilterPropsModel } from "../filter-store.model"
import { RootStore } from "../../root-store"
import * as metric from "../../../theme"
export const FilterActions = types.compose(types.model(FilterPropsModel), UserStoreModel).actions(self => {
    const setRegionList = (value: ISelect[]) => self.regionList = value
    const setCityList = (value: ISelect[]) => self.cityList = value
    const setIsSelectedAllTags = (value: boolean) => self.isSelectedAllTags = value
    const setIsSelectedAllRegions = (value: boolean) => self.isSelectedAllRegions = value
    const setIsSelectedAllCities = (value: boolean) => self.isSelectedAllCities = value
    const setIsSelectedAllInterests = (value: boolean) => self.isSelectedAllInterests = value
    const setMinAge = (value: number) => self.minAge = value
    const setMaxAge = (value: number) => self.maxAge = value
    const setDefaultValue = (rootStore?: RootStore) => {
        const allCities = rootStore?.getAllGeneralResourcesStore.getCities
        const allInterests = rootStore?.getAllGeneralResourcesStore.getInterests
        const allTags = rootStore?.getAllGeneralResourcesStore.getTags
        const allRegions = rootStore?.getAllGeneralResourcesStore.getRegions
        setCityList(allCities)
        setRegionList(allRegions)
        self.setInterestList(allInterests)
        self.setTagList(allTags)
        self.isSelectedAllCities = true
        self.isSelectedAllInterests = true
        self.isSelectedAllTags = true
        self.isSelectedAllRegions = true
        self.minAge = metric.minAge
        self.maxAge = metric.starterMaxAge
    }
    return {
        setCityList,
        setRegionList,
        setIsSelectedAllTags,
        setIsSelectedAllCities,
        setIsSelectedAllRegions,
        setIsSelectedAllInterests,
        setMinAge,
        setMaxAge,
        setDefaultValue
    }
})