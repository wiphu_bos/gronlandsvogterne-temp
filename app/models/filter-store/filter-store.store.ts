import { types } from "mobx-state-tree"
import { FilterPropsModel } from "./filter-store.model"
import { UserStoreModel } from "../user-store/user.store"
import { ValidationStoreModel } from "../validation-store/validation-store.store"
import { StoreName } from "../../constants/app.constant"
import { FilterActions, FilterViews } from "./viewmodel"
import { FilterServiceActions } from "../../modules/filter/services/filter.services"

const FilterModel = types.model(StoreName.Filter, FilterPropsModel)

export const FilterStoreModel = types.compose(
    UserStoreModel,
    ValidationStoreModel,
    FilterModel,
    FilterViews,
    FilterActions,
    FilterServiceActions)

