import { types } from "mobx-state-tree"
import * as metric from "../../theme"
import { ISelect } from "../user-store/user.types"

/*
    Some properties'name are as same as the backend.
*/

export const FilterPropsModel = {
    isSelectedAllTags: types.optional(types.boolean, false),
    isSelectedAllCities: types.optional(types.boolean, false),
    isSelectedAllRegions: types.optional(types.boolean, false),
    isSelectedAllInterests: types.optional(types.boolean, false),
    minAge: types.optional(types.number, metric.minAge),
    maxAge: types.optional(types.number, 35),
    cityList: types.maybeNull(types.frozen<ISelect[]>()),
    regionList: types.maybeNull(types.frozen<ISelect[]>()),
}