import { types } from "mobx-state-tree"
import { NavigationStoreModel } from "../navigation-store"
import { UserStoreModel } from "../user-store"
import { AuthStoreModel } from "../auth-store"
import { MenuStoreModel } from "../menu-store"
import { ModuleStoreModel } from "../module-store"
import { SharedStoreModel } from "../shared-store"
import { GeneralResourcesStoreModel } from "../general-resources-store"
import { NotificationStoreModel } from "../notification-store"
import { FilterStoreModel } from "../filter-store/filter-store.store"
import { ChatStoreModel } from "../chat-store/chat-store.store"

export const RootPropsModel = {
    NavigationStore: types.optional(NavigationStoreModel, {}),
    NotificationStore: types.optional(NotificationStoreModel, {}),
    UserStore: types.optional(UserStoreModel, {}),
    ChatStore: types.optional(ChatStoreModel, {}),
    AuthStore: types.optional(AuthStoreModel, {}),
    MenuStore: types.optional(MenuStoreModel, {}),
    SharedStore: types.optional(SharedStoreModel, {}),
    ModuleStore: types.optional(ModuleStoreModel, {}),
    GeneralResourcesStore: types.optional(GeneralResourcesStoreModel, {}),
    isFetchingResourcesPropertyStore: types.optional(types.boolean, true),
    FilterStore: types.optional(FilterStoreModel, {})
}