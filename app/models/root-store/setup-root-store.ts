import { onSnapshot } from "mobx-state-tree"
import { RootStoreModel } from "./root-store"
import { RootStore } from "./root.types"
import { Environment } from "./environment"
import * as storage from "../../utils/storage"
import * as StorageConfig from "../../config/storage.config"

type StringToAnyMap = { [key: string]: any }

/**
 * Setup the environment that all the models will be sharing.
 *
 * The environment includes other functions that will be picked from some
 * of the models that get created later. This is how we loosly couple things
 * like events between models.
 */
export async function createEnvironment() {
  const env = new Environment()
  await env.setup()
  return env
}

/**
 * Setup the root state.
 */
export async function setupRootStore() {
  let rootStore: RootStore
  let data: any

  // prepare the environment that will be associated with the RootStore.
  const env = await createEnvironment()
  try {
    // load data from storage
    data = (await storage.load(StorageConfig.ROOT_STATE_STORAGE_KEY)) || {}
    rootStore = RootStoreModel.create(data, env)
  } catch (e) {
    // if there's any problems loading, then let's at least fallback to an empty state
    // instead of crashing.
    rootStore = RootStoreModel.create({}, env)
  }

  // reactotron logging
  if (__DEV__) {
    env.reactotron.setRootStore(rootStore, data)
  }

  const { jsonify, storeBlacklist } = StorageConfig.storagePersistOptions
  // track changes & save to storage
  onSnapshot(rootStore, (_snapshot: StringToAnyMap) => {
    const snapshot = { ..._snapshot }
    Object.keys(snapshot).forEach((key) => {
      storeBlacklist && storeBlacklist[key] && delete snapshot[key]
    })
    const snapshotData = !jsonify ? snapshot : JSON.parse(JSON.stringify(snapshot))
    storage.save(StorageConfig.ROOT_STATE_STORAGE_KEY, snapshotData)
  })

  return rootStore
}
