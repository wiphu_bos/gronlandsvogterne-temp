import { types } from "mobx-state-tree"
import { RootPropsModel } from "../root-store-model"
import { NavigationStoreModel } from "../../navigation-store"
import { UserStoreModel } from "../../user-store"
import { AuthStoreModel } from "../../auth-store"
import { ModuleStoreModel } from "../../module-store"
import { RootStore } from "../root.types"


export const RootActions = types.model(RootPropsModel).actions(self => {

    const setNavigationStore = (value: any) => {
        self.NavigationStore = value
    }
    const setFilterStore = (value: any, rootStore?: RootStore) => {
        self.FilterStore = value
        rootStore && self.FilterStore.setDefaultValue(rootStore)
    }
    const setUserStore = (value: any) => {
        self.UserStore = value
    }
    const setAuthStore = (value: any) => {
        self.AuthStore = value
    }
    const setSharedStore = (value: any) => {
        self.SharedStore = value
    }
    const setNotificationStore = (value: any) => {
        self.NotificationStore = value
    }
    const setModuleStore = (value: any) => {
        self.ModuleStore.data = { ...self.ModuleStore.data, ...value }
    }
    const setGeneralResourcesStore = (value: any) => {
        self.GeneralResourcesStore = value
    }
    const setIsFetchingResources = (value: boolean) => {
        self.isFetchingResourcesPropertyStore = value
    }
    const resetRootStore = () => {
        setNavigationStore(NavigationStoreModel.create({}))
        setUserStore(UserStoreModel.create({}))
        setAuthStore(AuthStoreModel.create({}))
        setModuleStore(ModuleStoreModel.create({}))
    }
    return {
        setNavigationStore,
        setUserStore,
        setAuthStore,
        setSharedStore,
        setModuleStore,
        setGeneralResourcesStore,
        setIsFetchingResources,
        resetRootStore,
        setNotificationStore,
        setFilterStore,
    }
})

