import { types } from "mobx-state-tree"
import { RootPropsModel } from "../root-store-model"
import * as Utils from "../../../utils"
import { Alert } from "react-native"

export const RootViews = types.model(RootPropsModel)
    .views(self => ({
        get getNavigationStore() {
            return self.NavigationStore
        },
        get getUserStore() {
            return self.UserStore
        },
        get getChatStore() {
            return self.ChatStore
        },
        get getAuthStore() {
            console.log('getAuthStore')
            // alert('getAuthStore')
            return self.AuthStore
        },
        get getSharedStore() {
            return self.SharedStore
        },
        get getAllModuleStore(): any {
            return self.ModuleStore
        },
        get getNotificationStore() {
            return self.NotificationStore
        },
        getModuleStore(key: string): any {
            const object = self.ModuleStore.getModule(key)
            return object && !Utils.isEmptyObject(object) && object || null
        },
        get getAllGeneralResourcesStore() {
            return self.GeneralResourcesStore
        },
        getGeneralResourcesStore(rootKey?: string, childKey?: string | boolean): any {
            return self.GeneralResourcesStore.getValues(rootKey, childKey)
        },
        get getIsFetchingResources() {
            return self.isFetchingResourcesPropertyStore
        },
        get getFilterStore() {
            return self.FilterStore
        }
    }))