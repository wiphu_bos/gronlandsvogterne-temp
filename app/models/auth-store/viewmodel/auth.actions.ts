import { types } from "mobx-state-tree"
import { AuthPropsModel } from "../auth.model"
import { FirebaseUserInfo } from "../../../utils/firebase/authentication/auth.types"

export const AuthActions = types.model(AuthPropsModel).actions(self => {
    const setFirebaseUserInfo = (value: FirebaseUserInfo) => {
        self.firebaseUserInfo = value
    }
    const setIsLoggedIn = (value: boolean | undefined) => {
        self.isLoggedIn = value
    }
    const setName = (value: string | undefined) => {
        self.myname = value
    }
    const setLang = (value: string | undefined) => {
        self.mylang = value
    }

    return { setFirebaseUserInfo, setIsLoggedIn, setName, setLang }
})