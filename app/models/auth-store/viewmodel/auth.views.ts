import { types } from "mobx-state-tree"
import { AuthPropsModel } from "../auth.model"
import { FirebaseUserInfo } from "../../../utils/firebase/authentication/auth.types"
export const AuthViews = types.model(AuthPropsModel)
    .views(self => ({
        get getFirebaseUserInfo(): FirebaseUserInfo {
            return self.firebaseUserInfo
        },
        get getLoggedIn(): boolean {
            return self.isLoggedIn
        },

        get getName(): string {
            return self.myname
        },

        get getLang(): string {
            return self.mylang
        },
   
        // getLoggedIn2 (value:string) {
        //     return self.
        // }
    }))