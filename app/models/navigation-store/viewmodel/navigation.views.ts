import { types } from "mobx-state-tree"
import { NavigationPropsModel } from "../navigation.model"
import * as Utils from "../../../utils"
import { INavigationState } from "../../../models/navigation-store/navigation.types"
import { toJS } from "mobx"
import * as AppConfig from "../../../config/app.config"
import { contains } from "ramda"
import { NavigationScreenProp } from "react-navigation"
import { State } from "react-native-gesture-handler"


export const NavigationViews = types.model(NavigationPropsModel)
    .views(self => {
        return {
            get getCurrentPageIndex(): number {
                return self.currentState.index
            },
            get getCurrentPageName(): string {
                return Utils.getActiveRouteName(self.currentState)
            },
            get getPreviousPageIndex(): number {
                return self.previousState.index
            },
            get getPreviousPageName(): string {
                return Utils.getActiveRouteName(self.previousState)
            },
            get getPassingParams(): any {
                const currentState = this.getCurrentState
                const currentRoutes = currentState.routes
                if (!currentState || !currentRoutes) return null
                return toJS(currentRoutes[this.getCurrentPageIndex]?.params)
            },
            get getPassedParams(): any {
                const previousState = this.getPreviousState
                const previousRoutes = previousState.routes
                if (!previousState || !previousRoutes) return null
                return toJS(previousRoutes[this.getPreviousPageIndex]?.params)
            },
            get getAllRoutes(): string[] {
                const currentState = this.getCurrentState
                if (!currentState || !currentState.routeNames) return null
                return currentState.routeNames.slice()
            },
            get getCurrentState(): INavigationState {
                return self.currentState
            },
            get getPreviousState(): INavigationState {
                return self.previousState
            },
            get canExit(): boolean {
                return contains(this.getCurrentPageName, AppConfig.screenExitList)
            },
            get canGoBack(): boolean {
                return this.getCurrentPageIndex > 0
            },
            get getNavigation(): NavigationScreenProp<State> {
                return self.navigation
            },
            get getParentNavigation(): NavigationScreenProp<any> | undefined {
                return this.getNavigation?.dangerouslyGetParent()
            },
        }
    })