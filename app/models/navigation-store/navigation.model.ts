import { types } from "mobx-state-tree"

const NavigationRoute = {
    name: types.maybeNull(types.string),
    key: types.maybeNull(types.string),
    state: types.maybeNull(types.late(() => NavigationState)),
    params: types.maybeNull(types.frozen())
}

export const NavigationState = types.model({
    stale: types.optional(types.boolean, false),
    type: types.maybeNull(types.string),
    key: types.maybeNull(types.string),
    index: types.optional(types.number, 0),
    routeNames: types.maybeNull(types.array(types.maybeNull(types.string))),
    routes: types.maybeNull(types.array(types.model(NavigationRoute)))
})

export const NavigationPropsModel = {
    currentState: types.optional(NavigationState, {}),
    previousState: types.optional(NavigationState, {}),
    navigation: types.maybeNull(types.frozen())
}