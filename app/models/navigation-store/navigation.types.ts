import { Instance, SnapshotOut } from "mobx-state-tree"
import { NavigationPropsModel } from "./navigation.model"

export type IRoute = {
    key?: string,
    name?: string,
    params?: any,
}

export type INavigationRoute = {
    route: IRoute
}

export type INavigationRoutes = {
    route: IRoute
}

export type INavigationRouteBase = {
    key?: string,
    name?: string,
    params?: any,
    state?: INavigationState
}

export type INavigationState = {
    stale: boolean,
    type?: string,
    key?: string,
    index: number,
    routeNames?: string[],
    routes?: INavigationRouteBase[]
}

export type NavigationStore = Instance<typeof NavigationPropsModel>
export type NavigationStoreSnapshot = SnapshotOut<typeof NavigationPropsModel>
