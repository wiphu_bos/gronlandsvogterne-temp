import { types } from "mobx-state-tree"
import { IChatLastMessage } from "./chat-store.types"
import { Property } from "../../constants/firebase/fire-store.constant"

export const ChatPropsModel = {
    [Property.CHAT_ROOM_ID]: types.maybeNull(types.string),
    [Property.LAST_MESSAGE]: types.maybeNull(types.frozen<IChatLastMessage>()),
    [Property.USER]: types.maybeNull(types.frozen()),
    [Property.CREATED_DATE]: types.maybeNull(types.frozen()),
    [Property.UPDATED_DATE]: types.maybeNull(types.frozen()),
}