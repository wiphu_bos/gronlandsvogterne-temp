import { Instance, SnapshotOut } from "mobx-state-tree"
import { Property } from "../../constants/firebase/fire-store.constant"
import { IListPage, UserStore, IUser } from "../user-store"
import { IMessage } from "../../libs/gifted-chat/Models"
import { ChatStoreModel } from "./chat-store.store"
export type ChatStore = Instance<typeof ChatStoreModel>
export type ChatStoreSnapshot = SnapshotOut<typeof ChatStoreModel>

export interface IChatLastMessage extends IMessage {
    [Property.IS_REPLIED]?: boolean
}

export type IChat = {
    [Property.CHAT_ROOM_ID]?: string,
    [Property.LAST_MESSAGE]?: IChatLastMessage,
    [Property.USER]?: UserStore & IUser,
    [Property.CREATED_DATE]?: any,
    [Property.UPDATED_DATE]?: any,
    [Property.INTERLOCUTOR_PROFILES]?: (UserStore & IUser)[]
}

export type IChatListResponse = {
    [Property.CHAT_LIST]?: IChat[],
    [Property.PAGE]?: IListPage
}