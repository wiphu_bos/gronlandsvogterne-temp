import { types } from "mobx-state-tree"
import { ChatActions, ChatViews } from "./viewmodel"
import { ChatPropsModel } from "./chat-store.model"
import { StoreName } from "../../constants/app.constant"
import { SharedStoreModel } from "../shared-store/shared.store"
import { ChatServiceActions } from "./servicemodel/chat-store.services"

export const ChatModel = types.model(StoreName.Chat, ChatPropsModel)

export const ChatStoreModel = types.compose(
    SharedStoreModel,
    ChatModel,
    ChatViews,
    ChatActions,
    ChatServiceActions)

