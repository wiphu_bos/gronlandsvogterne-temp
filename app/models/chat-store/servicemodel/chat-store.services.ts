import { types, flow } from "mobx-state-tree"
import ChatServices from "../../../services/api-domain/chat.services"
import { ChatPropsModel } from "../chat-store.model"
import { IMessage } from "../../../libs/gifted-chat/Models"

export const ChatServiceActions = types.model(ChatPropsModel).actions(self => {

    const markReadChatById = (chat_room_id: string) => {
        const params = {
            chat_room_id
        }
        ChatServices.markReadChatById(params)
    }

    const fetchChatList = flow(function* (last_id: string) {
        const params = {
            last_id
        }
        return yield ChatServices.fetchChatList(params)
    })

    const fetchMessageList = flow(function* (chat_room_id: string, last_id: string) {
        const params = {
            chat_room_id,
            last_id
        }
        return yield ChatServices.fetchMessageList(params)
    })

    const createMessage = flow(function* (message: IMessage) {
        const params = {
            ...message
        }
        return yield ChatServices.createMessage(params)
    })

    const createChatRoomById = flow(function* (uid: string) {
        const params = {
            uid
        }
        return yield ChatServices.createChatRoomById(params)
    })
    const deleteChatRoomById = flow(function* (chat_room_id: string) {
        const params = {
            chat_room_id
        }
        return yield ChatServices.deleteChatRoomById(params)
    })
    const deleteMessageById = flow(function* (message_id: string) {
        const params = {
            message_id
        }
        return yield ChatServices.deleteMessageById(params)
    })
    return {
        markReadChatById,
        fetchChatList,
        createChatRoomById,
        fetchMessageList,
        createMessage,
        deleteChatRoomById,
        deleteMessageById
    }
})