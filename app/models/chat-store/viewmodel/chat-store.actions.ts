import { types } from "mobx-state-tree"
import { ChatPropsModel } from "../chat-store.model"
import { IChat } from "../chat-store.types"

export const ChatActions = types.model(ChatPropsModel).actions(self => {
    //MARK: Volatile State

    //MARK: Actions

    const bindingDataFromObject = (chatObject?: IChat) => {
        if (!chatObject) return
        const { chat_room_id, updated_date } = chatObject
        self.chat_room_id = chat_room_id,
        self.last_message = chatObject?.last_message,
        self.user = chatObject?.user
        self.updated_date = updated_date
    }

    return {
        bindingDataFromObject
    }
})