import { types } from "mobx-state-tree"

export const GeneralResourcesPropsModel = {
    version: types.maybeNull(types.string),
    generalResources: types.maybeNull(types.frozen()),
    introductions: types.maybeNull(types.frozen()),
    countries: types.maybeNull(types.frozen()),
    regions: types.maybeNull(types.frozen()),
    citiesRegions: types.maybeNull(types.frozen()),
    cities: types.maybeNull(types.frozen()),
    interests: types.maybeNull(types.frozen()),
    havdSogerHands: types.maybeNull(types.frozen()), //will fix the name later.
    tags: types.maybeNull(types.frozen()),
    notifications: types.maybeNull(types.frozen()),
}