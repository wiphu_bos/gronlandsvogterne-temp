import { types, flow } from "mobx-state-tree"
import { GeneralResourcesPropsModel } from "../general-resources.model"
import * as FirebaseUtils from "../../../utils/firebase/remote-config"
import * as AppConstant from "../../../constants/app.constant"
import * as RMCFConstant from "../../../constants/firebase/remote-config"
import * as Locale from "../../../utils/locale"
import * as FileSystem from "../../../utils/file-system"
import { RootStore } from "../../root-store"
import { IFlatlistSelection } from "../../shared-store"
import FireBaseServices from "../../../utils/firebase/authentication/auth"

export const GeneralResourcesServiceActions = types.model(GeneralResourcesPropsModel).actions(self => {
    //MARK: Volatile State


    //MARK: Helper
    const getModalContentList = (data: any[]) => {
        const objects = data as any[]
        return objects?.map(v => {
            let options = {
                key: Object.keys(v)[0],
                label: Object.values(v)[0]
            } as IFlatlistSelection
            return options
        })
    }

    //MARK: Service Actions

    const setGeneralResources = (object: any) => {
        const { GeneralResources, Introductions, Cities, RegionsCities, Regions, Countries, Interests, HavdSogerHans, Tags, Notifications } = RMCFConstant.RemoteConfigRootKey
        self.generalResources = object[GeneralResources] || null
        self.introductions = object[Introductions] || null
        self.countries = object[Countries] || null
        self.cities = object[Cities] || null
        self.citiesRegions = object[RegionsCities] || null
        self.regions = object[Regions] || null
        self.interests = object[Interests] || null
        self.havdSogerHands = object[HavdSogerHans] || null
        self.tags = object[Tags] || null
        self.notifications = object[Notifications] || null
    }

    const fetchRemoteConfig = flow(function* () {
        try {
            const object = yield FirebaseUtils.getRemoteConfigValues()
            if (object) {
                yield FileSystem.writeFile(AppConstant.GeneralResourcesJSONPath.Path, object)
                setGeneralResources(object)
            }
        } catch (e) {
        }
    })

    const fetchLocalizedGeneralResources = flow(function* () {
        try {
            let object = yield FileSystem.readFile(AppConstant.GeneralResourcesJSONPath.Path)
            if (!object) {
                object = yield Locale.getLocalizedGeneralResources()
            }
            if (object) {
                yield FileSystem.writeFile(AppConstant.GeneralResourcesJSONPath.Path, object)
                setGeneralResources(object)
            }
        } catch (e) {
        }
    })

    const fetchCurrentUser = () => {
        try {
            return FireBaseServices.fetchCurrentUser()
        } catch (e) {
            return e
        }
    }

    const initSharedInstanceProps = (rootStore: RootStore) => {
        const { getSharedStore } = rootStore
        const ageContant = AppConstant.ageConstant
        const countries = getModalContentList(self.countries)
        const cities = getModalContentList(self.cities)
        const regions = getModalContentList(self.regions)
        const havdSogerHands = getModalContentList(self.havdSogerHands)
        const interests = getModalContentList(self.interests)
        const tags = getModalContentList(self.tags)
        getSharedStore?.setAge(ageContant.age)
        getSharedStore?.setCountries(countries)
        getSharedStore?.setCities(cities)
        getSharedStore?.setRegions(regions)
        getSharedStore?.setHavdSogerHan(havdSogerHands)
        getSharedStore?.setInterest(interests)
        getSharedStore?.setTags(tags)
    }
    return {
        fetchRemoteConfig,
        fetchLocalizedGeneralResources,
        fetchCurrentUser,
        initSharedInstanceProps
    }
})

