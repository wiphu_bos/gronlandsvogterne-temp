import { types } from "mobx-state-tree"
import { GeneralResourcesViews, GeneralResourcesActions } from "./viewmodel"
import { GeneralResourcesPropsModel } from "./general-resources.model"
import { StoreName } from "../../constants/app.constant"
import { GeneralResourcesServiceActions } from "./services/general-resources.services"

const GeneralResourcesModel = types.model(StoreName.GeneralResources, GeneralResourcesPropsModel)

export const GeneralResourcesStoreModel = types.compose(
    GeneralResourcesModel,
    GeneralResourcesViews,
    GeneralResourcesActions,
    GeneralResourcesServiceActions)

