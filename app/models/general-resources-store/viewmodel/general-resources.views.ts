import { types } from "mobx-state-tree"
import { GeneralResourcesPropsModel } from "../general-resources.model"
import * as DataUtils from "../../../utils/data.utils"
import * as Utils from "../../../utils"
import { ISelect } from "../../user-store/user.types"

export const GeneralResourcesViews = types.model(GeneralResourcesPropsModel)
    .views(self => ({
        getValues(rootKey?: string, childKeyORShareKey?: string | boolean, isChildDotNotation: boolean = false): any {
            const notExistingKey = "NOT_EXISTING_KEY"
            if (this.getGeneralResources) {
                if (childKeyORShareKey) {
                    if (childKeyORShareKey == true) {
                        return Utils.refDotNotation(this.getGeneralResources, rootKey) || notExistingKey
                    } else if (this.getGeneralResources[rootKey]) {
                        const resources = this.getGeneralResources[rootKey]
                        if (isChildDotNotation) return Utils.refDotNotation(resources, childKeyORShareKey) || notExistingKey
                        return resources[childKeyORShareKey]
                    }
                }
            }
            return notExistingKey
        },
        get getGeneralResources(): any {
            return self.generalResources
        },
        get getCities(): ISelect[] {
            return DataUtils.getSelectObject(self.cities)
        },
        get getCitiesRegions(): ISelect[] {
            return DataUtils.getSelectObject(self.citiesRegions)
        },
        get getRegions(): ISelect[] {
            return DataUtils.getSelectObject(self.regions)
        },
        get getCountries(): ISelect[] {
            return DataUtils.getSelectObject(self.countries)
        },
        get getTags(): ISelect[] {
            return DataUtils.getSelectObject(self.tags)
        },
        get getInterests(): ISelect[] {
            return DataUtils.getSelectObject(self.interests)
        },
        get getHavdSogerHans(): any {
            return self.havdSogerHands
        },
        getIntroductions(rootKey: string): any[] {
            return self.introductions && self.introductions[rootKey]
        },
        getNotification(rootKey: string): string {
            return self.notifications && Utils.refDotNotation(self.notifications, rootKey)
        },
    }))