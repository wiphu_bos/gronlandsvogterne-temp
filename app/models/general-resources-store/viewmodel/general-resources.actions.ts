import { types } from "mobx-state-tree"
import { GeneralResourcesPropsModel } from "../general-resources.model"

export const GeneralResourcesActions = types.model(GeneralResourcesPropsModel).actions(self => {
    const setGeneralResources = (value: any) => {
        self.generalResources = value
    }
    const setIntroductions = (value: any) => {
        self.introductions = value
    }
    const setCities = (value: any) => {
        self.cities = value
    }
    const setCountries = (value: any) => {
        self.countries = value
    }
    const setInterests = (value: any) => {
        self.interests = value
    }
    const setHavdSogerHands = (value: any) => {
        self.havdSogerHands = value
    }
    const setTags = (value: any) => {
        self.tags = value
    }
    const setNotification = (value: any) => {
        self.notifications = value
    }
    return {
        setGeneralResources,
        setIntroductions,
        setCities,
        setCountries,
        setInterests,
        setHavdSogerHands,
        setTags,
        setNotification
    }
})

