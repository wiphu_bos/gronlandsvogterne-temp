import { types } from "mobx-state-tree"
import { MenuViews, MenuActions } from "./viewmodel"
import { MenuPropsModel } from "./menu.model"
import { StoreName } from "../../constants/app.constant"


const MenuModel = types.model(StoreName.Menu, MenuPropsModel)


export const MenuStoreModel = types.compose(
    MenuModel,
    MenuViews,
    MenuActions)

