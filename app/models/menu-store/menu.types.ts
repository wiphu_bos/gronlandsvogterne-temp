import { Instance, SnapshotOut } from "mobx-state-tree"
import { MenuStoreModel } from "./menu.store"

export type MenuStore = Instance<typeof MenuStoreModel>
export type MenuStoreSnapshot = SnapshotOut<typeof MenuStoreModel>

export type IMenuResponse = {
    token?: string
    isLoggedOut?: boolean
}