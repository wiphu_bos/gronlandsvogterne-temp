import { types } from "mobx-state-tree"
import { MenuPropsModel } from "../menu.model"
export const MenuViews = types.model(MenuPropsModel)
    .views(self => ({

        get getName(): string {
            return self.myname
        },

        get getLang(): string {
            return self.mylang
            // return 'hello'
        },
    }))