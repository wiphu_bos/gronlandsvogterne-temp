import { types } from "mobx-state-tree"
import { MenuPropsModel } from "../menu.model"

export const MenuActions = types.model(MenuPropsModel).actions(self => {

    const setName = (value: string | undefined) => {
        self.myname = value
    }
    const setLang = (value: string | undefined) => {
        self.mylang = value
    }

    return { setName, setLang }
})