import { types } from "mobx-state-tree"
import { INotificationNavigation, INotificationMiscData } from "./notification-store.types"
import { Property } from "../../constants/firebase/fire-store.constant"

export const NotificationPropsModel = {
    [Property.NOTIFICATION_ID]: types.maybeNull(types.string),
    [Property.TITLE]: types.maybeNull(types.string),
    [Property.BODY]: types.maybeNull(types.string),
    [Property.TOPIC]: types.maybeNull(types.string),
    [Property.DATA]: types.maybeNull(types.frozen()),
    [Property.NAVIGATION]: types.maybeNull(types.frozen<INotificationNavigation>()),
    [Property.MISC]: types.maybeNull(types.frozen<INotificationMiscData>()),
} 