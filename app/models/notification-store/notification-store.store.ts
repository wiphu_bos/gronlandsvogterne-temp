import { types } from "mobx-state-tree"
import { NotificationViews, NotificationActions } from "./viewmodel"
import { NotificationPropsModel } from "./notification-store.model"
import { StoreName } from "../../constants/app.constant"
import { NotificationServiceActions } from "./servicemodel"
import { SharedStoreModel } from "../shared-store"


export const NotificationModel = types.model(StoreName.NotificationList, NotificationPropsModel)

export const NotificationStoreModel = types.compose(
    SharedStoreModel,
    NotificationModel,
    NotificationViews,
    NotificationActions,
    NotificationServiceActions)

