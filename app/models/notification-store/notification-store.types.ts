import { Instance, SnapshotOut } from "mobx-state-tree"
import { NotificationStoreModel } from "./notification-store.store"
import { Property } from "../../constants/firebase/fire-store.constant"
export type NotificationStore = Instance<typeof NotificationStoreModel>
export type NotificationStoreSnapshot = SnapshotOut<typeof NotificationStoreModel>

export type INotificationNavigation = {
    id?: string,
    layout?: string,
    subLayout?: string
}

export type INotificationMiscData = {
    date?: any,
    [Property.IS_READ]?: boolean,
}

export type INotificationResponse = {
    [Property.NOTIFICATION_ID]?: string,
    [Property.TOPIC]?: string,
    [Property.DATA]?: any,
    [Property.NAVIGATION]?: INotificationNavigation,
    [Property.MISC]?: INotificationMiscData
}

export interface INotification extends INotificationResponse {
    [Property.TITLE]?: string,
    [Property.BODY]?: string
}
