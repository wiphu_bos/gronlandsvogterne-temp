import { Instance, SnapshotOut } from "mobx-state-tree"
import { UserStoreModel } from "./user.store"
import { Property, MobileProperty, MediaProperty } from "../../constants/firebase/fire-store.constant"
import { Purchase } from "react-native-iap"
export type UserStore = Instance<typeof UserStoreModel>
export type UserStoreSnapshot = SnapshotOut<typeof UserStoreModel>

export type ISelect = {
    key?: string,
    value?: string
}

export type IFireStorage = {
    [MediaProperty.STORAGE_PATH]?: string,
    [MediaProperty.FILE_NAME]?: string,
    [MediaProperty.URL]?: string
}

export type IUser = {
    [Property.UID]?: string,
    [Property.DUDE_ID]?: string,
    [Property.DUDETTE_ID]?: string,
    [Property.USER_ID]?: string,
    [Property.FULL_NAME]?: string,
    [Property.EMAIL]?: string,
    [Property.COUNTRY]?: string,
    [Property.CITY]?: string,
    [Property.AGE]?: string,
    [Property.DESCRIPTION]?: string,
    [MobileProperty.PASSWORD]?: string,
    [MobileProperty.CONFIRM_PASSWORD]?: string,
    [Property.HAVD_SOGER_HAN_LIST]?: string[],
    [Property.INTEREST_LIST]?: string[],
    [Property.TAG_LIST]?: string[],
    [Property.ROLE]?: string,
    [Property.PROFILE_IMAGE]?: IFireStorage,
    [Property.GALLERY_IMAGE_LIST]?: IFireStorage[],
    [Property.IS_HIGHLIGHT]?: boolean,
    [Property.CREATED_DATE]?: any,
    [Property.UPDATED_DATE]?: any,
    [Property.IS_VERIFIED]?: boolean,
    [Property.IS_FAVOURITE]?: boolean,
    [Property.VIEW_COUNT]?: number,
    [Property.COMMENT_COUNT]?: number,
    [Property.CHAT_COUNT]?: number,
    [Property.UNREAD_COMMENT_COUNT]?: number,
    [Property.UNREAD_CHAT_COUNT]?: number,
    [Property.STATUS]?: string,
    [MobileProperty.PROFILE_IMAGE_TO_REMOVE]?: IFireStorage[],
    [MobileProperty.GALLERY_IMAGE_LIST_TO_REMOVE]?: IFireStorage[],
    [MobileProperty.COMPARE_PROFILE_IMAGE]?: IFireStorage[],
    [MobileProperty.COMPARE_GALLERY_IMAGE_LIST]?: IFireStorage[],
    [MobileProperty.EXPIRY_DATE_COUNT]?: number,
    [MobileProperty.IS_TIMER_SET]?: boolean,
    [Property.IS_SUBSCRIPTION]?: boolean,
    [Property.EXPIRY_DATE]?: any
}

export type IFavouriteResponse = {
    [Property.CREATED_DATE]?: any,
    [Property.DUDE_ID]?: string,
    [Property.FULL_NAME]?: string,
    [Property.IS_FAVOURITE]?: boolean,
    [Property.PROFILE_IMAGE]?: IFireStorage,
    [Property.USER_ID]?: string
}

export type IListPage = {
    [Property.PAGE_SIZE]?: number,
    [Property.IS_LAST_PAGE]?: boolean,
    [Property.LAST_ID]?: string,
}

export type IMyDudeListResponse = {
    [Property.MY_DUDE_LIST]?: IUser[],
    [Property.PAGE]?: IListPage
}

export type IFavouriteListResponse = {
    [Property.FAVOURITE_LIST]?: IUser[],
    [Property.PAGE]?: IListPage
}

export type IPublishDudeWithPayment = {
    [Property.DUDE_DETAILS]: IUser,
    [Property.PAYMENT_DETAILS]: IPaymentResponse,
}

export type IPaymentResponse = {
    [Property.CREATED_DATE]?: any,
    [Property.EXPIRY_DATE]?: any,
    [Property.IS_EXPIRED]?: boolean,
    [Property.SKU_ID]?: string,
    [Property.TRANSACTION_ID]?: string,
    [Property.TRANSACTION_RECEIPT]?: string,
    [Property.USER_ID]?: string
} & Purchase