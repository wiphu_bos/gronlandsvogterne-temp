import { types, flow } from "mobx-state-tree"
import { UserPropsModel } from "../user.model"
import UserServices from "../../../services/api-domain/user.services"

export const UserDetailsServiceActions = types.model(UserPropsModel).actions(self => {

    const fetchMyProfile = flow(function* () {
        return yield UserServices.fetchMyProfile()
    })
    const updateDeviceToken = flow(function* (token: string) {
        return yield UserServices.updateDeviceToken(token)
    })

    const deleteMyUser = flow(function* () {
        return yield UserServices.deleteMyUser()
    })

    const signOut = flow(function* () {
        if (!self.device_token) return
        return yield UserServices.signOut(self.device_token)
    })

    const updateSeekingGuy = flow(function* (status: boolean) {
        const params = {
            status: status
        }
        return yield UserServices.patchSeekingGuy(params)
    })

    return {
        fetchMyProfile,
        updateDeviceToken,
        signOut,
        updateSeekingGuy,
        deleteMyUser
    }
})