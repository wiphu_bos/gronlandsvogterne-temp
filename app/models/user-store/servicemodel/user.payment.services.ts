import { types, flow } from "mobx-state-tree"
import { UserPropsModel } from "../user.model"
import { SKU_HIGHLIGT, SKU_PREMIUM_ACCOUNT, CANCEL_SUBSCRIPTION_URL } from "../../../constants/in-app-billing/in-app-billing.constant"
import PaymentServices from "../../../services/api-domain/payment.services"
import { IPaymentResponse } from "../user.types"
import { Property } from "../../../constants/firebase/fire-store.constant"

export const UserPaymentServiceActions = types.model(UserPropsModel).actions(self => {
    const purchaseHighlightDudeIAP = flow(function* () {
        return yield PaymentServices.purchaseHighlightDudeIAP(SKU_HIGHLIGT)
    })
    const subscribePremiumAccountIAP = flow(function* () {
        return yield PaymentServices.subscribePremiumAccountIAP(SKU_PREMIUM_ACCOUNT)
    })

    const confirmSubscribePremiumAccount = flow(function* (payment: IPaymentResponse) {
        const params = {
            [Property.SKU_ID]: payment?.productId,
            [Property.TRANSACTION_ID]: payment?.transactionId,
            [Property.TRANSACTION_RECEIPT]: payment?.transactionReceipt ? payment?.transactionReceipt : payment?.purchaseToken
        }
        return yield PaymentServices.confirmSubscribePremiumAccount(params)
    })

    const cancelSubscribePremiumAccount = flow(function* () {
        return yield PaymentServices.cancelSubscribePremiumAccount(CANCEL_SUBSCRIPTION_URL)
    })

    const invalidateDudeHighlight = flow(function* (uid?: string) {
        const params = {
            uid
        }
        return yield PaymentServices.invalidateDudeHighlight(params)
    })
    const invalidatePremiumAccount = flow(function* () {
        return yield PaymentServices.invalidatePremiumAccount()
    })

    return {
        purchaseHighlightDudeIAP,
        subscribePremiumAccountIAP,
        cancelSubscribePremiumAccount,
        confirmSubscribePremiumAccount,
        invalidateDudeHighlight,
        invalidatePremiumAccount
    }
})