import UserServices from "../../../services/api-domain/user.services"
import DudeServices from "../../../services/api-domain/dude.services"
import { flow, types } from "mobx-state-tree"
import { UserPropsModel } from "../user.model"
export const UserDudeDetailsServiceActions = types.model(UserPropsModel).actions(self => {

    const fetchFavouriteList = flow(function* (last_id?: string, type?: string) {
        const params = {
            last_id,
            type
        }
        return yield UserServices.fetchFavouriteList(params)
    })
    const fetchDudeProfileDetail = flow(function* (uid?: string) {
        const params = {
            uid
        }
        return yield DudeServices.getDudeProfile(params)
    })

    const fetchDudetteProfileDetail = flow(function* (uid?: string) {
        const params = {
            uid
        }
        return yield UserServices.getDudetteDetail(params)
    })

    const fetchMyDudeList = flow(function* (last_id: string) {
        const params = {
            last_id
        }
        return yield UserServices.fetchMyDudeList(params)
    })

    const deleteDude = flow(function* (uid?: string) {
        const params = {
            uid
        }
        return yield DudeServices.deleteDude(params)
    })
    const changeDudeStatus = flow(function* (uid?: string, status?: string) {
        const params = {
            uid,
            status
        }
        return yield DudeServices.changeStatus(params)
    })

    return {
        fetchDudeProfileDetail,
        fetchMyDudeList,
        fetchDudetteProfileDetail,
        deleteDude,
        changeDudeStatus,
        fetchFavouriteList
    }
})