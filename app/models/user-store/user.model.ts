import { types } from "mobx-state-tree"
import { ISelect, IUser } from "./user.types"
import { IUploadFileData } from "../../utils/firebase/fire-storage/fire-storage.types"
import { Image } from "react-native-image-crop-picker"
import { Property, MobileProperty } from "../../constants/firebase/fire-store.constant"
import { INotification } from "../notification-store/notification-store.types"

/*
    Some properties'name are as same as the backend.
*/

export const UserPropsModel = {
    [Property.UID]: types.maybeNull(types.string),

    //As a general object can get this variable.
    [Property.DUDETTE_ID]: types.maybeNull(types.string),
    [Property.DUDE_ID]: types.maybeNull(types.string),

    //As a dude object can get this variable (his own creator_id).
    [Property.USER_ID]: types.maybeNull(types.string),
    [Property.IS_HIGHLIGHT]: types.optional(types.boolean, true),

    [Property.FULL_NAME]: types.maybeNull(types.string),
    [Property.EMAIL]: types.maybeNull(types.string),
    [MobileProperty.PASSWORD]: types.maybeNull(types.string),
    [MobileProperty.CONFIRM_PASSWORD]: types.maybeNull(types.string),
    [Property.DESCRIPTION]: types.maybeNull(types.string),
    [Property.STATUS]: types.maybeNull(types.string),

    //As a user object can get this variable.
    [Property.COUNTRY]: types.maybeNull(types.frozen<ISelect>()),
    [Property.IS_VERIFIED]: types.maybeNull(types.boolean),

    [Property.CITY]: types.maybeNull(types.frozen<ISelect>()),
    [Property.HAVD_SOGER_HAN_LIST]: types.optional(types.frozen<ISelect[]>(), []),
    [Property.INTEREST_LIST]: types.optional(types.frozen<ISelect[]>(), []),
    [Property.TAG_LIST]: types.optional(types.frozen<ISelect[]>(), []),
    [Property.AGE]: types.maybeNull(types.frozen<ISelect>()),
    [Property.ROLE]: types.maybeNull(types.string),
    [Property.CREATED_DATE]: types.maybeNull(types.frozen()),
    [Property.UPDATED_DATE]: types.maybeNull(types.frozen()),
    [Property.DEVICE_TOKEN]: types.maybeNull(types.string),
    [Property.VIEW_COUNT]: types.optional(types.number, 0),
    [Property.COMMENT_COUNT]: types.optional(types.number, 0),
    [Property.CHAT_COUNT]: types.optional(types.number, 0),
    [Property.UNREAD_COMMENT_COUNT]: types.optional(types.number, 0),
    [Property.UNREAD_CHAT_COUNT]: types.optional(types.number, 0),
    [Property.IS_FAVOURITE]: types.optional(types.boolean, false),
    [Property.PROFILE_IMAGE]: types.optional(types.frozen<(Partial<IUploadFileData & Image>)[]>(), []),
    [Property.GALLERY_IMAGE_LIST]: types.optional(types.frozen<(Partial<IUploadFileData & Image>)[]>(), []),
    [MobileProperty.MY_DUDE_LIST]: types.optional(types.frozen<Partial<IUser>[]>(), []),
    [MobileProperty.NOTIFICATION_LIST]: types.optional(types.frozen<Partial<INotification>[]>(), []),
    [MobileProperty.FAVOURITE_DUDE_LIST]: types.optional(types.frozen<Partial<IUser>[]>(), []),
    [MobileProperty.FAVOURITE_DUDETTE_LIST]: types.optional(types.frozen<Partial<IUser>[]>(), []),
    [Property.IS_SEEKING_GUY]: types.optional(types.boolean, false),

    //Payment
    [Property.IS_SUBSCRIPTION]: types.optional(types.boolean, false),
    [Property.EXPIRY_DATE]: types.maybeNull(types.frozen()),

    //Time tricking handling
    [MobileProperty.EXPIRY_DATE_COUNT]: types.optional(types.number, 0),
    [MobileProperty.IS_TIMER_SET]: types.optional(types.boolean, false),
    [MobileProperty.EXPIRY_TIMER]: types.maybeNull(types.frozen()),

    //images handling
    [MobileProperty.COMPARE_PROFILE_IMAGE]: types.optional(types.frozen<(Partial<IUploadFileData & Image>)[]>(), []),
    [MobileProperty.COMPARE_GALLERY_IMAGE_LIST]: types.optional(types.frozen<(Partial<IUploadFileData & Image>)[]>(), []),
    [MobileProperty.PROFILE_IMAGE_TO_REMOVE]: types.optional(types.frozen<(Partial<IUploadFileData & Image>)[]>(), []),
    [MobileProperty.GALLERY_IMAGE_LIST_TO_REMOVE]: types.optional(types.frozen<(Partial<IUploadFileData & Image>)[]>(), []),
}