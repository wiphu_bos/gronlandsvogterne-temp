import { types } from "mobx-state-tree"
import { UserViews, UserActions } from "./viewmodel"
import { UserPropsModel } from "./user.model"
import { StoreName } from "../../constants/app.constant"
import { UserServiceActions } from "./servicemodel/user.main.services"


export const UserModel = types.model(StoreName.User, UserPropsModel)

export const UserStoreModel = types.compose(
    UserModel,
    UserViews,
    UserActions,
    UserServiceActions)

