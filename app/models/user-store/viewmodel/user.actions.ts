import { types, isAlive } from "mobx-state-tree"
import { UserPropsModel } from "../user.model"
import { IUser, ISelect, UserStore } from "../user.types"
import { Image } from "react-native-image-crop-picker"
import { IUploadFileData } from "../../../utils/firebase/fire-storage/fire-storage.types"
import { RootStore } from "../../root-store/root.types"
import * as DataUtils from "../../../utils/data.utils"
import BackgroundTimer from 'react-native-background-timer'
import moment from "moment"
import { UserType } from "../../../constants/app.constant"
import { INotification } from "../../notification-store/notification-store.types"
import { flow } from "mobx"

export const UserActions = types.model(UserPropsModel).actions(self => {
    //MARK: Volatile State

    //MARK: Actions
    const setUser = (value: Partial<IUser>) => {
        self.uid = value.uid
        self.full_name = value.full_name
    }
    const setFullName = (value: string) => {
        self.full_name = value
    }
    const setEmail = (value: string) => {
        self.email = value?.trim()
    }
    const setCountry = (value: ISelect) => {
        self.country = value
    }
    const setCity = (value: ISelect) => {
        self.city = value
    }
    const setHavdSogerHanList = (value: ISelect[]) => {
        self.havd_soger_han_list = value
    }
    const setInterestList = (value: ISelect[]) => {
        self.interest_list = value
    }
    const setTagList = (value: ISelect[]) => {
        self.tag_list = value
    }
    const setAge = (value: ISelect) => {
        self.age = value
    }
    const setDescription = (value: string) => {
        self.description = value
    }
    const setPassword = (value: string) => {
        self.password = value
    }
    const setConfirmPassword = (value: string) => {
        self.confirmPassword = value
    }
    const setUserId = (value: string) => {
        self.uid = value
    }
    const setCreatorId = (value: string) => {
        self.user_id = value
    }
    const setIsVerified = (value: boolean) => {
        self.is_verified = value
    }
    const setIsFavourite = (value: boolean) => {
        self.is_favourite = value
    }
    const setIsHighlight = (value: boolean) => {
        self.is_highlight = value
    }
    const setIsSubscription = (value: boolean) => {
        self.is_subscription = value
    }
    const setDeviceToken = (value: string) => {
        self.device_token = value
    }
    const setProfileImage = (value: (Partial<Image | IUploadFileData>)[]) => {
        self.profile_image = value
    }
    const setGalleryImages = (value: (Partial<Image | IUploadFileData>)[]) => {
        self.gallery_image_list = value
    }
    const setCompareProfileImage = (value: (Partial<Image | IUploadFileData>)[]) => {
        self.compare_profile_image = value
    }
    const setCompareGalleryImages = (value: (Partial<Image | IUploadFileData>)[]) => {
        self.compare_gallery_image_list = value
    }
    const setProfileImageToRemove = () => {
        if (self.profile_image?.length === 0 || self.compare_profile_image?.length === 0) return
        self.profile_image_to_remove = self.compare_profile_image?.filter(x => !self.profile_image?.includes(x))
    }
    const setGalleryImagesToRemove = (index: number) => {
        if (self.gallery_image_list?.length === 0 || self.compare_gallery_image_list?.length === 0) return
        const existingList = self.gallery_image_list_to_remove
        const filteredRemove = self.compare_gallery_image_list?.filter(e => e.file_name === self.gallery_image_list[index]?.file_name)
        if (filteredRemove.length === 0) return
        self.gallery_image_list_to_remove = existingList?.concat(filteredRemove)
    }
    const setStatus = (value: string) => {
        self.status = value
    }
    const setMyDudeList = (value: Partial<IUser>[]) => {
        self.myDudeList = value
    }
    const addMyDudeToList = (value: Partial<IUser>) => {
        let list = self.myDudeList?.slice() || []
        list.push(value)
        setMyDudeList(list)
    }
    const setViewCount = (value: number) => {
        self.view_count = value
    }
    const setCommentCount = (value: number) => {
        self.comment_count = value
    }
    const setChatCount = (value: number) => {
        self.chat_count = value
    }

    const setUnReadCommentCount = (value: number) => {
        self.unread_comment_count = value
    }

    const setUnReadChatCount = (value: number) => {
        self.unread_chat_count = value
    }
    const setExpiryDate = (value: any) => {
        self.expiry_date = value
    }
    const setExpiryDateCount = (value: number) => {
        self.expiryDateCount = value
    }
    const saveMyDudeToList = (rootStore: RootStore, value: Partial<IUser>) => {
        let list = self.myDudeList?.slice() || []
        const foundItemIndex = list?.findIndex(e => e.uid === value.uid)
        if (foundItemIndex > -1) {
            const previousValue = list[foundItemIndex]
            const obj = DataUtils.getUserProfileObject(rootStore, previousValue, value)
            list[foundItemIndex] = obj
            setMyDudeList(list)
        } else {
            addMyDudeToList(value)
        }
    }
    const removeMyDudeFromList = (value: IUser) => {
        let list = self.myDudeList?.slice() || []
        const foundItemIndex = list?.findIndex(e => e.uid === value.uid)
        if (list.length > 0 && foundItemIndex > -1) list.splice(foundItemIndex, 1)
        self.myDudeList = list
    }
    const setFavDudeList = (value: Partial<IUser>[]) => {
        self.favDudeList = value
    }
    const addFavDudeToList = (value: Partial<IUser>) => {
        let list = self.favDudeList?.slice() || []
        list.push(value)
        setFavDudeList(list)
    }
    const saveFavDudeToList = (value: Partial<IUser>) => {
        let list = self.favDudeList?.slice() || []
        const foundItemIndex = list?.findIndex(e => e.uid === value.uid)
        if (foundItemIndex > -1) {
            list[foundItemIndex] = value
            setFavDudeList(list)
        } else {
            addFavDudeToList(value)
        }
    }
    const removeFavDudeFromList = (value: IUser) => {
        let list = self.favDudeList?.slice() || []
        const foundItemIndex = list?.findIndex(e => e.uid === value.uid)
        if (list.length > 0 && foundItemIndex > -1) list.splice(foundItemIndex, 1)
        self.favDudeList = list
    }

    const setFavDudetteList = (value: Partial<IUser>[]) => {
        self.favDudetteList = value
    }
    const addFavDudetteToList = (value: Partial<IUser>) => {
        let list = self.favDudetteList?.slice() || []
        list.push(value)
        setFavDudetteList(list)
    }
    const saveFavDudetteToList = (value: Partial<IUser>) => {
        let list = self.favDudetteList?.slice() || []
        const foundItemIndex = list?.findIndex(e => e.uid === value.uid)
        if (foundItemIndex > -1) {
            list[foundItemIndex] = value
            setFavDudetteList(list)
        } else {
            addFavDudetteToList(value)
        }
    }
    const removeFavDudetteFromList = (value: IUser) => {
        let list = self.favDudetteList?.slice() || []
        const foundItemIndex = list?.findIndex(e => e.uid === value.uid)
        if (list.length > 0 && foundItemIndex > -1) list.splice(foundItemIndex, 1)
        self.favDudetteList = list
    }

    const setIsSeekingGuy = (value: boolean) => {
        self.is_seeking_guy = value
    }

    const setIsTimerSet = (rootStore?: RootStore, value?: boolean) => {
        self.isTimerSet = value
        if (value && self.expiry_date) {
            runInterval(rootStore)
        } else {
            invalidateTimer()
        }
    }

    const runInterval = async (rootStore?: RootStore) => {
        if (self.expiry_timer) return
        const timeTickingProcess = async () => {
            const serverDate = moment(rootStore?.getSharedStore?.getServerTime)
            const expiryDate = moment(self.expiry_date)
            if (!expiryDate) return setIsTimerSet(null, false)  //safe if no expiryDate
            const isTimeUp = serverDate.isSameOrAfter(expiryDate)
            if (isTimeUp) {
                self.is_highlight = !isTimeUp
                setIsTimerSet(null, !isTimeUp)
                await rootStore?.getUserStore?.invalidateDudeHighlight(self.uid) //dudeId
                return
            }
            isAlive(self) ? self.expiryDateCount = moment.duration(expiryDate.diff(serverDate)).asMilliseconds() : invalidateTimer()
        }
        await timeTickingProcess()
        if (!self.expiry_timer) {
            self.expiry_timer = BackgroundTimer.setInterval(async () => await timeTickingProcess(), 1000)
            //self.expiry_timer = BackgroundTimer.setInterval(async () => await timeTickingProcess(), 60 * 1000)
        }
    }

    const invalidateTimer = () => {
        if (self.expiry_timer)
            BackgroundTimer.clearInterval(self.expiry_timer)
    }

    const _setReadToNotificationByIndex = (existingList: INotification[], index: number) => {
        const targetItem = existingList[index]
        targetItem.misc.is_read = true
        if (self.notificationList?.length > 0) {
            existingList[index] = targetItem
            self.notificationList = existingList
        }
    }

    const setNotificationList = (notificationList?: INotification[]) => self.notificationList = notificationList
    const addNotificationToList = (notification?: INotification) => {
        const list = self.notificationList?.slice() || []
        const isExist = list && list.findIndex(e => e.notification_id === notification.notification_id) > -1
        if (!isExist) list.splice(0, 0, notification)
        self.notificationList = list
    }
    const setReadNotificationById = (id?: string) => {
        if (!id) return
        const tempList = self.notificationList
        const existingList = tempList?.slice() // Slice to disobserve property
        const foundItemIndex = existingList?.findIndex(v => v.notification_id === id)
        if (foundItemIndex === -1) return
        _setReadToNotificationByIndex(existingList, foundItemIndex)
    }
    const setReadNotificationByTopic = (topic?: string) => {
        if (!topic) return
        const tempList = self.notificationList
        const existingList: INotification[] = tempList?.slice() // Slice to disobserve property
        existingList?.forEach((v, i) => {
            if (v.topic === topic) _setReadToNotificationByIndex(existingList, i)
        })
    }


    /*
        Why we assign one - by - one variable because try to preserve observable property
    */

    const bindingDataFromObject = (dudeProfileObject?: any, type?: UserType) => {
        if (!dudeProfileObject) return

        const profile: UserStore = { ...dudeProfileObject }
        setUserId(profile?.uid)
        setFullName(profile?.full_name)
        setEmail(profile?.email)
        setCountry(profile?.country)
        setCity(profile?.city)
        setAge(profile?.age)
        setDescription(profile?.description)
        setHavdSogerHanList(profile?.havd_soger_han_list)
        setInterestList(profile?.interest_list)
        setTagList(profile?.tag_list)
        setProfileImage(profile?.profile_image)
        setGalleryImages(profile?.gallery_image_list)
        setCompareProfileImage(profile?.compare_profile_image)
        setCompareGalleryImages(profile?.compare_gallery_image_list)
        setViewCount(profile?.view_count)
        setCommentCount(profile?.comment_count)
        setChatCount(profile?.chat_count)
        setUnReadChatCount(profile?.unread_chat_count)
        setUnReadCommentCount(profile?.unread_comment_count)
        setStatus(profile?.status)
        setIsFavourite(profile?.is_favourite)
        setExpiryDate(profile?.expiry_date)

        if (type === UserType.Dude) {
            setCreatorId(profile?.user_id)
            setIsHighlight(profile?.is_highlight)
        } else if (type === UserType.Me) {
            setIsVerified(profile?.is_verified)
            setIsSubscription(profile?.is_subscription)
            setIsSeekingGuy(profile?.is_seeking_guy)
        }
    }

    return {
        setUser,
        setFullName,
        setEmail,
        setCity,
        setCountry,
        setAge,
        setPassword,
        setConfirmPassword,
        setUserId,
        setCreatorId,
        setIsVerified,
        setHavdSogerHanList,
        setInterestList,
        setTagList,
        setDeviceToken,
        setDescription,
        setIsHighlight,
        setProfileImage,
        setGalleryImages,
        setStatus,
        setMyDudeList,
        addMyDudeToList,
        removeMyDudeFromList,
        saveMyDudeToList,
        setFavDudeList,
        addFavDudeToList,
        removeFavDudeFromList,
        saveFavDudeToList,
        setFavDudetteList,
        addFavDudetteToList,
        removeFavDudetteFromList,
        saveFavDudetteToList,
        setIsFavourite,
        setViewCount,
        setCommentCount,
        setChatCount,
        setUnReadCommentCount,
        setUnReadChatCount,
        bindingDataFromObject,
        setProfileImageToRemove,
        setGalleryImagesToRemove,
        setCompareProfileImage,
        setCompareGalleryImages,
        setIsSeekingGuy,
        setIsTimerSet,
        runInterval,
        setIsSubscription,
        setExpiryDate,
        setExpiryDateCount,
        setNotificationList,
        addNotificationToList,
        setReadNotificationById,
        setReadNotificationByTopic
    }
})