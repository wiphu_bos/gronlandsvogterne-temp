import { types } from "mobx-state-tree"
import { UserPropsModel } from "../user.model"
import { ISelect, IUser, IFireStorage } from "../user.types"
import { IUploadFileData } from "../../../utils/firebase/fire-storage/fire-storage.types"
import { Image } from "react-native-image-crop-picker"
import * as DataUtils from "../../../utils/data.utils"
import { INotification } from "../../notification-store/notification-store.types"
export const UserViews = types.model(UserPropsModel)
    .views(self => ({
        get getUserId(): string {
            return self.uid
        },
        get getCreatorId(): string {
            return self.user_id
        },
        get getFullName(): string {
            return self.full_name
        },
        get getEmail(): string {
            return self.email
        },
        get getCountry(): ISelect {
            return self.country
        },
        get getCity(): ISelect {
            return self.city
        },
        get getHavdSogerHanList(): ISelect[] {
            return self.havd_soger_han_list
        },
        get getInterestList(): ISelect[] {
            return self.interest_list
        },
        get getTagList(): ISelect[] {
            return self.tag_list
        },
        get getAge(): ISelect {
            return self.age
        },
        get getPassword(): string {
            return self.password
        },
        get getDescription(): string {
            return self.description
        },
        get getConfirmPassword(): string {
            return self.confirmPassword
        },
        get getIsVerified(): boolean {
            return self.is_verified
        },
        get getIsFavourite(): boolean {
            return self.is_favourite
        },
        get getDeviceToken(): string {
            return self.device_token
        },
        get getIsHighlight(): boolean {
            return self.is_highlight
        },
        get getShouldUploadProfileImage(): (Partial<IUploadFileData & Image>)[] {
            const isSame = DataUtils.isEqual(self.profile_image, self.compare_profile_image)
            return !isSame ? self.profile_image?.filter(x => !self.compare_profile_image?.includes(x)) : []
        },
        get getShouldUploadGalleryImages(): (Partial<IUploadFileData & Image>)[] {
            const isSame = DataUtils.isEqual(self.gallery_image_list, self.compare_gallery_image_list)
            return !isSame ? self.gallery_image_list?.filter(x => !self.compare_gallery_image_list?.includes(x)) : []
        },
        get getProfileImageToUpdateDetails(): IFireStorage {
            return self.profile_image?.length > 0 && {
                file_name: self.profile_image[0].file_name,
                storage_path: self.profile_image[0].storage_path,
                url: self.profile_image[0].url
            }
        },
        get getGalleryImageToUpdateDetails(): IFireStorage[] {
            const isSame = DataUtils.isEqual(self.gallery_image_list, self.compare_gallery_image_list)
            const allGalleryImageList = (self.gallery_image_list?.length > 0 && !isSame) ?
                self.gallery_image_list.concat(self.compare_gallery_image_list) : self.gallery_image_list
            const filteredFromRemoveList = allGalleryImageList?.filter(x => !self.gallery_image_list_to_remove?.includes(x))
            return filteredFromRemoveList?.length > 0 && filteredFromRemoveList?.map(e => {
                return {
                    file_name: e.file_name,
                    storage_path: e.storage_path,
                    url: e.url
                } as IFireStorage
            })
        },
        get getProfileImage(): (Partial<IUploadFileData & Image>)[] {
            return self.profile_image
        },
        get getGalleryImages(): (Partial<IUploadFileData & Image>)[] {
            return self.gallery_image_list
        },
        get getGalleryImagesToRemove(): (Partial<IUploadFileData & Image>)[] {
            return self.gallery_image_list_to_remove
        },
        get getProfileImagesToRemove(): (Partial<IUploadFileData & Image>)[] {
            return self.profile_image_to_remove
        },
        get getStatus(): string {
            return self.status
        },
        get getMyDudeList(): Partial<IUser>[] {
            return self.myDudeList
        },
        get getFavDudeList(): Partial<IUser>[] {
            return self.favDudeList
        },
        get getFavDudetteList(): Partial<IUser>[] {
            return self.favDudetteList
        },
        get getViewCount(): number {
            return self.view_count
        },
        get getCommentCount(): number {
            return self.comment_count
        },
        get getChatCount(): number {
            return self.chat_count
        },
        get getUnReadCommentCount(): number {
            return self.unread_comment_count
        },
        get getUnReadChatCount(): number {
            return self.unread_chat_count
        },
        getMyDudeListByUID(uid: string): Partial<IUser> {
            const user = self.myDudeList?.filter(e => e.uid === uid)
            return user && user[0] || null
        },
        getFavListByUID(uid: string): Partial<IUser> {
            const user = self.favDudeList?.filter(e => e.uid === uid)
            return user && user[0] || null
        },
        get getIsSeekingGuy(): boolean {
            return self.is_seeking_guy
        },
        get getIsSubscription(): boolean {
            return self.is_subscription
        },
        get getExpiryDate() {
            return self.expiry_date
        },
        get getIsTimerSet(): boolean {
            return self.isTimerSet
        },
        get getExpiryDateCount(): number {
            return self.expiryDateCount
        },
        
        get getNotificationList(): INotification[] {
            return self.notificationList
        },
        getNotificationById(id: string): INotification {
            const filtered = self.notificationList?.filter(e => e.notification_id === id)
            return filtered?.length > 0 && filtered[0]
        }
    }))