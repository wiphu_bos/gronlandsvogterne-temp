import { types } from "mobx-state-tree"

export enum SelectionType {
    COUNTRY = "country",
    CITY = "city",
    REGION = "region",
    AGE = "age",
    HavdSogerHan = "havdSogerHan",
    Interest = "interest",
}

export const ValidationPropsModel = {
    fullNameErrorMessage: types.maybeNull(types.string),
    emailErrorMessage: types.maybeNull(types.string),
    countryErrorMessage: types.maybeNull(types.string),
    cityErrorMessage: types.maybeNull(types.string),
    ageErrorMessage: types.maybeNull(types.string),
    passwordErrorMessage: types.maybeNull(types.string),
    confirmPasswordErrorMessage: types.maybeNull(types.string),
    tagListErrorMessage: types.maybeNull(types.string),
    descriptionErrorMessage: types.maybeNull(types.string),
    interestListErrorMessage: types.maybeNull(types.string),
    havdSogerHanListErrorMessage: types.maybeNull(types.string),
    profileImageErrorMessage: types.maybeNull(types.string),
    galleryImageErrorMessage: types.maybeNull(types.string),
    isValid: types.optional(types.boolean, false),
    isCreateDudeValid: types.optional(types.boolean, false)
} 