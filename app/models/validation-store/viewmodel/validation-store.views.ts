import { types } from "mobx-state-tree"
import { ValidationPropsModel } from "../validation-store.model"
export const ValidationViews = types.model(ValidationPropsModel)
    .views(self => ({
        get getIsValid(): boolean {
            return !this.getFullNameErrorMessage &&
                !this.getEmailErrorMessage &&
                !this.getCountryErrorMessage &&
                !this.getAgeErrorMessage &&
                !this.getPasswordErrorMessage &&
                !this.getConfirmPasswordErrorMessage &&
                self.isValid
        },
        get getAllUserCredentialsValid(): boolean {
            return !this.getFullNameErrorMessage &&
                !this.getEmailErrorMessage &&
                !this.getCountryErrorMessage &&
                !this.getCityErrorMessage &&
                !this.getAgeErrorMessage &&
                !this.getTagListErrorMessage &&
                !this.getDescriptionErrorMessage &&
                !this.getHavdSogerHanListErrorMessage &&
                !this.getInterestListErrorMessage &&
                !this.getProfileImageErrorMessage &&
                !this.getGalleryImageErrorMessage &&
                self.isValid
        },
        get getFullNameErrorMessage(): string {
            return self.fullNameErrorMessage
        },
        get getEmailErrorMessage(): string {
            return self.emailErrorMessage
        },
        get getCountryErrorMessage(): string {
            return self.countryErrorMessage
        },
        get getCityErrorMessage(): string {
            return self.cityErrorMessage
        },
        get getAgeErrorMessage(): string {
            return self.ageErrorMessage
        },
        get getPasswordErrorMessage(): string {
            return self.passwordErrorMessage
        },
        get getConfirmPasswordErrorMessage(): string {
            return self.confirmPasswordErrorMessage
        },
        get getTagListErrorMessage(): string {
            return self.tagListErrorMessage
        },
        get getHavdSogerHanListErrorMessage(): string {
            return self.havdSogerHanListErrorMessage
        },
        get getInterestListErrorMessage(): string {
            return self.interestListErrorMessage
        },
        get getDescriptionErrorMessage(): string {
            return self.descriptionErrorMessage
        },
        get getProfileImageErrorMessage(): string {
            return self.profileImageErrorMessage
        },
        get getGalleryImageErrorMessage(): string {
            return self.galleryImageErrorMessage
        }
    }))