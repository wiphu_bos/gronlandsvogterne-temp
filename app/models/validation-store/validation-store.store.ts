import { types } from "mobx-state-tree"
import { ValidationViews, ValidationActions } from "./viewmodel"
import { ValidationPropsModel } from "./validation-store.model"
import { StoreName } from "../../constants/app.constant"


export const ValidationModel = types.model(StoreName.ValidationStore, ValidationPropsModel)

export const ValidationStoreModel = types.compose(
    ValidationModel,
    ValidationViews,
    ValidationActions)

