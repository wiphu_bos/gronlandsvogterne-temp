import { Instance, SnapshotOut } from "mobx-state-tree"
import { ValidationStoreModel } from "./validation-store.store"
import { ISelect } from "../user-store/user.types"
export type ValidationStore = Instance<typeof ValidationStoreModel>
export type ValidationStoreSnapshot = SnapshotOut<typeof ValidationStoreModel>

export type IValidation = {
    id?: string,
    fullName?: string,
    email?: string,
    country?: ISelect,
    age?: ISelect,
    password?: string
}