import { types, flow } from "mobx-state-tree"
import ChatServices from "../../../services/api-domain/chat.services"
import { MessagePropsModel } from "../message-store.model"
import { IMessage } from "../../../libs/gifted-chat/Models"
import { APISuccessResponse } from "../../../constants/service.types"

export const MessageServiceActions = types.model(MessagePropsModel).actions(self => {
    const createMessage = flow(function* () {
        const param = {
            chat_room_id: self.chat_room_id,
            message_id: self.message_id,
            text: self.text,
            image: self.image,
            video: self.video
        } as IMessage
        let result: APISuccessResponse = yield ChatServices.createMessage(param)
        const data: APISuccessResponse = result?.data
        const chat: IMessage = data?.data
        self.created_date = chat?.created_date
        return chat
    })
    return {
        createMessage
    }
})