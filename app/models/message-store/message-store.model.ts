import { types } from "mobx-state-tree"
import { Property } from "../../constants/firebase/fire-store.constant"
import { IUser } from "../user-store"
import QuickReplies from "../../libs/gifted-chat/QuickReplies"

export const MessagePropsModel = {
    [Property.MESSAGE_ID]: types.maybeNull(types.string),
    [Property.CHAT_ROOM_ID]: types.maybeNull(types.string),
    [Property.CREATED_DATE]: types.maybeNull(types.frozen()),
    [Property.TEXT]: types.maybeNull(types.string),
    [Property.SENDER]: types.maybeNull(types.frozen<IUser>()),
    [Property.IMAGE]: types.maybeNull(types.string),
    [Property.VIDEO]: types.maybeNull(types.string),
    [Property.AUDIO]: types.maybeNull(types.string),
    [Property.SYSTEM]: types.optional(types.boolean, false),
    [Property.IS_SENT]: types.optional(types.boolean, false),
    [Property.IS_RECEIVED]: types.optional(types.boolean, false),
    [Property.IS_PENDING]: types.optional(types.boolean, false),
    [Property.QUICK_REPLIES]: types.maybeNull(types.frozen()),
}