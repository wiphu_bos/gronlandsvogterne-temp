import { Instance, SnapshotOut } from "mobx-state-tree"
import { MessageStoreModel } from "./message-store.store"
import { Property } from "../../constants/firebase/fire-store.constant"
import { IListPage } from "../user-store"
import { IMessage } from "../../libs/gifted-chat/Models"
export type MessageStore = Instance<typeof MessageStoreModel>
export type MessageStoreSnapshot = SnapshotOut<typeof MessageStoreModel>

export type IMessageListResponse = {
    [Property.MESSAGE_LIST]?: IMessage[],
    [Property.PAGE]?: IListPage
}