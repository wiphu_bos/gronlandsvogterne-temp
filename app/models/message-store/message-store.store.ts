import { types } from "mobx-state-tree"
import { MessageViews, MessageActions } from "./viewmodel"
import { MessagePropsModel } from "./message-store.model"
import { StoreName } from "../../constants/app.constant"
import { MessageServiceActions } from "./servicemodel"
import { SharedStoreModel } from "../shared-store/shared.store"

export const MessageModel = types.model(StoreName.Message, MessagePropsModel)

export const MessageStoreModel = types.compose(
    SharedStoreModel,
    MessageModel,
    MessageViews,
    MessageActions,
    MessageServiceActions)

