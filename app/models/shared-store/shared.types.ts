import { Instance, SnapshotOut } from "mobx-state-tree"
import { SharedStoreModel } from "./shared.store"
import { MobileProperty } from "../../constants/firebase/fire-store.constant"

export type SharedStore = Instance<typeof SharedStoreModel>
export type SharedStoreSnapshot = SnapshotOut<typeof SharedStoreModel>

export type IFlatlistSelection = {
    [MobileProperty.KEY]?: string,
    [MobileProperty.LABEL]?: string
}

export type ISharedTimer = {
    [MobileProperty.SERVER_TIME]?: any,
    [MobileProperty.DATE_TIME]?: any,
    [MobileProperty.IS_TIMER_SET]?: boolean,
    [MobileProperty.TIMER]?: any,
    [MobileProperty.DATE_TIME_AGO]?: string,
}