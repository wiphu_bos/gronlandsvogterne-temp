import { types } from "mobx-state-tree"
import { IFlatlistSelection } from "./shared.types"
import { MobileProperty, Property } from "../../constants/firebase/fire-store.constant"

export const SharedPropsModel = {
    isEditing: types.optional(types.boolean, false),
    isConnected: types.maybeNull(types.boolean),
    isButtonDisabled: types.optional(types.boolean, false),
    isLoading: types.optional(types.boolean, false),
    isSubscriptionModalShowing: types.optional(types.boolean, false),


    /*
        Instance of constant value from once initialize on launchscreen to increase performance.
    */
    [Property.AGE]: types.maybeNull(types.frozen<IFlatlistSelection[]>()),
    introductions: types.maybeNull(types.frozen<IFlatlistSelection[]>()),
    countries: types.maybeNull(types.frozen<IFlatlistSelection[]>()),
    cities: types.maybeNull(types.frozen<IFlatlistSelection[]>()),
    regions: types.maybeNull(types.frozen<IFlatlistSelection[]>()),
    interests: types.maybeNull(types.frozen<IFlatlistSelection[]>()),
    havdSogerHands: types.maybeNull(types.frozen<IFlatlistSelection[]>()),
    tags: types.maybeNull(types.frozen<IFlatlistSelection[]>()),

    /*
        Time tricking handling
    */
    [MobileProperty.SERVER_TIME]: types.maybeNull(types.frozen()),
    [MobileProperty.DATE_TIME]: types.maybeNull(types.frozen()),
    [MobileProperty.IS_TIMER_SET]: types.optional(types.boolean, false),
    [MobileProperty.TIMER]: types.maybeNull(types.frozen()),
    [MobileProperty.DATE_TIME_AGO]: types.maybeNull(types.string),
}