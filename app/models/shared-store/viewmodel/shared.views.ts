import { types } from "mobx-state-tree"
import { SharedPropsModel } from "../shared.model"
import { IFlatlistSelection } from "../shared.types"

export const SharedViews = types.model(SharedPropsModel)
    .views(self => ({
        get getIsEditing() {
            return self.isEditing
        },
        get getIsConnected() {
            return self.isConnected
        },
        get getIsButtonDisabled() {
            return self.isButtonDisabled
        },
        get getIsLoading() {
            return self.isLoading
        },
        get getAge(): IFlatlistSelection[] {
            return self.age
        },
        get getCountries(): IFlatlistSelection[] {
            return self.countries
        },
        get getCities(): IFlatlistSelection[] {
            return self.cities
        },
        get getRegions(): IFlatlistSelection[] {
            return self.regions
        },
        get getHavdSogerHans(): IFlatlistSelection[] {
            return self.havdSogerHands
        },
        get getTags(): IFlatlistSelection[] {
            return self.tags
        },
        get getInterests(): IFlatlistSelection[] {
            return self.interests
        },
        get getServerTime() {
            return self.serverTime
        },
        get getTimer() {
            return self.timer
        }
    }))