import { types } from "mobx-state-tree"
import { SharedPropsModel } from "../shared.model"
import { IFlatlistSelection } from "../shared.types"
import { RootStore } from "../../root-store/root.types"
import BackgroundTimer from 'react-native-background-timer'
import moment from "moment"
import * as LocaleUtils from "../../../utils/locale"
import 'moment/locale/da'
export const SharedActions = types.model(SharedPropsModel).actions(self => {
    const setIsEditing = (value: boolean) => {
        self.isEditing = value
    }
    const setIsConnected = (value: boolean) => {
        self.isConnected = value
    }
    const setIsButtonDisabled = (value: boolean) => {
        self.isButtonDisabled = value
    }
    const setIsLoading = (value: boolean) => {
        if (value === self.isLoading) return
        self.isLoading = value
    }
    const setAge = (value: IFlatlistSelection[]) => {
        self.age = value
    }
    const setCountries = (value: IFlatlistSelection[]) => {
        self.countries = value
    }
    const setCities = (value: IFlatlistSelection[]) => {
        self.cities = value
    }
    const setRegions = (value: IFlatlistSelection[]) => {
        self.regions = value
    }
    const setHavdSogerHan = (value: IFlatlistSelection[]) => {
        self.havdSogerHands = value
    }
    const setTags = (value: IFlatlistSelection[]) => {
        self.tags = value
    }
    const setInterest = (value: IFlatlistSelection[]) => {
        self.interests = value
    }
    const setServerTime = (value: Date) => {
        self.serverTime = value
    }
    const setTimer = (value: any) => {
        self.timer = value
    }


    const setIsTimerSet = (rootStore?: RootStore, value?: boolean) => {
        self.isTimerSet = value
        if (value) {
            runInterval(rootStore)
        } else {
            invalidateTimer()
        }
    }

    const runInterval = (rootStore?: RootStore) => {
        const timeTickingProcess = async () => {
            const serverDate = moment(rootStore?.getSharedStore?.getServerTime)
            const updatedDate = moment(self.dateTime)
            self.dateTimeAgo = moment.duration(updatedDate.diff(serverDate)).locale(LocaleUtils.getLocaleDevice().toLowerCase()).humanize(true)
        }
        timeTickingProcess()
        let timer = BackgroundTimer.setInterval(() => timeTickingProcess(), 1000)
        self.timer = timer
    }

    const invalidateTimer = () => {
        BackgroundTimer.clearInterval(self.timer)
    }

    return {
        setIsEditing,
        setIsConnected,
        setIsButtonDisabled,
        setIsLoading,
        setAge,
        setCountries,
        setCities,
        setHavdSogerHan,
        setTags,
        setInterest,
        setRegions,
        setServerTime,
        setTimer,
        setIsTimerSet
    }
})