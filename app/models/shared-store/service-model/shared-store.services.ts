import { types, flow } from "mobx-state-tree"
import InformationServices from "../../../services/api-domain/information.services"
import { SharedPropsModel } from "../shared.model"
import { APIResponse, APISuccessResponse } from "../../../constants/service.types"
import { Property } from "../../../constants/firebase/fire-store.constant"

export const SharedServicesActions = types.model(SharedPropsModel).actions(self => {

    const fetchServerDateTime = flow(function* () {
        try {
            const result: APIResponse = yield InformationServices.fetchServerDateTime()
            const success = (result as APISuccessResponse)?.data
            let data = success?.data
            if (!data) throw result
            const serverDateTime = data[Property.SERVER_TIME]
            self.serverTime = serverDateTime
            return data
        } catch (e) {
            return e
        }
    })
    return {
        fetchServerDateTime
    }
})