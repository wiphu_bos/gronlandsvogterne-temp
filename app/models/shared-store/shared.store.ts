import { types } from "mobx-state-tree"
import { SharedViews, SharedActions } from "./viewmodel"
import { SharedPropsModel } from "./shared.model"
import { StoreName } from "../../constants/app.constant"
import { SharedServicesActions } from "./service-model/shared-store.services"

const SharedModel = types.model(StoreName.Shared, SharedPropsModel)

export const SharedStoreModel = types.compose(
    SharedModel,
    SharedViews,
    SharedActions,
    SharedServicesActions)

