import { ViewStyle, TextStyle, ImageStyle } from "react-native"
import { IconTypes } from "../icon/icons"
import { ReactChild } from "react"

export interface HeaderProps {
  headerTx?: string
  headerText?: string
  containerStyle?: ViewStyle
  titleStyle?: TextStyle
  isRightIconAnimated?: boolean,
  rightText?: string,
  rightIconStyle?: ImageStyle,
  rightIconBadgeNumber?: number,
  onRightPress?(): void,
  rightIcon?: IconTypes
  rightIconSource?: string,
  rightTextStyle?: TextStyle,
  leftText?: string,
  leftIconStyle?: ImageStyle,
  isLeftIconAnimated?: boolean,
  leftIconBadgeNumber?: number,
  onLeftPress?(): void,
  leftIcon?: IconTypes
  leftIconSource?: string
  customLeftView?: ReactChild
  customRightView?: ReactChild
}
