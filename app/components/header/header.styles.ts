import { ViewStyle, TextStyle } from "react-native"
import * as metric from "../../theme"

export const CONTAINER_VIEW: ViewStyle = {
  flexDirection: "row",
  alignItems: "center",
  justifyContent: "flex-start",
}

export const HEADER: ViewStyle = {
  marginTop: metric.backButtonHeaderMarginTop,
  height:metric.ratioHeight(50)
}

export const HEADER_MARGIN_FOR_OFFLINE: ViewStyle = {
  marginTop: metric.actualHeaderMargin,
  height:metric.ratioHeight(50)
}

export const TITLE: TextStyle = { textAlign: "center" }
export const TITLE_MIDDLE: ViewStyle = { flex: 1, justifyContent: "center" }
export const LEFT: ViewStyle = { width: metric.ratioWidth(24) }
export const RIGHT: ViewStyle = { width: metric.ratioWidth(24) }
