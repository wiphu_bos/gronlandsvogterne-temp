import * as React from "react"
import { View, StyleSheet, ViewStyle } from "react-native"
import { HeaderProps } from "./header.props"
import { Button } from "../button/button"
import { Icon } from "../icon/icon"
import { Text } from "../text/text"
import { translate } from "../../i18n/"
import * as Styles from "./header.styles"
import { useStores } from "../../models/root-store"
import { observer } from "mobx-react-lite"
import * as Utils from "../../utils"
export const Header: React.FunctionComponent<HeaderProps> = observer(props => {
  const rootStore = useStores()
  const {
    headerText,
    headerTx,
    containerStyle,
    titleStyle
  } = props
  const header = headerText || (headerTx && translate(headerTx)) || ""
  const workAroundStyle = rootStore?.getSharedStore?.getIsConnected ? Styles.HEADER : Styles.HEADER_MARGIN_FOR_OFFLINE

  const { leftIcon,
    leftIconSource,
    leftIconStyle,
    onLeftPress,
    isLeftIconAnimated } = props

  const { rightIcon,
    rightIconSource,
    rightText,
    isRightIconAnimated,
    onRightPress,
    rightIconBadgeNumber,
    rightIconStyle,
    rightTextStyle } = props

  const { customLeftView,
    customRightView } = props


  const calculatedContainer: ViewStyle = {
    ...containerStyle,
    ...workAroundStyle,
    marginTop: parseFloat((containerStyle?.marginTop || 0).toString()) + parseFloat((workAroundStyle?.marginTop || 0).toString())
  }

  const renderLeftMenuView = () => {
    if (customLeftView) return customLeftView
    return leftIcon || leftIconSource ? (
      <Button isAnimated={isLeftIconAnimated} preset="topLeftMenu" onPress={onLeftPress}>
        <Icon icon={leftIcon} source={leftIconSource} style={leftIconStyle} />
      </Button>
    ) : null
  }

  const renderRightMenuView = () => {
    if (customRightView) return customRightView
    return rightIcon || rightIconSource || rightText ? (
      <Button isAnimated={isRightIconAnimated} preset="topRightMenu" onPress={onRightPress}>
        {rightIcon || rightIconSource ? <Icon icon={rightIcon} source={rightIconSource} style={rightIconStyle} /> : null}
        {rightIconBadgeNumber ? <Button preset="topMenuBadge" text={Utils.getBadgeNumber(rightIconBadgeNumber)} /> : null}
        {rightText ? <Text text={rightText} style={rightTextStyle} /> : null}
      </Button>
    ) : null
  }

  return (
    <View shouldRasterizeIOS style={{ ...Styles.CONTAINER_VIEW, ...calculatedContainer }}>
      {
        renderLeftMenuView ? renderLeftMenuView() : (
          <View shouldRasterizeIOS style={Styles.LEFT} />
        )}
      <View shouldRasterizeIOS style={Styles.TITLE_MIDDLE}>
        <Text style={{ ...Styles.TITLE, ...titleStyle }} text={header} />
      </View>
      {
        renderRightMenuView ? renderRightMenuView() : (
          <View shouldRasterizeIOS style={Styles.RIGHT} />
        )}
    </View>
  )
})
