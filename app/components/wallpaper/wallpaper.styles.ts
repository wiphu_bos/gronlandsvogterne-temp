import { ViewStyle } from "react-native";
import { color } from "../../theme";

export const FULL: ViewStyle = {
    position:'absolute',
    left:0,
    top:0,
    right:0,
    bottom:0,
}

export const BACKGROUND_STYLE = color.pinkBlueGradient