export interface WallpaperProps {
  showBubble?: boolean
  children?: React.ReactNode
}
