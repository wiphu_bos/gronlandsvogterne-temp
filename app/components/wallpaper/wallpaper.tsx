import React from "react"
import { WallpaperProps } from "./wallpaper.props"
import LinearGradient from "react-native-linear-gradient"
import * as Styles from "./wallpaper.styles"
import { BubbleMark } from "../bubble-mark/bubble-mark"

export function Wallpaper(props: WallpaperProps) {
  return (
    <LinearGradient shouldRasterizeIOS style={Styles.FULL} colors={Styles.BACKGROUND_STYLE} >
      {props.showBubble ? <BubbleMark /> : null}
      {(props as any).children}
    </LinearGradient>
  )
}
