import { ViewStyle, TextStyle } from "react-native"
import * as metric from "../../theme"
import { color } from "../../theme"
import { RFValue } from "react-native-responsive-fontsize"

export const deviceStyle: ViewStyle = {
    height: metric.ratioHeight(68),
    paddingTop: metric.ratioHeight(35)
}
export const defaultViewStyle: ViewStyle = {
    backgroundColor: color.palette.darkRed,
    justifyContent: 'center',
    position: 'absolute',
    left: 0,
    top: 0,
    right: 0,
    zIndex: 1,
}

export const baseTextStyle: TextStyle = {
    fontFamily: 'Montserrat',
    alignSelf: 'center',
}
export const defaultTextStyle: TextStyle = {
    ...baseTextStyle,
    fontWeight: '400',
    fontSize: RFValue(16),

    //Android
    fontFamily: 'Montserrat-Regular'
}