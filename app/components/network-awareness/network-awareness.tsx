import * as React from "react"
import { TouchableOpacity } from "react-native"
import { Text } from "../"
import { NetworkAwarenessProps } from "./network-awareness.props"
import { useStores } from "../../models/root-store"
import { GeneralResources } from "../../constants/firebase/remote-config"
import * as Styles from "./netword-awareness.styles"
import { observer } from "mobx-react-lite"
import * as metric from "../../theme"


export const NetworkAwarenessComponent = observer((props: NetworkAwarenessProps) => {

  const rootStore = useStores()
  const { SharedKeys } = GeneralResources
  const { getIsConnected } = rootStore.getSharedStore
  // grab the props
  const {
    tx,
    text,
    style: styleOverride,
    textStyle: textStyleOverride,
    children,
    ...rest
  } = props

  const viewStyle = styleOverride
  const textStyle = textStyleOverride
  const content = children || <Text tx={tx} text={text || rootStore.getGeneralResourcesStore(SharedKeys.noInternetConnectionTitle, true)} style={[textStyle, Styles.defaultTextStyle]} />

  return (
    getIsConnected !== null && !getIsConnected ?
      <TouchableOpacity style={[viewStyle, Styles.defaultViewStyle, Styles.deviceStyle, !metric.isIPhone && { position: 'relative' }]} {...rest}>
        {content}
      </TouchableOpacity>
      : null
  )
})
