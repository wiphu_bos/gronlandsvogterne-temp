import { ViewStyle, TextStyle, ImageStyle } from "react-native"
import * as metric from "../../theme"
import { color } from "../../theme"
import { RFValue } from "react-native-responsive-fontsize"
export const FULL: ViewStyle = {
    flex: 1
}
export const USER_TAG_CONTAINER: ViewStyle = {
    marginTop: metric.ratioHeight(30),
    flexDirection: 'column',
}

export const USER_TAG_HEADER: ViewStyle = {
    ...FULL,
    flexDirection: 'row'
}
export const USER_TAG_BODY: ViewStyle = {
    marginTop: metric.ratioHeight(20),
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
}

export const USER_TAG_ITEM_CONTAINER: ViewStyle = {
    paddingHorizontal: metric.ratioWidth(20),
    paddingVertical: metric.ratioHeight(10),
    borderRadius: metric.ratioWidth(92),
    borderWidth: 1,
    marginHorizontal: metric.ratioWidth(6),
    marginVertical: metric.ratioWidth(9),
}

export const USER_TAG_ITEM_CONTAINER_DESELECTED: ViewStyle = {
    ...USER_TAG_ITEM_CONTAINER,
    borderColor: color.palette.white,
}

export const USER_TAG_ITEM_CONTAINER_SELECTED: ViewStyle = {
    ...USER_TAG_ITEM_CONTAINER,
    borderColor: color.palette.pink,
    backgroundColor: color.palette.pink,
}
export const TEXT: TextStyle = {
    color: color.palette.white,
    fontFamily: "Montserrat",
}
export const ICON_USER_TAG: ImageStyle = {
    width: metric.ratioWidth(35),
    height: metric.ratioHeight(28),
}

export const TITLE: TextStyle = {
    ...TEXT,
    fontSize: RFValue(12),
    fontWeight: '400',

    //Android
    fontFamily: "Montserrat-Regular",
}

export const USER_TAG_TITLE_TEXT: TextStyle = {
    ...TITLE,
    fontSize: RFValue(16),
    paddingHorizontal: metric.ratioWidth(16),
}

export const USER_TAG_ITEM_TEXT: TextStyle = {
    ...TITLE,
    fontSize: RFValue(14),
}