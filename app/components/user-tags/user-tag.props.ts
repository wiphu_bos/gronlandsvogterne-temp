import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { UserStore, ISelect } from '../../models/user-store'
import { ReactChild } from 'react'
import { ViewStyle } from 'react-native'

export interface UserTagsProps extends NavigationContainerProps<ParamListBase> {
    selectAllTitle?: string,
    observeViewModel?: UserStore
    isDisabled?: boolean,
    onTagPressed?: (item: ISelect) => void
    tagList: ISelect[],
    tagTitle?: string,
    customHeaderView?: ReactChild,
    headerStyle?: ViewStyle,
    containerStyle?: ViewStyle
    contentStyle?: ViewStyle,
    onSelectAllPressed?: () => void
    titleTestID?: any,
    buttonItemTestID?: any,
    isSelectedAll?: boolean
}