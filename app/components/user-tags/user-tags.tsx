
import React from "react"
import { observer } from "mobx-react-lite"
import { Button } from "../../components/button/button"
import * as Styles from "./user-tags.styles"
import { Text } from "../../components/text"
import { Icon } from "../../components/icon"
import { View } from "react-native"
import { images } from "../../theme/images"
import { UserTagsProps } from "./user-tag.props"
import { ISelect } from "../../models/user-store"

export const UserTagsComponent: React.FunctionComponent<UserTagsProps> = observer((props) => {

    const TagButtonView = (i: number, e: ISelect, isSelected: boolean) => {
        const isSelectAllPress = e.value === props?.selectAllTitle//resourcesViewModel?.getResourceButtonSelectAllTitle()
        const style = selectedTagList && props?.isSelectedAll ?
            Styles.USER_TAG_ITEM_CONTAINER_SELECTED :
            (
                isSelected ? Styles.USER_TAG_ITEM_CONTAINER_SELECTED :
                    Styles.USER_TAG_ITEM_CONTAINER_DESELECTED
            )
        return (<Button preset="link"
            disabled={props?.isDisabled || false}
            isAnimated={false}
            style={style}
            onPress={isSelectAllPress && props?.onSelectAllPressed || (() => props?.onTagPressed(e))}>
            <Text text={e.value as string} style={Styles.USER_TAG_ITEM_TEXT} />
        </Button>)
    }

    const selectedTagList = props?.observeViewModel?.getTagList
    const selectAllItem: ISelect[] = [{ key: "select_all_key", value: props?.selectAllTitle }]
    const tagList = props?.tagList?.length > 0 && (props?.onSelectAllPressed && selectAllItem.concat(props?.tagList) || props?.tagList)
    const tagItems = () => tagList && tagList?.map((e, i) => {
        const isSelected = selectedTagList?.length > 0 && selectedTagList?.findIndex(v => e.key === v.key) !== -1 || false
        return TagButtonView(i, e, isSelected)
    })
    return (
        <View shouldRasterizeIOS style={{ ...Styles.USER_TAG_CONTAINER, ...props?.containerStyle }}>
            {props?.customHeaderView && props?.customHeaderView ||
                <View shouldRasterizeIOS style={{ ...Styles.USER_TAG_HEADER, ...props?.headerStyle }}>
                    <Icon source={images.userTag} style={Styles.ICON_USER_TAG} />
                    <Text style={Styles.USER_TAG_TITLE_TEXT} text={props?.tagTitle} />
                </View>
            }
            <View shouldRasterizeIOS style={{ ...Styles.USER_TAG_BODY, ...props?.contentStyle }}>
                {tagItems()}
            </View>
        </View >)
})