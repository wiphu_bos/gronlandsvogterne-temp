import { ViewStyle, TextStyle } from "react-native"
import * as metric from "../../theme"
import { color } from "../../theme"
import { RFValue } from "react-native-responsive-fontsize"
export const FULL: ViewStyle = {
    flex: 1
}
export const STATUS: ViewStyle = {
    ...FULL,
    flexDirection: 'row',
    alignItems: 'center',
}
export const STATUS_VIEW: ViewStyle = {
    marginLeft: metric.ratioWidth(10),
    paddingHorizontal: metric.ratioHeight(6),
    paddingVertical: metric.ratioHeight(3),
    borderRadius: metric.ratioWidth(7),
    backgroundColor: color.palette.lightBlue
}

export const TEXT: TextStyle = {
    color: color.palette.white,
    fontFamily: "Montserrat",
}

export const TITLE: TextStyle = {
    ...TEXT,
    fontSize: RFValue(12),
    fontWeight: '400',

    //Android
    fontFamily: "Montserrat-Regular",
}