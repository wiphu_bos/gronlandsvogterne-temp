import React from "react"
import { observer } from "mobx-react-lite"
import * as Styles from "./user-status.styles"
import { Text } from "../text"
import { View } from "react-native"
import { UserStatusProps } from "./user-status.props"

export const StatusComponent: React.FunctionComponent<UserStatusProps> = observer((props) => {
    return (
        <View shouldRasterizeIOS style={Styles.STATUS}
            {...props?.testID}
        >
            <Text text={props?.statusText} style={Styles.TITLE} />
            {props?.statusValue && <View shouldRasterizeIOS style={Styles.STATUS_VIEW}>
                <Text text={props?.statusValue} style={Styles.TITLE} />
            </View>}
        </View>)
})