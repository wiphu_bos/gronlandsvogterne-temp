import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"

export interface UserStatusProps extends NavigationContainerProps<ParamListBase> {
    statusText?: string,
    statusValue?: string,
    testID?: any,
}