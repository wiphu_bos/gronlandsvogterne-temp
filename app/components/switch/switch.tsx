import * as React from "react"
import { ViewStyle, Animated, Easing, TouchableWithoutFeedback } from "react-native"
import { color } from "../../theme"
import { SwitchProps } from "./switch.props"
import { mergeAll, flatten } from "ramda"
import * as metric from "../../theme"

// colors
const ON_COLOR = color.primary
const OFF_COLOR = color.palette.offWhite
const BORDER_ON_COLOR = ON_COLOR
const BORDER_OFF_COLOR = "rgba(0, 0, 0, 0.1)"

// animation
const DURATION = 250

const enhance = (style, newStyles): any => {
  return mergeAll(flatten([style, newStyles]))
}

const makeAnimatedValue = switchOn => new Animated.Value(switchOn ? 1 : 0)

export const Switch: React.FunctionComponent<SwitchProps> = props => {
  const trackWidth: number = parseFloat(((props?.baseTrackStyle as ViewStyle).width || 0).toString())
  const thumbWidth: number = parseFloat(((props?.baseThumbStyle as ViewStyle).width || 0).toString())
  const OFF_POSITION = metric.ratioWidth(-1)
  const ON_POSITION = trackWidth - thumbWidth - metric.ratioWidth(1)

  const [timer] = React.useState<Animated.Value>(makeAnimatedValue(props.value))
  const startAnimation = React.useMemo(
    () => (newValue: boolean) => {
      const toValue = newValue ? 1 : 0
      const easing = Easing.out(Easing.circle)
      Animated.timing(timer, {
        toValue,
        duration: DURATION,
        easing,
        useNativeDriver: true,
      }).start()
    },
    [timer],
  )

  const [previousValue, setPreviousValue] = React.useState<boolean>(props.value)
  React.useEffect(() => {
    if (props.value !== previousValue) {
      startAnimation(props.value)
      setPreviousValue(props.value)
    }
  }, [props.value])

  const handlePress = React.useMemo(() => () => props.onToggle && props.onToggle(!props.value), [
    props.onToggle,
    props.value,
  ])

  if (!timer) {
    return null
  }

  const translateX = timer.interpolate({
    inputRange: [0, 1],
    outputRange: [OFF_POSITION, ON_POSITION],
  })

  const style = enhance({}, props.style)

  let trackStyle = props?.baseTrackStyle
  trackStyle = enhance(trackStyle, {
    backgroundColor: props.value ? ON_COLOR : OFF_COLOR,
    borderColor: props.value ? BORDER_ON_COLOR : BORDER_OFF_COLOR,
  })
  trackStyle = enhance(trackStyle, props.value ? props.trackOnStyle : props.trackOffStyle)

  let thumbStyle = props?.baseThumbStyle
  thumbStyle = enhance(thumbStyle, {
    transform: [{ translateX }],
  })
  thumbStyle = enhance(thumbStyle, props.value ? props.thumbOnStyle : props.thumbOffStyle)

  return (
    <TouchableWithoutFeedback onPress={handlePress} style={style}>
      <Animated.View style={trackStyle}>
        <Animated.View style={thumbStyle} />
      </Animated.View>
    </TouchableWithoutFeedback>
  )
}
