import { Image } from "react-native-image-crop-picker";
import { ViewStyle, TextStyle } from "react-native";
import { IUploadFileData } from "../../utils/firebase/fire-storage/fire-storage.types";

export interface ProfileTitleProps {
  containerStyle?: ViewStyle,
  displayName?: string
  displayCity?: string
  profileImage?: Image & IUploadFileData
  isShowProfileImage?: boolean,
  buttonText?: string,
  isShowButton?: boolean,
  onButtonClick?(): void,
  displayNameTestID?: any,
  displayCityTestID?: any,
  profileImageTestID?: any,
  buttonTestID?: any,
  isDude?: boolean,
  buttonTextStyle?: TextStyle
}
