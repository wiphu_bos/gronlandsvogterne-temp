import { ViewStyle, TextStyle, ImageStyle } from "react-native"
import * as metric from "../../theme"
import { RFValue } from "react-native-responsive-fontsize"
import { color } from "../../theme"


export const FULL: ViewStyle = {
  flex: 1
}

export const SAFE_AREA_VIEW: ViewStyle = {
  ...FULL
}

export const MENU_ICON_STYLE: ImageStyle = {
  width: metric.ratioWidth(25),
  height: metric.ratioHeight(25),
  resizeMode: null
}


const TEXT: TextStyle = {
  fontFamily: "Montserrat",
}

export const TITLE: TextStyle = {
  ...TEXT,
  lineHeight: metric.ratioHeight(36),
  fontSize: RFValue(22),
  fontWeight: '400',

  //Android
  fontFamily: "Montserrat-Regular",
  color: color.palette.white
}

export const DESCRIPTION: TextStyle = {
  ...TITLE,
  fontSize: RFValue(12),
  marginVertical: metric.ratioWidth(15),
  color: color.palette.white,
  marginHorizontal: metric.ratioWidth(10.5)
}

export const PIN: ImageStyle = {
  width: metric.ratioWidth(10.5),
  height: metric.ratioHeight(14)
}

export const SUB_PROFILE_CONTAINER: ViewStyle = {
  flexDirection: 'row',
  alignItems: 'center',
  marginTop: metric.ratioHeight(2)
}

export const CONTAINER: ViewStyle = {
  minHeight: metric.ratioHeight(124)
}

export const TOP_CONTAINER: ViewStyle = {
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'space-between',
  minHeight: metric.ratioHeight(85)
}

export const PROFILE_CONTAINER: ViewStyle = {
  ...FULL,
  marginRight: metric.ratioWidth(20)
}

export const IMAGE_PROFILE_CONTAINNER: ViewStyle = {
  alignItems: 'flex-end',
  width: metric.ratioWidth(85)
}

export const IMAGE_PROFILE: ImageStyle = {
  width: metric.ratioWidth(85),
  height: metric.ratioWidth(85),
  borderRadius: metric.ratioWidth(85) / 2
}

export const BUTTON: ViewStyle = {
  width: metric.ratioWidth(200),
  height: metric.ratioHeight(46),
  marginBottom: metric.ratioWidth(24),
  borderRadius: metric.ratioWidth(12)
}