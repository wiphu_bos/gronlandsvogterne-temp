import * as React from "react"
import { View } from "react-native"
import { ProfileTitleProps } from "./profile-title.props"
import { Icon } from "../icon/icon"
import { Text } from "../text/text"
import * as Styles from "./profile-title.styles"
import { observer } from "mobx-react-lite"
import { images } from "../../theme/images"
import FastImage from "react-native-fast-image"
import { GradientButton } from "../gradient-button/button"

export const ProfileTitleComponents: React.FunctionComponent<ProfileTitleProps> = observer(props => {

  const {
    containerStyle,
    displayName,
    displayCity,
    profileImage,
    isShowProfileImage,
    buttonText,
    buttonTextStyle,
    isShowButton,
    onButtonClick,
    displayNameTestID,
    displayCityTestID,
    profileImageTestID,
    buttonTestID,
    isDude } = props


  return (
    <View style={{ ...Styles.CONTAINER, ...containerStyle }}>
      <View style={Styles.TOP_CONTAINER}>
        <View style={Styles.PROFILE_CONTAINER}>
          {displayName && <Text {...displayNameTestID} text={displayName} style={Styles.TITLE} numberOfLines={2} />}
          {displayCity && <View style={Styles.SUB_PROFILE_CONTAINER}>
            <Icon source={images.whitePin} style={Styles.PIN} />
            <Text {...displayCityTestID} text={displayCity} style={Styles.DESCRIPTION} numberOfLines={1} />
          </View>}
        </View>
        <View shouldRasterizeIOS style={Styles.IMAGE_PROFILE_CONTAINNER}>
          {isShowProfileImage && <FastImage {...profileImageTestID} source={{uri : profileImage?.url}} style={Styles.IMAGE_PROFILE} />}
        </View>
      </View>
      {isShowButton && <GradientButton
        {...buttonTestID}
        preset='whiteShadow'
        text={buttonText}
        textStyle={buttonTextStyle}
        style={Styles.BUTTON}
        onPress={onButtonClick} />}
    </View>
  )
})
