import { color } from "../../theme"
import { ViewStyle } from "react-native"
import * as metric from "../../theme"

export const CIRCLE: ViewStyle = {
    backgroundColor: color.superWhiteDim,
    position: "absolute",
}

export const BIG_CIRCLE: ViewStyle = {
    ...CIRCLE,
    width: metric.ratioWidth(300),
    height: metric.ratioWidth(300),
    borderRadius: metric.ratioWidth(290) / 2,
    top: -metric.ratioHeight(70),
    right: -metric.ratioWidth(45)
}

export const MIDDLE_CIRCLE: ViewStyle = {
    ...CIRCLE,
    width: metric.ratioWidth(240),
    height: metric.ratioWidth(240),
    borderRadius: metric.ratioWidth(239) / 2,
    top: (metric.screenHeight / 2) - metric.ratioHeight(165),
    left: -metric.ratioWidth(135),
}

export const SMALL_CIRCLE: ViewStyle = {
    ...CIRCLE,
    width: metric.ratioWidth(170),
    height: metric.ratioWidth(170),
    borderRadius: metric.ratioWidth(170) / 2,
    top: (metric.screenHeight / 2) + metric.ratioHeight(30),
    bottom: 0,
    right: -metric.ratioWidth(75),
}