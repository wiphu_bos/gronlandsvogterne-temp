import React from "react"
import { View } from "react-native"
import * as Styles from "./bubble-mark.styles"

export const BubbleMark: React.FunctionComponent = () => {
    return (
        <View>
            <View shouldRasterizeIOS style={Styles.BIG_CIRCLE} />
            <View shouldRasterizeIOS style={Styles.MIDDLE_CIRCLE} />
            <View shouldRasterizeIOS style={Styles.SMALL_CIRCLE} />
        </View>
    )

}