// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { Text } from "../text"
import { GeneralResources } from "../../constants/firebase/remote-config"
import { TextStyle } from "react-native"
import * as DataUtils from "../../utils/data.utils"

// MARK: Style Import

import * as Styles from "./my-status.styles"
import { MyStatusProps } from "./my-status.props"

export const MyStatusComponents: React.FunctionComponent<MyStatusProps> = observer((props) => {
    const { rootStore, status } = props
    let textStyle: TextStyle
    const { approvedStatusTitle,
        waitingForApprovalStatusTitle,
        waitingForPublishStatusTitle,
        publishStatusTitle,
        unPublishStatusTitle,
        requestForEditingStatusTitle,
        deletedStatusTitle } = GeneralResources.UserStatus
    if (approvedStatusTitle.includes(status)) {
        textStyle = Styles.BLUE_TEXT
    } else if (publishStatusTitle.includes(status)) {
        textStyle = Styles.GREEN_TEXT
    } else if (unPublishStatusTitle.includes(status) ||
        deletedStatusTitle.includes(status) ||
        waitingForApprovalStatusTitle.includes(status) ||
        waitingForPublishStatusTitle.includes(status) ||
        requestForEditingStatusTitle.includes(status)) {
        textStyle = Styles.SUPERLIGHT_TEXT
    }

    return (<Text
            {...props?.testID}
            text={DataUtils.getUserStatusByLocaleKey(rootStore?.GeneralResourcesStore, status)}
            style={textStyle} />
    )
})