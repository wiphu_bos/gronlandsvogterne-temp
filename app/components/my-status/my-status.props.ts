import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { RootStore } from '../../models'

export interface MyStatusProps extends NavigationContainerProps<ParamListBase> {
    testID?: any,
    status?: string,
    rootStore: RootStore
}