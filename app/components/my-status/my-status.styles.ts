import { TextStyle } from "react-native"
import * as metric from "../../theme"
import { RFValue } from "react-native-responsive-fontsize"
import { color } from "../../theme/color"

const TEXT: TextStyle = {
    color: color.palette.darkGrey,
    fontFamily: "Montserrat",
}
export const TITLE: TextStyle = {
    ...TEXT,
    lineHeight: metric.ratioHeight(36),
    fontSize: RFValue(12),
    fontWeight: '400',

    //Android
    fontFamily: "Montserrat-Regular",
}

export const BLUE_TEXT: TextStyle = {
    ...TITLE,
    color: color.palette.lightBlue
}

export const SUPERLIGHT_TEXT: TextStyle = {
    ...TITLE,
    color: color.palette.superLightGrey
}

export const GREEN_TEXT: TextStyle = {
    ...TITLE,
    color: color.palette.green
}