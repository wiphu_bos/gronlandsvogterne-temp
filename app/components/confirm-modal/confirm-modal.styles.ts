import { ViewStyle, TextStyle } from "react-native"
import * as metric from "../../theme"
import { color } from '../../theme'
import { RFValue } from "react-native-responsive-fontsize"
import { ButtonProps } from "../button/button.props"

export const CONTENT_CONTAINER: ViewStyle = {
    minHeight: metric.ratioHeight(105),

}

export const CONTAINER: ViewStyle = {
    backgroundColor: color.palette.white,
    marginHorizontal: metric.ratioWidth(25),
}

export const FOOTER_CONTAINER: ViewStyle = {
    paddingHorizontal: metric.ratioWidth(9),
    height: metric.ratioHeight(55),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
}

const OK_BUTTON_STYLE: ViewStyle = {
    minWidth: metric.ratioWidth(72),
    height: metric.ratioHeight(36),
    backgroundColor: color.palette.pink,
    borderRadius: metric.ratioWidth(75),
    marginRight: metric.ratioWidth(15),
    justifyContent: 'center',
}

export const CLEAR_BUTTON_STYLE: ViewStyle = {
    height: metric.ratioHeight(55),
    justifyContent: 'center'
}

export const regularTextStyle: TextStyle = {
    fontWeight: '400',

    //Android
    fontFamily: 'Montserrat-Regular'
}

const OK_BUTTON_TEXT_STYLE: TextStyle = {
    ...regularTextStyle,
    textAlign: 'center',
    fontSize: RFValue(14),
    color: color.palette.white,
}

export const CLEAR_TEXT_STYLE: TextStyle = {
    ...regularTextStyle,
    fontSize: RFValue(14),
    color: color.palette.pink,
    justifyContent: 'center',
    alignItems: 'center'
}

export const MESSAGE_TEXT_STYLE: TextStyle = {
    ...regularTextStyle,
    color: color.palette.darkGrey,
    fontSize: RFValue(16),
    lineHeight: metric.ratioHeight(31),
    marginHorizontal: metric.ratioWidth(20),
    marginVertical: metric.ratioHeight(27)

}

const BUTTON_PROPS: ButtonProps = {
    isAnimated: false,
    preset: 'none'
}

export const OK_BUTTON_PROPS: ButtonProps = {
    ...BUTTON_PROPS,
    containerStyle: OK_BUTTON_STYLE,
    textStyle: OK_BUTTON_TEXT_STYLE
}

export const CLEAR_BUTTON_PROPS: ButtonProps = {
    ...BUTTON_PROPS,
    containerStyle: CLEAR_BUTTON_STYLE,
    textStyle: CLEAR_TEXT_STYLE
}
