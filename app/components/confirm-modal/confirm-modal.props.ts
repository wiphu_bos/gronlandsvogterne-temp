import { Animation, CustomAnimation } from "react-native-animatable";

export interface ConfirmModalProps {
  onOK?: () => void
  onCancel?: () => void
  onClose?: () => void
  okButtonText?: string
  clearButtonText?: string
  message?: string
  isVisible?: boolean
  onModalClosed?: () => void
  animationOutTiming?: number
  animationIn?: Animation | CustomAnimation
  animationOut?: Animation | CustomAnimation
}
