import { ViewStyle, TextStyle } from "react-native"

export enum PopoverMenuActionType {
  Subsciption = "subscription",
  CancelSubsciption = "cancel_subscription",
  ActivateProfile = "published",//"activate_profile",
  DeactivateProfile = "unpublished",//"deactivate_profile",
  Delete = "delete_profile",
  Unknown = "unknown"
}

export type PopoverMenuItem = {
  type: PopoverMenuActionType,
  title: string,
  titleStyle?: TextStyle,
  containerStyle?: ViewStyle
}

export interface PopoverMenuProps {
  items: PopoverMenuItem[],
  containerStyle?: ViewStyle,
  onPress?: (type?: PopoverMenuActionType) => void,
}
