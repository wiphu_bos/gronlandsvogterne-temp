import * as React from "react"
import { PopoverMenuProps } from "./popover-menu.props"
import { observer } from "mobx-react-lite"
import { Button } from "../button/button"
import { ButtonProps } from "../button/button.props"
import * as Animatable from 'react-native-animatable'

import * as Styles from "./popove-menu.styles"

export const MenuPopover: React.FunctionComponent<PopoverMenuProps> = observer(props => {
  const {
    items,
    containerStyle,
    onPress
  } = props
  const renderItem = () => {
    return items?.map((e,i) => {
      const menuOptions: ButtonProps = {
        text: e.title,
        textStyle: { ...Styles.TITLE, ...e.titleStyle },
        style: {...Styles.ITEM_CONTAINER,...e.containerStyle},
        onPress: () => onPress(e.type)  //will fix later
      }
      return <Button preset="none" isAnimated={false} key={i} {...menuOptions} />
    })
  }
  return items && items?.length ? <Animatable.View useNativeDriver style={{ ...containerStyle, ...Styles.CONTAINER }}>{renderItem()}</Animatable.View> : null
})