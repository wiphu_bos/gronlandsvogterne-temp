import { ViewStyle, TextStyle } from "react-native"
import * as metric from "../../theme"
import { color } from "../../theme"
import { RFValue } from "react-native-responsive-fontsize"

export const CONTAINER: ViewStyle = {
  shadowColor: color.palette.black,
  shadowOffset: { width: 0, height: 5 },
  shadowOpacity: 0.13,
  elevation: 4,
  backgroundColor: color.palette.white
}

export const ITEM_CONTAINER: ViewStyle = {
  justifyContent: 'center',
  height: metric.ratioHeight(47),
  paddingHorizontal: metric.ratioWidth(5)
}

const TEXT: TextStyle = {
  color: color.palette.black,
  fontFamily: "Montserrat",
}

export const TITLE: TextStyle = {
  ...TEXT,
  lineHeight: metric.ratioHeight(24),
  fontSize: RFValue(14),
  fontWeight: '400',

  //Android
  fontFamily: "Montserrat-Regular",
  color: color.palette.black
}
