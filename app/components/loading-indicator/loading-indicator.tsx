import * as React from "react"
import FastImage from 'react-native-fast-image'
import Modal from '../../libs/react-native-modal'
import { LoadingIndicatorProps } from "./loading-indicator.props"
import * as Styles from "./loading-indicator.styles"
import * as Images from "../../theme/images"
import { useStores } from "../../models/root-store"
import { observer } from "mobx-react-lite"

export const LoadingIndicator = observer((props: LoadingIndicatorProps) => {
  const { containerStyle } = props
  const rootStore = useStores()
  const { isLoading } = rootStore.getSharedStore

  return (
    <Modal presentationStyle="overFullScreen" isVisible={isLoading} animationIn='fadeIn' animationOut='fadeOut' style={containerStyle}>
      <FastImage source={Images.images.splashLogo} style={Styles.INDICATOR_VIEW} resizeMode={FastImage.resizeMode.contain} />
    </Modal>
  )
})
