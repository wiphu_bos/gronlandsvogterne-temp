import { ViewStyle } from "react-native"

export interface LoadingIndicatorProps {

  containerStyle?: ViewStyle

}
