import { Source } from "react-native-fast-image";

export interface CardTRCProps {
    contentFirstLine: string,
    contentSecondLine: string,
    iconFirstLine?: Source,
    iconSecondLine?: Source
}