
import * as metric from "../../theme"
import { ViewStyle, TextStyle, ImageStyle } from "react-native"
import { color } from "../../theme"
import { RFValue } from "react-native-responsive-fontsize"
const FULL: ViewStyle = {
    flex: 1
}

export const ROOT: ViewStyle = {
    ...FULL,
    marginTop: metric.ratioHeight(33),
    minHeight: metric.ratioHeight(115)
}

export const RECTANGLE_TOP: ViewStyle = {
    ...FULL,
    flexDirection: 'row',
    justifyContent: 'space-between'
}

export const RECTANGLE_LEFT: ViewStyle = {
    width: metric.ratioWidth(6),
    backgroundColor: "#EB95B3",
    height: '100%',
}

export const RECTANGLE_CONTENT: ViewStyle = {
    width: metric.ratioWidth(290),
    minHeight: metric.ratioHeight(75),
    backgroundColor: color.redTRC,
    paddingLeft: metric.ratioWidth(35),
    paddingRight: metric.ratioWidth(35),
}

export const RECTANGLE_FOOTER: ViewStyle = {
    width: metric.ratioWidth(290),
    minHeight: metric.ratioHeight(20),
    backgroundColor: color.redTRC,
}

export const TRIANGLE_CORNER: ViewStyle = {
    width: metric.ratioWidth(290),
    height: 0,
    borderBottomWidth: metric.ratioWidth(20),
    borderBottomColor: color.redTRC,
    borderRightWidth: metric.ratioWidth(20),
    borderRightColor: 'transparent',
    borderStyle: 'solid'
}

export const CONTENT_CONTAINER: ViewStyle = {
    flexDirection: 'row',
    alignItems: 'center',
}

export const ICON: ImageStyle = {
    width: metric.ratioWidth(10),
    height: metric.ratioWidth(14),
    marginRight: metric.ratioWidth(10)
}

export const TEXT_CONTENT: TextStyle = {
    fontSize: RFValue(16),
    lineHeight: metric.ratioHeight(30),
    fontWeight: '300',

    //Android
    fontFamily: "Montserrat-Light"
}

export const SMALL_TEXT_CONTENT: TextStyle = {
    ...TEXT_CONTENT,
    fontSize: RFValue(14),
}

export const TEXT_UPPERCASE: TextStyle = {
    textTransform: 'uppercase'
}