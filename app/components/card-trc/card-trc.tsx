import * as React from "react"
import { View } from "react-native"
import { Text } from "../text/text"
import FastImage from "react-native-fast-image"
import * as Styles from "./card-trc.styles"
import { CardTRCProps } from "./card-trc.props"


export function CardTRC(props: CardTRCProps) {
  return (
    <View shouldRasterizeIOS style={Styles.ROOT}>
      <View shouldRasterizeIOS style={[Styles.RECTANGLE_TOP]}>
        <View shouldRasterizeIOS style={Styles.RECTANGLE_LEFT} />
        <View>
          <View shouldRasterizeIOS style={Styles.TRIANGLE_CORNER} />
          <View shouldRasterizeIOS style={Styles.RECTANGLE_CONTENT}>
            <View shouldRasterizeIOS style={Styles.CONTENT_CONTAINER}>
              {props?.iconFirstLine && <FastImage testID="imageIcon" source={props?.iconFirstLine} style={Styles.ICON} resizeMode="contain" />}
              <Text testID="lblFirstLine" accessibilityLabel="lblFirstLine" text={props?.contentFirstLine} style={[Styles.TEXT_CONTENT, Styles.TEXT_UPPERCASE]} />
            </View>
            <View shouldRasterizeIOS style={Styles.CONTENT_CONTAINER}>
              {props?.iconSecondLine && <FastImage testID="imageIcon" source={props?.iconSecondLine} style={Styles.ICON} resizeMode="contain" />}
              <Text testID="lblSecondLine" accessibilityLabel="lblSecondLine" text={props?.contentSecondLine} style={Styles.SMALL_TEXT_CONTENT} />
            </View>
          </View>
          <View shouldRasterizeIOS style={Styles.RECTANGLE_FOOTER} />
        </View>
      </View>
    </View>
  )
}
