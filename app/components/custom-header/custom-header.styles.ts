/*
    Workaround for overlapping between offline mode banner and top bar menu
*/

import { ViewStyle } from "react-native"
import * as metric from "../../theme"

export const HEADER: ViewStyle = {
    marginTop: metric.backButtonHeaderMarginTop
}

export const HEADER_MARGIN_FOR_OFFLINE: ViewStyle = {
    marginTop: metric.actualHeaderMargin
}