import { ViewStyle, TextStyle, ImageStyle } from "react-native"
import * as metric from "../../theme"
import { color } from "../../theme"
import { RFValue } from "react-native-responsive-fontsize"
import { images } from "../../theme/images"

export const DUDETTE_ICON: string = images.dudette

const FULL: ViewStyle = {
    flex: 1
}

export const ROOT_CONTAINNER: ViewStyle = {
    width: '100%',
    height: metric.ratioHeight(76.5),
    backgroundColor: color.bodyPearl
}

export const WRAPPER_CONTAINNER: ViewStyle = {
    ...FULL,
    flexDirection: 'row'
}

export const BODY_CONTAINNER: ViewStyle = {
    ...FULL,
    flexDirection: 'row'
}

export const IMAGE_PROFILE_CONTAINNER: ViewStyle = {
    alignItems: 'flex-end',
    justifyContent: 'center',
    width: metric.ratioWidth(72)
}

export const IMAGE_PROFILE: ImageStyle = {
    width: metric.ratioWidth(50),
    height: metric.ratioWidth(50),
    borderRadius: metric.ratioWidth(50) / 2
}

export const IMAGE_INFO: ImageStyle = {
    width: metric.ratioWidth(13.56),
    height: metric.ratioWidth(13.56),
}

export const CONTENT_CONTAINER: ViewStyle = {
    ...FULL,
    paddingHorizontal: metric.ratioWidth(21),
    justifyContent: 'center'
}

export const CONTENT_HEADER: ViewStyle = {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
}

const TEXT: TextStyle = {
    color: color.palette.darkGrey,
    fontFamily: "Montserrat",
}

export const TITLE: TextStyle = {
    ...TEXT,
    lineHeight: metric.ratioHeight(36),
    fontSize: RFValue(17),
    fontWeight: '400',

    //Android
    fontFamily: "Montserrat-Regular"
}

export const TITLE_VIEW: ViewStyle = {
    width: '66%'
}

export const TIME_AGO_VIEW: ViewStyle | TextStyle = {
    textAlign:'right',
    width: '31%'
}

export const SUB_TITLE: TextStyle = {
    ...TEXT,
    lineHeight: metric.ratioHeight(24),
    fontSize: RFValue(12),
    fontWeight: '300',

    //Android
    fontFamily: "Montserrat-Light",
}