import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { ExtractCSTWithSTN } from 'mobx-state-tree/dist/core/type/type'
import { ChatStoreModel } from '../../models/chat-store'

export interface ChatListCardProps extends NavigationContainerProps<ParamListBase> {
    animation?: any,
    imageProfileTestID?: any,
    fullnameTestID?: any,
    timeAgoTestID?: any,
    chat$?: ExtractCSTWithSTN<typeof ChatStoreModel>,
    onPress?: (favouriteDetails$?: ExtractCSTWithSTN<typeof ChatStoreModel>) => void
    onLongPress?: () => void
}