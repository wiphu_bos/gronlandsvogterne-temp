// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { images } from "../../theme/images"
import FastImage from "react-native-fast-image"
import { View } from "react-native"
import { ChatListCardProps } from "./chat-list-card.props"
import * as Animatable from 'react-native-animatable'
import { Button } from "../button/button"
import { Text } from "../text/text"
import { TimeAgoCompoenents } from "../time-ago"

// MARK: Style Import

import * as Styles from "./chat-list-card.styles"
export const ChatListCardComponents: React.FunctionComponent<ChatListCardProps> = observer((props) => {
    return (
        <Button
            isAnimated={false}
            isSolid
            style={Styles.ROOT_CONTAINNER}
            containerStyle={Styles.ROOT_CONTAINNER}
            onPress={() => props?.onPress(props?.chat$)}
            onLongPress={props?.onLongPress}
        >
            <Animatable.View shouldRasterizeIOS style={Styles.BODY_CONTAINNER}
                {...props?.animation}>
                <View shouldRasterizeIOS style={Styles.IMAGE_PROFILE_CONTAINNER}>
                    <FastImage
                        {...props?.imageProfileTestID}
                        source={props.chat$?.user?.profile_image ? { uri: props.chat$?.user?.profile_image[0]?.url } : images.dudette}
                        style={Styles.IMAGE_PROFILE} />
                </View>
                <View shouldRasterizeIOS style={Styles.CONTENT_CONTAINER}>
                    <View style={Styles.CONTENT_HEADER}>
                        <Text
                            {...props?.fullnameTestID}
                            numberOfLines={1}
                            text={props?.chat$?.user?.full_name}
                            style={{ ...Styles.TITLE, ...Styles.TITLE_VIEW }} />
                        <TimeAgoCompoenents testID={props.timeAgoTestID} timeObject$={props.chat$} />
                    </View>
                    <Text
                        {...props?.fullnameTestID}
                        numberOfLines={1}
                        text={props?.chat$?.last_message?.text}
                        style={Styles.SUB_TITLE} />
                </View>
            </Animatable.View>
        </Button>
    )
})
