import * as React from "react"
import FastImage from 'react-native-fast-image'
import { LoadingIndicatorProps } from "./loadmore-indicator.props"
import * as Styles from "./loadmore-indicator.styles"
import * as Images from "../../theme/images"
import { observer } from "mobx-react-lite"
import { View } from "react-native"

export const LoadMoreIndicator = observer((props: LoadingIndicatorProps) => {
  const { containerStyle } = props

  return (
    <View style={{ ...Styles.CONTAINER, ...containerStyle }}>
      <FastImage source={Images.images.splashLogo} style={Styles.INDICATOR_VIEW} resizeMode={FastImage.resizeMode.contain} />
    </View>
  )
})
