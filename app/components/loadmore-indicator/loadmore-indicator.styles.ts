import { ImageStyle, ViewStyle } from "react-native"
import * as metric from "../../theme"

const iconWidth = metric.ratioWidth(20)
const iconHeight = metric.ratioHeight(24)

export const INDICATOR_VIEW: ImageStyle = {
    width: iconWidth,
    height: iconHeight,
    alignSelf: 'center'
}
export const CONTAINER: ViewStyle = {
    marginTop: metric.ratioHeight(10)
}