
export interface SquareImageProps {
  index:number,
  imageSource:string,
  showDelete?: boolean,
  deleteTestID?: any,
  onDelete?: (item: any) => void
}
