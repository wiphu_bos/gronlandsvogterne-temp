import * as React from "react"
import * as Animatable from 'react-native-animatable'
import { SquareImageProps } from "./squre-image.props"
import { images } from "../../theme/images"
import { Button } from "../button/button"
import * as Styles from "./square-image.styles"
import FastImage from "react-native-fast-image"

export function SquareImage(props: SquareImageProps) {
  const {
    imageSource,
    deleteTestID,
    showDelete,
    onDelete,
    index,
    ...rest
  } = props
  const imageStyle = Styles.SQAURE
  return (
    <Animatable.View useNativeDriver key={index} animation="fadeIn">
      <FastImage style={imageStyle} source={{ uri: imageSource }} {...rest} />
      {(showDelete && imageSource) ?
        <Button 
          isAnimated={false}
          isSolid
          preset="none"
          {...deleteTestID}
          onPress={onDelete}
          imageSource={images.remove}
          containerStyle={Styles.BUTTON_DELETE}
          imageStyle={Styles.REMOVE_ICON} />
        : null}
    </Animatable.View>
  )
}
