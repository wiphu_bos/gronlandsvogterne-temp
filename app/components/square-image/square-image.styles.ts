import { ImageStyle, ViewStyle } from "react-native"
import * as metric from "../../theme"
export const SQAURE: ImageStyle = {
    width: metric.ratioWidth(110),
    height: metric.ratioWidth(110),
}

export const REMOVE_ICON: ImageStyle = {
    width: metric.ratioWidth(20),
    height: metric.ratioWidth(20),
    marginLeft: metric.ratioWidth(7),
    marginTop: metric.ratioHeight(8)
}

export const BUTTON_DELETE: ViewStyle = {
    width: metric.ratioWidth(50),
    height: metric.ratioWidth(34),
    position: 'absolute',
    bottom: 0,
    left: 0
}