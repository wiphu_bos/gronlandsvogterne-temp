import * as React from "react"
import { TouchableOpacity, TouchableOpacityProps, GestureResponderEvent } from "react-native"
import { Text } from "../text/text"
import { Icon } from "../icon/icon"
import { viewPresets, textPresets, imagePresets } from "./button.presets"
import { ButtonProps } from "./button.props"
import { mergeAll, flatten } from "ramda"
import { BouncyView } from "../../libs/bouncy-touchable"
import * as metric from "../../theme"
import { useStores } from "../../models/root-store"
import { withPreventDoubleClick } from '../../libs/with-prevent-double-click'
import { observer } from "mobx-react-lite"

export const Button = observer((props: ButtonProps) => {

  // MARK: Golbal Variable

  const rootStore = useStores()

  const isDisabled: boolean = rootStore.getSharedStore?.getIsLoading

  // MARK: Local Variable - Coming Props

  const defaultValue = {
    animated: true
  }
  const {
    preset = "primary",
    textPreset,
    isSolid,
    imagePreset,
    tx,
    text,
    imageSource,
    containerStyle,
    style: styleOverride,
    textStyle: textStyleOverride,
    imageStyle,
    children,
    isAnimated,
    ...rest
  } = props

  let restProps = { ...rest }
  delete restProps["onPress"]


  // MARK: Event Handler
  const onBeginAnimatedButtonTouch = () => {
  }

  const onEndButtonAnimation = (evt?: GestureResponderEvent) => {
    doOnPressProcess(evt)
  }

  const doOnPressProcess = (evt?: GestureResponderEvent) => {
    if (!isDisabled) {
      rest?.onPress && rest?.onPress(evt)
    }
  }

  // MARK: Compoenent's options

  const activeOpacity = isSolid ? 1 : 0.7
  const onAnimatedViewPress = { onBeginAnimatedButtonTouch: () => onBeginAnimatedButtonTouch(), onEndButtonAnimation: () => onEndButtonAnimation() }
  const viewStyle = mergeAll(flatten([viewPresets[preset] || viewPresets.primary, styleOverride]))
  const containerTouchViewStyle = containerStyle || viewStyle
  const textStyle = mergeAll(flatten([textPresets[textPreset || preset] || textPresets.primary, textStyleOverride]))
  const imgStyle = imagePresets[imagePreset] || imageStyle
  const iconWithImagePreset = imagePreset && <Icon icon={imagePreset} style={imgStyle} />
  const iconWithImageSource = imageSource && <Icon source={imageSource} style={imgStyle} />
  const content = children || iconWithImagePreset || iconWithImageSource || <Text tx={tx} text={text} style={textStyle} />
  const animate = typeof isAnimated == "boolean" ? isAnimated : defaultValue.animated

  // MARK: Components

  const ButtonView = (props: TouchableOpacityProps) =>
    <TouchableOpacity
      style={containerTouchViewStyle}
      onLongPress={props.onLongPress}
      onPress={props.onPress} {...restProps}
      activeOpacity={props.activeOpacity} >
      {content}
    </TouchableOpacity>

  const ButtonViewWithPreventDoubleClick = withPreventDoubleClick(ButtonView)

  const AnimateButtonView = (props: ButtonProps) =>
    <BouncyView style={containerStyle}
      {...metric.bouceButtonOptions}
      {...props}
      {...onAnimatedViewPress} >
      <ButtonViewWithPreventDoubleClick activeOpacity={1} />
    </BouncyView>

  // MARK: Render

  return (
    animate ? <AnimateButtonView /> : <ButtonViewWithPreventDoubleClick activeOpacity={activeOpacity} onPress={doOnPressProcess} />
  )
})