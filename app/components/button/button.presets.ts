import { ViewStyle, TextStyle, ImageStyle } from "react-native"
import { color } from "../../theme"
import { RFValue } from "react-native-responsive-fontsize"
import * as metric from "../../theme"

/**
 * All text will start off looking like this.
 */
const BASE_VIEW: ViewStyle = {
  paddingVertical: metric.ratioHeight(8),
  paddingHorizontal: metric.ratioWidth(8),
  borderRadius: metric.ratioWidth(4),
  justifyContent: "center",
  alignItems: "center",
}
const FLOAT_LEFT: ViewStyle = {
  justifyContent: "center"
}

const BASE_TEXT: TextStyle = {
  fontFamily: "Montserrat",
  paddingHorizontal: metric.ratioWidth(12),
  color: color.palette.white,
}

const REGULAR_TEXT: TextStyle = {
  fontWeight: '400',

  //Android 
  fontFamily: 'Montserrat-Regular'
}

const LIGHT_TEXT: TextStyle = {
  fontWeight: '300',

  //Android 
  fontFamily: 'Montserrat-Light'
}

/**
 * All the variations of text styling within the app.
 *
 * You want to customize these to whatever you need in your app.
 */
export const viewPresets = {
  /**
   * A smaller piece of secondard information.
   */
  primary: { ...BASE_VIEW, backgroundColor: color.palette.orange } as ViewStyle,
  disabled: {},
  left: {
    ...FLOAT_LEFT
  },

  /**
   * A button without extras.
   */
  none: {
    
  },
  link: {
    ...BASE_VIEW,
    paddingHorizontal: 0,
    paddingVertical: 0,
  } as ViewStyle,

  sideMenuBarBadge: {
    borderRadius: metric.ratioWidth(9),
    paddingHorizontal: 0,
    paddingVertical: metric.ratioHeight(3),
    marginLeft: metric.ratioWidth(9),
    backgroundColor: color.palette.blue
  } as ViewStyle,

  shadow: {
    shadowColor: color.palette.black,
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.13
  } as ViewStyle,

  whiteShadow: {
    shadowColor: color.palette.white,
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.7
  } as ViewStyle,

  transparent: {
    ...BASE_VIEW,
    backgroundColor: color.clear,
    borderColor: color.palette.white,
    borderWidth: metric.ratioWidth(2),
    width: metric.ratioWidth(300),
    borderRadius: metric.ratioWidth(16),
    height: metric.ratioHeight(60)
  } as ViewStyle,

  topLeftMenu: {
    ...BASE_VIEW,
    paddingLeft: metric.ratioWidth(24),
    width: metric.ratioWidth(100),
    alignItems: 'flex-start',
  } as ViewStyle,

  topRightMenu: {
    ...BASE_VIEW,
    paddingRight: metric.ratioWidth(24),
    width: metric.ratioWidth(100),
    alignItems: 'flex-end',
  } as ViewStyle,

  bottomRightMenu: {
    ...BASE_VIEW,
    position: 'absolute',
    right: metric.ratioWidth(14),
    bottom: -metric.ratioHeight(2),
    width: metric.ratioWidth(100),
    alignItems: 'flex-end'
  } as ViewStyle,

  topMenuBadge: {
    position: 'absolute',
    bottom: metric.ratioHeight(12),
    left: -metric.ratioWidth(12),
    paddingVertical: metric.ratioHeight(3),
    borderRadius: metric.ratioWidth(5),
    width: metric.ratioWidth(20),
    backgroundColor: color.palette.red,
  } as ViewStyle,
}

export const textPresets = {
  primary: {
    ...BASE_TEXT,
    fontSize: RFValue(17),
    fontWeight: '500',

    //Android 
    fontFamily: 'Montserrat-SemiBold'
  } as TextStyle,

  left: {
    ...BASE_TEXT,
    ...REGULAR_TEXT,
    paddingHorizontal: 0,
    textAlign: 'left'
  } as TextStyle,

  regularNormal: {
    ...BASE_TEXT,
    ...REGULAR_TEXT,
    fontSize: RFValue(13),

  } as TextStyle,
  regularMedium: {
    ...BASE_TEXT,
    ...REGULAR_TEXT,
    fontSize: RFValue(15),

  } as TextStyle,

  link: {
    ...BASE_TEXT,
    color: color.text,
    paddingHorizontal: 0,
    paddingVertical: 0,
  } as TextStyle,

  sideMenuBarBadge: {
    ...BASE_TEXT,
    ...LIGHT_TEXT,
    color: color.palette.white,
    paddingHorizontal: metric.ratioWidth(7),
    paddingVertical: 0,
    fontSize: RFValue(12),

  } as TextStyle,
  topMenuBadge: {
    ...BASE_TEXT,
    ...LIGHT_TEXT,
    color: color.palette.white,
    paddingHorizontal: 0,
    fontSize: RFValue(8),
    textAlign: 'center'
  } as TextStyle,
}
export const imagePresets = {
  back: {
    width: metric.ratioWidth(30),
    height: metric.backButtonHeaderHeight,
    alignItems: 'flex-end'
  } as ImageStyle,
  next: {
    marginLeft: metric.ratioWidth(10),
    width: metric.ratioWidth(40),
    height: metric.ratioHeight(40),
    alignItems: 'flex-end'
  } as ImageStyle,
  close: {
    position: 'relative',
    width: metric.ratioWidth(19),
    height: metric.ratioHeight(19),
    alignSelf: 'center',
  } as ImageStyle
}

/**
 * A list of preset names.
 */
export type ButtonPresetNames = keyof typeof viewPresets

export type TextPresetNames = keyof typeof textPresets

export type ImageButtonPresetNames = keyof typeof imagePresets