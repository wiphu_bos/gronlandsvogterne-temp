import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { StatisticsCounterProps } from '../statistic-counter/statistic-counter.props'
import { UserType } from '../../constants/app.constant'
import { UserModel } from '../../models/user-store/user.store'
import { ExtractCSTWithSTN } from 'mobx-state-tree/dist/core/type/type'

export interface FavouriteCardProps extends NavigationContainerProps<ParamListBase>, StatisticsCounterProps {
    userType?: UserType,
    imageProfileTestID?: any,
    fullnameTestID?: any,
    favouriteDetails$?: ExtractCSTWithSTN<typeof UserModel>,
    onPress?: (favouriteDetails$?: ExtractCSTWithSTN<typeof UserModel>) => void
}