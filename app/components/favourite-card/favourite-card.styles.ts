import { ViewStyle, TextStyle, ImageStyle } from "react-native"
import * as metric from "../../theme"
import { color } from "../../theme"
import { RFValue } from "react-native-responsive-fontsize"
import { images } from "../../theme/images"

export const DUDETTE_ICON: string = images.dudette

const FULL: ViewStyle = {
    flex: 1
}

const SHADOW: ViewStyle = {
    shadowColor: color.palette.black,
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.13,
    elevation: 4
}

export const ROOT_CONTAINNER: ViewStyle = {
    width: metric.ratioWidth(376),
    minHeight: metric.ratioHeight(78),
    borderRadius: metric.ratioWidth(14),
    backgroundColor: color.hardWhiteDim,
    ...SHADOW
}

export const WRAPPER_CONTAINNER: ViewStyle = {
    ...FULL,
    flexDirection: 'row'
}

export const BODY_CONTAINNER: ViewStyle = {
    ...FULL,
    flexDirection: 'row'
}

export const IMAGE_PROFILE_CONTAINNER: ViewStyle = {
    alignItems: 'flex-end',
    justifyContent: 'center',
    width: metric.ratioWidth(72)
}

export const IMAGE_PROFILE: ImageStyle = {
    width: metric.ratioWidth(50),
    height: metric.ratioWidth(50),
    borderRadius: metric.ratioWidth(50) / 2
}

export const IMAGE_INFO: ImageStyle = {
    width: metric.ratioWidth(13.56),
    height: metric.ratioWidth(13.56),
}

export const BODY_CONTAINNER_TEXT: ViewStyle = {
    ...FULL,
    paddingHorizontal: metric.ratioWidth(21)
}

export const CONTAINNER_TEXT: ViewStyle = {
    justifyContent: 'center'
}

const TEXT: TextStyle = {
    color: color.palette.darkGrey,
    fontFamily: "Montserrat",
}

export const TITLE: TextStyle = {
    ...TEXT,
    lineHeight: metric.ratioHeight(36),
    fontSize: RFValue(17),
    fontWeight: '400',

    //Android
    fontFamily: "Montserrat-Regular",
}
