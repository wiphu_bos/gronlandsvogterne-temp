// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { Text, Button } from "../../components"
import { images } from "../../theme/images"
import FastImage from "react-native-fast-image"
import { View } from "react-native"
import { StatisticCounterComponents } from "../statistic-counter"
import { FavouriteCardProps } from "./favourite-card-props"
import { UserType } from "../../constants/app.constant"

// MARK: Style Import

import * as Styles from "./favourite-card.styles"

export const FavouriteCardComponents: React.FunctionComponent<FavouriteCardProps> = observer((props) => {
    return (
        <Button
            isAnimated={false}
            isSolid
            style={Styles.ROOT_CONTAINNER}
            onPress={() => props?.onPress(props?.favouriteDetails$)}
        >
            <View shouldRasterizeIOS style={Styles.WRAPPER_CONTAINNER}>
                <View shouldRasterizeIOS style={Styles.BODY_CONTAINNER}>
                    <View shouldRasterizeIOS style={Styles.IMAGE_PROFILE_CONTAINNER}>
                        <FastImage
                            {...props?.imageProfileTestID}
                            source={props.favouriteDetails$?.profile_image ? { uri: props.favouriteDetails$?.profile_image[0]?.url } : props?.userType === UserType.Dude ? images.dude : images.dudette}
                            style={Styles.IMAGE_PROFILE} />
                    </View>
                    <View shouldRasterizeIOS style={Styles.BODY_CONTAINNER_TEXT}>
                        <View shouldRasterizeIOS style={Styles.CONTAINNER_TEXT}>
                            <Text
                                {...props?.fullnameTestID}
                                text={props?.favouriteDetails$?.full_name}
                                style={Styles.TITLE} />
                            <StatisticCounterComponents
                                primaryCountTestID={props?.primaryCountTestID}
                                unreadPrimaryCountTestID={props?.unreadPrimaryCountTestID}
                                viewCountTestID={props?.viewCountTestID}
                                viewCount={props?.favouriteDetails$?.view_count}
                                primaryCount={props?.primaryCount}
                                unreadPrimaryCount={props?.unreadPrimaryCount}
                                isHighlight={props?.isHighlight}
                            />
                        </View>
                    </View >
                </View >
            </View >
        </Button >
    )
})
