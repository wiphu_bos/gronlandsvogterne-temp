import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { PopoverMenuActionType } from '../popover-menu/popover-menu.props'
import { PrimaryInformationFirstStore } from '../../modules/tabs/pages/primary-infomation-page/page-1/storemodels/pi-first-page.types'
import { UserProfileDetailFirstStore } from '../../modules/tabs/pages/user-profile-detail-page/page-1/storemodels/user-profile-detail-first-page.types'
import { UserStore } from '../../models/user-store/user.types'

export interface ThreeDotMenuProps extends NavigationContainerProps<ParamListBase> {
    publishText?: string,
    unPublishText?: string,
    buttonDeleteText?: string,
    testID?: any,
    rootViewModel?: UserStore
    observeViewModel?: PrimaryInformationFirstStore | UserProfileDetailFirstStore,
    onPopoverMenuPress?: () => void,
    forceClosePopoverMenu?: () => void,
    onPopoverMenuItemPressed?: (type: PopoverMenuActionType) => void,
    onPopoverClosed?: () => void
    onSave?: () => void
    onPublish?: () => void
}