import React from "react"
import { observer } from "mobx-react-lite"
import { Button } from "../button/button"
import * as Styles from "./three-dot-menu.styles"
import { View } from "react-native"
import { images } from "../../theme/images"
import { PopoverMenuActionType, PopoverMenuItem } from "../popover-menu/popover-menu.props"
import { MenuPopover } from "../popover-menu/popover-menu"
import Modal from "../../libs/react-native-modal"
import { DudeStatus } from "../../constants/app.constant"
import { ThreeDotMenuProps } from "./three-dot-menu.props"
export const ThreeDotsMenuComponent: React.FunctionComponent<ThreeDotMenuProps> = observer((props) => {
    const mainMenuObject = () => {
        const status = props?.rootViewModel?.getStatus
        if (status === DudeStatus.Published) {
            return {
                title: props?.unPublishText,
                type: PopoverMenuActionType.DeactivateProfile
            }
        } else if (status === DudeStatus.Unpublished || status === DudeStatus.Approved) {
            return {
                title: props?.publishText,
                type: PopoverMenuActionType.ActivateProfile
            }
        }
        return {
            title: "",
            type: PopoverMenuActionType.Unknown
        }
    }
    const menuItems: PopoverMenuItem[] = [
        mainMenuObject(),
        {
            title: props?.buttonDeleteText,
            type: PopoverMenuActionType.Delete, titleStyle: Styles.DELETE
        }]
    return (
        <View>
            <Button preset="none" containerStyle={Styles.MENU_CONTAINER}
                isAnimated={false}
                imageSource={images.threeDotsMenuWhite}
                {...props?.testID}
                imageStyle={Styles.ICON_MENU}
                onPress={props?.onPopoverMenuPress} />
            {props.observeViewModel?.getIsPopoverMenuVisible ?
                <Modal {...Styles.MODAL_OPTIONS}
                    onBackdropPress={props?.forceClosePopoverMenu}
                    onModalHide={props?.onPopoverClosed}>
                    <View shouldRasterizeIOS style={Styles.POPOVER_MENU_ROOT_CONTAINER}>
                        <MenuPopover containerStyle={Styles.POPOVER_MENU_BODY}
                            items={menuItems}
                            onPress={props?.onPopoverMenuItemPressed} />
                    </View>
                </Modal> : null
            }
        </View >)
})