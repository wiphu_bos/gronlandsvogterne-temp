import { ViewStyle, ImageStyle, TextStyle } from "react-native"
import * as metric from "../../theme"
import { color } from "../../theme/color"
export const FULL: ViewStyle = {
    flex: 1
}

export const MENU_CONTAINER: ViewStyle = {
    alignItems: 'center',
    justifyContent: 'center',
    width: metric.ratioWidth(55),
    height: metric.ratioHeight(30),
    marginRight: -metric.ratioWidth(18),
}

export const ICON_MENU: ImageStyle = {
    width: metric.ratioWidth(4.38),
    height: metric.ratioHeight(20.8),
}

export const POPOVER_MENU_ROOT_CONTAINER: ViewStyle = {
    position: 'absolute',
    right: metric.ratioWidth(10),
    top: metric.ratioHeight(155),
    minWidth: metric.ratioWidth(156),
    minHeight: metric.ratioHeight(100)
}
export const POPOVER_MENU_BODY: ViewStyle = {
    position: 'absolute',
    right: -metric.ratioWidth(10)
}
export const DELETE: TextStyle = {
    color: color.palette.red
}
export const MODAL_OPTIONS: any = {
    isVisible: true,
    backdropColor: color.clear,
    animationIn: "fadeIn",
    animationOut: "fadeOut"
}