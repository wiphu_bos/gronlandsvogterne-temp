// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import * as Styles from "./loading-list-styles"
import { images } from "../../theme/images"
import FastImage from "react-native-fast-image"
import { View } from "react-native-animatable"

export const LoadingListComponent: React.FunctionComponent = observer((props) => {

    // MARK: Render
    return (
        <View style={Styles.CONTAINER}>
            <FastImage source={images.splashLogo} style={Styles.INDICATOR_VIEW} resizeMode={FastImage.resizeMode.contain} />
        </View>
    )
})