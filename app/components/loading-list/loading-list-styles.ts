import { ImageStyle, ViewStyle } from "react-native";
import * as metric from "../../theme"
const iconWidth = metric.ratioWidth(60)
const iconHeight = metric.ratioHeight(62)
export const INDICATOR_VIEW: ImageStyle = {
    alignSelf:'center',
    width: iconWidth,
    height: iconHeight,
    opacity:0.5
}
export const CONTAINER: ViewStyle = {
    justifyContent:'center',
    alignSelf:'center',
    flex:1,
    position:'absolute',
    top:'35%'
}