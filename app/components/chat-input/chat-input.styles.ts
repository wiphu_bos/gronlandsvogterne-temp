import { ViewStyle, TextStyle, ImageStyle } from "react-native"
import * as metric from "../../theme"
import { color } from "../../theme"
import { RFValue } from "react-native-responsive-fontsize"

export const FULL: ViewStyle = {
    flex: 1
}

export const CHAT_INPUT_CONTAINER: ViewStyle = {
    height: metric.ratioHeight(96)
}

export const CHAT_INPUT_CONTENT_VIEW: ViewStyle = {
    paddingVertical: metric.ratioHeight(21.5),
    justifyContent: 'center',
    marginHorizontal: metric.ratioWidth(17)
}
export const SEPERATOR_VIEW: ViewStyle = {
    backgroundColor: color.palette.white,
    height: metric.ratioHeight(1)
}
export const TEXT_INPUT_CONTAINER: ViewStyle = {
    flexDirection: 'row',
    marginHorizontal: metric.ratioWidth(17)
}
const TEXT: TextStyle = {
    fontFamily: "Montserrat",
}
export const TITLE: TextStyle = {
    ...TEXT,
    lineHeight: metric.ratioHeight(36),
    fontSize: RFValue(22),
    fontWeight: '400',

    //Android
    fontFamily: "Montserrat-Regular",
    color: color.palette.white
}

export const TEXT_INPUT: ViewStyle & TextStyle = {
    ...FULL,
    paddingLeft: metric.ratioWidth(15),
    backgroundColor: color.palette.white,
    height: metric.ratioHeight(50),
    paddingTop: metric.ratioHeight(16),
    borderRadius: metric.ratioWidth(10),
    marginRight: metric.ratioWidth(11),

    ...TITLE,
    lineHeight: metric.ratioHeight(0),
    fontSize: RFValue(12),
    color: color.palette.black,
}

export const ICON_CHAT: ImageStyle = {
    width: metric.ratioWidth(50),
    height: metric.ratioWidth(50)
}

export const CURSOR_COLOR: string = color.palette.pink
