// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { ChatInputProps } from "./chat-input.props"
import { View, TextInput } from "react-native"
import { Button } from "../button/button"

// MARK: Style Import

import * as Styles from "./chat-input.styles"


export const ChatTextInputComponents: React.FunctionComponent<ChatInputProps> = observer((props) => {
    return (
        <TextInput
            {...props?.textInputTestID}
            ref={props.textInputRef}
            multiline
            autoCompleteType="off"
            autoCorrect={false}
            selectionColor={Styles.CURSOR_COLOR}
            style={Styles.TEXT_INPUT}
            placeholder={props.placeholder}
            onFocus={props.onFocus}
            onChangeText={props.onChangeText}
            onBlur={props.onBlur}
        />
    )
})

export const ChatSubmitButtonComponents: React.FunctionComponent<ChatInputProps> = observer((props) => {
    return (
        <Button
            {...props?.buttonSubmitTestID}
            isAnimated={false}
            onPress={props.onSubmit}
            isSolid
            preset="none"
            imageSource={props.buttonSubmitIconSource}
            imageStyle={Styles.ICON_CHAT} />
    )
})

export const ChatInputComponents: React.FunctionComponent<ChatInputProps> = observer((props) => {
    return (
        <View style={Styles.CHAT_INPUT_CONTAINER}>
            <View style={Styles.SEPERATOR_VIEW} />
            <View style={Styles.CHAT_INPUT_CONTENT_VIEW}>

                <View style={Styles.TEXT_INPUT_CONTAINER}>
                    <ChatTextInputComponents {...props} />
                    <ChatSubmitButtonComponents {...props} />
                </View>
            </View>
        </View>
    )
})
