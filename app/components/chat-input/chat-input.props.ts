import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { NativeSyntheticEvent, TextInputFocusEventData, GestureResponderEvent } from 'react-native'

export interface ChatInputProps extends NavigationContainerProps<ParamListBase> {
    textInputTestID?: any,
    buttonSubmitTestID?: any,
    textInputRef?: React.MutableRefObject<any>,
    placeholder?: string,
    onFocus?: (e: NativeSyntheticEvent<TextInputFocusEventData>) => void,
    onChangeText?: (text: string) => void,
    onBlur?: (e: NativeSyntheticEvent<TextInputFocusEventData>) => void,
    buttonSubmitIconSource?: string,
    onSubmit?: (event: GestureResponderEvent) => void
}