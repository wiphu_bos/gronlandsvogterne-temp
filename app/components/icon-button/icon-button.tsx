import * as React from "react"
import { ViewStyle, Image, ImageStyle, TextStyle } from "react-native"
import { Text } from "../text/text"
import { spacing } from "../../theme"
import { IconButtonProps } from "./icon-button.props"
import { mergeAll, flatten } from "ramda"
import * as metric from "../../theme"
import { Button } from "../button/button"

const ROOT: ViewStyle = {
  flexDirection: "row",
  paddingVertical: spacing[1],
  alignSelf: "flex-start",
  justifyContent: "center",
  alignItems: "center",
  width: metric.ratioWidth(350),
  height: metric.ratioHeight(60),
}

const DIMENSIONS = { width: metric.ratioWidth(16), height: metric.ratioWidth(16) }


const IMAGE_STYLE: ImageStyle = {
  width: DIMENSIONS.width,
  height: DIMENSIONS.height,
  marginRight: metric.ratioWidth(5)
}
const TEXT_STYLE: TextStyle = {
  lineHeight: metric.ratioHeight(24)
}

export function IconButton(props: IconButtonProps) {
  const {
    icon,
    onPress,
    style,
    textStyle,
    ...rest
  } = props

  const rootStyle = mergeAll(flatten([ROOT, style]))

  return (
    <Button
      preset="none"
      isAnimated={false}
      onPress={onPress}
      style={rootStyle}
      {...rest}
    >
      <Image
        source={icon}
        style={IMAGE_STYLE}
      />
      <Text text={props.text} tx={props.tx} style={[TEXT_STYLE,textStyle]} />
    </Button>
  )
}
