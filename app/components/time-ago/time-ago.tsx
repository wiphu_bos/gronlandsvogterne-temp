import React from "react"
import { observer } from "mobx-react-lite"
import * as Styles from "./time-ago.styles"
import { Text } from "../text"
import { TimeAgoProps } from "./time-ago.props"

export const TimeAgoCompoenents: React.FunctionComponent<TimeAgoProps> = observer((props) => {
    return <Text
        {...props.testID}
        text={props?.timeObject$?.dateTimeAgo}
        style={{ ...Styles.SUB_TITLE, ...Styles.TIME_AGO_VIEW }} />
})