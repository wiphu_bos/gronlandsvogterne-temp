import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { ExtractCSTWithSTN } from 'mobx-state-tree/dist/core/type/type'
import { SharedStoreModel } from '../../models/shared-store/shared.store'

export interface TimeAgoProps extends NavigationContainerProps<ParamListBase> {
    timeObject$?: ExtractCSTWithSTN<typeof SharedStoreModel>,
    testID?: any,
}