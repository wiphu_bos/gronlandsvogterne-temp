import { ViewStyle, TextStyle } from "react-native"
import * as metric from "../../theme"
import { color } from "../../theme"
import { RFValue } from "react-native-responsive-fontsize"

export const TEXT: TextStyle = {
    color: color.palette.darkGrey,
    fontFamily: "Montserrat",
}

export const TIME_AGO_VIEW: ViewStyle | TextStyle = {
    textAlign: 'right',
    width: '37%'
}

export const SUB_TITLE: TextStyle = {
    ...TEXT,
    lineHeight: metric.ratioHeight(24),
    fontSize: RFValue(12),
    fontWeight: '300',

    //Android
    fontFamily: "Montserrat-Light",
}