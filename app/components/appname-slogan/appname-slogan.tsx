import * as React from "react"
import { View } from "react-native"
import * as Styles from "./appname-slogan.styles"
import { Text } from "../text/text"
import { GeneralResources } from "../../constants/firebase/remote-config"

import { AppNameSloganProps } from "./appname-slogan.props"
import { useStores } from "../../models/root-store"

export const AppNameSlogan = (props: AppNameSloganProps) => {

  // MARK: Global Variables

  const rootStore = useStores()
  const { SharedKeys } = GeneralResources

  // MARK: Render
  return (
    <View shouldRasterizeIOS style={props.style || Styles.TEXT_CONTAINER}>
      <Text style={Styles.TITLE} testID="lblAppTitle" accessibilityLabel="lblAppTitle" text={rootStore.getGeneralResourcesStore(SharedKeys.appName, true)} />
      <View shouldRasterizeIOS style={Styles.SPACE_VIEW} />
      <Text style={Styles.SUB_TITLE} testID="lblSubTitle" accessibilityLabel="lblSubTitle" text={rootStore.getGeneralResourcesStore(SharedKeys.slogan, true)} />
    </View>
  )
}
