import { RFValue } from "react-native-responsive-fontsize"
import { color } from "../../theme"
import { ViewStyle, TextStyle } from "react-native"
import * as metric from "../../theme"

export const TEXT_CONTAINER: ViewStyle = {
    marginTop: metric.ratioHeight(230),
    justifyContent: 'flex-start'
}

export const TEXT: TextStyle = {
    color: color.palette.white,
    textAlign: "center",
    fontFamily: "Montserrat",
}
export const TITLE: TextStyle = {
    ...TEXT,
    fontSize: RFValue(26),
    fontWeight: '400',

    //Android
    fontFamily: "Montserrat-Regular",
}
export const SPACE_VIEW: ViewStyle = {
    height: metric.ratioHeight(9)
}

export const SUB_TITLE: TextStyle = {
    ...TEXT,
    fontSize: RFValue(19),
    fontWeight: '300',

    //Android
    fontFamily: "Montserrat-Light",
}
