import { ViewStyle } from "react-native"

export interface AppNameSloganProps {
  style?: ViewStyle | ViewStyle[]
  title?: string,
  subTitle?: string
}