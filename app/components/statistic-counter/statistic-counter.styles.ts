import { ViewStyle, TextStyle, ImageStyle } from "react-native"
import * as metric from "../../theme"
import { RFValue } from "react-native-responsive-fontsize"
import { color } from "../../theme/color"

const ROW_CENTER: ViewStyle = {
    alignItems: 'center',
    flexDirection: 'row'
}

export const IMAGE_CHAT: ImageStyle = {
    width: metric.ratioWidth(14),
    height: metric.ratioHeight(12.25),
}

export const IMAGE_VIEW: ImageStyle = {
    width: metric.ratioWidth(15.75),
    height: metric.ratioHeight(10.5)
}

export const USER_DETAILS_CONTAINNER: ViewStyle = {
    ...ROW_CENTER,
}

export const SUB_USER_DETAILS_CONTAINNER: ViewStyle = {
    ...ROW_CENTER
}

export const SUB_RIGHT_USER_DETAILS_CONTAINNER: ViewStyle = {
    ...ROW_CENTER,
    marginLeft: metric.ratioWidth(15)
}

export const IMAGE_INFO: ImageStyle = {
    width: metric.ratioWidth(13.56),
    height: metric.ratioWidth(13.56),
}
const TEXT: TextStyle = {
    color: color.palette.darkGrey,
    fontFamily: "Montserrat",
}
const SUB_TITLE: TextStyle = {
    ...TEXT,
    lineHeight: metric.ratioHeight(24),
    fontSize: RFValue(12),
    fontWeight: '300',

    //Android
    fontFamily: "Montserrat-Light",
}
export const DESCRIPTION: TextStyle = {
    ...TEXT,
    ...SUB_TITLE,
    lineHeight: metric.ratioHeight(27),
    fontSize: RFValue(12),
    color: color.palette.darkBrown,
    marginLeft: metric.ratioWidth(9),
    letterSpacing: metric.ratioWidth(1.13)
}

export const SUB_INDICATOR_TEXT: TextStyle = {
    ...DESCRIPTION,
    maxWidth: metric.ratioWidth(55)
}

export const INDICATOR_TEXT: TextStyle = {
    ...DESCRIPTION,
    maxWidth: metric.ratioWidth(40)
}


export const INDICATOR_VIEW: ViewStyle = {
    flexDirection: 'row',
}