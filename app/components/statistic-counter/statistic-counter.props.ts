import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"

export interface StatisticsCounterProps extends NavigationContainerProps<ParamListBase> {
    primaryCountTestID?: any,
    unreadPrimaryCountTestID?: any,
    viewCountTestID?: any,
    primaryCount?: number,
    unreadPrimaryCount?: number,
    viewCount?: number,
    isHighlight?: boolean
}