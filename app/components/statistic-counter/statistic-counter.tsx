// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { Text } from "../text"
import { images } from "../../theme/images"
import FastImage from "react-native-fast-image"
import { View } from "react-native"
import { StatisticsCounterProps } from "./statistic-counter.props"

// MARK: Style Import

import * as Styles from "./statistic-counter.styles"

export const StatisticCounterComponents: React.FunctionComponent<StatisticsCounterProps> = observer((props) => {
    return (<View shouldRasterizeIOS style={Styles.USER_DETAILS_CONTAINNER}>
        <View shouldRasterizeIOS style={Styles.SUB_USER_DETAILS_CONTAINNER}>
            <FastImage source={props?.isHighlight ? images.chatPink : images.chatGrey} style={Styles.IMAGE_CHAT} resizeMode={FastImage.resizeMode.contain} />
            <View style={Styles.INDICATOR_VIEW}>
                <Text
                    {...props?.primaryCountTestID}
                    text={props?.primaryCount?.toString()}
                    style={Styles.INDICATOR_TEXT} numberOfLines={1} />
                {props?.unreadPrimaryCount > 0 &&
                    <Text
                        {...props?.unreadPrimaryCountTestID}
                        text={"(+" + props?.unreadPrimaryCount + ")"}
                        style={Styles.SUB_INDICATOR_TEXT}
                        numberOfLines={1} />}
            </View>
        </View>
        <View shouldRasterizeIOS style={Styles.SUB_RIGHT_USER_DETAILS_CONTAINNER}>
            <FastImage source={images.view} style={Styles.IMAGE_VIEW} resizeMode={FastImage.resizeMode.contain} />
            <Text
                {...props?.viewCountTestID}
                text={props?.viewCount?.toString()}
                style={Styles.INDICATOR_TEXT} numberOfLines={1} />
        </View>
    </View>)
})