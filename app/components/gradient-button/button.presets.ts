import { ViewStyle, TextStyle } from "react-native"
import { color } from "../../theme"
import { RFValue } from "react-native-responsive-fontsize"

import * as metric from "../../theme"
/**
 * All text will start off looking like this.
 */
const BASE_VIEW: ViewStyle = {
  paddingVertical: metric.ratioHeight(8),
  paddingHorizontal: metric.ratioWidth(8),
  justifyContent: "center",
  alignItems: "center",
  width: metric.ratioWidth(300),
  borderRadius: metric.ratioWidth(16),
  height: metric.ratioHeight(60),

  //Android Shadow
  elevation: 4
}

const BASE_TEXT: TextStyle = {
  paddingHorizontal: metric.ratioWidth(8),
}

/**
 * All the variations of text styling within the app.
 *
 * You want to customize these to whatever you need in your app.
 */
export const viewPresets = {
  primary: {
    ...BASE_VIEW,
    colors: color.redFleshGradient,
    borderColor: color.palette.white,

  } as ViewStyle,
  disabled: {
    ...BASE_VIEW,
    colors: [color.disabled, color.disabled]
  } as ViewStyle
}

export const textPresets = {
  primary: {
    ...BASE_TEXT,
    fontSize: RFValue(17),
    color: color.palette.white,
    fontWeight: '500',

    //Android 
    fontFamily: 'Montserrat-SemiBold'
  } as TextStyle,
  link: {
    ...BASE_TEXT,
    color: color.text,
    paddingHorizontal: 0,
    paddingVertical: 0,
  } as TextStyle,
}

/**
 * A list of preset names.
 */
export type ButtonPresetNames = keyof typeof viewPresets
