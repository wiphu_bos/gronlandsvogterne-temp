import * as React from "react"
import { Text } from "../text"
import { Button } from "../button/button"
import { viewPresets, textPresets } from "./button.presets"
import { mergeAll, flatten } from "ramda"
import LinearGradient from "react-native-linear-gradient"
import { GradientButtonProps } from "./button.props"

/**
 * For your text displaying needs.
 *
 * This component is a HOC over the built-in React Native one.
 */
export function GradientButton(props: GradientButtonProps) {
  // grab the props
  const {
    preset = "shadow",
    tx,
    text,
    containerStyle,
    style: styleOverride,
    textStyle: textStyleOverride,
    children,
    ...rest
  } = props

  const viewStyle: any = mergeAll(flatten([viewPresets[preset] || viewPresets.primary, styleOverride]))
  const textStyle = mergeAll(
    flatten([textPresets[preset] || textPresets.primary, textStyleOverride]),
  )
  const content = <Text tx={tx} text={text} style={textStyle} />

  return (
    <Button preset={preset} containerStyle={containerStyle} {...rest}>
      <LinearGradient shouldRasterizeIOS
        start={{ x: 0, y: 1 }}
        end={{ x: 1, y: 1 }}
        colors={viewStyle.colors}
        style={viewStyle}>
        {content}
      </LinearGradient>
    </Button>
  )
}
