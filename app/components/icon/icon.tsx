import * as React from "react"
import { View, ImageStyle } from "react-native"
import { IconProps } from "./icon.props"
import { icons } from "./icons"
import FastImage from 'react-native-fast-image'

const ROOT: ImageStyle = {
  resizeMode: "contain",
}

export const Icon = (props: IconProps) => {
  const { style: styleOverride, icon, containerStyle, source } = props
  const style: ImageStyle = { ...ROOT, ...styleOverride }
  const imageSource = icons[icon] || source
  return (
   <View shouldRasterizeIOS style={containerStyle}>
      <FastImage style={style} source={imageSource} resizeMode="contain" />
    </View>
  )
}
