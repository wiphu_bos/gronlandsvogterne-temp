import { images } from "../../../theme/images"
export const icons = {
  close: images.close,
  back: images.back,
  next: images.next,
  warning: images.waring,
  down: images.down,
  plus: images.plus,
  bullet: require("./bullet.png"),
}

export type IconTypes = keyof typeof icons
