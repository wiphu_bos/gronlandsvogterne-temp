import { RootStore } from "../../models/root-store/root.types"
import { NavigationKey, NotificationTopicKey } from "../../constants/app.constant"
import { INotification } from "../../models/notification-store/notification-store.types"
import * as DataUtils from "../../utils/data.utils"
import { DrawerActions, StackActions } from "@react-navigation/core"
import { GeneralResources } from "../../constants/firebase/remote-config/remote-config.constant"
import DudeProfileDetailController from "../../modules/tabs/dude-profile-detail-tab/dude-profile-detail.controllers"
import ChatRoomController from "../../modules/chat-room/chat-room.controllers"
import { IComment, IUserComment } from "../../modules/tabs/pages/dude-profile-page/page-4/storemodels"
import { IChat } from "../../models/chat-store"
import CreateEditProfileController from "../../modules/tabs/create-edit-profile-tab/create-edit-profile.controllers"
import { FirebaseMessagingTypes } from "@react-native-firebase/messaging"
import PushNotification from "react-native-push-notification"

class NotificationHandler {

    public handleNotificationPayload = (rootStore?: RootStore, notification?: FirebaseMessagingTypes.RemoteMessage): boolean => {
        const data: INotification = DataUtils.transformNotificationData(notification)
        const currentScreen: string = rootStore?.getNavigationStore?.getCurrentPageName
        const previousScreen: string = rootStore?.getNavigationStore?.getPreviousPageName
        const notificationStore = rootStore?.getNotificationStore
        const notificationId = data?.notification_id
        const isRegisterDudeEvent = data?.topic?.includes(NotificationTopicKey.PublishDude) || data?.topic?.includes(NotificationTopicKey.CreatedDude)
        let isInSamePage: boolean = false
        let shouldShowForeground: boolean = true
        if (isRegisterDudeEvent &&
            (
                (!previousScreen) ||
                (currentScreen === NavigationKey.EmailSendSuccess && previousScreen === NavigationKey.CreateDude) ||
                (currentScreen === NavigationKey.WaitingDudePublishProfile)
            )
        ) {
            this._notificationRouter(rootStore, data)
        } else {
            //Comment & Report
            if (data?.topic.includes(NotificationTopicKey.Comment) || data?.topic.includes(NotificationTopicKey.ReportComment)) {
                const commentData = data?.data
                const commentDataObj: Partial<IComment> = DataUtils.getCommentObject(commentData)
                //user is in current comment page.
                if (currentScreen === NavigationKey.DudeProfileDetailFourth &&
                    commentDataObj.dude_id === DudeProfileDetailController?.viewModel?.getUserId) {
                    isInSamePage = true
                }
            }
            //Chat
            else if (data?.topic.includes(NotificationTopicKey.CreateMessage)) {
                const chatData = data?.data
                const chatDataObj: Partial<IChat> = DataUtils.getChatMessageObject(rootStore, chatData)
                //user is in current chat page.
                if (currentScreen === NavigationKey.ChatRoom &&
                    chatDataObj?.user?.uid === ChatRoomController?.myProps?.chatRoomObject?.user?.uid) {
                    isInSamePage = true
                }
            }
        }

        //Config for user is in same page before storing it.
        if (isInSamePage) {
            notificationStore?.markReadNotificationById(notificationId)  // no need to await
            shouldShowForeground = false
            data.misc.is_read = true
        }
        rootStore?.getUserStore?.addNotificationToList(data)
        return isRegisterDudeEvent || shouldShowForeground
    }

    public handleNotificationPayloadByUserActions = (rootStore?: RootStore, notification?: FirebaseMessagingTypes.RemoteMessage) => {
        const data: INotification = DataUtils.transformNotificationData(notification)
        this._notificationRouter(rootStore, data)
    }

    private _notificationRouter = (rootStore?: RootStore, params?: INotification) => {
        const notificationStore = rootStore?.getNotificationStore
        const userStore = rootStore?.getUserStore
        const notificationId = params?.notification_id
        const navigationStore = rootStore?.getNavigationStore
        const navigation = navigationStore?.getNavigation
        let baseParams = params
        const layout = baseParams?.navigation?.layout
        const subLayout = baseParams?.navigation?.subLayout
        const popToTopAction: any = StackActions.popToTop()
        //Dude Approved
        if (layout === NavigationKey.CreateEditProfileTab) {
            const dudeData = baseParams?.data
            userStore?.addMyDudeToList(dudeData)
            baseParams = {
                ...baseParams,
                data: DataUtils.getUserProfileObject(rootStore, null, dudeData)
            }
            //Check current page
            if (dudeData?.uid !== CreateEditProfileController?.myProps?.dudeProfileObject?.uid) {
                const goToPage = () => navigation?.navigate(layout, { dudeProfileObject: dudeData })
                Promise.all([navigation?.dispatch(popToTopAction),
                goToPage()])
            }
        }
        //Dude Published
        else if (layout === NavigationKey.ManageProfileTab) {
            const jumpToPage = () => {
                navigation?.dispatch(DrawerActions.jumpTo(layout, {
                    screen: rootStore?.getGeneralResourcesStore(layout, GeneralResources.ManageProfileTabScreen.myDudeProfileTabMenuTitle),
                    params: {
                        shouldShowAddDudeMenu: true
                    },
                }) as any)
            }
            Promise.all([navigation?.dispatch(popToTopAction), jumpToPage()])
        }
        //Comment
        else if (layout === NavigationKey.DudeProfileDetailTab) {
            const data = baseParams?.data
            const dataObj: Partial<IUserComment> = DataUtils.getUserCommentObject(data)
            baseParams = {
                ...baseParams,
                data: dataObj
            }
            const navigationParams = {
                commentObject: baseParams.data?.comment,
                dudeProfileObject: DataUtils.getUserProfileObject(rootStore, null, dataObj?.dudeProfile),
                shouldGoToCommentTab: true
                // screen: subLayout, params: {
                //     dudeProfileObject: DataUtils.getUserProfileObject(rootStore,null,dataObj?.dudeProfile)
                // }
            }
            //Check current page is tabpage & same dude
            if (dataObj?.dudeProfile?.dude_id === DudeProfileDetailController?.viewModel?.getUserId) {
                navigation?.navigate(layout, navigationParams)
            } else {
                navigation?.dispatch(StackActions.push(layout, navigationParams) as any)
            }
        }
        //Chat
        else if (layout === NavigationKey.ChatRoom) {
            const chatData = baseParams?.data
            const chatDataObj: Partial<IChat> = DataUtils.getChatMessageObject(rootStore, chatData)
            baseParams = {
                ...baseParams,
                data: chatDataObj
            }
            const navigationParams = {
                chatRoomObject: chatDataObj,
                shouldDelayFetching: true
            }

            //Check current page is chat room & same receiver id
            if (chatDataObj?.user?.uid !== ChatRoomController?.myProps?.chatRoomObject?.user?.uid) {
                navigation?.dispatch(StackActions.push(layout, navigationParams) as any)
            }
        }
        notificationStore?.markReadNotificationById(notificationId)
        userStore?.setReadNotificationById(notificationId)
    }

    public handleNotificationBadgeNumber = (notification?: FirebaseMessagingTypes.RemoteMessage) => {
        PushNotification.setApplicationIconBadgeNumber(notification.notification?.android?.count)
    }
}

export default new NotificationHandler()

