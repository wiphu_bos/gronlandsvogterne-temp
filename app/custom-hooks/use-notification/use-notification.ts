import { useEffect } from "react"
import { NotificationController } from "./notification.controller"
import { RootStore } from "../../models/root-store/root.types"

export interface NotificationCompletion {
    badge?: boolean
    alert?: boolean
    sound?: boolean
}

export const useNotification = (rootStore?: RootStore) => {
    const controller = new NotificationController(rootStore)
    useEffect(() => {
        controller.requestNotification()
        controller.registerNotificationReceivedForeground()
        controller.registerNotificationOpened()
        controller.registerNotificationReceivedBackground()
        controller.getInitialNotification()

        return () => {
            NotificationController.forgroundMessagingDisposer && NotificationController.forgroundMessagingDisposer()
        }
    }, [])
}
