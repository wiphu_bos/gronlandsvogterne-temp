import NotificationServices from "../../utils/firebase/notification/notification"
import NotificationHandler from "./notification.handler"
import { RootStore } from "../../models/root-store/root.types"
import message, { FirebaseMessagingTypes } from '@react-native-firebase/messaging'
import { Platform } from "react-native"

export class NotificationController {
    private _rootStore: RootStore
    public static forgroundMessagingDisposer: () => void
    private isIos = Platform.OS === "ios"

    constructor(rootStore: RootStore) {
        this._rootStore = rootStore
    }

    public getFCMToken = async () => {
        const token: string = await NotificationServices.getToken()
        this.setAndUpdateFCMToken(token)
    }

    public requestNotification = async () => {
        await this.registerDeviceForRemoteMessages()
        await this.getFCMToken()
        this.onTokenRefresh()
    }

    public registerDeviceForRemoteMessages = async () => {
        if (!NotificationServices.isDeviceRegisteredForRemoteMessages) {
            await NotificationServices.registerDeviceForRemoteMessages()
            await NotificationServices.requestPermission()
        }
    }
    public onTokenRefresh = () => {
        NotificationServices.onTokenRefresh(token => {
            this.setAndUpdateFCMToken(token)
        })
    }

    public setAndUpdateFCMToken = (token: string,) => {
        this._rootStore?.UserStore?.updateDeviceToken(token)
        this._rootStore?.UserStore?.setDeviceToken(token)
    }

    public registerNotificationReceivedForeground = () => {
        NotificationController.forgroundMessagingDisposer = NotificationServices.foregroundStateMessage(async (remoteMessage: FirebaseMessagingTypes.RemoteMessage) => {
            const isShowForeground: boolean = NotificationHandler.handleNotificationPayload(this._rootStore, remoteMessage)
            if (isShowForeground) NotificationServices.createLocalNotification(remoteMessage)
            if (!this.isIos)
                NotificationHandler.handleNotificationBadgeNumber(remoteMessage)
        })
    }

    public registerNotificationOpened = () => {
        NotificationServices.onNotificationOpenedApp(async (remoteMessage: FirebaseMessagingTypes.RemoteMessage) => {
            NotificationHandler.handleNotificationPayloadByUserActions(this._rootStore, remoteMessage)
        })

    }

    public registerNotificationReceivedBackground = () => {
        NotificationServices.setBackgroundMessageHandler(async (remoteMessage: FirebaseMessagingTypes.RemoteMessage) => {
            if (!this.isIos) {
                NotificationHandler.handleNotificationBadgeNumber(remoteMessage)
            }
        })
    }

    public getInitialNotification = () => {
        message().getInitialNotification()
            .then((remoteMessage: FirebaseMessagingTypes.RemoteMessage) => {
                if (remoteMessage) {
                    NotificationHandler.handleNotificationPayloadByUserActions(this._rootStore, remoteMessage)
                }
            })
            .catch((err) => console.log("getInitialNotifiation() failed", err));
    }
}