import { RootStore } from "../models/root-store/root.types"
import { useEffect } from "react"
import BackgroundTimer from 'react-native-background-timer'
import moment, { Moment } from "moment"

export const useTimingHandler = (rootStore?: RootStore) => {
    useEffect(() => {
        const sharedStore = rootStore?.getSharedStore
        const userStore = rootStore?.getUserStore

        const timeTickingProcess = async (serverDate: Moment) => {
            const isSubscription = userStore?.getIsSubscription
            const expiryDate = userStore?.getExpiryDate

            if (isSubscription && expiryDate) {
                const isTimeUp = serverDate.isSameOrAfter(expiryDate)
                if (isTimeUp) {
                    userStore?.setIsSubscription(false)
                    await userStore?.invalidatePremiumAccount()
                    return
                }
                
                const timeDiff = moment.duration(expiryDate.diff(serverDate)).asMilliseconds()
                userStore?.setExpiryDateCount(timeDiff)
            }
            serverDate.add(1, 'second')
            sharedStore.setServerTime(serverDate.toDate())
        }

        const refreshServerDateTime = async () => {
            const dateNow = moment()
            const serverTime = sharedStore?.getServerTime
            const serverDate = serverTime && moment(serverTime) || dateNow //safe, if no serverTime
            await timeTickingProcess(serverDate)
            let timer = BackgroundTimer.setInterval(async () => await timeTickingProcess(serverDate), 1000)
            sharedStore?.setTimer(timer)
        }

        const invalidateTimer = () => {
            BackgroundTimer.clearInterval(sharedStore.getTimer)
        }

        const fetchServerDateTime = async () => {
            await sharedStore?.fetchServerDateTime()
            refreshServerDateTime()
        }
        fetchServerDateTime()

        return () => {
            invalidateTimer()
        }

    }, [])
}