import { FunctionComponent, useEffect } from "react"
import { AppState, Keyboard } from "react-native"
import { observer } from "mobx-react-lite"

interface GlobalAppStateHandlerProps {
}

export const AppStateHandler: FunctionComponent<GlobalAppStateHandlerProps> = observer(props => {
    useEffect(() => {
        AppState.addEventListener('change', handleChange)
        return () => AppState.removeEventListener('change', handleChange)
    }, [])

    const handleChange = (newState: any) => {
        if (newState !== "active") {
            Keyboard.dismiss()
        }
    }
    return (props as any).children
})
