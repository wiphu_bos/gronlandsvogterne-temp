import { useState } from "react"

export const useDrawerInitialRender = () => {
    /*
        Workaround for flashing open when openning the app.
    */
   
   const [isFirstLoadDone, setIsFirstLoadDone] = useState<boolean>(false)
   if(!isFirstLoadDone){ 
       setTimeout(() => { setIsFirstLoadDone(true)}, 1)
   }
   return isFirstLoadDone
}