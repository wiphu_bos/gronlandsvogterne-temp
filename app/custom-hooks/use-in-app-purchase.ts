import { useEffect } from "react"
import RNIap, {
    purchaseErrorListener,
    purchaseUpdatedListener,
    InAppPurchase, SubscriptionPurchase, PurchaseError
} from 'react-native-iap'
import { EmitterSubscription, Alert } from "react-native"
import { RootStore } from "../models/root-store/root.types"
import { InAppPurchaseErrorResponse } from "../constants/firebase/auth/auth-error-response.constant"

export const useInAppPurchase = (rootStore?: RootStore) => {

    useEffect(() => {

        RNIap.initConnection()
        let purchaseUpdateSubscription: EmitterSubscription
        let purchaseErrorSubscription: EmitterSubscription

        const iapTransactionErrorCallback = (error: PurchaseError) => {
            rootStore?.getSharedStore?.setIsLoading(false)
            if (error?.code === InAppPurchaseErrorResponse.E_USER_CANCELLED) {

            } else {
                Alert.alert(null, error.message)
            }
        }
        const iapTransactionUpdateCallback = (purchase: InAppPurchase | SubscriptionPurchase) => {

            rootStore?.getSharedStore?.setIsLoading(false)
            const receipt = purchase.transactionReceipt
            if (receipt) {
                try {
                    // const deliveryResult = deliverOrDownloadFancyInAppPurchase(purchase.transactionReceipt)
                    // if (isSuccess(deliveryResult)) {
                    //     // Tell the store that you have delivered what has been paid for.
                    //     // Failure to do this will result in the purchase being refunded on Android and
                    //     // the purchase event will reappear on every relaunch of the app until you succeed
                    //     // in doing the below. It will also be impossible for the user to purchase consumables
                    //     // again untill you do this.
                    //     if (Platform.OS === 'ios') {
                    // RNIap.finishTransactionIOS(purchase.transactionId);
                    //     } else if (Platform.OS === 'android') {
                    //         // If consumable (can be purchased again)
                    //         RNIap.consumePurchaseAndroid(purchase.purchaseToken);
                    //         // If not consumable
                    //         RNIap.acknowledgePurchaseAndroid(purchase.purchaseToken);
                    //     }

                    //     // From react-native-iap@4.1.0 you can simplify above `method`. Try to wrap the statement with `try` and `catch` to also grab the `error` message.
                    //     // If consumable (can be purchased again)
                    RNIap.finishTransaction(purchase, true);
                    //     // If not consumable
                    //     RNIap.finishTransaction(purchase, false);
                    // } else {
                    //     // Retry / conclude the purchase is fraudulent, etc...
                    // }
                } catch (e) {
                }
            }
        }

        purchaseUpdateSubscription = purchaseUpdatedListener(iapTransactionUpdateCallback)
        purchaseErrorSubscription = purchaseErrorListener(iapTransactionErrorCallback)

        return () => {
            purchaseUpdateSubscription.remove()
            purchaseErrorSubscription.remove()
            purchaseUpdateSubscription = null
            purchaseErrorSubscription = null
            RNIap.endConnection()
        }
    }, [])
}
