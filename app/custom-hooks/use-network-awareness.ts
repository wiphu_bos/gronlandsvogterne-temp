import { useState, useEffect } from 'react'
import NetInfo from '@react-native-community/netinfo'
import { RootStore } from '../models/root-store'
import * as metric from "../theme"

export const useNetworkAwareness = (rootStore: RootStore) => {
  const [networkStatus, setNetworkStatus] = useState<boolean>(true)

  useEffect(() => {
    const unsubscribe = NetInfo.addEventListener(state => {
      rootStore.getSharedStore?.setIsConnected(metric.isIPhone ? state.isConnected : state.isInternetReachable)
      // setNetworkStatus(state.isConnected)
    });

    return () => unsubscribe()
  })

  return [networkStatus, setNetworkStatus]
}