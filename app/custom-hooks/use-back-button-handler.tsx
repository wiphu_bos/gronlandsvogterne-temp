import { FunctionComponent, useEffect } from "react"
import { BackHandler } from "react-native"
import { observer } from "mobx-react-lite"
import { useStores } from "../models/root-store"
import { ListenerKey } from "../constants/app.constant"

interface GlobalBackButtonHandlerProps {
}

export const GlobalBackButtonHandler: FunctionComponent<GlobalBackButtonHandlerProps> = observer(props => {

  // MARK: Global Variables

  const rootStore = useStores()

  useEffect(() => {
    const onBackPress = () => {
      if (rootStore.getNavigationStore.canExit && !rootStore.getSharedStore.getIsEditing) {
        BackHandler.exitApp()
      }
      return true
    }
    BackHandler.addEventListener(ListenerKey.HardwareBackPress, onBackPress)
    return () => BackHandler.removeEventListener(ListenerKey.HardwareBackPress, onBackPress)
  }, [])
  return (props as any).children
})
