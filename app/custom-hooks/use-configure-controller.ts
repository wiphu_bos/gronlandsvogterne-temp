import { useEffect, useCallback } from 'react'
import { useStores } from '../models/root-store'
import { useFocusEffect, ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { IRoute } from '../models'
import { DrawerContentComponentProps, DrawerContentOptions } from '@react-navigation/drawer'
import { MaterialTopTabBarProps } from '@react-navigation/material-top-tabs'
import { useNotification } from "./use-notification"
import { NavigationKey } from '../constants/app.constant'
import { BaseController } from '../modules/base.controller'
import { useInAppPurchase } from './use-in-app-purchase'
import { useTimingHandler } from './use-timing-handler'
export type BaseNavigationProps =
    | NavigationContainerProps<ParamListBase>
    | DrawerContentComponentProps<DrawerContentOptions>
    | MaterialTopTabBarProps & string

export type NavigationProps = {
    navigation?: NavigationContainerProps<ParamListBase>,
    route?: IRoute
}

export const useConfigurate = (controller: typeof BaseController, props?: BaseNavigationProps, isNestedNavigation?: boolean) => {
    const rootStore = useStores()
    const ctrl = new controller(rootStore, props, isNestedNavigation)
    const setupNotification = () => {
        const routeProps = props as NavigationProps
        if (routeProps?.route?.name === NavigationKey.Home) {
            useInAppPurchase(rootStore)
            useNotification(rootStore)
            useTimingHandler(rootStore)
        }
    }
    ctrl.viewWillAppearOnce && ctrl.viewWillAppearOnce()
    setupNotification()
    useEffect(() => {
        const viewDidAppearOnce = async () => await ctrl.viewDidAppearOnce()
        viewDidAppearOnce()
        return () => ctrl.deInit()
    }, [])
    try {
        useFocusEffect(
            useCallback(() => {
                const viewDidAppearAfterFocus = async () => {
                    await ctrl.viewDidAppearAfterFocus(props)
                }
                ctrl.viewDidAppearAfterFocus && viewDidAppearAfterFocus()
                return () => ctrl.viewWillDisappear && ctrl.viewWillDisappear()
            }, [props])
        )
    } catch (e) {
    }
    return ctrl
}