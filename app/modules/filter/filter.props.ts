import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { SearchListType } from '../../constants/app.constant'

export interface FilterProps extends NavigationContainerProps<ParamListBase> {
    searchType: SearchListType
}