import { types } from "mobx-state-tree"
import { FilterPropsModel } from "./filter.models"
export const FilterActions = types.model(FilterPropsModel).actions(self => {
    const setIsModalShowing = (value: boolean) => self.isModalShowing = value
    return {
        setIsModalShowing
    }
})