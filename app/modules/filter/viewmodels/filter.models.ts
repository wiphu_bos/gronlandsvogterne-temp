import { types } from "mobx-state-tree"

export const FilterPropsModel = {
    isModalShowing: types.optional(types.boolean, false)
}