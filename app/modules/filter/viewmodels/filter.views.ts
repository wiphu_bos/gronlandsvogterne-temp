import { types } from "mobx-state-tree"
import { FilterPropsModel } from "./filter.models"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store/general-resources.store"
import { NavigationKey } from "../../../constants/app.constant"
import { TestIDResources } from "../../../constants/test-key/test.constant"
import * as Utils from "../../../utils"
import { GeneralResources } from "../../../constants/firebase/remote-config/remote-config.constant"
export const FilterViews = types.model(FilterPropsModel)
    .views(self => ({
        get getIsModalShowing() {
            return self.isModalShowing
        },
        
    }))

export const FilterResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State
    const { Filters } = GeneralResources
    const { screenTitle,
        ageTitle,
        tagTitle,
        interestTitle,
        havdSogerHanTitle,
        regionTitle,
        buttonSaveAndContinueTitle,
        buttonSelectAllTitle,
        buttonSelectTitle,
        modalButtonCancelTitle,
        modalButtonClearTitle,
        modalButtonOKTitle,
        modalInterestPlaceholder,
        modalRegionPlaceholder,
        seekingGuyTitle
    } = Filters
    const { Filters: FiltersScreen } = NavigationKey

    //MARK: Views
    const getResources = (key: string, childKeyOrShareKey: string | boolean = false) => self.getValues(childKeyOrShareKey ? key : FiltersScreen, childKeyOrShareKey ? true : key)
    const getResourceScreenTitle = () => getResources(screenTitle)
    const getResourceAgeTitle = () => getResources(ageTitle)
    const getResourceInterestTitle = () => getResources(interestTitle)
    const getResourceHavdSogerHanTitle = () => getResources(havdSogerHanTitle)
    const getResourceRegionTitle = () => getResources(regionTitle)
    const getResourceTagTitle = () => getResources(tagTitle)
    const getResourceButtonSaveAndContinueTitle = () => getResources(buttonSaveAndContinueTitle, true)
    const getResourceButtonSelectAllTitle = () => getResources(buttonSelectAllTitle, true)
    const getResourceButtonSelectTitle = () => getResources(buttonSelectTitle, true)
    const getResourceModalButtonCancelTitle = () => getResources(modalButtonCancelTitle, true)
    const getResourceModalButtonClearTitle = () => getResources(modalButtonClearTitle, true)
    const getResourceModalButtonOKTitle = () => getResources(modalButtonOKTitle, true)
    const getResourceModalInterestPlaceholder = () => getResources(modalInterestPlaceholder, true)
    const getResourceModalRegionPlaceholder = () => getResources(modalRegionPlaceholder,true)
    const getResourceSeekingGuyTitle = () => getResources(seekingGuyTitle)

    return {
        getResourceScreenTitle,
        getResourceAgeTitle,
        getResourceInterestTitle,
        getResourceHavdSogerHanTitle,
        getResourceRegionTitle,
        getResourceTagTitle,
        getResourceButtonSaveAndContinueTitle,
        getResourceButtonSelectAllTitle,
        getResourceButtonSelectTitle,
        getResourceModalButtonCancelTitle,
        getResourceModalButtonClearTitle,
        getResourceModalButtonOKTitle,
        getResourceModalInterestPlaceholder,
        getResourceModalRegionPlaceholder,
        getResourceSeekingGuyTitle
    }
})
    .views(self => {

        //MARK: Volatile State

        //MARK: Views

        const getTestIDTitle = () => Utils.getTestIDObject(TestIDResources.Filter.title)
        const getTestIDRegionSelected = () => Utils.getTestIDObject(TestIDResources.Filter.regionSelected)
        const getTestIDInterestSelected = () => Utils.getTestIDObject(TestIDResources.Filter.interestSelected)
        const getTestIDAgeTitle = () => Utils.getTestIDObject(TestIDResources.Filter.ageTitle)
        const getTestIDModalPicker = () => Utils.getTestIDObject(TestIDResources.Filter.modalPicker)
        const getTestIDSelectAll = () => Utils.getTestIDObject(TestIDResources.Filter.selectAll)
        const getTestIDSliderBarAgeRange = () => Utils.getTestIDObject(TestIDResources.Filter.sliderBarAgeRange)
        const getTestIDTagTitle = () => Utils.getTestIDObject(TestIDResources.Filter.tagTitle)
        const getTestIDRegionTitle = () => Utils.getTestIDObject(TestIDResources.Filter.regionTitle)
        const getTestIDInterestTitle = () => Utils.getTestIDObject(TestIDResources.Filter.interestTitle)
        const getTestIDPickerInterest = () => Utils.getTestIDObject(TestIDResources.Filter.pickerInterest)
        const getTestIDPickerRegion = () => Utils.getTestIDObject(TestIDResources.Filter.pickerRegion)
        const getTestIDButtonTagItem = () => Utils.getTestIDObject(TestIDResources.Filter.buttonTagItem)
        const getTestIDButtonSaveAndContinue = () => Utils.getTestIDObject(TestIDResources.Filter.buttonSaveAndContinue)
        const getTestIDSeekingGuy = () => Utils.getTestIDObject(TestIDResources.Filter.swhSeekingGuy)

        return {
            getTestIDTitle,
            getTestIDAgeTitle,
            getTestIDModalPicker,
            getTestIDSelectAll,
            getTestIDSliderBarAgeRange,
            getTestIDTagTitle,
            getTestIDRegionTitle,
            getTestIDInterestTitle,
            getTestIDPickerInterest,
            getTestIDButtonTagItem,
            getTestIDButtonSaveAndContinue,
            getTestIDPickerRegion,
            getTestIDRegionSelected,
            getTestIDInterestSelected,
            getTestIDSeekingGuy
        }
    })

