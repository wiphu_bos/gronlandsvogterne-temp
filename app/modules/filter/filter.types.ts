import { Instance, SnapshotOut } from "mobx-state-tree"
import { FilterStoreModel } from "./storemodels"

export type FilterStore = Instance<typeof FilterStoreModel>
export type FilterStoreSnapshot = SnapshotOut<typeof FilterStoreModel>