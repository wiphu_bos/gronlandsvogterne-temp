import { ViewStyle, ImageStyle, TextStyle } from "react-native"
import { color } from "../../theme"
import * as metric from "../../theme"
import { images } from "../../theme/images"
import { HeaderProps } from "../../components/header/header.props"
import { RFValue } from "react-native-responsive-fontsize"

export const FULL: ViewStyle = {
    flex: 1
}

export const SCROLL_VIEW: ViewStyle = {
    paddingBottom: metric.ratioHeight(25),
    flexGrow: 1
}
export const TEXT: TextStyle = {
    color: color.palette.white,
    fontFamily: "Montserrat",
}

export const TITLE: TextStyle = {
    ...TEXT,
    fontSize: RFValue(24),
    fontWeight: '400',

    //Android
    fontFamily: "Montserrat-Regular",
}

export const SUB_TITLE: TextStyle = {
    ...TITLE,
    fontSize: RFValue(17)
}

export const SAFE_AREA_VIEW: ViewStyle = {
    ...FULL
}

export const SEPARATOR_VIEW: ViewStyle = {
    marginTop: metric.ratioHeight(16)
}

export const TITLE_CONTAINNER: ViewStyle = {
    marginBottom: metric.ratioHeight(14)
}

export const CONTAINER_MENU_VIEW: ViewStyle = {
    marginHorizontal: metric.ratioWidth(24),
    marginTop: metric.ratioHeight(40),
}

export const MENU_VIEW: ViewStyle = {
    flexDirection: 'row',
    justifyContent: 'space-between'
}


export const CLOSE_ICON_STYLE: ImageStyle = {
    width: metric.ratioWidth(17.5),
    height: metric.ratioWidth(17.5)
}

export const HEADER_PROPS: HeaderProps = {
    rightIconStyle: CLOSE_ICON_STYLE,
    rightIconSource: images.close,
    isRightIconAnimated: false,
    containerStyle: {
        marginTop: metric.isIPhone ? metric.ratioHeight(50) : 0
    }
}

export const TAG_TITLE_CONTAINER: ViewStyle = {
    alignItems: 'flex-start',
    marginTop: metric.ratioHeight(17),
}

export const TAG_CONTAINER: ViewStyle = {
    marginTop: metric.ratioHeight(16),
    backgroundColor: color.brightDim
}

export const TAG_CONTENT: ViewStyle = {
    marginTop: metric.ratioHeight(0),
    paddingBottom: metric.ratioHeight(10)
}

export const BUTTON_MARGIN: ViewStyle = {
    marginTop: metric.ratioHeight(30),
    marginBottom: metric.ratioHeight(24),
    alignSelf: 'center'
}
export const ICON_SELECTION: ImageStyle = {
    marginLeft: metric.ratioWidth(18),
    width: metric.ratioWidth(21),
    height: metric.ratioWidth(21),
    marginRight: metric.ratioWidth(28),
}