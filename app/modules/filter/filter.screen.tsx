// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { Wallpaper, Header, AnimatedScrollView } from "../../components"
import { FilterProps } from "./filter.props"
import FilterController from "./filter.controllers"
import { SafeAreaView } from "react-navigation"
import { useConfigurate } from "../../custom-hooks/use-configure-controller"
import { FilterTitleMenuComponent } from "./views/filter-title-menu"
import { FilterSelectionComponent } from "./views/filter-selection"
import * as metric from "../../theme"
import { FilterRangeComponent } from "./views/filter-range"
import { RangeAgeComponent } from "./views/range-age"
import { UserTagsComponent } from "../../components/user-tags"
import ModalFilterPicker from '../../libs/modal-filter-picker/modal-filter-picker'
import { GradientButton } from "../../components/gradient-button/button"

// MARK: Style Import

import * as Styles from "./filter.styles"
import { SeekingGuyComponent } from "./views/filter-seeking-guy"
import { SearchListType } from "../../constants/app.constant"
import { INavigationRoute } from "../../models"

export const FilterScreen: React.FunctionComponent<FilterProps & Partial<INavigationRoute>> = observer((props) => {

    const controller = useConfigurate(FilterController, props) as FilterController
    const renderSeeking = () => {
       return FilterController.myProps?.searchType === SearchListType.Dudette ?
            <SeekingGuyComponent description={FilterController.resourcesViewModel?.getResourceSeekingGuyTitle()}
                onToggle={(value) => { controller.onSeekingGuyToggled(value) }}
                value={FilterController.viewModel?.getIsSeekingGuy}
                seekingGuyTestID={FilterController.resourcesViewModel?.getTestIDSeekingGuy}
                descriptionTestID={FilterController.resourcesViewModel?.getTestIDInterestSelected()} /> : null
    }
    // MARK: Render
    return (
        <Wallpaper>
            <SafeAreaView
                {...metric.safeAreaViewProps}
                shouldRasterizeIOS
                style={Styles.SAFE_AREA_VIEW}>
                <Header {...Styles.HEADER_PROPS}
                    onRightPress={controller.onClose} />
                <FilterTitleMenuComponent
                    titleTestID={FilterController.resourcesViewModel?.getTestIDTitle()}
                    leftText={FilterController.resourcesViewModel?.getResourceScreenTitle()}
                    leftTextStyle={Styles.TITLE}
                    containerStyle={Styles.TITLE_CONTAINNER} />
                <AnimatedScrollView
                    contentContainerStyle={Styles.SCROLL_VIEW}
                    {...metric.solidScrollView}>

                    {renderSeeking()}

                    <FilterSelectionComponent
                        isSelectAll={FilterController.viewModel?.getIsSelectedAllRegions}
                        containerStyle={Styles.SEPARATOR_VIEW}
                        onSelectAllTestID={FilterController.resourcesViewModel?.getTestIDSelectAll()}
                        onSelectTestID={FilterController.resourcesViewModel?.getTestIDPickerRegion()}
                        titleTestID={FilterController.resourcesViewModel?.getTestIDRegionTitle()}
                        leftText={FilterController.resourcesViewModel?.getResourceRegionTitle()}
                        onSelect={controller.onSelectRegion}
                        onSelectAll={controller.onSelectAllRegions}
                        leftTextStyle={Styles.SUB_TITLE}
                        description={!FilterController.viewModel?.getIsSelectedAllRegions && FilterController.viewModel?.getRegionList?.map(e => e.value)?.join(', ')}
                        descriptionTestID={FilterController.resourcesViewModel?.getTestIDRegionSelected()}
                    />
                    <FilterRangeComponent
                        titleTestID={FilterController.resourcesViewModel?.getTestIDAgeTitle()}
                        sliderTestID={FilterController.resourcesViewModel?.getTestIDSliderBarAgeRange()}
                        leftText={FilterController.resourcesViewModel?.getResourceAgeTitle()}
                        onMinValueChanged={controller.onMinValueChanged}
                        onMaxValueChanged={controller.onMaxValueChanged}
                        leftTextStyle={Styles.SUB_TITLE}
                        containerStyle={Styles.SEPARATOR_VIEW}
                        initialMinAge={FilterController.viewModel?.getMinAge}
                        initialMaxAge={FilterController.viewModel?.getMaxAge}
                        customRightView={<RangeAgeComponent
                            minAge={FilterController.viewModel?.getMinAge}
                            maxAge={FilterController.viewModel?.getMaxAge}
                            textStyle={Styles.SUB_TITLE} />} />
                    <FilterSelectionComponent
                        isSelectAll={FilterController.viewModel?.getIsSelectedAllInterests}
                        onSelectAllTestID={FilterController.resourcesViewModel?.getTestIDSelectAll()}
                        onSelectTestID={FilterController.resourcesViewModel?.getTestIDPickerInterest()}
                        titleTestID={FilterController.resourcesViewModel?.getTestIDInterestTitle()}
                        leftText={FilterController.resourcesViewModel?.getResourceInterestTitle()}
                        onSelect={controller.onSelectInterest}
                        onSelectAll={controller.onSelectAllInterests}
                        leftTextStyle={Styles.SUB_TITLE}
                        containerStyle={Styles.SEPARATOR_VIEW}
                        description={!FilterController.viewModel?.getIsSelectedAllInterests && FilterController.viewModel?.getInterestList?.map(e => e.value)?.join(', ')}
                        descriptionTestID={FilterController.resourcesViewModel?.getTestIDInterestSelected()}
                    />
                    <UserTagsComponent
                        selectAllTitle={FilterController.resourcesViewModel?.getResourceButtonSelectAllTitle()}
                        titleTestID={FilterController.resourcesViewModel?.getTestIDTagTitle()}
                        buttonItemTestID={FilterController.resourcesViewModel?.getTestIDButtonTagItem()}
                        observeViewModel={FilterController.viewModel}
                        customHeaderView={<FilterTitleMenuComponent
                            containerStyle={Styles?.TAG_TITLE_CONTAINER}
                            leftText={FilterController.resourcesViewModel?.getResourceTagTitle()}
                            leftTextStyle={Styles.SUB_TITLE} />}
                        tagTitle={FilterController.resourcesViewModel?.getResourceTagTitle()}
                        tagList={FilterController.resourcesViewModel?.getTags}
                        onTagPressed={controller.onTagPressed}
                        containerStyle={Styles?.TAG_CONTAINER}
                        contentStyle={Styles?.TAG_CONTENT}
                        onSelectAllPressed={controller.onSelectAllTagsPressed}
                        isSelectedAll={FilterController.viewModel?.getIsSelectedAllTags}
                    />
                    <GradientButton
                        isAnimated={false}
                        isSolid
                        {...FilterController.resourcesViewModel?.getTestIDButtonSaveAndContinue()}
                        text={FilterController.resourcesViewModel?.getResourceButtonSaveAndContinueTitle()}
                        style={Styles.BUTTON_MARGIN}
                        onPress={controller.onSaveAndContinueButtonDidTouch} />
                </AnimatedScrollView>
            </SafeAreaView>
            <ModalFilterPicker
                {...FilterController.resourcesViewModel?.getTestIDModalPicker()}
                cancelButtonText={FilterController.resourcesViewModel?.getResourceModalButtonCancelTitle()}
                okButtonText={FilterController.resourcesViewModel?.getResourceModalButtonOKTitle()}
                clearButtonText={FilterController.resourcesViewModel?.getResourceModalButtonClearTitle()}
                visible={FilterController.viewModel?.getIsModalShowing || false}
                {...controller.getSelectOptions()}
                iconSelectionStyle={Styles.ICON_SELECTION}
            />
        </Wallpaper>
    )
})
