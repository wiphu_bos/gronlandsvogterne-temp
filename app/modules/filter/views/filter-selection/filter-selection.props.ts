import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { FilterTitleMenuProps } from '../filter-title-menu'

export interface FilterSelectionProps extends NavigationContainerProps<ParamListBase>, FilterTitleMenuProps {
    onSelectAllTestID?: any,
    onSelectTestID?: any,
    onSelectAll?: () => void,
    onSelect?: () => void,
    isSelectAll?: boolean,
    description?: string,
    descriptionTestID?: any
}