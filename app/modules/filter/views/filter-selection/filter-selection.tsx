// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { FilterSelectionProps } from "./filter-selection.props"
import * as Styles from "./filter-selection.styles"
import { View } from "react-native"
import { FilterTitleMenuComponent } from "../filter-title-menu"
import { Button, Text } from "../../../../components"
import FilterController from "../../filter.controllers"

export const FilterSelectionComponent: React.FunctionComponent<FilterSelectionProps> = observer((props) => {

    // MARK: Render

    const buttonSelectAllStyle = props?.isSelectAll ?
        { ...Styles.BUTTON_SELECT_ALL, ...Styles.SELECTED_STYLE } :
        { ...Styles.BUTTON_SELECT_ALL, ...Styles.DESELECT_STYLE }

    const buttonSelectStyle = !props?.isSelectAll ?
        { ...Styles.BUTTON_SELECT, ...Styles.SELECTED_STYLE } :
        { ...Styles.BUTTON_SELECT, ...Styles.DESELECT_STYLE }

    const textSelectAllStyle = props?.isSelectAll ?
        { ...Styles.TITLE, ...Styles.SELECTED_STYLE } :
        { ...Styles.TITLE, ...Styles.DESELECT_STYLE }

    const textSelectStyle = !props?.isSelectAll ?
        { ...Styles.TITLE, ...Styles.SELECTED_STYLE } :
        { ...Styles.TITLE, ...Styles.DESELECT_STYLE }

    return (
        <View style={{ ...Styles.CONTAINNER_STYLE, ...props?.containerStyle }} >
            <FilterTitleMenuComponent
                titleTestID={props?.titleTestID}
                leftText={props?.leftText}
                leftTextStyle={{ ...props.leftTextStyle, ...Styles.TOP }}
                containerStyle={Styles.TITLE_CONTAINER} />
            <View style={Styles.TOGGLE_CONTAINNER}>
                <Button preset="none"
                    onPress={props?.onSelectAll}
                    isAnimated={false}
                    isSolid text={FilterController.resourcesViewModel?.getResourceButtonSelectAllTitle()}
                    textStyle={textSelectAllStyle}
                    containerStyle={buttonSelectAllStyle} />
                <Button preset="none"
                    onPress={props?.onSelect}
                    isAnimated={false}
                    isSolid text={FilterController.resourcesViewModel?.getResourceButtonSelectTitle()}
                    textStyle={textSelectStyle}
                    containerStyle={buttonSelectStyle} />
            </View>
            {props?.description && <View style={Styles.DESCRIPTION}>
                <Text style={Styles.DESCRIPTION_TEXT} text={props?.description} {...props?.descriptionTestID} />
            </View>}
        </View>
    )
})