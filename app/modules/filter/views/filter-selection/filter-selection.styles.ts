import { ViewStyle, TextStyle } from "react-native"
import * as metric from "../../../../theme"
import { color } from "../../../../theme"
import { RFValue } from "react-native-responsive-fontsize"

const FULL: ViewStyle = {
    flex: 1
}

const TEXT: TextStyle = {
    fontFamily: "Montserrat",
}

const CENTER: TextStyle = {
    textAlign: 'center'
}

const LEFT: TextStyle = {
    textAlign: 'left'
}

export const TOP: TextStyle = {
    textAlignVertical: 'top'
}
export const TITLE: TextStyle = {
    ...TEXT,
    ...CENTER,
    fontSize: RFValue(16),
    fontWeight: '400',

    //Android
    fontFamily: "Montserrat-Regular"
}

export const DESCRIPTION_TEXT: TextStyle = {
    ...TITLE,
    ...LEFT,
    fontSize: RFValue(12)
}
export const CONTAINNER_STYLE: ViewStyle = {
    ...FULL,
    backgroundColor: color.brightDim,
    justifyContent: 'center',
    minHeight: metric.ratioHeight(133),
    paddingVertical: metric.ratioHeight(18)
}
export const TITLE_CONTAINER: ViewStyle = {
    alignItems: 'flex-start',
    height: metric.ratioHeight(27),
    marginBottom: metric.ratioHeight(15)
}

export const TOGGLE_CONTAINNER: ViewStyle = {
    flexDirection: 'row',
    marginHorizontal: metric.ratioWidth(28),
    marginBottom: metric.ratioHeight(10)
}

export const DESCRIPTION: ViewStyle = {
    marginTop: metric.ratioHeight(5),
    marginHorizontal: metric.ratioWidth(28),
}


const ROUND_LEFT: ViewStyle = {
    borderBottomLeftRadius: metric.ratioWidth(10),
    borderTopLeftRadius: metric.ratioWidth(10),
}
const ROUND_RIGHT: ViewStyle = {
    borderBottomRightRadius: metric.ratioWidth(10),
    borderTopRightRadius: metric.ratioWidth(10)
}
const BUTTON: ViewStyle = {
    ...FULL,
    justifyContent: 'center',
    height: metric.ratioHeight(50),
}

export const BUTTON_SELECT_ALL: ViewStyle = {
    ...BUTTON,
    ...ROUND_LEFT
}

export const BUTTON_SELECT: ViewStyle = {
    ...BUTTON,
    ...ROUND_RIGHT
}

export const SELECTED_STYLE: ViewStyle & TextStyle = {
    color: color.palette.white,
    backgroundColor: color.palette.pink
}

export const DESELECT_STYLE: ViewStyle & TextStyle = {
    color: color.palette.pink,
    backgroundColor: color.palette.white
}