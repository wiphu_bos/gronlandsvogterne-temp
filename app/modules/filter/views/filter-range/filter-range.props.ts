import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { FilterTitleMenuProps } from '../filter-title-menu'

export interface FilterRangeProps extends NavigationContainerProps<ParamListBase>, FilterTitleMenuProps {
    sliderTestID?: any,
    onMinValueChanged?: (value: number) => void,
    onMaxValueChanged?: (value: number) => void,
    initialMinAge?: number,
    initialMaxAge?: number
}