import { ViewStyle, TextStyle } from "react-native"
import * as metric from "../../../../theme"
import { color } from "../../../../theme"

export const CENTER: TextStyle = {
    textAlign: 'center'
}
export const TOP: TextStyle = {
    textAlignVertical: 'top'
}
export const TITLE_CONTAINER: ViewStyle = {
    alignItems: 'flex-start',
    height: metric.ratioHeight(27),
    marginBottom: metric.ratioHeight(8)
}
export const CONTAINNER_STYLE: ViewStyle = {
    backgroundColor: color.brightDim,
    height: metric.ratioHeight(114),
    justifyContent: 'center'
}

export const SLIDER_STYLE: ViewStyle = {
    marginHorizontal: metric.ratioWidth(35),
    height: metric.ratioHeight(40)
}