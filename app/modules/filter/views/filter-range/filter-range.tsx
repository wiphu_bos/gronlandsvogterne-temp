// MARK: Import

import React, { useRef } from "react"
import { observer } from "mobx-react-lite"
import { FilterRangeProps } from "./filter-range.props"
import * as Styles from "./filter-range.styles"
import { View } from "react-native"
import { FilterTitleMenuComponent } from "../filter-title-menu"
// import RangeSlider from "../../../../libs/range-slider"
import * as metric from "../../../../theme"
import RangeSlider from 'rn-range-slider'

export const FilterRangeComponent: React.FunctionComponent<FilterRangeProps> = observer((props) => {

    // MARK: Render

    const sliderRef = useRef(null)
    let min = metric.minAge
    let max = metric.starterMaxAge
    let isFocusingLow: boolean = false
    let isFocusingHigh: boolean = false

    return (
        <View style={{ ...Styles.CONTAINNER_STYLE, ...props?.containerStyle }} >
            <FilterTitleMenuComponent leftText={props?.leftText}
                leftTextStyle={{ ...props.leftTextStyle, ...Styles.TOP }}
                containerStyle={Styles.TITLE_CONTAINER}
                customRightView={props?.customRightView} />
            <RangeSlider
                ref={sliderRef}
                style={Styles.SLIDER_STYLE} //Error from libs
                {...metric.ageRangeOptions}
                initialLowValue={props?.initialMinAge || min}
                initialHighValue={props?.initialMaxAge || max}
                onValueChanged={(low: number, high: number, fromUser: boolean) => {
                    isFocusingLow = min != low
                    isFocusingHigh = max != high
                    const isOverlappedFromHight = high <= low
                    const isOverlappedFrowLow = low >= high
                    if (isOverlappedFromHight && isFocusingHigh) {
                        sliderRef.current.setHighValue(low + 1)
                    } else if (isOverlappedFrowLow && isFocusingLow) {
                        sliderRef.current.setLowValue(high - 1)
                    }
                    props?.onMinValueChanged(low)
                    props?.onMaxValueChanged(high)
                    min = low
                    max = high
                }}
            />
        </View>
    )
})