import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { FilterTitleMenuProps } from '../filter-title-menu'
import { TextStyle } from 'react-native'

export interface RangeAgeProps extends NavigationContainerProps<ParamListBase>, FilterTitleMenuProps {
    minAge?: number,
    maxAge?: number,
    textStyle: TextStyle
}