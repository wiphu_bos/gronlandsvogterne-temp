import { ViewStyle } from "react-native"
import * as metric from "../../../../theme"

export const TITLE_CONTAINER: ViewStyle = {
    alignItems: 'flex-start',
    height: metric.ratioHeight(25),
    marginBottom: metric.ratioHeight(17)
}
export const CONTAINNER_STYLE: ViewStyle = {
    justifyContent: 'center',
    alignItems: 'center'
}
