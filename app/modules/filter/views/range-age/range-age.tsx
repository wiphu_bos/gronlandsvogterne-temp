// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { RangeAgeProps } from "./range-age.props"
import * as Styles from "./range-age.styles"
import { View } from "react-native"
import { Text } from "../../../../components"

export const RangeAgeComponent: React.FunctionComponent<RangeAgeProps> = observer((props) => {

    // MARK: Render

    return (
        <View style={{ ...Styles.CONTAINNER_STYLE, ...props?.containerStyle }} >
            <Text text={props?.minAge?.toString() + " - " + props?.maxAge?.toString()} style={props?.textStyle} />
        </View>
    )
})