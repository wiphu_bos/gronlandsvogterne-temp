import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { ViewStyle } from 'react-native'

export interface SeekingGuyProps extends NavigationContainerProps<ParamListBase> {
    containerStyle?: ViewStyle
    description?: string,
    descriptionTestID?: any,
    seekingGuyTestID?: any,
    onToggle: (newValue: boolean) => void,
    value: boolean
}