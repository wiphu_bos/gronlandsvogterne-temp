import { ViewStyle, TextStyle } from "react-native"
import * as metric from "../../../../theme"
import { color } from "../../../../theme"
import { RFValue } from "react-native-responsive-fontsize"

const FULL: ViewStyle = {
    flex: 1
}

export const CONTAINNER_STYLE: ViewStyle = {
    ...FULL,
    flexDirection: 'row',
    backgroundColor: color.brightDim,
    height: metric.ratioHeight(114),
    justifyContent: 'space-between',
    paddingHorizontal: metric.ratioWidth(35),
    alignItems: 'center'
}

export const DESCRIPTION_TEXT: TextStyle = {
    fontSize: RFValue(17),
    fontWeight: '400',

    //Android
    fontFamily: "Montserrat-Regular"
}



export const TRACK: ViewStyle = {
    height: metric.ratioHeight(18),
    width: metric.ratioWidth(44),
    borderRadius: (metric.ratioWidth(26) * 3) / 4,
    borderWidth: metric.ratioWidth(2) / 2,
    backgroundColor: color.background,
}

export const THUMB: ViewStyle = {
    position: "absolute",
    width: metric.ratioWidth(26),
    height: metric.ratioWidth(26),
    borderColor: color.blackDim,
    borderRadius: metric.ratioWidth(26) / 2,
    borderWidth: metric.ratioWidth(2) / 2,
    backgroundColor: color.background,
    shadowColor: color.blackDim,
    shadowOffset: { width: metric.ratioWidth(1), height: metric.ratioHeight(2) },
    shadowOpacity: 1,
    shadowRadius: 2,
    elevation: 2,
    top: metric.ratioHeight(18 / 2) -  metric.ratioWidth(13),
}

export const TRACK_OFF_STYLE: ViewStyle = {
    backgroundColor:  color.palette.white,
    borderColor: color.blackDim,
}

export const TRACK_ON_STYLE: ViewStyle = {
    backgroundColor: color.palette.white,
    borderColor: color.blackDim,
}

export const THUMB_OFF_STYLE: ViewStyle = {
    backgroundColor: color.palette.pink,
    borderColor: color.palette.white
}

export const THUMB_NO_STYLE: ViewStyle = {
    backgroundColor:color.palette.pink,
    borderColor: color.palette.white,
}

export const SWITCH_STYLE: ViewStyle = {
    width: metric.ratioWidth(44),
    borderColor: color.palette.white,
}

export const DESCRIPTION: ViewStyle = {
    marginTop: metric.ratioHeight(5),
    marginHorizontal: metric.ratioWidth(28),
}
