// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { SeekingGuyProps } from "./filter-seeking-guy.props"
import * as Styles from "./filter-seeking-guy.styles"
import { Text, Switch } from "../../../../components"
import { View } from "react-native"

export const SeekingGuyComponent: React.FunctionComponent<SeekingGuyProps> = observer((props) => {

    // MARK: Render

    return (
        <View style={{ ...Styles.CONTAINNER_STYLE, ...props?.containerStyle }} >
            <View style={Styles.SWITCH_STYLE}>
                <Switch value={props?.value}
                    baseThumbStyle={Styles.THUMB}
                    baseTrackStyle={Styles.TRACK}
                    thumbOffStyle={Styles.THUMB_OFF_STYLE}
                    thumbOnStyle={Styles.THUMB_NO_STYLE}
                    trackOffStyle={Styles.TRACK_OFF_STYLE}
                    trackOnStyle={Styles.TRACK_ON_STYLE} onToggle={(value) => { props?.onToggle(value) }} {...props?.seekingGuyTestID} />
            </View>
            {props?.description && <View style={Styles.DESCRIPTION}>
                <Text style={Styles.DESCRIPTION_TEXT} text={props?.description} {...props?.descriptionTestID} />
            </View>}
        </View>
    )
})