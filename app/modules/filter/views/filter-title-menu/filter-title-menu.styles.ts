import { ViewStyle, TextStyle } from "react-native";
import * as metric from "../../../../theme"

export const CONTAINNER_STYLE: ViewStyle = {
    height: metric.ratioHeight(40),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: metric.ratioWidth(35)
}

export const DEFAULT_TEXT: TextStyle = {
}