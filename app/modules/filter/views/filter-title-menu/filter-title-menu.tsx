// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { FilterTitleMenuProps } from "./filter-title-menu.props"
import * as Styles from "./filter-title-menu.styles"
import { Text } from "../../../../components"
import { View } from "react-native"

export const FilterTitleMenuComponent: React.FunctionComponent<FilterTitleMenuProps> = observer((props) => {

    // MARK: Render

    const renderRightView = () => props?.customRightView
    return (
        <View style={{ ...Styles.CONTAINNER_STYLE, ...props?.containerStyle }} >
            <Text text={props?.leftText} style={{ ...Styles.DEFAULT_TEXT, ...props?.leftTextStyle }} />
            {renderRightView()}
        </View>
    )
})