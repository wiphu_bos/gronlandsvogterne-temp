import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { TextStyle, ViewStyle } from 'react-native'
import { ReactChild } from 'react'

export interface FilterTitleMenuProps extends NavigationContainerProps<ParamListBase> {
    titleTestID?: any
    leftText?: string,
    leftTextStyle?: TextStyle,
    customRightView?: ReactChild,
    containerStyle?: ViewStyle
}