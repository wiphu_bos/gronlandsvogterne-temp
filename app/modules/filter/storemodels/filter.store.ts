import { types } from "mobx-state-tree"
import { FilterViews, FilterActions, FilterResourcesViews } from "../viewmodels"
import { FilterPropsModel } from "../viewmodels"
import { FilterServiceActions } from "../services/filter.services"
import { StoreName } from "../../../constants/app.constant"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store/general-resources.store"
import { FilterStoreModel as FilterBaseStoreModel } from "../../../models/filter-store/filter-store.store"

const FilterModel = types.model(StoreName.Filter, FilterPropsModel)

export const FilterStoreLocalStore = types.compose(
    FilterModel,
    FilterViews,
    FilterActions,
    FilterServiceActions)

export const FilterStoreModel = types.compose(FilterStoreLocalStore, FilterBaseStoreModel)
export const FilterResourcesStoreModel = types.compose(GeneralResourcesStoreModel, FilterResourcesViews)

