import { Instance, SnapshotOut } from "mobx-state-tree"
import { FilterStoreModel, FilterResourcesStoreModel } from "./filter.store"

export type FilterStore = Instance<typeof FilterStoreModel>
export type FilterStoreSnapshot = SnapshotOut<typeof FilterStoreModel>

export type FilterResourcesStore = Instance<typeof FilterResourcesStoreModel>
export type FilterResourcesStoreSnapshot = SnapshotOut<typeof FilterResourcesStoreModel>
