// MARK: Import

import { NavigationKey } from "../../constants/app.constant"
import { FilterProps } from "./filter.props"
import { FilterResourcesStoreModel, FilterStoreModel as localFilterStoreModel } from "./storemodels/filter.store"
import { FilterResourcesStore } from "./storemodels/filter.types"
import { RootStore, INavigationRoute } from "../../models"
import { onSnapshot } from "mobx-state-tree"
import { FilterStore } from "./filter.types"
import { ISelect } from "../../models/user-store"
import { SelectionType } from "../../models/validation-store/validation-store.model"
import * as DataUtils from "../../utils/data.utils"
import { FilterStoreModel as globalFilterStoreModel } from "../../models/filter-store/filter-store.store"
import { DrawerActions } from "@react-navigation/native"
import { BaseController } from "../base.controller"

class FilterController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static resourcesViewModel: FilterResourcesStore
    public static viewModel: FilterStore
    private static _currentFilterValue: object
    private static _tagListSelected: ISelect[]
    private static _isSelectedAll: boolean

    private static _regionContentList: ISelect[]
    private static _regionListSelected: any[]
    private static _regionSelectedFirstIndex: number

    private static _interestContentList: ISelect[]
    private static _interestListSelected: any[]
    private static _interestSelectedFirstIndex: number
    private static _selectionTypeSelected: SelectionType
    public static myProps: FilterProps & Partial<INavigationRoute>

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, props?: FilterProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, props, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(props)
        this._setupViewModel()
        this._setupSnapShot()
        this._setupContents()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        FilterController.resourcesViewModel = FilterResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        FilterController._currentFilterValue = { ...this._rootStore?.getFilterStore }
        FilterController.viewModel = FilterController._currentFilterValue && localFilterStoreModel.create({ ...FilterController._currentFilterValue })
    }

    //@override
    _setupProps = (props: FilterProps & Partial<INavigationRoute>) => {
        FilterController.myProps = {
            ...props as object,
            ...(props as INavigationRoute)?.route?.params
        }
    }

    private _setupSnapShot = () => {
        onSnapshot(FilterController.viewModel, (snap: FilterStore) => {
            this._setInitializedToPropsParams()
        })
    }

    /*
       Mark Data
   */

    private _setupContents = () => {
        this._getRegionContent()
        this._getInterestContent()
        this._syncDataTagContent()
    }

    private _multipleSelectionMenuOptions = {
        onOK: (items: any[]) => this.onSelectItems(items),
        onClose: () => this._onCloseModal()
    }

    public getSelectOptions = () => {
        const regionOptions = {
            flatListViewProps: { selectedIndex: FilterController._regionSelectedFirstIndex },
            isMultipleSelectionDropDown: true,
            selectedOptions: FilterController.viewModel?.getRegionList && DataUtils.getModalContentList(FilterController.viewModel?.getRegionList, true),
            placeholderText: FilterController.resourcesViewModel?.getResourceModalRegionPlaceholder(),
            options: FilterController._regionContentList,
            ...this._multipleSelectionMenuOptions
        }

        const interestOptions = {
            flatListViewProps: { selectedIndex: FilterController._interestSelectedFirstIndex },
            isMultipleSelectionDropDown: true,
            selectedOptions: FilterController.viewModel?.getInterestList && DataUtils.getModalContentList(FilterController.viewModel?.getInterestList, true),
            placeholderText: FilterController.resourcesViewModel?.getResourceModalInterestPlaceholder(),
            options: FilterController._interestContentList,
            ...this._multipleSelectionMenuOptions
        }
        switch (FilterController._selectionTypeSelected) {
            case SelectionType.REGION:
                return regionOptions
            case SelectionType.Interest:
                return interestOptions
            default:
                return null
        }
    }

    private _getRegionContent = () => {
        FilterController._regionContentList = this._rootStore?.getSharedStore?.getRegions || []
        FilterController._regionListSelected = FilterController.viewModel?.getRegionList && FilterController.viewModel?.getRegionList?.map((e) => e) || null
        const regionSelectedFirstObject = FilterController._regionListSelected?.length > 0 ? FilterController._regionListSelected[0] : 0
        FilterController._regionSelectedFirstIndex = FilterController._regionContentList?.findIndex((e) => {
            if (!regionSelectedFirstObject) return true
            return e.key === regionSelectedFirstObject?.key
        })
        FilterController._regionSelectedFirstIndex = FilterController._regionSelectedFirstIndex < 0 ? 0 : FilterController._regionSelectedFirstIndex
    }

    private _getInterestContent = () => {
        FilterController._interestContentList = this._rootStore?.getSharedStore?.getInterests || []
        FilterController._interestListSelected = FilterController.viewModel?.getInterestList && FilterController.viewModel?.getInterestList?.map((e) => e) || null
        const interestSelectedFirstObject = FilterController._interestListSelected?.length > 0 ? FilterController._interestListSelected[0] : 0
        FilterController._interestSelectedFirstIndex = FilterController._interestContentList?.findIndex((e) => {
            if (!interestSelectedFirstObject) return true
            return e.key === interestSelectedFirstObject?.key
        })
        FilterController._interestSelectedFirstIndex = FilterController._interestSelectedFirstIndex < 0 ? 0 : FilterController._interestSelectedFirstIndex
    }

    private _syncDataTagContent = () => {
        FilterController._isSelectedAll = FilterController.viewModel?.getTagList?.length === this._rootStore?.getSharedStore?.getTags?.length
        FilterController._tagListSelected = FilterController.viewModel?.getTagList
    }


    /*
       Mark Event
   */

    public onSelectItems = (item?: any[]): void => {
        const selectedItems = item?.map(e => {
            return {
                key: e.key,
                value: e.label
            }
        })
        if (FilterController._selectionTypeSelected == SelectionType.REGION) {
            const isSelectAll = selectedItems?.length === this._rootStore?.getSharedStore?.getRegions?.length || selectedItems?.length === 0
            if (isSelectAll) {
                this.onSelectAllRegions()
            } else {
                FilterController.viewModel?.setRegionList(selectedItems)
                this._rootStore?.getFilterStore?.setRegionList(selectedItems)
                FilterController._regionSelectedFirstIndex = FilterController._regionContentList?.findIndex((e) => {
                    if (!(selectedItems?.length > 0)) return true
                    return e.key === selectedItems[0].key
                })
                FilterController._regionSelectedFirstIndex = FilterController._regionSelectedFirstIndex < 0 ? 0 : FilterController._regionSelectedFirstIndex
            }
            FilterController._regionListSelected = item
            FilterController.viewModel?.setIsSelectedAllRegions(isSelectAll)
            this._rootStore?.getFilterStore?.setIsSelectedAllRegions(isSelectAll)

        } else if (FilterController._selectionTypeSelected == SelectionType.Interest) {
            const isSelectAll = selectedItems.length === this._rootStore?.getSharedStore?.getInterests.length || selectedItems.length === 0
            if (isSelectAll) {
                this.onSelectAllInterests()
            } else {
                FilterController.viewModel?.setInterestList(selectedItems)
                this._rootStore?.getFilterStore?.setInterestList(selectedItems)
                FilterController._interestSelectedFirstIndex = FilterController._interestContentList?.findIndex((e) => {
                    if (!(selectedItems?.length > 0)) return true
                    return e.key === selectedItems[0].key
                })
                FilterController._interestSelectedFirstIndex = FilterController._interestSelectedFirstIndex < 0 ? 0 : FilterController._interestSelectedFirstIndex
            }
            FilterController._interestListSelected = item
            FilterController.viewModel?.setIsSelectedAllInterests(isSelectAll)
            this._rootStore?.getFilterStore?.setIsSelectedAllInterests(isSelectAll)
        }
        this._onCloseModal()
    }

    private _onCloseModal = (): void => {
        FilterController.viewModel?.setIsModalShowing(false)
        FilterController._selectionTypeSelected = null
    }

    public onSelectRegion = () => {
        FilterController._selectionTypeSelected = SelectionType.REGION
        FilterController.viewModel?.setIsModalShowing(true)
    }

    public onSelectAllRegions = () => {
        const allRegions = this._rootStore?.GeneralResourcesStore?.getRegions
        FilterController.viewModel?.setIsSelectedAllRegions(true) //Need
        FilterController.viewModel?.setRegionList(allRegions)
        this._rootStore?.getFilterStore?.setRegionList(allRegions)
        this._rootStore?.getFilterStore?.setIsSelectedAllRegions(true) //Need
    }

    public onSelectInterest = () => {
        FilterController._selectionTypeSelected = SelectionType.Interest
        FilterController.viewModel?.setIsModalShowing(true)
    }

    public onSelectAllInterests = () => {
        const allInterests = this._rootStore?.GeneralResourcesStore?.getInterests
        FilterController.viewModel?.setIsSelectedAllInterests(true) //Need
        FilterController.viewModel?.setInterestList(allInterests)
        this._rootStore?.getFilterStore?.setInterestList(allInterests)
        this._rootStore?.getFilterStore?.setIsSelectedAllInterests(true) //Need
    }

    public onMinValueChanged = (value: number) => {
        FilterController.viewModel?.setMinAge(value)
        this._rootStore?.getFilterStore?.setMinAge(value)
    }

    public onMaxValueChanged = (value: number) => {
        FilterController.viewModel?.setMaxAge(value)
        this._rootStore?.getFilterStore?.setMaxAge(value)
    }

    public onTagPressed = (item: ISelect) => {
        const { key, value } = item
        const foundItemIndex = FilterController._tagListSelected?.findIndex(v => v.key === key)
        const transformList = FilterController._tagListSelected?.map(e => {
            return { key: e.key, value: e.value }
        })
        let selectedList = transformList || []
        if (selectedList?.length > 0 && foundItemIndex !== -1) {
            selectedList.splice(foundItemIndex, 1)
        } else {
            selectedList?.push({ key: key, value: value })
        }
        FilterController._tagListSelected = selectedList
        const isSelectedAllCheck = FilterController._tagListSelected?.length === this._rootStore?.getSharedStore?.getTags?.length
        if (!isSelectedAllCheck) {
            FilterController._isSelectedAll = isSelectedAllCheck
            FilterController.viewModel?.setTagList(selectedList)
            this._rootStore?.getFilterStore?.setTagList(selectedList)
            FilterController.viewModel?.setIsSelectedAllTags(FilterController._isSelectedAll)
            this._rootStore?.getFilterStore?.setIsSelectedAllTags(FilterController._isSelectedAll)
        } else {
            this.onSelectAllTagsPressed()
        }
    }

    public onSelectAllTagsPressed = () => {
        FilterController._isSelectedAll = !FilterController._isSelectedAll
        const tagList = FilterController._isSelectedAll ? FilterController.resourcesViewModel?.getTags : []
        FilterController.viewModel?.setTagList(tagList)
        FilterController.viewModel?.setIsSelectedAllTags(FilterController._isSelectedAll)
        this._rootStore?.getFilterStore?.setTagList(tagList)
        this._rootStore?.getFilterStore?.setIsSelectedAllTags(FilterController._isSelectedAll)
        FilterController._tagListSelected = tagList
    }

    public onSaveAndContinueButtonDidTouch = () => {
        this.onClose(true, true)
    }

    public backProcess = () => this._myProps?.navigation?.goBack()

    public onClose = (isFetchData: boolean = false, saveFilterValue: boolean = false) => {
        if (!saveFilterValue) this._rootStore?.setFilterStore(globalFilterStoreModel.create({ ...FilterController._currentFilterValue }))
        const drawerActions: any = DrawerActions.jumpTo(NavigationKey.SearchListScreen, {
            isShowFilter: false,
            isFetchData: isFetchData
        })
        Promise.all([this.backProcess(), this._myProps?.navigation?.dispatch(drawerActions)])
    }

    public onSeekingGuyToggled = (value: boolean) => {
        FilterController.viewModel?.setIsSeekingGuy(value)
        this._rootStore?.getFilterStore?.setIsSeekingGuy(value)
    }
    /*
       Mark Life cycle
   */

}

export default FilterController
