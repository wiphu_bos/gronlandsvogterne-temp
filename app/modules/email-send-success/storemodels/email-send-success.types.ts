import { Instance, SnapshotOut } from "mobx-state-tree"
import { EmailSendSuccessResourcesStoreModel,EmailSendSuccessStoreModel } from "./email-send-success.store"

export type EmailSendSuccessStore = Instance<typeof EmailSendSuccessStoreModel>
export type EmailSendSuccessStoreSnapshot = SnapshotOut<typeof EmailSendSuccessStoreModel>

export type EmailSendSuccessResourcesStore = Instance<typeof EmailSendSuccessResourcesStoreModel>
export type EmailSendSuccessResourcesStoreSnapshot = SnapshotOut<typeof EmailSendSuccessResourcesStoreModel>
