import { types } from "mobx-state-tree"
import { StoreName } from "../../../constants/app.constant"
import { EmailSendSuccessServiceActions } from "../services/email-send-success.services"
import { EmailSendSuccessPropsModel } from "../viewmodels/email-send-success.models"
import { EmailSendSuccessActions } from "../viewmodels/email-send-success.actions"
import { EmailSendSuccessViews, EmailSendSuccessResourcesViews } from "../viewmodels/email-send-success.views"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store"


const  EmailSendSuccessModel = types.model(StoreName.EmailSendSuccess,  EmailSendSuccessPropsModel)

export const EmailSendSuccessStoreModel = types.compose(
    EmailSendSuccessModel,
    EmailSendSuccessViews,
    EmailSendSuccessActions,
    EmailSendSuccessServiceActions)
export const  EmailSendSuccessResourcesStoreModel = types.compose(GeneralResourcesStoreModel, EmailSendSuccessResourcesViews)