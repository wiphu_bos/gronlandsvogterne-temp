import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { Source } from 'react-native-fast-image'

export interface EmailSendSuccessProps extends NavigationContainerProps<ParamListBase> {
    title?: string,
    subTitle?: string,
    description?: string,
    firstContent?: string,
    secondContent?: string,
    firstIcon?: Source,
    secondIcon?: Source,
    screenState: string
}