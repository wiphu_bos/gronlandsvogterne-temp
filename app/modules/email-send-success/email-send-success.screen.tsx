// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import EmailSendSuccessController from "./email-send-success.controllers"
import { View } from "react-native"
import * as metric from "../../theme"
import { SafeAreaView } from "react-navigation"
import { EmailSendSuccessProps } from "./email-send-success.props"
import { HeaderComponent } from "../../components/custom-header/custom-header"
import { Wallpaper, Text, AnimatedScrollView } from "../../components"
import { useConfigurate } from "../../custom-hooks/use-configure-controller"
import { images } from "../../theme/images"
import { CardTRC } from "../../components/card-trc/card-trc"
import { INavigationRoute } from "../../models"
import FastImage from "react-native-fast-image"

// MARK: Style Import

import * as Styles from "./email-send-success.styles"


export const EmailSendSuccesScreen: React.FunctionComponent<EmailSendSuccessProps & Partial<INavigationRoute>> = observer((props) => {

    const controller = useConfigurate(EmailSendSuccessController, props) as EmailSendSuccessController

    // MARK: Render
    return (
        <Wallpaper showBubble>
            <SafeAreaView shouldRasterizeIOS style={Styles.SAFE_AREA_VIEW}>
                <HeaderComponent onBackButtonPress={controller.backProcess} />
                <AnimatedScrollView
                    contentContainerStyle={Styles.SCROLL_VIEW}
                    {...metric.solidScrollView} >
                    <View shouldRasterizeIOS style={Styles.TEXT_CONTAINER}>
                        <FastImage source={images.waitingDudeTitle} style={Styles.IMAGE_TITLE} />
                        <Text style={Styles.TITLE}
                            {...EmailSendSuccessController.resourcesViewModel?.getTestIDTitle()} text={EmailSendSuccessController.myProps?.title} />
                        {EmailSendSuccessController.myProps?.subTitle && <View shouldRasterizeIOS style={Styles.SUBTITLE_VIEW}>
                            <Text style={Styles.SUB_TITLE}
                                {...EmailSendSuccessController.resourcesViewModel?.getTestIDSubTitle()} text={EmailSendSuccessController.myProps?.subTitle} />
                        </View>}
                    </View>
                    <CardTRC iconFirstLine={EmailSendSuccessController.myProps?.firstIcon}
                        iconSecondLine={EmailSendSuccessController.myProps?.secondIcon}
                        contentFirstLine={EmailSendSuccessController.myProps?.firstContent}
                        contentSecondLine={EmailSendSuccessController.myProps?.secondContent} />
                    <Text style={Styles.DESCRIPTION}
                        {...EmailSendSuccessController.resourcesViewModel?.getTestIDDescription()} text={EmailSendSuccessController.myProps?.description} />
                </AnimatedScrollView>
            </SafeAreaView>
        </Wallpaper>
    )
})
