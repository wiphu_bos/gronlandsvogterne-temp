import { ViewStyle, TextStyle, ImageStyle } from "react-native"
import * as metric from "../../theme"
import { TEXT } from "../../components/appname-slogan/appname-slogan.styles"
import { RFValue } from "react-native-responsive-fontsize"

export const FULL: ViewStyle = {
    flex: 1,
}

export const SAFE_AREA_VIEW: ViewStyle = {
    ...FULL,
}

export const SCROLL_VIEW: ViewStyle = {
    paddingBottom: metric.ratioHeight(10),
    alignItems: "center"
}

export const SUBTITLE_VIEW: ViewStyle = {
    marginVertical: metric.ratioHeight(39),
    marginHorizontal: metric.ratioWidth(56),
}

export const TEXT_CONTAINER: ViewStyle = {
    alignItems: "center",
    marginTop: metric.ratioHeight(80),
}

const FONT_REGULAR: TextStyle = {
    //Android
    fontFamily: "Montserrat-Regular",
}

const FONT_LIGHT: TextStyle = {
    //Android
    fontFamily: "Montserrat-Light",
}

export const TITLE: TextStyle = {
    ...TEXT,
    ...FONT_REGULAR,
    fontSize: RFValue(18),
    fontWeight: '400',
    marginTop: metric.ratioHeight(30)
}

export const SUB_TITLE: TextStyle = {
    ...TEXT,
    ...FONT_LIGHT,
    fontSize: RFValue(18),
    fontWeight: '300',
    lineHeight: metric.ratioHeight(30),
}

export const IMAGE_TITLE: ImageStyle = {
    width: metric.ratioWidth(70),
    height: metric.ratioWidth(70)
}

export const DESCRIPTION: TextStyle = {
    ...TEXT,
    ...FONT_LIGHT,
    lineHeight: metric.ratioHeight(30),
    width: metric.ratioWidth(290),
    fontSize: RFValue(17),
    fontWeight: '300',
    marginTop: metric.ratioHeight(39)
}
