// MARK: Import

import { NavigationKey } from "../../constants/app.constant"
import { RootStore, INavigationRoute } from "../../models"
import { onSnapshot } from "mobx-state-tree"
import { EmailSendSuccessProps } from "./email-send-success.props"
import { EmailSendSuccessStore, EmailSendSuccessResourcesStore } from "./storemodels/email-send-success.types"
import { EmailSendSuccessStoreModel, EmailSendSuccessResourcesStoreModel } from "./storemodels/email-send-success.store"
import { BaseController } from "../base.controller"

class EmailSendSuccessController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static viewModel: EmailSendSuccessStore
    public static resourcesViewModel: EmailSendSuccessResourcesStore
    public static myProps: EmailSendSuccessProps & Partial<INavigationRoute>

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, myProps?: EmailSendSuccessProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, myProps, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(myProps)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        EmailSendSuccessController.resourcesViewModel = EmailSendSuccessResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.EmailSendSuccess)
        EmailSendSuccessController.viewModel = localStore && EmailSendSuccessStoreModel.create({ ...localStore }) || EmailSendSuccessStoreModel.create({})
    }

    private _setupSnapShot = () => {
        onSnapshot(EmailSendSuccessController.viewModel, (snap: EmailSendSuccessStore) => {
            this._setInitializedToPropsParams()
        })
    }
    
    //@override
    _setupProps = (myProps: EmailSendSuccessProps & Partial<INavigationRoute>) => {
        super._setupProps && super._setupProps(myProps)
        EmailSendSuccessController.myProps = {
            ...myProps,
            title: myProps?.route?.params?.title,
            subTitle: myProps?.route?.params?.subTitle,
            description: myProps?.route?.params?.description,
            firstContent: myProps?.route?.params?.firstContent,
            secondContent: myProps?.route?.params?.secondContent,
            firstIcon: myProps?.route?.params?.firstIcon,
            secondIcon: myProps?.route?.params?.secondIcon,
            screenState: myProps?.route?.params?.screenState
        }
    }

    /*
       Mark Data
   */

    /*
       Mark Event
   */

    /*
       Mark Life cycle
   */

    /*
       Mark Helper
   */
}

export default EmailSendSuccessController