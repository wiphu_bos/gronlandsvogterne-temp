import { types } from "mobx-state-tree"
import { EmailSendSuccessPropsModel } from "./email-send-success.models"
import { GeneralResources } from "../../../constants/firebase/remote-config"
import * as Utils from "../../../utils"
import { TestIDResources } from "../../../constants/test-key/test.constant"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store"

export const EmailSendSuccessViews = types.model(EmailSendSuccessPropsModel)
    .views(self => ({
        get getEx() {
            return
        }
    }))


export const EmailSendSuccessResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State

    const { } = GeneralResources

    //MARK: Views

    return {

    }
})
    .views(self => {

        //MARK: Volatile State

        //MARK: Views

        const getTestIDTitle = () => Utils.getTestIDObject(TestIDResources.WaitingDudeApproveScreen.title)
        const getTestIDSubTitle = () => Utils.getTestIDObject(TestIDResources.WaitingDudeApproveScreen.subTitle)
        const getTestIDDescription = () => Utils.getTestIDObject(TestIDResources.WaitingDudeApproveScreen.description)

        return {
            getTestIDTitle,
            getTestIDSubTitle,
            getTestIDDescription
        }
    })
