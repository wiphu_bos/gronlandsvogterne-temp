import { Instance, SnapshotOut } from "mobx-state-tree"
import { CreateDudeResourcesStoreModel, CreateDudeStoreModel } from "./create-dude.store"

export type CreateDudeStore = Instance<typeof CreateDudeStoreModel>
export type CreateDudeStoreSnapshot = SnapshotOut<typeof CreateDudeStoreModel>

export type CreateDudeResourcesStore = Instance<typeof CreateDudeResourcesStoreModel>
export type CreateDudeResourcesStoreSnapshot = SnapshotOut<typeof CreateDudeResourcesStoreModel>
