import { types } from "mobx-state-tree"
import { StoreName } from "../../../constants/app.constant"
import { CreateDudeServiceActions } from "../services/create-dude.services"
import { CreateDudePropsModel } from "../viewmodels/create-dude.models"
import { CreateDudeActions } from "../viewmodels/create-dude.actions"
import { CreateDudeViews, CreateDudeResourcesViews } from "../viewmodels/create-dude.views"
import { ValidationStoreModel } from "../../../models/validation-store"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store"
import { UserStoreModel } from "../../../models/user-store"


const CreateDudeModel = types.model(StoreName.CreateDude, CreateDudePropsModel)

export const CreateDudeStoreModel = types.compose(
    CreateDudeModel,
    UserStoreModel,
    ValidationStoreModel,
    CreateDudeViews,
    CreateDudeActions,
    CreateDudeServiceActions)
export const CreateDudeResourcesStoreModel = types.compose(GeneralResourcesStoreModel, CreateDudeResourcesViews)