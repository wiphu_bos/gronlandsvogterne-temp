import { ViewStyle, TextStyle, ImageStyle } from "react-native"
import * as metric from "../../theme"
import { TEXT } from "../../components/appname-slogan/appname-slogan.styles"
import { RFValue } from "react-native-responsive-fontsize"

export const FULL: ViewStyle = {
    flex: 1,
}

export const SAFE_AREA_VIEW: ViewStyle = {
    ...FULL,
}

export const AVOID_KEYBOARD_VIEW: ViewStyle = {
    ...FULL,
}

export const SCROLL_VIEW: ViewStyle = {
    paddingBottom: metric.ratioHeight(10),
    alignItems: "center"
}

export const BUTTON_MARGIN: ViewStyle = {
    marginTop: metric.ratioHeight(31),
    paddingHorizontal: metric.ratioWidth(10),
    paddingVertical: metric.ratioHeight(10)
}

export const SUBTITLE_VIEW: ViewStyle = {
    marginTop: metric.ratioHeight(44),
    marginBottom: metric.ratioHeight(55),
    marginHorizontal: metric.ratioWidth(43),
}

export const TEXT_CONTAINER: ViewStyle = {
    alignItems: "center",
    marginTop: metric.ratioHeight(62)
}

export const FONT_REGULAR: TextStyle = {
    //Android
    fontFamily: "Montserrat-Regular",
}

export const FONT_LIGHT: TextStyle = {
    //Android
    fontFamily: "Montserrat-Light",
}

export const TITLE: TextStyle = {
    ...TEXT,
    ...FONT_REGULAR,
    fontSize: RFValue(25),
    fontWeight: '400',
    marginTop: metric.ratioHeight(25)
}

export const SUB_TITLE: TextStyle = {
    ...TEXT,
    ...FONT_LIGHT,
    fontSize: RFValue(18),
    fontWeight: '300',
    lineHeight: metric.ratioHeight(30),
}

export const IMAGE_TITLE: ImageStyle = {
    width: metric.ratioWidth(70),
    height: metric.ratioWidth(70),
}
