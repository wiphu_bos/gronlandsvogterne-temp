// MARK: Import

import React, { useRef } from "react"
import { observer } from "mobx-react-lite"
import CreateDudeController from "./create-dude.controllers"
import { View, KeyboardAvoidingView } from "react-native"
import FastImage from 'react-native-fast-image'
import * as metric from "../../theme"
import { GradientButton } from "../../components/gradient-button/button"
import FloatLabelTextInput from '../../libs/float-label-textfield/float-label-text-field'
import { SafeAreaView } from "react-navigation"
import { CreateDudeProps } from "./create-dude.props"
import { HeaderComponent } from "../../components/custom-header/custom-header"
import { Wallpaper, Text, AnimatedScrollView } from "../../components"
import { images } from "../../theme/images"
import { useConfigurate } from "../../custom-hooks/use-configure-controller"

// MARK: Style Import

import * as Styles from "./create-dude.styles"

export const CreateDudeScreen: React.FunctionComponent<CreateDudeProps> = observer((props) => {
    
    const controller = useConfigurate(CreateDudeController, props) as CreateDudeController

    const emailTextInput = useRef(null)

    // MARK: Render
    return (
        <Wallpaper showBubble>
            <SafeAreaView shouldRasterizeIOS style={Styles.SAFE_AREA_VIEW}>
                <HeaderComponent onBackButtonPress={controller.backProcess} />
                <KeyboardAvoidingView behavior={metric.keyboardBehavior} style={Styles.AVOID_KEYBOARD_VIEW}>
                    <AnimatedScrollView
                        contentContainerStyle={Styles.SCROLL_VIEW}
                        {...metric.solidScrollView}>
                        <View shouldRasterizeIOS style={Styles.TEXT_CONTAINER}>
                            <FastImage source={images.createDudeTitle} style={Styles.IMAGE_TITLE} />
                            <Text style={Styles.TITLE}
                                {...CreateDudeController.resourcesViewModel?.getTestIDTitle()} text={CreateDudeController.resourcesViewModel?.getResourceTitle()} />
                            <View shouldRasterizeIOS style={Styles.SUBTITLE_VIEW}>
                                <Text style={Styles.SUB_TITLE}
                                    {...CreateDudeController.resourcesViewModel?.getTestIDSubTitle()}
                                    text={CreateDudeController.resourcesViewModel?.getResourceSubTitle()} />
                            </View>
                        </View>

                        <FloatLabelTextInput
                            {...CreateDudeController.resourcesViewModel?.getResourceFullNamePlaceholder}
                            errorMessage={CreateDudeController.viewModel?.getFullNameErrorMessage}
                            testIDErrorMessage={CreateDudeController.resourcesViewModel?.getTestIDErrorMessage()}
                            placeholder={CreateDudeController.resourcesViewModel?.getResourceFullNamePlaceholder()}
                            onChangeTextValue={controller.onFullNameChangeText}
                            onBlur={controller.onBlurFullNameTextField}
                            returnKeyType="next"
                            onSubmitEditing={() => { emailTextInput.current.focus() }}
                            noBorder
                        />
                        <FloatLabelTextInput
                            ref={emailTextInput}
                            {...CreateDudeController.resourcesViewModel?.getTestIDEmailPlaceholder()}
                            errorMessage={CreateDudeController.viewModel?.getEmailErrorMessage}
                            testIDErrorMessage={CreateDudeController.resourcesViewModel?.getTestIDErrorMessage()}
                            placeholder={CreateDudeController.resourcesViewModel?.getResourceEmailPlaceholder()}
                            onChangeTextValue={controller.onEmailChangeText}
                            onBlur={controller.onBlurEmailTextField}
                            returnKeyType="send"
                            keyboardType="email-address"
                            onSubmitEditing={controller.onSubmitButtonDidTouch}
                            noBorder
                        />
                        <GradientButton
                            text={CreateDudeController.resourcesViewModel?.getResourceButtonContinueTitle()}
                            {...CreateDudeController.resourcesViewModel?.getResourceButtonContinueTitle()}
                            containerStyle={Styles.BUTTON_MARGIN} onPress={controller.onSubmitButtonDidTouch} />

                    </AnimatedScrollView>
                </KeyboardAvoidingView>
            </SafeAreaView>
        </Wallpaper>
    )
})
