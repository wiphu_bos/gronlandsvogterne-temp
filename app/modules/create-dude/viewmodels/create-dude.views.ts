import { types } from "mobx-state-tree"
import { CreateDudePropsModel } from "./create-dude.models"
import { GeneralResources } from "../../../constants/firebase/remote-config"
import { NavigationKey } from "../../../constants/app.constant"
import * as Utils from "../../../utils"
import { TestIDResources } from "../../../constants/test-key/test.constant"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store"

export const CreateDudeViews = types.model(CreateDudePropsModel)
    .views(self => ({
        get getExample() {
            return self
        }
    }))


export const CreateDudeResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State

    const { AcceptanceFromDudeScreen, WaitingDudeApproveScreen } = GeneralResources
    const { title,
        subTitle,
        dudeEmailPlaceholder,
        dudeFullNamePlaceholder,
        buttonContinueTitle,
        keyboardButtonNextTitle,
        keyboardButtonSubmitTitle,
    } = AcceptanceFromDudeScreen

    const { successTitle,
        successSubTitle,
        successDescription,
    } = WaitingDudeApproveScreen
    const { CreateDude, WaitingDudeApprove } = NavigationKey

    //MARK: Views
    const getResources = (key: string, childKeyOrShareKey: string | boolean = false) => self.getValues(childKeyOrShareKey ? key : CreateDude, childKeyOrShareKey ? true : key)
    const getResourcesWaitingDudeApprove = (key: string) => self.getValues(WaitingDudeApprove,key)
    const getResourceTitle = () => getResources(title)
    const getResourceSubTitle = () => getResources(subTitle)
    const getResourceFullNamePlaceholder = () => getResources(dudeFullNamePlaceholder)
    const getResourceEmailPlaceholder = () => getResources(dudeEmailPlaceholder)
    const getResourceButtonContinueTitle = () => getResources(buttonContinueTitle, true)
    const getResourceKeyboardButtonNextTitle = () => getResources(keyboardButtonNextTitle, true)
    const getResourceKeyboardButtonSubmitTitle = () => getResources(keyboardButtonSubmitTitle, true)
    const getResourceSendEmailSuccessTitle = () => getResourcesWaitingDudeApprove(successTitle)
    const getResourceSendEmailSuccessSubTitle = () => getResourcesWaitingDudeApprove(successSubTitle)
    const getResourceSendEmailSuccessDescription = () => getResourcesWaitingDudeApprove(successDescription)

    return {
        getResourceTitle,
        getResourceSubTitle,
        getResourceEmailPlaceholder,
        getResourceFullNamePlaceholder,
        getResourceButtonContinueTitle,
        getResourceKeyboardButtonNextTitle,
        getResourceKeyboardButtonSubmitTitle,
        getResourceSendEmailSuccessTitle,
        getResourceSendEmailSuccessSubTitle,
        getResourceSendEmailSuccessDescription
    }
})
    .views(self => {

        //MARK: Volatile State

        //MARK: Views

        const getTestIDTitle = () => Utils.getTestIDObject(TestIDResources.CreateDudeScreen.title)
        const getTestIDSubTitle = () => Utils.getTestIDObject(TestIDResources.CreateDudeScreen.subTitle)
        const getTestIDEmailPlaceholder = () => Utils.getTestIDObject(TestIDResources.CreateDudeScreen.emailPlaceholder)
        const getTestIDFullNamePlaceholder = () => Utils.getTestIDObject(TestIDResources.CreateDudeScreen.fullNamePlaceholder)
        const getTestIDButtonContinueTitle = () => Utils.getTestIDObject(TestIDResources.CreateDudeScreen.buttonContinue)
        const getTestIDErrorMessage = () => Utils.getTestIDObject(TestIDResources.CreateDudeScreen.errorMessage)
        return {
            getTestIDTitle,
            getTestIDSubTitle,
            getTestIDEmailPlaceholder,
            getTestIDFullNamePlaceholder,
            getTestIDButtonContinueTitle,
            getTestIDErrorMessage
        }
    })
