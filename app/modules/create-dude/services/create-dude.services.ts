import { types, flow } from "mobx-state-tree"
import { CreateDudePropsModel } from "../viewmodels/create-dude.models"
import DudeServices from "../../../services/api-domain/dude.services"
import { UserStoreModel } from "../../../models/user-store/user.store"

export const CreateDudeServiceActions = types.compose(types.model(CreateDudePropsModel), UserStoreModel).actions(self => {
    const createDude = flow(function* () {
        try {
            const params = {
                full_name: self.getFullName,
                email: self.getEmail
            }
            return yield DudeServices.createDude(params)
        } catch (e) {
            return (e)
        }
    })
    return { createDude }
})