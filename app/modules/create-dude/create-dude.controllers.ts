// MARK: Import
import { Keyboard } from "react-native"
import { NavigationKey } from "../../constants/app.constant"
import { RootStore, INavigationRoute } from "../../models"
import { onSnapshot } from "mobx-state-tree"
import { CreateDudeProps } from "./create-dude.props"
import { CreateDudeStore, CreateDudeResourcesStore } from "./storemodels/create-dude.types"
import { CreateDudeStoreModel, CreateDudeResourcesStoreModel } from "./storemodels/create-dude.store"
import * as Validate from "../../utils/local-validate"
import { APIResponse, APISuccessResponse } from "../../constants/service.types"
import { StackActions } from "@react-navigation/core"
import { BaseController } from "../base.controller"
import { ValidationServices } from "../../services/api-domain/validate.services"

class CreateDudeController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static viewModel: CreateDudeStore
    public static resourcesViewModel: CreateDudeResourcesStore

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, myProps?: CreateDudeProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, myProps, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(myProps)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        CreateDudeController.resourcesViewModel = CreateDudeResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.CreateDude)
        CreateDudeController.viewModel = localStore && CreateDudeStoreModel.create({ ...localStore }) || CreateDudeStoreModel.create({})
    }

    private _setupSnapShot = () => {
        onSnapshot(CreateDudeController.viewModel, (snap: CreateDudeStore) => {
            this._setInitializedToPropsParams()
        })
    }

    /*
       Mark Data
   */


    /*
       Mark Event
   */

    public onEmailChangeText = (value: string) => {
        const params = {
            value: value,
            localViewModel: CreateDudeController.viewModel as any
        }
        CreateDudeController.viewModel.setEmail(value)
        Validate.onEmailChangeText(params)
    }

    public onFullNameChangeText = (value: string) => {
        const params = {
            value: value,
            localViewModel: CreateDudeController.viewModel as any
        }
        CreateDudeController.viewModel.setFullName(value)
        Validate.onFullNameChangeText(params)
    }

    public validateData = () => {
        this.onBlurFullNameTextField()
        this.onBlurEmailTextField()
    }

    public onBlurEmailTextField = () => {
        const email = CreateDudeController.viewModel?.getEmail
        const params = {
            value: email,
            localViewModel: CreateDudeController.viewModel as any,
            rootStore: this._rootStore
        }
        Validate.onBlurEmailTextField(params)
    }

    public onBlurFullNameTextField = () => {
        const fullName = CreateDudeController.viewModel?.getFullName
        const params = {
            value: fullName,
            localViewModel: CreateDudeController.viewModel as any,
            rootStore: this._rootStore
        }
        Validate.onBlurFullNameTextField(params)
    }

    public onSubmitButtonDidTouch = async () => {
        Keyboard.dismiss()
        this._checkEmptyData()
        this.validateData()
        if (!CreateDudeController.viewModel?.getIsValid) return
        this._isGlobalLoading(true)
        const result: APIResponse = await CreateDudeController.viewModel?.createDude()
        const success = result as APISuccessResponse
        this._isGlobalLoading(false)
        if (success?.data) {
            const { navigation } = this._myProps
            const params = {
                title: CreateDudeController.resourcesViewModel.getResourceSendEmailSuccessTitle(),
                subTitle: CreateDudeController.resourcesViewModel.getResourceSendEmailSuccessSubTitle(),
                description: CreateDudeController.resourcesViewModel.getResourceSendEmailSuccessDescription(),
                firstContent: CreateDudeController.viewModel?.getEmail,
                secondContent: CreateDudeController.viewModel?.getFullName,
                screenState: NavigationKey.WaitingDudeApprove
            }

            navigation?.dispatch(StackActions.push(NavigationKey.EmailSendSuccess, params) as any)
        } else {
            const params = {
                rootStore: this._rootStore,
                localViewModel: CreateDudeController.viewModel as any,
                error: result
            }
            const alertMessage = new ValidationServices(params).generalAPIResponseValidation()
            if (alertMessage) this._generalAlertOS({ message: alertMessage })
        }
    }

    /*
       Mark Helper
   */


    private _checkEmptyData = () => {
        const { getEmail, getFullName } = CreateDudeController.viewModel
        CreateDudeController.viewModel?.setIsValid(getEmail ? true : false && getFullName ? true : false)
    }
}

export default CreateDudeController
