import { types } from "mobx-state-tree"
import { IUser } from "../../../models/user-store/user.types"

export const SearchListPropsModel = {
    start_at: types.optional(types.number, 1),
    page_size: types.optional(types.number, 20),
    userList: types.optional(types.frozen<Partial<IUser>[]>(), []),
    isPullRefreshing: types.optional(types.boolean, false),
    isLoadDone: types.optional(types.boolean, false),
    isFirstLoadDone: types.optional(types.boolean, false),
    isLoadMore: types.optional(types.boolean, false),
} 