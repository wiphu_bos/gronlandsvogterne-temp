import { types } from "mobx-state-tree"
import { SearchListPropsModel } from "./search-list.models"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store/general-resources.store"
import { TestIDResources } from "../../../constants/test-key/test.constant"
import * as Utils from "../../../utils"
import { GeneralResources } from "../../../constants/firebase/remote-config/remote-config.constant"
import { NavigationKey } from "../../../constants/app.constant"
import { IUser } from "../../../models/user-store"
export const SearchListViews = types.model(SearchListPropsModel)
    .views(self => ({
        get getSearchList(): Partial<IUser>[] {
            return self.userList
        },
        get getIsPullRefreshing(): boolean {
            return self.isPullRefreshing
        },
        get getIsLoadMore(): boolean {
            return self.isLoadMore
        },
        get getIsLoadDone(): boolean {
            return self.isLoadDone
        },
        get getIsFirstLoadDone(): boolean {
            return self.isFirstLoadDone
        },
        get getShouldLoadmore(): boolean {
            return !self.isLoadDone && !self.isLoadMore && self.userList?.length >= (self.page_size * self.start_at) && self.isFirstLoadDone
        }
    }))

export const SearchListResourcesViews = GeneralResourcesStoreModel
    .views(self => {

        //MARK: Volatile State

        const { SearchList } = GeneralResources
        const {
            searchDudeTitle,
            helpDudetteTitle,
            buttonFiltersMenuTitle,
            accessUnpublishedDudeProfile
        } = SearchList
        const { SearchListScreen } = NavigationKey

        //MARK: Views
        const getResources = (key: string, childKeyOrShareKey: string | boolean = false) => self.getValues(childKeyOrShareKey ? key : SearchListScreen, childKeyOrShareKey ? true : key)
        const getResourceSearchDudeTitle = () => getResources(searchDudeTitle)
        const getResourceHelpDudetteTitle = () => getResources(helpDudetteTitle)
        const getResourceButtonFilterMenuTitle = () => getResources(buttonFiltersMenuTitle, true)
        const getAccessUnpublishedDudeProfile = () => getResources(accessUnpublishedDudeProfile, true)
        return {
            getResourceSearchDudeTitle,
            getResourceHelpDudetteTitle,
            getResourceButtonFilterMenuTitle,
            getAccessUnpublishedDudeProfile
        }
    })

    .views(self => {

        //MARK: Volatile State

        //MARK: Views

        const getTestIDTitle = () => Utils.getTestIDObject(TestIDResources.SearchList.title)
        const getTestIDSearchingDudeList = () => Utils.getTestIDObject(TestIDResources.SearchList.searchingDudeList)
        const getTestIDIcon = () => Utils.getTestIDObject(TestIDResources.SearchList.icon)
        return {
            getTestIDTitle,
            getTestIDSearchingDudeList,
            getTestIDIcon
        }
    })

