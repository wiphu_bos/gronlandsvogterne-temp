import { types } from "mobx-state-tree"
import { SearchListPropsModel } from "./search-list.models"
export const SearchListActions = types.model(SearchListPropsModel).actions(self => {
    const setIsPullRefreshing = (value: boolean) => self.isPullRefreshing = value
    const setIsLoadMore = (value: boolean) => self.isLoadMore = value
    const setToNextPage = () => {
        const currentPage = self.start_at
        self.start_at = currentPage + 1
    }
    const resetFetching = () => {
        self.userList = []
        self.start_at = 1
        self.isLoadDone = false
        self.isLoadMore = false
        self.isFirstLoadDone = false
    }
    const removeUserInListById = (uid: String) => {
        const tempList = self.userList
        const existingList = tempList?.slice() //Slice to disobserve property
        const foundItemIndex = existingList?.findIndex(v => v.uid === uid)
        if (foundItemIndex === -1) return
        existingList.splice(foundItemIndex, 1)
        self.userList = existingList
    }
    return {
        setIsPullRefreshing,
        setToNextPage,
        resetFetching,
        setIsLoadMore,
        removeUserInListById
    }
})