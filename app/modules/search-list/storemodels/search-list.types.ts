import { Instance, SnapshotOut } from "mobx-state-tree"
import { SearchListStoreModel, SearchListResourcesStoreModel } from "./search-list.store"

export type SearchListStore = Instance<typeof SearchListStoreModel>
export type SearchListStoreSnapshot = SnapshotOut<typeof SearchListStoreModel>

export type SearchListResourcesStore = Instance<typeof SearchListResourcesStoreModel>
export type SearchListResourcesStoreSnapshot = SnapshotOut<typeof SearchListResourcesStoreModel>
