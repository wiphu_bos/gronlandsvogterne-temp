import { types } from "mobx-state-tree"
import { SearchListViews, SearchListActions, SearchListResourcesViews } from "../viewmodels/"
import { SearchListPropsModel } from "../viewmodels"
import { SearchListServiceActions } from "../services/search-list.services"
import { StoreName } from "../../../constants/app.constant"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store/general-resources.store"

const SearchListModel = types.model(StoreName.SearchList, SearchListPropsModel)

export const SearchListStoreModel = types.compose(
    SearchListModel,
    SearchListViews,
    SearchListActions,
    SearchListServiceActions)

export const SearchListResourcesStoreModel = types.compose(GeneralResourcesStoreModel, SearchListResourcesViews)

