import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { SearchListType } from '../../constants/app.constant'

export interface SearchListProps extends NavigationContainerProps<ParamListBase> {
    type: SearchListType,
    isShowFilter: boolean,
    isFetchData: boolean
}