import { ViewStyle, ImageStyle, TextStyle } from "react-native"
import { color } from "../../theme"
import * as metric from "../../theme"
import { images } from "../../theme/images"
import { HeaderProps } from "../../components/header/header.props"
import { RFValue } from "react-native-responsive-fontsize"
import { FilterMenuProps } from "./views/filter-menu"

export const TEXT: TextStyle = {
    color: color.palette.white,
    fontFamily: "Montserrat",
}

export const TITLE: TextStyle = {
    ...TEXT,
    lineHeight: metric.ratioHeight(24),
    fontSize: RFValue(19),
    fontWeight: '600',

    //Android
    fontFamily: "Montserrat-SemiBold",
    marginHorizontal: metric.ratioWidth(15.67)
}

export const FULL: ViewStyle = {
    flex: 1
}

export const SAFE_AREA_VIEW: ViewStyle = {
    ...FULL
}

export const CONTAINER_LIST_VIEW: ViewStyle = {
    ...FULL,
    marginTop: metric.ratioHeight(12)
}
export const TITLE_CONTAINNER: ViewStyle = {
    paddingLeft: metric.ratioWidth(45),
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: metric.ratioHeight(28)
}
export const LIST_VIEW: ViewStyle = {
    paddingHorizontal: metric.ratioWidth(24),
    paddingBottom: metric.ratioHeight(33),
}

export const SEPARATOR_VIEW: ViewStyle = {
    height: metric.ratioHeight(12)
}

export const BACKGROUND_STYLE = color.pinkBlueGradient


export const MENU_ICON_STYLE: ImageStyle = {
    width: metric.ratioWidth(24),
    height: metric.ratioHeight(24)
}

export const HEADER_PROPS: HeaderProps = {
    leftIconSource: images.hamMenu,
    leftIconStyle: MENU_ICON_STYLE,
    isLeftIconAnimated: false
}

export const FILTER_MENU_PROPS: FilterMenuProps = {
    iconSource: images.filter,
    iconStyle: {
        width: metric.ratioWidth(17),
        height: metric.ratioHeight(17),
        marginLeft: metric.ratioWidth(5)
    }
}

export const REFRESH_CONTROL: any = { tintColor: color.brightDim }

export const ICON_PORTRAIT: ImageStyle = {
    width: metric.ratioWidth(36.34),
    height: metric.ratioHeight(40.46)
}