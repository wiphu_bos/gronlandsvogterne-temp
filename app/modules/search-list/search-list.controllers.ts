// MARK: Import

import { NavigationKey, SearchListType, DudeStatus } from "../../constants/app.constant"
import { SearchListProps } from "./search-list.props"
import { SearchListResourcesStoreModel, SearchListStoreModel } from "./storemodels/search-list.store"
import { SearchListResourcesStore } from "./storemodels/search-list.types"
import { RootStore, INavigationRoute } from "../../models"
import { onSnapshot } from "mobx-state-tree"
import { SearchListStore } from "./search-list.types"
import { StackActions, DrawerActions, CommonActions } from "@react-navigation/core"
import * as DataUtils from "../../utils/data.utils"
import { BaseController } from "../base.controller"
import { ValidationServices } from "../../services/api-domain/validate.services"

class SearchListController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static resourcesViewModel: SearchListResourcesStore
    public static viewModel: SearchListStore
    public static myProps: SearchListProps & Partial<INavigationRoute>
    private static _shouldCheckSubscription: boolean

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, props?: SearchListProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, props, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(props)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        SearchListController.resourcesViewModel = SearchListResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.SearchListScreen)
        SearchListController.viewModel = localStore && SearchListStoreModel.create({ ...localStore }) || SearchListStoreModel.create({})
    }

    //@override
    _setupProps = (props: SearchListProps & Partial<INavigationRoute>) => {
        SearchListController.myProps = {
            ...props as object,
            ...(props as INavigationRoute)?.route?.params
        }
    }

    private _setupSnapShot = () => {
        onSnapshot(SearchListController.viewModel, (snap: SearchListStore) => {
            this._setInitializedToPropsParams()
        })
    }

    /*
       Mark Data
   */

    private _fetchSearchList = async () => {
        const type = SearchListController.myProps?.type
        const result = await SearchListController.viewModel?.fetchSearchList(this._rootStore, type)
        if (!result?.data) {
            if (SearchListController._shouldCheckSubscription && type === SearchListType.Dude) {
                this._delayExecutor(() => this._myProps?.navigation?.dispatch(CommonActions.navigate(NavigationKey.SubscriptionConfirmation,
                    {
                        onSuccess: () => this.onRefresh,
                        navigationPropsAfterCancel: DrawerActions.jumpTo(NavigationKey.SearchListScreen, {
                            isShowFilter: false,
                            isFetchData: false
                        })
                    }) as any))
            }
            const params = {
                rootStore: this._rootStore,
                error: result
            }
            const [isError, message] = new ValidationServices(params).premiumAccount()
            SearchListController._shouldCheckSubscription = isError
        }
    }

    public onLoadMore = async () => {
        if (!SearchListController.viewModel?.getShouldLoadmore) return
        SearchListController.viewModel?.setIsLoadMore(true)
        SearchListController.viewModel?.setToNextPage()
        await this._fetchSearchList()
    }

    /*
       Mark Event
   */

    private _isLoading = (value: boolean) => SearchListController.viewModel?.setIsPullRefreshing(value)

    public onRefresh = async () => {
        this._isLoading(true)
        SearchListController.viewModel?.resetFetching()
        await this._fetchSearchList()
        this._isLoading(false)
    }

    public onSideMenuPress = () => {
        SearchListController.myProps?.navigation?.dispatch(DrawerActions.openDrawer() as any)
    }

    public onFilterPress = () => {
        SearchListController.myProps?.navigation?.dispatch(StackActions.push(NavigationKey.Filters, { searchType: SearchListController.myProps.type }) as any)
    }

    public onListItemPress = async (uid: string) => {
        const { type } = SearchListController.myProps
        this._isGlobalLoading(true)
        const userProfile = await SearchListController.viewModel?.fetchProfileDetail(this._rootStore, uid, type)
        this._isGlobalLoading(false)
        if (!userProfile) return
        if (userProfile?.status === DudeStatus.Unpublished) {
            const params = {
                rootStore: this._rootStore,
                error: { errors: [{ msg: SearchListController.resourcesViewModel?.getAccessUnpublishedDudeProfile() }] },
                //goBackHandler: () => backProcess()
            }
            const alertMessage = new ValidationServices(params).generalAPIResponseValidation()
            if (alertMessage) this._generalAlertOS({ message: alertMessage })
            SearchListController.viewModel?.removeUserInListById(userProfile?.uid)

        } else {
            const params = {
                [type === SearchListType.Dude ? "dudeProfileObject" : "dudetteProfileObject"]: DataUtils.getUserProfileObject(this._rootStore, null, userProfile)
            }
            let navigationKey: string = type === SearchListType.Dude ? NavigationKey.DudeProfileDetailTab : NavigationKey.DudetteProfileDetails
            SearchListController.myProps?.navigation?.dispatch(StackActions.push(navigationKey, params) as any)
        }
    }

    //@override
    backProcess = () => {

    }


    /*
       Mark Life cycle
   */

    public viewDidAppearAfterFocus = async (newProps?: SearchListProps & Partial<INavigationRoute>) => {
        this._setupProps(newProps) //setup local props
        if (SearchListController.myProps.type === SearchListType.Dudette) SearchListController._shouldCheckSubscription = undefined
        if (SearchListController.myProps?.isShowFilter) this._delayExecutor(() => this.onFilterPress(), 400)
        if ((!SearchListController._shouldCheckSubscription && SearchListController.viewModel?.getSearchList?.length === 0) ||
            SearchListController.myProps?.isFetchData) {
            await this.onRefresh()
        }

        // this.registerBackButtonHandler()
        super.viewDidAppearAfterFocus && super.viewDidAppearAfterFocus() //setup base props
    }
}

export default SearchListController
