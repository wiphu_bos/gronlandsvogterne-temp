import { Instance, SnapshotOut } from "mobx-state-tree"
import { SearchListStoreModel } from "./storemodels"

export type SearchListStore = Instance<typeof SearchListStoreModel>
export type SearchListStoreSnapshot = SnapshotOut<typeof SearchListStoreModel>