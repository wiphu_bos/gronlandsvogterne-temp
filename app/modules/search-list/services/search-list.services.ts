import { types, flow } from "mobx-state-tree"
import { SearchListPropsModel } from "../viewmodels"
import { APIResponse, APISuccessResponse } from "../../../constants/service.types"
import { RootStore } from "../../../models/root-store"
import { SearchListType } from "../../../constants/app.constant"
import DudeServices from "../../../services/api-domain/dude.services"
import UserServices from "../../../services/api-domain/user.services"
import * as metric from "../../../theme"
import * as DataUtils from "../../../utils/data.utils"
import { IUser } from "../../../models/user-store"

export const SearchListServiceActions = types.model(SearchListPropsModel).actions(self => {

    //MARK: Volatile State

    //MARK: Service Actions

    const fetchSearchList = flow(function* (rootStore?: RootStore, type?: SearchListType) {
        const filterStore = rootStore?.getFilterStore

        const params = {
            starting_age: filterStore?.getMinAge || metric.minAge,
            ending_age: filterStore?.getMaxAge || metric.starterMaxAge,
            city_list: DataUtils.getAllCitiesByRegions(rootStore, filterStore?.getRegionList),
            interest_list: filterStore?.getInterestList?.map(e => e.key)?.join(','),
            tag_list: filterStore?.getTagList?.map(e => e.key)?.join(','),
            start_at: self.start_at,
            page_size: self.page_size,
            is_seeking_guy: filterStore?.getIsSeekingGuy
        }
        try {
            const result: APIResponse = type == SearchListType.Dude ? yield DudeServices.searchDudeList(params) : yield UserServices.searchUserList(params)
            let success = (result as APISuccessResponse)?.data
            let list: Partial<IUser[]> = success?.data
            if (!list) throw result
            if (list?.length > 0) {
                const existingList = self.userList
                self.userList = self.isLoadMore ? existingList?.concat(list) : list
            }
            self.isLoadDone = list?.length < self.page_size
            return result
        } catch (e) {
            return e
        } finally {
            self.isPullRefreshing = false
            self.isLoadMore = false
            self.isFirstLoadDone = true

        }
    })

    const fetchProfileDetail = flow(function* (rootStore?: RootStore, uid?: string, type?: SearchListType) {
        try {
            let result: APISuccessResponse
            if (type === SearchListType.Dude) {
                result = yield rootStore?.getUserStore?.fetchDudeProfileDetail(uid)
            } else if (type === SearchListType.Dudette) {
                result = yield rootStore?.getUserStore?.fetchDudetteProfileDetail(uid)
            }
            const data: APISuccessResponse = result?.data
            const profileDetail: IUser = data?.data
            if (!profileDetail) throw data
            return profileDetail
        } catch (e) {
            return e
        }
    })

    return { fetchSearchList, fetchProfileDetail }
})