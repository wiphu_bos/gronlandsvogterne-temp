import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { IUser } from '../../../../models/user-store'

export interface ListItemProps extends NavigationContainerProps<ParamListBase> {
    onPress: (uid: string) => void,
    item: Partial<IUser>
}