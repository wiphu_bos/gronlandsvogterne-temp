import { ViewStyle, TextStyle, ImageStyle } from "react-native";
import * as metric from "../../../../theme"
import { color } from "../../../../theme"
import { RFValue } from "react-native-responsive-fontsize";

export const CONTAINNER_STYLE: ViewStyle = {
    width: metric.ratioWidth(376),
    height: metric.ratioHeight(80),
    backgroundColor: color.hardWhiteDim,
    borderTopRightRadius: metric.ratioWidth(19),
    borderBottomRightRadius: metric.ratioWidth(19),
    flexDirection: 'row'
}

export const EDGE_CONTAINER: ViewStyle = {
    width: metric.ratioWidth(4)
}

export const EDGE_COLORS: any = color.edgeRainBow
export const HIGHLIGHT_COLORS: any = color.highlightDude

const TEXT: TextStyle = {
    fontFamily: "Montserrat",
}

export const TITLE: TextStyle = {
    ...TEXT,
    lineHeight: metric.ratioHeight(22),
    fontSize: RFValue(17),
    fontWeight: '400',

    //Android
    fontFamily: "Montserrat-Regular",
    color: color.palette.darkGrey
}

export const DESCRIPTION: TextStyle = {
    ...TITLE,
    fontSize: RFValue(12),
    color: color.palette.superLightGrey,
    marginHorizontal: metric.ratioWidth(10.5)
}

export const PINK_PIN: ImageStyle = {
    width: metric.ratioWidth(10.5),
    height: metric.ratioHeight(14)
}

export const SUB_CONTENT_CONTAINER: ViewStyle = {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: metric.ratioHeight(2)
}

export const CONTENT_CONTAINER: ViewStyle = {
    marginHorizontal: metric.ratioWidth(30),
    marginTop: metric.ratioHeight(19)
}

export const HIGHLIGHT_CONTENT_CONTAINER: ViewStyle = {
    flex: 1,
    borderTopRightRadius: metric.ratioWidth(19),
    borderBottomRightRadius: metric.ratioWidth(19),
}

export const ICON_MARK_HIGHLIGHT: ImageStyle = {
    width: metric.ratioWidth(19.5),
    height: metric.ratioHeight(26)
}

export const MARK_HIGHLIGHT_CONTAINER: ViewStyle = {
    position: 'absolute',
    right: metric.ratioWidth(15.5),
    top: 0,
}