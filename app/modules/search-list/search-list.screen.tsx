// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { Wallpaper, Header, Text, Icon } from "../../components"
import { View, RefreshControl } from "react-native"
import { SearchListProps } from "./search-list.props"
import SearchListController from "./search-list.controllers"
import { SafeAreaView } from "react-navigation"
import { FlatList } from "react-native-gesture-handler"
import { ListItemComponent } from "./views/list-item"
import { FilterMenuComponent } from "./views/filter-menu"
import { images } from "../../theme/images"
import { INavigationRoute } from "../../models/navigation-store/navigation.types"
import { SearchListType } from "../../constants/app.constant"
import { LoadMoreIndicator } from "../../components/loadmore-indicator"
import { LoadingListComponent } from "../../components/loading-list/loading-list"
import * as metric from "../../theme"
// MARK: Style Import

import * as Styles from "./search-list.styles"
import { useConfigurate } from "../../custom-hooks/use-configure-controller"

export const SearchListScreen: React.FunctionComponent<SearchListProps & Partial<INavigationRoute>> = observer((props) => {

    const controller = useConfigurate(SearchListController, props) as SearchListController
    
    // MARK: Render

    return (
        <Wallpaper >
            <SafeAreaView
                {...metric.safeAreaViewProps}
                shouldRasterizeIOS
                style={Styles.SAFE_AREA_VIEW}>
                <Header {...Styles.HEADER_PROPS}
                    onLeftPress={controller.onSideMenuPress}
                    onRightPress={controller.onFilterPress}
                    customRightView={
                        <FilterMenuComponent {...Styles.FILTER_MENU_PROPS}
                            onPress={controller.onFilterPress}
                            title={SearchListController.resourcesViewModel?.getResourceButtonFilterMenuTitle()}
                        />
                    }
                />
                <View shouldRasterizeIOS style={Styles.CONTAINER_LIST_VIEW}>
                    <View shouldRasterizeIOS style={Styles.TITLE_CONTAINNER}>
                        <Icon source={SearchListController.myProps?.type === SearchListType.Dude ? images.dudeWhite :
                            images.dudetteWhite} style={Styles.ICON_PORTRAIT} />
                        <Text text={SearchListController.myProps?.type === SearchListType.Dude ? SearchListController.resourcesViewModel?.getResourceSearchDudeTitle() :
                            SearchListController.resourcesViewModel?.getResourceHelpDudetteTitle()}
                            style={Styles.TITLE}
                            {...SearchListController.resourcesViewModel?.getTestIDTitle()} />
                    </View>
                    {SearchListController.viewModel?.isFirstLoadDone ?
                        <FlatList
                            {...SearchListController.resourcesViewModel?.getTestIDSearchingDudeList()}
                            refreshControl={
                                <RefreshControl
                                    style={Styles.REFRESH_CONTROL}
                                    refreshing={SearchListController.viewModel?.getIsPullRefreshing}
                                    onRefresh={controller.onRefresh}
                                />
                            }
                            contentContainerStyle={Styles.LIST_VIEW}
                            ItemSeparatorComponent={({ highlighted }) => (
                                <View shouldRasterizeIOS style={Styles.SEPARATOR_VIEW} />
                            )}
                            data={SearchListController.viewModel?.getSearchList}
                            renderItem={({ item, index, separators }: any) => (<ListItemComponent onPress={() => controller.onListItemPress(item.uid)} item={item} />)
                            }
                            ListFooterComponent={SearchListController.viewModel?.getIsLoadMore && <LoadMoreIndicator />}
                            onEndReachedThreshold={0.4}
                            onEndReached={controller.onLoadMore}
                        />
                        : <LoadingListComponent />}
                </View>
            </SafeAreaView>
        </Wallpaper>
    )
})
