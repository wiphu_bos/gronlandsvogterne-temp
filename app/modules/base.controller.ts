import { RootStore } from "../models/root-store/root.types";
import { BaseNavigationProps } from "../custom-hooks/use-configure-controller";
import { NativeEventSubscription, BackHandler, Keyboard, Alert } from "react-native";
import { ListenerKey } from "../constants/app.constant";
import { INavigationRoute } from "../models/navigation-store/navigation.types";
import { GeneralResources } from "../constants/firebase/remote-config/remote-config.constant";

export type IAlertParams = {
    title?: string,
    message?: string,
    onOKPress?: () => void,
    onCancelPress?: () => void,
    delay?: number
}

export type BaseProps =
    BaseNavigationProps &
    Partial<INavigationRoute>

export class BaseController {

    /*
       Mark Inherited Variable & Declaration
   */

    protected _viewModel: any
    protected _isNestedNavigation: boolean
    public static rootStore: RootStore //for Sharing
    public static myProps: BaseProps //for Sharing
    protected _rootStore: RootStore
    protected _myProps: BaseProps
    private _backHandlerDiposer: NativeEventSubscription

    /*
       Mark Setup
   */

    constructor(rootStore?: RootStore, myProps?: BaseProps, isNestedNavigation?: boolean) {
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupRootStore(rootStore)
        this._isNestedNavigation = isNestedNavigation
        this._setupProps(myProps)
    }

    private _setupRootStore = (rootStore?: RootStore) => {
        BaseController.rootStore = rootStore
        this._rootStore = rootStore
    }

    protected _setupProps = (myProps: BaseProps) => {
        const comingProps = this._isNestedNavigation ? myProps?.route?.params?.route?.params : myProps?.route?.params
        this._myProps = {
            ...myProps as any,
            ...comingProps
        }
        BaseController.myProps = this._myProps
    }

    /*
       Mark Data
   */

    protected _setupNavigation = () => {
        this._rootStore?.getNavigationStore?.setNavigation(this._myProps?.navigation)
    }

    protected _setInitializedToPropsParams = () => {
        if (!this._myProps.route) return
        this._myProps.route.params = {
            ...this._myProps.route.params,
            isInitialized: true
        }
    }

    /*
       Mark Event
   */

    protected _isGlobalLoading = (value: boolean) => this._rootStore?.SharedStore?.setIsLoading(value)

    private registerBackButtonHandler = () => {
        if (!this._myProps?.route?.params?.isInitialized)
            this._backHandlerDiposer = BackHandler.addEventListener(ListenerKey.HardwareBackPress, this.backProcess)
    }
    private _unRegisterBackButtonHandler = () => this._backHandlerDiposer?.remove()

    public backProcess = () => {
        const { navigation } = this._myProps
        Keyboard.dismiss()
        navigation?.goBack()
    }

    /*
       Mark Life cycle
   */

    public viewWillAppearOnce = () => {
    }

    public viewDidAppearOnce = async () => {
    }

    public viewDidAppearAfterFocus = async (newProps?: BaseProps) => {
        this._setupProps(newProps)
        this.registerBackButtonHandler()
    }

    public viewWillDisappear = () => {
        this._unRegisterBackButtonHandler()
    }

    public deInit = () => {
        this._unRegisterBackButtonHandler()
    }


    /*
       Mark Utils
    */

    protected static _delayExecutor = (task: any, delay: number = 1000) => {
        setTimeout(() => { task && task() }, delay)
    }

    protected _delayExecutor = (task: any, delay: number = 1000) => {
        setTimeout(() => { task && task() }, delay)
    }

    protected _generalAlertOS = (params: IAlertParams) => {
        this._delayExecutor(() => Alert.alert(params.title, params.message), params?.delay || 1000)
    }

    protected _confirmationAlertOS = (params: IAlertParams) => {
        const buttonOKTitle = this._rootStore.getGeneralResourcesStore(GeneralResources.SharedKeys.buttonOKTitle, true)
        const buttonCancelTitle = this._rootStore.getGeneralResourcesStore(GeneralResources.SharedKeys.buttonCancelTitle, true)
        this._delayExecutor(() => Alert.alert(params?.title, params?.message, [
            { text: buttonOKTitle, onPress: params?.onOKPress },
            { text: buttonCancelTitle, onPress: params?.onCancelPress }
        ]), params?.delay || 1000)
    }
}