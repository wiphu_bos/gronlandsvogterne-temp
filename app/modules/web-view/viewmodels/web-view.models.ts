import { types } from "mobx-state-tree"

export const WebViewPropsModel = {
    webViewHeight: types.optional(types.number, 0)
} 