import { types } from "mobx-state-tree"
import { WebViewPropsModel } from "./web-view.models"
export const WebViewActions = types.model(WebViewPropsModel).actions(self => {
    //MARK: Volatile State
   
    //MARK: Actions
    const setWebViewHeight = (value: number) => self.webViewHeight = value

    return {
        setWebViewHeight
    }
})