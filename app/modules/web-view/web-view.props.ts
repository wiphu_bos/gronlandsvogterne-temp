import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"

export interface WebViewProps extends NavigationContainerProps<ParamListBase> {
    title: string,
    uri: string
}