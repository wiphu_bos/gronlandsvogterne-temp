// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import WebViewController from "./web-view.controllers"
import { View, Text, KeyboardAvoidingView } from "react-native"
import * as metric from "../../theme"
import { SafeAreaView } from "react-navigation"
import { WebViewProps } from "./web-view.props"
import { Wallpaper, AnimatedScrollView, Header } from "../../components"
import { useConfigurate } from "../../custom-hooks/use-configure-controller"
import WebView from 'react-native-webview'


// MARK: Style Import

import * as Styles from "./web-view.styles"


export const WebViewScreen: React.FunctionComponent<WebViewProps> = observer((props) => {

    const controller = useConfigurate(WebViewController, props) as WebViewController

    // MARK: Render
    return (
        <Wallpaper showBubble>
            <SafeAreaView shouldRasterizeIOS style={Styles.SAFE_AREA_VIEW}>
                <Header {...Styles.HEADER_PROPS} {...controller.getHeaderLeftOptions} />
                <KeyboardAvoidingView behavior={metric.keyboardBehavior} style={Styles.AVOID_KEYBOARD_VIEW}>
                    <View
                        style={Styles.CONTAINER}>
                        <View shouldRasterizeIOS style={Styles.TEXT_CONTAINER}>
                            <Text style={Styles.TITLE}
                                {...controller.resourcesViewModel?.getTestIDTitle()}>{controller.myProps?.title}</Text>
                        </View>

                        <WebView style={Styles.WEB_VIEW} source={{ uri: controller.myProps?.uri }}
                            onLoadStart={() => (controller.onLoadStart())}
                            onLoad={() => controller.onLoad()}
                            scalesPageToFit={true}
                            containerStyle={{ ...Styles.WEB_VIEW_CONTAINER }}
                        />
                    </View>
                </KeyboardAvoidingView>
            </SafeAreaView>
        </Wallpaper>

    )
})
