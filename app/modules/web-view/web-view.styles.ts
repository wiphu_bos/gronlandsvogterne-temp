import { ViewStyle, TextStyle, ImageStyle } from "react-native"
import * as metric from "../../theme"
import { TEXT } from "../../components/appname-slogan/appname-slogan.styles"
import { RFValue } from "react-native-responsive-fontsize"
import { color } from "../../theme"
import { HeaderProps } from "../../components/header/header.props"
import { images } from "../../theme/images"

export const FULL: ViewStyle = {
    flex: 1,
}

export const SAFE_AREA_VIEW: ViewStyle = {
    ...FULL,
}

export const AVOID_KEYBOARD_VIEW: ViewStyle = {
    ...FULL,
}


export const CONTAINER: ViewStyle = {
    alignItems: "center",
    height: '95%',
    marginBottom: metric.ratioHeight(30)
}

export const MENU_ICON_STYLE: ImageStyle = {
    width: metric.ratioWidth(24),
    height: metric.ratioHeight(24)
}

export const HEADER_CONTAINER: ViewStyle = {
    marginBottom: metric.ratioHeight(22),
    alignItems: 'center'
}

export const HEADER_PROPS: HeaderProps = {
    leftIconSource: images.hamMenu,
    leftIconStyle: MENU_ICON_STYLE,
    isLeftIconAnimated: false,
}

export const TEXT_CONTAINER: ViewStyle = {
    marginTop: metric.ratioHeight(22)
}

export const FONT_REGULAR: TextStyle = {
    //Android
    fontFamily: "Montserrat-Regular",
}

export const TITLE: TextStyle = {
    ...TEXT,
    ...FONT_REGULAR,
    fontSize: RFValue(25),
    fontWeight: '400',
}

export const SUB_TITLE: TextStyle = {
    ...TEXT,
    ...FONT_REGULAR,
    fontSize: RFValue(17),
    fontWeight: '400',
    lineHeight: metric.ratioHeight(30),
}

export const WEB_VIEW: ViewStyle = {
    backgroundColor: 'transparent'
}

export const WEB_VIEW_CONTAINER: ViewStyle = {
    width: '80%',
    marginTop: metric.ratioHeight(22),
    opacity: 0.8,
    backgroundColor: color.palette.white,
    borderRadius: metric.ratioWidth(14),
    paddingHorizontal: metric.ratioWidth(10),
    paddingVertical: metric.ratioWidth(10),
    shadowColor: '#00000029',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.2,
    elevation: 4
}
