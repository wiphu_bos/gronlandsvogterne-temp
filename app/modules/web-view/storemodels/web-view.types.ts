import { Instance, SnapshotOut } from "mobx-state-tree"
import { WebViewResourcesStoreModel,WebViewStoreModel } from "./web-view.store"

export type WebViewStore = Instance<typeof WebViewStoreModel>
export type WebViewStoreSnapshot = SnapshotOut<typeof WebViewStoreModel>

export type WebViewResourcesStore = Instance<typeof WebViewResourcesStoreModel>
export type WebViewResourcesStoreSnapshot = SnapshotOut<typeof WebViewResourcesStoreModel>
