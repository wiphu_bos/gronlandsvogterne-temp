import { types } from "mobx-state-tree"
import { StoreName } from "../../../constants/app.constant"
import { WebViewPropsModel } from "../viewmodels/web-view.models"
import { WebViewActions } from "../viewmodels/web-view.actions"
import { WebViewViews, WebViewResourcesViews } from "../viewmodels/web-view.views"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store"


const  WebViewModel = types.model(StoreName.WebView,  WebViewPropsModel)

export const WebViewStoreModel = types.compose(
    WebViewModel,
    WebViewViews,
    WebViewActions)
export const  WebViewResourcesStoreModel = types.compose(GeneralResourcesStoreModel, WebViewResourcesViews)