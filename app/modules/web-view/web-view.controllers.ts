// MARK: Import
import { NavigationKey } from "../../constants/app.constant"
import { RootStore, INavigationRoute } from "../../models"
import { onSnapshot } from "mobx-state-tree"
import { WebViewProps } from "./web-view.props"
import { BaseController } from "../base.controller"
import { WebViewStore, WebViewResourcesStore, WebViewResourcesStoreModel, WebViewStoreModel } from "./storemodels"
import { HeaderProps } from "../../components/header/header.props"
import { DrawerActions } from "@react-navigation/core"

class WebViewController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public viewModel: WebViewStore
    public resourcesViewModel: WebViewResourcesStore
    public myProps: WebViewProps & Partial<INavigationRoute>

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, props?: WebViewProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, props, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(props)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    //@override
    _setupProps = (props: WebViewProps & Partial<INavigationRoute>) => {
        this.myProps = {
            ...props as object,
            ...(props as INavigationRoute)?.route?.params
        }
    }

    private _setupResourcesViewModel = () => {
        this.resourcesViewModel = WebViewResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.WebView)
        this.viewModel = localStore && WebViewStoreModel.create({ ...localStore }) || WebViewStoreModel.create({})
    }

    private _setupSnapShot = () => {
        onSnapshot(this.viewModel, (snap: WebViewStore) => {
            this._setInitializedToPropsParams()
        })
    }

    /*
       Mark Data
   */


    /*
       Mark Event
   */
    //@override
    backProcess = () => {
    }


    public getHeaderLeftOptions: HeaderProps = {
        onLeftPress: () => this.onSideMenuPress()
    }

    public onSideMenuPress = () => {
        this._myProps?.navigation?.dispatch(DrawerActions.openDrawer() as any)
    }


    viewDidAppearAfterFocus = async () => {
        super.viewDidAppearAfterFocus && super.viewDidAppearAfterFocus()
    }

    onLoadStart = async () => {
        this._isGlobalLoading(true)
    }

    onLoad = async () => {
        this._delayExecutor(() => this._isGlobalLoading(false))
    }



    /*
       Mark Helper
   */
}

export default WebViewController