// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { NotificationListProps } from "./notification-list.props"
import NotificationListController from "./notification-list.controllers"
import { useConfigurate } from "../../custom-hooks/use-configure-controller"
import { Wallpaper, Header, Text } from "../../components"
import { View, RefreshControl } from "react-native"
import { FlatList } from "react-native-gesture-handler"
import { NotificationComponents } from "./views/notification-card/notification-card"
import { SafeAreaView } from "react-navigation"
import * as metric from "../../theme"

// MARK: Style Import

import * as Styles from "./notification-list.styles"

export const NotificationListScreen: React.FunctionComponent<NotificationListProps> = observer((props) => {

    const controller = useConfigurate(NotificationListController, props) as NotificationListController

    // MARK: Render
    return (
        <Wallpaper showBubble>
            <SafeAreaView
                {...metric.safeAreaViewProps}
                shouldRasterizeIOS
                style={Styles.SAFE_AREA_VIEW}>
                <Header {...Styles.HEADER_PROPS} {...controller.getHeaderLeftOptions} />
                <View shouldRasterizeIOS style={Styles.HEADER_CONTAINER}>
                    <Text text={NotificationListController.resourcesViewModel?.getResourceScreenTitle()} style={Styles.TITLE}
                        {...NotificationListController.resourcesViewModel?.getTestIDTitle()} />
                </View>
                <View shouldRasterizeIOS style={Styles.BODY_CONTAINER}>
                    <FlatList
                        contentContainerStyle={Styles.LIST_VIEW}
                        refreshControl={
                            <RefreshControl
                                style={Styles.REFRESH_CONTROL}
                                refreshing={NotificationListController.viewModel?.getIsPullRefreshing}
                                onRefresh={controller.onRefresh}
                            />
                        }
                        ItemSeparatorComponent={({ highlighted }) => (
                            <View shouldRasterizeIOS style={Styles.SEPARATOR_VIEW} />
                        )}
                        extraData={NotificationListController.viewModel?.getNotificationList?.slice()}
                        data={NotificationListController.viewModel?.getNotificationList}
                        renderItem={({ item, index, separators }) => (
                            <NotificationComponents
                                // timeAgoTestID
                                animation={controller.flatlistResultAnimationConfig}
                                key={index}
                                notification$={item}
                                onPress={() => controller.onNotificationItemPress(item)} />
                        )}
                    />

                </View>
            </SafeAreaView>
        </Wallpaper>

    )
})
