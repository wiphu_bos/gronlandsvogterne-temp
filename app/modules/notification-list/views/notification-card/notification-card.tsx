// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { Text, Button } from "../../../../components"
import { View } from "react-native"
import { NotificationListProps } from "../../notification-list.props"
import NotificationListController from "../../notification-list.controllers"
import { TimeAgoCompoenents } from "../../../../components/time-ago"
import * as Animatable from 'react-native-animatable'

// MARK: Style Import

import * as Styles from "./notification-card.styles"

export const NotificationComponents: React.FunctionComponent<NotificationListProps> = observer((props) => {

    const title = NotificationListController.resourcesViewModel?.getResourceNotificationTitle(props?.notification$?.title)
    const body = NotificationListController.resourcesViewModel?.getResourceNotificationTitle(props?.notification$?.body)
    return (
        <Button
            preset="none"
            isAnimated={false}
            isSolid
            style={Styles.ROOT_CONTAINNER}
            containerStyle={Styles.ROOT_CONTAINNER}
            onPress={() => props.onPress(props?.notification$)}>
            <Animatable.View shouldRasterizeIOS style={Styles.BODY_CONTAINNER}
                {...props?.animation}>
                <View shouldRasterizeIOS style={Styles.UNREAD_CONTAINER}>
                    {!props?.notification$?.misc?.is_read && <View shouldRasterizeIOS style={Styles.UNREAD} />}
                </View>
                <View shouldRasterizeIOS style={Styles.CONTENT_CONTAINER}>
                    <View style={Styles.CONTENT_HEADER}>
                        <Text
                            // {...props?.fullnameTestID}
                            numberOfLines={1}
                            text={title || props?.notification$?.title}
                            style={{ ...Styles.TITLE, ...Styles.TITLE_VIEW }} />
                            <TimeAgoCompoenents testID={props?.timeAgoTestID} timeObject$={props?.notification$} />
                    </View>
                    <Text text={body || props?.notification$?.body}
                     style={Styles.DESCRIPTION} />
                </View>
            </Animatable.View>
        </Button >
    )
})
