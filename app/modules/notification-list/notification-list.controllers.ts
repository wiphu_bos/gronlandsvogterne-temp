// MARK: Import
import { NavigationKey, NotificationTopicKey } from "../../constants/app.constant"
import { NotificationListProps } from "./notification-list.props"
import { NotificationListResourcesStoreModel, NotificationListStoreModel } from "./storemodels/notification-list.store"
import { NotificationListResourcesStore, NotificationListStore } from "./storemodels/notification-list.types"
import { RootStore, INavigationRoute, ISharedTimer } from "../../models"
import { HeaderProps } from "../../components/header/header.props"
import { INotification } from "../../models/notification-store"
import { StackActions } from "@react-navigation/core"
import * as DataUtils from "../../utils/data.utils"
import { FirebaseFirestoreTypes } from '@react-native-firebase/firestore'
import { SnapshotChangeType } from "../../constants/firebase/fire-store.constant"
import { onSnapshot, unprotect } from "mobx-state-tree"
import { INotificationListListener } from "../../utils/firebase/fire-store/fire-store.types"
import FirebaseFireStore from "../../utils/firebase/fire-store/fire-store"
import { BaseController } from "../base.controller"
import { IUser } from "../../models/user-store"
import * as metric from "../../theme"
import { IChat } from "../../models/chat-store"

class NotificationListController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static resourcesViewModel: NotificationListResourcesStore
    public static viewModel: NotificationListStore
    private static _disposer: () => void
    private static _isVisited: boolean

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, props?: NotificationListProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, props, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(props)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        NotificationListController.resourcesViewModel = NotificationListResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.SearchListScreen)
        NotificationListController.viewModel = localStore && NotificationListStoreModel.create({ ...localStore }) || NotificationListStoreModel.create({})
        unprotect(NotificationListController.viewModel)
    }

    private _setupSnapShot = () => {
        onSnapshot(NotificationListController.viewModel, (snap: NotificationListStore) => {
            this._setInitializedToPropsParams()
        })
    }

    /*
       Mark Data
   */

    private _loadCacheData = () => NotificationListController.viewModel?.setNotificationList(this._rootStore, this._rootStore?.getUserStore?.getNotificationList)

    public flatlistResultAnimationConfig = NotificationListController._isVisited ? null : metric.flatlistResultAnimationConfig

    private _fetchNotificationList = async () => await NotificationListController.viewModel?.fetchNotificationList(this._rootStore)

    private _onNotificationListener = () => {
        const onNext: (snapshot: FirebaseFirestoreTypes.QuerySnapshot) => void = (snap => {
            if (!NotificationListController.viewModel?.isFirstLoadDone || snap.metadata.hasPendingWrites) return
            snap.docChanges().forEach(change => {
                const notification: INotification & ISharedTimer = DataUtils.getNotificationObject(change.doc.data(), true)
                if (change.type === SnapshotChangeType.ADDED) {
                    if (notification) NotificationListController.viewModel?.addNotificationIntoList(this._rootStore, notification)
                }
            })
        })

        const onError: (e: Error) => void = (e => {
        })

        const onComplete: () => void = () => {
        }

        const params: INotificationListListener = {
            userId: this._rootStore?.getUserStore?.getUserId,
            onNext,
            onError,
            onComplete
        }

        NotificationListController._disposer = FirebaseFireStore.onNotificationListener(params)
    }

    /*
       Mark Event
   */

    private _isLoading = (value: boolean) => NotificationListController.viewModel?.setIsPullRefreshing(value)

    public onRefresh = async () => {
        this._isLoading(true)
        NotificationListController.viewModel?.resetFetching()
        await this._fetchNotificationList()
        this._isLoading(false)
    }

    public getHeaderLeftOptions: HeaderProps = {
        onLeftPress: this.backProcess
    }

    public onLoadMore = async () => {
        if (!NotificationListController.viewModel?.getShouldLoadmore) return
        NotificationListController.viewModel?.setIsLoadMore(true)
        await this._fetchNotificationList()
    }

    /*
    
    Will be refactored later
    
    */
    public onNotificationItemPress = async (item?: INotification) => {
        const notificationId = item?.notification_id
        const isRead: boolean = item?.misc?.is_read
        let actions: any[]
        if (item?.topic?.includes(NotificationTopicKey.CreatedDude) || item?.topic?.includes(NotificationTopicKey.PublishDude)) {
            const dudeId = item?.data?.uid
            if (!dudeId) return
            this._isGlobalLoading(true)
            const dudeProfile: IUser = await NotificationListController.viewModel?.fetchDudeProfileDetails(this._rootStore, dudeId)
            this._isGlobalLoading(false)
            if (!dudeProfile) return
            const dudeProfileObject = DataUtils.getUserProfileObject(this._rootStore, null, dudeProfile)
            actions = [StackActions.push(NavigationKey.CreateEditProfileTab, { dudeProfileObject: dudeProfileObject })]

        } else if (item?.topic?.includes(NotificationTopicKey.Comment)) {
            const dudeId = item?.data?.dudeProfile?.uid
            if (!dudeId) return
            this._isGlobalLoading(true)
            const dudeProfile: IUser = await NotificationListController.viewModel?.fetchDudeProfileDetails(this._rootStore, dudeId)
            this._isGlobalLoading(false)
            if (!dudeProfile) return
            const dudeProfileObject = DataUtils.getUserProfileObject(this._rootStore, null, dudeProfile)
            actions = [StackActions.push(NavigationKey.DudeProfileDetailTab, {
                dudeProfileObject: dudeProfileObject,
                shouldGoToCommentTab: true
            })]
        } else if (item?.topic?.includes(NotificationTopicKey.CreateMessage)) {
            const chatRoomObject = { ...item.data } as IChat //spread to IChat to dis-observe
            if (!chatRoomObject) return
            actions = [StackActions.push(NavigationKey.ChatRoom, { chatRoomObject: chatRoomObject, shouldDelayFetching: true })]
        }

        Promise.all(actions.map(e => this._myProps?.navigation?.dispatch(e)))
        this._rootStore?.getUserStore?.setReadNotificationById(item?.notification_id)
        !isRead && await NotificationListController.viewModel?.markReadNotificationByIdWaitResponse(notificationId)
    }

    /*
       Mark Life cycle
   */

    //@override
    viewDidAppearOnce = async () => {
        this._onNotificationListener()
        this._loadCacheData()
    }
    //@override
    viewDidAppearAfterFocus = async () => {
        super.viewDidAppearAfterFocus && super.viewDidAppearAfterFocus()
        const isConnected = this._rootStore?.getSharedStore?.getIsConnected
        if (isConnected) {
            if (NotificationListController.viewModel?.getNotificationList?.length === 0) {
                await this.onRefresh()
            } else if (!NotificationListController.viewModel?.getIsFirstLoadDone) {
                this._isLoading(true)
                await this._fetchNotificationList()
                this._isLoading(false)
            }
        } else {
            NotificationListController.viewModel?.setIsFirstLoadDone(true)
        }
        if (!NotificationListController._isVisited) {
            NotificationListController._isVisited = true
        }
    }

    //@override
    deInit = () => {
        super.deInit && super.deInit()
        NotificationListController._disposer && NotificationListController._disposer()
        NotificationListController.viewModel?.invalidateAllTimerIfNeeded()
    }
}

export default NotificationListController
