import { Instance, SnapshotOut } from "mobx-state-tree"
import { NotificationListStoreModel, NotificationListResourcesStoreModel } from "./notification-list.store"
import { INotification } from "../../../models/notification-store"

export type NotificationListStore = Instance<typeof NotificationListStoreModel>
export type NotificationListStoreSnapshot = SnapshotOut<typeof NotificationListStoreModel>

export type NotificationListResourcesStore = Instance<typeof NotificationListResourcesStoreModel>
export type NotificationListResourcesStoreSnapshot = SnapshotOut<typeof NotificationListResourcesStoreModel>


export type INotificationPage = {
    page_size?: number,
    is_last_page?: boolean,
    last_id?: string,
}

export type INotificationResponse = {
    notification_list?: INotification[],
    page?: INotificationPage
}