import { types } from "mobx-state-tree"
import { StoreName } from "../../../constants/app.constant"
import { NotificationListPropsModel } from "../viewmodels/notification-list.models"
import { NotificationListActions } from "../viewmodels/notification-list.actions"
import { NotificationListViews, NotificationListResourcesViews } from "../viewmodels/notification-list.views"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store"
import { NotificationListServiceActions } from "../services/notification-list.services"

const NotificationListModel = types.model(StoreName.NotificationList, NotificationListPropsModel)
export const NotificationListStoreModel = types.compose(NotificationListModel, NotificationListViews, NotificationListActions, NotificationListServiceActions)
export const NotificationListResourcesStoreModel = types.compose(GeneralResourcesStoreModel, NotificationListResourcesViews)

