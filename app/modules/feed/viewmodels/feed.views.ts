import { types } from "mobx-state-tree"
import { FeedPropsModel } from "./feed.models"
import { GeneralResources, RemoteConfigGroupedKey } from "../../../constants/firebase/remote-config"
import { NavigationKey } from "../../../constants/app.constant"
import { TestIDResources } from "../../../constants/test-key/test.constant"
import * as Utils from "../../../utils"
import * as DataUtils from "../../../utils/data.utils"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store"

export const FeedViews = types.model(FeedPropsModel)
    .views(self => ({
        get getNotificationList() {
            var rv = [];

            for( var i = 0; i < 3; i++ ){
                rv.push( { title: 'yellow', body: 'yellow', height : '100%'  })
            }
            rv.push( { title: 'yellow', body: 'yellow', height : '90%'  })

            return rv
        },
        get getShouldLoadmore(): boolean {
            return !self.isLoadDone && !self.isLoadMore && self.notificationList$.length > 0 && self.isFirstLoadDone
        },
        get getIsFirstLoadDone(): boolean {
            return self.isFirstLoadDone
        },
        get getIsPullRefreshing(): boolean {
            return self.isPullRefreshing
        },
    }))

export const FeedResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State

    const { NotificationScreen } = GeneralResources
    const { screenTitle } = NotificationScreen
    const { NotificationList } = NavigationKey
    RemoteConfigGroupedKey
    //MARK: Views
    const getResources = (key: string, childKeyOrShareKey: string | boolean = false) => self.getValues(childKeyOrShareKey ? key : NotificationList, childKeyOrShareKey ? true : key)
    const getResourceScreenTitle = () => getResources(screenTitle)
    const getResourceNotificationTitle = (value: string) => DataUtils.getNotificationByLocaleKey(self, value, RemoteConfigGroupedKey.NotificationTitle)
    const getResourceNotificationBody = (value: string) => DataUtils.getNotificationByLocaleKey(self, value, RemoteConfigGroupedKey.NotificationBody)
    return {
        getResourceScreenTitle,
        getResourceNotificationTitle,
        getResourceNotificationBody
    }
})
    .views(self => {

        //MARK: Volatile State

        //MARK: Views

        const getTestIDTitle = () => Utils.getTestIDObject(TestIDResources.NotificationListScreen.title)
        const getTestIDDescription = () => Utils.getTestIDObject(TestIDResources.NotificationListScreen.description)
        const getTestIDTimeAgo = () => Utils.getTestIDObject(TestIDResources.NotificationListScreen.timeAgo)
        return {
            getTestIDTitle,
            getTestIDDescription,
            getTestIDTimeAgo,
        }
    })
