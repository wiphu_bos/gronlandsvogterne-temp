// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { Text, Button } from "../../../../components"
import { View, Alert, TouchableWithoutFeedback, StyleSheet, Image } from "react-native"
import { FeedProps } from "../../feed.props"
import { images } from "../../../../theme/images"
import { Icon } from "../../../../components"
import { FeedDotComponents } from "./feed-dot-card"

// MARK: Style Import


export const FeedComponents: React.FunctionComponent<FeedProps> = observer((props) => {

    const title =  props?.notification$?.title
    const body =  props?.notification$?.body

   return (

         <TouchableWithoutFeedback >
              
              <View style={{ width: '100%', height:100, flex:1, margin:0, backgroundColor:'white', flexDirection:"row"}}> 

                <FeedDotComponents                                 
                notification$={props?.notification$}
                style={{ width: '5%', height:"90%", backgroundColor:'white', justifyContent: 'center', alignItems: 'center'}}> 
                </FeedDotComponents>

                <View style={{ width: '95%', height:"90%", backgroundColor:'pink'}}> 

                <View style={flatListStyle.cell}>
                    <View style={{ width: '30%', justifyContent: 'center', alignItems: 'center',}}>
                      <Icon source={images.infomation} style={flatListStyle.imageView} />
                      <Text style={flatListStyle.textBottomImage} >{title}</Text>
                    </View>
                    <View style={{width: '70%'}}>
                      <Text style={flatListStyle.textView} >{title}</Text>
                      <Text style={flatListStyle.textView} >{body}</Text>
                    </View>
                  </View>
                </View>

              </View> 

           </TouchableWithoutFeedback>
    )
 })

const flatListStyle = StyleSheet.create({
    flatListView: {
      width: '100%',
      height: '50%' ,
      padding:15,
      backgroundColor:'#880000',
      // backgroundColor:'transparent',
    },
  
    cell :{
      flex:1, 
      margin: 5,
      backgroundColor:'transparent',
      padding : 5,
      height: 100,
      flexDirection:"row",
    },

    stepButton: {
      width: 20,
      height: 20 ,
      margin: 0,
      padding:0,
      borderRadius : 10,
      overflow: 'hidden',
    },

    imageView: {
      width: 30,
      height: 30 ,
      margin: 0,
      padding:0,
      borderRadius : 1000,
      overflow: 'hidden',
    },
  
    textView: {
      width:'90%',
      height: '15%' ,
      textAlign:'center',
      margin: 5,
      padding:0,
      color:'#000000', backgroundColor:'#ffffff',
    },
  
    textBottomImage: {
      width:'90%',
      height: '40%' ,
      textAlign:'center',
      margin: 10,
      padding:0,
      color:'#000000', backgroundColor:'#ffffff',
      fontSize: 8,
    }
  });

  const bx = StyleSheet.create({
    box1:{
      height:50,
      width:50,
      backgroundColor:'red'
    },
    box2:{
      height:50,
      width:50,
      backgroundColor:'green',
      top:10,
      left:30
      
    },
  });