// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { View } from "react-native"
import { FeedProps } from "../../feed.props"

// MARK: Style Import


export const FeedDotComponents: React.FunctionComponent<FeedProps> = observer((props) => {
  const title =  props?.notification$?.title
  const body =  props?.notification$?.body
  let color: string = props?.notification$?.title;
  let ht: string = props?.notification$?.height;

   return (
              
      <View style={{flex: 1,flexDirection:'row', backgroundColor:'transparent', justifyContent:'center', alignItems:'center'}}>           
          <View style={{ height:'90%', width:'100%', backgroundColor:'transparent', justifyContent:'flex-start', alignItems:'flex-start', alignSelf:'flex-start'}}>
              <View style={{ height:'100%', width:'50%', backgroundColor:'pink', justifyContent:'flex-end', alignItems:'flex-end', alignSelf:'flex-end'}}>
              </View>
          </View>

          <View style={{height:'100%', justifyContent:'center', alignItems:'center', alignSelf:'center', position:'absolute'}}>
              <View style={ {backgroundColor:'red', borderRadius:10000, height:14, width:14}}>
              </View>
          </View>
          
          <View style={{ height:'100%', justifyContent:'flex-start',alignItems:'flex-start',alignSelf:'flex-start', position:'absolute'}}>
              <View style={{backgroundColor:'red', height:ht, width:2 }}></View>
          </View>
    </View>

    )
 })
