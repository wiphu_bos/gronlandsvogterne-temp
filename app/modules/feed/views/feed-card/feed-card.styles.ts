import { TextStyle, ViewStyle } from "react-native"
import * as metric from "../../../../theme"
import { color } from "../../../../theme"
import { RFValue } from "react-native-responsive-fontsize"

const FULL: ViewStyle = {
    flex: 1
}

export const ROOT_CONTAINNER: ViewStyle = {
    width: '100%',
    height: metric.ratioHeight(76.5),
    backgroundColor: color.bodyPearl
}
export const UNREAD_CONTAINER: ViewStyle = {
    width: metric.ratioWidth(21),
    justifyContent: 'center',
    alignItems: 'flex-end',
    marginTop: metric.ratioHeight(8)

}

export const UNREAD: ViewStyle = {
    backgroundColor: color.palette.blue,
    width: metric.ratioWidth(8),
    height: metric.ratioHeight(8),
    borderRadius: metric.ratioWidth(4),
}

export const CONTAINNER_TEXT: ViewStyle = {
    flexDirection: 'column',
    flex: 1,
    paddingHorizontal: metric.ratioWidth(15),
}

export const CONTAINNER: ViewStyle = {
    flexDirection: 'row'
}


export const BODY_CONTAINNER: ViewStyle = {
    ...FULL,
    flexDirection: 'row'
}

export const IMAGE_PROFILE_CONTAINNER: ViewStyle = {
    alignItems: 'flex-end',
    justifyContent: 'center',
    width: metric.ratioWidth(72)
}

export const CONTENT_CONTAINER: ViewStyle = {
    ...FULL,
    paddingHorizontal: metric.ratioWidth(21),
    justifyContent: 'center'
}

export const CONTENT_HEADER: ViewStyle = {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
}

const TEXT: TextStyle = {
    color: color.palette.pink,
    fontFamily: "Montserrat",
}

export const TITLE: TextStyle = {
    ...TEXT,
    lineHeight: metric.ratioHeight(36),
    fontSize: RFValue(17),
    fontWeight: '400',

    //Android
    fontFamily: "Montserrat-Regular"
}
export const DESCRIPTION: TextStyle = {
    ...TEXT,
    lineHeight: metric.ratioHeight(20),
    fontSize: RFValue(12.5),
    fontWeight: '300',

    //Android
    fontFamily: "Montserrat-Light",
    color: color.palette.darkGrey,
}



export const TITLE_VIEW: ViewStyle = {
    width: '66%'
}

export const TIME_AGO_VIEW: ViewStyle | TextStyle = {
    textAlign:'right',
    width: '31%'
}

export const SUB_TITLE: TextStyle = {
    ...TEXT,
    lineHeight: metric.ratioHeight(24),
    fontSize: RFValue(12),
    fontWeight: '300',

    //Android
    fontFamily: "Montserrat-Light",
}
