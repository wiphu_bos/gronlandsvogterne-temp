import { types } from "mobx-state-tree"
import { StoreName } from "../../../constants/app.constant"
import { FeedPropsModel } from "../viewmodels/feed.models"
import { FeedActions } from "../viewmodels/feed.actions"
import { FeedViews, FeedResourcesViews } from "../viewmodels/feed.views"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store"
import { FeedServiceActions } from "../services/feed.services"

const FeedModel = types.model(StoreName.NotificationList, FeedPropsModel)
export const FeedStoreModel = types.compose(FeedModel, FeedViews, FeedActions, FeedServiceActions)
export const FeedResourcesStoreModel = types.compose(GeneralResourcesStoreModel, FeedResourcesViews)

