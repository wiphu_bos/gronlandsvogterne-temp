import { Instance, SnapshotOut } from "mobx-state-tree"
// import { FeedStoreModel, FeedResourcesStoreModel } from "./atest-list.store"
import { FeedStoreModel, FeedResourcesStoreModel } from "./feed.store"
import { INotification } from "../../../models/notification-store"

export type FeedStore = Instance<typeof FeedStoreModel>
export type FeedStoreSnapshot = SnapshotOut<typeof FeedStoreModel>

export type FeedResourcesStore = Instance<typeof FeedResourcesStoreModel>
export type FeedResourcesStoreSnapshot = SnapshotOut<typeof FeedResourcesStoreModel>


export type INotificationPage = {
    page_size?: number,
    is_last_page?: boolean,
    last_id?: string,
}

export type INotificationResponse = {
    notification_list?: INotification[],
    page?: INotificationPage
}