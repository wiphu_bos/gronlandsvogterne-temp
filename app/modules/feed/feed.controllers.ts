// MARK: Import
import { NavigationKey, NotificationTopicKey } from "../../constants/app.constant"
import { FeedProps } from "./feed.props"
import { FeedResourcesStoreModel, FeedStoreModel } from "./storemodels/feed.store"
import { FeedResourcesStore, FeedStore } from "./storemodels/feed.types"
import { RootStore, INavigationRoute, ISharedTimer } from "../../models"
import { HeaderProps } from "../../components/header/header.props"
import { INotification } from "../../models/notification-store"
import { StackActions } from "@react-navigation/core"
import * as DataUtils from "../../utils/data.utils"
import { FirebaseFirestoreTypes } from '@react-native-firebase/firestore'
import { SnapshotChangeType } from "../../constants/firebase/fire-store.constant"
import { onSnapshot, unprotect } from "mobx-state-tree"
import { INotificationListListener } from "../../utils/firebase/fire-store/fire-store.types"
import FirebaseFireStore from "../../utils/firebase/fire-store/fire-store"
import { BaseController } from "../base.controller"
import { IUser } from "../../models/user-store"
import * as metric from "../../theme"
import { IChat } from "../../models/chat-store"

class FeedController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static resourcesViewModel: FeedResourcesStore
    public static viewModel: FeedStore
    private static _disposer: () => void
    private static _isVisited: boolean

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, props?: FeedProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, props, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(props)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        FeedController.resourcesViewModel = FeedResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.SearchListScreen)
        FeedController.viewModel = localStore && FeedStoreModel.create({ ...localStore }) || FeedStoreModel.create({})
        unprotect(FeedController.viewModel)
    }

    private _setupSnapShot = () => {
        onSnapshot(FeedController.viewModel, (snap: FeedStore) => {
            this._setInitializedToPropsParams()
        })
    }

    /*
       Mark Data
   */

    private _loadCacheData = () => FeedController.viewModel?.setNotificationList(this._rootStore, this._rootStore?.getUserStore?.getNotificationList)

    public flatlistResultAnimationConfig = FeedController._isVisited ? null : metric.flatlistResultAnimationConfig

    private _fetchNotificationList = async () => await FeedController.viewModel?.fetchNotificationList(this._rootStore)

    private _onFeedener = () => {
        const onNext: (snapshot: FirebaseFirestoreTypes.QuerySnapshot) => void = (snap => {
            if (!FeedController.viewModel?.isFirstLoadDone || snap.metadata.hasPendingWrites) return
            snap.docChanges().forEach(change => {
                const notification: INotification & ISharedTimer = DataUtils.getNotificationObject(change.doc.data(), true)
                if (change.type === SnapshotChangeType.ADDED) {
                    if (notification) FeedController.viewModel?.addNotificationIntoList(this._rootStore, notification)
                }
            })
        })

        const onError: (e: Error) => void = (e => {
        })

        const onComplete: () => void = () => {
        }

        const params: INotificationListListener = {
            userId: this._rootStore?.getUserStore?.getUserId,
            onNext,
            onError,
            onComplete
        }
        FeedController._disposer = FirebaseFireStore.onNotificationListener(params)
    }

    /*
       Mark Event
   */

    private _isLoading = (value: boolean) => FeedController.viewModel?.setIsPullRefreshing(value)

    public onRefresh = async () => {
        this._isLoading(true)
        FeedController.viewModel?.resetFetching()
        await this._fetchNotificationList()
        this._isLoading(false)
    }

    public getHeaderLeftOptions: HeaderProps = {
        onLeftPress: this.backProcess
    }

    public onLoadMore = async () => {
        if (!FeedController.viewModel?.getShouldLoadmore) return
        FeedController.viewModel?.setIsLoadMore(true)
        await this._fetchNotificationList()
    }

    /*
    
    Will be refactored later
    
    */
    public onNotificationItemPress = async (item?: INotification) => {
        const notificationId = item?.notification_id
        const isRead: boolean = item?.misc?.is_read
        let actions: any[]
        if (item?.topic?.includes(NotificationTopicKey.CreatedDude) || item?.topic?.includes(NotificationTopicKey.PublishDude)) {
            const dudeId = item?.data?.uid
            if (!dudeId) return
            this._isGlobalLoading(true)
            const dudeProfile: IUser = await FeedController.viewModel?.fetchDudeProfileDetails(this._rootStore, dudeId)
            this._isGlobalLoading(false)
            if (!dudeProfile) return
            const dudeProfileObject = DataUtils.getUserProfileObject(this._rootStore, null, dudeProfile)
            actions = [StackActions.push(NavigationKey.CreateEditProfileTab, { dudeProfileObject: dudeProfileObject })]

        } else if (item?.topic?.includes(NotificationTopicKey.Comment)) {
            const dudeId = item?.data?.dudeProfile?.uid
            if (!dudeId) return
            this._isGlobalLoading(true)
            const dudeProfile: IUser = await FeedController.viewModel?.fetchDudeProfileDetails(this._rootStore, dudeId)
            this._isGlobalLoading(false)
            if (!dudeProfile) return
            const dudeProfileObject = DataUtils.getUserProfileObject(this._rootStore, null, dudeProfile)
            actions = [StackActions.push(NavigationKey.DudeProfileDetailTab, {
                dudeProfileObject: dudeProfileObject,
                shouldGoToCommentTab: true
            })]
        } else if (item?.topic?.includes(NotificationTopicKey.CreateMessage)) {
            const chatRoomObject = { ...item.data } as IChat //spread to IChat to dis-observe
            if (!chatRoomObject) return
            actions = [StackActions.push(NavigationKey.ChatRoom, { chatRoomObject: chatRoomObject, shouldDelayFetching: true })]
        }

        Promise.all(actions.map(e => this._myProps?.navigation?.dispatch(e)))
        this._rootStore?.getUserStore?.setReadNotificationById(item?.notification_id)
        !isRead && await FeedController.viewModel?.markReadNotificationByIdWaitResponse(notificationId)
    }

    /*
       Mark Life cycle
   */

    //@override
    viewDidAppearOnce = async () => {
        this._onFeedener()
        this._loadCacheData()
    }
    //@override
    viewDidAppearAfterFocus = async () => {
        super.viewDidAppearAfterFocus && super.viewDidAppearAfterFocus()
        const isConnected = this._rootStore?.getSharedStore?.getIsConnected
        if (isConnected) {
            if (FeedController.viewModel?.getNotificationList?.length === 0) {
                await this.onRefresh()
            } else if (!FeedController.viewModel?.getIsFirstLoadDone) {
                this._isLoading(true)
                await this._fetchNotificationList()
                this._isLoading(false)
            }
        } else {
            FeedController.viewModel?.setIsFirstLoadDone(true)
        }
        if (!FeedController._isVisited) {
            FeedController._isVisited = true
        }
    }

    //@override
    deInit = () => {
        super.deInit && super.deInit()
        FeedController._disposer && FeedController._disposer()
        FeedController.viewModel?.invalidateAllTimerIfNeeded()
    }
}

export default FeedController
