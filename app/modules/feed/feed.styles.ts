import { ViewStyle, ImageStyle, TextStyle } from "react-native"
import { color } from "../../theme"
import * as metric from "../../theme"
import { HeaderProps } from "../../components/header/header.props"
import { images } from "../../theme/images"
import { RFValue } from "react-native-responsive-fontsize"

export const FULL: ViewStyle = {
    flex: 1
}

export const TAB_PAGE_BACKGROUND: any = { colors: color.tabPageBackground }

export const SAFE_AREA_VIEW: ViewStyle = {
    ...FULL,
}

export const LIST_VIEW: ViewStyle = {
    paddingBottom: metric.ratioHeight(32),
    backgroundColor: color.bodyPearl
}

export const BODY_CONTAINER: ViewStyle = {
    ...FULL,
    backgroundColor: color.bodyPearl
}
export const SEPARATOR_VIEW: ViewStyle = {
    height: metric.ratioHeight(1),
    backgroundColor: color.palette.white,
    marginHorizontal: metric.ratioWidth(20)
}

export const SEPARATOR_VIEW_FEED: ViewStyle = {
    height: metric.ratioHeight(0),
    backgroundColor: color.palette.white,
    marginHorizontal: metric.ratioWidth(20)
}

export const MENU_ICON_STYLE: ImageStyle = {
    width: metric.ratioWidth(24),
    height: metric.ratioHeight(24)
}
export const HEADER_PROPS: HeaderProps = {
    leftIconSource: images.back,
    leftIconStyle: MENU_ICON_STYLE,
    isLeftIconAnimated: false,
    containerStyle: {
        height: metric.ratioHeight(59)
    }
}

const TEXT: TextStyle = {
    color: color.palette.white,
    fontFamily: "Montserrat",
}

export const TITLE: TextStyle = {
    ...TEXT,
    lineHeight: metric.ratioHeight(36),
    fontSize: RFValue(24),
    fontWeight: '400',

    //Android
    fontFamily: "Montserrat-Regular",
}

export const HEADER_CONTAINER: ViewStyle = {
    marginBottom: metric.ratioHeight(22),
    alignItems: 'center'
}

export const REFRESH_CONTROL: any = { tintColor: color.palette.lighterGrey }
