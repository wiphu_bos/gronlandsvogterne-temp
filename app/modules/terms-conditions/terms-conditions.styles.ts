import { ViewStyle, TextStyle, ImageStyle } from "react-native"
import { color, ratioWidth } from "../../theme"
import * as metric from "../../theme"
import { RFValue } from "react-native-responsive-fontsize"

export const FULL: ViewStyle = {
    flex: 1
}

export const SAFE_AREA_VIEW: ViewStyle = {
    ...FULL,
}
export const SCROLL_VIEW: ViewStyle = {
    ...FULL,
    marginTop: metric.ratioHeight(33),
    backgroundColor: color.palette.pearl
}
export const TEXT_CONTAINER_VIEW: ViewStyle = {
    marginTop: metric.ratioHeight(34),
    marginHorizontal: metric.ratioWidth(37),
    paddingBottom: metric.ratioHeight(23)
}

export const BASE_TEXT: TextStyle = {
    fontFamily: 'Montserrat',
    textAlign: 'center',
}

export const TITLE_TEXT: TextStyle = {
    ...BASE_TEXT,
    fontWeight: '400',
    fontSize: RFValue(25),

    //Android
    fontFamily: "Montserrat-Regular",

}
export const SUB_TITLE_TEXT: TextStyle = {
    ...TITLE_TEXT
}

export const DESCRIPTION_TEXT: TextStyle = {
    ...BASE_TEXT,
    fontWeight: '300',
    fontSize: RFValue(16),
    lineHeight: metric.ratioHeight(30),

    //Android
    fontFamily: "Montserrat-Light",
    color: color.palette.darkGrey,
    textAlign: 'justify'
}


export const TITLE_CONTAINNER: ViewStyle = {
    marginHorizontal: ratioWidth(81)
}

export const SUB_TITLE_CONTAINNER: ViewStyle = {
}

export const BUTTON_CONTAINER_VIEW: ViewStyle = {
    backgroundColor: color.palette.pearl,
    alignItems: 'center',
    paddingTop: metric.ratioHeight(10)
}

export const CHECKBOX_CONTAINER_VIEW: ViewStyle = {
    backgroundColor: color.palette.pearl,
    flexDirection: "row",
    alignItems: 'center',
    paddingHorizontal: metric.ratioWidth(37),
}

export const PRIVACY_CONTAINER_VIEW: ViewStyle = {
    flexDirection: 'row',
}


export const BUTTON: ViewStyle = {
    marginBottom: metric.ratioHeight(39),
}

export const BACKGROUND_STYLE = color.pinkBlueGradient

export const ACTIVE_BACKGROUND_STYLE = color.redFleshGradient

export const MODAL_BACKGROUND: ViewStyle = {
    ...FULL,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'

}

export const MODAL: ViewStyle = {
    backgroundColor: color.palette.darkRed,
    height: metric.ratioHeight(450),
    width: metric.ratioWidth(350),
}

export const MODAL_TITLE_TEXT: TextStyle = {
    fontFamily: "Montserrat-Bold",
    marginLeft: metric.ratioWidth(10)
}

export const MODAL_SUBTITLEBAR: ViewStyle = {
    justifyContent: 'center',
    backgroundColor: color.palette.pinkRed,
    height: metric.ratioHeight(50),
}

export const MODAL_CONTENT: ViewStyle = {
    alignItems: 'center',
    justifyContent: 'center'
}

export const MODAL_BUTTON: ViewStyle = {
    marginTop: metric.ratioHeight(30)
}

export const CHECK: ViewStyle = {
    flexDirection: 'row',
    alignItems:'center'
}

export const ICON_DUDE: ImageStyle = {
    width: metric.ratioWidth(30),
    height: metric.ratioHeight(30),
}
const TEXT: TextStyle = {
    color: color.palette.white,
    fontFamily: "Montserrat",
}
export const PRIVACY_TEXT: TextStyle = {
    color: color.palette.darkGrey,
    fontWeight: '300',
    fontSize: RFValue(13),
    lineHeight: metric.ratioHeight(30),

    //Android
    fontFamily: "Montserrat-Light",
    paddingHorizontal: metric.ratioWidth(10),
}

export const PRIVACY_LINK: TextStyle = {
    color: color.palette.pink,
    fontWeight: '300',
    fontSize: RFValue(13),
    lineHeight: metric.ratioHeight(30),

    //Android
    fontFamily: "Montserrat-Light",

    textDecorationLine: 'underline' 
}

export const TITLE: TextStyle = {
    ...TEXT,
    fontSize: RFValue(14),
    fontWeight: '400',

    //Android
    fontFamily: "Montserrat-Regular",
}


