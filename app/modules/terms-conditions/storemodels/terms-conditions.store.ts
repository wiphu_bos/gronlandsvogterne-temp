import { types } from "mobx-state-tree"
import { TermsAndConditionsActions, TermsAndConditionsViews, TermsAndConditionsPropsModel, TermsAndConditionsResourcesViews } from "../viewmodels"
import { StoreName } from "../../../constants/app.constant"
import { TermsAndConditionsServiceActions } from "../services/terms-conditions.services"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store"

const TermsAndConditionsModel = types.model(StoreName.TermsAndConditions, TermsAndConditionsPropsModel)
const TermsAndConditionsLocalStore = types.compose(TermsAndConditionsModel, TermsAndConditionsActions, TermsAndConditionsViews, TermsAndConditionsServiceActions)
export const TermsAndConditionsStoreModel = TermsAndConditionsLocalStore
export const TermsAndConditionsResourcesStoreModel = types.compose(GeneralResourcesStoreModel, TermsAndConditionsResourcesViews)