import { Instance, SnapshotOut } from "mobx-state-tree"
import { TermsAndConditionsStoreModel, TermsAndConditionsResourcesStoreModel } from "./terms-conditions.store"

export type TermsAndConditionsStore = Instance<typeof TermsAndConditionsStoreModel>
export type TermsAndConditionsStoreSnapshot = SnapshotOut<typeof TermsAndConditionsStoreModel>

export type TermsAndConditionsResourcesStore = Instance<typeof TermsAndConditionsResourcesStoreModel>
export type TermsAndConditionsResourcesStoreSnapshot = SnapshotOut<typeof TermsAndConditionsResourcesStoreModel>