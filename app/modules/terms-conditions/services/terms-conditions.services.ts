
import { TermsAndConditionsPropsModel } from "../viewmodels"
import { types, flow } from "mobx-state-tree"
import { RootStore } from "../../../models/root-store"
import { APIResponse, APISuccessResponse } from "../../../constants/service.types"
import { IUser } from "../../../models/user-store/user.types"
import * as DataUtils from "../../../utils/data.utils"
import { UserType } from "../../../constants/app.constant"

export const TermsAndConditionsServiceActions = types.model(TermsAndConditionsPropsModel).actions(self => {
    const createNewUser = flow(function* (rootStore: RootStore) {
        try {
            const result: APIResponse = yield rootStore.getUserStore?.createUser()
            let success = (result as APISuccessResponse)?.data
            let data: IUser = success?.data
            if (data) {
                const userProfile = DataUtils.getUserProfileObject(rootStore, rootStore?.getUserStore, data)
                rootStore?.getUserStore?.bindingDataFromObject(userProfile, UserType.Me)
            }
            return result
        } catch (e) {
            return (e)
        }
    })
    return { createNewUser }
})