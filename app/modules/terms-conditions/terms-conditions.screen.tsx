// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { TermsAndConditionsProps } from "./terms-conditions.props"
import TermsAndConditionsController from "./terms-conditions.controllers"
import { Text, Wallpaper, AnimatedScrollView, Icon, Button } from "../../components"
import { GradientButton } from "../../components/gradient-button/button"
import { SafeAreaView } from "react-navigation"
import { HeaderComponent } from "../../components/custom-header/custom-header"
import { View, TouchableOpacity, Linking } from "react-native"
import { useConfigurate } from "../../custom-hooks/use-configure-controller"
import * as metric from "../../theme"
import * as StringUtils from "../../utils/string.utils"
// MARK: Style Import

import * as Styles from "./terms-conditions.styles"
import { images } from "../../theme/images"

export const TermsAndConditionsScreen: React.FunctionComponent<TermsAndConditionsProps> = observer((props) => {

    const controller = useConfigurate(TermsAndConditionsController, props) as TermsAndConditionsController
    // MARK: Render
    const yesIconSource: string = TermsAndConditionsController.viewModel.getIsValidate ? images.selected : images.unSelected

    return (
        <Wallpaper showBubble>
            <SafeAreaView
                {...metric.safeAreaViewProps}
                shouldRasterizeIOS
                style={Styles.SAFE_AREA_VIEW}>
                <HeaderComponent onBackButtonPress={controller.backProcess} />
                <View shouldRasterizeIOS style={Styles.TITLE_CONTAINNER}>
                    <Text style={Styles.TITLE_TEXT}
                        {...TermsAndConditionsController.resourcesViewModel?.getTestIDTitle()}
                        text={TermsAndConditionsController.resourcesViewModel?.getResourceTitle()} />
                </View>
                <View shouldRasterizeIOS style={Styles.SUB_TITLE_CONTAINNER}>
                    <Text style={Styles.SUB_TITLE_TEXT}
                        {...TermsAndConditionsController.resourcesViewModel?.getTestIDSubTitle()}
                        text={TermsAndConditionsController.resourcesViewModel?.getResourceSubTitle()} />
                </View>
                <AnimatedScrollView
                    {...metric.solidScrollView}
                    style={Styles.SCROLL_VIEW}
                >
                    <View shouldRasterizeIOS style={Styles.TEXT_CONTAINER_VIEW}>
                        <Text style={Styles.DESCRIPTION_TEXT}
                            {...TermsAndConditionsController.resourcesViewModel?.getTestIDDescription()}
                        >
                            {StringUtils.brToNewLine(TermsAndConditionsController.resourcesViewModel?.getResourceDescription())}
                        </Text>
                    </View>
                </AnimatedScrollView>
                <View shouldRasterizeIOS style={Styles.CHECKBOX_CONTAINER_VIEW}>
                    <Button preset="none" isAnimated={false} isSolid style={Styles.CHECK} onPress={() => controller.onCheckboxDidTouch(!TermsAndConditionsController.viewModel.getIsValidate)} >
                        <Icon source={yesIconSource} style={Styles.ICON_DUDE} />
                    </Button>
                    <View shouldRasterizeIOS style={Styles.PRIVACY_CONTAINER_VIEW}>
                        <Text style={Styles.PRIVACY_TEXT} {...TermsAndConditionsController.resourcesViewModel?.getTestIDPrivacyTitle}>
                            {TermsAndConditionsController.resourcesViewModel?.getResourcePrivacyTitle()} &nbsp;
                            <Text text={TermsAndConditionsController.resourcesViewModel?.getResourcePrivacyLinkTitle()} {...TermsAndConditionsController.resourcesViewModel?.getTestIDPrivacyLinkTitle()} style={Styles.PRIVACY_LINK} onPress={() => controller.openPrivacy()} />
                        </Text>
                    </View>

                </View>

                <View shouldRasterizeIOS style={Styles.BUTTON_CONTAINER_VIEW}>
                    <GradientButton disabled={true} preset={!TermsAndConditionsController.viewModel.getIsValidate ? "disabled" : "none"} text={TermsAndConditionsController.resourcesViewModel?.getResourceButtonAcceptTitle()}
                        {...TermsAndConditionsController.resourcesViewModel?.getTestIDButtonAcceptTitle()} style={Styles.BUTTON}
                        onPress={controller.callNEMID} />
                </View>

            </SafeAreaView>
        </Wallpaper>


    )
})
