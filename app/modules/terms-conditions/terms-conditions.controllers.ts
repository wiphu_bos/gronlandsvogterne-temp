// MARK: Import

import { NavigationKey } from "../../constants/app.constant"
import { TermsAndConditionsProps } from "./terms-conditions.props"
import { RootStore, INavigationRoute } from "../../models"
import { TermsAndConditionsStore, TermsAndConditionsStoreModel, TermsAndConditionsResourcesStore, TermsAndConditionsResourcesStoreModel } from "./storemodels"
import { onSnapshot } from "mobx-state-tree"
import { APIResponse, APISuccessResponse } from "../../constants/service.types"
import { StackActions } from "@react-navigation/native"
import { NEMIDResponse, verifyNEMID } from "../../services/nem/nem.services"
import { GeneralResources } from "../../constants/firebase/remote-config"
import { BaseController, IAlertParams } from "../base.controller"
import CreateAccountController from "../create-account/create-account.controllers"
import { ValidationServices } from "../../services/api-domain/validate.services"
import { Linking } from "react-native"
import { PRIVACY_URL } from "../../constants/web-url/web-url.constant"

class TermsAndConditionsController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static viewModel: TermsAndConditionsStore
    public static resourcesViewModel: TermsAndConditionsResourcesStore

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, myProps?: TermsAndConditionsProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, myProps, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(myProps)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        TermsAndConditionsController.resourcesViewModel = TermsAndConditionsResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.TermsAndConditions)
        TermsAndConditionsController.viewModel = localStore && TermsAndConditionsStoreModel.create({ ...localStore }) || TermsAndConditionsStoreModel.create({})
    }

    private _setupSnapShot = () => {
        onSnapshot(TermsAndConditionsController.viewModel, (snap: TermsAndConditionsStore) => {
            this._setInitializedToPropsParams()
        })
    }

    /*
       Mark Data
   */


    /*
       Mark Event
   */

    public authenticaitonProcess = async () => {
        this._isGlobalLoading(true)
        const result: APIResponse = await TermsAndConditionsController.viewModel?.createNewUser(this._rootStore)
        this._isGlobalLoading(false)
        const success = result as APISuccessResponse
        if (success?.data) {
            const { navigation } = this._myProps
            const params = {
                email: this._rootStore?.getUserStore?.getEmail,
                shouldSendVerifyCode: true
            }
            navigation?.dispatch(StackActions.push(NavigationKey.VerifyUser, params) as any)
        } else {
            const params = {
                rootStore: this._rootStore,
                localViewModel: CreateAccountController.viewModel as any,
                error: result,
                goBackHandler: () => this.backProcess()
            }
            const alertMessage = new ValidationServices(params).generalAPIResponseValidation()
            if (alertMessage) this._generalAlertOS({ message: alertMessage })
        }
    }

    public onCheckboxDidTouch = (value: boolean) => {
        TermsAndConditionsController.viewModel?.setIsValidate(value)
    }

    public openPrivacy = () => {
        Linking.openURL(PRIVACY_URL)
    }

    public callNEMID = async () => {
        if(!TermsAndConditionsController.viewModel?.getIsValidate) return
        const { ErrorMessage } = GeneralResources
        this._isGlobalLoading(true)
        const result: NEMIDResponse = await verifyNEMID()
        this._isGlobalLoading(false)
        if (result?.data) {
            await this.authenticaitonProcess()
        } else {
            const params: IAlertParams = {
                title: this._rootStore.getGeneralResourcesStore(ErrorMessage.verifyNEMIDTitle, true),
                message: this._rootStore.getGeneralResourcesStore(ErrorMessage.verifyNEMID, true)
            }
            this._generalAlertOS(params)
        }
    }


    /*
       Mark Helper
   */
}

export default TermsAndConditionsController