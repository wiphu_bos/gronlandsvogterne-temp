import { types } from "mobx-state-tree"
import { TermsAndConditionsPropsModel } from "./terms-conditions.models"
import { GeneralResources } from "../../../constants/firebase/remote-config"
import { NavigationKey } from "../../../constants/app.constant"
import { TestIDResources } from "../../../constants/test-key/test.constant"
import * as Utils from "../../../utils"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store"

export const TermsAndConditionsViews = types.model(TermsAndConditionsPropsModel).views(self => ({
    get getPassword() {
        return self.password
    },
    get getIsValidate() {
        return self.isValidate
    }
}))

export const TermsAndConditionsResourcesViews = GeneralResourcesStoreModel

    .views(self => {
        //MARK: Volatile State

        const { TermsAndConditionScreen } = GeneralResources
        const { title, subTitle, description, buttonAcceptTitle, privacyTitle, privacyLinkTitle } = TermsAndConditionScreen
        const { TermsAndConditions } = NavigationKey

        //MARK: Views
        const getResources = (key: string, childKeyOrShareKey: string | boolean = false) => self.getValues(childKeyOrShareKey ? key : TermsAndConditions, childKeyOrShareKey ? true : key)
        const getResourceTitle = () => getResources(title)
        const getResourceSubTitle = () => getResources(subTitle)
        const getResourceDescription = () => getResources(description)
        const getResourceButtonAcceptTitle = () => getResources(buttonAcceptTitle, true)
        const getResourcePrivacyTitle = () => getResources(privacyTitle)
        const getResourcePrivacyLinkTitle= () => getResources(privacyLinkTitle)

        return {
            getResourceTitle,
            getResourceSubTitle,
            getResourceDescription,
            getResourceButtonAcceptTitle,
            getResourcePrivacyTitle,
            getResourcePrivacyLinkTitle
        }
    })

    .views(self => {

        //MARK: Volatile State

        //MARK: Views
        const getTestIDTitle = () => Utils.getTestIDObject(TestIDResources.TermsAndConditionScreen.title)
        const getTestIDSubTitle = () => Utils.getTestIDObject(TestIDResources.TermsAndConditionScreen.subTitle)
        const getTestIDDescription = () => Utils.getTestIDObject(TestIDResources.TermsAndConditionScreen.description)
        const getTestIDButtonAcceptTitle = () => Utils.getTestIDObject(TestIDResources.TermsAndConditionScreen.buttonAccept)
        const getTestIDPrivacyTitle = () => Utils.getTestIDObject(TestIDResources.TermsAndConditionScreen.privacyTitle)
        const getTestIDPrivacyLinkTitle = () => Utils.getTestIDObject(TestIDResources.TermsAndConditionScreen.privacyLinkTitle)
        return {
            getTestIDTitle,
            getTestIDSubTitle,
            getTestIDDescription,
            getTestIDButtonAcceptTitle,
            getTestIDPrivacyTitle,
            getTestIDPrivacyLinkTitle
        }
    })