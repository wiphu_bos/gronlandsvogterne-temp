import { TermsAndConditionsPropsModel } from "./terms-conditions.models"
import { types } from "mobx-state-tree"

export const TermsAndConditionsActions = types.model(TermsAndConditionsPropsModel).actions(self => {
    const setPassword = (value: string) => self.password = value
    const setIsValidate = (value: boolean) => self.isValidate = value
    return {
        setPassword,
        setIsValidate
    }
})