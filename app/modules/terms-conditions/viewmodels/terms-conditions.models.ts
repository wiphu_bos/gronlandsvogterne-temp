import { types } from "mobx-state-tree"

export const TermsAndConditionsPropsModel = {
    userId: types.maybeNull(types.string),
    errorMessageUserId: types.maybeNull(types.string),
    password: types.maybeNull(types.string),
    errorMessagePassword: types.maybeNull(types.string),
    isValidate: types.optional(types.boolean,false),
    modalVisible: types.optional(types.boolean,false),
} 