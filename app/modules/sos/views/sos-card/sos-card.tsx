// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { Text, Button } from "../../../../components"
import { View, Alert, TouchableWithoutFeedback, StyleSheet, Image } from "react-native"
import { SOSProps } from "../../sos.props"
import { images } from "../../../../theme/images"
import { Icon } from "../../../../components"

// MARK: Style Import


export const SOSComponents: React.FunctionComponent<SOSProps> = observer((props) => {

    // const title = ATestListController.resourcesViewModel?.getResourceNotificationTitle(props?.notification$?.title)
    // const body = ATestListController.resourcesViewModel?.getResourceNotificationTitle(props?.notification$?.body)
    const title =  props?.notification$?.title
    const body =  props?.notification$?.body

   return (
         <TouchableWithoutFeedback >

             <View style={flatListStyle.cell}>
                    <View style={{ width: '30%', justifyContent: 'center', alignItems: 'center',}}>
                      {/* <Image source = {{ uri: images.hamMenu }} style={flatListStyle.imageView} /> */}
                      <Icon source={images.infomation} style={flatListStyle.imageView} />

                      <Text style={flatListStyle.textBottomImage} >title</Text>
                    </View>
                    <View style={{width: '70%'}}>
                      <Text style={flatListStyle.textView} >{title}</Text>
                      <Text style={flatListStyle.textView} >{body}</Text>
                    </View>
                  </View>



           </TouchableWithoutFeedback>
    )
 })

const flatListStyle = StyleSheet.create({
    flatListView: {
      width: '100%',
      height: '50%' ,
      padding:15,
  
      backgroundColor:'#880000',
    },
  
    cell :{
      flex:1, margin: 5,
      backgroundColor: '#880000', padding : 5,
      height: 100,
      flexDirection:"row",
    },
  
    imageView: {
      width: 30,
      height: 30 ,
      margin: 0,
      padding:0,
      borderRadius : 1000,
      overflow: 'hidden',
    },
  
    textView: {
      width:'90%',
      height: '15%' ,
      textAlign:'center',
      margin: 5,
      padding:0,
      color:'#000000', backgroundColor:'#ffffff',
    },
  
    textBottomImage: {
      width:'90%',
      height: '40%' ,
      textAlign:'center',
      margin: 10,
      padding:0,
      color:'#000000', backgroundColor:'#ffffff',
      fontSize: 8,
    }
  });