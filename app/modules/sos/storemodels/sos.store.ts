import { types } from "mobx-state-tree"
import { StoreName } from "../../../constants/app.constant"
import { SOSPropsModel } from "../viewmodels/sos.models"
import { SOSActions } from "../viewmodels/sos.actions"
import { SOSViews, SOSResourcesViews } from "../viewmodels/sos.views"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store"
import { SOSServiceActions } from "../services/sos.services"

const SOSModel = types.model(StoreName.NotificationList, SOSPropsModel)
export const SOSStoreModel = types.compose(SOSModel, SOSViews, SOSActions, SOSServiceActions)
export const SOSResourcesStoreModel = types.compose(GeneralResourcesStoreModel, SOSResourcesViews)

