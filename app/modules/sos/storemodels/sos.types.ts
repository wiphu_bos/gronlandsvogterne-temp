import { Instance, SnapshotOut } from "mobx-state-tree"
// import { SOSStoreModel, SOSResourcesStoreModel } from "./atest-list.store"
import { SOSStoreModel, SOSResourcesStoreModel } from "./sos.store"
import { INotification } from "../../../models/notification-store"

export type SOSStore = Instance<typeof SOSStoreModel>
export type SOSStoreSnapshot = SnapshotOut<typeof SOSStoreModel>

export type SOSResourcesStore = Instance<typeof SOSResourcesStoreModel>
export type SOSResourcesStoreSnapshot = SnapshotOut<typeof SOSResourcesStoreModel>


export type INotificationPage = {
    page_size?: number,
    is_last_page?: boolean,
    last_id?: string,
}

export type INotificationResponse = {
    notification_list?: INotification[],
    page?: INotificationPage
}