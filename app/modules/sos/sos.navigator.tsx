// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { SOSProps } from "./sos.props"
import { createStackNavigator } from '@react-navigation/stack';
const Stack = createStackNavigator();
import { SOSScreen } from "./sos.screen"
import { View, Text, StyleSheet, Image, TouchableHighlight, TouchableOpacity } from 'react-native'
import { images } from "../../theme/images"
import { Icon } from "../../components"

// MARK: Style Import


export const SOSNavigator: React.FunctionComponent<SOSProps> = observer((props) => {

    return (
      <Stack.Navigator
        initialRouteName="Home"
        headerMode="screen"        
        screenOptions={{
          headerTintColor: 'white',
          headerStyle: { backgroundColor: 'black' },
        }}
      >

      <Stack.Screen name="SOS" 
              component={SOSScreen}
              options={{

                headerLeft: () => (
                  <View style={leftNavigationBar.cell}>
                  <TouchableHighlight style ={{ height: '100%', width: '100%', backgroundColor: 'transparent'}}  
                    onPress={() => alert('Left!')}
                    >
                  <Image source = {images.hamMenu} style={sosStyles.imageStyle}/>
                  </TouchableHighlight>
                  </View>
                ),

              }}
      />

    </Stack.Navigator>

    )
})
  
const leftNavigationBar = StyleSheet.create({
  cell :{
      flex:1, flexDirection: 'row', margin: 5,
      height: 30, width : 30,
      backgroundColor: 'transparent', padding : 5
  },
 
 imageView: {
     flex: 1,
     width: null,
     height: null,
     resizeMode: 'contain'
 },                                   
});

const sosStyles = StyleSheet.create({
   touchableStyle:{
       height: '80%',
       width: '140%',
       marginRight:10,
       marginLeft:0,
       marginTop:0,
       paddingTop:0,
       paddingBottom:0,
       backgroundColor:'#ff0000',
       borderRadius:7,
       borderWidth: 2,
       borderColor: '#ff0000',
       justifyContent: 'center',
   },
   
   imageStyle:{
       flex: 1,
       width: null,
       height: null,
       resizeMode: 'contain',
   },

   textStyle:{
       color:'#fff',
       textAlign:'left',
       paddingLeft : 5,
       paddingRight : 0,
       fontSize: 10,
   },
                                  
});
 
 
  