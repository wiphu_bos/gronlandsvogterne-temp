
import { types, flow, unprotect } from "mobx-state-tree"
import { APIResponse, APISuccessResponse } from "../../../constants/service.types"
import { SOSPropsModel } from "../viewmodels"
import NotificationServices from "../../../services/api-domain/notification.services"
import { INotificationResponse } from "../storemodels/sos.types"
import { RootStore } from "../../../models/root-store/root.types"
import { IUser } from "../../../models/user-store/user.types"
import { Property } from "../../../constants/firebase/fire-store.constant"
import { NotificationStoreModel } from "../../../models/notification-store/notification-store.store"
import * as DataUtils from '../../../utils/data.utils'
import { INotification } from "../../../models/notification-store/notification-store.types"

export const SOSServiceActions = types.model(SOSPropsModel).actions(self => {

    // const findExistingItemIndex = (notification: INotification): number => {
    //     const tempDudeList = self.notificationList$
    //     const existingList = tempDudeList?.slice() //Slice to disobserve property
    //     const foundItemIndex = existingList?.findIndex(v => v?.notification_id === notification?.notification_id)
    //     return foundItemIndex
    // }
    // const fetchNotificationList = flow(function* (rootStore?: RootStore) {
    //     const params = {
    //         [Property.LAST_ID]: self.isFirstLoadDone ? self.last_id : null
    //     }
    //     try {
    //         // const result: APIResponse = yield NotificationServices.getATestList(params)
    //         const result: APIResponse = yield NotificationServices.getNotificationList(params)
    //         const success = (result as APISuccessResponse)?.data
    //         const notificationResult: INotificationResponse = success?.data
    //         const notificationList = notificationResult?.notification_list?.map(e => DataUtils.getNotificationObject(e, false))
    //         const pagingObj = notificationResult?.page
    //         if (notificationList?.length > 0) {
    //             if (!self.isLoadMore) {
    //                 self.notificationList$.forEach(e => e.setIsTimerSet(null, false))
    //                 self.notificationList$.clear()
    //             }
    //             notificationList?.forEach(e => {
    //                 const childViewModel = NotificationStoreModel.create(e)
    //                 childViewModel.setIsTimerSet(rootStore, true)
    //                 unprotect(childViewModel)
    //                 self.notificationList$.push(childViewModel)
    //             })
    //         }
    //         self.last_id = pagingObj?.last_id
    //         self.isLoadDone = pagingObj?.is_last_page
    //         return result
    //     } catch (e) {
    //         return e
    //     } finally {
    //         self.isFirstLoadDone = true
    //         self.isPullRefreshing = false
    //         self.isLoadMore = false
    //     }
    // })

    const fetchDudeProfileDetails = flow(function* (rootStore?: RootStore, dudeId?: string) {
        try {
            let result: APISuccessResponse = yield rootStore?.getUserStore?.fetchDudeProfileDetail(dudeId)
            const data: APISuccessResponse = result?.data
            const profileDetail: IUser = data?.data
            if (!profileDetail) throw data
            return profileDetail
        } catch (error) {
            return error
        }
    })

    // const markReadNotificationByIdWaitResponse = flow(function* (notificationId: string) {
    //     const params: Partial<INotification> = {
    //         notification_id: notificationId
    //     }
    //     const result: APIResponse = yield NotificationServices.markReadNotificationByIdWaitResponse(params)
    //     const success = (result as APISuccessResponse)?.data
    //     const notificationResult: INotification = success?.data
    //     const foundItemIndex = findExistingItemIndex(notificationResult)
    //     if (foundItemIndex === -1) return
    //     // const notificationObject = DataUtils.getNotificationObject(notification, false)
    //     self.notificationList$[foundItemIndex].misc = { ...self.notificationList$[foundItemIndex].misc, is_read: true }
    // })

    return {
        // fetchNotificationList,
        fetchDudeProfileDetails,
        // markReadNotificationByIdWaitResponse
    }
})