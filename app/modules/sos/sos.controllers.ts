// MARK: Import
import { NavigationKey, NotificationTopicKey } from "../../constants/app.constant"
import { SOSProps } from "./sos.props"
import { SOSResourcesStoreModel, SOSStoreModel } from "./storemodels/sos.store"
import { SOSResourcesStore, SOSStore } from "./storemodels/sos.types"
import { RootStore, INavigationRoute, ISharedTimer } from "../../models"
import { HeaderProps } from "../../components/header/header.props"
import { INotification } from "../../models/notification-store"
import { StackActions } from "@react-navigation/core"
import * as DataUtils from "../../utils/data.utils"
import { FirebaseFirestoreTypes } from '@react-native-firebase/firestore'
import { SnapshotChangeType } from "../../constants/firebase/fire-store.constant"
import { onSnapshot, unprotect } from "mobx-state-tree"
import { INotificationListListener } from "../../utils/firebase/fire-store/fire-store.types"
import FirebaseFireStore from "../../utils/firebase/fire-store/fire-store"
import { BaseController } from "../base.controller"
import { IUser } from "../../models/user-store"
import * as metric from "../../theme"
import { IChat } from "../../models/chat-store"

class SOSController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static resourcesViewModel: SOSResourcesStore
    public static viewModel: SOSStore
    private static _disposer: () => void
    private static _isVisited: boolean

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, props?: SOSProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, props, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(props)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        SOSController.resourcesViewModel = SOSResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.SearchListScreen)
        SOSController.viewModel = localStore && SOSStoreModel.create({ ...localStore }) || SOSStoreModel.create({})
        unprotect(SOSController.viewModel)
    }

    private _setupSnapShot = () => {
        onSnapshot(SOSController.viewModel, (snap: SOSStore) => {
            this._setInitializedToPropsParams()
        })
    }

    /*
       Mark Data
   */

    // private _loadCacheData = () => SOSController.viewModel?.setNotificationList(this._rootStore, this._rootStore?.getUserStore?.getNotificationList)

    public flatlistResultAnimationConfig = SOSController._isVisited ? null : metric.flatlistResultAnimationConfig

    // private _fetchNotificationList = async () => await SOSController.viewModel?.fetchNotificationList(this._rootStore)

    private _onSOSener = () => {
        // const onNext: (snapshot: FirebaseFirestoreTypes.QuerySnapshot) => void = (snap => {
        //     if (!SOSController.viewModel?.isFirstLoadDone || snap.metadata.hasPendingWrites) return
        //     snap.docChanges().forEach(change => {
        //         const notification: INotification & ISharedTimer = DataUtils.getNotificationObject(change.doc.data(), true)
        //         if (change.type === SnapshotChangeType.ADDED) {
        //             if (notification) SOSController.viewModel?.addNotificationIntoList(this._rootStore, notification)
        //         }
        //     })
        // })

        const onError: (e: Error) => void = (e => {
        })

        const onComplete: () => void = () => {
        }

        const params: INotificationListListener = {
            userId: this._rootStore?.getUserStore?.getUserId,
            // onNext,
            onError,
            onComplete
        }
        SOSController._disposer = FirebaseFireStore.onNotificationListener(params)
    }

    /*
       Mark Event
   */

    // private _isLoading = (value: boolean) => SOSController.viewModel?.setIsPullRefreshing(value)

    public onRefresh = async () => {
        // this._isLoading(true)
        SOSController.viewModel?.resetFetching()
        // await this._fetchNotificationList()
        // this._isLoading(false)
    }

    public getHeaderLeftOptions: HeaderProps = {
        onLeftPress: this.backProcess
    }

    // public onLoadMore = async () => {
    //     if (!SOSController.viewModel?.getShouldLoadmore) return
    //     SOSController.viewModel?.setIsLoadMore(true)
    //     await this._fetchNotificationList()
    // }

    /*
    
    Will be refactored later
    
    */
    // public onNotificationItemPress = async (item?: INotification) => {
    //     const notificationId = item?.notification_id
    //     const isRead: boolean = item?.misc?.is_read
    //     let actions: any[]
    //     if (item?.topic?.includes(NotificationTopicKey.CreatedDude) || item?.topic?.includes(NotificationTopicKey.PublishDude)) {
    //         const dudeId = item?.data?.uid
    //         if (!dudeId) return
    //         this._isGlobalLoading(true)
    //         const dudeProfile: IUser = await SOSController.viewModel?.fetchDudeProfileDetails(this._rootStore, dudeId)
    //         this._isGlobalLoading(false)
    //         if (!dudeProfile) return
    //         const dudeProfileObject = DataUtils.getUserProfileObject(this._rootStore, null, dudeProfile)
    //         actions = [StackActions.push(NavigationKey.CreateEditProfileTab, { dudeProfileObject: dudeProfileObject })]

    //     } else if (item?.topic?.includes(NotificationTopicKey.Comment)) {
    //         const dudeId = item?.data?.dudeProfile?.uid
    //         if (!dudeId) return
    //         this._isGlobalLoading(true)
    //         const dudeProfile: IUser = await SOSController.viewModel?.fetchDudeProfileDetails(this._rootStore, dudeId)
    //         this._isGlobalLoading(false)
    //         if (!dudeProfile) return
    //         const dudeProfileObject = DataUtils.getUserProfileObject(this._rootStore, null, dudeProfile)
    //         actions = [StackActions.push(NavigationKey.DudeProfileDetailTab, {
    //             dudeProfileObject: dudeProfileObject,
    //             shouldGoToCommentTab: true
    //         })]
    //     } else if (item?.topic?.includes(NotificationTopicKey.CreateMessage)) {
    //         const chatRoomObject = { ...item.data } as IChat //spread to IChat to dis-observe
    //         if (!chatRoomObject) return
    //         actions = [StackActions.push(NavigationKey.ChatRoom, { chatRoomObject: chatRoomObject, shouldDelayFetching: true })]
    //     }

    //     Promise.all(actions.map(e => this._myProps?.navigation?.dispatch(e)))
    //     this._rootStore?.getUserStore?.setReadNotificationById(item?.notification_id)
    //     !isRead && await SOSController.viewModel?.markReadNotificationByIdWaitResponse(notificationId)
    // }

    /*
       Mark Life cycle
   */

    //@override
    viewDidAppearOnce = async () => {
        this._onSOSener()
        // this._loadCacheData()
    }
    //@override
    viewDidAppearAfterFocus = async () => {
        super.viewDidAppearAfterFocus && super.viewDidAppearAfterFocus()
        // const isConnected = this._rootStore?.getSharedStore?.getIsConnected
        // if (isConnected) {
        //     if (SOSController.viewModel?.getNotificationList?.length === 0) {
        //         await this.onRefresh()
        //     } else if (!SOSController.viewModel?.getIsFirstLoadDone) {
        //         this._isLoading(true)
        //         await this._fetchNotificationList()
        //         this._isLoading(false)
        //     }
        // } else {
        //     SOSController.viewModel?.setIsFirstLoadDone(true)
        // }
        // if (!SOSController._isVisited) {
        //     SOSController._isVisited = true
        // }
    }

    //@override
    deInit = () => {
        super.deInit && super.deInit()
        SOSController._disposer && SOSController._disposer()
        // SOSController.viewModel?.invalidateAllTimerIfNeeded()
    }
}

export default SOSController
