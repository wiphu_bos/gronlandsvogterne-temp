
import { types, flow, unprotect } from "mobx-state-tree"
import { APIResponse, APISuccessResponse } from "../../../constants/service.types"
import { MenuPropsModel } from "../viewmodels"
import { RootStore } from "../../../models/root-store/root.types"
import { IUser } from "../../../models/user-store/user.types"

export const MenuServiceActions = types.model(MenuPropsModel).actions(self => {


    const fetchDudeProfileDetails = flow(function* (rootStore?: RootStore, dudeId?: string) {
        try {
            let result: APISuccessResponse = yield rootStore?.getUserStore?.fetchDudeProfileDetail(dudeId)
            const data: APISuccessResponse = result?.data
            const profileDetail: IUser = data?.data
            if (!profileDetail) throw data
            return profileDetail
        } catch (error) {
            return error
        }
    })


    return {
        fetchDudeProfileDetails,
    }
})