// MARK: Import
import { NavigationKey, NotificationTopicKey } from "../../constants/app.constant"
import { MenuProps } from "./menu.props"
import { MenuResourcesStoreModel, MenuStoreModel } from "./storemodels/menu.store"
import { MenuResourcesStore, MenuStore } from "./storemodels/menu.types"
import { RootStore, INavigationRoute, ISharedTimer } from "../../models"
import { HeaderProps } from "../../components/header/header.props"
import { INotification } from "../../models/notification-store"
import { StackActions } from "@react-navigation/core"
import * as DataUtils from "../../utils/data.utils"
import { FirebaseFirestoreTypes } from '@react-native-firebase/firestore'
import { SnapshotChangeType } from "../../constants/firebase/fire-store.constant"
import { onSnapshot, unprotect } from "mobx-state-tree"
import { INotificationListListener } from "../../utils/firebase/fire-store/fire-store.types"
import FirebaseFireStore from "../../utils/firebase/fire-store/fire-store"
import { BaseController } from "../base.controller"
import { IUser } from "../../models/user-store"
import * as metric from "../../theme"
import { IChat } from "../../models/chat-store"
import { Alert } from "react-native"

class MenuController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static resourcesViewModel: MenuResourcesStore
    public static viewModel: MenuStore
    private static _disposer: () => void
    private static _isVisited: boolean
    private static nm: string = 'jon'

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, props?: MenuProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, props, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(props)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        MenuController.resourcesViewModel = MenuResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.SearchListScreen)
        MenuController.viewModel = localStore && MenuStoreModel.create({ ...localStore }) || MenuStoreModel.create({})
        unprotect(MenuController.viewModel)
    }

    private _setupSnapShot = () => {
        onSnapshot(MenuController.viewModel, (snap: MenuStore) => {
            this._setInitializedToPropsParams()
        })
    }

    /*
       Mark Data
   */

    // private _loadCacheData = () => MenuController.viewModel?.setNotificationList(this._rootStore, this._rootStore?.getUserStore?.getNotificationList)

    public flatlistResultAnimationConfig = MenuController._isVisited ? null : metric.flatlistResultAnimationConfig

 
    /*
       Mark Event
   */
  public onFooterClick = () => {
        console.log('onTogglePopoverMenu')
        // alert('hello')
        const viewModel = MenuController.viewModel
        viewModel.setLanguage()
    }

    public onLangClick = () => {
    }


    public onRefresh = async () => {
    }

    public getHeaderLeftOptions: HeaderProps = {
        onLeftPress: this.backProcess
    }

    public onLoadMore = async () => {
        // if (!MenuController.viewModel?.getShouldLoadmore) return
        // MenuController.viewModel?.setIsLoadMore(true)
        // await this._fetchNotificationList()
    }
    /*
    
    Will be refactored later
    
    */

    /*
       Mark Life cycle
   */

    //@override
    viewDidAppearOnce = async () => {
        // this._onMenuener()
        // this._loadCacheData()
    }
    //@override
    viewDidAppearAfterFocus = async () => {
        super.viewDidAppearAfterFocus && super.viewDidAppearAfterFocus()
    }

    //@override
    deInit = () => {
        super.deInit && super.deInit()
        MenuController._disposer && MenuController._disposer()
    }
}

export default MenuController
