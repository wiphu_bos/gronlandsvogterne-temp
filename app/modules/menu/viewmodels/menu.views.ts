import { types } from "mobx-state-tree"
import { MenuPropsModel } from "./menu.models"
import { GeneralResources, RemoteConfigGroupedKey } from "../../../constants/firebase/remote-config"
import { NavigationKey } from "../../../constants/app.constant"
import { TestIDResources } from "../../../constants/test-key/test.constant"
import * as Utils from "../../../utils"
import * as DataUtils from "../../../utils/data.utils"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store"

export const MenuViews = types.model(MenuPropsModel)
    .views(self => ({
        get getNotificationList() {
            var rv = [];

            rv.push( { title: 'MINE INDDERETINIGER', body: 'MINE INDDERETINIGER'  })
            rv.push( { title: 'INDSTILLINER', body: 'INDSTILLINER'  })
            rv.push( { title: 'SYNKRONISER', body: 'SYNKRONISER'  })
            rv.push( { title: 'PROFIL', body: 'PROFIL'  })
            rv.push( { title: 'INFO', body: 'INFO'  })

            return rv
        },
        get getName(): string {
            return(self.profile_name)
        },
        get getLanguage(): string {
            return( self.profile_language )
        },

        get getShouldLoadmore(): boolean {
            return !self.isLoadDone && !self.isLoadMore && self.notificationList$.length > 0 && self.isFirstLoadDone
        },
        get getIsFirstLoadDone(): boolean {
            return self.isFirstLoadDone
        },
        get getIsPullRefreshing(): boolean {
            return self.isPullRefreshing
        },
    }))

export const MenuResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State

    const { NotificationScreen } = GeneralResources
    const { screenTitle } = NotificationScreen
    const { NotificationList } = NavigationKey
    RemoteConfigGroupedKey
    //MARK: Views
    const getResources = (key: string, childKeyOrShareKey: string | boolean = false) => self.getValues(childKeyOrShareKey ? key : NotificationList, childKeyOrShareKey ? true : key)
    const getResourceScreenTitle = () => getResources(screenTitle)
    const getResourceNotificationTitle = (value: string) => DataUtils.getNotificationByLocaleKey(self, value, RemoteConfigGroupedKey.NotificationTitle)
    const getResourceNotificationBody = (value: string) => DataUtils.getNotificationByLocaleKey(self, value, RemoteConfigGroupedKey.NotificationBody)
    return {
        getResourceScreenTitle,
        getResourceNotificationTitle,
        getResourceNotificationBody
    }
})
    .views(self => {
        const getTestIDTitle = () => Utils.getTestIDObject(TestIDResources.NotificationListScreen.title)
        const getTestIDDescription = () => Utils.getTestIDObject(TestIDResources.NotificationListScreen.description)
        const getTestIDTimeAgo = () => Utils.getTestIDObject(TestIDResources.NotificationListScreen.timeAgo)
        return {
            getTestIDTitle,
            getTestIDDescription,
            getTestIDTimeAgo,
        }
    })
