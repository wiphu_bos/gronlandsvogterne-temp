import { types, unprotect } from "mobx-state-tree"
import { MenuPropsModel } from "./menu.models"

export const MenuActions = types.model(MenuPropsModel).actions(self => {

    const setLanguage = () => {
        self.profile_language = 'English'
    }

    return {
        setLanguage,
    }
})