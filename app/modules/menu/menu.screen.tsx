// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { MenuProps } from "./menu.props"
import MenuController from "./menu.controllers"
import { useConfigurate } from "../../custom-hooks/use-configure-controller"
import { Wallpaper, Header, Text, Button } from "../../components"
import { View, RefreshControl, StyleSheet, TouchableOpacity, Image, TouchableWithoutFeedback, Alert  } from "react-native"
import { FlatList } from "react-native-gesture-handler"
import { MenuComponents } from "./views/menu-card/menu-card"
import { MenuFooterComponents } from "./views/menu-card/menu-footer-card"
import { SafeAreaView } from "react-navigation"
import * as metric from "../../theme"
import { images } from "../../theme/images"
import { Icon } from "../../components"
import { useStores } from "../../models/root-store"

// MARK: Style Import

import * as Styles from "./menu.styles"

export const MenuScreen: React.FunctionComponent<MenuProps> = observer((props) => {

    const controller = useConfigurate(MenuController, props) as MenuController
    const rootStore = useStores()

    const changeFlag = () => {
      const v = rootStore.MenuStore.getLang
      if ( v == 'Danish' ){
        rootStore.MenuStore.setLang('English')
      } else {
        rootStore.MenuStore.setLang('Danish')
      }
  }
  
    // MARK: Render
    return (
        <Wallpaper showBubble>
            <SafeAreaView
                {...metric.safeAreaViewProps}
                shouldRasterizeIOS
                style={Styles.SAFE_AREA_VIEW}>
                <View shouldRasterizeIOS style={Styles.BODY_CONTAINER}>
                  <View style={{flex: 0.1, backgroundColor: 'red', justifyContent: 'center', alignItems: 'center'}}>
                      {/* <Text>  {rootStore.AuthStore.getName}</Text>
                      <Button
                        style={flatListStyle.test}
                        preset="link" onPress={changeFlag} 
                        text='hello'
                        /> */}
                  {/* <Text style={flatListStyle.textView}> { MenuController.viewModel.getName} </Text> */}
                  <Text style={flatListStyle.textView}> { rootStore.MenuStore.getName} </Text>

                  </View>

                  <View style={{flex: 0.5, backgroundColor: 'cyan', padding : 10}}>

                      <FlatList
                          contentContainerStyle={Styles.LIST_VIEW}
                          refreshControl={
                              <RefreshControl
                                  style={Styles.REFRESH_CONTROL}
                                  // refreshing={ChatListController.viewModel?.getIsPullRefreshing}
                                  onRefresh={controller.onRefresh}
                              />
                          }
                          showsVerticalScrollIndicator={false}
                          ItemSeparatorComponent={({ highlighted }) => (
                              <View shouldRasterizeIOS style={Styles.SEPARATOR_VIEW} />
                          )}
                          // extraData={ChatListController.viewModel?.getChatList?.slice()}
                          data={MenuController.viewModel?.getNotificationList}
                          renderItem={({ item, index, separators }) => (
                              <MenuComponents
                              menudata$={item}
                                  onPress={changeFlag}  />
                                  // onPress={controller.onFooterClick}  />
                          )}
                          onEndReachedThreshold={0.4}
                          onEndReached={controller.onLoadMore}
                      />

                  </View>

                  <View style={{flex: 0.1, backgroundColor: 'black', justifyContent: 'center'}}>
                          <TouchableOpacity
                                  style={ {width: '100%', height: '50%'} } 
                                  onPress={changeFlag}>
                              <Text style={flatListStyle.textView}> { rootStore.MenuStore.getLang} </Text>
                              {/* <Text style={flatListStyle.textView}> hello </Text> */}
                          </TouchableOpacity>
                  </View>

                  <View style={{flex: 0.22, backgroundColor: 'red'}}>
                  </View>

                  <View style={{flex: 0.08, backgroundColor: 'red'}}>
                    <MenuFooterComponents
                    /> 
                  </View>

                </View>

            </SafeAreaView>
        </Wallpaper>

    )
})


  
  const flatListStyle = StyleSheet.create({
    flatListView: {
      width: '100%',
      height: '50%' ,
      padding:15,
  
      backgroundColor:'#ffffff',
    },
    cell :{
      flex:1, 
      margin: 0,
      backgroundColor: '#000000', 
      paddingLeft : 30,
      paddingRight : 30,
      paddingTop : 10,
      paddingBottom : 10,
      height: 35,
      flexDirection:"row",
    },
  
    imageView: {
      width: 20,
      height: 20 ,
      margin: 0,
      padding:0,
      borderRadius : 1000,
      overflow: 'hidden',
    },
    test: {
      width: 20,
      height: 20 ,
      margin: 10,
      padding:0,
      borderRadius : 0,
      backgroundColor:'#000000',
    },

    textView: {
      textAlign:'left',
      color:'#000000', backgroundColor:'#ffffff',
      fontSize: 14,
      alignItems: "center",
    },
  });