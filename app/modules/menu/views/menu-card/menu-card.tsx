// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { Text, Button } from "../../../../components"
import { View, Alert, StyleSheet, TouchableWithoutFeedback } from "react-native"
import { MenuProps } from "../../menu.props"
import { images } from "../../../../theme/images"
import { Icon } from "../../../../components"

// MARK: Style Import


export const MenuComponents: React.FunctionComponent<MenuProps> = observer((props) => {

    const title =  props?.menudata$?.title
    const body =  props?.menudata$?.body

   return (
      <TouchableWithoutFeedback  onPress={() => props?.onPress(props?.menudata$)} >
          <View style={flatListStyle.main}>
          <View style={flatListStyle.cell}>
          <View style={{ width: '30%', justifyContent: 'center', alignItems: 'center',}}>
          <Icon source={images.infomation} style={flatListStyle.imageView} />
          </View>
          <View style={{width: '70%', justifyContent: 'center'}}>
          <Text style={flatListStyle.textView} > {title}</Text>
          </View>
          </View>
          </View>

      </TouchableWithoutFeedback>
    )
 })

const flatListStyle = StyleSheet.create({
    main :{
      flex:1, 
      margin: 0,
      width: '100%',
      backgroundColor: 'transparent', 
      height: 50,
      alignItems: "center",
    },

    cell :{
      margin: 0,
      height: '100%',
      width: '80%',
      backgroundColor: '#880000', 
      padding : 5,
      flexDirection:"row",
      justifyContent: 'center',

    },
  
    imageView: {
      width: 20,
      height: 20 ,
      margin: 0,
      padding:0,
      borderRadius : 1000,
      overflow: 'hidden',
    },
  
    textView: {
      width:'100%',
      // height: '100%' ,
      textAlign:'left',
      margin: 0,
      padding:0,
      color:'#ff0000', backgroundColor:'#ffffff',
    },
  
  });