// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { Text, Button } from "../../../../components"
import { View, Alert, TouchableOpacity, StyleSheet, Image } from "react-native"
import { MenuProps } from "../../menu.props"
import { images } from "../../../../theme/images"
import { Icon } from "../../../../components"
import { useConfigurate } from "../../../../custom-hooks/use-configure-controller"
import MenuController from "../../menu.controllers"

// MARK: Style Import


export const MenuFooterComponents: React.FunctionComponent<MenuProps> = observer((props) => {
  const controller = useConfigurate(MenuController, props) as MenuController
        return (
              <View style={styles.MainContainer}>
              <TouchableOpacity
                      style={footerStyle.touchableStyle} 
                      onPress={controller?.onFooterClick}>
                    <View style={{flexDirection:"row",justifyContent: 'center'}}>
                        <View style={{width: '45%',justifyContent: 'center' }}>
                          <Text style={footerStyle.textStyle}>LOG UID</Text>
                        </View>
                        <View style={{ width: '20%'}}>
                          <Icon source = {images.infomation} style={footerStyle.footerImage}/>
                        </View>
                    </View>
              </TouchableOpacity>
              </View>
          )
})

 const styles = StyleSheet.create({
  MainContainer :{
      alignItems:'center',
      justifyContent:'center',
      padding:5,
      flex: 1,    

  },                                                                   
});

 const footerStyle = StyleSheet.create({
  touchableStyle:{
    height: '50%',
    width: '50%',
    backgroundColor:'#1E6738',
    borderRadius:2,
    borderWidth: 2,
    borderColor: '#ff0000',
    justifyContent: 'center', alignItems: 'center',
    flex: 1,
  },

  footerImage: {
    marginLeft:0,
    width: 25,
    height: 25,
    borderRadius:10000,
    borderWidth: 2,
    borderColor: '#ff0000',
    resizeMode: 'contain',
  },

  textStyle:{
       color:'#fff',
       backgroundColor:'black',
       textAlign: 'right',
       paddingRight:5,
  },
});