import { types } from "mobx-state-tree"
import { StoreName } from "../../../constants/app.constant"
import { MenuPropsModel } from "../viewmodels/menu.models"
import { MenuActions } from "../viewmodels/menu.actions"
import { MenuViews, MenuResourcesViews } from "../viewmodels/menu.views"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store"
import { MenuServiceActions } from "../services/menu.services"

const MenuModel = types.model(StoreName.NotificationList, MenuPropsModel)
export const MenuStoreModel = types.compose(MenuModel, MenuViews, MenuActions, MenuServiceActions)
export const MenuResourcesStoreModel = types.compose(GeneralResourcesStoreModel, MenuResourcesViews)

