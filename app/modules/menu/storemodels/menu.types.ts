import { Instance, SnapshotOut } from "mobx-state-tree"
import { MenuStoreModel, MenuResourcesStoreModel } from "./menu.store"
import { INotification } from "../../../models/notification-store"

export type MenuStore = Instance<typeof MenuStoreModel>
export type MenuStoreSnapshot = SnapshotOut<typeof MenuStoreModel>

export type MenuResourcesStore = Instance<typeof MenuResourcesStoreModel>
export type MenuResourcesStoreSnapshot = SnapshotOut<typeof MenuResourcesStoreModel>


export type INotificationPage = {
    page_size?: number,
    is_last_page?: boolean,
    last_id?: string,
}

export type INotificationResponse = {
    notification_list?: INotification[],
    page?: INotificationPage
}