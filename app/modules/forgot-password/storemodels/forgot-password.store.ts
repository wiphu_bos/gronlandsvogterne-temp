import { types } from "mobx-state-tree"
import { StoreName } from "../../../constants/app.constant"
import { ForgotPasswordServiceActions } from "../services/forgot-password.services"
import { ForgotPasswordPropsModel } from "../viewmodels/forgot-password.models"
import { ForgotPasswordActions } from "../viewmodels/forgot-password.actions"
import { ForgotPasswordViews, ForgotPasswordResourcesViews } from "../viewmodels/forgot-password.views"
import { UserStoreModel } from "../../../models/user-store"
import { ValidationStoreModel } from "../../../models/validation-store"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store"


const ForgotPasswordModel = types.model(StoreName.ForgotPassword, ForgotPasswordPropsModel)

export const ForgotPasswordStoreModel = types.compose(
    ForgotPasswordModel,
    UserStoreModel,
    ValidationStoreModel,
    ForgotPasswordViews,
    ForgotPasswordActions,
    ForgotPasswordServiceActions)
export const ForgotPasswordResourcesStoreModel = types.compose(GeneralResourcesStoreModel, ForgotPasswordResourcesViews)