import { Instance, SnapshotOut } from "mobx-state-tree"
import { ForgotPasswordStoreModel, ForgotPasswordResourcesStoreModel } from "./forgot-password.store"

export type ForgotPasswordStore = Instance<typeof ForgotPasswordStoreModel>
export type ForgotPasswordStoreSnapshot = SnapshotOut<typeof ForgotPasswordStoreModel>

export type ForgotPasswordResourcesStore = Instance<typeof ForgotPasswordResourcesStoreModel>
export type ForgotPasswordResourcesStoreSnapshot = SnapshotOut<typeof ForgotPasswordResourcesStoreModel>
