import { types, flow } from "mobx-state-tree"
import { ForgotPasswordPropsModel } from "../viewmodels/forgot-password.models"
import { RootStore } from "../../../models"

export const ForgotPasswordServiceActions = types.model(ForgotPasswordPropsModel).actions(self => {
    const sendPasswordResetEmail = flow(function* (rootStore: RootStore) {
        try {
            return yield rootStore.getUserStore?.sendPasswordResetEmail()
        } catch (e) {
            return (e)
        }
    })
    return { sendPasswordResetEmail }
})