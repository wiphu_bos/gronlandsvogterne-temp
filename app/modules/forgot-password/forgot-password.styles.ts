import { ViewStyle, TextStyle } from "react-native"
import * as metric from "../../theme"
import { TEXT } from "../../components/appname-slogan/appname-slogan.styles"
import { RFValue } from "react-native-responsive-fontsize"

export const FULL: ViewStyle = {
    flex: 1,
}

export const SAFE_AREA_VIEW: ViewStyle = {
    ...FULL,
}

export const AVOID_KEYBOARD_VIEW: ViewStyle = {
    ...FULL,
}


export const SCROLL_VIEW: ViewStyle = {
    paddingBottom: metric.ratioHeight(10),
    alignItems: "center"
}


export const BUTTON_MARGIN: ViewStyle = {
    marginTop: metric.ratioHeight(50),
}

export const SUBTITLE_VIEW: ViewStyle = {
    marginVertical: metric.ratioHeight(56),
    marginHorizontal: metric.ratioWidth(56),
}

export const TEXT_CONTAINER: ViewStyle = {
    marginTop: metric.ratioHeight(133)
}

export const FONT_REGULAR: TextStyle = {
    //Android
    fontFamily: "Montserrat-Regular",
}

export const TITLE: TextStyle = {
    ...TEXT,
    ...FONT_REGULAR,
    fontSize: RFValue(25),
    fontWeight: '400',
}

export const SUB_TITLE: TextStyle = {
    ...TEXT,
    ...FONT_REGULAR,
    fontSize: RFValue(17),
    fontWeight: '400',
    lineHeight: metric.ratioHeight(30),
}