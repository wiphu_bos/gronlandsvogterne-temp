// MARK: Import
import { Keyboard } from "react-native"
import { NavigationKey } from "../../constants/app.constant"
import { RootStore, INavigationRoute } from "../../models"
import { onSnapshot } from "mobx-state-tree"
import { ForgotPasswordProps } from "./forgot-password.props"
import { ForgotPasswordStore, ForgotPasswordResourcesStore } from "./storemodels/forgot-password.types"
import { ForgotPasswordStoreModel, ForgotPasswordResourcesStoreModel } from "./storemodels/forgot-password.store"
import * as Validate from "../../utils/local-validate"
import { BaseController } from "../base.controller"
import { ValidationServices } from "../../services/api-domain/validate.services"

class ForgotPasswordController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static viewModel: ForgotPasswordStore
    public static resourcesViewModel: ForgotPasswordResourcesStore

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, myProps?: ForgotPasswordProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, myProps, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(myProps)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        ForgotPasswordController.resourcesViewModel = ForgotPasswordResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.ForgotPassword)
        ForgotPasswordController.viewModel = localStore && ForgotPasswordStoreModel.create({ ...localStore }) || ForgotPasswordStoreModel.create({})
    }

    private _setupSnapShot = () => {
        onSnapshot(ForgotPasswordController.viewModel, (snap: ForgotPasswordStore) => {
            this._setInitializedToPropsParams()
        })
    }

    /*
       Mark Data
   */


    /*
       Mark Event
   */

    private _isLoading = (value: boolean) => this._rootStore?.getSharedStore?.setIsLoading(value)

    public onEmailChangeText = (value: string) => {
        const params = {
            value: value,
            localViewModel: ForgotPasswordController.viewModel as any,
            globalViewModel: this._rootStore?.getUserStore
        }
        ForgotPasswordController.viewModel.setEmail(value)
        Validate.onEmailChangeText(params)
    }

    public validateData = () => {
        this.onBlurEmailTextField()
    }

    public onBlurEmailTextField = () => {
        const email = ForgotPasswordController.viewModel?.getEmail
        const params = {
            value: email,
            localViewModel: ForgotPasswordController.viewModel as any,
            rootStore: this._rootStore
        }
        Validate.onBlurEmailTextField(params)
    }

    public onSubmitButtonDidTouch = async () => {
        Keyboard.dismiss()
        this._checkEmptyData()
        this.validateData()
        if (!ForgotPasswordController.viewModel?.getIsValid) return
        this._isLoading(true)
        const error = await ForgotPasswordController.viewModel?.sendPasswordResetEmail(this._rootStore)
        this._isLoading(false)
        if (error.code) {
            const params = {
                rootStore: this._rootStore,
                localViewModel: ForgotPasswordController.viewModel as any,
                error: error
            }
            const alertMessage = new ValidationServices(params).generalAPIResponseValidation()
            if (alertMessage) this._generalAlertOS({ message: alertMessage })
        } else {
            const message: string = ForgotPasswordController.resourcesViewModel?.getResourceAlertEmailSentTitle()
            this._generalAlertOS({ message: message })
        }
    }

    /*
       Mark Helper
   */

    private _checkEmptyData = () => {
        const { getEmail } = ForgotPasswordController.viewModel
        ForgotPasswordController.viewModel?.setIsValid(getEmail ? true : false)
    }
}

export default ForgotPasswordController