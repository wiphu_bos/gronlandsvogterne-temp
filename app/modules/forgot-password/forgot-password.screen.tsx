// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import ForgotPasswordController from "./forgot-password.controllers"
import { View, Text, KeyboardAvoidingView } from "react-native"
import * as metric from "../../theme"
import { GradientButton } from "../../components/gradient-button/button"
import FloatLabelTextInput from '../../libs/float-label-textfield/float-label-text-field'
import { SafeAreaView } from "react-navigation"
import { ForgotPasswordProps } from "./forgot-password.props"
import { HeaderComponent } from "../../components/custom-header/custom-header"
import { Wallpaper, AnimatedScrollView } from "../../components"
import { useConfigurate } from "../../custom-hooks/use-configure-controller"

// MARK: Style Import

import * as Styles from "./forgot-password.styles"


export const ForgotPasswordScreen: React.FunctionComponent<ForgotPasswordProps> = observer((props) => {

    const controller = useConfigurate(ForgotPasswordController, props) as ForgotPasswordController

    // MARK: Render
    return (
        <Wallpaper showBubble>
            <SafeAreaView shouldRasterizeIOS style={Styles.SAFE_AREA_VIEW}>
                <HeaderComponent onBackButtonPress={controller.backProcess} />
                <KeyboardAvoidingView behavior={metric.keyboardBehavior} style={Styles.AVOID_KEYBOARD_VIEW}>
                    <AnimatedScrollView
                        contentContainerStyle={Styles.SCROLL_VIEW}
                        {...metric.solidScrollView}>

                        <View shouldRasterizeIOS style={Styles.TEXT_CONTAINER}>
                            <Text style={Styles.TITLE}
                                {...ForgotPasswordController.resourcesViewModel?.getTestIDTitle()}>{ForgotPasswordController.resourcesViewModel?.getResourceTitle()}</Text>
                            <View shouldRasterizeIOS style={Styles.SUBTITLE_VIEW}>
                                <Text style={Styles.SUB_TITLE}
                                    {...ForgotPasswordController.resourcesViewModel?.getTestIDSubTitle()}>{ForgotPasswordController.resourcesViewModel?.getResourceSubTitle()}</Text>
                            </View>
                        </View>
                        <FloatLabelTextInput
                            {...ForgotPasswordController.resourcesViewModel?.getTestIDEmailPlaceholder()}
                            errorMessage={ForgotPasswordController.viewModel?.getEmailErrorMessage}
                            testIDErrorMessage={ForgotPasswordController.resourcesViewModel?.getTestIDErrorMessage()}
                            placeholder={ForgotPasswordController.resourcesViewModel?.getResourceEmailPlaceholder()}
                            onChangeTextValue={controller.onEmailChangeText}
                            onBlur={controller.onBlurEmailTextField}
                            returnKeyType="send"
                            keyboardType="email-address"
                            onSubmitEditing={controller.onSubmitButtonDidTouch}
                            noBorder
                        />
                        <GradientButton /*{localViewModel?.getIsValid ? "primary" : "disabled"}*/
                            text={ForgotPasswordController.resourcesViewModel?.getResourceButtonSubmitTitle()}
                            {...ForgotPasswordController.resourcesViewModel?.getResourceButtonSubmitTitle()}
                            containerStyle={Styles.BUTTON_MARGIN} onPress={controller.onSubmitButtonDidTouch} />

                    </AnimatedScrollView>
                </KeyboardAvoidingView>
            </SafeAreaView>
        </Wallpaper>
    )
})
