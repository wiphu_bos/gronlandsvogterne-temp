import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"

export interface ForgotPasswordProps extends NavigationContainerProps<ParamListBase> {
}