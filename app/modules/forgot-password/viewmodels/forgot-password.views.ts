import { types } from "mobx-state-tree"
import { ForgotPasswordPropsModel } from "./forgot-password.models"
import { GeneralResources } from "../../../constants/firebase/remote-config"
import { NavigationKey } from "../../../constants/app.constant"
import * as Utils from "../../../utils"
import { TestIDResources } from "../../../constants/test-key/test.constant"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store"

export const ForgotPasswordViews = types.model(ForgotPasswordPropsModel)
    .views(self => ({
        get getExample() {
            return
        }
    }))

export const ForgotPasswordResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State

    const { ForgotPasswordScreen } = GeneralResources
    const { title, subTitle, emailPlaceholder, buttonSubmitTitle, alertEmailSentTitle } = ForgotPasswordScreen
    const { ForgotPassword } = NavigationKey

    //MARK: Views
    const getResources = (key: string, childKeyOrShareKey: string | boolean = false) => self.getValues(childKeyOrShareKey ? key : ForgotPassword, childKeyOrShareKey ? true : key)
    const getResourceTitle = () => getResources(title)
    const getResourceSubTitle = () => getResources(subTitle)
    const getResourceButtonSubmitTitle = () => getResources(buttonSubmitTitle, true)
    const getResourceAlertEmailSentTitle = () => getResources(alertEmailSentTitle, true)
    const getResourceEmailPlaceholder = () => getResources(emailPlaceholder)

    return {
        getResourceTitle,
        getResourceSubTitle,
        getResourceEmailPlaceholder,
        getResourceAlertEmailSentTitle,
        getResourceButtonSubmitTitle
    }
})
    .views(self => {

        //MARK: Volatile State

        //MARK: Views

        const getTestIDTitle = () => Utils.getTestIDObject(TestIDResources.ForgotPasswordScreen.title)
        const getTestIDSubTitle = () => Utils.getTestIDObject(TestIDResources.ForgotPasswordScreen.subTitle)
        const getTestIDEmailPlaceholder = () => Utils.getTestIDObject(TestIDResources.ForgotPasswordScreen.emailPlaceholder)
        const getTestIDButtonSubmitTitle = () => Utils.getTestIDObject(TestIDResources.ForgotPasswordScreen.buttonSubmit)
        const getTestIDErrorMessage = () => Utils.getTestIDObject(TestIDResources.ForgotPasswordScreen.errorMessage)
        return {
            getTestIDTitle,
            getTestIDSubTitle,
            getTestIDEmailPlaceholder,
            getTestIDButtonSubmitTitle,
            getTestIDErrorMessage
        }
    })
