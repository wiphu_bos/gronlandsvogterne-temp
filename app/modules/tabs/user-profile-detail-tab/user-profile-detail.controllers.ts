// MARK: Import

import { NavigationKey, DudeStatus, UserType } from "../../../constants/app.constant"
import { UserProfileDetailProps } from "./user-profile-detail.props"
import { UserProfileDetailStoreModel, UserProfileDetailResourcesStoreModel } from "./storemodels/user-profile-detail.store"
import { RootStore, INavigationRoute } from "../../../models"
import { onSnapshot } from "mobx-state-tree"
import { UserProfileDetailStoreTab } from "./user-profile-detail.types"
import { HeaderProps } from "../../../components/header/header.props"
import { UserProfileDetailResourcesStore } from "./storemodels/user-profile-detail.types"
import { TabController } from "../../tab.controller"
import { IAlertParams } from "../../base.controller"


class UserProfileDetailController extends TabController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static resourcesViewModel: UserProfileDetailResourcesStore
    public static viewModel: UserProfileDetailStoreTab
    public static myProps: UserProfileDetailProps & Partial<INavigationRoute>

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, props?: UserProfileDetailProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, props, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(props)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        UserProfileDetailController.resourcesViewModel = UserProfileDetailResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.UserProfileDetailTab)
        UserProfileDetailController.viewModel = localStore && UserProfileDetailStoreModel.create({ ...localStore }) || UserProfileDetailStoreModel.create({})
    }

    private _setupSnapShot = () => {
        onSnapshot(UserProfileDetailController.viewModel, (snap: UserProfileDetailStoreTab) => {
            this._setInitializedToPropsParams()
        })
    }

    //@override
    _setupProps = (myProps: UserProfileDetailProps & Partial<INavigationRoute>) => {
        super._setupProps && super._setupProps(myProps)
        UserProfileDetailController.myProps = {
            ...myProps,
            ...myProps?.route?.params
        }
    }

    /*
       Mark Data
   */

    private _bindingDudetteProfile = () => {
        UserProfileDetailController.viewModel?.bindingDataFromObject(this._rootStore?.getUserStore, UserType.Me)
        if (!UserProfileDetailController.viewModel?.status)
            UserProfileDetailController.viewModel?.setStatus(DudeStatus.Unpublished)
        this._isGlobalLoading(false)
    }

    /*
       Mark Event
   */

    backProcess = () => {
        const { navigation } = this._myProps
        navigation?.goBack()
    }



    public getHeaderLeftOptions: HeaderProps = {
        onLeftPress: this.backProcess
    }

    private static _onOpenModal = () => UserProfileDetailController.viewModel?.setIsModalShowing(true)
    private _onCloseModal = () => UserProfileDetailController.viewModel?.setIsModalShowing(false)

    public static onSaveAndContinueDidTouch = async () => {
        UserProfileDetailController._validateData()
        if (!UserProfileDetailController.viewModel?.getAllUserCredentialsValid) {
            const params: IAlertParams = {
                title: UserProfileDetailController.resourcesViewModel?.getResourceAlertValidateUserTitle(),
                message: UserProfileDetailController.resourcesViewModel?.getResourceAlertValidateUserMessage()
            }
            UserProfileDetailController._generalAlertOS(params)
        } else {
            UserProfileDetailController._onOpenModal()
        }
    }

    public onSave = (isPublish: boolean) => {
        this._onCloseModal()
        this._delayExecutor(async () => await this._askForPublish(isPublish))
    }

    public onModalClosed = () => {

    }

    private _askForPublish = async (isPublish: boolean) => {
        const userId = UserProfileDetailController.viewModel?.getUserId
        this._isGlobalLoading(true)
        await UserProfileDetailController.viewModel?.askForPublish(this._rootStore, userId, isPublish)
        this._isGlobalLoading(false)
        this.backProcess()
    }


    /*
       Mark Life cycle
   */

    //@override
    viewDidAppearOnce = async () => {
        this._bindingDudetteProfile()
    }

    /*
        Helper
    */

    private static _validateData = () => {
        const isValid = UserProfileDetailController.viewModel?.getIsAllPagesValid
        UserProfileDetailController.viewModel?.setIsValid(isValid)
    }
}

export default UserProfileDetailController