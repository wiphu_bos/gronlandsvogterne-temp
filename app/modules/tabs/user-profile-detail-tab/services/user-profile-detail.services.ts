import { types, flow } from "mobx-state-tree"
import { UserProfileDetailPropsModel } from "../viewmodels"
import { UserStoreModel, IUser } from "../../../../models/user-store"
import UserServices from "../../../../services/api-domain/user.services"
import { IUploadFileParams, IUploadFileData } from "../../../../utils/firebase/fire-storage/fire-storage.types"
import { RootStore } from "../../../../models"
import * as FirebaseUtils from "../../../../utils/firebase/fire-storage"
import { FileUploadType, DudeStatus, UserType } from "../../../../constants/app.constant"
import { APISuccessParams, APISuccessResponse } from "../../../../constants/service.types"
import { GeneralResources } from "../../../../constants/firebase/remote-config/remote-config.constant"
import { FirebaseAuthErrorResponse } from "../../../../constants/firebase/auth/auth-error-response.constant"
import * as StringUtils from "../../../../utils/string.utils"
import * as DataUtils from "../../../../utils/data.utils"

export const UserProfileDetailServiceActions = types.compose(types.model(UserProfileDetailPropsModel), UserStoreModel).actions(self => {

    const saveUserProfileProcess = flow(function* (isPublish: boolean) {
        const params: Partial<IUser> = {
            uid: self.getUserId,
            full_name: self.getFullName,
            email: self.email,
            age: self.getAge?.value,
            city: self.getCity?.key,
            havd_soger_han_list: self.getHavdSogerHanList?.map(e => e.key),
            interest_list: self.getInterestList?.map(e => e.key),
            tag_list: self.getTagList?.map(e => e.key),
            description: StringUtils.newLineToBr(self.getDescription),
            profile_image: self.getProfileImageToUpdateDetails,
            gallery_image_list: self.getGalleryImageToUpdateDetails,
            status: isPublish ? DudeStatus.Published : DudeStatus.Unpublished
        }
        return yield UserServices.putUser(params)
    })

    const uploadImages = flow(function* (path: string) {
        const galleryImagesList = self.getShouldUploadGalleryImages
        const profileImage = self?.getShouldUploadProfileImage
        const uploadList: any[] = galleryImagesList?.concat(profileImage)
        if (uploadList?.length > 0) {
            const params: IUploadFileParams = {
                fileList: uploadList,
                uid: path
            }
            const uploadedImageList: IUploadFileData[] = yield FirebaseUtils.uploadfiles(params)
            const profileImageList = uploadedImageList?.filter(e => e.type === FileUploadType.ProfileImage)
            const gallertImageList = uploadedImageList?.filter(e => e.type === FileUploadType.GalleryImage)
            if (profileImage.length > 0) self?.setProfileImage(profileImageList)
            if (gallertImageList.length > 0) self?.setGalleryImages(gallertImageList)
        }
    })

    const deleteImages = flow(function* (path: string) {
        const galleryImagesToRemoveList = self?.getGalleryImagesToRemove
        const profileImageToRemove = self?.getProfileImagesToRemove
        if (galleryImagesToRemoveList.length > 0 || profileImageToRemove.length > 0) {
            const params: IUploadFileParams = {
                fileList: galleryImagesToRemoveList?.concat(profileImageToRemove) as any,
                uid: path
            }
            yield FirebaseUtils.deletefiles(params)
        }
    })

    const askForPublish = flow(function* (rootStore: RootStore, path: string, isPublish: boolean) {
        const isConnected = rootStore?.getSharedStore?.getIsConnected
        if (!isConnected) {
            const firebaseErrorMessageObject = rootStore?.getGeneralResourcesStore(GeneralResources.SharedKeys.firebaseErrorMessage, true)
            const internetErrorMessage = firebaseErrorMessageObject[FirebaseAuthErrorResponse.NetworkRequestFailed]
            return internetErrorMessage
        }
        try {
            yield deleteImages(path)
            yield uploadImages(path)
            const askForPublish: APISuccessParams = yield saveUserProfileProcess(isPublish)
            let success = (askForPublish as APISuccessResponse)?.data
            let data: IUser = success?.data
            if (!data) throw askForPublish
            const userProfile = DataUtils.getUserProfileObject(rootStore, rootStore?.getUserStore, data)
            rootStore?.getUserStore?.bindingDataFromObject(userProfile, UserType.Me)
            return data
        } catch (e) {
            return (e)
        }
    })

    return { askForPublish }
})