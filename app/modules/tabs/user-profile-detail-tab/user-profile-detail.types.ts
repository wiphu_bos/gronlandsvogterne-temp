import { Instance, SnapshotOut } from "mobx-state-tree"
import { UserProfileDetailStoreModel } from "./storemodels"

export type UserProfileDetailStoreTab = Instance<typeof UserProfileDetailStoreModel>
export type UserProfileDetailStoreTabSnapshot = SnapshotOut<typeof UserProfileDetailStoreModel>