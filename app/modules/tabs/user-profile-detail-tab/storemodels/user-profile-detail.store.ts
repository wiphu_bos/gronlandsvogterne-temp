import { types } from "mobx-state-tree"
import { UserProfileDetailViews, UserProfileDetailActions, UserProfileDetailResourcesViews } from "../viewmodels"
import { UserProfileDetailPropsModel } from "../viewmodels"
import { UserProfileDetailServiceActions } from "../services/user-profile-detail.services"
import { StoreName } from "../../../../constants/app.constant"
import { GeneralResourcesStoreModel } from "../../../../models/general-resources-store/general-resources.store"
import { UserStoreModel } from "../../../../models/user-store/user.store"
import { ValidationStoreModel } from "../../../../models/validation-store/validation-store.store"

const UserProfileDetailModel = types.model(StoreName.UserProfileDetailTab, UserProfileDetailPropsModel)

export const UserProfileDetailStoreModel = types.compose(
    UserStoreModel,
    ValidationStoreModel,
    UserProfileDetailModel,
    UserProfileDetailViews,
    UserProfileDetailActions,
    UserProfileDetailServiceActions)

    export const UserProfileDetailResourcesStoreModel = types.compose(GeneralResourcesStoreModel, UserProfileDetailResourcesViews)

