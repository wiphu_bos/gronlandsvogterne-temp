import { Instance, SnapshotOut } from "mobx-state-tree"
import { UserProfileDetailStoreModel, UserProfileDetailResourcesStoreModel } from "./user-profile-detail.store"

export type UserProfileDetailStore = Instance<typeof UserProfileDetailStoreModel>
export type UserProfileDetailStoreSnapshot = SnapshotOut<typeof UserProfileDetailStoreModel>

export type UserProfileDetailResourcesStore = Instance<typeof UserProfileDetailResourcesStoreModel>
export type UserProfileDetailResourcesStoreSnapshot = SnapshotOut<typeof UserProfileDetailResourcesStoreModel>