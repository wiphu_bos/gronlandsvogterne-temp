// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { Wallpaper, Header, ConfirmModal } from "../../../components"
import { UserProfileDetailProps } from "./user-profile-detail.props"
import UserProfileDetailController from "./user-profile-detail.controllers"
import { SafeAreaView } from "react-navigation"
import { useConfigurate } from "../../../custom-hooks/use-configure-controller"
import { UserProfileDetailTapComponents } from "../../../navigation/tabs/user-profile-detail-tab/user-profile-detail-tab"
import { INavigationRoute } from "../../../models/navigation-store/navigation.types"
import * as metric from "../../../theme"

// MARK: Style Import

import * as Styles from "./user-profile-detail.styles"

export const UserProfileDetailScreen: React.FunctionComponent<UserProfileDetailProps & Partial<INavigationRoute>> = observer((props) => {

    const controller = useConfigurate(UserProfileDetailController, props) as UserProfileDetailController

    // MARK: Render
    return (
        <Wallpaper >
            <SafeAreaView shouldRasterizeIOS style={Styles.SAFE_AREA_VIEW} {...metric.safeAreaViewProps}>
                <Header {...Styles.HEADER_PROPS} {...controller.getHeaderLeftOptions} />
                <UserProfileDetailTapComponents tabController={controller} />
                <ConfirmModal
                    {...UserProfileDetailController.resourcesViewModel?.getTestIDConfirmModal()}
                    okButtonText={UserProfileDetailController.resourcesViewModel?.getResourceModalButtonOKTitle()}
                    clearButtonText={UserProfileDetailController.resourcesViewModel?.getResourceModalButtonCancelTitle()}
                    message={UserProfileDetailController.resourcesViewModel?.getResourceModalConfirmPublishProfile()}
                    isVisible={UserProfileDetailController.viewModel?.getIsModalShowing || false}
                    onModalClosed={controller.onModalClosed}
                    onCancel={() => controller.onSave(false)}
                    onOK={() => controller.onSave(true)}
                />
            </SafeAreaView>
        </Wallpaper>
    )

})
