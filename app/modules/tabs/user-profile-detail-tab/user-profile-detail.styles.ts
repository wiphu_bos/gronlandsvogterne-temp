import { ViewStyle, ImageStyle } from "react-native"
import * as metric from "../../../theme"
import { images } from "../../../theme/images"
import { HeaderProps } from "../../../components/header/header.props"

export const FULL: ViewStyle = {
    flex: 1
}

export const SAFE_AREA_VIEW: ViewStyle = {
    ...FULL
}
const MENU_ICON_STYLE: ImageStyle = {
    width: metric.ratioWidth(25),
    height: metric.ratioHeight(25),
    resizeMode: null
}

export const HEADER_PROPS: HeaderProps = {
    leftIconSource: images.back,
    leftIconStyle: MENU_ICON_STYLE,
    isLeftIconAnimated: false,
    containerStyle: {
        height: metric.ratioHeight(59)
    }
}