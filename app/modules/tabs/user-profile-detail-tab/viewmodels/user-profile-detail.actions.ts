import { types } from "mobx-state-tree"
import { UserProfileDetailPropsModel } from "./user-profile-detail.models"
export const UserProfileDetailActions = types.model(UserProfileDetailPropsModel).actions(self => {

    //MARK: Volatile State

    //MARK: Actions
    const setIsModalShowing = (value: boolean) => self.isModalShowing = value
    return {
        setIsModalShowing
    }
})