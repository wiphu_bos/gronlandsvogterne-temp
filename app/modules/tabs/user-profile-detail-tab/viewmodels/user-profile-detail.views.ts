import { types } from "mobx-state-tree"
import { UserProfileDetailPropsModel } from "./user-profile-detail.models"
import { GeneralResourcesStoreModel } from "../../../../models/general-resources-store/general-resources.store"
import { GeneralResources } from "../../../../constants/firebase/remote-config/remote-config.constant"
import { TestIDResources } from "../../../../constants/test-key/test.constant"
import * as Utils from "../../../../utils"
import { UserStoreModel } from "../../../../models/user-store"
export const UserProfileDetailViews = types.compose(UserStoreModel, types.model(UserProfileDetailPropsModel))
    .views(self => ({
        get getIsModalShowing() {
            return self.isModalShowing
        },
        get getIsFirstPageValid() {
            return self.getEmail &&
                self.getAge?.value &&
                self.getCity?.value &&
                self.getFullName &&
                self.getHavdSogerHanList?.length > 0 &&
                self.getInterestList?.length > 0 &&
                self.getTagList?.length >= 3 ? true : false
        },
        get getIsSecondPageValid() {
            return self.getDescription
        },
        get getIsThirdPageValid() {
            return self.getGalleryImages?.length > 0 && self.getProfileImage?.length > 0
        },
        get getIsAllPagesValid() {
            return this.getIsFirstPageValid &&
                this.getIsSecondPageValid &&
                this.getIsThirdPageValid
        }
    }))


export const UserProfileDetailResourcesViews = GeneralResourcesStoreModel.views(self => {

    const { UserProfileDetailTabScreen } = GeneralResources
    const {
        modalButtonCancelTitle,
        modalButtonOKTitle,
        modalConfirmPublishProfile,
        alertValidateUserTitle,
        alertValidateUserMessage
    } = UserProfileDetailTabScreen

    const getShareResources = (key: string) => self.getValues(key, true)
    const getResourceModalButtonOKTitle = () => getShareResources(modalButtonOKTitle)
    const getResourceModalButtonCancelTitle = () => getShareResources(modalButtonCancelTitle)
    const getResourceModalConfirmPublishProfile = () => getShareResources(modalConfirmPublishProfile)
    const getResourceAlertValidateUserTitle = () => getShareResources(alertValidateUserTitle)
    const getResourceAlertValidateUserMessage = () => getShareResources(alertValidateUserMessage)

    return {
        getResourceModalButtonOKTitle,
        getResourceModalButtonCancelTitle,
        getResourceModalConfirmPublishProfile,
        getResourceAlertValidateUserTitle,
        getResourceAlertValidateUserMessage
    }
})
    .views(self => {
        //MARK: Volatile State
        const getTestIDConfirmModal = () => Utils.getTestIDObject(TestIDResources.PrimaryInformationFourthScreen.confirmModal)
        return {
            getTestIDConfirmModal
        }
    })
