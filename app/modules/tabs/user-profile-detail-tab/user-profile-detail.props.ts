import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { TabController } from '../../tab.controller'

export interface UserProfileDetailProps extends NavigationContainerProps<ParamListBase> {
    tabController:TabController
}