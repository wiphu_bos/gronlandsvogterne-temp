import { types } from "mobx-state-tree"
import { FavouriteListPropsModel } from "./favourite-list.models"
export const FavouriteListActions = types.model(FavouriteListPropsModel).actions(self => {
    const setShouldShowAddDudeMenu = (value: boolean) => self.shouldShowAddDudeMenu = value
    return {
        setShouldShowAddDudeMenu
    }
})