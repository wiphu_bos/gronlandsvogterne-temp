import { types } from "mobx-state-tree"
import { FavouriteListPropsModel } from "./favourite-list.models"
import { GeneralResourcesStoreModel } from "../../../../models/general-resources-store/general-resources.store"
import { GeneralResources } from "../../../../constants/firebase/remote-config/remote-config.constant"
import { NavigationKey } from "../../../../constants/app.constant"
import { TestIDResources } from "../../../../constants/test-key/test.constant"
import * as Utils from "../../../../utils"
export const FavouriteListViews = types.model(FavouriteListPropsModel)
    .views(self => ({
        get getShouldShowAddDudeMenu() {
            return self.shouldShowAddDudeMenu
        }
    }))


export const FavouriteListResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State

    const { FavouriteListTabScreen } = GeneralResources
    const { favouriteDudesTabMenuTitle,
        favouriteDudettesTabMenuTitle } = FavouriteListTabScreen
    const { FavouriteListTab } = NavigationKey

    //MARK: Views
    const getResources = (key: string) => self.getValues(FavouriteListTab, key)
    const getResourceFavouriteDudesTabMenuTitle = () => getResources(favouriteDudesTabMenuTitle)
    const getResourceFavouriteDudettesTabMenuTitle = () => getResources(favouriteDudettesTabMenuTitle)
    return {
        getResourceFavouriteDudesTabMenuTitle,
        getResourceFavouriteDudettesTabMenuTitle
    }
})
    .views(self => {

        //MARK: Volatile State

        //MARK: Views

        const getTestIDFavouriteDudesTabMenuTitle = () => Utils.getTestIDObject(TestIDResources.FavouriteListTabScreen.favouriteDudesTabMenu)
        const getTestIDFavouriteDudettesTabMenuTitle = () => Utils.getTestIDObject(TestIDResources.FavouriteListTabScreen.favouriteDudettesTabMenu)
        return {
            getTestIDFavouriteDudesTabMenuTitle,
            getTestIDFavouriteDudettesTabMenuTitle
        }
    })
