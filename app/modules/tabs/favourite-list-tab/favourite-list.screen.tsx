// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { Wallpaper, Header } from "../../../components"
import { FavouriteListProps } from "./favourite-list.props"
import FavouriteController from "./favourite-list.controllers"
import { SafeAreaView } from "react-navigation"
import { useConfigurate } from "../../../custom-hooks/use-configure-controller"
import * as metric from "../../../theme"
import { FavouriteListTapComponents } from "../../../navigation/tabs/favourite-list-tab/favourite-list-tab"

// MARK: Style Import

import * as Styles from "./favourite-list.styles"

export const FavouriteListScreen: React.FunctionComponent<FavouriteListProps> = observer((props) => {

    const controller = useConfigurate(FavouriteController, props) as FavouriteController

    // MARK: Render
    
    return (
        <Wallpaper >
            <SafeAreaView
                {...metric.safeAreaViewProps}
                shouldRasterizeIOS
                style={Styles.SAFE_AREA_VIEW}>
                <Header {...Styles.HEADER_PROPS} {...controller.getHeaderLeftOptions} />
                <FavouriteListTapComponents tabController={controller}
                />
            </SafeAreaView>
        </Wallpaper>
    )

})
