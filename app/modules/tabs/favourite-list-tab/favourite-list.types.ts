import { Instance, SnapshotOut } from "mobx-state-tree"
import { FavouriteListStoreModel } from "./storemodels"

export type FavouriteListStore = Instance<typeof FavouriteListStoreModel>
export type FavouriteListStoreSnapshot = SnapshotOut<typeof FavouriteListStoreModel>