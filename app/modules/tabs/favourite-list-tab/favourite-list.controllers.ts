// MARK: Import
import { TextStyle } from "react-native"
import { NavigationKey } from "../../../constants/app.constant"
import { FavouriteListProps } from "./favourite-list.props"
import { FavouriteListStoreModel, FavouriteListResourcesStoreModel } from "./storemodels/favourite-list.store"
import { RootStore, INavigationRoute } from "../../../models"
import { onSnapshot } from "mobx-state-tree"
import { FavouriteListStore } from "./favourite-list.types"
import { DrawerActions } from "@react-navigation/core"
import { HeaderProps } from "../../../components/header/header.props"
import { FavouriteListResourcesStore } from "./storemodels/favourite-list.types"
import { GeneralResources } from "../../../constants/firebase/remote-config"
import { RFValue } from "react-native-responsive-fontsize"
import { TabController } from "../../tab.controller"

class FavouriteController extends TabController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static resourcesViewModel: FavouriteListResourcesStore
    public static viewModel: FavouriteListStore

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, props?: FavouriteListProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, props, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(props)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        FavouriteController.resourcesViewModel = FavouriteListResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.FavouriteListTab)
        FavouriteController.viewModel = localStore && FavouriteListStoreModel.create({ ...localStore }) || FavouriteListStoreModel.create({})
    }

    private _setupSnapShot = () => {
        onSnapshot(FavouriteController.viewModel, (snap: FavouriteListStore) => {
            this._setInitializedToPropsParams()
        })
    }

    /*
       Mark Data
   */

    //@override
    tapTitleStyle: TextStyle = {
        fontSize: RFValue(14.5)
    }

    /*
       Mark Event
   */

    public getHeaderLeftOptions: HeaderProps = {
        onLeftPress: () => this.onSideMenuPress()
    }

    //@override
    _onTabPageChange = () => {
        const index = this._tabState?.index
        const routeNames = this._tabState?.routeNames
        const dudeProfileTabTitle = this._rootStore?.getGeneralResourcesStore(NavigationKey.FavouriteListTab, GeneralResources.FavouriteListTabScreen.favouriteDudettesTabMenuTitle)
        FavouriteController.viewModel?.setShouldShowAddDudeMenu(routeNames && routeNames[index] === dudeProfileTabTitle || false)
    }

    public onSideMenuPress = () => {
        this._myProps?.navigation?.dispatch(DrawerActions.openDrawer() as any)
    }

    /*
       Mark Life cycle
   */

}

export default FavouriteController