import { types } from "mobx-state-tree"
import { FavouriteListViews, FavouriteListActions, FavouriteListResourcesViews } from "../viewmodels"
import { FavouriteListPropsModel } from "../viewmodels"
import { FavouriteListServiceActions } from "../services/favourite-list.services"
import { StoreName } from "../../../../constants/app.constant"
import { GeneralResourcesStoreModel } from "../../../../models/general-resources-store/general-resources.store"

const FavouriteListModel = types.model(StoreName.FavouriteListTab, FavouriteListPropsModel)

export const FavouriteListStoreModel = types.compose(
    FavouriteListModel,
    FavouriteListViews,
    FavouriteListActions,
    FavouriteListServiceActions)

    export const FavouriteListResourcesStoreModel = types.compose(GeneralResourcesStoreModel, FavouriteListResourcesViews)

