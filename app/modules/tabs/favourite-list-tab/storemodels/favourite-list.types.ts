import { Instance, SnapshotOut } from "mobx-state-tree"
import { FavouriteListStoreModel, FavouriteListResourcesStoreModel } from "./favourite-list.store"

export type FavouriteListStore = Instance<typeof FavouriteListStoreModel>
export type FavouriteListStoreSnapshot = SnapshotOut<typeof FavouriteListStoreModel>

export type FavouriteListResourcesStore = Instance<typeof FavouriteListResourcesStoreModel>
export type FavouriteListResourcesStoreSnapshot = SnapshotOut<typeof FavouriteListResourcesStoreModel>