import { types } from "mobx-state-tree"
import { StoreName } from "../../../../../constants/app.constant"
import { FavouriteDudeListPropsModel } from "../viewmodels/favourite-dude-list.models"
import { FavouriteDudeListActions } from "../viewmodels/favourite-dude-list.actions"
import { FavouriteDudeListViews, FavouriteDudeListResourcesViews } from "../viewmodels/favourite-dude-list.views"
import { GeneralResourcesStoreModel } from "../../../../../models/general-resources-store"
import { FavouriteDudeListServiceActions } from "../services/favourite-dude-list.services"

const DudeProfileModel = types.model(StoreName.FavouriteDudeList, FavouriteDudeListPropsModel)
export const FavouriteDudeListStoreModel = types.compose(DudeProfileModel, FavouriteDudeListViews, FavouriteDudeListActions, FavouriteDudeListServiceActions)
export const FavouriteDudeListResourcesStoreModel = types.compose(GeneralResourcesStoreModel, FavouriteDudeListResourcesViews)

