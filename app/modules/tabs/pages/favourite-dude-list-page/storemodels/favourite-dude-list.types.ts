import { Instance, SnapshotOut } from "mobx-state-tree"
import { FavouriteDudeListStoreModel, FavouriteDudeListResourcesStoreModel } from "./favourite-dude-list.store"

export type FavouriteDudeListStore = Instance<typeof FavouriteDudeListStoreModel>
export type FavouriteDudeListStoreSnapshot = SnapshotOut<typeof FavouriteDudeListStoreModel>

export type FavouriteDudeListResourcesStore = Instance<typeof FavouriteDudeListResourcesStoreModel>
export type FavouriteDudeListResourcesStoreSnapshot = SnapshotOut<typeof FavouriteDudeListResourcesStoreModel>
