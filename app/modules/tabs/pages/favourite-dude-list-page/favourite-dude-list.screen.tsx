// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { FavouriteDudeListProps } from "./favourite-dude-list.props"
import LinearGradient from "react-native-linear-gradient"
import { View, RefreshControl } from "react-native"
import { FlatList } from "react-native-gesture-handler"
import { LoadMoreIndicator } from "../../../../components/loadmore-indicator/loadmore-indicator"
import { LoadingListComponent } from "../../../../components"
import { useConfigurate } from "../../../../custom-hooks/use-configure-controller"
import FavouriteDudeListController from "./favourite-dude-list.controllers"
import { FavouriteCardComponents } from "../../../../components/favourite-card/favourite-card"
import { UserType } from "../../../../constants/app.constant"

// MARK: Style Import

import * as Styles from "./favourite-dude-list.styles"

export const FavouriteDudeListScreen: React.FunctionComponent<FavouriteDudeListProps> = observer((props) => {

    const controller = useConfigurate(FavouriteDudeListController, props) as FavouriteDudeListController

    // MARK: Render
    return (
        <LinearGradient
            shouldRasterizeIOS
            style={Styles.CONTAINER}
            {...Styles.TAB_PAGE_BACKGROUND}>
            {FavouriteDudeListController.viewModel?.getIsFirstLoadDone ?
                <FlatList
                    // {...resourcesViewModel?.getTestIDFavList()}
                    refreshControl={
                        <RefreshControl
                            style={Styles.REFRESH_CONTROL}
                            refreshing={false}
                            onRefresh={controller.onRefresh}
                        />
                    }
                    contentContainerStyle={Styles.LIST_VIEW}
                    showsVerticalScrollIndicator={false}
                    ItemSeparatorComponent={({ highlighted }) => (
                        <View shouldRasterizeIOS style={Styles.SEPARATOR_VIEW} />
                    )}
                    extraData={FavouriteDudeListController.viewModel?.getFavouriteDudeList?.slice()}
                    data={FavouriteDudeListController.viewModel?.getFavouriteDudeList}
                    renderItem={({ item, index, separators }) => (
                        <FavouriteCardComponents
                            userType={UserType.Dude}
                            primaryCount={item.comment_count}
                            unreadPrimaryCount={item.unread_comment_count}
                            primaryCountTestID={FavouriteDudeListController.resourcesViewModel?.getTestIDCommentCountTitle()}
                            fullnameTestID={FavouriteDudeListController.resourcesViewModel?.getTestIDFullNameTitle()}
                            imageProfileTestID={FavouriteDudeListController.resourcesViewModel?.getTestIDImageProfile()}
                            unreadPrimaryCountTestID={FavouriteDudeListController.resourcesViewModel?.getTestIDUnreadCommentCountTitle()}
                            viewCountTestID={FavouriteDudeListController.resourcesViewModel?.getTestIDViewCountTitle()}
                            viewCount={item.view_count}
                            favouriteDetails$={item}
                            navigation={props?.navigation}
                            isHighlight={false}
                            onPress={(item) => controller.onDudeCardPressed(item)} />
                    )}
                    ListFooterComponent={!FavouriteDudeListController.viewModel?.getIsLoadDone && <LoadMoreIndicator />}
                    onEndReachedThreshold={0.4}
                    onEndReached={controller.onLoadMore}
                />
                : <LoadingListComponent />}
        </LinearGradient>

    )
})
