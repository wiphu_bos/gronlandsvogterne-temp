import { types, unprotect } from "mobx-state-tree"
import { FavouriteDudeListPropsModel } from "./favourite-dude-list.models"
import { IUser, UserStoreModel } from "../../../../../models/user-store"
import * as DataUtils from "../../../../../utils/data.utils"
import { RootStore } from "../../../../../models/root-store/root.types"
import { UserType } from "../../../../../constants/app.constant"

export const FavouriteDudeListActions = types.model(FavouriteDudeListPropsModel).actions(self => {
    const setFavouriteDudeList = (rootStore: RootStore, list: IUser[]) => {
        list.forEach(e => {
            const dudeProfileObject = DataUtils.getUserProfileObject(rootStore, null, e)
            const childViewModel = UserStoreModel.create(dudeProfileObject)
            unprotect(childViewModel)
            self.favouriteDudeList$.push(childViewModel)
        })
        self.isPullRefreshing = false
        self.isLoadMore = false
    }
    const findExistingItemIndex = (dude: IUser): number => {
        const tempFavDudeList = self.favouriteDudeList$
        const existingList = tempFavDudeList?.slice() //Slice to disobserve property
        const foundItemIndex = existingList?.findIndex(v => v.dude_id === dude?.dude_id)
        return foundItemIndex
    }
    const addDudeInList = (rootStore: RootStore, dude: IUser) => {
        const foundItemIndex = findExistingItemIndex(dude)
        if (foundItemIndex !== -1) return
        const tempFavDudeList = self.favouriteDudeList$
        const dudeProfileObject = DataUtils.getUserProfileObject(rootStore, null, dude)
        const childViewModel = UserStoreModel.create(dudeProfileObject)
        unprotect(childViewModel)
        self.favouriteDudeList$ = [].concat.apply(childViewModel, tempFavDudeList)
    }
    const updateDudeInList = (rootStore: RootStore, dude: IUser) => {
        const foundItemIndex = findExistingItemIndex(dude)
        if (foundItemIndex === -1) return
        const dudeProfileObject = DataUtils.getUserProfileObject(rootStore, null, dude)
        self.favouriteDudeList$[foundItemIndex].bindingDataFromObject(dudeProfileObject, UserType.Dude)
        rootStore?.getUserStore?.saveFavDudeToList(dude)
    }
    const removeDudeInList = (rootStore: RootStore, dude: IUser) => {
        const foundItemIndex = findExistingItemIndex(dude)
        if (foundItemIndex === -1) return
        self.favouriteDudeList$.splice(foundItemIndex, 1)
        rootStore?.getUserStore?.removeFavDudeFromList(dude)
    }
    const setIsPullRefreshing = (value: boolean) => self.isPullRefreshing = value
    const setIsLoadMore = (value: boolean) => self.isLoadMore = value
    const setIsFirstLoadDone = (value: boolean) => self.isFirstLoadDone = value
    const setIsLoadDone = (value: boolean) => self.isLoadDone = value
    const resetFetching = (rootStore: RootStore) => {
        rootStore?.getUserStore?.setFavDudeList([])
        self.favouriteDudeList$.clear()
        self.isFirstLoadDone = false
        self.isLoadDone = false
        self.isLoadMore = false
    }
    return {
        addDudeInList,
        updateDudeInList,
        removeDudeInList,
        setIsPullRefreshing,
        resetFetching,
        setIsLoadMore,
        setFavouriteDudeList,
        setIsLoadDone,
        setIsFirstLoadDone
    }
})