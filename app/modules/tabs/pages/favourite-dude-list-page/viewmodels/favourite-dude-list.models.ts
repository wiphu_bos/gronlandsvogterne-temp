import { types } from "mobx-state-tree"
import { UserStoreModel } from "../../../../../models/user-store/user.store"

export const FavouriteDudeListPropsModel = {
    favouriteDudeList$: types.optional(types.array(UserStoreModel), []),
    last_id: types.maybeNull(types.string),
    isPullRefreshing: types.optional(types.boolean, false),
    isFirstLoadDone: types.optional(types.boolean, false),
    isLoadDone: types.optional(types.boolean, false),
    isLoadMore: types.optional(types.boolean, false)
} 