// MARK: Import
import { NavigationKey, SearchListType } from "../../../../constants/app.constant"
import { FavouriteDudeListProps } from "./favourite-dude-list.props"
import { FavouriteDudeListResourcesStoreModel, FavouriteDudeListStoreModel } from "./storemodels/favourite-dude-list.store"
import { FavouriteDudeListResourcesStore, FavouriteDudeListStore } from "./storemodels/favourite-dude-list.types"
import { RootStore, INavigationRoute } from "../../../../models"
import { StackActions } from "@react-navigation/core"
import { IUser, UserStore, UserModel } from "../../../../models/user-store"
import * as DataUtils from "../../../../utils/data.utils"
import { BaseController } from "../../../base.controller"
import { onSnapshot, unprotect } from "mobx-state-tree"
import { SnapshotChangeType } from "../../../../constants/firebase/fire-store.constant"
import { FirebaseFirestoreTypes } from "@react-native-firebase/firestore"
import FirebaseFireStore from "../../../../utils/firebase/fire-store/fire-store"
import { IFavouriteListListener } from "../../../../utils/firebase/fire-store/fire-store.types"
import { ExtractCSTWithSTN } from "mobx-state-tree/dist/core/type/type"

class FavouriteDudeListController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static viewModel: FavouriteDudeListStore
    public static resourcesViewModel: FavouriteDudeListResourcesStore
    private static _disposer: () => void
    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, myProps?: FavouriteDudeListProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, myProps, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(myProps)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        FavouriteDudeListController.resourcesViewModel = FavouriteDudeListResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.FavouriteDudeList)
        FavouriteDudeListController.viewModel = localStore && FavouriteDudeListStoreModel.create({ ...localStore }) || FavouriteDudeListStoreModel.create({})
        unprotect(FavouriteDudeListController.viewModel)
    }
    private _setupSnapShot = () => {
        onSnapshot(FavouriteDudeListController.viewModel, (snap: FavouriteDudeListStore) => {
            this._setInitializedToPropsParams()
        })
    }

    /*
       Mark Data
   */

    private _loadCacheData = () => FavouriteDudeListController.viewModel?.setFavouriteDudeList(this._rootStore, this._rootStore?.getUserStore?.getFavDudeList)

    private _fetchFavouriteDudeList = async () => await FavouriteDudeListController.viewModel?.fetchFavouriteDudeList(this._rootStore)


    /*
       Mark Event
   */


    private _onFavouriteDudeListListener = () => {
        const onNext: (snapshot: FirebaseFirestoreTypes.QuerySnapshot) => void = (snap => {
            if (!FavouriteDudeListController.viewModel?.getIsFirstLoadDone || snap.metadata.hasPendingWrites) return
            snap.docChanges().forEach(change => {
                const dude: IUser = change.doc.data()
                if (change.type === SnapshotChangeType.ADDED) {
                    if (dude) FavouriteDudeListController.viewModel?.addDudeInList(this._rootStore, dude)
                }
                else if (change.type === SnapshotChangeType.MODIFIED) {
                    if (dude) FavouriteDudeListController.viewModel?.updateDudeInList(this._rootStore, dude)
                }
                else if (change.type === SnapshotChangeType.REMOVED) {
                    if (dude) FavouriteDudeListController.viewModel?.removeDudeInList(this._rootStore, dude)
                }
            })
        })

        const onError: (e: Error) => void = (e => {
        })

        const onComplete: () => void = () => {
        }

        const params: IFavouriteListListener = {
            userId: this._rootStore?.getUserStore?.getUserId,
            type: SearchListType.Dude,
            onNext,
            onError,
            onComplete
        }

        FavouriteDudeListController._disposer = FirebaseFireStore.onFavouriteListener(params)
    }

    public onRefresh = async () => {
        const isConnected = this._rootStore?.getSharedStore?.getIsConnected
        if (!isConnected) return
        this._isLoading(true)
        FavouriteDudeListController.viewModel?.resetFetching(this._rootStore)
        await this._fetchFavouriteDudeList()
        this._isLoading(false)
    }

    public onLoadMore = async () => {
        if (!FavouriteDudeListController.viewModel?.getShouldLoadmore) return
        FavouriteDudeListController.viewModel?.setIsLoadMore(true)
        await this._fetchFavouriteDudeList()
    }

    private _isLoading = (value: boolean) => FavouriteDudeListController.viewModel?.setIsPullRefreshing(value)

    public onDudeCardPressed = async (favouriteDetails$: ExtractCSTWithSTN<typeof UserModel>) => {
        this._isGlobalLoading(true)
        const dudeProfile = await FavouriteDudeListController.viewModel?.fetchDudeProfileDetails(this._rootStore, favouriteDetails$.dude_id)
        this._isGlobalLoading(false)
        if (!dudeProfile) return
        const dudeProfileObject: UserStore = DataUtils.getUserProfileObject(this._rootStore, null, dudeProfile)
        if (dudeProfileObject) {
            this._myProps?.navigation?.dispatch(StackActions.push(NavigationKey.DudeProfileDetailTab, { dudeProfileObject: dudeProfileObject }) as any)
        }
    }

    //@override
    backProcess = () => {
    }

    /*
       Mark Life cycle
   */

    //@override
    viewDidAppearOnce = async () => {
        this._onFavouriteDudeListListener()
        this._loadCacheData()
    }

    //@override

    viewDidAppearAfterFocus = async () => {
        super.viewDidAppearAfterFocus && super.viewDidAppearAfterFocus()
        const isConnected = this._rootStore?.getSharedStore?.getIsConnected
        if (isConnected) {
            if (FavouriteDudeListController.viewModel?.getFavouriteDudeList?.length === 0) {
                await this.onRefresh()
            } else if (!FavouriteDudeListController.viewModel?.getIsFirstLoadDone) {
                await this._fetchFavouriteDudeList()
            }
        } else {
            FavouriteDudeListController.viewModel?.setIsFirstLoadDone(true)
        }
    }

    //@override
    deInit = () => {
        super.deInit && super.deInit()
        FavouriteDudeListController._disposer && FavouriteDudeListController._disposer()
    }

    /*
       Mark Helper
   */
}

export default FavouriteDudeListController