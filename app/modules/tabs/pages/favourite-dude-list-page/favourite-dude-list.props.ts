import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"

export interface FavouriteDudeListProps extends NavigationContainerProps<ParamListBase> {
}