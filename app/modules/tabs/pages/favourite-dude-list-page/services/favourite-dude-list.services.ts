
import { FavouriteDudeListPropsModel } from "../viewmodels"
import { types, flow, unprotect } from "mobx-state-tree"
import { APIResponse, APISuccessResponse } from "../../../../../constants/service.types"
import { RootStore } from "../../../../../models"
import { IFavouriteListResponse, IUser } from "../../../../../models/user-store/user.types"
import { UserType } from "../../../../../constants/app.constant"
import * as DataUtils from "../../../../../utils/data.utils"
import { UserStoreModel } from "../../../../../models/user-store/user.store"

export const FavouriteDudeListServiceActions = types.model(FavouriteDudeListPropsModel).actions(self => {
    const fetchFavouriteDudeList = flow(function* (rootStore: RootStore) {
       const last_id = self.isFirstLoadDone ? self.last_id : null
        try {
            self.isLoadDone = self.favouriteDudeList$.length > 0
            const result: APIResponse = yield rootStore?.getUserStore.fetchFavouriteList(last_id, UserType.Dude)
            let success = (result as APISuccessResponse)?.data
            const response: IFavouriteListResponse = success?.data
            const favouriteDudeList = response?.favourite_list
            const pagingObj = response?.page
            if (favouriteDudeList?.length > 0) {
                if (!self.isLoadMore) {
                    rootStore?.getUserStore?.setFavDudeList([])
                    self.favouriteDudeList$.clear()
                }
                favouriteDudeList?.forEach(e => {
                    const favouriteDudeObject = DataUtils.getUserProfileObject(rootStore, null, e)
                    const childViewModel = UserStoreModel.create(favouriteDudeObject)
                    unprotect(childViewModel)
                    self.favouriteDudeList$.push(childViewModel)
                })
                rootStore?.getUserStore?.setFavDudeList(favouriteDudeList)
            }
            self.last_id = pagingObj?.last_id
            self.isLoadDone = pagingObj?.is_last_page
            return result
        } catch (e) {
            return e
        } finally {
            self.isFirstLoadDone = true
            self.isPullRefreshing = false
            self.isLoadMore = false
        }
    })

    const fetchDudeProfileDetails = flow(function* (rootStore?: RootStore, uid?: string) {
        try {
            let result: APISuccessResponse = yield rootStore?.getUserStore?.fetchDudeProfileDetail(uid)
            const data: APISuccessResponse = result?.data
            const profileDetail: IUser = data?.data
            if (!profileDetail) throw data
            return profileDetail
        } catch (e) {
            return e
        }
    })

    return { fetchFavouriteDudeList, fetchDudeProfileDetails }
})