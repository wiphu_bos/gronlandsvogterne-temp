import { types } from "mobx-state-tree"
import { UserProfileDetailSecondPropsModel } from "./user-profile-detail-second-page.models"
import { GeneralResources } from "../../../../../../constants/firebase/remote-config"
import { NavigationKey } from "../../../../../../constants/app.constant"
import { TestIDResources } from "../../../../../../constants/test-key/test.constant"
import * as Utils from "../../../../../../utils"
import { GeneralResourcesStoreModel } from "../../../../../../models/general-resources-store"

export const UserProfileDetailSecondViews = types.model(UserProfileDetailSecondPropsModel)
    .views(self => ({
        get getExample() {
            return self
        },
    }))

export const UserProfileDetailSecondResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State

    const { UserProfileDetailSecondScreen } = GeneralResources
    const { fullDescriptionTitle, buttonSaveAndContinueTitle } = UserProfileDetailSecondScreen

    //MARK: Views
    const getResources = (key: string, childKeyOrShareKey: string | boolean = false) => self.getValues(childKeyOrShareKey ? key : NavigationKey.UserProfileDetailScreen, childKeyOrShareKey ? true : key)
    const getResourceTitle = () => getResources(fullDescriptionTitle)
    const getResourceButtonSaveAndContinueTitle = () => getResources(buttonSaveAndContinueTitle, true)
    return {
        getResourceTitle,
        getResourceButtonSaveAndContinueTitle
    }
})
    .views(self => {

        //MARK: Volatile State

        //MARK: Views

        const getTestIDTitle = () => Utils.getTestIDObject(TestIDResources.UserProfileDetailSecondScreen.title)
        const getTestIDDescription = () => Utils.getTestIDObject(TestIDResources.UserProfileDetailSecondScreen.description)
        const getTestIDButtonSaveAndContinue = () => Utils.getTestIDObject(TestIDResources.UserProfileDetailSecondScreen.buttonSaveAndContinue)
        return {
            getTestIDTitle,
            getTestIDDescription,
            getTestIDButtonSaveAndContinue
        }
    })
