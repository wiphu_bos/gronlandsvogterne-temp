// MARK: Import
import { NavigationKey } from "../../../../../constants/app.constant"
import { RootStore, INavigationRoute } from "../../../../../models"
import UserProfileDetailController from "../../../user-profile-detail-tab/user-profile-detail.controllers"
import { UserProfileDetailProps } from "./user-profile-detail-second-page.props"
import { UserProfileDetailSecondResourcesStore, UserProfileDetailSecondStore } from "./storemodels/user-profile-detail-second-page.types"
import { UserProfileDetailSecondResourcesStoreModel, UserProfileDetailSecondStoreModel } from "./storemodels/user-profile-detail-second-page.store"
import { BaseController, IAlertParams } from "../../../../base.controller"
import { onSnapshot } from "mobx-state-tree"
import { CommonActions } from "@react-navigation/core"
import { Keyboard } from "react-native"

class UserProfileDetailSecondController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static resourcesViewModel: UserProfileDetailSecondResourcesStore
    public static viewModel: UserProfileDetailSecondStore

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, props?: UserProfileDetailProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, props, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(props)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        UserProfileDetailSecondController.resourcesViewModel = UserProfileDetailSecondResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.UserProfileDetailSecond)
        UserProfileDetailSecondController.viewModel = localStore && UserProfileDetailSecondStoreModel.create({ ...localStore }) || UserProfileDetailSecondStoreModel.create({})
    }

    private _setupSnapShot = () => {
        onSnapshot(UserProfileDetailSecondController.viewModel, (snap: UserProfileDetailSecondStore) => {
            this._setInitializedToPropsParams()
        })
    }

    /*
       Mark Data
    */

    /*
       Mark Event
    */

    public onDescriptionChangeText = (value: string): void => {
        UserProfileDetailController.viewModel?.setDescription(value)
    }

    public onBlurDescriptionTextField = (): void => {

    }

    public onSaveAndContinueButtonDidTouch = () => {
        if (!UserProfileDetailController.viewModel?.getIsSecondPageValid) {
            const params: IAlertParams = {
                title: UserProfileDetailController.resourcesViewModel?.getResourceAlertValidateUserTitle(),
                message: UserProfileDetailController.resourcesViewModel?.getResourceAlertValidateUserMessage()
            }
            this._generalAlertOS(params)
        } else {
            this._myProps?.navigation?.dispatch(CommonActions.navigate(NavigationKey.UserProfileDetailThird) as any)
        }
    }

    /*
       Mark Life cycle
    */

    //@override
    viewWillDisappear = () => {
        super.viewWillDisappear && super.viewWillDisappear()
        Keyboard.dismiss()
    }

}

export default UserProfileDetailSecondController