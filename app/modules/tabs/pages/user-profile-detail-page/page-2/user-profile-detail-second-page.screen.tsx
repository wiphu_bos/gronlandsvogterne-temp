// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { UserProfileDetailProps } from "./user-profile-detail-second-page.props"
import LinearGradient from "react-native-linear-gradient"
import { useConfigurate } from "../../../../../custom-hooks/use-configure-controller"
import { TextInput } from "react-native-gesture-handler"
import { View } from "react-native-animatable"
import { KeyboardAvoidingView } from "react-native"
import * as metric from "../../../../../theme"
import UserProfileDetailSecondController from "./user-profile-detail-second-page.controllers"
import { INavigationRoute } from "../../../../../models/navigation-store/navigation.types"
import { GradientButton } from "../../../../../components/gradient-button/button"
import { Text, AnimatedScrollView } from "../../../../../components"
import * as StringUtils from "../../../../../utils/string.utils"

// MARK: Style Import

import * as Styles from "./user-profile-detail-second-page.styles"
import UserProfileDetailController from "../../../user-profile-detail-tab/user-profile-detail.controllers"

export const UserProfileDetailSecondScreen: React.FunctionComponent<UserProfileDetailProps & Partial<INavigationRoute>> = observer((props) => {

    const controller = useConfigurate(UserProfileDetailSecondController, props) as UserProfileDetailSecondController

    // MARK: Render
    return (
        <LinearGradient shouldRasterizeIOS style={Styles.CONTAINNER} {...Styles.TAB_PAGE_BACKGROUND}>
            <KeyboardAvoidingView
                keyboardVerticalOffset={metric.isIPhone ? metric.ratioHeight(120) : metric.ratioHeight(120)}
                behavior={metric.keyboardBehavior}
                style={Styles.AVOID_KEYBOARD_VIEW}
            >
                <AnimatedScrollView
                    contentContainerStyle={Styles.SCROLL_VIEW}
                    {...metric.solidScrollView}>
                    <View shouldRasterizeIOS style={Styles.CONTENT_CONTAINER}>
                        <View shouldRasterizeIOS style={Styles.HEADER}>
                            <Text text={UserProfileDetailSecondController.resourcesViewModel?.getResourceTitle()}
                                {...UserProfileDetailSecondController.resourcesViewModel?.getTestIDTitle()}
                                style={Styles.TITLE} />
                        </View>
                        <TextInput
                            style={{ ...Styles.TEXT_AREA, ...Styles.TEXT_AREA_TEXT }}
                            {...Styles.TEXT_AREA_OPTIONS}
                            onChangeText={controller.onDescriptionChangeText}
                            testIDErrorMessage={UserProfileDetailSecondController.resourcesViewModel?.getTestIDDescription()}
                            onBlur={controller.onBlurDescriptionTextField}
                        >
                            {StringUtils.brToNewLine(UserProfileDetailController.viewModel?.getDescription)}
                        </TextInput>
                    </View>
                </AnimatedScrollView>
            </KeyboardAvoidingView>
            <View shouldRasterizeIOS style={Styles.BUTTON_CONTAINER}>
                <GradientButton
                    {...UserProfileDetailSecondController.resourcesViewModel?.getTestIDButtonSaveAndContinue()}
                    text={UserProfileDetailSecondController.resourcesViewModel?.getResourceButtonSaveAndContinueTitle()}
                    onPress={controller.onSaveAndContinueButtonDidTouch} />
            </View>
        </LinearGradient>
    )
})
