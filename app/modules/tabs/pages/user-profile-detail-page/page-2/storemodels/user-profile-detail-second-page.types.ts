import { Instance, SnapshotOut } from "mobx-state-tree"
import { UserProfileDetailSecondStoreModel, UserProfileDetailSecondResourcesStoreModel } from "./user-profile-detail-second-page.store"

export type UserProfileDetailSecondStore = Instance<typeof UserProfileDetailSecondStoreModel>
export type UserProfileDetailSecondStoreSnapshot = SnapshotOut<typeof UserProfileDetailSecondStoreModel>

export type UserProfileDetailSecondResourcesStore = Instance<typeof UserProfileDetailSecondResourcesStoreModel>
export type UserProfileDetailSecondResourcesStoreSnapshot = SnapshotOut<typeof UserProfileDetailSecondResourcesStoreModel>