import { types } from "mobx-state-tree"
import { StoreName } from "../../../../../../constants/app.constant"
import { UserProfileDetailSecondPropsModel } from "../viewmodels/user-profile-detail-second-page.models"
import { UserProfileDetailSecondActions } from "../viewmodels/user-profile-detail-second-page.actions"
import { UserProfileDetailSecondViews, UserProfileDetailSecondResourcesViews } from "../viewmodels/user-profile-detail-second-page.views"
import { GeneralResourcesStoreModel } from "../../../../../../models/general-resources-store"

const UserProfileDetailSecondModel = types.model(StoreName.UserProfileDetailSecond, UserProfileDetailSecondPropsModel)
export const UserProfileDetailSecondStoreModel = types.compose(UserProfileDetailSecondModel,UserProfileDetailSecondViews,UserProfileDetailSecondActions)
export const UserProfileDetailSecondResourcesStoreModel = types.compose(GeneralResourcesStoreModel, UserProfileDetailSecondResourcesViews)

