import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { UserProfileDetailSecondStore } from "./storemodels/user-profile-detail-second-page.types"
import { UserProfileDetailStoreTab } from '../../../user-profile-detail-tab/user-profile-detail.types'
import { BaseController } from '../../../../base.controller'

type UserProfileDetailStore =
| UserProfileDetailSecondStore

export interface UserProfileDetailProps extends NavigationContainerProps<ParamListBase> {
    rootController: BaseController,
    rootViewModel?: UserProfileDetailStoreTab
    viewModel?: UserProfileDetailStore
}