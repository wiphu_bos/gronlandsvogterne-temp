// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { UserProfileDetailProps } from "./user-profile-detail-third-page.props"
import LinearGradient from "react-native-linear-gradient"
import { useConfigurate } from "../../../../../custom-hooks/use-configure-controller"
import { View } from "react-native-animatable"
import * as metric from "../../../../../theme"
import UserProfileDetailThirdController from "./user-profile-detail-third-page.controllers"
import { INavigationRoute } from "../../../../../models/navigation-store/navigation.types"
import { Text } from "../../../../../components/text/text"
import { images } from "../../../../../theme/images"
import { SquareImage } from "../../../../../components/square-image/square-image"
import { IconButton } from "../../../../../components/icon-button/icon-button"
import { GradientButton } from "../../../../../components/gradient-button/button"
import { AnimatedScrollView } from "../../../../../components"
import UserProfileDetailController from "../../../user-profile-detail-tab/user-profile-detail.controllers"
// MARK: Style Import
import * as Styles from "./user-profile-detail-third-page.styles"


export const UserProfileDetailThirdScreen: React.FunctionComponent<UserProfileDetailProps & Partial<INavigationRoute>> = observer((props) => {

    const controller = useConfigurate(UserProfileDetailThirdController, props) as UserProfileDetailThirdController

    // MARK: Render
    return (
        <LinearGradient shouldRasterizeIOS style={Styles.CONTAINNER} {...Styles.TAB_PAGE_BACKGROUND}>
            <AnimatedScrollView
                contentContainerStyle={Styles.SCROLL_VIEW}
                {...metric.solidScrollView}>
                <View shouldRasterizeIOS style={Styles.CONTENT_CONTAINER}>
                    <View shouldRasterizeIOS style={Styles.HEADER}>
                        <Text style={Styles.TITLE} text={UserProfileDetailThirdController.resourcesViewModel?.getResourceUploadProfilePhoto()} />
                    </View>
                    <View shouldRasterizeIOS style={Styles.IMAGE_CONTAINER}>
                        {(!UserProfileDetailController.viewModel?.getProfileImage || UserProfileDetailController.viewModel?.getProfileImage?.length == 0 || !UserProfileDetailController.viewModel?.getProfileImage[0].file_path) ?
                            <IconButton
                                {...UserProfileDetailThirdController.resourcesViewModel?.getTestIDButtonSelectPhoto()}
                                text={UserProfileDetailThirdController.resourcesViewModel?.getResourceSelectPhoto()}
                                textStyle={Styles.BUTTON_DOTTED_TEXT}
                                icon={images.addWhite} style={Styles.BUTTON_DOTTED}
                                onPress={controller.onSelectPhotoButtonDidTouch} /> : null}
                        <View shouldRasterizeIOS style={Styles.GRID}>
                            {(UserProfileDetailController.viewModel?.getProfileImage?.length > 0) ?
                                <View shouldRasterizeIOS style={Styles.GRID_IMAGE}>
                                    <SquareImage {...UserProfileDetailThirdController.resourcesViewModel?.getTestIDProfileImage()}
                                        deleteTestID={UserProfileDetailThirdController.resourcesViewModel?.getTestIDDeleteProfilePhoto()}
                                        imageSource={UserProfileDetailController.viewModel?.getProfileImage[0].file_path} showDelete={true}
                                        onDelete={controller.onRemoveProfileButtonDidTouch}
                                        index={0} />
                                </View>
                                : null}
                        </View>
                    </View>
                    <View shouldRasterizeIOS style={{ ...Styles.HEADER, ...Styles.ADDITIONAL_MARGIN_TOP }}>
                        <Text style={Styles.TITLE} preset="fieldLabel" text={UserProfileDetailThirdController.resourcesViewModel?.getResourceUploadPhotoGallery()} />
                    </View>
                    <View shouldRasterizeIOS style={Styles.IMAGE_CONTAINER}>
                        {(!UserProfileDetailController.viewModel?.getGalleryImages || UserProfileDetailController.viewModel?.getGalleryImages?.length < 6) ?
                            <IconButton
                                {...UserProfileDetailThirdController.resourcesViewModel?.getTestIDButtonSelectPhotoInGallery()}
                                text={UserProfileDetailThirdController.resourcesViewModel?.getResourceSelectPhoto()}
                                textStyle={Styles.BUTTON_DOTTED_TEXT}
                                icon={images.addWhite} style={Styles.BUTTON_DOTTED}
                                onPress={controller.onSelectPhotoToGalleryButtonDidTouch} /> : null}
                        <View shouldRasterizeIOS style={Styles.GRID}>
                            {(UserProfileDetailController.viewModel?.getGalleryImages) ?
                                UserProfileDetailController.viewModel?.getGalleryImages?.map((item, index) =>
                                    <View shouldRasterizeIOS style={Styles.GRID_IMAGE}>
                                        <SquareImage
                                            imageSource={item.file_path}
                                            showDelete={true}
                                            onDelete={(item: number) => controller.onRemoveGalleryButtonDidTouch(index)}
                                            index={index} />
                                    </View>
                                ) : null}
                        </View>
                    </View>
                </View>
            </AnimatedScrollView>
            <GradientButton
                {...UserProfileDetailThirdController.resourcesViewModel?.getTestIDButtonSaveAndContinue()}
                text={UserProfileDetailThirdController.resourcesViewModel?.getResourceButtonSaveAndContinueTitle()}
                containerStyle={Styles.BUTTON_MARGIN}
                onPress={UserProfileDetailController.onSaveAndContinueDidTouch} />
        </LinearGradient>
    )
})
