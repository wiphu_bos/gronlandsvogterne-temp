import { NavigationContainerProps } from "react-navigation"
import { ParamListBase } from "@react-navigation/native"
import { UserProfileDetailThirdStore } from "./storemodels/user-profile-detail-third-page.types"
import { UserProfileDetailStoreTab } from "../../../user-profile-detail-tab/user-profile-detail.types"
import { BaseController } from "../../../../base.controller"

type UserProfileDetailStore =
    | UserProfileDetailThirdStore

export interface UserProfileDetailProps extends NavigationContainerProps<ParamListBase> {
    rootController: BaseController,
    rootViewModel?: UserProfileDetailStoreTab
    viewModel?: UserProfileDetailStore
}