import { types } from "mobx-state-tree"
import { StoreName } from "../../../../../../constants/app.constant"
import { UserProfileDetailThirdPropsModel } from "../viewmodels/user-profile-detail-third-page.models"
import { UserProfileDetailFirstActions } from "../viewmodels/user-profile-detail-third-page.actions"
import { UserProfileDetailThirdViews, UserProfileDetailThirdResourcesViews } from "../viewmodels/user-profile-detail-third-page.views"
import { GeneralResourcesStoreModel } from "../../../../../../models/general-resources-store"

const UserProfileDetailThirdModel = types.model(StoreName.UserProfileDetailThird, UserProfileDetailThirdPropsModel)
export const UserProfileDetailThirdStoreModel = types.compose(UserProfileDetailThirdModel,UserProfileDetailThirdViews,UserProfileDetailFirstActions)
export const UserProfileDetailThirdResourcesStoreModel = types.compose(GeneralResourcesStoreModel, UserProfileDetailThirdResourcesViews)

