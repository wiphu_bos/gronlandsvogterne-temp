import { Instance, SnapshotOut } from "mobx-state-tree"
import { UserProfileDetailThirdStoreModel, UserProfileDetailThirdResourcesStoreModel } from "./user-profile-detail-third-page.store"

export type UserProfileDetailThirdStore = Instance<typeof UserProfileDetailThirdStoreModel>
export type UserProfileDetailThirdStoreSnapshot = SnapshotOut<typeof UserProfileDetailThirdStoreModel>

export type UserProfileDetailThirdResourcesStore = Instance<typeof UserProfileDetailThirdResourcesStoreModel>
export type UserProfileDetailThirdResourcesStoreSnapshot = SnapshotOut<typeof UserProfileDetailThirdResourcesStoreModel>