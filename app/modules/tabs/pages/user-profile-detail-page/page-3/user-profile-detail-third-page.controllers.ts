// MARK: Import
import { NavigationKey, FileUploadType } from "../../../../../constants/app.constant"
import { RootStore, INavigationRoute } from "../../../../../models"
import { UserProfileDetailProps } from "./user-profile-detail-third-page.props"
import { UserProfileDetailThirdResourcesStore, UserProfileDetailThirdStore } from "./storemodels/user-profile-detail-third-page.types"
import { UserProfileDetailThirdResourcesStoreModel, UserProfileDetailThirdStoreModel } from "./storemodels/user-profile-detail-third-page.store"
import ImagePicker, { Image } from 'react-native-image-crop-picker'
import { Storage } from "../../../../../constants/firebase/fire-storage.constant"
import { IUploadFileData } from "../../../../../utils/firebase/fire-storage/fire-storage.types"
import * as Validate from "../../../../../utils/local-validate"
import { BaseController, IAlertParams } from "../../../../base.controller"
import { onSnapshot } from "mobx-state-tree"
import UserProfileDetailController from "../../../user-profile-detail-tab/user-profile-detail.controllers"

class UserProfileDetailThirdController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static resourcesViewModel: UserProfileDetailThirdResourcesStore
    public static viewModel: UserProfileDetailThirdStore

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, props?: UserProfileDetailProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, props, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(props)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        UserProfileDetailThirdController.resourcesViewModel = UserProfileDetailThirdResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.UserProfileDetailSecond)
        UserProfileDetailThirdController.viewModel = localStore && UserProfileDetailThirdStoreModel.create({ ...localStore }) || UserProfileDetailThirdStoreModel.create({})
    }

    private _setupSnapShot = () => {
        onSnapshot(UserProfileDetailThirdController.viewModel, (snap: UserProfileDetailThirdStore) => {
            this._setInitializedToPropsParams()
        })
    }

    /*
       Mark Data
    */

    /*
       Mark Event
    */

    private _pickImage = (circular: boolean, callBack: (image: Image) => void) => {
        ImagePicker.openPicker({
            width: 1024,
            height: 1024,
            cropping: true,
            cropperCircleOverlay: circular,
            sortOrder: 'none',
            compressImageMaxWidth: 1024,
            compressImageMaxHeight: 1024,
            compressImageQuality: 1,
            includeExif: true,
        }).then((image: Image) => {
            callBack(image)
        }).catch(e => {
            if (e.code == 'E_PERMISSION_MISSING') {
                const params: IAlertParams = {
                    title: UserProfileDetailThirdController.resourcesViewModel.getResourceErrorPermisionTitle(),
                    message: UserProfileDetailThirdController.resourcesViewModel.getResourceErrorPermision(),
                    delay: 0
                }
                this._generalAlertOS(params)
            }
        })
    }

    public onSelectPhotoButtonDidTouch = () => {
        this._pickImage(true, (image: Image) => {
            const { path, filename, ...filteredData } = image
            const profileImageParams: IUploadFileData = {
                type: FileUploadType.ProfileImage,
                storage_path: Storage.ProfileImage,
                file_path: path,
                file_name: filename
            }
            UserProfileDetailController.viewModel?.setProfileImage([{ ...filteredData, ...profileImageParams }])
        })
    }

    public onSelectPhotoToGalleryButtonDidTouch = () => {
        this._pickImage(false, (image: Image) => {
            const galleries = UserProfileDetailController.viewModel?.getGalleryImages?.slice() || []
            const { path, filename, ...filteredData } = image
            const galleryImageParams: IUploadFileData = {
                type: FileUploadType.GalleryImage,
                storage_path: Storage.GalleryImage,
                file_path: path,
                file_name: filename
            }
            galleries.push({ ...filteredData, ...galleryImageParams })
            UserProfileDetailController.viewModel?.setGalleryImages(galleries)
        })
        //pickImage(false, rootViewModel.getGalleryImages)
    }

    public onRemoveProfileButtonDidTouch = () => {
        const params: IAlertParams = {
            title: UserProfileDetailThirdController.resourcesViewModel.getResourceAlertDeleteProfileImageTitle(),
            message: UserProfileDetailThirdController.resourcesViewModel.getResourceAlertDeleteProfileImageMessage(),
            onOKPress: () => this._onConfirmRemoveProfileImage(),
            onCancelPress: () => console.log('Cancel Pressed'),
            delay: 0
        }
        this._confirmationAlertOS(params)
    }

    private _onConfirmRemoveProfileImage = () => {
        UserProfileDetailController.viewModel?.setProfileImageToRemove()
        UserProfileDetailController.viewModel?.setProfileImage(null)
        const params = {
            isEmpty: true,
            localViewModel: UserProfileDetailController.viewModel as any,
            rootStore: this._rootStore
        }
        Validate.onErrorMessageProfileImageSelection(params)
    }

    public onRemoveGalleryButtonDidTouch = (index: number) => {
        const params: IAlertParams = {
            title: UserProfileDetailThirdController.resourcesViewModel.getResourceAlertDeleteGalleryImageTitle(),
            message: UserProfileDetailThirdController.resourcesViewModel.getResourceAlertDeleteGalleryImageMessage(),
            onOKPress: () => this._onConfirmRemoveGalleryImage(index),
            delay: 0
        }
        this._confirmationAlertOS(params)
    }

    private _onConfirmRemoveGalleryImage = (index: number) => {
        UserProfileDetailController.viewModel?.setGalleryImagesToRemove(index)
        let galleries = UserProfileDetailController.viewModel.getGalleryImages.slice();
        galleries.splice(index, 1)
        UserProfileDetailController.viewModel?.setGalleryImages(galleries)
    }


    /*
       Mark Life cycle
    */

}

export default UserProfileDetailThirdController