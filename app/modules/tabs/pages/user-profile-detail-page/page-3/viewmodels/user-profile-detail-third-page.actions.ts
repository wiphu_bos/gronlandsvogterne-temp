import { types } from "mobx-state-tree"
import { UserProfileDetailThirdPropsModel } from "./user-profile-detail-third-page.models"

export const UserProfileDetailFirstActions = types.model(UserProfileDetailThirdPropsModel).actions(self => {

    //MARK: Volatile State

    //MARK: Actions
    const setIsPopoverMenuVisible = (value: boolean) => self.isPopoverMenuVisible = value
    
    return {
        setIsPopoverMenuVisible
    }
})