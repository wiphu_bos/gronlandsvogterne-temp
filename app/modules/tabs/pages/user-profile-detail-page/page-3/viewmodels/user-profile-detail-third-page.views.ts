import { types } from "mobx-state-tree"
import { UserProfileDetailThirdPropsModel } from "./user-profile-detail-third-page.models"
import { GeneralResources } from "../../../../../../constants/firebase/remote-config"
import { TestIDResources } from "../../../../../../constants/test-key/test.constant"
import * as Utils from "../../../../../../utils"
import { GeneralResourcesStoreModel } from "../../../../../../models/general-resources-store"

export const UserProfileDetailThirdViews = types.model(UserProfileDetailThirdPropsModel)
    .views(self => ({
        get getIsPopoverMenuVisible() {
            return self.isPopoverMenuVisible
        },
    }))

export const UserProfileDetailThirdResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State

    const { UserProfileDetailThirdScreen } = GeneralResources
    const {
        uploadProfilePhotoTitle,
        updatePhotoToGalleryTitle,
        selectPhoto,
        buttonSaveAndContinueTitle,
        alertDeleteProfileImageTitle,
        alertDeleteProfileImageMessage,
        alertDeleteGalleryImageTitle,
        alertDeleteGalleryImageMessage,
        grantPermissionTitle,
        grantPermission
    } = UserProfileDetailThirdScreen

    //MARK: Views
    const getShareResources = (key: string) => self.getValues(key, true)
    const getResourceUploadProfilePhoto = () => getShareResources(uploadProfilePhotoTitle)
    const getResourceUploadPhotoGallery = () => getShareResources(updatePhotoToGalleryTitle)
    const getResourceSelectPhoto = () => getShareResources(selectPhoto)
    const getResourceButtonSaveAndContinueTitle = () => getShareResources(buttonSaveAndContinueTitle)
    const getResourceAlertDeleteProfileImageTitle = () => getShareResources(alertDeleteProfileImageTitle)
    const getResourceAlertDeleteProfileImageMessage = () => getShareResources(alertDeleteProfileImageMessage)
    const getResourceAlertDeleteGalleryImageTitle = () => getShareResources(alertDeleteGalleryImageTitle)
    const getResourceAlertDeleteGalleryImageMessage = () => getShareResources(alertDeleteGalleryImageMessage)
    const getResourceErrorPermisionTitle = () => getShareResources(grantPermissionTitle)
    const getResourceErrorPermision = () => getShareResources(grantPermission)
    return {
        getResourceUploadProfilePhoto,
        getResourceUploadPhotoGallery,
        getResourceSelectPhoto,
        getResourceButtonSaveAndContinueTitle,
        getResourceAlertDeleteProfileImageTitle,
        getResourceAlertDeleteProfileImageMessage,
        getResourceAlertDeleteGalleryImageTitle,
        getResourceAlertDeleteGalleryImageMessage,
        getResourceErrorPermisionTitle,
        getResourceErrorPermision,
    }
})
    .views(self => {

        //MARK: Volatile State

        //MARK: Views

        const getTestIDUploadProfilePhoto = () => Utils.getTestIDObject(TestIDResources.UserProfileDetailThirdScreen.uploadProfilePhoto)
        const getTestIDUploadPhotoGallerySection = () => Utils.getTestIDObject(TestIDResources.UserProfileDetailThirdScreen.uploadPhotoGallerySection)
        const getTestIDButtonSelectPhoto = () => Utils.getTestIDObject(TestIDResources.UserProfileDetailThirdScreen.buttonSelectPhoto)
        const getTestIDDeleteProfilePhoto = () => Utils.getTestIDObject(TestIDResources.UserProfileDetailThirdScreen.buttonDeleteProfilePhoto)
        const getTestIDButtonSelectPhotoInGallery = () => Utils.getTestIDObject(TestIDResources.UserProfileDetailThirdScreen.buttonSelectPhotoInGallery)
        const getTestIDButtonSaveAndContinue = () => Utils.getTestIDObject(TestIDResources.UserProfileDetailFirstScreen.buttonSaveAndContinue)
        const getTestIDProfileImage = () => Utils.getTestIDObject(TestIDResources.UserProfileDetailThirdScreen.imageProfile)
        return {
            getTestIDUploadProfilePhoto,
            getTestIDUploadPhotoGallerySection,
            getTestIDButtonSelectPhoto,
            getTestIDButtonSelectPhotoInGallery,
            getTestIDButtonSaveAndContinue,
            getTestIDDeleteProfilePhoto,
            getTestIDProfileImage
        }
    })
