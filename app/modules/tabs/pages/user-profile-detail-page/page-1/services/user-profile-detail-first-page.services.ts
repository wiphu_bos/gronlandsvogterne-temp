import { types, flow } from "mobx-state-tree"
import { UserProfileDetailFirstPropsModel } from "../viewmodels"
import { APIResponse, APISuccessResponse } from "../../../../../../constants/service.types"
import { RootStore } from "../../../../../../models/root-store"
import { IUser } from "../../../../../../models/user-store/user.types"

export const UserProfileDetailFirstServiceActions = types.model(UserProfileDetailFirstPropsModel).actions(self => {
    const changeMyStatus = flow(function* (rootStore: RootStore, uid: string, status: string) {
        try {
            // const result: APIResponse = yield rootStore?.UserStore?.changeDudeStatus(dude_id, status)
            // let success = (result as APISuccessResponse)?.data
            // let data: IUser = success?.data
            // if (!data) throw result
            // return data
        } catch (e) {
            return e
        }
    })

    const deleteMyUser = flow(function* (rootStore?: RootStore) {
        try {
            yield rootStore?.getUserStore?.deleteMyUser()
        } catch (e) {
            return e
        }
    })

    return {
        deleteMyUser,
        changeMyStatus
    }
})