import { types } from "mobx-state-tree"
import { UserProfileDetailFirstPropsModel } from "./user-profile-detail-first-page.models"
import { GeneralResources } from "../../../../../../constants/firebase/remote-config"
import { NavigationKey } from "../../../../../../constants/app.constant"
import { TestIDResources } from "../../../../../../constants/test-key/test.constant"
import * as Utils from "../../../../../../utils"
import { GeneralResourcesStoreModel } from "../../../../../../models/general-resources-store"
import * as DataUtils from "../../../../../../utils/data.utils"

export const UserProfileDetailFirstViews = types.model(UserProfileDetailFirstPropsModel)
    .views(self => ({
        get getIsPopoverMenuVisible() {
            return self.isPopoverMenuVisible
        },
        get getIsModalShowing() {
            return self.isModalShowing
        }
        ,
        get getIsAlertModalShowing() {
            return self.isAlertModalShowing
        }
    }))

export const UserProfileDetailFirstResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State

    const { UserProfileDetailFirstScreen } = GeneralResources
    const { statusTitle,
        fullNamePlaceholder,
        emailPlaceholder,
        agePlaceholder,
        cityPlaceholder,
        interestPlaceholder,
        havdSogerHanPlaceholder,
        tagTitle,
        modalButtonCancelTitle,
        modalButtonClearTitle,
        modalButtonOKTitle,
        modalCityPlaceholder,
        modalHavdSogerHandPlaceholder,
        modalAgePlaceholder,
        buttonActivateTitle,
        buttonDeactivateTitle,
        buttonDeleteTitle,
        buttonSaveAndContinueTitle,
        modalInterestPlaceholder,
        buttonUnPublish,
        buttonPublish,
        modalConfirmDeleteUser
    } = UserProfileDetailFirstScreen

    //MARK: Views
    const getShareResources = (key: string) => self.getValues(key, true)
    const getResources = (key: string) => self.getValues(NavigationKey.UserProfileDetailScreen, key)
    const getResourceStatusTitle = () => getShareResources(statusTitle)
    const getResourceFullNamePlaceholder = () => getResources(fullNamePlaceholder)
    const getResourceEmailPlaceholder = () => getResources(emailPlaceholder)
    const getResourceAgePlaceholder = () => getResources(agePlaceholder)
    const getResourceInterestPlaceholder = () => getResources(interestPlaceholder)
    const getResourceHavdSogerHanPlaceholder = () => getResources(havdSogerHanPlaceholder)
    const getResourceTagTitle = () => getResources(tagTitle)
    const getResourceCityPlaceholder = () => getResources(cityPlaceholder)
    const getResourceModalButtonCancelTitle = () => getShareResources(modalButtonCancelTitle)
    const getResourceModalButtonClearTitle = () => getShareResources(modalButtonClearTitle)
    const getResourceModalButtonOKTitle = () => getShareResources(modalButtonOKTitle)
    const getResourceModalCityPlaceholder = () => getShareResources(modalCityPlaceholder)
    const getResourceModalAgePlaceholder = () => getShareResources(modalAgePlaceholder)
    const getResourceModalHavdSogerHandPlaceholder = () => getShareResources(modalHavdSogerHandPlaceholder)
    const getResourceModalInterestPlaceholder = () => getShareResources(modalInterestPlaceholder)
    const getResourceButtonActivateTitle = () => getResources(buttonActivateTitle)
    const getResourceButtonDeactivateTitle = () => getResources(buttonDeactivateTitle)
    const getResourceButtonDeleteTitle = () => getShareResources(buttonDeleteTitle)
    const getResourceButtonSaveAndContinueTitle = () => getShareResources(buttonSaveAndContinueTitle)
    const getResourceButtonPublish = () => getShareResources(buttonPublish)
    const getResourceButtonUnPublish = () => getShareResources(buttonUnPublish)
    const getResourceModalConfirmDeleteUser = () => getShareResources(modalConfirmDeleteUser)
    const getResourceUserStatusTitle = (status: string) => DataUtils.getUserStatusByLocaleKey(self, status)
    return {
        getResourceStatusTitle,
        getResourceFullNamePlaceholder,
        getResourceEmailPlaceholder,
        getResourceAgePlaceholder,
        getResourceHavdSogerHanPlaceholder,
        getResourceInterestPlaceholder,
        getResourceTagTitle,
        getResourceCityPlaceholder,
        getResourceModalButtonCancelTitle,
        getResourceModalCityPlaceholder,
        getResourceModalAgePlaceholder,
        getResourceButtonDeactivateTitle,
        getResourceButtonDeleteTitle,
        getResourceModalButtonClearTitle,
        getResourceModalButtonOKTitle,
        getResourceButtonSaveAndContinueTitle,
        getResourceModalHavdSogerHandPlaceholder,
        getResourceModalInterestPlaceholder,
        getResourceUserStatusTitle,
        getResourceButtonActivateTitle,
        getResourceButtonPublish,
        getResourceButtonUnPublish,
        getResourceModalConfirmDeleteUser
    }
})
    .views(self => {

        //MARK: Volatile State

        //MARK: Views

        const getTestIDStatusTitle = () => Utils.getTestIDObject(TestIDResources.UserProfileDetailFirstScreen.status)
        const getTestIDApproveStatusTitle = () => Utils.getTestIDObject(TestIDResources.UserProfileDetailFirstScreen.approveStatus)
        const getTestIDFullNamePlaceholder = () => Utils.getTestIDObject(TestIDResources.UserProfileDetailFirstScreen.fullNamePlaceholder)
        const getTestIDEmailPlaceholder = () => Utils.getTestIDObject(TestIDResources.UserProfileDetailFirstScreen.emailPlaceholder)
        const getTestIDDropdownAge = () => Utils.getTestIDObject(TestIDResources.UserProfileDetailFirstScreen.dropdownAge)
        const getTestIDDropdownCity = () => Utils.getTestIDObject(TestIDResources.UserProfileDetailFirstScreen.dropdownCity)
        const getTestIDPickerInterest = () => Utils.getTestIDObject(TestIDResources.UserProfileDetailFirstScreen.pickerInterest)
        const getTestIDPickerHavdSogerHan = () => Utils.getTestIDObject(TestIDResources.UserProfileDetailFirstScreen.pickerHavdSogerHan)
        const getTestIDTagTitle = () => Utils.getTestIDObject(TestIDResources.UserProfileDetailFirstScreen.tagTitle)
        const getTestIDButtonSaveAndContinue = () => Utils.getTestIDObject(TestIDResources.UserProfileDetailFirstScreen.buttonSaveAndContinue)
        const getTestIDModalPicker = () => Utils.getTestIDObject(TestIDResources.CreateAccountScreen.modalPicker)
        const getTestIDErrorMessage = () => Utils.getTestIDObject(TestIDResources.VerifyUserScreen.errorMessage)
        const getTestIDButtonThreeDotsMenu = () => Utils.getTestIDObject(TestIDResources.UserProfileDetailFirstScreen.buttonThreeDotsMenu)
        const getTestIDButtonDeactivateTitle = () => Utils.getTestIDObject(TestIDResources.UserProfileDetailFirstScreen.buttonDeactivate)
        const getTestIDButtonDeleteTitle = () => Utils.getTestIDObject(TestIDResources.UserProfileDetailFirstScreen.buttonDelete)
        return {
            getTestIDStatusTitle,
            getTestIDFullNamePlaceholder,
            getTestIDEmailPlaceholder,
            getTestIDDropdownAge,
            getTestIDDropdownCity,
            getTestIDApproveStatusTitle,
            getTestIDPickerInterest,
            getTestIDPickerHavdSogerHan,
            getTestIDTagTitle,
            getTestIDButtonSaveAndContinue,
            getTestIDModalPicker,
            getTestIDErrorMessage,
            getTestIDButtonThreeDotsMenu,
            getTestIDButtonDeactivateTitle,
            getTestIDButtonDeleteTitle
        }
    })
