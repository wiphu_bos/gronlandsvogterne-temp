import { types } from "mobx-state-tree"
import { UserProfileDetailFirstPropsModel } from "./user-profile-detail-first-page.models"

export const UserProfileDetailFirstActions = types.model(UserProfileDetailFirstPropsModel).actions(self => {

    //MARK: Volatile State

    //MARK: Actions
    const setIsPopoverMenuVisible = (value: boolean) => self.isPopoverMenuVisible = value
    const setIsModalShowing = (value: boolean) => self.isModalShowing = value
    const setIsAlertModalShowing = (value: boolean) => self.isAlertModalShowing = value
    return {
        setIsPopoverMenuVisible,
        setIsModalShowing,
        setIsAlertModalShowing
    }
})