import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { UserProfileDetailFirstStore } from "./storemodels/user-profile-detail-first-page.types"
import { UserProfileDetailStoreTab } from '../../../user-profile-detail-tab/user-profile-detail.types'
import { BaseController } from '../../../../base.controller'

type UserProfileDetailStore =
    | UserProfileDetailFirstStore

export interface UserProfileDetailProps extends NavigationContainerProps<ParamListBase> {
    rootController: BaseController,
    rootViewModel?: UserProfileDetailStoreTab
    observeViewModel?: UserProfileDetailStore
}