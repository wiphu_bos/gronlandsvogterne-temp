// MARK: Import
import { NavigationKey } from "../../../../../constants/app.constant"
import { RootStore, INavigationRoute } from "../../../../../models"
import { UserProfileDetailProps } from "./user-profile-detail-first-page.props"
import { Keyboard } from "react-native"
import { UserProfileDetailFirstResourcesStore, UserProfileDetailFirstStore } from "./storemodels/user-profile-detail-first-page.types"
import { UserProfileDetailFirstResourcesStoreModel, UserProfileDetailFirstStoreModel } from "./storemodels/user-profile-detail-first-page.store"
import * as Validate from "../../../../../utils/local-validate"
import { SelectionType } from "../../../../../models/validation-store/validation-store.model"
import { ISelect } from "../../../../../models/user-store/user.types"
import { PopoverMenuActionType } from "../../../../../components/popover-menu/popover-menu.props"
import * as DataUtils from "../../../../../utils/data.utils"
import { ConfirmModalProps } from "../../../../../components/confirm-modal/confirm-modal.props"
import { BaseController, IAlertParams } from "../../../../base.controller"
import { onSnapshot } from "mobx-state-tree"
import UserProfileDetailController from "../../../user-profile-detail-tab/user-profile-detail.controllers"
import { CommonActions } from "@react-navigation/native"
import { ThreeDotMenuProps } from "../../../../../components/ThreeDotsMenuComponent/three-dot-menu.props"

class UserProfileDetailFirstController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static resourcesViewModel: UserProfileDetailFirstResourcesStore
    public static viewModel: UserProfileDetailFirstStore
    public static myProps: UserProfileDetailProps & Partial<INavigationRoute>

    private static _ageContentList: ISelect[]
    private static _ageSelected: string
    private static _ageSelectedIndex: number

    private static _cityContentList: ISelect[]
    private static _citySelected: string
    private static _citySelectedIndex: number

    private static _havdSogerHanContentList: ISelect[]
    private static _havdSogerHanListSelected: any[]
    private static _havdSogerHanSelectedFirstIndex: number

    private static _interestContentList: ISelect[]
    private static _interestListSelected: any[]
    private static _interestSelectedFirstIndex: number

    private static _tagListSelected: ISelect[]
    private static _selectionTypeSelected: SelectionType


    private static _popoverMenuActionTypeSelected: PopoverMenuActionType
    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, props?: UserProfileDetailProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, props, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(props)
        this._setupViewModel()
        this._setupSnapShot()
        this._getDropdownContents()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        UserProfileDetailFirstController.resourcesViewModel = UserProfileDetailFirstResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.UserProfileDetailFirst)
        UserProfileDetailFirstController.viewModel = localStore && UserProfileDetailFirstStoreModel.create({ ...localStore }) || UserProfileDetailFirstStoreModel.create({})
    }

    private _setupSnapShot = () => {
        onSnapshot(UserProfileDetailFirstController.viewModel, (snap: UserProfileDetailFirstStore) => {
            this._setInitializedToPropsParams()
        })
    }

    /*
       Mark Data
    */


    private _getDropdownContents = () => {
        this._getCityContent()
        this._getAgeContent()
        this._getInterestContent()
        this._getHavdSogerHanContent()
        this._getTagContent()
    }

    public getMultitpleSelectionDataType = (list?: ISelect[]) => list?.map(e => e.value).join(', ').toString()

    private _singleSelectionMenuOptions = {
        onSelect: (item: any) => this.onSelectItem(item),
        onCancel: () => this.onCancelModal()
    }

    private _multipleSelectionMenuOptions = {
        onOK: (items: any[]) => this.onSelectItems(items),
        onClose: () => this.onCloseModal()
    }

    private _getTagContent = () => {
        UserProfileDetailFirstController._tagListSelected = UserProfileDetailController.viewModel?.getTagList || []
    }

    private _getInterestContent = () => {
        UserProfileDetailFirstController._interestContentList = this._rootStore?.getSharedStore?.getInterests || []
        UserProfileDetailFirstController._interestListSelected = UserProfileDetailController.viewModel?.getInterestList && UserProfileDetailController.viewModel?.getInterestList?.map((e) => e) || null
        const interestSelectedFirstObject = UserProfileDetailFirstController._interestListSelected?.length > 0 ? UserProfileDetailFirstController._interestListSelected[0] : 0
        UserProfileDetailFirstController._interestSelectedFirstIndex = UserProfileDetailFirstController._interestContentList?.findIndex((e) => {
            if (!interestSelectedFirstObject) return true
            return e.key === interestSelectedFirstObject?.key
        })
        UserProfileDetailFirstController._interestSelectedFirstIndex = UserProfileDetailFirstController._interestSelectedFirstIndex < 0 ? 0 : UserProfileDetailFirstController._interestSelectedFirstIndex
    }

    private _getHavdSogerHanContent = () => {
        UserProfileDetailFirstController._havdSogerHanContentList = this._rootStore?.getSharedStore?.getHavdSogerHans || []
        UserProfileDetailFirstController._havdSogerHanListSelected = UserProfileDetailController.viewModel?.getHavdSogerHanList && UserProfileDetailController.viewModel?.getHavdSogerHanList?.map((e) => e) || null
        const havdSogerHanSelectedFirstObject = UserProfileDetailFirstController._havdSogerHanListSelected?.length > 0 ? UserProfileDetailFirstController._havdSogerHanListSelected[0] : 0
        UserProfileDetailFirstController._havdSogerHanSelectedFirstIndex = UserProfileDetailFirstController._havdSogerHanContentList?.findIndex((e) => {
            if (!havdSogerHanSelectedFirstObject) return true
            return e.key === havdSogerHanSelectedFirstObject?.key
        })
        UserProfileDetailFirstController._havdSogerHanSelectedFirstIndex = UserProfileDetailFirstController._havdSogerHanSelectedFirstIndex < 0 ? 0 : UserProfileDetailFirstController._havdSogerHanSelectedFirstIndex
    }

    private _getCityContent = () => {
        UserProfileDetailFirstController._cityContentList = this._rootStore?.getSharedStore?.getCities || []
        UserProfileDetailFirstController._citySelected = UserProfileDetailController.viewModel?.getCity?.key
        UserProfileDetailFirstController._citySelectedIndex = UserProfileDetailFirstController._cityContentList?.findIndex((e) => {
            if (!UserProfileDetailFirstController._citySelected) return true
            return e.key === UserProfileDetailFirstController._citySelected
        })
        UserProfileDetailFirstController._citySelectedIndex = UserProfileDetailFirstController._citySelectedIndex < 0 ? 0 : UserProfileDetailFirstController._citySelectedIndex
    }

    private _getAgeContent = () => {
        UserProfileDetailFirstController._ageContentList = this._rootStore?.getSharedStore?.getAge || []
        UserProfileDetailFirstController._ageSelected = UserProfileDetailController.viewModel?.getAge?.key
        UserProfileDetailFirstController._ageSelectedIndex = DataUtils.getAgeSelectedIndex(UserProfileDetailFirstController._ageSelected, UserProfileDetailFirstController._ageContentList)
    }

    public getSelectOptions = () => {
        const cityOptions = {
            flatListViewProps: { selectedIndex: UserProfileDetailFirstController._citySelectedIndex },
            selectedOption: UserProfileDetailFirstController._citySelected,
            placeholderText: UserProfileDetailFirstController.resourcesViewModel?.getResourceModalCityPlaceholder(),
            options: UserProfileDetailFirstController._cityContentList,
            ...this._singleSelectionMenuOptions
        }
        const ageOptions = {
            flatListViewProps: { selectedIndex: UserProfileDetailFirstController._ageSelectedIndex },
            selectedOption: UserProfileDetailFirstController._ageSelected,
            placeholderText: UserProfileDetailFirstController.resourcesViewModel?.getResourceModalAgePlaceholder(),
            options: UserProfileDetailFirstController._ageContentList,
            keyboardType: "phone-pad",
            ...this._singleSelectionMenuOptions
        }
        const havdSogerHanOptions = {
            flatListViewProps: { selectedIndex: UserProfileDetailFirstController._havdSogerHanSelectedFirstIndex },
            isMultipleSelectionDropDown: true,
            selectedOptions: UserProfileDetailController.viewModel?.getHavdSogerHanList &&
                DataUtils.getModalContentList(UserProfileDetailController.viewModel?.getHavdSogerHanList, true),
            placeholderText: UserProfileDetailFirstController.resourcesViewModel?.getResourceModalHavdSogerHandPlaceholder(),
            options: UserProfileDetailFirstController._havdSogerHanContentList,
            ...this._multipleSelectionMenuOptions
        }
        const interestOptions = {
            flatListViewProps: { selectedIndex: UserProfileDetailFirstController._interestSelectedFirstIndex },
            isMultipleSelectionDropDown: true,
            selectedOptions: UserProfileDetailController.viewModel?.getInterestList
                && DataUtils.getModalContentList(UserProfileDetailController.viewModel?.getInterestList, true),
            placeholderText: UserProfileDetailFirstController.resourcesViewModel?.getResourceModalInterestPlaceholder(),
            options: UserProfileDetailFirstController._interestContentList,
            ...this._multipleSelectionMenuOptions
        }

        switch (UserProfileDetailFirstController._selectionTypeSelected) {
            case SelectionType.CITY:
                return cityOptions
            case SelectionType.AGE:
                return ageOptions
            case SelectionType.HavdSogerHan:
                return havdSogerHanOptions
            case SelectionType.Interest:
                return interestOptions
            default:
                return null
        }
    }

    /*
       Mark Event
    */
    private _onOpenAlertModal = () => UserProfileDetailFirstController.viewModel?.setIsAlertModalShowing(true)
    private _onCloseAlertModal = () => UserProfileDetailFirstController.viewModel?.setIsAlertModalShowing(false)

    public onTagPressed = (item: ISelect) => {
        const { key, value } = item
        const params = {
            localViewModel: UserProfileDetailController.viewModel as any,
            rootStore: this._rootStore
        }
        const foundItemIndex = UserProfileDetailFirstController._tagListSelected?.findIndex(v => v.key === key)
        const transformList = UserProfileDetailFirstController._tagListSelected?.map(e => {
            return { key: e.key, value: e.value }
        })

        let selectedList = transformList || []
        if (selectedList?.length > 0 && foundItemIndex !== -1) {
            selectedList.splice(foundItemIndex, 1)
        } else {
            selectedList?.push({ key: key, value: value })
        }
        UserProfileDetailFirstController._tagListSelected = selectedList
        const isInvalid = { isInvalid: UserProfileDetailFirstController._tagListSelected.length < 3 }
        const validateParams = { ...params, ...isInvalid }
        Validate.onErrorMessageTagListSelection(validateParams)
        UserProfileDetailController.viewModel?.setTagList(selectedList)
    }

    public onSelectItem = (item?: any): void => {
        const { key, label } = item
        const selectedItem = { key: key, value: label }
        if (UserProfileDetailFirstController._selectionTypeSelected === SelectionType.CITY) {
            UserProfileDetailFirstController._citySelected = key
            UserProfileDetailController.viewModel?.setCity(selectedItem)
            UserProfileDetailFirstController._citySelectedIndex = UserProfileDetailFirstController._cityContentList?.findIndex((e) => {
                if (!key) return true
                return e.key === key
            })
        } else if (UserProfileDetailFirstController._selectionTypeSelected == SelectionType.AGE) {
            UserProfileDetailFirstController._ageSelected = key
            UserProfileDetailController.viewModel?.setAge(selectedItem)
            UserProfileDetailFirstController._ageSelectedIndex = DataUtils.getAgeSelectedIndex(UserProfileDetailFirstController._ageSelected, UserProfileDetailFirstController._ageContentList)
        }
        this.onCancelModal()
    }

    public onCancelModal = (): void => {
        const params = {
            localViewModel: UserProfileDetailController.viewModel as any,
            rootStore: this._rootStore
        }
        UserProfileDetailFirstController.viewModel?.setIsModalShowing(false)
        if (UserProfileDetailFirstController._selectionTypeSelected === SelectionType.CITY) {
            const isInvalid = { isInvalid: !(UserProfileDetailController.viewModel?.getCity?.key) }
            const validateParams = { ...params, ...isInvalid }
            Validate.onErrorMessageCityField(validateParams)
        } else if (UserProfileDetailFirstController._selectionTypeSelected == SelectionType.AGE) {
            const isInvalid = { isInvalid: !(UserProfileDetailController.viewModel?.getAge?.key) }
            const validateParams = { ...params, ...isInvalid }
            Validate.onErrorMessageAgeField(validateParams)
        }
        UserProfileDetailFirstController._selectionTypeSelected = null
    }

    public onSelectItems = (item?: any[]): void => {
        const selectedItems = item?.map(e => {
            return {
                key: e.key,
                value: e.label
            }
        })
        if (UserProfileDetailFirstController._selectionTypeSelected == SelectionType.HavdSogerHan) {
            UserProfileDetailController.viewModel?.setHavdSogerHanList(selectedItems)
            UserProfileDetailFirstController._havdSogerHanSelectedFirstIndex = UserProfileDetailFirstController._havdSogerHanContentList?.findIndex((e) => {
                if (!(selectedItems?.length > 0)) return true
                return e.key === selectedItems[0].key
            })
            UserProfileDetailFirstController._havdSogerHanSelectedFirstIndex = UserProfileDetailFirstController._havdSogerHanSelectedFirstIndex < 0 ? 0 : UserProfileDetailFirstController._havdSogerHanSelectedFirstIndex
            UserProfileDetailFirstController._havdSogerHanListSelected = item
        } else if (UserProfileDetailFirstController._selectionTypeSelected == SelectionType.Interest) {
            UserProfileDetailController.viewModel?.setInterestList(selectedItems)
            UserProfileDetailFirstController._interestSelectedFirstIndex = UserProfileDetailFirstController._interestContentList?.findIndex((e) => {
                if (!(selectedItems?.length > 0)) return true
                return e.key === selectedItems[0].key
            })
            UserProfileDetailFirstController._interestSelectedFirstIndex = UserProfileDetailFirstController._interestSelectedFirstIndex < 0 ? 0 : UserProfileDetailFirstController._interestSelectedFirstIndex
            UserProfileDetailFirstController._interestListSelected = item
        }
        this.onCloseModal()
    }

    public onCloseModal = (): void => {
        const params = {
            localViewModel: UserProfileDetailController.viewModel as any,
            rootStore: this._rootStore
        }
        UserProfileDetailFirstController.viewModel?.setIsModalShowing(false)
        if (UserProfileDetailFirstController._selectionTypeSelected == SelectionType.HavdSogerHan) {
            const isInvalid = { isInvalid: !(UserProfileDetailController.viewModel?.getHavdSogerHanList?.length > 0) }
            const validateParams = { ...params, ...isInvalid }
            Validate.onErrorMessageHavdSogerHanField(validateParams)
        } else if (UserProfileDetailFirstController._selectionTypeSelected == SelectionType.Interest) {
            const isInvalid = { isInvalid: !(UserProfileDetailController.viewModel?.getInterestList?.length > 0) }
            const validateParams = { ...params, ...isInvalid }
            Validate.onErrorMessageInterestField(validateParams)
        }
        UserProfileDetailFirstController._selectionTypeSelected = null
    }

    public onFullNameChangeText = (value: string): void => {
        const params = {
            value: value,
            localViewModel: UserProfileDetailController.viewModel as any,
        }
        Validate.onFullNameChangeText(params)
    }

    public onBlurFullNameTextField = (): void => {
        const fullName = UserProfileDetailController.viewModel?.getFullName
        const params = {
            value: fullName,
            localViewModel: UserProfileDetailController.viewModel as any,
            rootStore: this._rootStore
        }
        Validate.onBlurFullNameTextField(params)
    }

    public onTouchAgeTextField = () => {
        Keyboard.dismiss()
        UserProfileDetailFirstController._selectionTypeSelected = SelectionType.AGE
        UserProfileDetailFirstController.viewModel?.setIsModalShowing(true)
    }

    public onTouchCityTextField = () => {
        Keyboard.dismiss()
        UserProfileDetailFirstController._selectionTypeSelected = SelectionType.CITY
        UserProfileDetailFirstController.viewModel?.setIsModalShowing(true)
    }

    public onTouchHavdSogerHanTextField = () => {
        Keyboard.dismiss()
        UserProfileDetailFirstController._selectionTypeSelected = SelectionType.HavdSogerHan
        UserProfileDetailFirstController.viewModel?.setIsModalShowing(true)
    }

    public onTouchInterestTextField = () => {
        Keyboard.dismiss()
        UserProfileDetailFirstController._selectionTypeSelected = SelectionType.Interest
        UserProfileDetailFirstController.viewModel?.setIsModalShowing(true)
    }

    public onPopoverMenuPressed = () => {
        const { getIsPopoverMenuVisible } = UserProfileDetailFirstController.viewModel
        UserProfileDetailFirstController.viewModel?.setIsPopoverMenuVisible(!getIsPopoverMenuVisible)
    }

    public onTogglePopoverMenu = () => {
        const { getIsPopoverMenuVisible } = UserProfileDetailFirstController.viewModel
        UserProfileDetailFirstController.viewModel?.setIsPopoverMenuVisible(!getIsPopoverMenuVisible)
    }

    public onSaveAndContinueButtonDidTouch = () => {
        if (!UserProfileDetailController.viewModel?.getIsFirstPageValid) {
            const params: IAlertParams = {
                title: UserProfileDetailController.resourcesViewModel?.getResourceAlertValidateUserTitle(),
                message: UserProfileDetailController.resourcesViewModel?.getResourceAlertValidateUserMessage()
            }
            this._generalAlertOS(params)
        } else {
            this._myProps?.navigation?.dispatch(CommonActions.navigate(NavigationKey.UserProfileDetailSecond) as any)
        }
    }

    public forceClosePopoverMenu = () => UserProfileDetailFirstController.viewModel?.setIsPopoverMenuVisible(false)

    public onPopoverMenuItemPressed = (type?: PopoverMenuActionType) => {
        UserProfileDetailFirstController._popoverMenuActionTypeSelected = type
        this.forceClosePopoverMenu()

        /* Work arround for setting timeoout because `onPopoverClosed` is not working*/
        setTimeout(() => {
            this.displayDialog()
        }, 300);
    }

    /* not working, will solve later */
    public onPopoverClosed = () => {
        // onOpenAlertModal()
    }

    public onAlertCancel = () => {
        UserProfileDetailFirstController._popoverMenuActionTypeSelected = null
        this._onCloseAlertModal()
    }

    public onConfirmAlertModalPress = async () => {
        this._onCloseAlertModal()
    }

    public onAlertModalClosed = async () => {
        if (!UserProfileDetailFirstController._popoverMenuActionTypeSelected) return
        this._isGlobalLoading(true)
        await UserProfileDetailFirstController.viewModel?.deleteMyUser(this._rootStore)
        this._isGlobalLoading(false)
    }

    public threeDotsMenuOptions: ThreeDotMenuProps = {
        onPopoverMenuPress: this.onTogglePopoverMenu,
        forceClosePopoverMenu: this.forceClosePopoverMenu,
        onPopoverMenuItemPressed: this.onPopoverMenuItemPressed,
        onPopoverClosed: this.onPopoverClosed
    }

    public displayDialog = () => {
        if (UserProfileDetailFirstController._popoverMenuActionTypeSelected === PopoverMenuActionType.Delete) {
            this._onOpenAlertModal()
        } else {
            UserProfileDetailController.onSaveAndContinueDidTouch()
        }
    }

    public getConfirmAlertModalOptions = (): ConfirmModalProps => {
        let options = {
            okButtonText: UserProfileDetailFirstController.resourcesViewModel?.getResourceModalButtonOKTitle(),
            clearButtonText: UserProfileDetailFirstController.resourcesViewModel?.getResourceModalButtonCancelTitle(),
            isVisible: UserProfileDetailFirstController.viewModel?.getIsAlertModalShowing || false,
            onCancel: this.onAlertCancel,
            onOK: this.onConfirmAlertModalPress,
            onModalClosed: this.onAlertModalClosed,
            message: UserProfileDetailFirstController.resourcesViewModel?.getResourceModalConfirmDeleteUser()
        }
        return options
    }


    /*
       Mark Life cycle
    */

    //@override
    viewWillDisappear = () => {
        super.viewWillDisappear && super.viewWillDisappear()
        this.forceClosePopoverMenu()
    }

}

export default UserProfileDetailFirstController