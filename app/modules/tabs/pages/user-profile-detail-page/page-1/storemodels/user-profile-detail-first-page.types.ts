import { Instance, SnapshotOut } from "mobx-state-tree"
import { UserProfileDetailFirstStoreModel, UserProfileDetailFirstResourcesStoreModel } from "./user-profile-detail-first-page.store"

export type UserProfileDetailFirstStore = Instance<typeof UserProfileDetailFirstStoreModel>
export type UserProfileDetailFirstStoreSnapshot = SnapshotOut<typeof UserProfileDetailFirstStoreModel>

export type UserProfileDetailFirstResourcesStore = Instance<typeof UserProfileDetailFirstResourcesStoreModel>
export type UserProfileDetailFirstResourcesStoreSnapshot = SnapshotOut<typeof UserProfileDetailFirstResourcesStoreModel>