import { types } from "mobx-state-tree"
import { StoreName } from "../../../../../../constants/app.constant"
import { UserProfileDetailFirstPropsModel } from "../viewmodels/user-profile-detail-first-page.models"
import { UserProfileDetailFirstActions } from "../viewmodels/user-profile-detail-first-page.actions"
import { UserProfileDetailFirstViews, UserProfileDetailFirstResourcesViews } from "../viewmodels/user-profile-detail-first-page.views"
import { GeneralResourcesStoreModel } from "../../../../../../models/general-resources-store"
import { UserProfileDetailFirstServiceActions } from "../services/user-profile-detail-first-page.services"

const UserProfileDetailFirstModel = types.model(StoreName.UserProfileDetailFirst, UserProfileDetailFirstPropsModel)
export const UserProfileDetailFirstStoreModel = types.compose(UserProfileDetailFirstModel, UserProfileDetailFirstViews, UserProfileDetailFirstActions, UserProfileDetailFirstServiceActions)
export const UserProfileDetailFirstResourcesStoreModel = types.compose(GeneralResourcesStoreModel, UserProfileDetailFirstResourcesViews)

