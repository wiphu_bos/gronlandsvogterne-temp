// MARK: Import

import React, { useRef } from "react"
import { observer } from "mobx-react-lite"
import { UserProfileDetailProps } from "./user-profile-detail-first-page.props"
import LinearGradient from "react-native-linear-gradient"
import FloatLabelTextInput from '../../../../../libs/float-label-textfield/float-label-text-field'
import { useConfigurate } from "../../../../../custom-hooks/use-configure-controller"
import { View } from "react-native-animatable"
import { KeyboardAvoidingView } from "react-native"
import * as metric from "../../../../../theme"
import { AnimatedScrollView, ConfirmModal } from "../../../../../components"
import ModalFilterPicker from '../../../../../libs/modal-filter-picker/modal-filter-picker'
import { INavigationRoute } from "../../../../../models/navigation-store/navigation.types"
import { GradientButton } from "../../../../../components/gradient-button/button"
import { UserTagsComponent } from "../../../../../components/user-tags"
import { ThreeDotsMenuComponent } from "../../../../../components/ThreeDotsMenuComponent/three-dot-menu"
import { StatusComponent } from "../../../../../components/user-status/user-status"
import UserProfileDetailFirstController from "./user-profile-detail-first-page.controllers"

// MARK: Style Import

import * as Styles from "./user-profile-detail-first-page.styles"
import UserProfileDetailController from "../../../user-profile-detail-tab/user-profile-detail.controllers"

export const UserProfileDetailFirstScreen: React.FunctionComponent<UserProfileDetailProps & Partial<INavigationRoute>> = observer((props) => {

    const controller = useConfigurate(UserProfileDetailFirstController, props) as UserProfileDetailFirstController

    const ageTextInput = useRef(null)
    const cityTextInput = useRef(null)
    const lookingForTextInput = useRef(null)
    const interestTextInput = useRef(null)

    // MARK: Render
    return (
        <LinearGradient shouldRasterizeIOS style={Styles.CONTAINNER} {...Styles.TAB_PAGE_BACKGROUND}>
            <View shouldRasterizeIOS style={Styles.BODY_CONTAINER}>
                <KeyboardAvoidingView behavior={metric.keyboardBehavior} style={Styles.AVOID_KEYBOARD_VIEW}>
                    <AnimatedScrollView
                        {...metric.solidScrollView}
                        contentContainerStyle={Styles.SCROLL_VIEW}>
                        <View shouldRasterizeIOS style={Styles.CONTENT_CONTAINER}>
                            <View shouldRasterizeIOS style={Styles.HEADER}>
                                <StatusComponent
                                    testID={UserProfileDetailFirstController.resourcesViewModel?.getTestIDStatusTitle()}
                                    statusText={UserProfileDetailFirstController.resourcesViewModel?.getResourceStatusTitle()}
                                    statusValue={
                                        UserProfileDetailFirstController.resourcesViewModel?.getResourceUserStatusTitle(UserProfileDetailController?.viewModel?.getStatus)
                                    }
                                />
                                <ThreeDotsMenuComponent
                                    publishText={UserProfileDetailFirstController.resourcesViewModel?.getResourceButtonPublish()}
                                    unPublishText={UserProfileDetailFirstController.resourcesViewModel?.getResourceButtonUnPublish()}
                                    buttonDeleteText={UserProfileDetailFirstController.resourcesViewModel?.getResourceButtonDeleteTitle()}
                                    testID={UserProfileDetailFirstController.resourcesViewModel?.getTestIDButtonThreeDotsMenu()}
                                    rootViewModel={UserProfileDetailController.viewModel}
                                    observeViewModel={UserProfileDetailFirstController.viewModel}
                                    {...controller.threeDotsMenuOptions} />
                            </View>
                            <View shouldRasterizeIOS style={Styles.TEXT_FIELD_CONTAINER}>
                                <FloatLabelTextInput
                                    {...UserProfileDetailFirstController.resourcesViewModel?.getTestIDFullNamePlaceholder()}
                                    placeholder={UserProfileDetailFirstController.resourcesViewModel?.getResourceFullNamePlaceholder()}
                                    onChangeTextValue={controller.onFullNameChangeText}
                                    value={UserProfileDetailController.viewModel?.getFullName}
                                    noBorder
                                    returnKeyType="next"
                                    onSubmitEditing={() => { ageTextInput.current.focus() }}
                                    errorMessage={UserProfileDetailController.viewModel?.getFullNameErrorMessage}
                                    testIDErrorMessage={UserProfileDetailFirstController.resourcesViewModel?.getTestIDErrorMessage()}
                                    onBlur={controller.onBlurFullNameTextField}
                                    containerStyle={Styles.TEXT_FIELD_ITEM}
                                />
                                <FloatLabelTextInput
                                    editable={UserProfileDetailController.viewModel?.getEmail ? false : true}
                                    disabled={UserProfileDetailController.viewModel?.getEmail ? true : false}
                                    returnKeyType="next"
                                    keyboardType="email-address"
                                    {...UserProfileDetailFirstController.resourcesViewModel?.getTestIDEmailPlaceholder()}
                                    placeholder={UserProfileDetailFirstController.resourcesViewModel?.getResourceEmailPlaceholder()}
                                    value={UserProfileDetailController.viewModel?.getEmail}
                                    noBorder
                                    containerStyle={Styles.TEXT_FIELD_ITEM}
                                />

                                <FloatLabelTextInput
                                    ref={ageTextInput}
                                    {...UserProfileDetailFirstController.resourcesViewModel?.getTestIDDropdownAge()}
                                    editable={false}
                                    value={UserProfileDetailController.viewModel?.getAge?.value || (UserProfileDetailFirstController.resourcesViewModel?.getResourceAgePlaceholder())}
                                    noBorder
                                    isDropDown
                                    onTouchTextFieldXL={controller.onTouchAgeTextField}
                                    errorMessage={UserProfileDetailController.viewModel?.getAgeErrorMessage}
                                    testIDErrorMessage={UserProfileDetailFirstController.resourcesViewModel?.getTestIDErrorMessage()}
                                    returnKeyType="next"
                                    onSubmitEditing={() => { cityTextInput.current.focus() }}
                                    containerStyle={Styles.TEXT_FIELD_ITEM}
                                />
                                <FloatLabelTextInput
                                    ref={cityTextInput}
                                    isDropDown
                                    {...UserProfileDetailFirstController.resourcesViewModel?.getTestIDDropdownCity()}
                                    editable={false}
                                    value={UserProfileDetailController.viewModel?.getCity?.value || (UserProfileDetailFirstController.resourcesViewModel?.getResourceCityPlaceholder())}
                                    noBorder
                                    onTouchTextFieldXL={controller.onTouchCityTextField}
                                    errorMessage={UserProfileDetailController.viewModel?.getCityErrorMessage}
                                    testIDErrorMessage={UserProfileDetailFirstController.resourcesViewModel?.getTestIDErrorMessage()}
                                    containerStyle={Styles.TEXT_FIELD_ITEM}
                                />
                                <FloatLabelTextInput
                                    ref={lookingForTextInput}
                                    {...UserProfileDetailFirstController.resourcesViewModel?.getTestIDPickerHavdSogerHan()}
                                    editable={false}
                                    value={UserProfileDetailFirstController.resourcesViewModel?.getResourceHavdSogerHanPlaceholder()}
                                    noBorder
                                    isMultipleSelectionDropDown
                                    onTouchTextFieldXL={controller.onTouchHavdSogerHanTextField}
                                    multipleSelectionItems={controller.getMultitpleSelectionDataType(UserProfileDetailController.viewModel?.getHavdSogerHanList)}
                                    testIDMultipleSelectionItems=""
                                    errorMessage={UserProfileDetailController.viewModel?.getHavdSogerHanListErrorMessage}
                                    testIDErrorMessage={UserProfileDetailFirstController.resourcesViewModel?.getTestIDErrorMessage()}
                                    containerStyle={Styles.TEXT_FIELD_ITEM}
                                />
                                <FloatLabelTextInput
                                    ref={interestTextInput}
                                    {...UserProfileDetailFirstController.resourcesViewModel?.getTestIDPickerInterest()}
                                    editable={false}
                                    value={UserProfileDetailFirstController.resourcesViewModel?.getResourceInterestPlaceholder()}
                                    noBorder
                                    isMultipleSelectionDropDown
                                    onTouchTextFieldXL={controller.onTouchInterestTextField}
                                    multipleSelectionItems={controller.getMultitpleSelectionDataType(UserProfileDetailController.viewModel?.getInterestList)}
                                    testIDMultipleSelectionItems=""
                                    errorMessage={UserProfileDetailController.viewModel?.getInterestListErrorMessage}
                                    testIDErrorMessage={UserProfileDetailFirstController.resourcesViewModel?.getTestIDErrorMessage()}
                                    containerStyle={Styles.TEXT_FIELD_ITEM}
                                />
                            </View>
                            <UserTagsComponent
                                observeViewModel={UserProfileDetailController.viewModel}
                                tagTitle={UserProfileDetailFirstController.resourcesViewModel?.getResourceTagTitle()}
                                onTagPressed={controller.onTagPressed}
                                tagList={UserProfileDetailFirstController.resourcesViewModel?.getTags} />
                            <GradientButton
                                {...UserProfileDetailFirstController.resourcesViewModel?.getTestIDButtonSaveAndContinue()}
                                text={UserProfileDetailFirstController.resourcesViewModel?.getResourceButtonSaveAndContinueTitle()}
                                containerStyle={Styles.BUTTON_MARGIN}
                                onPress={controller.onSaveAndContinueButtonDidTouch} />
                        </View>
                    </AnimatedScrollView>
                </KeyboardAvoidingView>
            </View>
            <ModalFilterPicker
                {...UserProfileDetailFirstController.resourcesViewModel?.getTestIDModalPicker()}
                cancelButtonText={UserProfileDetailFirstController.resourcesViewModel?.getResourceModalButtonCancelTitle()}
                okButtonText={UserProfileDetailFirstController.resourcesViewModel?.getResourceModalButtonOKTitle()}
                clearButtonText={UserProfileDetailFirstController.resourcesViewModel?.getResourceModalButtonClearTitle()}
                visible={UserProfileDetailFirstController.viewModel?.getIsModalShowing || false}
                {...controller.getSelectOptions()}
                iconSelectionStyle={Styles.ICON_SELECTION}
            />
            <ConfirmModal
                // {...UserProfileDetailFirstController.resourcesViewModel?.getTestIDConfirmModal()}
                {...controller.getConfirmAlertModalOptions()}
            />
        </LinearGradient>
    )
})
