import { Instance, SnapshotOut } from "mobx-state-tree"
import { DudeProfileDetailSendEmailStoreModel, DudeProfileDetailSendEmailResourcesStoreModel } from "./dude-profile-send-email-page.store"

export type DudeProfileDetailSendEmailStore = Instance<typeof DudeProfileDetailSendEmailStoreModel>
export type DudeProfileDetailSendEmailStoreSnapshot = SnapshotOut<typeof DudeProfileDetailSendEmailStoreModel>

export type DudeProfileDetailSendEmailResourcesStore = Instance<typeof DudeProfileDetailSendEmailResourcesStoreModel>
export type DudeProfileDetailSendEmailResourcesStoreSnapshot = SnapshotOut<typeof DudeProfileDetailSendEmailResourcesStoreModel>