import { types } from "mobx-state-tree"
import { StoreName } from "../../../../../../constants/app.constant"
import { DudeProfileDetailSendEmailPropsModel } from "../viewmodels/dude-profile-send-email-page.models"
import { DudeProfileDetailSendEmailActions } from "../viewmodels/dude-profile-send-email-page.actions"
import { DudeProfileDetailSendEmailViews, DudeProfileDetailSendEmailResourcesViews } from "../viewmodels/dude-profile-send-email-page.views"
import { GeneralResourcesStoreModel } from "../../../../../../models/general-resources-store"
import { DudeProfileDetailSendEmailServiceActions } from "../services/dude-profile-send-email-page.services"

const DudeProfileDetailSendEmailModel = types.model(StoreName.DudeProfileDetailSendEmail, DudeProfileDetailSendEmailPropsModel)
export const DudeProfileDetailSendEmailStoreModel = types.compose(
    DudeProfileDetailSendEmailModel,
    DudeProfileDetailSendEmailViews,
    DudeProfileDetailSendEmailActions,
    DudeProfileDetailSendEmailServiceActions
)
export const DudeProfileDetailSendEmailResourcesStoreModel = types.compose(GeneralResourcesStoreModel, DudeProfileDetailSendEmailResourcesViews)

