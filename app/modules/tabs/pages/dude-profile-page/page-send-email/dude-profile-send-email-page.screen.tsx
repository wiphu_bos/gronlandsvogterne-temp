// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { DudeProfileDetailSendEmailScreenProps } from "./dude-profile-send-email-page.props"
import LinearGradient from "react-native-linear-gradient"
import { useConfigurate } from "../../../../../custom-hooks/use-configure-controller"
import DudeProfileSendEmailPageController from "./dude-profile-send-email-page.controllers"
import { INavigationRoute } from "../../../../../models/navigation-store/navigation.types"
import { View } from "react-native"
import { TextInput } from "react-native-gesture-handler"
import { KeyboardAvoidingView } from "react-native"
import * as metric from "../../../../../theme"
import { GradientButton } from "../../../../../components/gradient-button/button"
import { Text, AnimatedScrollView } from "../../../../../components"
import * as StringUtils from "../../../../../utils/string.utils"
import { images } from "../../../../../theme/images"
import FastImage from 'react-native-fast-image'

// MARK: Style Import

import * as Styles from "./dude-profile-send-email-page.styles"


export const DudeProfileDetailSendEmailScreen: React.FunctionComponent<DudeProfileDetailSendEmailScreenProps & Partial<INavigationRoute>> = observer((props) => {

    const controller = useConfigurate(DudeProfileSendEmailPageController, props) as DudeProfileSendEmailPageController

    // MARK: Render
    return (
        <LinearGradient shouldRasterizeIOS style={Styles.CONTAINNER} {...Styles.TAB_PAGE_BACKGROUND}>
            <KeyboardAvoidingView 
            behavior={metric.keyboardBehavior} 
            style={Styles.AVOID_KEYBOARD_VIEW} 
            keyboardVerticalOffset={metric.isIPhone ? metric.ratioHeight(40) : 0}
            >
                <AnimatedScrollView
                    contentContainerStyle={Styles.SCROLL_VIEW}
                    {...metric.solidScrollView}
                >
                    <View shouldRasterizeIOS style={Styles.CONTENT_CONTAINER}>
                        <View shouldRasterizeIOS style={Styles.HEADER}>
                            <FastImage source={images.dudeWhite} resizeMode={FastImage.resizeMode.contain} style={Styles.IMAGE_TITLE} />
                            <Text text={DudeProfileSendEmailPageController.resourcesViewModel?.getResourceMenuDescription()}
                                {...DudeProfileSendEmailPageController.resourcesViewModel?.getTestIDTitle()}
                                style={Styles.TITLE} />
                        </View>
                        <Text text={DudeProfileSendEmailPageController.resourcesViewModel?.getResourceWriteYourMessageTitle()}
                            {...DudeProfileSendEmailPageController.resourcesViewModel?.getResourceWriteYourMessageTitle}
                            style={Styles.TEXT_AREA_TITLE} />
                        <TextInput
                            style={{ ...Styles.TEXT_AREA, ...Styles.TEXT_AREA_TEXT }}
                            {...Styles.TEXT_AREA_OPTIONS}
                            autoCorrect={false}
                            onChangeText={controller.onEmailContentChangeText}
                            testIDErrorMessage={DudeProfileSendEmailPageController.resourcesViewModel?.getTestIDWriteYourMessageTitle()}
                            onBlur={controller.onBlurEmailContentTextField}
                        >
                            {StringUtils.brToNewLine(DudeProfileSendEmailPageController.viewModel?.getEmailContent)}
                        </TextInput>
                    </View>
                    <GradientButton
                        preset={controller.isValid() ? "shadow" : "disabled"}
                        {...DudeProfileSendEmailPageController.resourcesViewModel?.getTestIDButtonSendEmailTitle()}
                        text={DudeProfileSendEmailPageController.resourcesViewModel?.getResourceButtonSendEmailTitle()}
                        containerStyle={Styles.BUTTON_MARGIN}
                        onPress={controller.onSendEmailButtonDidTouch} />
                </AnimatedScrollView>

            </KeyboardAvoidingView>
        </LinearGradient>
    )
})
