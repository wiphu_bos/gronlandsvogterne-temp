import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { CreateEditProfileStore } from "../../../create-edit-profile-tab/storemodels"
import { DudeProfileDetailSendEmailStore } from "./storemodels/dude-profile-send-email-page.types"

type DudeProfileStoreModel =
    | DudeProfileDetailSendEmailStore

export interface DudeProfileDetailSendEmailScreenProps extends NavigationContainerProps<ParamListBase> {
    rootViewModel?: CreateEditProfileStore
    viewModel?: DudeProfileStoreModel
}