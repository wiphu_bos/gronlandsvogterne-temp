// MARK: Import
import { NavigationKey } from "../../../../../constants/app.constant"
import { RootStore, INavigationRoute } from "../../../../../models"
import { DudeProfileDetailSendEmailScreenProps } from "./dude-profile-send-email-page.props"
import { DudeProfileDetailSendEmailResourcesStore, DudeProfileDetailSendEmailStore } from "./storemodels/dude-profile-send-email-page.types"
import { DudeProfileDetailSendEmailResourcesStoreModel, DudeProfileDetailSendEmailStoreModel } from "./storemodels/dude-profile-send-email-page.store"
import DudeProfileDetailController from "../../../dude-profile-detail-tab/dude-profile-detail.controllers"
import { Keyboard } from "react-native"
import { StackActions } from "@react-navigation/core"
import { images } from "../../../../../theme/images"
import { BaseController } from "../../../../base.controller"
import { onSnapshot } from "mobx-state-tree"

class DudeProfileSendEmailPageController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static resourcesViewModel: DudeProfileDetailSendEmailResourcesStore
    public static viewModel: DudeProfileDetailSendEmailStore
    public static myProps: DudeProfileDetailSendEmailScreenProps & Partial<INavigationRoute>

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, props?: DudeProfileDetailSendEmailScreenProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, props, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(props)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        DudeProfileSendEmailPageController.resourcesViewModel = DudeProfileDetailSendEmailResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.EmailSent)
        DudeProfileSendEmailPageController.viewModel = localStore && DudeProfileDetailSendEmailStoreModel.create({ ...localStore }) || DudeProfileDetailSendEmailStoreModel.create({})
    }

    private _setupSnapShot = () => {
        onSnapshot(DudeProfileSendEmailPageController.viewModel, (snap: DudeProfileDetailSendEmailStore) => {
            this._setInitializedToPropsParams()
        })
    }

    /*
       Mark Data
   */

    /*
       Mark Event
   */

    public onEmailContentChangeText = (value: string): void => {
        DudeProfileSendEmailPageController.viewModel?.setEmailContent(value)
    }

    public onBlurEmailContentTextField = (): void => {

    }

    private _sendEmail = async () => {
        const dudeId = DudeProfileDetailController.viewModel?.getUserId
        await DudeProfileSendEmailPageController.viewModel?.sendEmailToDude(dudeId)
    }

    public onSendEmailButtonDidTouch = async () => {
        if (!this.isValid()) return
        this._isGlobalLoading(true)
        await this._sendEmail()
        this._isGlobalLoading(false)
        //gotoSendEmailSuccessScreen
        const params = {
            title: DudeProfileSendEmailPageController.resourcesViewModel.getResourceEmailSendSuccessTitle(),
            description: DudeProfileSendEmailPageController.resourcesViewModel.getResourceEmailSendSuccessDescription(),
            firstContent: [DudeProfileDetailController.viewModel?.getFullName, DudeProfileDetailController.viewModel?.getAge.value].join(', '),
            secondContent: DudeProfileDetailController.viewModel?.getCity?.value,
            secondIcon: images.whitePin,
            screenState: NavigationKey.EmailSent
        }
        const firstSequenceAction: any = StackActions.popToTop()
        const secondSequenceAction: any = StackActions.push(NavigationKey.EmailSendSuccess, params)
        Promise.all([this._myProps?.navigation?.dispatch(firstSequenceAction), this._myProps?.navigation?.dispatch(secondSequenceAction)])
    }

    /*
       Mark Life cycle
   */

    //@override
    viewWillDisappear = () => {
        super.viewWillDisappear && super.viewWillDisappear()
        Keyboard.dismiss()
    }

    /*
        Helper
    */
    public isValid = () => {
        return DudeProfileSendEmailPageController.viewModel?.getEmailContent || false
    }

}

export default DudeProfileSendEmailPageController