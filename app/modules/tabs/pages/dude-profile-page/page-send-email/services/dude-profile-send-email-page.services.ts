
import { types, flow } from "mobx-state-tree"
import { APIResponse, APISuccessResponse } from "../../../../../../constants/service.types"
import DudeServices from "../../../../../../services/api-domain/dude.services"
import { DudeProfileDetailSendEmailPropsModel } from "../viewmodels"
import * as StringUtils from "../../../../../../utils/string.utils"
export const DudeProfileDetailSendEmailServiceActions = types.model(DudeProfileDetailSendEmailPropsModel).actions(self => {
    const sendEmailToDude = flow(function* (dude_id: string) {
        const params = {
            uid: dude_id,
            message: StringUtils.newLineToBr(self.emailContent)
        }
        try {
            const result: APIResponse = yield DudeServices.sendEmailToDude(params)
            let success = (result as APISuccessResponse)?.data
            return success
        } catch (e) {
            return e
        }
    })

    return { sendEmailToDude }
})