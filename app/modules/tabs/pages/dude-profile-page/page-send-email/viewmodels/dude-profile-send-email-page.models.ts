import { types } from "mobx-state-tree"

export const DudeProfileDetailSendEmailPropsModel = {
    emailContent: types.maybeNull(types.string)
} 