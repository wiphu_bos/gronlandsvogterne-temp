import { types } from "mobx-state-tree"
import { DudeProfileDetailSendEmailPropsModel } from "./dude-profile-send-email-page.models"
import { GeneralResources } from "../../../../../../constants/firebase/remote-config"
import { TestIDResources } from "../../../../../../constants/test-key/test.constant"
import * as Utils from "../../../../../../utils"
import { GeneralResourcesStoreModel } from "../../../../../../models/general-resources-store"
import { NavigationKey } from "../../../../../../constants/app.constant"

export const DudeProfileDetailSendEmailViews = types.model(DudeProfileDetailSendEmailPropsModel)
    .views(self => ({
        get getEmailContent() {
            return self.emailContent
        }
    }))

export const DudeProfileDetailSendEmailResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State

    const { DudeProfileDetailSendEmailScreen,EmailSendToUserSuccessScreen } = GeneralResources
    const {
        sendEmailDescription,
        writeYourMessageTitle,
        buttonSendEmailTitle
    } = DudeProfileDetailSendEmailScreen

    const {
        title,
        description
    } = EmailSendToUserSuccessScreen

    //MARK: Views
    const getShareResources = (key: string) => self.getValues(key, true)
    const getResources = (key: string) => self.getValues(NavigationKey.DudeProfileDetailScreen, key)
    const getResourcesEmailSendToUserSuccessScreen = (key: string) => self.getValues(NavigationKey.EmailSendToUserSuccessScreen, key)
    const getResourceMenuDescription = () => getResources(sendEmailDescription)
    const getResourceWriteYourMessageTitle = () => getShareResources(writeYourMessageTitle)
    const getResourceButtonSendEmailTitle = () => getShareResources(buttonSendEmailTitle)

    const getResourceEmailSendSuccessTitle = () => getResourcesEmailSendToUserSuccessScreen(title)
    const getResourceEmailSendSuccessDescription = () => getResourcesEmailSendToUserSuccessScreen(description)

    return {
        getResourceMenuDescription,
        getResourceWriteYourMessageTitle,
        getResourceButtonSendEmailTitle,
        getResourceEmailSendSuccessTitle,
        getResourceEmailSendSuccessDescription
    }
})
    .views(self => {

        //MARK: Volatile State

        //MARK: Views

        const getTestIDTitle = () => Utils.getTestIDObject(TestIDResources.DudeProfileDetailSendEmailScreen.title)
        const getTestIDWriteYourMessageTitle = () => Utils.getTestIDObject(TestIDResources.DudeProfileDetailSendEmailScreen.writeYourMessageTitle)
        const getTestIDButtonSendEmailTitle = () => Utils.getTestIDObject(TestIDResources.DudeProfileDetailSendEmailScreen.buttonSendEmailTitle)

        return {
            getTestIDTitle,
            getTestIDWriteYourMessageTitle,
            getTestIDButtonSendEmailTitle
        }
    })
