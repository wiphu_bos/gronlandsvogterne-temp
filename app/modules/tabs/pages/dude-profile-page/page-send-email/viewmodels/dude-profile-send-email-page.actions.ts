import { types } from "mobx-state-tree"
import { DudeProfileDetailSendEmailPropsModel } from "./dude-profile-send-email-page.models"

export const DudeProfileDetailSendEmailActions = types.model(DudeProfileDetailSendEmailPropsModel).actions(self => {

    //MARK: Volatile State
    const setEmailContent = (value: string) => { 
        self.emailContent = value
    }
    //MARK: Actions
    return {
        setEmailContent
    }
})