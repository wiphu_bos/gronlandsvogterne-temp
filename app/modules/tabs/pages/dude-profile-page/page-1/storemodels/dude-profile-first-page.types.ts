import { Instance, SnapshotOut } from "mobx-state-tree"
import { DudeProfileFirstScreenStoreModel, DudeProfileFirstScreenResourcesStoreModel } from "./dude-profile-first-page.store"

export type DudeProfileFirstScreenStore = Instance<typeof DudeProfileFirstScreenStoreModel>
export type DudeProfileFirstScreenStoreSnapshot = SnapshotOut<typeof DudeProfileFirstScreenStoreModel>

export type DudeProfileFirstScreenResourcesStore = Instance<typeof DudeProfileFirstScreenResourcesStoreModel>
export type DudeProfileFirstScreenResourcesStoreSnapshot = SnapshotOut<typeof DudeProfileFirstScreenResourcesStoreModel>