import { types } from "mobx-state-tree"
import { StoreName } from "../../../../../../constants/app.constant"
import { DudeProfileFirstScreenPropsModel } from "../viewmodels/dude-profile-first-page.models"
import { DudeProfileFirstScreenActions } from "../viewmodels/dude-profile-first-page.actions"
import { DudeProfileFirstScreenViews, DudeProfileFirstScreenResourcesViews } from "../viewmodels/dude-profile-first-page.views"
import { GeneralResourcesStoreModel } from "../../../../../../models/general-resources-store"

const DudeProfileFirstScreenModel = types.model(StoreName.DudeProfileDetailFirst, DudeProfileFirstScreenPropsModel)
export const DudeProfileFirstScreenStoreModel = types.compose(DudeProfileFirstScreenModel, DudeProfileFirstScreenViews, DudeProfileFirstScreenActions)
export const DudeProfileFirstScreenResourcesStoreModel = types.compose(GeneralResourcesStoreModel, DudeProfileFirstScreenResourcesViews)

