import { ViewStyle, TextStyle } from "react-native"
import { color } from "../../../../../theme"
import * as metric from "../../../../../theme"
import { RFValue } from "react-native-responsive-fontsize"

export const FULL: ViewStyle = {
    flex: 1
}
export const TAB_PAGE_BACKGROUND: any = { colors: color.tabPageBackground }

export const CONTAINNER: ViewStyle = {
    ...FULL
}

export const CONTENT_CONTAINNER: ViewStyle = {
    ...FULL
}

export const BODY_CONTAINER: ViewStyle = {
    flex:0.8,
    marginHorizontal: metric.ratioWidth(35),
}

export const SCROLL_VIEW: ViewStyle = {
    paddingBottom: metric.ratioHeight(10),
}

export const TAG_CONTENT_CONTAINER: ViewStyle = {
    marginTop: 0
}

export const TEXT_FIELD_CONTAINER: ViewStyle = {
    ...FULL,
    alignItems: 'center'
}

export const TEXT: TextStyle = {
    color: color.palette.white,
    fontFamily: "Montserrat",
}

export const TITLE: TextStyle = {
    color: color.palette.lightPink,
    fontSize: RFValue(19),
    marginTop: metric.ratioHeight(34),
    fontWeight: '500',

    //Android
    fontFamily: "Montserrat-Medium",
}