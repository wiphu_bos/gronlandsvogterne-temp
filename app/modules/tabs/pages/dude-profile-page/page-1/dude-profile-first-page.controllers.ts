// MARK: Import

import { NavigationKey } from "../../../../../constants/app.constant"
import { RootStore, INavigationRoute } from "../../../../../models"
import { DudeProfileDetailPageProps } from "./dude-profile-first-page.props"
import { DudeProfileFirstScreenResourcesStore, DudeProfileFirstScreenStore } from "./storemodels/dude-profile-first-page.types"
import { DudeProfileFirstScreenResourcesStoreModel, DudeProfileFirstScreenStoreModel } from "./storemodels/dude-profile-first-page.store"
import { BaseController } from "../../../../base.controller"
import { onSnapshot } from "mobx-state-tree"

class DudeProfileFirstPageController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static viewModel: DudeProfileFirstScreenStore
    public static resourcesViewModel: DudeProfileFirstScreenResourcesStore

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, myProps?: DudeProfileDetailPageProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, myProps, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(myProps)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        DudeProfileFirstPageController.resourcesViewModel = DudeProfileFirstScreenResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.DudeProfileDetailFirst)
        DudeProfileFirstPageController.viewModel = localStore && DudeProfileFirstScreenStoreModel.create({ ...localStore }) || DudeProfileFirstScreenStoreModel.create({})
    } 
    private _setupSnapShot = () => {
        onSnapshot(DudeProfileFirstPageController.viewModel, (snap: DudeProfileFirstScreenStore) => {
            this._setInitializedToPropsParams()
        })
    }

    /*
       Mark Data
   */

    /*
       Mark Event
   */

    /*
       Mark Life cycle
   */

    /*
       Mark Helper
   */
}

export default DudeProfileFirstPageController