// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import LinearGradient from "react-native-linear-gradient"
import { View } from "react-native-animatable"
import * as metric from "../../../../../theme"
import { INavigationRoute } from "../../../../../models/navigation-store/navigation.types"
import { UserTagsComponent, AnimatedScrollView,Text } from "../../../../../components"
import { DudeProfileDetailPageProps } from "./dude-profile-first-page.props"
import DudeProfileDetailController from "../../../dude-profile-detail-tab/dude-profile-detail.controllers"

// MARK: Style Import
import * as Styles from "./dude-profile-first-page.styles"
import { useConfigurate } from "../../../../../custom-hooks/use-configure-controller"
import DudeProfileFirstPageController from "./dude-profile-first-page.controllers"


export const DudeProfileDetailFirstScreen: React.FunctionComponent<DudeProfileDetailPageProps & Partial<INavigationRoute>> = observer((props) => {
    
    const controller = useConfigurate(DudeProfileFirstPageController, props) as DudeProfileFirstPageController
    
    // MARK: Render
    return (
        <LinearGradient shouldRasterizeIOS style={Styles.CONTAINNER} {...Styles.TAB_PAGE_BACKGROUND}>
            <View shouldRasterizeIOS style={Styles.BODY_CONTAINER}>
                <AnimatedScrollView
                    contentContainerStyle={Styles.SCROLL_VIEW}
                    {...metric.solidScrollView}>
                    {DudeProfileDetailController.viewModel?.getTagList && <View shouldRasterizeIOS style={Styles.CONTENT_CONTAINNER}>
                        <Text style={Styles.TITLE}
                            {...DudeProfileFirstPageController.resourcesViewModel?.getTestIDTagTitle()}
                            text={DudeProfileFirstPageController.resourcesViewModel?.getResourceTagTitle()} />
                        <UserTagsComponent
                            isDisabled
                            tagList={DudeProfileDetailController.viewModel?.getTagList}
                            customHeaderView={<View />}
                            contentStyle={{ marginTop: 0 }} />
                    </View>}
                </AnimatedScrollView>
            </View>
        </LinearGradient>
    )
})
