import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { DudeProfileFirstScreenStore } from "./storemodels/dude-profile-first-page.types"
import { DudeProfileStore } from '../../../dude-profile-detail-tab/storemodels'
import { DudeProfileProps } from '../../../dude-profile-detail-tab/dude-profile-detail.props'

type DudeProfileDetailStore =
    | DudeProfileFirstScreenStore

export interface DudeProfileDetailPageProps extends NavigationContainerProps<ParamListBase>, DudeProfileProps {
    rootViewModel?: DudeProfileStore
    viewModel?: DudeProfileDetailStore
}