import { types } from "mobx-state-tree"
import { DudeProfileFirstScreenPropsModel } from "./dude-profile-first-page.models"
import { GeneralResources } from "../../../../../../constants/firebase/remote-config"
import { NavigationKey } from "../../../../../../constants/app.constant"
import { TestIDResources } from "../../../../../../constants/test-key/test.constant"
import * as Utils from "../../../../../../utils"
import { GeneralResourcesStoreModel } from "../../../../../../models/general-resources-store"

export const DudeProfileFirstScreenViews = types.model(DudeProfileFirstScreenPropsModel)
    .views(self => ({
        get getExample() {
            return self
        }
    }))

export const DudeProfileFirstScreenResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State

    const { DudeProfileDetailFirst } = GeneralResources
    const { tagTitle} = DudeProfileDetailFirst

    //MARK: Views
    const getShareResources = (key: string) => self.getValues(key, true)
    const getResources = (key: string) => self.getValues(NavigationKey.DudeProfileDetailFirst, key)
    const getResourceTagTitle = () => getResources(tagTitle)

    return {
        getResourceTagTitle
    }
})
    .views(self => {
        const getTestIDTagTitle = () => Utils.getTestIDObject(TestIDResources.DudeProfileFirstScreen.tagTitle)
        //MARK: Volatile State

        //MARK: Views

        return {
            getTestIDTagTitle
        }
    })
