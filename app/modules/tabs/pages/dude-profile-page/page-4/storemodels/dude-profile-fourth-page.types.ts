import { Instance, SnapshotOut } from "mobx-state-tree"
import { DudeProfileFourthStoreModel, DudeProfileFourthResourcesStoreModel } from "./dude-profile-fourth-page.store"
import { IUser } from "../../../../../../models/user-store"

export type DudeProfileFourthStore = Instance<typeof DudeProfileFourthStoreModel>
export type DudeProfileFourthStoreSnapshot = SnapshotOut<typeof DudeProfileFourthStoreModel>

export type DudeProfileFourthResourcesStore = Instance<typeof DudeProfileFourthResourcesStoreModel>
export type DudeProfileFourthResourcesStoreSnapshot = SnapshotOut<typeof DudeProfileFourthResourcesStoreModel>

export type IComment = {
    /*
        Naming as same as the backend props
    */
    comment_id?: string,
    full_name?: string,
    message?: string,
    created_date?: any,
    status?: string,
    dude_id?: string,
    user_id?: string
}

export type ICommentPage = {
    page_size?: number,
    is_last_page?: boolean,
    last_id?: string,
}

export type ICommentResponse = {
    comment_list?: IComment[],
    page?: ICommentPage
}

export type IReportComment = {
    /*
        Naming as same as the backend props
    */
    report_id?: string,
    full_name?: string,
    description?: string,
    created_date?: any,
    updated_date?: any,
    reporter_id?: string,
    comment_id?: string,
    dude_id?: string
}

export type IUserComment = {
    comment?: IComment,
    dudeProfile?: IUser
}