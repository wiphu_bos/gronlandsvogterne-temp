import { types } from "mobx-state-tree"
import { StoreName } from "../../../../../../constants/app.constant"
import { DudeProfileFourthPropsModel } from "../viewmodels/dude-profile-fourth-page.models"
import { DudeProfileFourthActions } from "../viewmodels/dude-profile-fourth-page.actions"
import { DudeProfileFourthViews, DudeProfileFourthResourcesViews } from "../viewmodels/dude-profile-fourth-page.views"
import { GeneralResourcesStoreModel } from "../../../../../../models/general-resources-store"
import { DudeProfileFourthServiceActions } from "../services/dude-profile-fourth-page.services"

const DudeProfileFourthModel = types.model(StoreName.DudeProfileDetailFourth, DudeProfileFourthPropsModel)
export const DudeProfileFourthStoreModel = types.compose(DudeProfileFourthModel, DudeProfileFourthViews, DudeProfileFourthActions, DudeProfileFourthServiceActions)
export const DudeProfileFourthResourcesStoreModel = types.compose(GeneralResourcesStoreModel, DudeProfileFourthResourcesViews)

