// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { ListItemProps } from "./list-item.props"
import * as Styles from "./list-item.styles"
import { Button, Text, Icon } from "../../../../../../../components"
import { View } from "react-native"
import * as DataUtils from "../../../../../../../utils/data.utils"
import { images } from "../../../../../../../theme/images"


export const ListItemComponent: React.FunctionComponent<ListItemProps> = observer((props) => {

    // MARK: Render
    const { item } = props
    const message = item?.message
    const full_name = item?.full_name

    const createDate = DataUtils.getDateTimeFormatted(item?.created_date)

    return (
        <Button preset="none" isSolid isAnimated={false} style={Styles.CONTAINNER_STYLE}>
            <View style={Styles.CONTENT_CONTAINER}>
                <Text text={message} style={Styles.TITLE} />
                <View style={Styles.FOOTER_CONTAINER}>
                    <Text text={full_name} style={Styles.SMALL_TEXT} />
                    <View style={Styles.FOOTER_RIGHT}>
                        <Text text={createDate} style={Styles.SMALL_TEXT} />
                        <Icon source={images.flag} style={Styles.ICON_FLAG} containerStyle={Styles.ICON_FLAG_CONTAINER} />
                    </View>
                </View>
                <Button preset='none' isAnimated={false} isSolid style={Styles.FLAG_TOUCH_AREA} onPress={() => props.onPress(props?.item)} />
            </View>
        </Button>
    )
})