import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { IComment } from '../../storemodels/dude-profile-fourth-page.types'

export interface ListItemProps extends NavigationContainerProps<ParamListBase> {
    item: Partial<IComment>
    onPress?: (item: Partial<IComment>) => void
}