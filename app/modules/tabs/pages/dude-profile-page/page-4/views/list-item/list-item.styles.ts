import { ViewStyle, TextStyle, ImageStyle } from "react-native";
import * as metric from "../../../../../../../theme"
import { color } from "../../../../../../../theme"
import { RFValue } from "react-native-responsive-fontsize";

export const CONTAINNER_STYLE: ViewStyle = {
    flexDirection: 'row',
    marginVertical: metric.ratioHeight(18),
}

const TEXT: TextStyle = {
    fontFamily: "Montserrat",
    color: color.palette.white
}

export const TITLE: TextStyle = {
    ...TEXT,
    lineHeight: metric.ratioHeight(26),
    fontSize: RFValue(14),
    fontWeight: '400',

    //Android
    fontFamily: "Montserrat-Regular",
}

export const SMALL_TEXT: TextStyle = {
    ...TEXT,
    lineHeight: metric.ratioHeight(20),
    fontSize: RFValue(8),
    fontWeight: '300',

    //Android
    fontFamily: "Montserrat-Light",
}

export const CONTENT_CONTAINER: ViewStyle = {
    flex: 1,
}
export const FOOTER_CONTAINER: ViewStyle = {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
}

export const ICON_FLAG: ImageStyle = {
    width: metric.ratioWidth(10),
    height: metric.ratioHeight(10)
}
export const ICON_FLAG_CONTAINER: ImageStyle = {
    marginLeft: metric.ratioWidth(7)
}

export const FOOTER_RIGHT: ViewStyle = {
    flexDirection: 'row',
    alignItems: 'center'
}

export const FLAG_TOUCH_AREA: ViewStyle = {
    position:'absolute',
    right:0,
    bottom:0,
    top:0,
    width:'8%'
}