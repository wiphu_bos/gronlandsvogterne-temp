// MARK: Import
import { NavigationKey, NotificationTopicKey } from "../../../../../constants/app.constant"
import { RootStore, INavigationRoute } from "../../../../../models"
import { DudeProfileDetailPageProps } from "./dude-profile-fourth-page.props"
import { DudeProfileFourthResourcesStore, DudeProfileFourthStore, IComment } from "./storemodels/dude-profile-fourth-page.types"
import { DudeProfileFourthResourcesStoreModel, DudeProfileFourthStoreModel } from "./storemodels/dude-profile-fourth-page.store"
import { FirebaseFirestoreTypes } from '@react-native-firebase/firestore'
import { StackActions, CommonActions } from "@react-navigation/core"
import { SnapshotChangeType } from "../../../../../constants/firebase/fire-store.constant"
import { Keyboard } from "react-native"
import { ICommentListListener } from "../../../../../utils/firebase/fire-store/fire-store.types"
import FirebaseFireStore from "../../../../../utils/firebase/fire-store/fire-store"
import { BaseController } from "../../../../base.controller"
import DudeProfileDetailController from "../../../dude-profile-detail-tab/dude-profile-detail.controllers"
import { onSnapshot } from "mobx-state-tree"
import { IChat } from "../../../../../models/chat-store/chat-store.types"
import * as DataUtils from "../../../../../utils/data.utils"
import { ValidationServices } from "../../../../../services/api-domain/validate.services"

class DudeProfileFourthPageController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static viewModel: DudeProfileFourthStore
    public static resourcesViewModel: DudeProfileFourthResourcesStore
    private static _flatListRef: React.MutableRefObject<any>
    private static _commentFlagSelected: Partial<IComment>
    private static _doReportTask: boolean
    private static _disposer: () => void
    private static _isVisited: boolean

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, myProps?: DudeProfileDetailPageProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, myProps, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(myProps)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    public setupFlatListRef = (ref: React.MutableRefObject<any>) => DudeProfileFourthPageController._flatListRef = ref


    private _setupResourcesViewModel = () => {
        DudeProfileFourthPageController.resourcesViewModel = DudeProfileFourthResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.DudeProfileDetailFourth)
        DudeProfileFourthPageController.viewModel = localStore && DudeProfileFourthStoreModel.create({ ...localStore }) || DudeProfileFourthStoreModel.create({})
    }

    private _setupSnapShot = () => {
        onSnapshot(DudeProfileFourthPageController.viewModel, (snap: DudeProfileFourthStore) => {
            this._setInitializedToPropsParams()
        })
    }

    /*
       Mark Data
   */

    private _fetchDudeCommentList = async () => await DudeProfileFourthPageController.viewModel?.fetchCommentList(DudeProfileDetailController.viewModel?.getUserId)

    /*
       Mark Event
   */

    private _isLoading = (value: boolean) => DudeProfileFourthPageController.viewModel?.setIsPullRefreshing(value)

    private _onCommentListener = () => {
        if (!DudeProfileDetailController.viewModel?.getUserId) return
        const onNext: (snapshot: FirebaseFirestoreTypes.QuerySnapshot) => void = (snap => {
            if (!DudeProfileFourthPageController.viewModel?.getIsFirstLoadDone || snap.metadata.hasPendingWrites) return
            snap.docChanges().forEach(change => {
                if (change.type === SnapshotChangeType.ADDED) {
                    const comment: IComment = change.doc.data()
                    if (comment) DudeProfileFourthPageController.viewModel?.setAddNewComment(comment)
                }
            })
        })

        const onError: (e: Error) => void = (e => {
        })

        const onComplete: () => void = () => {
        }

        const params: ICommentListListener = {
            creatorId: DudeProfileDetailController.viewModel?.getCreatorId,
            dudeId: DudeProfileDetailController.viewModel?.getUserId,
            size: 1,
            onNext,
            onError,
            onComplete
        }

        DudeProfileFourthPageController._disposer = FirebaseFireStore.onCommentListener(params)
    }

    private _markReadNotificationForComment = () => {
        const topic: string = NotificationTopicKey.Comment + DudeProfileDetailController.viewModel?.getUserId
        const notificationStore = this._rootStore?.getNotificationStore
        this._rootStore?.getUserStore?.setReadNotificationByTopic(topic)
        notificationStore.markReadNotificationByTopic(topic)
    }

    private _goToChatRoom = (chatRoom: IChat) => {
        const chatRoomObject = DataUtils.getChatMessageObject(this._rootStore, chatRoom)
        this._myProps?.navigation?.dispatch(StackActions.push(NavigationKey.ChatRoom, { chatRoomObject: chatRoomObject, shouldDelayFetching: false }) as any)
    }

    public onRefresh = async () => {
        this._isLoading(true)
        DudeProfileFourthPageController.viewModel?.resetFetching()
        await this._fetchDudeCommentList()
        this._isLoading(false)
    }

    public onLoadMore = async () => {
        if (!DudeProfileFourthPageController.viewModel?.getShouldLoadmore) return
        DudeProfileFourthPageController.viewModel?.setIsLoadMore(true)
        await this._fetchDudeCommentList()
    }

    public onSendEmailToDudePressed = () => {
        DudeProfileDetailController.viewModel?.setIsFocusedCommentScreen(false)
        this._myProps?.navigation?.dispatch(StackActions.push(NavigationKey.DudeProfileDetailSendEmail) as any)
    }

    public onDirectMessageToCreatorPressed = async () => {
        const creatorId: string = DudeProfileDetailController.viewModel?.getCreatorId
        this._isGlobalLoading(true)
        const result = await DudeProfileFourthPageController.viewModel?.createChatRoom(this._rootStore, creatorId)
        this._isGlobalLoading(false)
        const chatRoom: IChat = result?.data
        if (!chatRoom) {
            const params = {
                rootStore: this._rootStore,
                error: result
            }
            const [isError, message] = new ValidationServices(params).premiumAccount()
            if (isError) this._delayExecutor(() => this._myProps?.navigation?.dispatch(CommonActions.navigate(NavigationKey.SubscriptionConfirmation,
                { onSuccess: () => this._goToChatRoom(chatRoom) }) as any))
        } else {
            this._goToChatRoom(chatRoom)
        }
    }

    public onFlagPressed = (item: Partial<IComment>) => {
        DudeProfileFourthPageController._commentFlagSelected = item
        DudeProfileFourthPageController.viewModel?.setIsModalShowing(true)
    }

    public onReportContentChangeText = (value: string): void => {
        DudeProfileFourthPageController.viewModel?.setReportDescription(value)
    }

    public onBlurReportContentTextField = (): void => {

    }

    public onSubmitReport = () => {
        if (!this.isReportValid())
        return
       DudeProfileFourthPageController._doReportTask = true
        this.onCloseModal()
    }


    public onCloseModal = () => {
        Keyboard.dismiss()
        DudeProfileFourthPageController.viewModel?.setIsModalShowing(false)
    }

    public onModalHide = async () => {
        if (!DudeProfileFourthPageController._doReportTask) return
        const dudeId = DudeProfileDetailController.viewModel?.getUserId
        const commentId = DudeProfileFourthPageController._commentFlagSelected?.comment_id
        this._isGlobalLoading(true)
        await DudeProfileFourthPageController.viewModel?.reportComment(dudeId, commentId)
        this._isGlobalLoading(false)
        DudeProfileFourthPageController.viewModel?.setReportDescription(null)
        DudeProfileFourthPageController._doReportTask = false
    }

    public static onSubmitComment = async (message?: string) => {
        if (!message) return
        const result = await DudeProfileFourthPageController.viewModel?.createComment(DudeProfileDetailController.viewModel?.getUserId, message)
        const data = result?.data
        if (!data) {
            const params = {
                rootStore: DudeProfileFourthPageController.rootStore,
                error: result
            }
            const [isError, message] = new ValidationServices(params).premiumAccount()
            if (isError) DudeProfileFourthPageController._delayExecutor(() => DudeProfileFourthPageController.myProps?.navigation?.dispatch(CommonActions.navigate(NavigationKey.SubscriptionConfirmation) as any))
        }

        DudeProfileFourthPageController._flatListRef?.current?.scrollToOffset({ animated: true, offset: 0 })
    }


    /*
       Mark Life cycle
   */

    //@override
    viewDidAppearOnce = async () => {
        DudeProfileFourthPageController._doReportTask = false
    }

    //@ovveride
    viewDidAppearAfterFocus = async () => {
        super.viewDidAppearAfterFocus && super.viewDidAppearAfterFocus()
        if (DudeProfileFourthPageController.viewModel?.getCommentList?.length === 0) {
            this._onCommentListener()
            this._markReadNotificationForComment()
            await this.onRefresh()
        }
        if (!DudeProfileFourthPageController._isVisited) {
            DudeProfileDetailController.viewModel?.increaseViewCountDude(DudeProfileDetailController.viewModel?.getUserId)
            DudeProfileFourthPageController._isVisited = true
        }
    }

    //@override
    deInit = () => {
        super.deInit && super.deInit()
        DudeProfileFourthPageController._disposer && DudeProfileFourthPageController._disposer()
    }

    /*
       Mark Helper
   */

    public isReportValid = () => {
        return DudeProfileFourthPageController.viewModel?.getReportDescription || false
    }
}

export default DudeProfileFourthPageController