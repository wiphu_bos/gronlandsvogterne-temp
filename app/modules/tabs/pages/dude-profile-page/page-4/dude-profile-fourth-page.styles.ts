import { ViewStyle, TextStyle } from "react-native"
import { color } from "../../../../../theme"
import * as metric from "../../../../../theme"
import { RFValue } from "react-native-responsive-fontsize"

export const FULL: ViewStyle = {
    flex: 1
}
export const TAB_PAGE_BACKGROUND: any = { colors: color.tabPageBackground }

export const CONTAINNER: ViewStyle = {
    ...FULL,
}

export const CONTENT_CONTAINNER: ViewStyle = {
    ...FULL
}
export const BODY_CONTAINER: ViewStyle = {
    ...FULL
}
export const MENU_CONTAINER: ViewStyle = {
    backgroundColor: color.brightDim,
    minHeight: metric.ratioHeight(128),
    paddingHorizontal: metric.ratioWidth(25),
    paddingVertical: metric.ratioHeight(18)
}

export const MENU_BUTTON_CONTAINER: ViewStyle = {
    marginTop: metric.ratioHeight(18),
    justifyContent: 'space-between',
    flexDirection: 'row'
}


const BUTTON: ViewStyle = {
    height: metric.ratioHeight(44),
    borderRadius: metric.ratioWidth(17),
    justifyContent: 'center',
    alignItems: 'center'
}

export const EMAIL_TO_DUDE_BUTTON: ViewStyle = {
    ...BUTTON,
    backgroundColor: color.palette.pale
}

export const DIRECT_CREATOR_BUTTON: ViewStyle = {
    ...BUTTON,
    backgroundColor: color.palette.pinkRed
}

const TEXT: TextStyle = {
    color: color.palette.white
}

export const TITLE: TextStyle = {
    ...TEXT,
    fontSize: RFValue(14),
    fontWeight: '400',

    //Android
    fontFamily: "Montserrat-Regular",
}

export const CONTENT_CONTAINER: ViewStyle = {
    ...FULL,
}

export const AVOID_KEYBOARD_VIEW: ViewStyle = {
    ...FULL
}
export const SCROLL_VIEW: ViewStyle = {
    paddingBottom: metric.ratioHeight(10),
    justifyContent: "center"
}

export const COMMENT_TITLE: TextStyle = {
    color: color.palette.lightPink,
    fontSize: RFValue(19),
    marginTop: metric.ratioHeight(18),
    fontWeight: '500',

    //Android
    fontFamily: "Montserrat-Medium",
    marginHorizontal: metric.ratioWidth(25)
}

export const BUTTON_TITLE: TextStyle = {
    ...TEXT,
    fontSize: RFValue(13.7),
    fontWeight: '500',

    //Android
    fontFamily: "Montserrat-SemiBold",
    textAlign: 'center',
}

export const TITLE_CONTAINER: ViewStyle = {

}

export const CONTENT: TextStyle = {
    color: color.palette.white,
    fontSize: RFValue(16),
    fontWeight: '400',
    marginTop: metric.ratioHeight(11),

    //Android
    fontFamily: "Montserrat-Regular",
}

export const REFRESH_CONTROL: any = { tintColor: color.brightDim }

export const TITLE_CONTAINNER: ViewStyle = {
    paddingLeft: metric.ratioWidth(45),
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: metric.ratioHeight(28)
}
export const LIST_VIEW: ViewStyle = {
    paddingHorizontal: metric.ratioWidth(25)
}

export const SEPARATOR_OPTIONS: any = {
    dashGap: 4,
    dashLength: 4,
    dashThickness: metric.ratioHeight(1),
    dashColor: color.brightDim,
    dashStyle: null,
    style: null
}


//Modal 
export const TEXT_AREA_TEXT: TextStyle = {
    ...TEXT,
    fontSize: RFValue(14),
    fontWeight: '400',

    //Android
    fontFamily: "Montserrat-Regular",
    color: color.palette.darkGrey,
    textAlign: 'left',
    textAlignVertical: 'top',
}
export const TEXT_AREA_OPTIONS: any = {
    ...metric.disableAutoCompleteTextField,
    multiline: true,
    numberOfLines: 0,
    maxLength: metric.maxDescriptionChar,
    selectionColor: color.palette.pink
}

export const TEXT_AREA: ViewStyle = {
    marginTop: metric.screenHeight * 0.2,
    height: '50%',
    backgroundColor: color.palette.white,
    borderRadius: metric.ratioWidth(10),
    paddingHorizontal: metric.ratioWidth(19),
    paddingTop: metric.ratioHeight(20),
    paddingBottom: metric.ratioHeight(20),
}

export const MODAL_CONTENT_CONTAINER: ViewStyle = {
    ...FULL
}

export const BUTTON_MARGIN: ViewStyle = {
    marginTop: metric.ratioHeight(24),
    alignItems: 'center',
}
export const MODAL_OPTIONS: any = {
    animationIn: "fadeIn",
    animationOut: "fadeOut",
    avoidKeyboard: true,
}
export const CLOSE_VIEW: ViewStyle = {
    backgroundColor: color.palette.pinkRed,
    width: metric.ratioWidth(67),
    height: metric.ratioWidth(67),
    borderRadius: metric.ratioWidth(67 / 2),
    position: 'absolute',
    justifyContent: 'center',
    right: -metric.ratioWidth(10),
    top: metric.screenHeight * 0.17,
    zIndex:1
}