// MARK: Import

import React, { useRef, useEffect } from "react"
import { observer } from "mobx-react-lite"
import { DudeProfileDetailPageProps } from "./dude-profile-fourth-page.props"
import LinearGradient from "react-native-linear-gradient"
import { useConfigurate } from "../../../../../custom-hooks/use-configure-controller"
import DudeProfileFourthPageController from "./dude-profile-fourth-page.controllers"
import { INavigationRoute } from "../../../../../models/navigation-store/navigation.types"
import { View, RefreshControl, KeyboardAvoidingView } from "react-native"
import Modal from "../../../../../libs/react-native-modal"
import { Text, Button, AnimatedScrollView } from "../../../../../components"
import { ListItemComponent } from "./views/list-item"
import { LoadMoreIndicator } from "../../../../../components/loadmore-indicator"
import Dash from "../../../../../libs/dash-line/dash-line.js"
import { FlatList, TextInput } from "react-native-gesture-handler"
import { GradientButton } from "../../../../../components/gradient-button/button"
import * as metric from "../../../../../theme"
// MARK: Style Import

import * as Styles from "./dude-profile-fourth-page.styles"

export const DudeProfileDetailFourthScreen: React.FunctionComponent<DudeProfileDetailPageProps & Partial<INavigationRoute>> = observer((props) => {

    const controller = useConfigurate(DudeProfileFourthPageController, props) as DudeProfileFourthPageController
    const flatListRef = useRef(null)

    useEffect(() => {
        controller.setupFlatListRef(flatListRef)
    }, [])

    // MARK: Render

    return (
        <LinearGradient shouldRasterizeIOS style={Styles.CONTAINNER} {...Styles.TAB_PAGE_BACKGROUND}>
            <View shouldRasterizeIOS style={Styles.BODY_CONTAINER}>
                <View style={Styles.MENU_CONTAINER}>
                    <Text style={Styles.TITLE}
                        {...DudeProfileFourthPageController.resourcesViewModel?.getTestIDTitle()}
                        text={DudeProfileFourthPageController.resourcesViewModel?.getResourceMenuDescription()} />
                    <View style={Styles.MENU_BUTTON_CONTAINER}>
                        <Button preset="shadow"
                            {...DudeProfileFourthPageController.resourcesViewModel?.getTestIDButtonEmailToDude()}
                            text={DudeProfileFourthPageController.resourcesViewModel?.getResourceButtonEmailToDudeTitle()}
                            style={Styles.EMAIL_TO_DUDE_BUTTON}
                            textStyle={Styles.BUTTON_TITLE}
                            onPress={controller.onSendEmailToDudePressed} />
                        <Button preset="shadow"
                            {...DudeProfileFourthPageController.resourcesViewModel?.getTestIDButtonDirectMessageToCreator()}
                            text={DudeProfileFourthPageController.resourcesViewModel?.getResourceButtonDirectMessageToCreatorTitle()}
                            style={Styles.DIRECT_CREATOR_BUTTON}
                            textStyle={Styles.BUTTON_TITLE}
                            onPress={controller.onDirectMessageToCreatorPressed}
                        />
                    </View>
                </View>
                <View style={Styles.CONTENT_CONTAINER}>
                    <Text
                        {...DudeProfileFourthPageController.resourcesViewModel?.getTestIDCommentTitle()}
                        text={DudeProfileFourthPageController.resourcesViewModel?.getResourceCommentTitle()}
                        style={Styles.COMMENT_TITLE} />
                    <FlatList
                        ref={flatListRef}
                        {...DudeProfileFourthPageController.resourcesViewModel?.getTestIDCommentList()}
                        refreshControl={
                            <RefreshControl
                                style={Styles.REFRESH_CONTROL}
                                refreshing={false}
                                onRefresh={controller.onRefresh}
                            />
                        }
                        contentContainerStyle={Styles.LIST_VIEW}
                        ItemSeparatorComponent={({ highlighted }) => (
                            <Dash {...Styles.SEPARATOR_OPTIONS} />
                        )}
                        extraData={DudeProfileFourthPageController.viewModel?.getCommentList}
                        data={DudeProfileFourthPageController.viewModel?.getCommentList}
                        renderItem={({ item, index, separators }) => (<ListItemComponent item={item} onPress={() => controller.onFlagPressed(item)} />)
                        }
                        ListFooterComponent={!DudeProfileFourthPageController.viewModel?.getIsLoadDone && <LoadMoreIndicator />}
                        onEndReachedThreshold={0.4}
                        onEndReached={controller.onLoadMore}
                    />
                </View>
            </View>
            <Modal shouldRasterizeIOS isVisible={DudeProfileFourthPageController.viewModel?.isModalShowing || false}
                style={Styles.MODAL_CONTENT_CONTAINER}
                onModalHide={controller.onModalHide}
                {...Styles.MODAL_OPTIONS}>
                <KeyboardAvoidingView
                    keyboardVerticalOffset={metric.isIPhone ? metric.ratioHeight(-200) : metric.ratioHeight(-150)}
                    behavior={'padding'}
                    style={Styles.AVOID_KEYBOARD_VIEW}>
                    <View style={Styles.MODAL_CONTENT_CONTAINER}>
                        <Button
                            isSolid
                            isAnimated={false}
                            preset='link'
                            imagePreset="close"
                            style={Styles.CLOSE_VIEW}
                            // {...resourcesViewModel?.getTestIDButtonCloseSideMenuBarTitle()}
                            onPressIn={controller.onCloseModal}
                            onPress={controller.onCloseModal}
                        />
                        <TextInput
                            style={{ ...Styles.TEXT_AREA, ...Styles.TEXT_AREA_TEXT }}
                            {...Styles.TEXT_AREA_OPTIONS}
                            autoCorrect={false}
                            onChangeText={controller.onReportContentChangeText}
                            onBlur={controller.onBlurReportContentTextField}
                        />

                        <GradientButton
                            preset={controller.isReportValid() ? "shadow" : "disabled"}
                            text={DudeProfileFourthPageController.resourcesViewModel?.getResourceButtonSubmitTitle()}
                            containerStyle={Styles.BUTTON_MARGIN}
                            onPressIn={controller.onSubmitReport}
                            onPress={controller.onSubmitReport}
                        />
                    </View>
                </KeyboardAvoidingView>
            </Modal >
        </LinearGradient >
    )
})
