import { types } from "mobx-state-tree"
import { DudeProfileFourthPropsModel } from "./dude-profile-fourth-page.models"
import { GeneralResources } from "../../../../../../constants/firebase/remote-config"
import { TestIDResources } from "../../../../../../constants/test-key/test.constant"
import * as Utils from "../../../../../../utils"
import { GeneralResourcesStoreModel } from "../../../../../../models/general-resources-store"
import { NavigationKey } from "../../../../../../constants/app.constant"
import { IComment } from "../storemodels"

export const DudeProfileFourthViews = types.model(DudeProfileFourthPropsModel)
    .views(self => ({
        get getCommentList(): Partial<IComment>[] {
            return self.commentList
        },
        get getLastId(): string {
            return self.last_id
        },
        get getIsPullRefreshing(): boolean {
            return self.isPullRefreshing
        },
        get getIsLoadMore(): boolean {
            return self.isLoadMore
        },
        get getIsLoadDone(): boolean {
            return self.isLoadDone
        },
        get getShouldLoadmore(): boolean {
            return !self.isLoadDone && !self.isLoadMore && self.commentList.length > 0
        },
        get getIsModalShowing(): boolean {
            return self.isModalShowing
        },
        get getReportDescription(): string {
            return self.reportDescription
        },
        get getIsFirstLoadDone(): boolean {
            return self.isFirstLoadDone
        },
    }))

export const DudeProfileFourthResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State

    const { DudeProfileFourthScreen } = GeneralResources
    const {
        commentTitle,
        menuDescription,
        enterTextPlaceholder,
        buttonDirectMessageToCreatorTitle,
        buttonEmailToDudeTitle,
        buttonSubmitTitle
    } = DudeProfileFourthScreen

    //MARK: Views
    const getShareResources = (key: string) => self.getValues(key, true)
    const getResources = (key: string) => self.getValues(NavigationKey.DudeProfileDetailScreen, key)
    const getResourceMenuDescription = () => getResources(menuDescription)
    const getResourceEnterTextPlaceholder = () => getShareResources(enterTextPlaceholder)
    const getResourceButtonDirectMessageToCreatorTitle = () => getShareResources(buttonDirectMessageToCreatorTitle)
    const getResourceButtonEmailToDudeTitle = () => getShareResources(buttonEmailToDudeTitle)
    const getResourceCommentTitle = () => getResources(commentTitle)
    const getResourceButtonSubmitTitle = () => getShareResources(buttonSubmitTitle)
    return {
        getResourceCommentTitle,
        getResourceMenuDescription,
        getResourceEnterTextPlaceholder,
        getResourceButtonDirectMessageToCreatorTitle,
        getResourceButtonEmailToDudeTitle,
        getResourceButtonSubmitTitle
    }
})
    .views(self => {

        //MARK: Volatile State

        //MARK: Views

        const getTestIDTitle = () => Utils.getTestIDObject(TestIDResources.DudeProfileFourthScreen.title)
        const getTestIDCommentTitle = () => Utils.getTestIDObject(TestIDResources.DudeProfileFourthScreen.commentTitle)
        const getTestIDCommentList = () => Utils.getTestIDObject(TestIDResources.DudeProfileFourthScreen.commentList)
        const getTestIDButtonEmailToDude = () => Utils.getTestIDObject(TestIDResources.DudeProfileFourthScreen.buttonEmailToDude)
        const getTestIDButtonDirectMessageToCreator = () => Utils.getTestIDObject(TestIDResources.DudeProfileFourthScreen.buttonDirectMessageToCreator)

        return {
            getTestIDTitle,
            getTestIDCommentTitle,
            getTestIDCommentList,
            getTestIDButtonEmailToDude,
            getTestIDButtonDirectMessageToCreator
        }
    })
