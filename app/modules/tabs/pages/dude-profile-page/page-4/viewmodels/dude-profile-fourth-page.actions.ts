import { types } from "mobx-state-tree"
import { DudeProfileFourthPropsModel } from "./dude-profile-fourth-page.models"
import { IComment } from "../storemodels"

export const DudeProfileFourthActions = types.model(DudeProfileFourthPropsModel).actions(self => {
    const findExistingItemIndex = (comment: IComment): number => {
        const tempChatList = self.commentList
        const existingList = tempChatList?.slice() //Slice to disobserve property
        const foundItemIndex = existingList?.findIndex(v => v.comment_id === comment?.comment_id)
        return foundItemIndex
    }

    const setAddNewComment = (comment: IComment) => {
        const foundItemIndex = findExistingItemIndex(comment)
        if(foundItemIndex !== -1) return
        const existingList = self.commentList
        self.commentList = [].concat.apply(comment, existingList)
    }
    const setIsPullRefreshing = (value: boolean) => self.isPullRefreshing = value
    const setIsLoadMore = (value: boolean) => self.isLoadMore = value
    const setIsModalShowing = (value: boolean) => self.isModalShowing = value
    const setReportDescription = (value: string) => self.reportDescription = value
    const setIsFirstLoadDone = (value: boolean) => self.isFirstLoadDone = value
    const resetFetching = () => {
        self.commentList = []
        self.isLoadDone = false
        self.isLoadMore = false
        self.isFirstLoadDone = false
    }
    return {
        setAddNewComment,
        setIsPullRefreshing,
        resetFetching,
        setIsLoadMore,
        setIsModalShowing,
        setReportDescription,
        setIsFirstLoadDone
    }
})