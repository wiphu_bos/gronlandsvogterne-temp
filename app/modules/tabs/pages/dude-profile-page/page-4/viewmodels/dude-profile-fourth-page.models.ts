import { types } from "mobx-state-tree"
import { IComment } from "../storemodels"

export const DudeProfileFourthPropsModel = {
    last_id: types.maybeNull(types.string),
    commentList: types.optional(types.frozen<Partial<IComment>[]>(), []),
    isPullRefreshing: types.optional(types.boolean, false),
    isFirstLoadDone: types.optional(types.boolean, false),
    isLoadDone: types.optional(types.boolean, false),
    isLoadMore: types.optional(types.boolean, false),
    isModalShowing: types.optional(types.boolean, false),
    reportDescription: types.maybeNull(types.string),
}