
import { DudeProfileFourthPropsModel } from "../viewmodels"
import { types, flow } from "mobx-state-tree"
import { APIResponse, APISuccessResponse } from "../../../../../../constants/service.types"
import { ICommentResponse, IComment, IReportComment } from "../storemodels"
import CommentServices from "../../../../../../services/api-domain/comment.service"
import { Property } from "../../../../../../constants/firebase/fire-store.constant"
import { RootStore } from "../../../../../../models"
import { IChat } from "../../../../../../models/chat-store"
export const DudeProfileFourthServiceActions = types.model(DudeProfileFourthPropsModel).actions(self => {
    const fetchCommentList = flow(function* (dude_id: string) {
        const params = {
            [Property.UID]: dude_id,
            [Property.LAST_ID]: self.isFirstLoadDone ? self.last_id : null
        }
        try {
            const result: APIResponse = yield CommentServices.getCommentList(params)
            let success = (result as APISuccessResponse)?.data
            const commentResult: ICommentResponse = success?.data
            const commentList = commentResult?.comment_list
            const pagingObj = commentResult?.page
            if (commentList?.length > 0) {
                const existingList = self.commentList
                self.commentList = self.isLoadMore ? existingList?.concat(commentList) : commentList
            }
            self.last_id = pagingObj?.last_id
            self.isLoadDone = pagingObj?.is_last_page
            return result
        } catch (e) {
            return e
        } finally {
            self.isFirstLoadDone = true
            self.isPullRefreshing = false
            self.isLoadMore = false
        }
    })

    const createComment = flow(function* (dude_id: string, message: string) {
        const params = {
            [Property.UID]: dude_id,
            [Property.MESSAGE]: message
        }
        try {
            const result: APIResponse = yield CommentServices.createComment(params)
            let success = (result as APISuccessResponse)?.data
            const data: IComment = success?.data
            if (!data) throw result
            // if (comment) {
            //     const existingList = self.commentList
            //     self.commentList = [].concat.apply(comment, existingList)
            // }
            return success
        } catch (e) {
            return e
        }
    })

    const reportComment = flow(function* (dude_id: string, comment_id: string) {
        const params = {
            [Property.DUDE_ID]: dude_id,
            [Property.COMMENT_ID]: comment_id,
            [Property.DESCRIPTION]: self.reportDescription
        }
        try {
            const result: APIResponse = yield CommentServices.reportComment(params)
            let success = (result as APISuccessResponse)?.data
            const reportResult: IReportComment = success?.data
            // if (comment) {
            //     const existingList = self.commentList
            //     self.commentList = [].concat.apply(comment, existingList)
            // }
            return result
        } catch (e) {
            return e
        }
    })

    const createChatRoom = flow(function* (rootStore?: RootStore, creatorId?: string) {
        try {
            const result: APIResponse = yield rootStore?.getChatStore?.createChatRoomById(creatorId)
            let success = (result as APISuccessResponse)?.data
            const data: IChat = success?.data
            if (!data) throw result
            return success
        } catch (e) {
            return e
        }
    })

    return { fetchCommentList, reportComment, createComment, createChatRoom }
})