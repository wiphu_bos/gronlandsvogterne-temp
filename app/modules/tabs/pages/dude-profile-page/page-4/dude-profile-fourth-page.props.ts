import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { DudeProfileFourthStore } from "./storemodels/dude-profile-fourth-page.types"
import { DudeProfileStore } from '../../../dude-profile-detail-tab/storemodels/dude-profile-detail.types'
import { DudeProfileProps } from '../../../dude-profile-detail-tab/dude-profile-detail.props'

type DudeProfileStoreModel =
    | DudeProfileFourthStore

export interface DudeProfileDetailPageProps extends NavigationContainerProps<ParamListBase>, DudeProfileProps {
    rootViewModel?: DudeProfileStore
    viewModel?: DudeProfileStoreModel
}