// MARK: Import
import { NavigationKey } from "../../../../../constants/app.constant"
import { RootStore, INavigationRoute } from "../../../../../models"
import { DudeProfileDetailPageProps } from "./dude-profile-third-page.props"
import { DudeProfileThirdScreenResourcesStore, DudeProfileThirdScreenStore } from "./storemodels/dude-profile-third-page.types"
import { DudeProfileThirdScreenResourcesStoreModel, DudeProfileThirdScreenStoreModel } from "./storemodels/dude-profile-third-page.store"
import { BaseController } from "../../../../base.controller"
import { onSnapshot } from "mobx-state-tree"

class DudeProfileThirdPageController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static viewModel: DudeProfileThirdScreenStore
    public static resourcesViewModel: DudeProfileThirdScreenResourcesStore

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, myProps?: DudeProfileDetailPageProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, myProps, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(myProps)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        DudeProfileThirdPageController.resourcesViewModel = DudeProfileThirdScreenResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.DudeProfileDetailThird)
        DudeProfileThirdPageController.viewModel = localStore && DudeProfileThirdScreenStoreModel.create({ ...localStore }) || DudeProfileThirdScreenStoreModel.create({})
    }
    private _setupSnapShot = () => {
        onSnapshot(DudeProfileThirdPageController.viewModel, (snap: DudeProfileThirdScreenStore) => {
            this._setInitializedToPropsParams()
        })
    }
    /*
       Mark Data
   */

    /*
       Mark Event
   */

    /*
       Mark Life cycle
   */

    /*
       Mark Helper
   */
}

export default DudeProfileThirdPageController