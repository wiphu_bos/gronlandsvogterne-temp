// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { DudeProfileDetailPageProps } from "./dude-profile-third-page.props"
import LinearGradient from "react-native-linear-gradient"
import * as metric from "../../../../../theme"
import { INavigationRoute } from "../../../../../models/navigation-store/navigation.types"
import { AnimatedScrollView } from "../../../../../components"
import { View } from "react-native"
import { SquareImage } from "../../../../../components/square-image/square-image"
import DudeProfileDetailController from "../../../dude-profile-detail-tab/dude-profile-detail.controllers"

// MARK: Style Import
import * as Styles from "./dude-profile-third-page.styles"

export const DudeProfileDetailThridScreen: React.FunctionComponent<DudeProfileDetailPageProps & Partial<INavigationRoute>> = observer((props) => {
    // MARK: Render
    return (
        <LinearGradient shouldRasterizeIOS style={Styles.CONTAINNER} {...Styles.TAB_PAGE_BACKGROUND}>
            <View shouldRasterizeIOS style={Styles.BODY_CONTAINER}>
                <AnimatedScrollView
                    contentContainerStyle={Styles.SCROLL_VIEW}
                    {...metric.solidScrollView}>
                    <View shouldRasterizeIOS style={Styles.IMAGE_CONTAINER}>
                        <View shouldRasterizeIOS style={Styles.GRID}>
                            {(DudeProfileDetailController.viewModel?.getGalleryImages) ?
                                DudeProfileDetailController.viewModel?.getGalleryImages?.map((item, index) =>
                                    <View shouldRasterizeIOS style={Styles.GRID_IMAGE}>
                                        <SquareImage
                                            imageSource={item?.file_path}
                                            showDelete={false}
                                            index={index} />
                                    </View>
                                ) : null}
                        </View>
                    </View>
                </AnimatedScrollView>
            </View>
        </LinearGradient>
    )
})
