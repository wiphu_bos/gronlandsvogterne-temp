import { ViewStyle } from "react-native"
import { color } from "../../../../../theme"
import * as metric from "../../../../../theme"

export const FULL: ViewStyle = {
    flex: 1
}
export const TAB_PAGE_BACKGROUND: any = { colors: color.tabPageBackground }

export const CONTAINNER: ViewStyle = {
    ...FULL
}

export const CONTENT_CONTAINNER: ViewStyle = {
    ...FULL
}

export const IMAGE_CONTAINER: ViewStyle = {
    ...FULL,
    marginTop: metric.ratioHeight(20),
    minHeight: metric.ratioHeight(135),
    marginHorizontal: metric.ratioWidth(15),
}

export const BODY_CONTAINER: ViewStyle = {
    flex: 0.8
}

export const SCROLL_VIEW: ViewStyle = {
    paddingBottom: metric.ratioHeight(10),
}

export const GRID: ViewStyle = {
    ...FULL,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'flex-start',
}

export const GRID_IMAGE: ViewStyle = {
    // marginHorizontal: metric.ratioWidth(2),
    marginVertical: metric.ratioHeight(2),
    width: '33.33%',
    alignItems: 'center'
}

export const FOOTER_CONTAINER: ViewStyle = {
    marginHorizontal: metric.ratioWidth(35),
}