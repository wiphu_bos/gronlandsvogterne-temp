import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { DudeProfileThirdScreenStore } from "./storemodels/dude-profile-third-page.types"
import { DudeProfileStore } from '../../../dude-profile-detail-tab/storemodels/dude-profile-detail.types'
import { DudeProfileProps } from '../../../dude-profile-detail-tab/dude-profile-detail.props'

type DudeProfileStoreModel =
    | DudeProfileThirdScreenStore

export interface DudeProfileDetailPageProps extends NavigationContainerProps<ParamListBase>, DudeProfileProps {
    rootViewModel?: DudeProfileStore
    viewModel?: DudeProfileStoreModel
}