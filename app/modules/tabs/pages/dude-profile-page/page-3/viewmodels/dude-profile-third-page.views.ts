import { types } from "mobx-state-tree"
import { DudeProfileThirdScreenPropsModel } from "./dude-profile-third-page.models"
import { GeneralResources } from "../../../../../../constants/firebase/remote-config"
import { NavigationKey } from "../../../../../../constants/app.constant"
import { TestIDResources } from "../../../../../../constants/test-key/test.constant"
import * as Utils from "../../../../../../utils"
import { GeneralResourcesStoreModel } from "../../../../../../models/general-resources-store"

export const DudeProfileThirdScreenViews = types.model(DudeProfileThirdScreenPropsModel)
    .views(self => ({
        get getExample() {
            return self
        }
    }))

export const DudeProfileThirdScreenResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State

    const { } = GeneralResources

    //MARK: Views
    const getShareResources = (key: string) => self.getValues(key, true)
    const getResources = (key: string) => self.getValues(NavigationKey.DudeProfileDetailScreen, key)
 

    return {
      
    }
})
    .views(self => {

        //MARK: Volatile State

        //MARK: Views

        const getTestIDGalleryImage = () => Utils.getTestIDObject(TestIDResources.DudeProfileThirdScreen.imageGallery)
        return {
            getTestIDGalleryImage
        }
    })
