import { types } from "mobx-state-tree"
import { StoreName } from "../../../../../../constants/app.constant"
import { DudeProfileThirdScreenPropsModel } from "../viewmodels/dude-profile-third-page.models"
import { DudeProfileThirdScreenActions } from "../viewmodels/dude-profile-third-page.actions"
import { DudeProfileThirdScreenViews, DudeProfileThirdScreenResourcesViews } from "../viewmodels/dude-profile-third-page.views"
import { GeneralResourcesStoreModel } from "../../../../../../models/general-resources-store"

const DudeProfileThirdScreenModel = types.model(StoreName.DudeProfileDetailThird, DudeProfileThirdScreenPropsModel)
export const DudeProfileThirdScreenStoreModel = types.compose(DudeProfileThirdScreenModel, DudeProfileThirdScreenViews, DudeProfileThirdScreenActions)
export const DudeProfileThirdScreenResourcesStoreModel = types.compose(GeneralResourcesStoreModel, DudeProfileThirdScreenResourcesViews)

