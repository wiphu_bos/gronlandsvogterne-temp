import { Instance, SnapshotOut } from "mobx-state-tree"
import { DudeProfileThirdScreenStoreModel, DudeProfileThirdScreenResourcesStoreModel } from "./dude-profile-third-page.store"

export type DudeProfileThirdScreenStore = Instance<typeof DudeProfileThirdScreenStoreModel>
export type DudeProfileThirdScreenStoreSnapshot = SnapshotOut<typeof DudeProfileThirdScreenStoreModel>

export type DudeProfileThirdScreenResourcesStore = Instance<typeof DudeProfileThirdScreenResourcesStoreModel>
export type DudeProfileThirdScreenResourcesStoreSnapshot = SnapshotOut<typeof DudeProfileThirdScreenResourcesStoreModel>