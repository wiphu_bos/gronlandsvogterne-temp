// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import LinearGradient from "react-native-linear-gradient"
import * as metric from "../../../../../theme"
import DudeProfileSecondPageController from "./dude-profile-second-page.controllers"
import { INavigationRoute } from "../../../../../models/navigation-store/navigation.types"
import { AnimatedScrollView, Text } from "../../../../../components"
import * as StringUtils from "../../../../../utils/string.utils"
import { View } from "react-native"
import { DudeProfileDetailPageProps } from "./dude-profile-second-page.props"
import { useConfigurate } from "../../../../../custom-hooks/use-configure-controller"
import DudeProfileDetailController from "../../../dude-profile-detail-tab/dude-profile-detail.controllers"

// MARK: Style Import

import * as Styles from "./dude-profile-second-page.styles"

export const DudeProfileDetailSecondScreen: React.FunctionComponent<DudeProfileDetailPageProps & Partial<INavigationRoute>> = observer((props) => {

    const controller = useConfigurate(DudeProfileSecondPageController, props) as DudeProfileSecondPageController

    // MARK: Render
    return (
        <LinearGradient shouldRasterizeIOS style={Styles.CONTAINNER} {...Styles.TAB_PAGE_BACKGROUND}>
            <View shouldRasterizeIOS style={Styles.BODY_CONTAINER}>
                <AnimatedScrollView
                    contentContainerStyle={Styles.SCROLL_VIEW}
                    {...metric.solidScrollView}>
                    <View shouldRasterizeIOS style={Styles.CONTENT_CONTAINNER}>
                        <Text style={Styles.TITLE}
                            {...DudeProfileSecondPageController.resourcesViewModel?.getTestIDHavdSogerHanTitle()}
                            text={DudeProfileSecondPageController.resourcesViewModel?.getResourceHavdSogerHanTitle()} />
                        <Text style={Styles.CONTENT} {...DudeProfileSecondPageController.resourcesViewModel?.getTestIDHavdSogerHanContent()}>
                            {controller.renderSeeking(DudeProfileDetailController.viewModel?.getHavdSogerHanList)}
                        </Text>

                        <Text style={Styles.TITLE}
                            {...DudeProfileSecondPageController.resourcesViewModel?.getTestIDInterestTitle()}
                            text={DudeProfileSecondPageController.resourcesViewModel?.getResourceInterestTitle()} />
                        <Text style={Styles.CONTENT} {...DudeProfileSecondPageController.resourcesViewModel?.getTestIDInterestContent()}>
                            {controller.renderInterest(DudeProfileDetailController.viewModel?.getInterestList)}
                        </Text>

                        <Text style={Styles.TITLE}
                            {...DudeProfileSecondPageController.resourcesViewModel?.getTestIDDescriptionTitle()}
                            text={DudeProfileSecondPageController.resourcesViewModel?.getResourceDetailTitle()} />
                        <Text style={Styles.CONTENT} {...DudeProfileSecondPageController.resourcesViewModel?.getTestIDDescriptionContent()}>
                            {StringUtils.brToNewLine(DudeProfileDetailController.viewModel?.getDescription)}
                        </Text>
                    </View>
                </AnimatedScrollView>
            </View>
        </LinearGradient>
    )
})
