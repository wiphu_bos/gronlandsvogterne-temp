import { types } from "mobx-state-tree"
import { DudeProfileSecondScreenPropsModel } from "./dude-profile-second-page.models"
import { GeneralResources } from "../../../../../../constants/firebase/remote-config"
import { NavigationKey } from "../../../../../../constants/app.constant"
import { TestIDResources } from "../../../../../../constants/test-key/test.constant"
import * as Utils from "../../../../../../utils"
import { GeneralResourcesStoreModel } from "../../../../../../models/general-resources-store"

export const DudeProfileSecondScreenViews = types.model(DudeProfileSecondScreenPropsModel)
    .views(self => ({
        get getExample() {
            return self
        }
    }))

export const DudeProfileSecondScreenResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State

    const { DudeProfileSecondScreen } = GeneralResources
    const { interestTitle,
        havdSogerHanTitle,
        detailTitle,
    } = DudeProfileSecondScreen

    //MARK: Views
    const getResources = (key: string) => self.getValues(NavigationKey.DudeProfileDetailScreen, key)
    const getResourceInterestTitle = () => getResources(interestTitle)
    const getResourceHavdSogerHanTitle = () => getResources(havdSogerHanTitle)
    const getResourceDetailTitle = () => getResources(detailTitle)

    return {
        getResourceInterestTitle,
        getResourceHavdSogerHanTitle,
        getResourceDetailTitle
    }
})
    .views(self => {

        //MARK: Volatile State

        //MARK: Views

        const getTestIDInterestTitle = () => Utils.getTestIDObject(TestIDResources.DudeProfileSecondScreen.interestTitle)
        const getTestIDHavdSogerHanTitle = () => Utils.getTestIDObject(TestIDResources.DudeProfileSecondScreen.havdSogerHanTitle)
        const getTestIDDescriptionTitle = () => Utils.getTestIDObject(TestIDResources.DudeProfileSecondScreen.descriptionTitle)
        const getTestIDInterestContent = () => Utils.getTestIDObject(TestIDResources.DudeProfileSecondScreen.interestContent)
        const getTestIDHavdSogerHanContent = () => Utils.getTestIDObject(TestIDResources.DudeProfileSecondScreen.descriptionTitle)
        const getTestIDDescriptionContent = () => Utils.getTestIDObject(TestIDResources.DudeProfileSecondScreen.descriptionContent)

        return {
            getTestIDInterestTitle,
            getTestIDHavdSogerHanTitle,
            getTestIDDescriptionTitle,
            getTestIDInterestContent,
            getTestIDHavdSogerHanContent,
            getTestIDDescriptionContent
        }
    })
