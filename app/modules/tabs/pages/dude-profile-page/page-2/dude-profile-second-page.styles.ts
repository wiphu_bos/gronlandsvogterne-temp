import { ViewStyle, TextStyle } from "react-native"
import { color } from "../../../../../theme"
import * as metric from "../../../../../theme"
import { RFValue } from "react-native-responsive-fontsize"

export const FULL: ViewStyle = {
    flex: 1
}
export const TAB_PAGE_BACKGROUND: any = { colors: color.tabPageBackground }

export const CONTAINNER: ViewStyle = {
    ...FULL
}

export const CONTENT_CONTAINNER: ViewStyle = {
    ...FULL
}

export const BODY_CONTAINER: ViewStyle = {
    flex: 0.8,
    marginHorizontal: metric.ratioWidth(35),
}

export const SCROLL_VIEW: ViewStyle = {
    paddingBottom: metric.ratioHeight(10),
}

export const TITLE: TextStyle = {
    color: color.palette.lightPink,
    fontSize: RFValue(19),
    marginTop: metric.ratioHeight(34),
    fontWeight: '500',

    //Android
    fontFamily: "Montserrat-Medium",
}
export const CONTENT: TextStyle = {
    color: color.palette.white,
    fontSize: RFValue(16),
    fontWeight: '400',
    marginTop: metric.ratioHeight(11),

    //Android
    fontFamily: "Montserrat-Regular",
}