// MARK: Import
import { NavigationKey } from "../../../../../constants/app.constant"
import { RootStore, INavigationRoute } from "../../../../../models"
import { DudeProfileDetailPageProps } from "./dude-profile-second-page.props"
import { DudeProfileSecondScreenResourcesStore, DudeProfileSecondScreenStore } from "./storemodels/dude-profile-second-page.types"
import { DudeProfileSecondScreenResourcesStoreModel, DudeProfileSecondScreenStoreModel } from "./storemodels/dude-profile-second-page.store"
import { ISelect } from "../../../../../models/user-store"
import { BaseController } from "../../../../base.controller"
import { onSnapshot } from "mobx-state-tree"

class DudeProfileSecondPageController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static viewModel: DudeProfileSecondScreenStore
    public static resourcesViewModel: DudeProfileSecondScreenResourcesStore

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, myProps?: DudeProfileDetailPageProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, myProps, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(myProps)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        DudeProfileSecondPageController.resourcesViewModel = DudeProfileSecondScreenResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.DudeProfileDetailSecond)
        DudeProfileSecondPageController.viewModel = localStore && DudeProfileSecondScreenStoreModel.create({ ...localStore }) || DudeProfileSecondScreenStoreModel.create({})
    }
    private _setupSnapShot = () => {
        onSnapshot(DudeProfileSecondPageController.viewModel, (snap: DudeProfileSecondScreenStore) => {
            this._setInitializedToPropsParams()
        })
    }

    /*
       Mark Data
   */

    public renderSeeking = (dudeSeekings: ISelect[]) => {
        return dudeSeekings?.map(obj => obj.value).join(', ')
    }

    public renderInterest = (dudeInterests: ISelect[]) => {
        return dudeInterests?.map(obj => obj.value).join(', ')
    }

    /*
       Mark Event
   */

    /*
       Mark Life cycle
   */

    /*
       Mark Helper
   */
}

export default DudeProfileSecondPageController