import { Instance, SnapshotOut } from "mobx-state-tree"
import { DudeProfileSecondScreenStoreModel, DudeProfileSecondScreenResourcesStoreModel } from "./dude-profile-second-page.store"

export type DudeProfileSecondScreenStore = Instance<typeof DudeProfileSecondScreenStoreModel>
export type DudeProfileSecondScreenStoreSnapshot = SnapshotOut<typeof DudeProfileSecondScreenStoreModel>

export type DudeProfileSecondScreenResourcesStore = Instance<typeof DudeProfileSecondScreenResourcesStoreModel>
export type DudeProfileSecondScreenResourcesStoreSnapshot = SnapshotOut<typeof DudeProfileSecondScreenResourcesStoreModel>