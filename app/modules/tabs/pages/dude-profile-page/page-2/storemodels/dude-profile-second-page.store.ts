import { types } from "mobx-state-tree"
import { StoreName } from "../../../../../../constants/app.constant"
import { DudeProfileSecondScreenPropsModel } from "../viewmodels/dude-profile-second-page.models"
import { DudeProfileSecondScreenActions } from "../viewmodels/dude-profile-second-page.actions"
import { DudeProfileSecondScreenViews, DudeProfileSecondScreenResourcesViews } from "../viewmodels/dude-profile-second-page.views"
import { GeneralResourcesStoreModel } from "../../../../../../models/general-resources-store"

const DudeProfileSecondScreenModel = types.model(StoreName.DudeProfileDetailSecond, DudeProfileSecondScreenPropsModel)
export const DudeProfileSecondScreenStoreModel = types.compose(DudeProfileSecondScreenModel, DudeProfileSecondScreenViews, DudeProfileSecondScreenActions)
export const DudeProfileSecondScreenResourcesStoreModel = types.compose(GeneralResourcesStoreModel, DudeProfileSecondScreenResourcesViews)

