import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { DudeProfileSecondScreenStore } from "./storemodels/dude-profile-second-page.types"
import { DudeProfileStore } from '../../../dude-profile-detail-tab/storemodels/dude-profile-detail.types'

type DudeProfileStoreModel =
    | DudeProfileSecondScreenStore

export interface DudeProfileDetailPageProps extends NavigationContainerProps<ParamListBase> {
    rootViewModel?: DudeProfileStore
    viewModel?: DudeProfileStoreModel
}