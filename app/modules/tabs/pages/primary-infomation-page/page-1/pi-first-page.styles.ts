import { ViewStyle, TextStyle, ImageStyle } from "react-native"
import { color } from "../../../../../theme"
import * as metric from "../../../../../theme"
import { RFValue } from "react-native-responsive-fontsize"
export const FULL: ViewStyle = {
    flex: 1
}

export const TAB_PAGE_BACKGROUND: any = { colors: color.tabPageBackground }

export const CONTAINNER: ViewStyle = {
    ...FULL
}

export const BODY_CONTAINER: ViewStyle = {
    ...FULL,
}

export const AVOID_KEYBOARD_VIEW: ViewStyle = {
    ...FULL
}
export const SCROLL_VIEW: ViewStyle = {
    paddingBottom: metric.ratioHeight(10),
}

export const CONTENT_CONTAINER: ViewStyle = {
    ...FULL,
    marginHorizontal: metric.ratioWidth(35),
    paddingVertical: metric.ratioHeight(29),
}

export const HEADER: ViewStyle = {
    ...FULL,
    flexDirection: 'row',
    zIndex: 1
}

export const TEXT_FIELD_CONTAINER: ViewStyle = {
    ...FULL,
    alignItems: 'center'
}

export const TEXT_FIELD_ITEM: ViewStyle = {
    width: metric.ratioWidth(344),
}

export const TEXT: TextStyle = {
    color: color.palette.white,
    fontFamily: "Montserrat",
}
export const TITLE: TextStyle = {
    ...TEXT,
    fontSize: RFValue(12),
    fontWeight: '400',

    //Android
    fontFamily: "Montserrat-Regular",
}

export const TRIPPLE_DOTS_CONTAINER: ViewStyle = {
    ...FULL,
    flexDirection: 'column',
    alignItems: 'flex-end'
}

export const ICON_SELECTION: ImageStyle = {
    marginLeft: metric.ratioWidth(18),
    width: metric.ratioWidth(21),
    height: metric.ratioWidth(21),
    marginRight: metric.ratioWidth(28),
}
export const BUTTON_MARGIN: ViewStyle = {
    marginTop: metric.ratioHeight(58),
    alignSelf: 'center',
}