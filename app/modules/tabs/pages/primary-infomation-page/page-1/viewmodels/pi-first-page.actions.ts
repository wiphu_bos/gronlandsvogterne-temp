import { types } from "mobx-state-tree"
import { PrimaryInformationFirstPropsModel } from "./pi-first-page.models"

export const PrimaryInformationFirstActions = types.model(PrimaryInformationFirstPropsModel).actions(self => {

    //MARK: Volatile State

    //MARK: Actions
    const setIsPopoverMenuVisible = (value: boolean) => self.isPopoverMenuVisible = value
    const setIsModalShowing = (value: boolean) => self.isModalShowing = value
    const setIsAlertModalShowing = (value: boolean) => self.isAlertModalShowing = value
    return {
        setIsPopoverMenuVisible,
        setIsModalShowing,
        setIsAlertModalShowing
    }
})