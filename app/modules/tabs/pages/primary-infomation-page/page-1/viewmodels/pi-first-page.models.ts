import { types } from "mobx-state-tree"

export const PrimaryInformationFirstPropsModel = {
    isPopoverMenuVisible: types.optional(types.boolean, false),
    isModalShowing: types.optional(types.boolean, false),
    isAlertModalShowing: types.optional(types.boolean, false)
} 