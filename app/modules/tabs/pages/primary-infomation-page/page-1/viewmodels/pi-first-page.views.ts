import { types } from "mobx-state-tree"
import { PrimaryInformationFirstPropsModel } from "./pi-first-page.models"
import { GeneralResources } from "../../../../../../constants/firebase/remote-config"
import { NavigationKey } from "../../../../../../constants/app.constant"
import { TestIDResources } from "../../../../../../constants/test-key/test.constant"
import * as Utils from "../../../../../../utils"
import { GeneralResourcesStoreModel } from "../../../../../../models/general-resources-store"
import * as DataUtils from "../../../../../../utils/data.utils"

export const PrimaryInformationFirstViews = types.model(PrimaryInformationFirstPropsModel)
    .views(self => ({
        get getIsPopoverMenuVisible() {
            return self.isPopoverMenuVisible
        },
        get getIsModalShowing() {
            return self.isModalShowing
        }
        ,
        get getIsAlertModalShowing() {
            return self.isAlertModalShowing
        }
    }))

export const PrimaryInformationFirstResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State

    const { PrimaryInformationFirstScreen } = GeneralResources
    const { statusTitle,
        fullNamePlaceholder,
        emailPlaceholder,
        agePlaceholder,
        cityPlaceholder,
        interestPlaceholder,
        havdSogerHanPlaceholder,
        tagTitle,
        modalButtonCancelTitle,
        modalButtonClearTitle,
        modalButtonOKTitle,
        modalCityPlaceholder,
        modalHavdSogerHandPlaceholder,
        modalAgePlaceholder,
        buttonActivateTitle,
        buttonDeactivateTitle,
        buttonDeleteTitle,
        buttonSaveAndContinueTitle,
        modalInterestPlaceholder,
        modalConfirmChangeStatusDude,
        modalConfirmDeleteDude
    } = PrimaryInformationFirstScreen

    //MARK: Views
    const getShareResources = (key: string) => self.getValues(key, true)
    const getResources = (key: string) => self.getValues(NavigationKey.PrimaryInformationScreen, key)
    const getResourceStatusTitle = () => getShareResources(statusTitle)
    const getResourceFullNamePlaceholder = () => getResources(fullNamePlaceholder)
    const getResourceEmailPlaceholder = () => getResources(emailPlaceholder)
    const getResourceAgePlaceholder = () => getResources(agePlaceholder)
    const getResourceInterestPlaceholder = () => getResources(interestPlaceholder)
    const getResourceHavdSogerHanPlaceholder = () => getResources(havdSogerHanPlaceholder)
    const getResourceTagTitle = () => getResources(tagTitle)
    const getResourceCityPlaceholder = () => getResources(cityPlaceholder)
    const getResourceModalButtonCancelTitle = () => getShareResources(modalButtonCancelTitle)
    const getResourceModalButtonClearTitle = () => getShareResources(modalButtonClearTitle)
    const getResourceModalButtonOKTitle = () => getShareResources(modalButtonOKTitle)
    const getResourceModalCityPlaceholder = () => getShareResources(modalCityPlaceholder)
    const getResourceModalAgePlaceholder = () => getShareResources(modalAgePlaceholder)
    const getResourceModalHavdSogerHandPlaceholder = () => getShareResources(modalHavdSogerHandPlaceholder)
    const getResourceModalInterestPlaceholder = () => getShareResources(modalInterestPlaceholder)
    const getResourceButtonActivateTitle = () => getResources(buttonActivateTitle)
    const getResourceButtonDeactivateTitle = () => getResources(buttonDeactivateTitle)
    const getResourceButtonDeleteTitle = () => getShareResources(buttonDeleteTitle)
    const getResourceButtonSaveAndContinueTitle = () => getShareResources(buttonSaveAndContinueTitle)
    const getResourceUserStatusTitle = (status: string) => DataUtils.getUserStatusByLocaleKey(self, status)
    const getResourceModalConfirmChangeStatusDude = () => getShareResources(modalConfirmChangeStatusDude)
    const getResourceModalConfirmDeleteDude = () => getShareResources(modalConfirmDeleteDude)
    return {
        getResourceStatusTitle,
        getResourceFullNamePlaceholder,
        getResourceEmailPlaceholder,
        getResourceAgePlaceholder,
        getResourceHavdSogerHanPlaceholder,
        getResourceInterestPlaceholder,
        getResourceTagTitle,
        getResourceCityPlaceholder,
        getResourceModalButtonCancelTitle,
        getResourceModalCityPlaceholder,
        getResourceModalAgePlaceholder,
        getResourceButtonDeactivateTitle,
        getResourceButtonDeleteTitle,
        getResourceModalButtonClearTitle,
        getResourceModalButtonOKTitle,
        getResourceButtonSaveAndContinueTitle,
        getResourceModalHavdSogerHandPlaceholder,
        getResourceModalInterestPlaceholder,
        getResourceUserStatusTitle,
        getResourceButtonActivateTitle,
        getResourceModalConfirmChangeStatusDude,
        getResourceModalConfirmDeleteDude
    }
})
    .views(self => {

        //MARK: Volatile State

        //MARK: Views

        const getTestIDStatusTitle = () => Utils.getTestIDObject(TestIDResources.PrimaryInformationFirstScreen.status)
        const getTestIDApproveStatusTitle = () => Utils.getTestIDObject(TestIDResources.PrimaryInformationFirstScreen.approveStatus)
        const getTestIDFullNamePlaceholder = () => Utils.getTestIDObject(TestIDResources.PrimaryInformationFirstScreen.fullNamePlaceholder)
        const getTestIDEmailPlaceholder = () => Utils.getTestIDObject(TestIDResources.PrimaryInformationFirstScreen.emailPlaceholder)
        const getTestIDDropdownAge = () => Utils.getTestIDObject(TestIDResources.PrimaryInformationFirstScreen.dropdownAge)
        const getTestIDDropdownCity = () => Utils.getTestIDObject(TestIDResources.PrimaryInformationFirstScreen.dropdownCity)
        const getTestIDPickerInterest = () => Utils.getTestIDObject(TestIDResources.PrimaryInformationFirstScreen.pickerInterest)
        const getTestIDPickerHavdSogerHan = () => Utils.getTestIDObject(TestIDResources.PrimaryInformationFirstScreen.pickerHavdSogerHan)
        const getTestIDTagTitle = () => Utils.getTestIDObject(TestIDResources.PrimaryInformationFirstScreen.tagTitle)
        const getTestIDButtonSaveAndContinue = () => Utils.getTestIDObject(TestIDResources.PrimaryInformationFirstScreen.buttonSaveAndContinue)
        const getTestIDModalPicker = () => Utils.getTestIDObject(TestIDResources.CreateAccountScreen.modalPicker)
        const getTestIDErrorMessage = () => Utils.getTestIDObject(TestIDResources.VerifyUserScreen.errorMessage)
        const getTestIDButtonThreeDotsMenu = () => Utils.getTestIDObject(TestIDResources.PrimaryInformationFirstScreen.buttonThreeDotsMenu)
        const getTestIDButtonDeactivateTitle = () => Utils.getTestIDObject(TestIDResources.PrimaryInformationFirstScreen.buttonDeactivate)
        const getTestIDButtonDeleteTitle = () => Utils.getTestIDObject(TestIDResources.PrimaryInformationFirstScreen.buttonDelete)
        return {
            getTestIDStatusTitle,
            getTestIDFullNamePlaceholder,
            getTestIDEmailPlaceholder,
            getTestIDDropdownAge,
            getTestIDDropdownCity,
            getTestIDApproveStatusTitle,
            getTestIDPickerInterest,
            getTestIDPickerHavdSogerHan,
            getTestIDTagTitle,
            getTestIDButtonSaveAndContinue,
            getTestIDModalPicker,
            getTestIDErrorMessage,
            getTestIDButtonThreeDotsMenu,
            getTestIDButtonDeactivateTitle,
            getTestIDButtonDeleteTitle
        }
    })
