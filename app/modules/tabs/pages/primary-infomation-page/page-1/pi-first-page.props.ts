import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { CreateEditProfileStore } from "../../../create-edit-profile-tab/storemodels"
import { PrimaryInformationFirstStore } from "./storemodels/pi-first-page.types"
import { UserStore } from '../../../../../models/user-store'
import { PopoverMenuActionType } from '../../../../../components/popover-menu/popover-menu.props'

type PrimaryInformationStore =
    | PrimaryInformationFirstStore

export interface PrimaryInformationProps extends NavigationContainerProps<ParamListBase> {
    dudeProfileObject?: UserStore
    rootViewModel?: CreateEditProfileStore
    observeViewModel?: PrimaryInformationStore,
    onPopoverMenuPress?: () => void,
    forceClosePopoverMenu?: () => void,
    onPopoverMenuItemPressed?: (type: PopoverMenuActionType) => void,
    onPopoverClosed?: () => void
}