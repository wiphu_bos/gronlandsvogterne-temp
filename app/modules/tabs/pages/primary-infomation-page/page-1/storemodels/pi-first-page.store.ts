import { types } from "mobx-state-tree"
import { StoreName } from "../../../../../../constants/app.constant"
import { PrimaryInformationFirstPropsModel } from "../viewmodels/pi-first-page.models"
import { PrimaryInformationFirstActions } from "../viewmodels/pi-first-page.actions"
import { PrimaryInformationFirstViews, PrimaryInformationFirstResourcesViews } from "../viewmodels/pi-first-page.views"
import { GeneralResourcesStoreModel } from "../../../../../../models/general-resources-store"
import { PrimaryInformationFirstServiceActions } from "../services/pi-first-page.services"

const PrimaryInformationFirstModel = types.model(StoreName.PrimaryInformationFirst, PrimaryInformationFirstPropsModel)
export const PrimaryInformationFirstStoreModel = types.compose(PrimaryInformationFirstModel, PrimaryInformationFirstViews, PrimaryInformationFirstActions, PrimaryInformationFirstServiceActions)
export const PrimaryInformationFirstResourcesStoreModel = types.compose(GeneralResourcesStoreModel, PrimaryInformationFirstResourcesViews)

