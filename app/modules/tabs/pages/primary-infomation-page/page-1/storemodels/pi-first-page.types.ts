import { Instance, SnapshotOut } from "mobx-state-tree"
import { PrimaryInformationFirstStoreModel, PrimaryInformationFirstResourcesStoreModel } from "./pi-first-page.store"

export type PrimaryInformationFirstStore = Instance<typeof PrimaryInformationFirstStoreModel>
export type PrimaryInformationFirstStoreSnapshot = SnapshotOut<typeof PrimaryInformationFirstStoreModel>

export type PrimaryInformationFirstResourcesStore = Instance<typeof PrimaryInformationFirstResourcesStoreModel>
export type PrimaryInformationFirstResourcesStoreSnapshot = SnapshotOut<typeof PrimaryInformationFirstResourcesStoreModel>