import { types, flow } from "mobx-state-tree"
import { PrimaryInformationFirstPropsModel } from "../viewmodels"
import { APIResponse, APISuccessResponse } from "../../../../../../constants/service.types"
import { RootStore } from "../../../../../../models/root-store"
import { IUser } from "../../../../../../models/user-store/user.types"

export const PrimaryInformationFirstServiceActions = types.model(PrimaryInformationFirstPropsModel).actions(self => {
    const changeDudeStatus = flow(function* (rootStore: RootStore, dude_id: string, status: string) {
        try {
            const result: APIResponse = yield rootStore?.UserStore?.changeDudeStatus(dude_id, status)
            let success = (result as APISuccessResponse)?.data
            let data: IUser = success?.data
            if (!data) throw result
            return success
        } catch (e) {
            return e
        }
    })

    const deleteDude = flow(function* (rootStore?: RootStore, dude_id?: string) {
        try {
            const result: APIResponse = yield rootStore?.getUserStore?.deleteDude(dude_id)
            let success = (result as APISuccessResponse)?.data
            let data: IUser = success?.data
            if (!data) throw result
            return success
        } catch (e) {
            return e
        }
    })

    return {
        changeDudeStatus,
        deleteDude
    }
})