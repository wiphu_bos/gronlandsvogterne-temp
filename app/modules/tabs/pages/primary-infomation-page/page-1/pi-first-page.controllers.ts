// MARK: Import
import { NavigationKey } from "../../../../../constants/app.constant"
import { RootStore, INavigationRoute } from "../../../../../models"
import { PrimaryInformationProps } from "./pi-first-page.props"
import { Keyboard } from "react-native"
import { PrimaryInformationFirstResourcesStore, PrimaryInformationFirstStore } from "./storemodels/pi-first-page.types"
import { PrimaryInformationFirstResourcesStoreModel, PrimaryInformationFirstStoreModel } from "./storemodels/pi-first-page.store"
import * as Validate from "../../../../../utils/local-validate"
import { SelectionType } from "../../../../../models/validation-store/validation-store.model"
import { ISelect } from "../../../../../models/user-store/user.types"
import { PopoverMenuActionType } from "../../../../../components/popover-menu/popover-menu.props"
import CreateEditProfileController from "../../../create-edit-profile-tab/create-edit-profile.controllers"
import * as DataUtils from "../../../../../utils/data.utils"
import { ConfirmModalProps } from "../../../../../components/confirm-modal/confirm-modal.props"
import { BaseController, IAlertParams } from "../../../../base.controller"
import { onSnapshot } from "mobx-state-tree"
import { CommonActions } from "@react-navigation/core"
import { ThreeDotMenuProps } from "../../../../../components/ThreeDotsMenuComponent/three-dot-menu.props"
class PrimaryInformationFirstPageController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static resourcesViewModel: PrimaryInformationFirstResourcesStore
    public static viewModel: PrimaryInformationFirstStore

    private static _ageContentList: ISelect[]
    private static _ageSelected: string
    private static _ageSelectedIndex: number

    private static _cityContentList: ISelect[]
    private static _citySelected: string
    private static _citySelectedIndex: number

    private static _havdSogerHanContentList: ISelect[]
    private static _havdSogerHanListSelected: any[]
    private static _havdSogerHanSelectedFirstIndex: number

    private static _interestContentList: ISelect[]
    private static _interestListSelected: any[]
    private static _interestSelectedFirstIndex: number

    private static _tagListSelected: ISelect[]
    private static _selectionTypeSelected: SelectionType

    private static _popoverMenuActionTypeSelected: PopoverMenuActionType
    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, props?: PrimaryInformationProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, props, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(props)
        this._setupViewModel()
        this._setupSnapShot()
        this._getDropdownContents()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        PrimaryInformationFirstPageController.resourcesViewModel = PrimaryInformationFirstResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.CreateEditProfileFirst)
        PrimaryInformationFirstPageController.viewModel = localStore && PrimaryInformationFirstStoreModel.create({ ...localStore }) || PrimaryInformationFirstStoreModel.create({})
    }

    private _setupSnapShot = () => {
        onSnapshot(PrimaryInformationFirstPageController.viewModel, (snap: PrimaryInformationFirstStore) => {
            this._setInitializedToPropsParams()
        })
    }

    /*
       Mark Data
    */


    private _getDropdownContents = () => {
        this._getCityContent()
        this._getAgeContent()
        this._getInterestContent()
        this._getHavdSogerHanContent()
        this._getTagContent()
    }

    public getMultitpleSelectionDataType = (list?: ISelect[]) => list?.map(e => e.value).join(', ').toString()

    private _singleSelectionMenuOptions = {
        onSelect: (item: any) => this.onSelectItem(item),
        onCancel: () => this.onCancelModal()
    }

    private _multipleSelectionMenuOptions = {
        onOK: (items: any[]) => this.onSelectItems(items),
        onClose: () => this.onCloseModal()
    }

    private _getTagContent = () => {
        PrimaryInformationFirstPageController._tagListSelected = CreateEditProfileController.viewModel?.getTagList || []
    }

    private _getInterestContent = () => {
        PrimaryInformationFirstPageController._interestContentList = this._rootStore?.getSharedStore?.getInterests || []
        PrimaryInformationFirstPageController._interestListSelected = CreateEditProfileController.viewModel?.getInterestList && CreateEditProfileController.viewModel?.getInterestList?.map((e) => e) || null
        const interestSelectedFirstObject = PrimaryInformationFirstPageController._interestListSelected?.length > 0 ? PrimaryInformationFirstPageController._interestListSelected[0] : 0
        PrimaryInformationFirstPageController._interestSelectedFirstIndex = PrimaryInformationFirstPageController._interestContentList?.findIndex((e) => {
            if (!interestSelectedFirstObject) return true
            return e.key === interestSelectedFirstObject?.key
        })
        PrimaryInformationFirstPageController._interestSelectedFirstIndex = PrimaryInformationFirstPageController._interestSelectedFirstIndex < 0 ? 0 : PrimaryInformationFirstPageController._interestSelectedFirstIndex
    }

    private _getHavdSogerHanContent = () => {
        PrimaryInformationFirstPageController._havdSogerHanContentList = this._rootStore?.getSharedStore?.getHavdSogerHans || []
        PrimaryInformationFirstPageController._havdSogerHanListSelected = CreateEditProfileController.viewModel?.getHavdSogerHanList && CreateEditProfileController.viewModel?.getHavdSogerHanList?.map((e) => e) || null
        const havdSogerHanSelectedFirstObject = PrimaryInformationFirstPageController._havdSogerHanListSelected?.length > 0 ? PrimaryInformationFirstPageController._havdSogerHanListSelected[0] : 0
        PrimaryInformationFirstPageController._havdSogerHanSelectedFirstIndex = PrimaryInformationFirstPageController._havdSogerHanContentList?.findIndex((e) => {
            if (!havdSogerHanSelectedFirstObject) return true
            return e.key === havdSogerHanSelectedFirstObject?.key
        })
        PrimaryInformationFirstPageController._havdSogerHanSelectedFirstIndex = PrimaryInformationFirstPageController._havdSogerHanSelectedFirstIndex < 0 ? 0 : PrimaryInformationFirstPageController._havdSogerHanSelectedFirstIndex
    }

    private _getCityContent = () => {
        PrimaryInformationFirstPageController._cityContentList = this._rootStore?.getSharedStore?.getCities || []
        PrimaryInformationFirstPageController._citySelected = CreateEditProfileController.viewModel?.getCity?.key
        PrimaryInformationFirstPageController._citySelectedIndex = PrimaryInformationFirstPageController._cityContentList?.findIndex((e) => {
            if (!PrimaryInformationFirstPageController._citySelected) return true
            return e.key === PrimaryInformationFirstPageController._citySelected
        })
        PrimaryInformationFirstPageController._citySelectedIndex = PrimaryInformationFirstPageController._citySelectedIndex < 0 ? 0 : PrimaryInformationFirstPageController._citySelectedIndex
    }

    private _getAgeContent = () => {
        PrimaryInformationFirstPageController._ageContentList = this._rootStore?.getSharedStore?.getAge || []
        PrimaryInformationFirstPageController._ageSelected = CreateEditProfileController.viewModel?.getAge?.key
        PrimaryInformationFirstPageController._ageSelectedIndex = DataUtils.getAgeSelectedIndex(PrimaryInformationFirstPageController._ageSelected, PrimaryInformationFirstPageController._ageContentList)
    }

    public getSelectOptions = () => {
        const cityOptions = {
            flatListViewProps: { selectedIndex: PrimaryInformationFirstPageController._citySelectedIndex },
            selectedOption: PrimaryInformationFirstPageController._citySelected,
            placeholderText: PrimaryInformationFirstPageController.resourcesViewModel?.getResourceModalCityPlaceholder(),
            options: PrimaryInformationFirstPageController._cityContentList,
            ...this._singleSelectionMenuOptions
        }
        const ageOptions = {
            flatListViewProps: { selectedIndex: PrimaryInformationFirstPageController._ageSelectedIndex },
            selectedOption: PrimaryInformationFirstPageController._ageSelected,
            placeholderText: PrimaryInformationFirstPageController.resourcesViewModel?.getResourceModalAgePlaceholder(),
            options: PrimaryInformationFirstPageController._ageContentList,
            keyboardType: "phone-pad",
            ...this._singleSelectionMenuOptions
        }
        const havdSogerHanOptions = {
            flatListViewProps: { selectedIndex: PrimaryInformationFirstPageController._havdSogerHanSelectedFirstIndex },
            isMultipleSelectionDropDown: true,
            selectedOptions: CreateEditProfileController.viewModel?.getHavdSogerHanList && DataUtils.getModalContentList(CreateEditProfileController.viewModel?.getHavdSogerHanList, true),
            placeholderText: PrimaryInformationFirstPageController.resourcesViewModel?.getResourceModalHavdSogerHandPlaceholder(),
            options: PrimaryInformationFirstPageController._havdSogerHanContentList,
            ...this._multipleSelectionMenuOptions
        }
        const interestOptions = {
            flatListViewProps: { selectedIndex: PrimaryInformationFirstPageController._interestSelectedFirstIndex },
            isMultipleSelectionDropDown: true,
            selectedOptions: CreateEditProfileController.viewModel?.getInterestList && DataUtils.getModalContentList(CreateEditProfileController.viewModel?.getInterestList, true),
            placeholderText: PrimaryInformationFirstPageController.resourcesViewModel?.getResourceModalInterestPlaceholder(),
            options: PrimaryInformationFirstPageController._interestContentList,
            ...this._multipleSelectionMenuOptions
        }

        switch (PrimaryInformationFirstPageController._selectionTypeSelected) {
            case SelectionType.CITY:
                return cityOptions
            case SelectionType.AGE:
                return ageOptions
            case SelectionType.HavdSogerHan:
                return havdSogerHanOptions
            case SelectionType.Interest:
                return interestOptions
            default:
                return null
        }
    }

    /*
       Mark Event
    */
    private _onOpenAlertModal = () => {
        PrimaryInformationFirstPageController.viewModel?.setIsAlertModalShowing(true)
    }
    private _onCloseAlertModal = () => {
        PrimaryInformationFirstPageController.viewModel?.setIsAlertModalShowing(false)
    }

    public onTagPressed = (item: ISelect) => {
        const { key, value } = item
        const params = {
            localViewModel: CreateEditProfileController.viewModel as any,
            rootStore: this._rootStore
        }
        const foundItemIndex = PrimaryInformationFirstPageController._tagListSelected?.findIndex(v => v.key === key)
        const transformList = PrimaryInformationFirstPageController._tagListSelected?.map(e => {
            return { key: e.key, value: e.value }
        })

        let selectedList = transformList || []
        if (selectedList?.length > 0 && foundItemIndex !== -1) {
            selectedList.splice(foundItemIndex, 1)
        } else {
            selectedList?.push({ key: key, value: value })
        }
        PrimaryInformationFirstPageController._tagListSelected = selectedList
        const isInvalid = { isInvalid: PrimaryInformationFirstPageController._tagListSelected.length < 3 }
        const validateParams = { ...params, ...isInvalid }
        Validate.onErrorMessageTagListSelection(validateParams)
        CreateEditProfileController.viewModel?.setTagList(selectedList)
    }

    public onSelectItem = (item?: any): void => {
        const { key, label } = item
        const selectedItem = { key: key, value: label }
        if (PrimaryInformationFirstPageController._selectionTypeSelected === SelectionType.CITY) {
            PrimaryInformationFirstPageController._citySelected = key
            CreateEditProfileController.viewModel?.setCity(selectedItem)
            PrimaryInformationFirstPageController._citySelectedIndex = PrimaryInformationFirstPageController._cityContentList?.findIndex((e) => {
                if (!key) return true
                return e.key === key
            })
        } else if (PrimaryInformationFirstPageController._selectionTypeSelected == SelectionType.AGE) {
            PrimaryInformationFirstPageController._ageSelected = key
            CreateEditProfileController.viewModel?.setAge(selectedItem)
            PrimaryInformationFirstPageController._ageSelectedIndex = DataUtils.getAgeSelectedIndex(PrimaryInformationFirstPageController._ageSelected, PrimaryInformationFirstPageController._ageContentList)
        }
        this.onCancelModal()
    }

    public onCancelModal = (): void => {
        const params = {
            localViewModel: CreateEditProfileController.viewModel as any,
            rootStore: this._rootStore
        }
        PrimaryInformationFirstPageController.viewModel?.setIsModalShowing(false)
        if (PrimaryInformationFirstPageController._selectionTypeSelected === SelectionType.CITY) {
            const isInvalid = { isInvalid: !(CreateEditProfileController.viewModel?.getCity?.key) }
            const validateParams = { ...params, ...isInvalid }
            Validate.onErrorMessageCityField(validateParams)
        } else if (PrimaryInformationFirstPageController._selectionTypeSelected == SelectionType.AGE) {
            const isInvalid = { isInvalid: !(CreateEditProfileController.viewModel?.getAge?.key) }
            const validateParams = { ...params, ...isInvalid }
            Validate.onErrorMessageAgeField(validateParams)
        }
        PrimaryInformationFirstPageController._selectionTypeSelected = null
    }

    public onSelectItems = (item?: any[]): void => {
        const selectedItems = item?.map(e => {
            return {
                key: e.key,
                value: e.label
            }
        })

        if (PrimaryInformationFirstPageController._selectionTypeSelected == SelectionType.HavdSogerHan) {
            CreateEditProfileController.viewModel?.setHavdSogerHanList(selectedItems)
            PrimaryInformationFirstPageController._havdSogerHanSelectedFirstIndex = PrimaryInformationFirstPageController._havdSogerHanContentList?.findIndex((e) => {
                if (!(selectedItems?.length > 0)) return true
                return e.key === selectedItems[0].key
            })
            PrimaryInformationFirstPageController._havdSogerHanSelectedFirstIndex = PrimaryInformationFirstPageController._havdSogerHanSelectedFirstIndex < 0 ?
                0 : PrimaryInformationFirstPageController._havdSogerHanSelectedFirstIndex
            PrimaryInformationFirstPageController._havdSogerHanListSelected = item
        } else if (PrimaryInformationFirstPageController._selectionTypeSelected == SelectionType.Interest) {
            CreateEditProfileController.viewModel?.setInterestList(selectedItems)
            PrimaryInformationFirstPageController._interestSelectedFirstIndex = PrimaryInformationFirstPageController._interestContentList?.findIndex((e) => {
                if (!(selectedItems?.length > 0)) return true
                return e.key === selectedItems[0].key
            })
            PrimaryInformationFirstPageController._interestSelectedFirstIndex = PrimaryInformationFirstPageController._interestSelectedFirstIndex < 0 ?
                0 : PrimaryInformationFirstPageController._interestSelectedFirstIndex
            PrimaryInformationFirstPageController._interestListSelected = item
        }
        this.onCloseModal()
    }

    public onCloseModal = (): void => {
        const params = {
            localViewModel: CreateEditProfileController.viewModel as any,
            rootStore: this._rootStore
        }
        PrimaryInformationFirstPageController.viewModel?.setIsModalShowing(false)
        if (PrimaryInformationFirstPageController._selectionTypeSelected == SelectionType.HavdSogerHan) {
            const isInvalid = { isInvalid: !(CreateEditProfileController.viewModel?.getHavdSogerHanList?.length > 0) }
            const validateParams = { ...params, ...isInvalid }
            Validate.onErrorMessageHavdSogerHanField(validateParams)
        } else if (PrimaryInformationFirstPageController._selectionTypeSelected == SelectionType.Interest) {
            const isInvalid = { isInvalid: !(CreateEditProfileController.viewModel?.getInterestList?.length > 0) }
            const validateParams = { ...params, ...isInvalid }
            Validate.onErrorMessageInterestField(validateParams)
        }
        PrimaryInformationFirstPageController._selectionTypeSelected = null
    }

    public onFullNameChangeText = (value: string): void => {
        const params = {
            value: value,
            localViewModel: CreateEditProfileController.viewModel as any,
        }
        Validate.onFullNameChangeText(params)
    }

    public onBlurFullNameTextField = (): void => {
        const fullName = CreateEditProfileController.viewModel?.getFullName
        const params = {
            value: fullName,
            localViewModel: CreateEditProfileController.viewModel as any,
            rootStore: this._rootStore
        }
        Validate.onBlurFullNameTextField(params)
    }

    public onTouchAgeTextField = () => {
        Keyboard.dismiss()
        PrimaryInformationFirstPageController._selectionTypeSelected = SelectionType.AGE
        PrimaryInformationFirstPageController.viewModel?.setIsModalShowing(true)
    }

    public onTouchCityTextField = () => {
        Keyboard.dismiss()
        PrimaryInformationFirstPageController._selectionTypeSelected = SelectionType.CITY
        PrimaryInformationFirstPageController.viewModel?.setIsModalShowing(true)
    }

    public onTouchHavdSogerHanTextField = () => {
        Keyboard.dismiss()
        PrimaryInformationFirstPageController._selectionTypeSelected = SelectionType.HavdSogerHan
        PrimaryInformationFirstPageController.viewModel?.setIsModalShowing(true)
    }

    public onTouchInterestTextField = () => {
        Keyboard.dismiss()
        PrimaryInformationFirstPageController._selectionTypeSelected = SelectionType.Interest
        PrimaryInformationFirstPageController.viewModel?.setIsModalShowing(true)
    }

    public onPopoverMenuPressed = () => {
        const { getIsPopoverMenuVisible } = PrimaryInformationFirstPageController.viewModel
        PrimaryInformationFirstPageController.viewModel?.setIsPopoverMenuVisible(!getIsPopoverMenuVisible)
    }

    public onTogglePopoverMenu = () => {
        const { getIsPopoverMenuVisible } = PrimaryInformationFirstPageController.viewModel
        PrimaryInformationFirstPageController.viewModel?.setIsPopoverMenuVisible(!getIsPopoverMenuVisible)
    }

    public onSaveAndContinueButtonDidTouch = () => {
        if (!CreateEditProfileController.viewModel?.getIsFirstPageValid) {
            const params: IAlertParams = {
                title: CreateEditProfileController.resourcesViewModel?.getResourceAlertValidateUserTitle(),
                message: CreateEditProfileController.resourcesViewModel?.getResourceAlertValidateUserMessage()
            }
            this._generalAlertOS(params)
        } else {
            this._myProps?.navigation?.dispatch(CommonActions.navigate(NavigationKey.CreateEditProfileSecond) as any)
        }
    }

    public forceClosePopoverMenu = () => PrimaryInformationFirstPageController.viewModel?.setIsPopoverMenuVisible(false)

    public onPopoverMenuItemPressed = (type?: PopoverMenuActionType) => {
        PrimaryInformationFirstPageController._popoverMenuActionTypeSelected = type
        this.forceClosePopoverMenu()

        /* Work arround for setting timeoout because `onPopoverClosed` is not working*/
        this._delayExecutor(() => this._onOpenAlertModal())
    }

    /* not working, will solve later */
    public onPopoverClosed = () => {
        // onOpenAlertModal()
    }

    public onAlertCancel = () => {
        PrimaryInformationFirstPageController._popoverMenuActionTypeSelected = null
        this._onCloseAlertModal()
    }


    public onConfirmAlertModalPress = async () => {
        this._onCloseAlertModal()
    }

    public onAlertModalClosed = async () => {
        if (!PrimaryInformationFirstPageController._popoverMenuActionTypeSelected) return
        const dudeId = CreateEditProfileController.viewModel?.getUserId
        if (PrimaryInformationFirstPageController._popoverMenuActionTypeSelected !== PopoverMenuActionType.Unknown) {
            if (PrimaryInformationFirstPageController._popoverMenuActionTypeSelected === PopoverMenuActionType.Delete) {
                this._isGlobalLoading(true)
                await PrimaryInformationFirstPageController.viewModel?.deleteDude(this._rootStore, dudeId)
                this._isGlobalLoading(false)
                this._myProps?.navigation.goBack()
            } else {
                const status = PrimaryInformationFirstPageController._popoverMenuActionTypeSelected
                if (status) {
                    this._isGlobalLoading(true)
                    const result = await PrimaryInformationFirstPageController.viewModel?.changeDudeStatus(this._rootStore, dudeId, status)
                    this._isGlobalLoading(false)
                    const data = result?.data
                    if (data) {
                        CreateEditProfileController.viewModel?.setStatus(data?.status)
                    }
                }
            }
        }
        PrimaryInformationFirstPageController._popoverMenuActionTypeSelected = null
    }

    public threeDotsMenuOptions: ThreeDotMenuProps = {
        onPopoverMenuPress: this.onTogglePopoverMenu,
        forceClosePopoverMenu: this.forceClosePopoverMenu,
        onPopoverMenuItemPressed: this.onPopoverMenuItemPressed,
        onPopoverClosed: this.onPopoverClosed
    }

    public getConfirmAlertModalOptions = (): ConfirmModalProps => {
        let options = {
            okButtonText: PrimaryInformationFirstPageController.resourcesViewModel?.getResourceModalButtonOKTitle(),
            clearButtonText: PrimaryInformationFirstPageController.resourcesViewModel?.getResourceModalButtonCancelTitle(),
            isVisible: PrimaryInformationFirstPageController.viewModel?.getIsAlertModalShowing || false,
            onCancel: this.onAlertCancel,
            onOK: this.onConfirmAlertModalPress,
            onModalClosed: this.onAlertModalClosed
        }
        if (PrimaryInformationFirstPageController._popoverMenuActionTypeSelected === PopoverMenuActionType.Delete) {
            return {
                ...options,
                message: PrimaryInformationFirstPageController.resourcesViewModel?.getResourceModalConfirmDeleteDude(),
            }
        } else {
            return {
                ...options,
                message: PrimaryInformationFirstPageController.resourcesViewModel?.getResourceModalConfirmChangeStatusDude(),
            }
        }
    }


    /*
       Mark Life cycle
    */

    //@override
    viewWillDisappear = () => {
        super.viewWillDisappear && super.viewWillDisappear()
        this.forceClosePopoverMenu()
    }

}

export default PrimaryInformationFirstPageController