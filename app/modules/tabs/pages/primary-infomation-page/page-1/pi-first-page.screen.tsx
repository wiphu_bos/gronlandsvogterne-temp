// MARK: Import

import React, { useRef } from "react"
import { observer } from "mobx-react-lite"
import { PrimaryInformationProps } from "./pi-first-page.props"
import LinearGradient from "react-native-linear-gradient"
import FloatLabelTextInput from '../../../../../libs/float-label-textfield/float-label-text-field'
import { useConfigurate } from "../../../../../custom-hooks/use-configure-controller"
import { View } from "react-native-animatable"
import { KeyboardAvoidingView } from "react-native"
import * as metric from "../../../../../theme"
import { AnimatedScrollView, ConfirmModal, StatusComponent, UserTagsComponent, ThreeDotsMenuComponent } from "../../../../../components"
import PrimaryInformationFirstPageController from "./pi-first-page.controllers"
import ModalFilterPicker from '../../../../../libs/modal-filter-picker/modal-filter-picker'
import { INavigationRoute } from "../../../../../models/navigation-store/navigation.types"
import { GradientButton } from "../../../../../components/gradient-button/button"

// MARK: Style Import

import * as Styles from "./pi-first-page.styles"
import CreateEditProfileController from "../../../create-edit-profile-tab/create-edit-profile.controllers"

export const PrimaryInformationFirstScreen: React.FunctionComponent<PrimaryInformationProps & Partial<INavigationRoute>> = observer((props) => {

    const controller = useConfigurate(PrimaryInformationFirstPageController, props) as PrimaryInformationFirstPageController

    const ageTextInput = useRef(null)
    const cityTextInput = useRef(null)
    const lookingForTextInput = useRef(null)
    const interestTextInput = useRef(null)

    // MARK: Render
    return (
        <LinearGradient shouldRasterizeIOS style={Styles.CONTAINNER} {...Styles.TAB_PAGE_BACKGROUND}>
            <View shouldRasterizeIOS style={Styles.BODY_CONTAINER}>
                <KeyboardAvoidingView behavior={metric.keyboardBehavior} style={Styles.AVOID_KEYBOARD_VIEW}>
                    <AnimatedScrollView
                        {...metric.solidScrollView}
                        contentContainerStyle={Styles.SCROLL_VIEW}>
                        <View shouldRasterizeIOS style={Styles.CONTENT_CONTAINER}>
                            <View shouldRasterizeIOS style={Styles.HEADER}>
                                <StatusComponent
                                    testID={PrimaryInformationFirstPageController.resourcesViewModel?.getTestIDStatusTitle()}
                                    statusText={PrimaryInformationFirstPageController.resourcesViewModel?.getResourceStatusTitle()}
                                    statusValue={
                                        PrimaryInformationFirstPageController.resourcesViewModel?.getResourceUserStatusTitle(CreateEditProfileController?.viewModel?.getStatus)
                                    } />
                                <ThreeDotsMenuComponent
                                    publishText={PrimaryInformationFirstPageController.resourcesViewModel?.getResourceButtonActivateTitle()}
                                    unPublishText={PrimaryInformationFirstPageController.resourcesViewModel?.getResourceButtonDeactivateTitle()}
                                    buttonDeleteText={PrimaryInformationFirstPageController.resourcesViewModel?.getResourceButtonDeleteTitle()}
                                    testID={PrimaryInformationFirstPageController.resourcesViewModel?.getTestIDButtonThreeDotsMenu()}
                                    rootViewModel={CreateEditProfileController.viewModel}
                                    observeViewModel={PrimaryInformationFirstPageController.viewModel}
                                    {...controller.threeDotsMenuOptions} />
                            </View>
                            <View shouldRasterizeIOS style={Styles.TEXT_FIELD_CONTAINER}>
                                <FloatLabelTextInput
                                    {...PrimaryInformationFirstPageController.resourcesViewModel?.getTestIDFullNamePlaceholder()}
                                    placeholder={PrimaryInformationFirstPageController.resourcesViewModel?.getResourceFullNamePlaceholder()}
                                    onChangeTextValue={controller.onFullNameChangeText}
                                    value={CreateEditProfileController.viewModel?.getFullName}
                                    noBorder
                                    returnKeyType="next"
                                    onSubmitEditing={() => { ageTextInput.current.focus() }}
                                    errorMessage={CreateEditProfileController.viewModel?.getFullNameErrorMessage}
                                    testIDErrorMessage={PrimaryInformationFirstPageController.resourcesViewModel?.getTestIDErrorMessage()}
                                    onBlur={controller.onBlurFullNameTextField}
                                    containerStyle={Styles.TEXT_FIELD_ITEM}
                                />
                                <FloatLabelTextInput
                                    editable={CreateEditProfileController.viewModel?.getEmail ? false : true}
                                    disabled={CreateEditProfileController.viewModel?.getEmail ? true : false}
                                    returnKeyType="next"
                                    keyboardType="email-address"
                                    {...PrimaryInformationFirstPageController.resourcesViewModel?.getTestIDEmailPlaceholder()}
                                    placeholder={PrimaryInformationFirstPageController.resourcesViewModel?.getResourceEmailPlaceholder()}
                                    value={CreateEditProfileController.viewModel?.getEmail}
                                    noBorder
                                    containerStyle={Styles.TEXT_FIELD_ITEM}
                                />

                                <FloatLabelTextInput
                                    ref={ageTextInput}
                                    {...PrimaryInformationFirstPageController.resourcesViewModel?.getTestIDDropdownAge()}
                                    editable={false}
                                    value={CreateEditProfileController.viewModel?.getAge?.value || (PrimaryInformationFirstPageController.resourcesViewModel?.getResourceAgePlaceholder())}
                                    noBorder
                                    isDropDown
                                    onTouchTextFieldXL={controller.onTouchAgeTextField}
                                    errorMessage={CreateEditProfileController.viewModel?.getAgeErrorMessage}
                                    testIDErrorMessage={PrimaryInformationFirstPageController.resourcesViewModel?.getTestIDErrorMessage()}
                                    returnKeyType="next"
                                    onSubmitEditing={() => { cityTextInput.current.focus() }}
                                    containerStyle={Styles.TEXT_FIELD_ITEM}
                                />
                                <FloatLabelTextInput
                                    ref={cityTextInput}
                                    isDropDown
                                    {...PrimaryInformationFirstPageController.resourcesViewModel?.getTestIDDropdownCity()}
                                    editable={false}
                                    value={CreateEditProfileController.viewModel?.getCity?.value || (PrimaryInformationFirstPageController.resourcesViewModel?.getResourceCityPlaceholder())}
                                    noBorder
                                    onTouchTextFieldXL={controller.onTouchCityTextField}
                                    errorMessage={CreateEditProfileController.viewModel?.getCityErrorMessage}
                                    testIDErrorMessage={PrimaryInformationFirstPageController.resourcesViewModel?.getTestIDErrorMessage()}
                                    containerStyle={Styles.TEXT_FIELD_ITEM}
                                />
                                <FloatLabelTextInput
                                    ref={lookingForTextInput}
                                    {...PrimaryInformationFirstPageController.resourcesViewModel?.getTestIDPickerHavdSogerHan()}
                                    editable={false}
                                    value={PrimaryInformationFirstPageController.resourcesViewModel?.getResourceHavdSogerHanPlaceholder()}
                                    noBorder
                                    isMultipleSelectionDropDown
                                    onTouchTextFieldXL={controller.onTouchHavdSogerHanTextField}
                                    multipleSelectionItems={controller.getMultitpleSelectionDataType(CreateEditProfileController.viewModel?.getHavdSogerHanList)}
                                    testIDMultipleSelectionItems=""
                                    errorMessage={CreateEditProfileController.viewModel?.getHavdSogerHanListErrorMessage}
                                    testIDErrorMessage={PrimaryInformationFirstPageController.resourcesViewModel?.getTestIDErrorMessage()}
                                    containerStyle={Styles.TEXT_FIELD_ITEM}
                                />
                                <FloatLabelTextInput
                                    ref={interestTextInput}
                                    {...PrimaryInformationFirstPageController.resourcesViewModel?.getTestIDPickerInterest()}
                                    editable={false}
                                    value={PrimaryInformationFirstPageController.resourcesViewModel?.getResourceInterestPlaceholder()}
                                    noBorder
                                    isMultipleSelectionDropDown
                                    onTouchTextFieldXL={controller.onTouchInterestTextField}
                                    multipleSelectionItems={controller.getMultitpleSelectionDataType(CreateEditProfileController.viewModel?.getInterestList)}
                                    testIDMultipleSelectionItems=""
                                    errorMessage={CreateEditProfileController.viewModel?.getInterestListErrorMessage}
                                    testIDErrorMessage={PrimaryInformationFirstPageController.resourcesViewModel?.getTestIDErrorMessage()}
                                    containerStyle={Styles.TEXT_FIELD_ITEM}
                                />
                            </View>
                            <UserTagsComponent
                                observeViewModel={CreateEditProfileController.viewModel}
                                tagTitle={PrimaryInformationFirstPageController.resourcesViewModel?.getResourceTagTitle()}
                                onTagPressed={controller.onTagPressed}
                                tagList={PrimaryInformationFirstPageController.resourcesViewModel?.getTags} />
                            <GradientButton
                                {...PrimaryInformationFirstPageController.resourcesViewModel?.getTestIDButtonSaveAndContinue()}
                                text={PrimaryInformationFirstPageController.resourcesViewModel?.getResourceButtonSaveAndContinueTitle()}
                                containerStyle={Styles.BUTTON_MARGIN}
                                onPress={controller.onSaveAndContinueButtonDidTouch} />
                        </View>
                    </AnimatedScrollView>
                </KeyboardAvoidingView>
            </View>
            <ModalFilterPicker
                {...PrimaryInformationFirstPageController.resourcesViewModel?.getTestIDModalPicker()}
                cancelButtonText={PrimaryInformationFirstPageController.resourcesViewModel?.getResourceModalButtonCancelTitle()}
                okButtonText={PrimaryInformationFirstPageController.resourcesViewModel?.getResourceModalButtonOKTitle()}
                clearButtonText={PrimaryInformationFirstPageController.resourcesViewModel?.getResourceModalButtonClearTitle()}
                visible={PrimaryInformationFirstPageController.viewModel?.getIsModalShowing || false}
                {...controller.getSelectOptions()}
                iconSelectionStyle={Styles.ICON_SELECTION}
            />
            <ConfirmModal
                // {...resourcesViewModel?.getTestIDConfirmModal()}
                {...controller.getConfirmAlertModalOptions()}
            />
        </LinearGradient>
    )
})
