import { ViewStyle, TextStyle, ImageStyle } from "react-native"
import { color } from "../../../../../theme"
import * as metric from "../../../../../theme"
import { RFValue } from "react-native-responsive-fontsize"
export const FULL: ViewStyle = {
    flex: 1
}

export const TAB_PAGE_BACKGROUND: any = { colors: color.tabPageBackground }

export const CONTAINNER: ViewStyle = {
    ...FULL
}

export const SCROLL_VIEW: ViewStyle = {
    paddingBottom: metric.ratioHeight(10),
    flexGrow: 1,
    flexDirection: 'column',
    justifyContent: 'space-between'
}

export const CONTENT_CONTAINER: ViewStyle = {
    marginHorizontal: metric.ratioWidth(32),
    marginTop: metric.ratioHeight(31)
}

export const HEADER: ViewStyle = {
    ...FULL,
    flexDirection: 'row',
}

export const ADDITIONAL_MARGIN_TOP: ViewStyle = {
    marginTop: metric.ratioHeight(35)
}

export const TEXT: TextStyle = {
    color: color.palette.white,
    fontFamily: "Montserrat",
}
export const TITLE: TextStyle = {
    ...TEXT,
    fontSize: RFValue(16),
    fontWeight: '400',
    //Android
    fontFamily: "Montserrat-Regular",
}

export const ICON_SELECTION: ImageStyle = {
    marginLeft: metric.ratioWidth(18),
    width: metric.ratioWidth(21),
    height: metric.ratioWidth(21),
    marginRight: metric.ratioWidth(28),
}
export const BUTTON_DOTTED: ViewStyle = {
    borderStyle: "dashed",
    borderWidth: metric.ratioWidth(1),
    borderColor: color.palette.white,
    borderRadius: metric.ratioWidth(2),
}

export const BUTTON_DOTTED_TEXT: TextStyle = {
    ...TEXT,
    fontSize: RFValue(16),
    fontWeight: '400',
    //Android
    fontFamily: "Montserrat-Regular",
    marginLeft: metric.ratioWidth(10)
}


export const IMAGE_CONTAINER: ViewStyle = {
    ...FULL,
    marginTop: metric.ratioHeight(23),
    minHeight: metric.ratioHeight(135)
}

export const GRID: ViewStyle = {
    ...FULL,
    marginTop: metric.ratioHeight(10),
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'flex-start',
}

export const GRID_IMAGE: ViewStyle = {
    // marginHorizontal: metric.ratioWidth(2),
    marginVertical: metric.ratioHeight(2),
    width: '33.33%',
    alignItems: 'center'
}


export const BUTTON_MARGIN: ViewStyle = {
    alignSelf: 'center',
    marginBottom: metric.ratioHeight(40)
}
