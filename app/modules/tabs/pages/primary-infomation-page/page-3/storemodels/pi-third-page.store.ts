import { types } from "mobx-state-tree"
import { StoreName } from "../../../../../../constants/app.constant"
import { PrimaryInformationThirdPropsModel } from "../viewmodels/pi-third-page.models"
import { PrimaryInformationFirstActions } from "../viewmodels/pi-third-page.actions"
import { PrimaryInformationThirdViews, PrimaryInformationThirdResourcesViews } from "../viewmodels/pi-third-page.views"
import { GeneralResourcesStoreModel } from "../../../../../../models/general-resources-store"

const PrimaryInformationThirdModel = types.model(StoreName.PrimaryInformationThird, PrimaryInformationThirdPropsModel)
export const PrimaryInformationThirdStoreModel = types.compose(PrimaryInformationThirdModel,PrimaryInformationThirdViews,PrimaryInformationFirstActions)
export const PrimaryInformationThirdResourcesStoreModel = types.compose(GeneralResourcesStoreModel, PrimaryInformationThirdResourcesViews)

