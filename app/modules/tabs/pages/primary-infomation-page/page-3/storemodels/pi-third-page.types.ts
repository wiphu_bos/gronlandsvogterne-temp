import { Instance, SnapshotOut } from "mobx-state-tree"
import { PrimaryInformationThirdStoreModel, PrimaryInformationThirdResourcesStoreModel } from "./pi-third-page.store"

export type PrimaryInformationThirdStore = Instance<typeof PrimaryInformationThirdStoreModel>
export type PrimaryInformationThirdStoreSnapshot = SnapshotOut<typeof PrimaryInformationThirdStoreModel>

export type PrimaryInformationThirdResourcesStore = Instance<typeof PrimaryInformationThirdResourcesStoreModel>
export type PrimaryInformationThirdResourcesStoreSnapshot = SnapshotOut<typeof PrimaryInformationThirdResourcesStoreModel>