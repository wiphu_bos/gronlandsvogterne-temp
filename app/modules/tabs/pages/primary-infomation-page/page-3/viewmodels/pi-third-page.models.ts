import { types } from "mobx-state-tree"

export const PrimaryInformationThirdPropsModel = {
    isPopoverMenuVisible: types.optional(types.boolean,false),
    isModalShowing: types.optional(types.boolean,false)
} 