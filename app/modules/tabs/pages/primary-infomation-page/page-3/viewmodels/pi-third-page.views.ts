import { types } from "mobx-state-tree"
import { PrimaryInformationThirdPropsModel } from "./pi-third-page.models"
import { GeneralResources } from "../../../../../../constants/firebase/remote-config"
import { TestIDResources } from "../../../../../../constants/test-key/test.constant"
import * as Utils from "../../../../../../utils"
import { GeneralResourcesStoreModel } from "../../../../../../models/general-resources-store"

export const PrimaryInformationThirdViews = types.model(PrimaryInformationThirdPropsModel)
    .views(self => ({
        get getIsPopoverMenuVisible() {
            return self.isPopoverMenuVisible
        },
        get getIsModalShowing() {
            return self.isModalShowing
        }
    }))

export const PrimaryInformationThirdResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State

    const { PrimaryInformationThirdScreen } = GeneralResources
    const {
        uploadProfilePhotoTitle,
        updatePhotoToGalleryTitle,
        selectPhoto,
        buttonSaveAndContinueTitle,
        alertDeleteProfileImageTitle,
        alertDeleteProfileImageMessage,
        alertDeleteGalleryImageTitle,
        alertDeleteGalleryImageMessage,
        grantPermissionTitle,
        grantPermission
    } = PrimaryInformationThirdScreen

    //MARK: Views
    const getShareResources = (key: string) => self.getValues(key, true)
    const getResourceUploadProfilePhoto = () => getShareResources(uploadProfilePhotoTitle)
    const getResourceUploadPhotoGallery = () => getShareResources(updatePhotoToGalleryTitle)
    const getResourceSelectPhoto = () => getShareResources(selectPhoto)
    const getResourceButtonSaveAndContinueTitle = () => getShareResources(buttonSaveAndContinueTitle)
    const getResourceAlertDeleteProfileImageTitle = () => getShareResources(alertDeleteProfileImageTitle)
    const getResourceAlertDeleteProfileImageMessage = () => getShareResources(alertDeleteProfileImageMessage)
    const getResourceAlertDeleteGalleryImageTitle = () => getShareResources(alertDeleteGalleryImageTitle)
    const getResourceAlertDeleteGalleryImageMessage = () => getShareResources(alertDeleteGalleryImageMessage)
    const getResourceErrorPermisionTitle = () => getShareResources(grantPermissionTitle)
    const getResourceErrorPermision = () => getShareResources(grantPermission)
    return {
        getResourceUploadProfilePhoto,
        getResourceUploadPhotoGallery,
        getResourceSelectPhoto,
        getResourceButtonSaveAndContinueTitle,
        getResourceAlertDeleteProfileImageTitle,
        getResourceAlertDeleteProfileImageMessage,
        getResourceAlertDeleteGalleryImageTitle,
        getResourceAlertDeleteGalleryImageMessage,
        getResourceErrorPermisionTitle,
        getResourceErrorPermision
    }
})
    .views(self => {

        //MARK: Volatile State

        //MARK: Views

        const getTestIDUploadProfilePhoto = () => Utils.getTestIDObject(TestIDResources.PrimaryInformationThirdScreen.uploadProfilePhoto)
        const getTestIDUploadPhotoGallerySection = () => Utils.getTestIDObject(TestIDResources.PrimaryInformationThirdScreen.uploadPhotoGallerySection)
        const getTestIDButtonSelectPhoto = () => Utils.getTestIDObject(TestIDResources.PrimaryInformationThirdScreen.buttonSelectPhoto)
        const getTestIDDeleteProfilePhoto = () => Utils.getTestIDObject(TestIDResources.PrimaryInformationThirdScreen.buttonDeleteProfilePhoto)
        const getTestIDButtonSelectPhotoInGallery = () => Utils.getTestIDObject(TestIDResources.PrimaryInformationThirdScreen.buttonSelectPhotoInGallery)
        const getTestIDButtonSaveAndContinue = () => Utils.getTestIDObject(TestIDResources.PrimaryInformationFirstScreen.buttonSaveAndContinue)
        const getTestIDProfileImage = () => Utils.getTestIDObject(TestIDResources.PrimaryInformationThirdScreen.imageProfile)
        return {
            getTestIDUploadProfilePhoto,
            getTestIDUploadPhotoGallerySection,
            getTestIDButtonSelectPhoto,
            getTestIDButtonSelectPhotoInGallery,
            getTestIDButtonSaveAndContinue,
            getTestIDDeleteProfilePhoto,
            getTestIDProfileImage,
        }
    })
