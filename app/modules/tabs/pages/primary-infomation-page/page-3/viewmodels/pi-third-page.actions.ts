import { types } from "mobx-state-tree"
import { PrimaryInformationThirdPropsModel } from "./pi-third-page.models"

export const PrimaryInformationFirstActions = types.model(PrimaryInformationThirdPropsModel).actions(self => {

    //MARK: Volatile State

    //MARK: Actions
    const setIsPopoverMenuVisible = (value: boolean) => self.isPopoverMenuVisible = value
    const setIsModalShowing = (value: boolean) => self.isModalShowing = value
    return {
        setIsPopoverMenuVisible,
        setIsModalShowing
    }
})