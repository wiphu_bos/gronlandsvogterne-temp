import { NavigationContainerProps } from "react-navigation"
import { ParamListBase } from "@react-navigation/native"
import { CreateEditProfileStore } from "../../../create-edit-profile-tab/storemodels"
import { PrimaryInformationThirdStore } from "./storemodels/pi-third-page.types"
import { UserStore } from "../../../../../models/user-store"

type PrimaryInformationStore =
    | PrimaryInformationThirdStore

export interface PrimaryInformationProps extends NavigationContainerProps<ParamListBase> {
    dudeProfileObject?: UserStore
    rootViewModel?: CreateEditProfileStore
    viewModel?: PrimaryInformationStore
}