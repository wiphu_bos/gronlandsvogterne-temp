// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { PrimaryInformationProps } from "./pi-third-page.props"
import LinearGradient from "react-native-linear-gradient"
import { useConfigurate } from "../../../../../custom-hooks/use-configure-controller"
import { View } from "react-native-animatable"
import * as metric from "../../../../../theme"
import PrimaryInformationThirdController from "./pi-third-page.controllers"
import { INavigationRoute } from "../../../../../models/navigation-store/navigation.types"
import { Text } from "../../../../../components/text/text"
import { images } from "../../../../../theme/images"
import { SquareImage } from "../../../../../components/square-image/square-image"
import { IconButton } from "../../../../../components/icon-button/icon-button"
import { GradientButton } from "../../../../../components/gradient-button/button"
import { AnimatedScrollView } from "../../../../../components"
// MARK: Style Import
import * as Styles from "./pi-third-page.styles"
import CreateEditProfileController from "../../../create-edit-profile-tab/create-edit-profile.controllers"


export const PrimaryInformationThirdScreen: React.FunctionComponent<PrimaryInformationProps & Partial<INavigationRoute>> = observer((props) => {

    const controller = useConfigurate(PrimaryInformationThirdController, props) as PrimaryInformationThirdController

    // MARK: Render
    return (
        <LinearGradient shouldRasterizeIOS style={Styles.CONTAINNER} {...Styles.TAB_PAGE_BACKGROUND}>
            <AnimatedScrollView
                contentContainerStyle={Styles.SCROLL_VIEW}
                {...metric.solidScrollView}>
                <View shouldRasterizeIOS style={Styles.CONTENT_CONTAINER}>
                    <View shouldRasterizeIOS style={Styles.HEADER}>
                        <Text style={Styles.TITLE} text={PrimaryInformationThirdController.resourcesViewModel?.getResourceUploadProfilePhoto()} />
                    </View>
                    <View shouldRasterizeIOS style={Styles.IMAGE_CONTAINER}>
                        {(!CreateEditProfileController.viewModel?.getProfileImage || CreateEditProfileController.viewModel?.getProfileImage?.length == 0 || !CreateEditProfileController.viewModel?.getProfileImage[0].file_path) ?
                            <IconButton
                                {...PrimaryInformationThirdController.resourcesViewModel?.getTestIDButtonSelectPhoto()}
                                text={PrimaryInformationThirdController.resourcesViewModel?.getResourceSelectPhoto()}
                                textStyle={Styles.BUTTON_DOTTED_TEXT}
                                icon={images.addWhite} style={Styles.BUTTON_DOTTED}
                                onPress={controller.onSelectPhotoButtonDidTouch} /> : null}
                        <View shouldRasterizeIOS style={Styles.GRID}>
                            {(CreateEditProfileController.viewModel?.getProfileImage?.length > 0) ?
                                <View shouldRasterizeIOS style={Styles.GRID_IMAGE}>
                                    <SquareImage {...PrimaryInformationThirdController.resourcesViewModel?.getTestIDProfileImage()}
                                        deleteTestID={PrimaryInformationThirdController.resourcesViewModel?.getTestIDDeleteProfilePhoto()}
                                        imageSource={CreateEditProfileController.viewModel?.getProfileImage[0].file_path} showDelete={true}
                                        onDelete={controller.onRemoveProfileButtonDidTouch}
                                        index={0} />
                                </View>
                                : null}
                        </View>
                    </View>
                    <View shouldRasterizeIOS style={{ ...Styles.HEADER, ...Styles.ADDITIONAL_MARGIN_TOP }}>
                        <Text style={Styles.TITLE} preset="fieldLabel" text={PrimaryInformationThirdController.resourcesViewModel?.getResourceUploadPhotoGallery()} />
                    </View>
                    <View shouldRasterizeIOS style={Styles.IMAGE_CONTAINER}>
                        {(!CreateEditProfileController.viewModel?.getGalleryImages || CreateEditProfileController.viewModel?.getGalleryImages?.length < 6) ?
                            <IconButton
                                {...PrimaryInformationThirdController.resourcesViewModel?.getTestIDButtonSelectPhotoInGallery()}
                                text={PrimaryInformationThirdController.resourcesViewModel?.getResourceSelectPhoto()}
                                textStyle={Styles.BUTTON_DOTTED_TEXT}
                                icon={images.addWhite} style={Styles.BUTTON_DOTTED}
                                onPress={controller.onSelectPhotoToGalleryButtonDidTouch} /> : null}
                        <View shouldRasterizeIOS style={Styles.GRID}>
                            {(CreateEditProfileController.viewModel?.getGalleryImages) ?
                                CreateEditProfileController.viewModel?.getGalleryImages?.map((item, index) =>
                                    <View shouldRasterizeIOS style={Styles.GRID_IMAGE}>
                                        <SquareImage
                                            imageSource={item.file_path}
                                            showDelete={true}
                                            onDelete={() => controller.onRemoveGalleryButtonDidTouch(index)}
                                            index={index} />
                                    </View>
                                ) : null}
                        </View>
                    </View>
                </View>
            </AnimatedScrollView>
            <GradientButton
                {...PrimaryInformationThirdController.resourcesViewModel?.getTestIDButtonSaveAndContinue()}
                text={PrimaryInformationThirdController.resourcesViewModel?.getResourceButtonSaveAndContinueTitle()}
                containerStyle={Styles.BUTTON_MARGIN}
                onPress={controller.onSaveAndContinueButtonDidTouch} />
        </LinearGradient>
    )
})
