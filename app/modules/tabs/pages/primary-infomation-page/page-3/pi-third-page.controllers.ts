// MARK: Import
import { NavigationKey, FileUploadType } from "../../../../../constants/app.constant"
import { RootStore, INavigationRoute } from "../../../../../models"
import ImagePicker, { Image } from 'react-native-image-crop-picker'
import { Storage } from "../../../../../constants/firebase/fire-storage.constant"
import { IUploadFileData } from "../../../../../utils/firebase/fire-storage/fire-storage.types"
import * as Validate from "../../../../../utils/local-validate"
import { BaseController, IAlertParams } from "../../../../base.controller"
import { PrimaryInformationThirdStore, PrimaryInformationThirdResourcesStore, PrimaryInformationThirdResourcesStoreModel, PrimaryInformationThirdStoreModel } from "./storemodels"
import { onSnapshot } from "mobx-state-tree"
import { PrimaryInformationProps } from "./pi-third-page.props"
import CreateEditProfileController from "../../../create-edit-profile-tab/create-edit-profile.controllers"
import { CommonActions } from "@react-navigation/native"

class PrimaryInformationThirdController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static resourcesViewModel: PrimaryInformationThirdResourcesStore
    public static viewModel: PrimaryInformationThirdStore

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, props?: PrimaryInformationProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, props, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(props)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        PrimaryInformationThirdController.resourcesViewModel = PrimaryInformationThirdResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.CreateEditProfileThird)
        PrimaryInformationThirdController.viewModel = localStore && PrimaryInformationThirdStoreModel.create({ ...localStore }) || PrimaryInformationThirdStoreModel.create({})
    }

    private _setupSnapShot = () => {
        onSnapshot(PrimaryInformationThirdController.viewModel, (snap: PrimaryInformationThirdStore) => {
            this._setInitializedToPropsParams()
        })
    }

    /*
       Mark Data
    */

    /*
       Mark Event
    */

    private _pickImage = (circular: boolean, callBack: (image: Image) => void) => {
        ImagePicker.openPicker({
            width: 1024,
            height: 1024,
            cropping: true,
            cropperCircleOverlay: circular,
            sortOrder: 'none',
            compressImageMaxWidth: 1024,
            compressImageMaxHeight: 1024,
            compressImageQuality: 1,
            includeExif: true,
        }).then((image: Image) => {
            callBack(image)
        }).catch(e => {
            if (e.code == 'E_PERMISSION_MISSING') {
                const params: IAlertParams = {
                    title: PrimaryInformationThirdController.resourcesViewModel.getResourceErrorPermisionTitle(),
                    message: PrimaryInformationThirdController.resourcesViewModel.getResourceErrorPermision(),
                    delay: 0
                }
                this._generalAlertOS(params)
            }
        })
    }

    public onSelectPhotoButtonDidTouch = () => {
        this._pickImage(true, (image: Image) => {
            const { path, filename, ...filteredData } = image
            const profileImageParams: IUploadFileData = {
                type: FileUploadType.ProfileImage,
                storage_path: Storage.ProfileImage,
                file_path: path,
                file_name: filename
            }
            CreateEditProfileController.viewModel?.setProfileImage([{ ...filteredData, ...profileImageParams }])
        })
    }

    public onSelectPhotoToGalleryButtonDidTouch = () => {
        this._pickImage(false, (image: Image) => {
            const galleries = CreateEditProfileController.viewModel?.getGalleryImages?.slice() || []
            const { path, filename, ...filteredData } = image
            const galleryImageParams: IUploadFileData = {
                type: FileUploadType.GalleryImage,
                storage_path: Storage.GalleryImage,
                file_path: path,
                file_name: filename
            }
            galleries.push({ ...filteredData, ...galleryImageParams })
            CreateEditProfileController.viewModel?.setGalleryImages(galleries)
        })
        //pickImage(false, rootViewModel.getGalleryImages)
    }

    public onRemoveProfileButtonDidTouch = () => {
        const params: IAlertParams = {
            title: PrimaryInformationThirdController.resourcesViewModel.getResourceAlertDeleteProfileImageTitle(),
            message: PrimaryInformationThirdController.resourcesViewModel.getResourceAlertDeleteProfileImageMessage(),
            onOKPress: () => this._onConfirmRemoveProfileImage(),
            onCancelPress: () => console.log('Cancel Pressed'),
            delay: 0
        }
        this._confirmationAlertOS(params)
    }

    private _onConfirmRemoveProfileImage = () => {
        CreateEditProfileController.viewModel?.setProfileImageToRemove()
        CreateEditProfileController.viewModel?.setProfileImage(null)
        const params = {
            isEmpty: true,
            localViewModel: CreateEditProfileController.viewModel as any,
            rootStore: this._rootStore
        }
        Validate.onErrorMessageProfileImageSelection(params)
    }

    public onRemoveGalleryButtonDidTouch = (index: number) => {
        const params: IAlertParams = {
            title: PrimaryInformationThirdController.resourcesViewModel.getResourceAlertDeleteGalleryImageTitle(),
            message: PrimaryInformationThirdController.resourcesViewModel.getResourceAlertDeleteGalleryImageMessage(),
            onOKPress: () => this._onConfirmRemoveGalleryImage(index),
            onCancelPress: () => console.log('Cancel Pressed'),
            delay: 0
        }
        this._confirmationAlertOS(params)
    }

    private _onConfirmRemoveGalleryImage = (index: number) => {
        CreateEditProfileController.viewModel?.setGalleryImagesToRemove(index)
        let galleries = CreateEditProfileController.viewModel.getGalleryImages.slice();
        galleries.splice(index, 1)
        CreateEditProfileController.viewModel?.setGalleryImages(galleries)

    }

    public onSaveAndContinueButtonDidTouch = async () => {
        if (!CreateEditProfileController.viewModel?.getIsThirdPageValid) {
            const params: IAlertParams = {
                title: CreateEditProfileController.resourcesViewModel?.getResourceAlertValidateUserTitle(),
                message: CreateEditProfileController.resourcesViewModel?.getResourceAlertValidateUserMessage()
            }
            this._generalAlertOS(params)
        } else {
            this._myProps?.navigation?.dispatch(CommonActions.navigate(NavigationKey.CreateEditProfileFourth) as any)
        }
    }


    /*
       Mark Life cycle
    */

}

export default PrimaryInformationThirdController