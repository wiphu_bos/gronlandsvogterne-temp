
import React from "react"
import { PrimaryInformationProps } from "../../pi-fourth-page.props"
import { observer } from "mobx-react-lite"
import * as Styles from "./questionair.styles"
import PrimaryInformationFourthController from "../../pi-fourth-page.controllers"
import { Text, Icon, Button } from "../../../../../../../components"
import { View } from "react-native"
import { images } from "../../../../../../../theme/images"
import CreateEditProfileController from "../../../../../create-edit-profile-tab/create-edit-profile.controllers"

export const QuestionaireComponent: React.FunctionComponent<PrimaryInformationProps> = observer((props) => {
    const rootViewModel = CreateEditProfileController.viewModel
    const resourcesViewModel = PrimaryInformationFourthController.resourcesViewModel

    const yesIconSource: string = rootViewModel?.getIsHighlight ? images.checkedCircle : images.uncheckCircle
    const noIconSource: string = !rootViewModel?.getIsHighlight ? images.checkedCircle : images.uncheckCircle
    return (<View shouldRasterizeIOS style={Styles.QUESTIONAIR_CONTAINER}>
        <Button preset="none" isAnimated={false} isSolid style={Styles.QUESTIONAIR} onPress={() => PrimaryInformationFourthController.onCheckboxDidTouch(true)} >
            <Icon source={yesIconSource} style={Styles.ICON_DUDE} />
            <Text text={resourcesViewModel?.getResourceYesAnswer()} {...resourcesViewModel?.getTestIDTitle()} style={{ ...Styles.TITLE, ...Styles.ADDITION_LEFT_MARGIN_TEXT }} />
        </Button>
        <Button preset="none" isAnimated={false} isSolid style={{ ...Styles.QUESTIONAIR, ...Styles.ADDITION_TOP_MARGIN_TEXT }} onPress={() => PrimaryInformationFourthController.onCheckboxDidTouch(false)} >
            <Icon source={noIconSource} style={Styles.ICON_DUDE} />
            <Text text={resourcesViewModel?.getResourceNoAnswer()} {...resourcesViewModel?.getTestIDTitle()} style={{ ...Styles.TITLE, ...Styles.ADDITION_LEFT_MARGIN_TEXT }} />
        </Button>
    </View>)
})