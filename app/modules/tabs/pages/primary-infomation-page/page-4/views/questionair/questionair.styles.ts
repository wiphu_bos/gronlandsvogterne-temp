import { ViewStyle, TextStyle, ImageStyle } from "react-native"
import * as metric from "../../../../../../../theme"
import { color } from "../../../../../../../theme"
import { RFValue } from "react-native-responsive-fontsize"
export const FULL: ViewStyle = {
    flex: 1
}

export const ICON_CHECK: ImageStyle = {
    width: metric.ratioWidth(27.13),
    height: metric.ratioWidth(27.13),
}

export const ADDITION_LEFT_MARGIN_TEXT: ViewStyle = {
    marginLeft: metric.ratioWidth(16.64),
}

export const ADDITION_TOP_MARGIN_TEXT: ViewStyle = {
    marginTop: metric.ratioHeight(27.88),
}
export const QUESTIONAIR_CONTAINER: ViewStyle = {
    marginTop: metric.ratioHeight(28.94),
    ...FULL,
}

export const QUESTIONAIR: ViewStyle = {
    flexDirection: 'row',
    alignItems:'center'
}

export const ICON_DUDE: ImageStyle = {
    width: metric.ratioWidth(36.34),
    height: metric.ratioHeight(40.46),
}
const TEXT: TextStyle = {
    color: color.palette.white,
    fontFamily: "Montserrat",
}
export const TITLE: TextStyle = {
    ...TEXT,
    fontSize: RFValue(14),
    fontWeight: '400',

    //Android
    fontFamily: "Montserrat-Regular",
}

