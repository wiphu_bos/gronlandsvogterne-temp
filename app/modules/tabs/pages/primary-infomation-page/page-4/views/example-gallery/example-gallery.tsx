

import React from "react"
import * as Styles from "./example-gallery.styles"
import { Icon } from "../../../../../../../components"
import { View } from "react-native"
import { images } from "../../../../../../../theme/images"


export const ExampleGalleryComponent = () => {
    return (<View shouldRasterizeIOS style={Styles.EXAMPLE_GALLERY_CONTAINER}>
       <View shouldRasterizeIOS style={Styles.EXAMPLE_GALLERY} >
           <View shouldRasterizeIOS style={Styles.DUDE_CONTAINER}>
                <Icon source={images.dudeWhite} style={Styles.ICON_DUDE} />
            </View>
        </View>
       <View shouldRasterizeIOS style={{ ...Styles.EXAMPLE_GALLERY, ...Styles.ADDITION_LEFT_MARGIN }} >
           <View shouldRasterizeIOS style={Styles.DUDE_CONTAINER}>
                <Icon source={images.dudeWhite} style={Styles.ICON_DUDE} />
            </View>
        </View>
    </View>)
}