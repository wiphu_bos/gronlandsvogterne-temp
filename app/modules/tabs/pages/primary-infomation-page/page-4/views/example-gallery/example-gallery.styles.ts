import { ViewStyle, TextStyle, ImageStyle } from "react-native"
import * as metric from "../../../../../../../theme"
import { color } from "../../../../../../../theme"
import { RFValue } from "react-native-responsive-fontsize"
export const FULL: ViewStyle = {
    flex: 1
}

export const EXAMPLE_GALLERY_CONTAINER: ViewStyle = {
    marginBottom: metric.ratioHeight(28),
    flexDirection: 'row'
}

export const EXAMPLE_GALLERY: ViewStyle = {
    width: metric.ratioWidth(200),
    height: metric.ratioHeight(110),
    borderRadius: metric.ratioWidth(16),
    backgroundColor: color.darkRedDim
}

export const DUDE_CONTAINER: ImageStyle = {
    position: 'absolute',
    top: metric.ratioHeight(16),
    right: metric.ratioWidth(14.66),
}

export const ADDITION_LEFT_MARGIN: ViewStyle = {
    marginLeft: metric.ratioWidth(10),
}
export const ICON_DUDE: ImageStyle = {
    width: metric.ratioWidth(36.34),
    height: metric.ratioHeight(40.46),
}
const TEXT: TextStyle = {
    color: color.palette.white,
    fontFamily: "Montserrat",
}
export const TITLE: TextStyle = {
    ...TEXT,
    fontSize: RFValue(14),
    fontWeight: '400',

    //Android
    fontFamily: "Montserrat-Regular",
}

