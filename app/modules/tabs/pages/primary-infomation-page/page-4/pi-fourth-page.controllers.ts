// MARK: Import
import { NavigationKey } from "../../../../../constants/app.constant"
import { RootStore, INavigationRoute } from "../../../../../models"
import { PrimaryInformationProps } from "./pi-fourth-page.props"
import { PrimaryInformationFourthResourcesStore, PrimaryInformationFourthStore } from "./storemodels/pi-fourth-page.types"
import { PrimaryInformationFourthResourcesStoreModel, PrimaryInformationFourthStoreModel } from "./storemodels/pi-fourth-page.store"
import { BaseController, IAlertParams } from "../../../../base.controller"
import { onSnapshot } from "mobx-state-tree"
import { CommonActions } from "@react-navigation/core"
import CreateEditProfileController from "../../../create-edit-profile-tab/create-edit-profile.controllers"
import { IUser, IPaymentResponse, IPublishDudeWithPayment } from "../../../../../models/user-store/user.types"
import { ValidationServices } from "../../../../../services/api-domain/validate.services"
import { Property } from "../../../../../constants/firebase/fire-store.constant"
import * as DataUtils from "../../../../../utils/data.utils"


class PrimaryInformationFourthController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static resourcesViewModel: PrimaryInformationFourthResourcesStore
    public static viewModel: PrimaryInformationFourthStore
    private static _confirmHighlightDude: boolean

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, props?: PrimaryInformationProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, props, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(props)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        PrimaryInformationFourthController.resourcesViewModel = PrimaryInformationFourthResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.CreateEditProfileFourth)
        PrimaryInformationFourthController.viewModel = localStore && PrimaryInformationFourthStoreModel.create({ ...localStore }) || PrimaryInformationFourthStoreModel.create({})
    }

    private _setupSnapShot = () => {
        onSnapshot(PrimaryInformationFourthController.viewModel, (snap: PrimaryInformationFourthStore) => {
            this._setInitializedToPropsParams()
        })
    }

    /*
       Mark Data
    */

    /*
       Mark Event
    */

    private _onCloseModal = (): void => {
        PrimaryInformationFourthController.viewModel?.setIsModalShowing(false)
    }

    public onConfirmHighlightDude = (): void => {
        PrimaryInformationFourthController._confirmHighlightDude = true
        this._onCloseModal()
    }

    public onModalClosed = async () => {
        if (PrimaryInformationFourthController._confirmHighlightDude) {
            this._isGlobalLoading(true)
            const payment = await CreateEditProfileController.viewModel?.purchaseHighlightDudeIAP(this._rootStore)
            if (payment?.transactionId) {
                await this._requestUpdateProfileWithPayment(payment)
            }
            this._isGlobalLoading(false)
        }
        PrimaryInformationFourthController._confirmHighlightDude = null
    }

    public gotoWaitDudeApprovePublishProfie = () => {
        const { navigation } = this._myProps
        navigation?.dispatch(CommonActions.navigate(NavigationKey.WaitingDudePublishProfile) as any)
    }

    public onConfirmModalCancel = () => {
        PrimaryInformationFourthController._confirmHighlightDude = null
        this._onCloseModal()
    }

    public onSubmitButtonDidTouch = async () => {
        this._validateData()
        if (!CreateEditProfileController.viewModel?.getAllUserCredentialsValid) {
            const params: IAlertParams = {
                title: CreateEditProfileController.resourcesViewModel?.getResourceAlertValidateUserTitle(),
                message: CreateEditProfileController.resourcesViewModel?.getResourceAlertValidateUserMessage()
            }
            this._generalAlertOS(params)
        } else {
            await this._requestUpdateProfile()
        }
    }

    private _requestUpdateProfile = async () => {
        const dudeId = CreateEditProfileController.viewModel?.getUserId
        this._isGlobalLoading(true)
        const result = await CreateEditProfileController.viewModel?.askForPublishDude(this._rootStore, dudeId)
        this._isGlobalLoading(false)
        const data: IUser = result?.data
        if (!data) {
            const params = {
                rootStore: this._rootStore,
                error: result
            }
            const [isError, message] = new ValidationServices(params).purchaseDudeHighlight()
            if (isError) return this._delayExecutor(() => PrimaryInformationFourthController.viewModel?.setIsModalShowing(isError))

            const errorMsg = new ValidationServices(params).generalAPIResponseValidation()
            if (errorMsg) return this._generalAlertOS({ message: errorMsg })
        } else {
            this._requestUpdateProfileSuccess(data)
        }
    }

    private _requestUpdateProfileWithPayment = async (payment: IPaymentResponse) => {
        const dudeId = CreateEditProfileController.viewModel?.getUserId
        this._isGlobalLoading(true)
        const result = await CreateEditProfileController.viewModel?.askForPublishDude(this._rootStore, dudeId, payment)
        this._isGlobalLoading(false)
        const data: IPublishDudeWithPayment = result?.data
        if (!data) return
        const dudeDetails: IUser = data[Property.DUDE_DETAILS]
        const paymentDetails: IPaymentResponse = data[Property.PAYMENT_DETAILS]
        const expiryDateForHighlightDude = paymentDetails[Property.EXPIRY_DATE] //already cast to date from backend.
        const dudeDetailsWithPurchaseDetails = DataUtils.addPurchaseToUserProfileObject(dudeDetails, expiryDateForHighlightDude, false)
        if (!dudeDetailsWithPurchaseDetails) return
        this._requestUpdateProfileSuccess(dudeDetailsWithPurchaseDetails)
    }

    private _requestUpdateProfileSuccess = (data: IUser) => {
        this._rootStore?.getUserStore?.saveMyDudeToList(this._rootStore, data)
        this.gotoWaitDudeApprovePublishProfie()
    }

    public static onCheckboxDidTouch = (value: boolean) => {
        CreateEditProfileController.viewModel?.setIsHighlight(value)
    }

    /*
       Mark Life cycle
    */

    /*
        Helper
    */

    private _validateData = () => {
        const isValid = CreateEditProfileController.viewModel?.getIsAllPagesValid
        CreateEditProfileController.viewModel?.setIsValid(isValid)
    }

}

export default PrimaryInformationFourthController