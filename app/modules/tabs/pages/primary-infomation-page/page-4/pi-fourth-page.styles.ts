import { ViewStyle, TextStyle } from "react-native"
import { color } from "../../../../../theme"
import * as metric from "../../../../../theme"
import { RFValue } from "react-native-responsive-fontsize"

export const FULL: ViewStyle = {
    flex: 1
}
export const TAB_PAGE_BACKGROUND: any = { colors: color.tabPageBackground }

export const CONTAINNER: ViewStyle = {
    ...FULL
}

export const AVOID_KEYBOARD_VIEW: ViewStyle = {
    ...FULL
}
export const SCROLL_VIEW: ViewStyle = {
    paddingBottom: metric.ratioHeight(10),
}

export const CONTENT_CONTAINER: ViewStyle = {
    marginHorizontal: metric.ratioWidth(37),
    paddingVertical: metric.ratioHeight(31),
}
export const HEADER: ViewStyle = {
    marginBottom: metric.ratioHeight(28),
}
export const TEXT: TextStyle = {
    color: color.palette.white,
    fontFamily: "Montserrat",
}
export const TITLE: TextStyle = {
    ...TEXT,
    fontSize: RFValue(16),
    fontWeight: '400',

    //Android
    fontFamily: "Montserrat-Regular",
}
export const DESCRIPTION: TextStyle = {
    ...TEXT,
    fontSize: RFValue(14),
    fontWeight: '400',

    //Android
    fontFamily: "Montserrat-Regular",
}

export const TEXT_AREA_OPTIONS: any = {
    ...metric.disableAutoCompleteTextField,
    multiline: true,
    numberOfLines: 0,
    maxLength: metric.maxDescriptionChar,
    selectionColor: color.palette.pink
}

export const TEXT_AREA: ViewStyle = {
    height: metric.ratioHeight(300),
    backgroundColor: color.palette.white,
    borderRadius: metric.ratioWidth(10),
    paddingHorizontal: metric.ratioWidth(19),
    paddingTop: metric.ratioHeight(20),
    paddingBottom: metric.ratioHeight(20),
}

export const BUTTON_MARGIN: ViewStyle = {
    alignSelf: 'center',
    marginBottom: metric.ratioHeight(40)
}