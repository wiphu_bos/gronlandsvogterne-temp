import { Instance, SnapshotOut } from "mobx-state-tree"
import { PrimaryInformationFourthStoreModel, PrimaryInformationFourthResourcesStoreModel } from "./pi-fourth-page.store"

export type PrimaryInformationFourthStore = Instance<typeof PrimaryInformationFourthStoreModel>
export type PrimaryInformationFourthStoreSnapshot = SnapshotOut<typeof PrimaryInformationFourthStoreModel>

export type PrimaryInformationFourthResourcesStore = Instance<typeof PrimaryInformationFourthResourcesStoreModel>
export type PrimaryInformationFourthResourcesStoreSnapshot = SnapshotOut<typeof PrimaryInformationFourthResourcesStoreModel>