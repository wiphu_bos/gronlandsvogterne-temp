import { types } from "mobx-state-tree"
import { StoreName } from "../../../../../../constants/app.constant"
import { PrimaryInformationFourthPropsModel } from "../viewmodels/pi-fourth-page.models"
import { PrimaryInformationFourthActions } from "../viewmodels/pi-fourth-page.actions"
import { PrimaryInformationFourthViews, PrimaryInformationFourthResourcesViews } from "../viewmodels/pi-fourth-page.views"
import { GeneralResourcesStoreModel } from "../../../../../../models/general-resources-store"

const PrimaryInformationFourthModel = types.model(StoreName.PrimaryInformationFourth, PrimaryInformationFourthPropsModel)
export const PrimaryInformationFourthStoreModel = types.compose(PrimaryInformationFourthModel,PrimaryInformationFourthViews,PrimaryInformationFourthActions)
export const PrimaryInformationFourthResourcesStoreModel = types.compose(GeneralResourcesStoreModel, PrimaryInformationFourthResourcesViews)

