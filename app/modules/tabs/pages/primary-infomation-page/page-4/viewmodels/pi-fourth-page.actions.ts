import { types } from "mobx-state-tree"
import { PrimaryInformationFourthPropsModel } from "./pi-fourth-page.models"

export const PrimaryInformationFourthActions = types.model(PrimaryInformationFourthPropsModel).actions(self => {

    //MARK: Volatile State

    //MARK: Actions
    const setIsPopoverMenuVisible = (value: boolean) => self.isPopoverMenuVisible = value
    const setIsModalShowing = (value: boolean) => {
        self.isModalShowing = value
    }
    return {
        setIsPopoverMenuVisible,
        setIsModalShowing
    }
})