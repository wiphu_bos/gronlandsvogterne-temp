import { types } from "mobx-state-tree"

export const PrimaryInformationFourthPropsModel = {
    isPopoverMenuVisible: types.optional(types.boolean,false),
    isModalShowing: types.optional(types.boolean,false)
} 