import { types } from "mobx-state-tree"
import { PrimaryInformationFourthPropsModel } from "./pi-fourth-page.models"
import { GeneralResources } from "../../../../../../constants/firebase/remote-config"
import { NavigationKey } from "../../../../../../constants/app.constant"
import { TestIDResources } from "../../../../../../constants/test-key/test.constant"
import * as Utils from "../../../../../../utils"
import { GeneralResourcesStoreModel } from "../../../../../../models/general-resources-store"

export const PrimaryInformationFourthViews = types.model(PrimaryInformationFourthPropsModel)
    .views(self => ({
        get getIsModalShowing() {
            return self.isModalShowing
        },
    }))

export const PrimaryInformationFourthResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State

    const { PrimaryInformationFourthScreen } = GeneralResources
    const { title, description, yesAnswer, noAnswer, buttonSubmitTitle, modalButtonCancelTitle, modalButtonClearTitle,
        modalButtonOKTitle, modalConfirmHilightDude } = PrimaryInformationFourthScreen

    //MARK: Views
    const getResources = (key: string, childKeyOrShareKey: string | boolean = false) => self.getValues(childKeyOrShareKey ? key : NavigationKey.PrimaryInformationScreen, childKeyOrShareKey ? true : key)
    const getShareResources = (key: string) => self.getValues(key, true)
    const getResourceTitle = () => getResources(title)
    const getResourceDescription = () => getResources(description)
    const getResourceYesAnswer = () => getResources(yesAnswer)
    const getResourceNoAnswer = () => getResources(noAnswer)
    const getResourceButtonSubmitTitle = () => getResources(buttonSubmitTitle, true)
    const getResourceModalButtonCancelTitle = () => getShareResources(modalButtonCancelTitle)
    const getResourceModalButtonOKTitle = () => getShareResources(modalButtonOKTitle)
    const getResourceModalButtonClearTitle = () => getShareResources(modalButtonClearTitle)
    const getResourceModalConfirmHilightDude = () => getShareResources(modalConfirmHilightDude)

    return {
        getResourceTitle,
        getResourceDescription,
        getResourceYesAnswer,
        getResourceNoAnswer,
        getResourceButtonSubmitTitle,
        getResourceModalButtonCancelTitle,
        getResourceModalButtonOKTitle,
        getResourceModalButtonClearTitle,
        getResourceModalConfirmHilightDude
    }
})
    .views(self => {

        //MARK: Volatile State

        //MARK: Views

        const getTestIDTitle = () => Utils.getTestIDObject(TestIDResources.PrimaryInformationFourthScreen.title)
        const getTestIDDescription = () => Utils.getTestIDObject(TestIDResources.PrimaryInformationFourthScreen.description)
        const getTestIDExampleHighlightGallery = () => Utils.getTestIDObject(TestIDResources.PrimaryInformationFourthScreen.exampleHighlightGallery)
        const getTestIDSelectYesAnswer = () => Utils.getTestIDObject(TestIDResources.PrimaryInformationFourthScreen.selectYesAnswer)
        const getTestIDSelectNoAnswer = () => Utils.getTestIDObject(TestIDResources.PrimaryInformationFourthScreen.selectNoAnswer)
        const getTestIDButtonSubmitTitle = () => Utils.getTestIDObject(TestIDResources.PrimaryInformationFourthScreen.buttonSubmit)
        const getTestIDConfirmModal = () => Utils.getTestIDObject(TestIDResources.PrimaryInformationFourthScreen.confirmModal)

        return {
            getTestIDTitle,
            getTestIDDescription,
            getTestIDExampleHighlightGallery,
            getTestIDSelectYesAnswer,
            getTestIDSelectNoAnswer,
            getTestIDButtonSubmitTitle,
            getTestIDConfirmModal
        }
    })
