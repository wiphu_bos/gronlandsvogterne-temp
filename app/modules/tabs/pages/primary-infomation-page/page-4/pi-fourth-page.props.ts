import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { CreateEditProfileStore } from "../../../create-edit-profile-tab/storemodels"
import { PrimaryInformationFourthStore } from "./storemodels/pi-fourth-page.types"

type PrimaryInformationStore =
| PrimaryInformationFourthStore

export interface PrimaryInformationProps extends NavigationContainerProps<ParamListBase> {
    rootViewModel?: CreateEditProfileStore
    viewModel?: PrimaryInformationStore,
}