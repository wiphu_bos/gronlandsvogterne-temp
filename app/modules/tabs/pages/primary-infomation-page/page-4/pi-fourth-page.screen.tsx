// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { PrimaryInformationProps } from "./pi-fourth-page.props"
import LinearGradient from "react-native-linear-gradient"
import { useConfigurate } from "../../../../../custom-hooks/use-configure-controller"
import { View } from "react-native-animatable"
import * as metric from "../../../../../theme"
import PrimaryInformationFourthController from "./pi-fourth-page.controllers"
import { INavigationRoute } from "../../../../../models/navigation-store/navigation.types"
import { GradientButton } from "../../../../../components/gradient-button/button"
import { Text, AnimatedScrollView } from "../../../../../components"
import { QuestionaireComponent } from "./views/questionair/questionair"
import { ExampleGalleryComponent } from "./views/example-gallery/example-gallery"
import { ConfirmModal } from "../../../../../components/confirm-modal"

// MARK: Style Import

import * as Styles from "./pi-fourth-page.styles"
import CreateEditProfileController from "../../../create-edit-profile-tab/create-edit-profile.controllers"

export const PrimaryInformationFourthScreen: React.FunctionComponent<PrimaryInformationProps & Partial<INavigationRoute>> = observer((props) => {

    const controller = useConfigurate(PrimaryInformationFourthController, props) as PrimaryInformationFourthController

    // MARK: Render
    return (
        <LinearGradient shouldRasterizeIOS style={Styles.CONTAINNER} {...Styles.TAB_PAGE_BACKGROUND}>
            <AnimatedScrollView
                contentContainerStyle={Styles.SCROLL_VIEW}
                {...metric.solidScrollView}>
                <View shouldRasterizeIOS style={Styles.CONTENT_CONTAINER}>
                    <View shouldRasterizeIOS style={Styles.HEADER}>
                        <Text text={PrimaryInformationFourthController.resourcesViewModel?.getResourceTitle()} {...PrimaryInformationFourthController.resourcesViewModel?.getTestIDTitle()} style={Styles.TITLE} />
                    </View>
                    <ExampleGalleryComponent />
                    <Text text={PrimaryInformationFourthController.resourcesViewModel?.getResourceDescription()} {...PrimaryInformationFourthController.resourcesViewModel?.getTestIDDescription()} style={Styles.DESCRIPTION} />
                    <QuestionaireComponent rootViewModel={CreateEditProfileController.viewModel} />
                </View>
            </AnimatedScrollView>
            <GradientButton
                {...PrimaryInformationFourthController.resourcesViewModel?.getTestIDButtonSubmitTitle()}
                text={PrimaryInformationFourthController.resourcesViewModel?.getResourceButtonSubmitTitle()}
                containerStyle={Styles.BUTTON_MARGIN}
                onPress={controller.onSubmitButtonDidTouch} />
            <ConfirmModal
                onCancel={controller.onConfirmModalCancel}
                onOK={controller.onConfirmHighlightDude}
                onModalClosed={controller.onModalClosed}
                {...PrimaryInformationFourthController.resourcesViewModel?.getTestIDConfirmModal()}
                okButtonText={PrimaryInformationFourthController.resourcesViewModel?.getResourceModalButtonOKTitle()}
                clearButtonText={PrimaryInformationFourthController.resourcesViewModel?.getResourceModalButtonCancelTitle()}
                message={PrimaryInformationFourthController.resourcesViewModel?.getResourceModalConfirmHilightDude()}
                isVisible={PrimaryInformationFourthController.viewModel?.getIsModalShowing || false}
            />
        </LinearGradient>

    )
})
