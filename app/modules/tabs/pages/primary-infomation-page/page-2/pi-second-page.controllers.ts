// MARK: Import
import { NavigationKey } from "../../../../../constants/app.constant"
import { RootStore, INavigationRoute } from "../../../../../models"
import { PrimaryInformationProps } from "./pi-second-page.props"
import { PrimaryInformationSecondResourcesStore, PrimaryInformationSecondStore } from "./storemodels/pi-second-page.types"
import { PrimaryInformationSecondResourcesStoreModel, PrimaryInformationSecondStoreModel } from "./storemodels/pi-second-page.store"
import { Keyboard } from "react-native"
import { BaseController, IAlertParams } from "../../../../base.controller"
import { onSnapshot } from "mobx-state-tree"
import CreateEditProfileController from "../../../create-edit-profile-tab/create-edit-profile.controllers"
import { CommonActions } from "@react-navigation/native"

class PrimaryInformationSecondController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static resourcesViewModel: PrimaryInformationSecondResourcesStore
    public static viewModel: PrimaryInformationSecondStore

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, props?: PrimaryInformationProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, props, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(props)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        PrimaryInformationSecondController.resourcesViewModel = PrimaryInformationSecondResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.CreateEditProfileSecond)
        PrimaryInformationSecondController.viewModel = localStore && PrimaryInformationSecondStoreModel.create({ ...localStore }) || PrimaryInformationSecondStoreModel.create({})
    }

    private _setupSnapShot = () => {
        onSnapshot(PrimaryInformationSecondController.viewModel, (snap: PrimaryInformationSecondStore) => {
            this._setInitializedToPropsParams()
        })
    }

    /*
       Mark Data
    */

    /*
       Mark Event
    */

    public onDescriptionChangeText = (value: string): void => {
        CreateEditProfileController.viewModel?.setDescription(value)
    }

    public onBlurDescriptionTextField = (): void => {

    }

    public onSaveAndContinueButtonDidTouch = () => {
        if (!CreateEditProfileController.viewModel?.getIsSecondPageValid) {
            const params: IAlertParams = {
                title: CreateEditProfileController.resourcesViewModel?.getResourceAlertValidateUserTitle(),
                message: CreateEditProfileController.resourcesViewModel?.getResourceAlertValidateUserMessage()
            }
            this._generalAlertOS(params)
        } else {
            this._myProps?.navigation?.dispatch(CommonActions.navigate(NavigationKey.CreateEditProfileThird) as any)
        }
    }

    /*
       Mark Life cycle
    */

    //@override
    viewWillDisappear = () => {
        super.viewWillDisappear && super.viewWillDisappear()
        Keyboard.dismiss()
    }

}

export default PrimaryInformationSecondController