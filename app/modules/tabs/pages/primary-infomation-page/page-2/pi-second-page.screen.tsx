// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { PrimaryInformationProps } from "./pi-second-page.props"
import LinearGradient from "react-native-linear-gradient"
import { useConfigurate } from "../../../../../custom-hooks/use-configure-controller"
import { TextInput } from "react-native-gesture-handler"
import { View } from "react-native-animatable"
import { KeyboardAvoidingView } from "react-native"
import * as metric from "../../../../../theme"
import { INavigationRoute } from "../../../../../models/navigation-store/navigation.types"
import { GradientButton } from "../../../../../components/gradient-button/button"
import { Text, AnimatedScrollView } from "../../../../../components"
import * as StringUtils from "../../../../../utils/string.utils"
import PrimaryInformationSecondController from "./pi-second-page.controllers"
import CreateEditProfileController from "../../../create-edit-profile-tab/create-edit-profile.controllers"

// MARK: Style Import

import * as Styles from "./pi-second-page.styles"

export const PrimaryInformationSecondScreen: React.FunctionComponent<PrimaryInformationProps & Partial<INavigationRoute>> = observer((props) => {

    const controller = useConfigurate(PrimaryInformationSecondController, props) as PrimaryInformationSecondController

    // MARK: Render

    return (
        <LinearGradient shouldRasterizeIOS style={Styles.CONTAINNER} {...Styles.TAB_PAGE_BACKGROUND}>
            <KeyboardAvoidingView
                keyboardVerticalOffset={metric.isIPhone ? metric.ratioHeight(120) : metric.ratioHeight(120)}
                behavior={metric.keyboardBehavior}
                style={Styles.AVOID_KEYBOARD_VIEW}>
                <AnimatedScrollView
                    contentContainerStyle={Styles.SCROLL_VIEW}
                    {...metric.solidScrollView}>
                    <View shouldRasterizeIOS style={Styles.CONTENT_CONTAINER}>
                        <View shouldRasterizeIOS style={Styles.HEADER}>
                            <Text text={PrimaryInformationSecondController.resourcesViewModel?.getResourceTitle()}
                                {...PrimaryInformationSecondController.resourcesViewModel?.getTestIDTitle()}
                                style={Styles.TITLE} />
                        </View>
                        <TextInput
                            style={{ ...Styles.TEXT_AREA, ...Styles.TEXT_AREA_TEXT }}
                            {...Styles.TEXT_AREA_OPTIONS}
                            onChangeText={controller.onDescriptionChangeText}
                            testIDErrorMessage={PrimaryInformationSecondController.resourcesViewModel?.getTestIDDescription()}
                            onBlur={controller.onBlurDescriptionTextField}
                        >
                            {StringUtils.brToNewLine(CreateEditProfileController.viewModel?.getDescription)}
                        </TextInput>
                    </View>

                </AnimatedScrollView>
            </KeyboardAvoidingView>
            <View shouldRasterizeIOS style={Styles.BUTTON_CONTAINER}>
                <GradientButton
                    {...PrimaryInformationSecondController.resourcesViewModel?.getTestIDButtonSaveAndContinue()}
                    text={PrimaryInformationSecondController.resourcesViewModel?.getResourceButtonSaveAndContinueTitle()}
                    onPress={controller.onSaveAndContinueButtonDidTouch} />
            </View>
        </LinearGradient>
    )
})
