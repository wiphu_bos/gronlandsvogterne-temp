import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { CreateEditProfileStore } from "../../../create-edit-profile-tab/storemodels"
import { PrimaryInformationSecondStore } from "./storemodels/pi-second-page.types"

type PrimaryInformationStore =
| PrimaryInformationSecondStore

export interface PrimaryInformationProps extends NavigationContainerProps<ParamListBase> {
    rootViewModel?: CreateEditProfileStore
    viewModel?: PrimaryInformationStore
}