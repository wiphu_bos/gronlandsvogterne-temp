import { Instance, SnapshotOut } from "mobx-state-tree"
import { PrimaryInformationSecondStoreModel, PrimaryInformationSecondResourcesStoreModel } from "./pi-second-page.store"

export type PrimaryInformationSecondStore = Instance<typeof PrimaryInformationSecondStoreModel>
export type PrimaryInformationSecondStoreSnapshot = SnapshotOut<typeof PrimaryInformationSecondStoreModel>

export type PrimaryInformationSecondResourcesStore = Instance<typeof PrimaryInformationSecondResourcesStoreModel>
export type PrimaryInformationSecondResourcesStoreSnapshot = SnapshotOut<typeof PrimaryInformationSecondResourcesStoreModel>