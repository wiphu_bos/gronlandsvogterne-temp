import { types } from "mobx-state-tree"
import { StoreName } from "../../../../../../constants/app.constant"
import { PrimaryInformationSecondPropsModel } from "../viewmodels/pi-second-page.models"
import { PrimaryInformationSecondActions } from "../viewmodels/pi-second-page.actions"
import { PrimaryInformationSecondViews, PrimaryInformationSecondResourcesViews } from "../viewmodels/pi-second-page.views"
import { GeneralResourcesStoreModel } from "../../../../../../models/general-resources-store"

const PrimaryInformationSecondModel = types.model(StoreName.PrimaryInformationSecond, PrimaryInformationSecondPropsModel)
export const PrimaryInformationSecondStoreModel = types.compose(PrimaryInformationSecondModel,PrimaryInformationSecondViews,PrimaryInformationSecondActions)
export const PrimaryInformationSecondResourcesStoreModel = types.compose(GeneralResourcesStoreModel, PrimaryInformationSecondResourcesViews)

