import { ViewStyle, TextStyle } from "react-native"
import { color } from "../../../../../theme"
import * as metric from "../../../../../theme"
import { RFValue } from "react-native-responsive-fontsize"

export const FULL: ViewStyle = {
    flex: 1
}
export const TAB_PAGE_BACKGROUND: any = { colors: color.tabPageBackground }

export const CONTAINNER: ViewStyle = {
    ...FULL
}

export const AVOID_KEYBOARD_VIEW: ViewStyle = {
    ...FULL
}
export const SCROLL_VIEW: ViewStyle = {
    ...FULL,
    paddingBottom: metric.ratioHeight(34),
}

export const CONTENT_CONTAINER: ViewStyle = {
    flex: 0.925,
    marginHorizontal: metric.ratioWidth(37),
    paddingTop: metric.ratioHeight(35)
}
export const HEADER: ViewStyle = {
    marginBottom: metric.ratioHeight(23),
}
export const TEXT: TextStyle = {
    color: color.palette.white,
    fontFamily: "Montserrat",
}
export const TITLE: TextStyle = {
    ...TEXT,
    fontSize: RFValue(16),
    fontWeight: '400',

    //Android
    fontFamily: "Montserrat-Regular",
}

export const TEXT_AREA_TEXT: TextStyle = {
    ...TEXT,
    fontSize: RFValue(14),
    fontWeight: '400',

    //Android
    fontFamily: "Montserrat-Regular",
    color:color.palette.darkGrey,
    textAlign:'left',
    textAlignVertical:'top',
}
export const TEXT_AREA_OPTIONS: any = {
    ...metric.disableAutoCompleteTextField,
    multiline: true,
    numberOfLines: 0,
    maxLength: metric.maxDescriptionChar,
    selectionColor:color.palette.pink
}

export const TEXT_AREA: ViewStyle = {
    ...FULL,
    backgroundColor: color.palette.white,
    borderRadius: metric.ratioWidth(10),
    paddingHorizontal: metric.ratioWidth(19),
    paddingTop: metric.ratioHeight(20),
    paddingBottom: metric.ratioHeight(20),
}

export const BUTTON_CONTAINER: ViewStyle = {
    flex: 0.225,
    alignSelf: 'center'
}