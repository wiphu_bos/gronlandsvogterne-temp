import { types } from "mobx-state-tree"
import { PrimaryInformationSecondPropsModel } from "./pi-second-page.models"
import { GeneralResources } from "../../../../../../constants/firebase/remote-config"
import { NavigationKey } from "../../../../../../constants/app.constant"
import { TestIDResources } from "../../../../../../constants/test-key/test.constant"
import * as Utils from "../../../../../../utils"
import { GeneralResourcesStoreModel } from "../../../../../../models/general-resources-store"

export const PrimaryInformationSecondViews = types.model(PrimaryInformationSecondPropsModel)
    .views(self => ({
        get getExample() {
            return self
        },
    }))

export const PrimaryInformationSecondResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State

    const { PrimaryInformationSecondScreen } = GeneralResources
    const { fullDescriptionTitle, buttonSaveAndContinueTitle } = PrimaryInformationSecondScreen

    //MARK: Views
    const getResources = (key: string, childKeyOrShareKey: string | boolean = false) => self.getValues(childKeyOrShareKey ? key : NavigationKey.PrimaryInformationScreen, childKeyOrShareKey ? true : key)
    const getResourceTitle = () => getResources(fullDescriptionTitle)
    const getResourceButtonSaveAndContinueTitle = () => getResources(buttonSaveAndContinueTitle, true)
    return {
        getResourceTitle,
        getResourceButtonSaveAndContinueTitle
    }
})
    .views(self => {

        //MARK: Volatile State

        //MARK: Views

        const getTestIDTitle = () => Utils.getTestIDObject(TestIDResources.PrimaryInformationSecondScreen.title)
        const getTestIDDescription = () => Utils.getTestIDObject(TestIDResources.PrimaryInformationSecondScreen.description)
        const getTestIDButtonSaveAndContinue = () => Utils.getTestIDObject(TestIDResources.PrimaryInformationSecondScreen.buttonSaveAndContinue)
        return {
            getTestIDTitle,
            getTestIDDescription,
            getTestIDButtonSaveAndContinue
        }
    })
