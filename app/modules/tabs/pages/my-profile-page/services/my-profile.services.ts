
import { MyProfilePropsModel } from "../viewmodels"
import { types, flow } from "mobx-state-tree"
import { APIResponse, APISuccessResponse, APISuccessParams } from "../../../../../constants/service.types"
import { RootStore } from "../../../../../models"
import * as DataUtils from "../../../../../utils/data.utils"
import { IUser } from "../../../../../models/user-store"
import { DudeStatus, FileUploadType, UserType } from "../../../../../constants/app.constant"
import * as StringUtils from "../../../../../utils/string.utils"
import { IUploadFileParams, IUploadFileData } from "../../../../../utils/firebase/fire-storage/fire-storage.types"
import * as FirebaseUtils from "../../../../../utils/firebase/fire-storage"
import { GeneralResources } from "../../../../../constants/firebase/remote-config"
import { FirebaseAuthErrorResponse } from "../../../../../constants/firebase/auth/auth-error-response.constant"
import UserServices from "../../../../../services/api-domain/user.services"

export const MyProfileServiceActions = types.model(MyProfilePropsModel).actions(self => {

    const cancelSubscribePremiumAccount = flow(function* (rootStore: RootStore) {
        return yield rootStore?.getUserStore?.cancelSubscribePremiumAccount()
    })

    const fetchMyProfile = flow(function* (rootStore: RootStore) {
        try {
            const result: APIResponse = yield rootStore?.getUserStore.fetchMyProfile()
            let success = (result as APISuccessResponse)?.data
            let data: IUser = success?.data
            if (!data) throw result
            const userDataWithSubscriptionData = DataUtils.addSubscriptionToUserProfileObject(rootStore, data)
            const userProfile = DataUtils.getUserProfileObject(rootStore, rootStore?.getUserStore, userDataWithSubscriptionData)
            rootStore?.getUserStore?.bindingDataFromObject(userProfile, UserType.Me)
            return data
        } catch (e) {
            return e
        } finally {
            self.isPullRefreshing = false
        }
    })

    const updateSeekingGuy = flow(function* (rootStore: RootStore, status: boolean) {
        try {
            const result: APIResponse = yield rootStore?.getUserStore.updateSeekingGuy(status)
            let success = (result as APISuccessResponse)?.data
            let data: IUser = success?.data
            if (!data) throw result
            return success
        } catch (e) {
            return e
        } finally {
            self.isPullRefreshing = false
        }
    })

    const saveUserProfileProcess = flow(function* (rootStore: RootStore, isPublish: boolean) {
        const userStore = rootStore.getUserStore
        const params: Partial<IUser> = {
            uid: userStore.getUserId,
            full_name: userStore.getFullName,
            email: userStore.email,
            age: userStore.getAge?.value,
            city: userStore.getCity?.key,
            havd_soger_han_list: userStore.getHavdSogerHanList?.map(e => e.key),
            interest_list: userStore.getInterestList?.map(e => e.key),
            tag_list: userStore.getTagList?.map(e => e.key),
            description: StringUtils.newLineToBr(userStore.getDescription),
            profile_image: userStore.getProfileImageToUpdateDetails,
            gallery_image_list: userStore.getGalleryImageToUpdateDetails,
            status: isPublish ? DudeStatus.Published : DudeStatus.Unpublished
        }
        return yield UserServices.putUser(params)
    })

    const uploadImages = flow(function* (rootStore: RootStore, path: string) {
        const userStore = rootStore.getUserStore
        const galleryImagesList = userStore.getShouldUploadGalleryImages
        const profileImage = userStore?.getShouldUploadProfileImage
        const uploadList: any[] = galleryImagesList?.concat(profileImage)
        if (uploadList?.length > 0) {
            const params: IUploadFileParams = {
                fileList: uploadList,
                uid: path
            }
            const uploadedImageList: IUploadFileData[] = yield FirebaseUtils.uploadfiles(params)
            const profileImageList = uploadedImageList?.filter(e => e.type === FileUploadType.ProfileImage)
            const gallertImageList = uploadedImageList?.filter(e => e.type === FileUploadType.GalleryImage)
            if (profileImage.length > 0) userStore?.setProfileImage(profileImageList)
            if (gallertImageList.length > 0) userStore?.setGalleryImages(gallertImageList)
        }
    })

    const deleteImages = flow(function* (rootStore: RootStore, path: string) {
        const userStore = rootStore.getUserStore
        const galleryImagesToRemoveList = userStore?.getGalleryImagesToRemove
        const profileImageToRemove = userStore?.getProfileImagesToRemove
        if (galleryImagesToRemoveList.length > 0 || profileImageToRemove.length > 0) {
            const params: IUploadFileParams = {
                fileList: galleryImagesToRemoveList?.concat(profileImageToRemove) as any,
                uid: path
            }
            yield FirebaseUtils.deletefiles(params)
        }
    })

    const askForPublish = flow(function* (rootStore: RootStore, path: string, isPublish: boolean) {
        const isConnected = rootStore?.getSharedStore?.getIsConnected
        if (!isConnected) {
            const firebaseErrorMessageObject = rootStore?.getGeneralResourcesStore(GeneralResources.SharedKeys.firebaseErrorMessage, true)
            const internetErrorMessage = firebaseErrorMessageObject[FirebaseAuthErrorResponse.NetworkRequestFailed]
            return internetErrorMessage
        }
        try {
            yield deleteImages(rootStore, path)
            yield uploadImages(rootStore, path)
            const askForPublish: APISuccessParams = yield saveUserProfileProcess(rootStore, isPublish)
            let success = (askForPublish as APISuccessResponse)?.data
            let data: IUser = success?.data
            if (!data) throw askForPublish
            const userProfile = DataUtils.getUserProfileObject(rootStore, rootStore?.getUserStore, data)
            rootStore?.getUserStore?.bindingDataFromObject(userProfile, UserType.Me)
            return userProfile
        } catch (e) {
            return (e)
        }
    })

    return {
        fetchMyProfile,
        updateSeekingGuy,
        askForPublish,
        cancelSubscribePremiumAccount
    }
})