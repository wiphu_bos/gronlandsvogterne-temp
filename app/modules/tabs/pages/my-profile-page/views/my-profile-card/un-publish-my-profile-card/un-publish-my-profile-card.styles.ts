import { ViewStyle, TextStyle, ImageStyle } from "react-native"
import * as metric from "../../../../../../../theme"
import { color } from "../../../../../../../theme"
import { RFValue } from "react-native-responsive-fontsize"
import { images } from "../../../../../../../theme/images"
import { ButtonProps } from "../../../../../../../components/button/button.props"

export const DUDETTE_ICON: string = images.dudette

const SHADOW: ViewStyle = {
    shadowColor: color.palette.black,
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.13,
    elevation: 4
}

const ROW_CENTER: ViewStyle = {
    alignItems: 'center',
    flexDirection: 'row'
}

export const CONTAINNER: ViewStyle = {
    width: metric.ratioWidth(376),
    borderRadius: metric.ratioWidth(14),
    backgroundColor: color.palette.white,
    marginTop: metric.ratioHeight(21),
    ...SHADOW,
}

export const BODY_CONTAINNER: ViewStyle = {
    flexDirection: 'row',
    paddingVertical: metric.ratioHeight(15),
    paddingHorizontal: metric.ratioWidth(20),
}

export const IMAGE_PROFILE_CONTAINNER: ViewStyle = {
    alignItems: 'flex-end',
    justifyContent: 'center',
    width: metric.ratioWidth(70)
}

export const IMAGE_PROFILE: ImageStyle = {
    width: metric.ratioWidth(70),
    height: metric.ratioWidth(70),
    borderRadius: metric.ratioWidth(70) / 2
}

export const IMAGE_CHAT: ImageStyle = {
    width: metric.ratioWidth(14),
    height: metric.ratioHeight(12.25),
}

export const IMAGE_VIEW: ImageStyle = {
    width: metric.ratioWidth(15.75),
    height: metric.ratioHeight(10.5)
}

export const BODY_CONTAINNER_TEXT: ViewStyle = {
    flex: 1,
    paddingHorizontal: metric.ratioWidth(21),
}
export const FOOTER_CONTAINNER: ViewStyle = {

}
export const SEPARATOR_VIEW: ViewStyle = {
    backgroundColor: color.boder,
    height: metric.ratioHeight(1),
}

export const CONTAINNER_TEXT: ViewStyle = {
    justifyContent: 'center',

}

export const STATISTIC_COUNTER_CONTAINER: ViewStyle = {
    ...ROW_CENTER
}

export const USER_DETAILS_ACTION_CONTAINNER: ViewStyle = {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: metric.ratioWidth(20),
    paddingRight: metric.ratioWidth(12),
    paddingBottom: metric.ratioHeight(10)
}

export const SUB_RIGHT_USER_DETAILS_ACTION_CONTAINNER: ViewStyle = {
    justifyContent: 'flex-end'
}

export const adjustedMarginForRightActionContainer = (isPublished: boolean): ViewStyle => {
    const value: number = isPublished ? 12 : 55
    return {
        ...SUB_RIGHT_USER_DETAILS_ACTION_CONTAINNER, ...{
            marginBottom: -metric.ratioHeight(value)
        }
    }
}

export const LEFT_STATISTIC_COUNTER: ViewStyle = {
    ...ROW_CENTER
}

export const RIGHT_STATISTIC_COUNTER: ViewStyle = {
    ...ROW_CENTER,
    marginLeft: metric.ratioWidth(39)
}

export const FOOTER_CONTENT_CONTAINNER: ViewStyle = {
    flexDirection: 'row',
    paddingHorizontal: metric.ratioWidth(20),
    paddingVertical: metric.ratioHeight(16),
}

export const INDICATOR_VIEW: ViewStyle = {
    flexDirection: 'row',
}

const TEXT: TextStyle = {
    color: color.palette.darkGrey,
    fontFamily: "Montserrat",
}

export const TITLE: TextStyle = {
    ...TEXT,
    lineHeight: metric.ratioHeight(36),
    fontSize: RFValue(17),
    fontWeight: '400',

    //Android
    fontFamily: "Montserrat-Regular",
}

export const SUB_TITLE: TextStyle = {
    ...TEXT,
    lineHeight: metric.ratioHeight(24),
    fontSize: RFValue(12),
    fontWeight: '300',

    //Android
    fontFamily: "Montserrat-Light",
}

const DESCRIPTION: TextStyle = {
    ...TEXT,
    ...SUB_TITLE,
    lineHeight: metric.ratioHeight(27),
    fontSize: RFValue(11),
    color: color.palette.darkGrey,
    marginLeft: metric.ratioWidth(9),
    letterSpacing: metric.ratioWidth(1.13)
}

export const INDICATOR_TEXT: TextStyle = {
    ...DESCRIPTION,
    maxWidth: metric.ratioWidth(40)
}

export const SUB_INDICATOR_TEXT: TextStyle = {
    ...DESCRIPTION,
    maxWidth: metric.ratioWidth(55)
}

export const SMALL_TITLE: TextStyle = {
    ...TEXT,
    ...DESCRIPTION,
    fontSize: RFValue(10),
    letterSpacing: metric.ratioWidth(1.03),
    color: color.palette.white,
    marginLeft: metric.ratioWidth(0),
    paddingHorizontal: metric.ratioWidth(20)
}

export const PUBLISH_BUTTON_PROPS: ButtonProps = {
    preset: "none",
    isAnimated: false,
    isSolid: true,
    containerStyle: {
        backgroundColor: color.palette.green,
        borderRadius: metric.ratioWidth(75),
        height: metric.ratioHeight(40),
    },
    textStyle: {
        lineHeight: metric.ratioHeight(36),
        fontSize: RFValue(14),
        fontWeight: '400',
        textAlign: 'center',
        //Android
        fontFamily: "Montserrat-Regular",
    }
}

export const EDIT_UNPUBLISH_BUTTON_PROPS: ButtonProps = {
    preset: "none",
    isAnimated: false,
    isSolid: true,
    imageSource: images.edit,
    imageStyle: {
        width: metric.ratioWidth(39),
        height: metric.ratioWidth(39),
    },
    containerStyle: {
        width: metric.ratioWidth(80),
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    }
}

export const EDIT_BUTTON_PROPS: ButtonProps = {
    preset: "none",
    isAnimated: false,
    isSolid: true,
    imageSource: images.edit,
    imageStyle: {
        width: metric.ratioWidth(39),
        height: metric.ratioWidth(39),
        marginRight: metric.ratioWidth(20),
        marginBottom: metric.ratioHeight(20),
    },
    containerStyle: {
        marginRight: -metric.ratioWidth(20),
        marginBottom: -metric.ratioHeight(20),
        width: metric.ratioWidth(80),
        height: metric.ratioWidth(80),
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    }
}

export const SEEKING_GUY_STATUS_TEXT: TextStyle = {
    fontSize: RFValue(12),
    fontWeight: '400',
    //Android
    fontFamily: "Montserrat-Regular",
    color: color.palette.darkGrey,
    marginLeft: metric.ratioWidth(5)
}

const thumpColor = {
    thumpWhite: '#FBFBFB',
    thumpGreen: color.palette.green,
}
const trackColor = {
    trackGrey: '#ACACAC',
    trackGreen: color.palette.darkGreen,
}

export const TRACK: ViewStyle = {
    height: metric.ratioHeight(18),
    width: metric.ratioWidth(44),
    borderRadius: (metric.ratioWidth(26) * 3) / 4,
    borderWidth: metric.ratioWidth(2) / 2,
    backgroundColor: color.background,
}

export const THUMB: ViewStyle = {
    position: "absolute",
    width: metric.ratioWidth(26),
    height: metric.ratioWidth(26),
    borderColor: color.blackDim,
    borderRadius: metric.ratioWidth(26) / 2,
    borderWidth: metric.ratioWidth(2) / 2,
    backgroundColor: color.background,
    shadowColor: color.blackDim,
    shadowOffset: { width: metric.ratioWidth(1), height: metric.ratioHeight(1) },
    shadowOpacity: 1,
    shadowRadius: 2,
    elevation: 2,
    top: metric.ratioHeight(18 / 2) - metric.ratioWidth(13),
}

export const TRACK_OFF_STYLE: ViewStyle = {
    backgroundColor: trackColor.trackGrey,
    borderColor: color.blackDim,
}

export const TRACK_ON_STYLE: ViewStyle = {
    backgroundColor: trackColor.trackGreen,
    borderColor: color.blackDim,
}

export const THUMB_OFF_STYLE: ViewStyle = {
    backgroundColor: thumpColor.thumpWhite,
    borderColor: color.palette.lightestGrey
}

export const THUMB_NO_STYLE: ViewStyle = {
    backgroundColor: thumpColor.thumpGreen,
    borderColor: thumpColor.thumpGreen
}