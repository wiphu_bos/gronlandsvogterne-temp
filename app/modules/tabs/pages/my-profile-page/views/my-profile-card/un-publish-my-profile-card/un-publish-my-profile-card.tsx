// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { Text, Button, Switch } from "../../../../../../../components"
import { View } from "react-native"
import FastImage from "react-native-fast-image"
import { images } from "../../../../../../../theme/images"
import { MyStatusComponents } from "../../../../../../../components/my-status/my-status"
import * as DataUtils from "../../../../../../../utils/data.utils"
import { DudeStatus } from "../../../../../../../constants/app.constant"
import MyProfileController from "../../../my-profile.controllers"
import { UnPublishMyProfileProps } from "../../../my-profile.props"

// MARK: Style Import

import * as Styles from "./un-publish-my-profile-card.styles"


export const UnPublishMyProfileComponents: React.FunctionComponent<UnPublishMyProfileProps> = observer((props) => {
    const userStore = MyProfileController.rootStore?.getUserStore
    const isPublished: boolean = userStore?.status === DudeStatus.Published

    const profileImages = userStore?.getProfileImage
    const imageURI = profileImages?.length > 0 && profileImages[0]?.url
    const imageSource = imageURI && { uri: imageURI } || images.dudette

    return (
        <View shouldRasterizeIOS style={Styles.CONTAINNER}>
            <View shouldRasterizeIOS style={Styles.BODY_CONTAINNER}>
                <View shouldRasterizeIOS style={Styles.IMAGE_PROFILE_CONTAINNER}>
                    <FastImage source={imageSource}
                        {...MyProfileController.resourcesViewModel?.getTestIDImageProfile()}
                        style={Styles.IMAGE_PROFILE}
                        resizeMode={FastImage.resizeMode.contain} />
                </View>
                <View shouldRasterizeIOS style={Styles.BODY_CONTAINNER_TEXT}>
                    <View shouldRasterizeIOS style={Styles.CONTAINNER_TEXT}>
                        <Text
                            {...MyProfileController.resourcesViewModel?.getTestIDFullNameTitle()}
                            text={userStore?.getFullName}
                            style={Styles.TITLE}
                            numberOfLines={2}
                        />
                        <MyStatusComponents
                            rootStore={MyProfileController.rootStore}
                            testID={MyProfileController.resourcesViewModel?.getTestIDUserStatusTitle(userStore?.getStatus)}
                            status={userStore?.getStatus} />
                    </View>
                    <View shouldRasterizeIOS style={Styles.STATISTIC_COUNTER_CONTAINER}>
                        <View shouldRasterizeIOS style={Styles.LEFT_STATISTIC_COUNTER}>
                            <FastImage source={images.chatGrey} style={Styles.IMAGE_CHAT} resizeMode={FastImage.resizeMode.contain} />
                            <View style={Styles.INDICATOR_VIEW}>
                                <Text
                                    {...MyProfileController.resourcesViewModel?.getTestIDCommentCountTitle()}
                                    text={userStore?.getCommentCount?.toString()}
                                    style={Styles.INDICATOR_TEXT} numberOfLines={1} />
                                {userStore?.getUnReadCommentCount > 0 &&
                                    <Text
                                        {...MyProfileController.resourcesViewModel?.getTestIDUnreadCommentCountTitle()}
                                        text={"(+" + userStore?.getUnReadCommentCount + ")"}
                                        style={Styles.SUB_INDICATOR_TEXT}
                                        numberOfLines={1} />}
                            </View>
                        </View>
                        <View shouldRasterizeIOS style={Styles.RIGHT_STATISTIC_COUNTER}>
                            <FastImage source={images.view} style={Styles.IMAGE_VIEW} resizeMode={FastImage.resizeMode.contain} />
                            <Text
                                {...MyProfileController.resourcesViewModel?.getTestIDViewCountTitle()}
                                text={userStore?.getViewCount?.toString()}
                                style={Styles.INDICATOR_TEXT} numberOfLines={1} />
                        </View>
                    </View>
                </View>
                <View>
                    {isPublished && <Button
                        {...MyProfileController.resourcesViewModel?.getTestIDButtonEditProfileTitle()}
                        {...Styles.EDIT_BUTTON_PROPS}
                        onPress={props?.onEditPress} />}
                </View>
            </View>
            {!isPublished && <View shouldRasterizeIOS style={Styles.USER_DETAILS_ACTION_CONTAINNER}>
                <View shouldRasterizeIOS style={Styles.LEFT_STATISTIC_COUNTER}>
                    <Button
                        {...MyProfileController.resourcesViewModel?.getTestIDButtonPublishMyProfileTitle()}
                        text={MyProfileController.resourcesViewModel.getResourcePublishMyProfileTitle()}
                        {...Styles.PUBLISH_BUTTON_PROPS}
                        onPress={props?.onPublishPress} />
                </View>
                <View shouldRasterizeIOS style={Styles.RIGHT_STATISTIC_COUNTER}>
                    <Button
                        {...MyProfileController.resourcesViewModel?.getTestIDButtonEditProfileTitle()}
                        {...Styles.EDIT_UNPUBLISH_BUTTON_PROPS}
                        onPress={props?.onEditPress} />
                </View>
            </View>}
            <View shouldRasterizeIOS style={Styles.FOOTER_CONTAINNER}>
                <View shouldRasterizeIOS style={Styles.SEPARATOR_VIEW} />
                <View shouldRasterizeIOS style={Styles.FOOTER_CONTENT_CONTAINNER} >
                    <Switch
                        baseTrackStyle={Styles.TRACK}
                        baseThumbStyle={Styles.THUMB}
                        thumbOffStyle={Styles.THUMB_OFF_STYLE}
                        thumbOnStyle={Styles.THUMB_NO_STYLE}
                        trackOffStyle={Styles.TRACK_OFF_STYLE}
                        trackOnStyle={Styles.TRACK_ON_STYLE}
                        {...MyProfileController.resourcesViewModel?.getTestIDSwitchStatusSeekingGuyTitle()}
                        value={userStore?.getIsSeekingGuy}
                        onToggle={(value) => props?.onSeekingChange(value)} />
                    <Text
                        text={DataUtils.getSeekingGuyStatusByLocaleKey(MyProfileController.resourcesViewModel, userStore?.getIsSeekingGuy)}
                        style={Styles.SEEKING_GUY_STATUS_TEXT} />
                </View>
            </View>

        </View>
    )
})
