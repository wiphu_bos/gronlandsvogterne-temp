// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { Text, Icon, Button } from "../../../../../../../components"
import { View } from "react-native"
import { UnUpdateChildProps } from "../../../my-profile.props"
import MyProfileController from "../../../my-profile.controllers"

// MARK: Style Import

import * as Styles from "./un-update-my-profile-card.styles"

export const UnUpdateMyProfileComponents: React.FunctionComponent<UnUpdateChildProps> = observer((props) => {
    const userStore = MyProfileController.rootStore?.getUserStore
    return (
        <View shouldRasterizeIOS style={Styles.CONTAINNER}>
            <View shouldRasterizeIOS style={Styles.CONTAINNER_TEXT} >
                <Text text={userStore?.getFullName} style={Styles.TITLE} />
                <Text text={userStore?.getCountry?.value as string} style={Styles.SUB_TITLE} />
                <Text text={userStore?.getEmail} style={Styles.SUB_TITLE} />
            </View>
            <View shouldRasterizeIOS style={Styles.BODY_CONTAINNER} >
                <Icon source={Styles.DUDETTE_ICON} style={Styles.ICON} />
                <Text text={MyProfileController.resourcesViewModel?.getResourceUpdateDudetteProfileTitle()} style={Styles.DESCRIPTION} />
            </View>
            <Button {...Styles.EDIT_BUTTON_PROPS} onPress={props?.onEditPress} />
        </View>
    )
})
