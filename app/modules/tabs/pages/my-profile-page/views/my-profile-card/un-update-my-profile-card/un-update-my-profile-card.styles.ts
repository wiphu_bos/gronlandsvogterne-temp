import { ViewStyle, TextStyle, ImageStyle } from "react-native"
import * as metric from "../../../../../../../theme"
import { color } from "../../../../../../../theme"
import { RFValue } from "react-native-responsive-fontsize"
import { images } from "../../../../../../../theme/images"
import { ButtonProps } from "../../../../../../../components/button/button.props"

export const DUDETTE_ICON: string = images.dudette

const SHADOW: ViewStyle = {
    shadowColor: color.palette.black,
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.13,
    elevation: 4
}

export const CONTAINNER: ViewStyle = {
    width: metric.ratioWidth(376),
    height: metric.ratioHeight(209),
    borderRadius: metric.ratioWidth(14),
    backgroundColor: 'rgba(255,255,255,0.9)',
    marginTop: metric.ratioHeight(21),
    ...SHADOW
}

export const CONTAINNER_TEXT: ViewStyle = {
    paddingHorizontal: metric.ratioWidth(33),
    marginTop: metric.ratioHeight(15)
}

export const BODY_CONTAINNER: ViewStyle = {
    height: metric.ratioHeight(80),
    backgroundColor: color.palette.white,
    alignItems: 'center',
    marginTop: metric.ratioHeight(11),
    flexDirection: 'row'
}

const TEXT: TextStyle = {
    color: color.palette.black,
    fontFamily: "Montserrat",
}

export const TITLE: TextStyle = {
    ...TEXT,
    lineHeight: metric.ratioHeight(36),
    fontSize: RFValue(18),
    fontWeight: '400',

    //Android
    fontFamily: "Montserrat-Regular",
}

export const SUB_TITLE: TextStyle = {
    ...TEXT,
    lineHeight: metric.ratioHeight(24),
    fontSize: RFValue(13),
    fontWeight: '300',

    //Android
    fontFamily: "Montserrat-Light",
}

export const DESCRIPTION: TextStyle = {
    ...TEXT,
    lineHeight: metric.ratioHeight(27),
    fontSize: RFValue(12),
    fontWeight: '300',

    //Android
    fontFamily: "Montserrat-Light",
    color: color.palette.darkBrown,
    marginLeft: metric.ratioWidth(18.66)
}

export const ICON: ImageStyle = {
    width: metric.ratioWidth(36.34),
    height: metric.ratioHeight(40.46),
    marginLeft: metric.ratioWidth(33)
}

export const EDIT_BUTTON_PROPS: ButtonProps = {
    preset: "none",
    isAnimated: false,
    isSolid: true,
    imageSource: images.edit,
    imageStyle: {
        width: metric.ratioWidth(39),
        height: metric.ratioWidth(39),
    },
    containerStyle: {
        position: 'absolute',
        right: 0,
        bottom: 0,
        width: metric.ratioWidth(50),
        height: metric.ratioHeight(66),
    }
}