import { ViewStyle, TextStyle } from "react-native"
import * as metric from "../../../../../../theme"
import { color } from "../../../../../../theme"
import { RFValue } from "react-native-responsive-fontsize"
import { images } from "../../../../../../theme/images"
import { ButtonProps } from "../../../../../../components/button/button.props"

export const DUDETTE_ICON: string = images.dudette

const SHADOW: ViewStyle = {
    shadowColor: color.palette.black,
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.13,
    elevation: 4
}

export const CONTAINNER: ViewStyle = {
    marginTop: metric.ratioHeight(26),
}

export const OUTTER_CONTAINNER: ViewStyle = {
    flexDirection: 'row',
    alignItems: 'center',
    width: metric.ratioWidth(376),
    height: metric.ratioHeight(82),
    borderRadius: metric.ratioWidth(14),
    backgroundColor: color.palette.white,
    marginTop: metric.ratioHeight(10),
    ...SHADOW
}

export const INNER_CONTAINNER: ViewStyle = {
    flex: 1,
    justifyContent: 'center',
}

export const CONTAINNER_TEXT: ViewStyle = {
    flexDirection: 'row',
    marginLeft: metric.ratioWidth(33),
    width: metric.ratioWidth(300),
}
const TEXT: TextStyle = {
    color: color.palette.black,
    fontFamily: "Montserrat",
}

export const TITLE: TextStyle = {
    ...TEXT,
    lineHeight: metric.ratioHeight(24),
    fontSize: RFValue(19),
    fontWeight: '500',

    //Android
    fontFamily: "Montserrat-SemiBold",
    color: color.palette.white
}

export const SUB_TITLE: TextStyle = {
    ...TEXT,
    lineHeight: metric.ratioHeight(27),
    fontSize: RFValue(13),
    fontWeight: '300',

    //Android
    fontFamily: "Montserrat-Light",
}

export const DESCRIPTION: TextStyle = {
    ...SUB_TITLE,
    marginLeft: metric.ratioWidth(18.66)
}

const SUBSCRIPTION_VALUE: TextStyle = {
    ...SUB_TITLE,
    marginLeft: metric.ratioWidth(10)
}

export const EXPIRYDATE_VALUE: TextStyle = {
    ...SUBSCRIPTION_VALUE,
}

export const STATUS_VALUE: TextStyle = {
    ...SUBSCRIPTION_VALUE
}

export const ACTIVE_STATUS: TextStyle = {
    color: color.palette.green
}

export const DEACTIVE_STATUS: TextStyle = {
    color: color.palette.darkGrey
}

export const CANCEL: TextStyle = {
    ...TEXT,
    lineHeight: metric.ratioHeight(22),
    fontSize: RFValue(14),
    fontWeight: '400',

    //Android
    fontFamily: "Montserrat-Regular",
    color: color.palette.red
}

export const MENU_BUTTON_PROPS: ButtonProps = {
    preset: "none",
    imageSource: images.threeDotsMenu,
    imageStyle: {
        width: metric.ratioWidth(4.38),
        height: metric.ratioHeight(20.78),
        alignSelf: 'center'
    },
    containerStyle: {
        justifyContent: 'center',
        width: metric.ratioWidth(40),
        height: metric.ratioHeight(60)
    },
    isAnimated: false
}

export const POPOVER_MENU: ViewStyle = {
    justifyContent: 'center',
    position: 'absolute',
    right: -metric.ratioWidth(10),
    bottom: metric.isNewIPhone ? '43%' : '43.6%'
}

export const MODAL_OPTIONS: any = {
    isVisible: true,
    backdropColor: color.clear,
    animationIn: "fadeIn",
    animationOut: "fadeOut"
}