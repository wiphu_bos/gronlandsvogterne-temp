// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { Text, Button } from "../../../../../../components"
import { View, TextStyle } from "react-native"
import Modal from "../../../../../../libs/react-native-modal"
import { MenuPopover } from "../../../../../../components/popover-menu/popover-menu"
import { PopoverMenuActionType, PopoverMenuItem } from "../../../../../../components/popover-menu/popover-menu.props"
import { SubscriptionProps } from "../../my-profile.props"
import MyProfileController from "../../my-profile.controllers"
import moment from "moment"

// MARK: Style Import
import * as Styles from "./subscription-card.styles"

export const SubscriptionComponents: React.FunctionComponent<SubscriptionProps> = observer((props) => {
    const userStore = MyProfileController.rootStore?.getUserStore
    const isSubscribed: boolean = userStore?.getIsSubscription
    const isExpired: boolean = userStore?.getExpiryDateCount <= 0
    const statusValue: string = isSubscribed && !isExpired ?
        MyProfileController.resourcesViewModel?.getResourceSubscriptionActiveStatusTitle() :
        MyProfileController.resourcesViewModel?.getResourceSubscriptionDeactiveStatusTitle()
    const statusValueStyle: TextStyle = isSubscribed ? Styles.ACTIVE_STATUS : Styles.DEACTIVE_STATUS
    const expiryDate = isSubscribed ? moment.duration(userStore?.getExpiryDateCount).format("D[d] H[h] m[m] s[s]", { trim: "both final" }) : "-"

    const menuItem: PopoverMenuItem = isSubscribed ?
        {
            title: MyProfileController?.resourcesViewModel?.getResourceDropDownCancelSubscriptionTitle(),
            type: PopoverMenuActionType.CancelSubsciption,
            titleStyle: Styles.CANCEL
        } :
        {
            title: MyProfileController?.resourcesViewModel?.getResourceDropDownMakeSubscriptionTitle(),
            type: PopoverMenuActionType.Subsciption
        }

    return (
        <View shouldRasterizeIOS style={Styles.CONTAINNER}>
            <Text text={MyProfileController.resourcesViewModel?.getResourceSubscriptionTitle()} style={Styles.TITLE} />
            <View shouldRasterizeIOS style={Styles.OUTTER_CONTAINNER}>
                <View shouldRasterizeIOS style={Styles.INNER_CONTAINNER}>
                    <View shouldRasterizeIOS style={Styles.CONTAINNER_TEXT}>
                        <Text text={MyProfileController.resourcesViewModel?.getResourceStatusTitle()} style={Styles.SUB_TITLE} />
                        <Text text={statusValue} style={[Styles.STATUS_VALUE, statusValueStyle]} />
                    </View>
                    <View shouldRasterizeIOS style={Styles.CONTAINNER_TEXT}>
                        <Text text={MyProfileController.resourcesViewModel?.getResourceExpireDateTitle()} style={Styles.SUB_TITLE} />
                        <Text text={expiryDate} style={Styles.EXPIRYDATE_VALUE} />
                    </View>

                </View>
                <Button {...Styles.MENU_BUTTON_PROPS} onPress={props?.controller?.onTogglePopoverMenu} />
                {MyProfileController.viewModel?.getIsPopoverMenuVisible ?
                    <Modal {...Styles.MODAL_OPTIONS}
                        onBackdropPress={props?.controller?.onPopoverMenuBackdropPressed}>
                        <View shouldRasterizeIOS style={Styles.POPOVER_MENU}>
                            <MenuPopover items={[menuItem]}
                                onPress={props?.controller?.onPopoverMenuItemPressed} />
                        </View>
                    </Modal> : null}
            </View>
        </View>
    )
})
