// MARK: Import
import { NavigationKey, UserType } from "../../../../constants/app.constant"
import { MyProfileProps, UnUpdateChildProps, UnPublishMyProfileProps, SubscriptionProps } from "./my-profile.props"
import { MyProfileResourcesStoreModel, MyProfileStoreModel } from "./storemodels/my-profile.store"
import { MyProfileResourcesStore, MyProfileStore } from "./storemodels/my-profile.types"
import { RootStore, INavigationRoute } from "../../../../models"
import { PopoverMenuActionType } from "../../../../components/popover-menu/popover-menu.props"
import { UserStore, IUser } from "../../../../models/user-store"
import { BaseController, IAlertParams } from "../../../base.controller"
import { onSnapshot } from "mobx-state-tree"
import { CommonActions } from "@react-navigation/core"
import { ValidationServices } from "../../../../services/api-domain/validate.services"
import * as DataUtils from "../../../../utils/data.utils"


class MyProfileController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static resourcesViewModel: MyProfileResourcesStore
    public static viewModel: MyProfileStore
    private static _popoverMenuActionTypeSelected: PopoverMenuActionType

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, props?: MyProfileProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, props, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(props)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        MyProfileController.resourcesViewModel = MyProfileResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.MyProfile)
        MyProfileController.viewModel = localStore && MyProfileStoreModel.create({ ...localStore }) || MyProfileStoreModel.create({})
    }

    private _setupSnapShot = () => {
        onSnapshot(MyProfileController.viewModel, (snap: MyProfileStore) => {
            this._setInitializedToPropsParams()
        })
    }

    /*
       Mark Data
    */

    /*
       Mark Event
    */

    private _isLoading = (value: boolean) => MyProfileController.viewModel?.setIsPullRefreshing(value)

    private _onOpenModal = () => MyProfileController.viewModel?.setIsModalShowing(true)
    private _onCloseModal = () => MyProfileController.viewModel?.setIsModalShowing(false)

    public onPublish = async () => {
        await this._askForPublish(true)
    }

    public onSave = async () => {
        await this._askForPublish(false)
    }

    private _askForPublish = async (isPublish: boolean) => {
        this._onCloseModal()
        const userId = this._rootStore?.getUserStore.getUserId
        this._isGlobalLoading(true)
        await MyProfileController.viewModel?.askForPublish(this._rootStore, userId, isPublish)
        this._isGlobalLoading(false)
        this._onCloseModal()
    }

    private _updateSeekingGuy = (data: IUser) => {
        const userProfile = DataUtils.getUserProfileObject(this._rootStore, this._rootStore?.getUserStore, data)
        this._rootStore?.getUserStore?.bindingDataFromObject(userProfile, UserType.Me)
    }

    public onRefresh = async () => {
        this._isLoading(true)
        // viewModel?.resetFetching()
        await MyProfileController.viewModel.fetchMyProfile(this._rootStore)
        this._isLoading(false)
        this._isGlobalLoading(false)
        // didFirstFetchData = true
    }


    public onEditPress = () => {
        const { navigation } = this._myProps
        navigation.dispatch(CommonActions.navigate(NavigationKey.UserProfileDetailTab) as any)
    }

    public onPublishPress = () => {
        this._validateData()
        if (!MyProfileController.viewModel?.getAllUserCredentialsValid) {
            const params: IAlertParams = {
                title: MyProfileController.resourcesViewModel?.getResourceAlertValidateUserTitle(),
                message: MyProfileController.resourcesViewModel?.getResourceAlertValidateUserMessage()
            }
            this._generalAlertOS(params)
        } else {
            this._onOpenModal()
        }
    }

    public onSeekingChange = async (value: boolean) => {
        this._isGlobalLoading(true)
        const result = await MyProfileController.viewModel?.updateSeekingGuy(this._rootStore, value)
        this._isGlobalLoading(false)
        const data: IUser = result?.data
        if (!data) {
            const params = {
                rootStore: this._rootStore,
                error: result
            }
            const [isError, message] = new ValidationServices(params).premiumAccount()
            if (isError) this._delayExecutor(() => this._myProps?.navigation?.dispatch(CommonActions.navigate(NavigationKey.SubscriptionConfirmation,
                { onSuccess: () => MyProfileController.viewModel?.updateSeekingGuy(this._rootStore, value) }) as any))
        } else {
            this._updateSeekingGuy(data)
        }
    }

    public onPopoverMenuBackdropPressed = () => {
        MyProfileController._popoverMenuActionTypeSelected = null
        this.forceClosePopoverMenu()
    }

    public onPopoverMenuItemPressed = (type?: PopoverMenuActionType) => {
        MyProfileController._popoverMenuActionTypeSelected = type
        this.forceClosePopoverMenu()

        /* Work arround for setting timeoout because `onPopoverClosed` is not working*/
        this._delayExecutor(async () => await this._onSelectedPopoverMenuItem())
    }

    public onTogglePopoverMenu = () => {
        const { getIsPopoverMenuVisible } = MyProfileController.viewModel
        MyProfileController.viewModel?.setIsPopoverMenuVisible(!getIsPopoverMenuVisible)
    }

    /* not working, will solve later */
    public onPopoverClosed = () => {
        // onOpenAlertModal()
    }

    public _onSelectedPopoverMenuItem = async () => {
        switch (MyProfileController._popoverMenuActionTypeSelected) {
            case PopoverMenuActionType.Subsciption:
                this._myProps?.navigation?.dispatch(CommonActions.navigate(NavigationKey.SubscriptionConfirmation, {
                    skipConfirmation: true
                }) as any)
                break
            case PopoverMenuActionType.CancelSubsciption:
                await MyProfileController.viewModel?.cancelSubscribePremiumAccount(this._rootStore)
                break
            default:
                break
        }
        MyProfileController._popoverMenuActionTypeSelected = null
    }

    public forceClosePopoverMenu = () => MyProfileController.viewModel?.setIsPopoverMenuVisible(false)

    //@override
    backProcess = () => {
    }

    public childProps: MyProfileProps = {
        onEditPress: this.onEditPress
    }

    public unUpdateChildProps: UnUpdateChildProps = {
        ...this.childProps
    }

    public unPublishChildProps: UnPublishMyProfileProps = {
        ...this.childProps,
        onPublishPress: this.onPublishPress,
        onSeekingChange: this.onSeekingChange
    }

    public subscriptionChildProps: SubscriptionProps = {
        controller: this
    }

    /*
       Mark Life cycle
    */

    //@override
    viewDidAppearOnce = async () => {
        // this._isGlobalLoading(true)
        // await this.onRefresh()
    }

    //@override
    viewWillDisappear = () => {
        this.forceClosePopoverMenu()
    }

    /*
        Helper
    */

    private _validateData = () => {
        const { getEmail, getAge, getCity, getDescription, getFullName,
            getGalleryImages, getHavdSogerHanList, getInterestList, getProfileImage, getTagList } = this._rootStore?.getUserStore
        MyProfileController.viewModel?.setIsValid(
            getEmail &&
                getAge?.value &&
                getCity?.value &&
                getDescription &&
                getFullName &&
                getGalleryImages?.length > 0 &&
                getProfileImage?.length > 0 &&
                getHavdSogerHanList?.length > 0 &&
                getInterestList?.length > 0 &&
                getTagList?.length >= 3 ? true : false
        )
    }

}

export default MyProfileController