import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import MyProfileController from './my-profile.controllers'
export interface MyProfileProps extends NavigationContainerProps<ParamListBase> {
    onEditPress?: () => void
}

export interface UnPublishMyProfileProps extends MyProfileProps {
    onPublishPress?: () => void
    onSeekingChange?: (value: boolean) => void
}

export interface SubscriptionProps extends MyProfileProps {
    controller?: MyProfileController
}
export interface UnUpdateChildProps extends MyProfileProps {

}