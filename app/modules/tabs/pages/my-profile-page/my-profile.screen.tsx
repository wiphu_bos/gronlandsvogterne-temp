// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { MyProfileProps } from "./my-profile.props"
import MyProfileController from "./my-profile.controllers"
import { useConfigurate } from "../../../../custom-hooks/use-configure-controller"
import LinearGradient from "react-native-linear-gradient"
import { SubscriptionComponents } from "./views/subscription-card/subscription-card"
import { UnUpdateMyProfileComponents } from "./views/my-profile-card/un-update-my-profile-card/un-update-my-profile-card"
import { UnPublishMyProfileComponents } from "./views/my-profile-card/un-publish-my-profile-card/un-publish-my-profile-card"
import { RefreshControl } from "react-native"
import { AnimatedScrollView, ConfirmModal } from "../../../../components"

// MARK: Style Import

import * as Styles from "./my-profile.styles"

export const MyProfileScreen: React.FunctionComponent<MyProfileProps> = observer((props) => {

    const controller = useConfigurate(MyProfileController, props) as MyProfileController

    const renderedCard = () => {

        return MyProfileController.rootStore?.getUserStore?.getStatus ?
            <UnPublishMyProfileComponents
                {...controller.unPublishChildProps}
            />
            : <UnUpdateMyProfileComponents
                {...controller.unUpdateChildProps}
            />
    }


    // MARK: Render
    return (
        <LinearGradient shouldRasterizeIOS style={Styles.CONTAINNER} {...Styles.TAB_PAGE_BACKGROUND}>
            <AnimatedScrollView contentContainerStyle={Styles.SCROLL_CONTAINNER}
                showsVerticalScrollIndicator={false}
                refreshControl={
                    <RefreshControl style={Styles.REFRESH_CONTROL}
                        refreshing={false}
                        onRefresh={controller.onRefresh} />
                }
            >
                {renderedCard()}
                <SubscriptionComponents
                    {...controller.subscriptionChildProps} />
                <ConfirmModal
                    {...MyProfileController.resourcesViewModel?.getTestIDConfirmModal()}
                    okButtonText={MyProfileController.resourcesViewModel?.getResourceModalButtonOKTitle()}
                    clearButtonText={MyProfileController.resourcesViewModel?.getResourceModalButtonCancelTitle()}
                    message={MyProfileController.resourcesViewModel?.getResourceModalConfirmPublishProfile()}
                    isVisible={MyProfileController.viewModel?.getIsModalShowing || false}
                    onCancel={controller.onSave}
                    onOK={controller.onPublish} />
            </AnimatedScrollView>
        </LinearGradient>

    )
})
