import { types } from "mobx-state-tree"
import { MyProfilePropsModel } from "./my-profile.models"
import { GeneralResources } from "../../../../../constants/firebase/remote-config"
import { NavigationKey } from "../../../../../constants/app.constant"
import { TestIDResources } from "../../../../../constants/test-key/test.constant"
import * as Utils from "../../../../../utils"
import { GeneralResourcesStoreModel } from "../../../../../models/general-resources-store"

export const MyProfileViews = types.model(MyProfilePropsModel)
    .views(self => ({
        get getIsPopoverMenuVisible() {
            return self.isPopoverMenuVisible
        },
        get getIsPullRefreshing() {
            return self.isPullRefreshing
        },
        get getIsModalShowing() {
            return self.isModalShowing
        }
    }))

export const MyProfileResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State

    const { MyProfileScreen } = GeneralResources
    const { subscriptionTitle,
        statusTitle,
        expireDateTitle,
        subscriptionActiveStatusTitle,
        subscriptionDeactiveStatusTitle,
        dropdownMakeSubscriptionTitle,
        dropdownCancelSubscriptionTitle,
        updateDudetteProfileTitle,
        publishMyProfileTitle,
        modalButtonOKTitle,
        modalButtonCancelTitle,
        modalConfirmPublishProfile,
        alertValidateUserTitle,
        alertValidateUserMessage } = MyProfileScreen
    const { MyProfile } = NavigationKey

    //MARK: Views
    const getResources = (key: string, childKeyOrShareKey: string | boolean = false) => self.getValues(childKeyOrShareKey ? key : MyProfile, childKeyOrShareKey ? true : key)
    const getResourceSubscriptionTitle = () => getResources(subscriptionTitle)
    const getResourceExpireDateTitle = () => getResources(expireDateTitle, true)
    const getResourceStatusTitle = () => getResources(statusTitle, true)
    const getResourceSubscriptionActiveStatusTitle = () => getResources(subscriptionActiveStatusTitle, true)
    const getResourceSubscriptionDeactiveStatusTitle = () => getResources(subscriptionDeactiveStatusTitle, true)
    const getResourceDropDownMakeSubscriptionTitle = () => getResources(dropdownMakeSubscriptionTitle, true)
    const getResourceDropDownCancelSubscriptionTitle = () => getResources(dropdownCancelSubscriptionTitle, true)
    const getResourceUpdateDudetteProfileTitle = () => getResources(updateDudetteProfileTitle, true)
    const getResourcePublishMyProfileTitle = () => getResources(publishMyProfileTitle, true)
    const getResourceModalButtonOKTitle = () => getResources(modalButtonOKTitle,true)
    const getResourceModalButtonCancelTitle = () => getResources(modalButtonCancelTitle,true)
    const getResourceModalConfirmPublishProfile = () => getResources(modalConfirmPublishProfile,true)
    const getResourceAlertValidateUserTitle = () => getResources(alertValidateUserTitle,true)
    const getResourceAlertValidateUserMessage = () => getResources(alertValidateUserMessage,true)

    return {
        getResourceSubscriptionTitle,
        getResourceExpireDateTitle,
        getResourceStatusTitle,
        getResourceSubscriptionActiveStatusTitle,
        getResourceSubscriptionDeactiveStatusTitle,
        getResourceDropDownMakeSubscriptionTitle,
        getResourceDropDownCancelSubscriptionTitle,
        getResourceUpdateDudetteProfileTitle,
        getResourcePublishMyProfileTitle,
        getResourceModalButtonOKTitle,
        getResourceModalButtonCancelTitle,
        getResourceModalConfirmPublishProfile,
        getResourceAlertValidateUserTitle,
        getResourceAlertValidateUserMessage
    }
})
    .views(self => {

        //MARK: Volatile State

        //MARK: Views

        const getTestIDSubscriptionTitle = () => Utils.getTestIDObject(TestIDResources.MyProfileScreen.subscription)
        const getTestIDStatusTitle = () => Utils.getTestIDObject(TestIDResources.MyProfileScreen.status)
        const getTestIDExpireDateTitle = () => Utils.getTestIDObject(TestIDResources.MyProfileScreen.expireDate)
        const getTestIDSubscriptionActiveStatusTitle = () => Utils.getTestIDObject(TestIDResources.MyProfileScreen.subscriptionActiveStatus)
        const getTestIDSubscriptionDeactiveStatusTitle = () => Utils.getTestIDObject(TestIDResources.MyProfileScreen.subscriptionDeactiveStatus)
        const getTestIDDropDownMakeSubscriptionTitle = () => Utils.getTestIDObject(TestIDResources.MyProfileScreen.dropdownMakeSubscription)
        const getTestIDDropDownCancelSubscriptionTitle = () => Utils.getTestIDObject(TestIDResources.MyProfileScreen.dropdownCancelSubscription)
        const getTestIDPublishStatusTitle = () => Utils.getTestIDObject(TestIDResources.MyProfileScreen.publishStatus)
        const getTestIDUnPublishStatusTitle = () => Utils.getTestIDObject(TestIDResources.MyProfileScreen.unPublishStatus)
        const getTestIDUpdateDudetteProfileTitle = () => Utils.getTestIDObject(TestIDResources.MyProfileScreen.updateDudetteProfile)
        const getTestIDImageProfile = () => Utils.getTestIDObject(TestIDResources.MyProfileScreen.imageProfile)
        const getTestIDFullNameTitle = () => Utils.getTestIDObject(TestIDResources.MyProfileScreen.fullName)
        const getTestIDExpireInTitle = () => Utils.getTestIDObject(TestIDResources.MyProfileScreen.expireIn)
        const getTestIDButtonEditProfileTitle = () => Utils.getTestIDObject(TestIDResources.MyProfileScreen.buttonEditProfile)
        const getTestIDCommentCountTitle = () => Utils.getTestIDObject(TestIDResources.MyProfileScreen.commentCountTitle)
        const getTestIDUnreadCommentCountTitle = () => Utils.getTestIDObject(TestIDResources.MyProfileScreen.unreadCommentCountTitle)
        const getTestIDViewCountTitle = () => Utils.getTestIDObject(TestIDResources.MyProfileScreen.viewCountTitle)
        const getTestIDUserStatusTitle = (status: string) => Utils.getTestIDObject(status)
        const getTestIDButtonPublishMyProfileTitle = () => Utils.getTestIDObject(TestIDResources.MyProfileScreen.buttonPublishMyProfile)
        const getTestIDSwitchStatusSeekingGuyTitle = () => Utils.getTestIDObject(TestIDResources.MyProfileScreen.switchStatusSeekingGuy)
        const getTestIDConfirmModal = () => Utils.getTestIDObject(TestIDResources.PrimaryInformationFourthScreen.confirmModal)

        return {
            getTestIDSubscriptionTitle,
            getTestIDStatusTitle,
            getTestIDExpireDateTitle,
            getTestIDSubscriptionActiveStatusTitle,
            getTestIDSubscriptionDeactiveStatusTitle,
            getTestIDDropDownMakeSubscriptionTitle,
            getTestIDDropDownCancelSubscriptionTitle,
            getTestIDPublishStatusTitle,
            getTestIDUnPublishStatusTitle,
            getTestIDUpdateDudetteProfileTitle,
            getTestIDImageProfile,
            getTestIDFullNameTitle,
            getTestIDExpireInTitle,
            getTestIDButtonEditProfileTitle,
            getTestIDCommentCountTitle,
            getTestIDUnreadCommentCountTitle,
            getTestIDViewCountTitle,
            getTestIDUserStatusTitle,
            getTestIDButtonPublishMyProfileTitle,
            getTestIDSwitchStatusSeekingGuyTitle,
            getTestIDConfirmModal
        }
    })
