import { types } from "mobx-state-tree"
import { MyProfilePropsModel } from "./my-profile.models"

export const MyProfileActions = types.model(MyProfilePropsModel).actions(self => {

    //MARK: Volatile State

    //MARK: Actions
    
    const setIsPopoverMenuVisible = (value: boolean) => self.isPopoverMenuVisible = value
    const setIsPullRefreshing = (value: boolean) => self.isPullRefreshing = value
    const setIsModalShowing = (value: boolean) => self.isModalShowing = value
    return {
        setIsPopoverMenuVisible,
        setIsPullRefreshing,
        setIsModalShowing
    }
})