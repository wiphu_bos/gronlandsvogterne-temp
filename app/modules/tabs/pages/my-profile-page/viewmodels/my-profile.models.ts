import { types } from "mobx-state-tree"

export const MyProfilePropsModel = {
    isPopoverMenuVisible: types.optional(types.boolean,false),
    isPullRefreshing: types.optional(types.boolean, false),
    isModalShowing: types.optional(types.boolean,false)
} 