import { types } from "mobx-state-tree"
import { StoreName } from "../../../../../constants/app.constant"
import { MyProfilePropsModel } from "../viewmodels/my-profile.models"
import { MyProfileActions } from "../viewmodels/my-profile.actions"
import { MyProfileViews, MyProfileResourcesViews } from "../viewmodels/my-profile.views"
import { GeneralResourcesStoreModel } from "../../../../../models/general-resources-store"
import { MyProfileServiceActions } from "../services/my-profile.services"
import { ValidationStoreModel } from "../../../../../models/validation-store"

const MyProfileModel = types.model(StoreName.MyProfile, MyProfilePropsModel)
export const MyProfileStoreModel = types.compose(MyProfileModel,MyProfileViews,MyProfileActions,MyProfileServiceActions,ValidationStoreModel)
export const MyProfileResourcesStoreModel = types.compose(GeneralResourcesStoreModel, MyProfileResourcesViews)

