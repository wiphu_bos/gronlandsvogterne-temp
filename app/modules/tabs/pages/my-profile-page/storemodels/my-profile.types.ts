import { Instance, SnapshotOut } from "mobx-state-tree"
import { MyProfileStoreModel, MyProfileResourcesStoreModel } from "./my-profile.store"

export type MyProfileStore = Instance<typeof MyProfileStoreModel>
export type MyProfileStoreSnapshot = SnapshotOut<typeof MyProfileStoreModel>

export type MyProfileResourcesStore = Instance<typeof MyProfileResourcesStoreModel>
export type MyProfileResourcesStoreSnapshot = SnapshotOut<typeof MyProfileResourcesStoreModel>