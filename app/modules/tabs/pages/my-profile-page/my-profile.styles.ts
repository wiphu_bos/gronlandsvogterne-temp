import { ViewStyle } from "react-native"
import { color } from "../../../../theme"
import * as metric from "../../../../theme"

export const FULL: ViewStyle = {
    flex: 1
}

export const TAB_PAGE_BACKGROUND: any = { colors: color.tabPageBackground }

export const CONTAINNER: ViewStyle = {
    ...FULL
}

export const SCROLL_CONTAINNER: ViewStyle = {
    ...FULL,
    alignItems: 'center',
}

export const REFRESH_CONTROL: any = { tintColor: color.brightDim }