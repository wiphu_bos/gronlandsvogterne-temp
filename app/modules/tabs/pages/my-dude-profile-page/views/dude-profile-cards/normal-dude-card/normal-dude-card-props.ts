import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { MyDudeProfileProps } from '../../../my-dude-profile.props'
import { ExtractCSTWithSTN } from 'mobx-state-tree/dist/core/type/type'
import { UserModel } from '../../../../../../../models/user-store/user.store'

export interface NormalDudeProfileCardProps extends NavigationContainerProps<ParamListBase>, MyDudeProfileProps {
    onDeletePress?: (dudeProfile?: ExtractCSTWithSTN<typeof UserModel>) => void
}