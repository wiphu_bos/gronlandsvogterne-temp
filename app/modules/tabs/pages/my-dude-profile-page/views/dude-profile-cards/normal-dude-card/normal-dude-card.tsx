// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { Text, Button, StatisticCounterComponents } from "../../../../../../../components"
import { images } from "../../../../../../../theme/images"
import FastImage from "react-native-fast-image"
import { View } from "react-native"
import { UserWarningComponents } from "../../dude-profile-components/update-profile-warning/update-profile-warning"
import { NormalDudeProfileCardProps } from "./normal-dude-card-props"
import { DudeStatus } from "../../../../../../../constants/app.constant"
import { MyStatusComponents } from "../../../../../../../components/my-status/my-status"
import MyDudeProfileController from "../../../my-dude-profile.controllers"

// MARK: Style Import

import * as Styles from "./normal-dude-card.styles"

export const NormalDudeProfileComponents: React.FunctionComponent<NormalDudeProfileCardProps> = observer((props) => {

    const renderCardDetailsByUserStatus = (status?: string) => {
        const indicatorView = () => {
            if (status === DudeStatus.Approved) {
                return <UserWarningComponents text={MyDudeProfileController.resourcesViewModel?.getResourceUpdateDudeProfileTitle()} />
            } else if (status === DudeStatus.WaitingForApproval) {
                return <Button
                    {...Styles.DELETE_BUTTON_PROPS}
                    text={MyDudeProfileController.resourcesViewModel?.getResourceDeleteTitle()}
                    {...MyDudeProfileController.resourcesViewModel?.getTestIDButtonDeleteTitle()}
                    onPress={() => props?.onDeletePress(props?.dudeProfile$)}
                />
            } else {
                return (<StatisticCounterComponents
                    primaryCountTestID={MyDudeProfileController.resourcesViewModel?.getTestIDCommentCountTitle}
                    unreadPrimaryCountTestID={MyDudeProfileController.resourcesViewModel?.getTestIDUnreadCommentCountTitle}
                    viewCountTestID={MyDudeProfileController.resourcesViewModel?.getTestIDViewCountTitle}
                    viewCount={props?.dudeProfile$?.view_count}
                    primaryCount={props?.dudeProfile$?.comment_count}
                    unreadPrimaryCount={props?.dudeProfile$?.unread_comment_count}
                    isHighlight={props?.dudeProfile$.is_highlight}
                />)
            }
        }
        const statusView = <MyStatusComponents
            {...MyDudeProfileController.resourcesViewModel?.getTestIDUserStatusTitle(status)}
            status={status}
            rootStore={MyDudeProfileController.rootStore} />
        return (
            <View>
                {statusView}
                {indicatorView()}
            </View>)
    }

    return (
        <View shouldRasterizeIOS style={Styles.ROOT_CONTAINNER}>
            <View shouldRasterizeIOS style={Styles.WRAPPER_CONTAINNER}>
                <View shouldRasterizeIOS style={Styles.BODY_HIGHLIGHT_CONTAINNER}>
                    <View shouldRasterizeIOS style={Styles.IMAGE_PROFILE_CONTAINNER}>
                        <FastImage
                            {...MyDudeProfileController.resourcesViewModel?.getTestIDImageProfile()}
                            source={props.dudeProfile$?.profile_image ? { uri: props.dudeProfile$?.profile_image[0]?.url } : images.avatarDude}
                            style={Styles.IMAGE_PROFILE} />
                    </View>
                    <View shouldRasterizeIOS style={Styles.BODY_CONTAINNER_TEXT}>
                        <View shouldRasterizeIOS style={Styles.CONTAINNER_TEXT}>
                            <Text
                                {...MyDudeProfileController.resourcesViewModel?.getTestIDFullNameTitle()}
                                text={props?.dudeProfile$?.full_name}
                                style={Styles.TITLE} />
                            {renderCardDetailsByUserStatus(props?.dudeProfile$?.status)}
                        </View>
                    </View >
                </View >
            </View >
            <Button
                {...MyDudeProfileController.resourcesViewModel?.getTestIDButtonEditProfileTitle()}
                {...Styles.EDIT_BUTTON_PROPS}
                onPress={() => props?.onPress(props?.dudeProfile$)} />
        </View >
    )
})
