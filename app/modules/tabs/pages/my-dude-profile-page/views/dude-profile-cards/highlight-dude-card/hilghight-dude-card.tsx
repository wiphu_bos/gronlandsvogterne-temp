// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { Text, Button, StatisticCounterComponents } from "../../../../../../../components"
import { images } from "../../../../../../../theme/images"
import { View } from "react-native"
import FastImage from "react-native-fast-image"
import { HighlightDudeCardProps } from "./highlight-dude-card-props"
import { MyStatusComponents } from "../../../../../../../components/my-status/my-status"
import MyDudeProfileController from "../../../my-dude-profile.controllers"
import moment from "moment"
import momentDurationFormatSetup from "moment-duration-format"

// MARK: Style Import

import * as Styles from "./highlight-dude-card.styles"


export const HighlightDudeProfileComponents: React.FunctionComponent<HighlightDudeCardProps> = observer((props) => {
    momentDurationFormatSetup(moment)
    return (
        <View shouldRasterizeIOS style={Styles.HIGHLIGHT_CONTAINNER}>
            <View shouldRasterizeIOS style={Styles.WRAPPER_CONTAINNER}>
                <View shouldRasterizeIOS style={Styles.BODY_HIGHLIGHT_CONTAINNER}>
                    <View shouldRasterizeIOS style={Styles.IMAGE_PROFILE_CONTAINNER}>
                        <FastImage source={props.dudeProfile$?.profile_image ? { uri: props.dudeProfile$?.profile_image[0]?.url } : images.avatarDude}
                            {...MyDudeProfileController.resourcesViewModel?.getTestIDImageProfile()}
                            style={Styles.IMAGE_PROFILE}
                            resizeMode={FastImage.resizeMode.contain} />
                    </View>
                    <View shouldRasterizeIOS style={Styles.BODY_HIGHLIGHT_CONTAINNER_TEXT}>
                        <View shouldRasterizeIOS style={Styles.CONTAINNER_TEXT}>
                            <Text
                                {...MyDudeProfileController.resourcesViewModel?.getTestIDFullNameTitle()}
                                text={props?.dudeProfile$?.full_name}
                                style={Styles.TITLE} />
                            <MyStatusComponents
                                rootStore={MyDudeProfileController.rootStore}
                                testID={MyDudeProfileController.resourcesViewModel?.getTestIDUserStatusTitle(props?.dudeProfile$?.status)}
                                status={props?.dudeProfile$?.status} />
                        </View>
                        <StatisticCounterComponents
                            primaryCountTestID={MyDudeProfileController.resourcesViewModel?.getTestIDCommentCountTitle}
                            unreadPrimaryCountTestID={MyDudeProfileController.resourcesViewModel?.getTestIDUnreadCommentCountTitle}
                            viewCountTestID={MyDudeProfileController.resourcesViewModel?.getTestIDViewCountTitle}
                            viewCount={props?.dudeProfile$?.view_count}
                            primaryCount={props?.dudeProfile$?.comment_count}
                            unreadPrimaryCount={props?.dudeProfile$?.unread_comment_count}
                            isHighlight={props?.dudeProfile$.is_highlight}
                        />
                    </View>
                </View>
                <Button
                    {...MyDudeProfileController.resourcesViewModel?.getTestIDButtonEditProfileTitle()}
                    {...Styles.EDIT_BUTTON_PROPS}
                    onPress={() => props?.onPress(props?.dudeProfile$)} />
            </View>
            <View shouldRasterizeIOS style={Styles.FOOTER_HIGHLIGHT_CONTAINNER}>
                <View shouldRasterizeIOS style={Styles.SEPARATOR_VIEW} />
                <View shouldRasterizeIOS style={Styles.FOOTER_CONTENT_CONTAINNER} >
                    <View shouldRasterizeIOS style={Styles.HIGHTLIGHT_VIEW} >
                        <Text
                            {...MyDudeProfileController.resourcesViewModel?.getTestIDHighLightStatusTitle()}
                            text={MyDudeProfileController.resourcesViewModel?.getResourceHightLightStatusTitle()}
                            style={Styles.SMALL_TITLE} />
                    </View>
                    <View shouldRasterizeIOS style={Styles.EXPIRE_VIEW} >
                        <Text
                            {...MyDudeProfileController.resourcesViewModel?.getTestIDExpireInTitle()}
                            text={MyDudeProfileController.resourcesViewModel?.getResourceExpireInTitle()
                                + " " +
                                //moment.duration(props?.dudeProfile$?.expiryDateCount).format("D[d] H[h] m[m]",{trim : "both final"})
                                moment.duration(props?.dudeProfile$?.expiryDateCount).format("D[d] H[h] m[m] s[s]",{trim : "both final"})
                            }
                            style={Styles.EXPIRE_TEXT} />
                    </View>
                </View>
            </View>

        </View>
    )
})
