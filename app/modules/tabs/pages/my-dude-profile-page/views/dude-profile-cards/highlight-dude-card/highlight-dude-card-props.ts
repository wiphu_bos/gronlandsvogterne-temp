import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { MyDudeProfileProps } from '../../../my-dude-profile.props'

export interface HighlightDudeCardProps extends NavigationContainerProps<ParamListBase>, MyDudeProfileProps {
}