import { ViewStyle, TextStyle, ImageStyle } from "react-native"
import * as metric from "../../../../../../../theme"
import { color } from "../../../../../../../theme"
import { RFValue } from "react-native-responsive-fontsize"
import { images } from "../../../../../../../theme/images"
import { ButtonProps } from "../../../../../../../components/button/button.props"

export const DUDETTE_ICON: string = images.dudette

const SHADOW: ViewStyle = {
    shadowColor: color.palette.black,
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.13,
    elevation: 4
}

const ROW_CENTER: ViewStyle = {
    alignItems: 'center',
    flexDirection: 'row'
}

export const HIGHLIGHT_CONTAINNER: ViewStyle = {
    width: metric.ratioWidth(376),
    borderRadius: metric.ratioWidth(14),
    backgroundColor: 'rgba(255,255,255,0.9)',
    marginTop: metric.ratioHeight(21),
    ...SHADOW
}

export const WRAPPER_CONTAINNER: ViewStyle = {
    flexDirection: 'row',

}

export const BODY_HIGHLIGHT_CONTAINNER: ViewStyle = {
    flexDirection: 'row',
    paddingVertical: metric.ratioHeight(15),
    flex: 1,
}

export const IMAGE_PROFILE_CONTAINNER: ViewStyle = {
    alignItems: 'flex-end',
    justifyContent: 'center',
    width: metric.ratioWidth(92)
}

export const IMAGE_PROFILE: ImageStyle = {
    width: metric.ratioWidth(70),
    height: metric.ratioWidth(70),
    borderRadius: metric.ratioWidth(70) / 2
}

export const IMAGE_CHAT: ImageStyle = {
    width: metric.ratioWidth(14),
    height: metric.ratioHeight(12.25),
}

export const IMAGE_VIEW: ImageStyle = {
    width: metric.ratioWidth(15.75),
    height: metric.ratioHeight(10.5)
}

export const BODY_HIGHLIGHT_CONTAINNER_TEXT: ViewStyle = {
    flex: 1,
    paddingHorizontal: metric.ratioWidth(21)
}
export const FOOTER_HIGHLIGHT_CONTAINNER: ViewStyle = {
    flexDirection: 'column',
    flex: 1
}
export const SEPARATOR_VIEW: ViewStyle = {
    backgroundColor: color.boder,
    height: 1,
}

export const CONTAINNER_TEXT: ViewStyle = {
    justifyContent: 'center',

}

export const USER_DETAILS_CONTAINNER: ViewStyle = {
    ...ROW_CENTER,
}

export const SUB_USER_DETAILS_CONTAINNER: ViewStyle = {
    ...ROW_CENTER
}

export const SUB_RIGHT_USER_DETAILS_CONTAINNER: ViewStyle = {
    ...ROW_CENTER,
    marginLeft: metric.ratioWidth(15)
}

export const CHAT_CONTAINNER: ViewStyle = {
}

export const FOOTER_CONTENT_CONTAINNER: ViewStyle = {
    flexDirection: 'row',
    flex: 1,
}

export const HIGHTLIGHT_VIEW: ViewStyle = {
    backgroundColor: color.highlight,
    justifyContent: 'center',
    borderBottomLeftRadius: metric.ratioWidth(14)
}
export const EXPIRE_VIEW: ViewStyle = {
    marginLeft: metric.ratioWidth(18),
    justifyContent: 'center'
}

const TEXT: TextStyle = {
    color: color.palette.darkGrey,
    fontFamily: "Montserrat",
}

export const TITLE: TextStyle = {
    ...TEXT,
    lineHeight: metric.ratioHeight(36),
    fontSize: RFValue(17),
    fontWeight: '400',

    //Android
    fontFamily: "Montserrat-Regular",
}

export const SUB_TITLE: TextStyle = {
    ...TEXT,
    lineHeight: metric.ratioHeight(24),
    fontSize: RFValue(12),
    fontWeight: '300',

    //Android
    fontFamily: "Montserrat-Light",
}

export const DESCRIPTION: TextStyle = {
    ...TEXT,
    ...SUB_TITLE,
    lineHeight: metric.ratioHeight(27),
    fontSize: RFValue(11),
    color: color.palette.darkGrey,
    marginLeft: metric.ratioWidth(9),
    letterSpacing: metric.ratioWidth(1.13)
}


export const SMALL_TITLE: TextStyle = {
    ...TEXT,
    ...DESCRIPTION,
    fontSize: RFValue(10),
    letterSpacing: metric.ratioWidth(1.03),
    color: color.palette.white,
    marginLeft: metric.ratioWidth(0),
    paddingHorizontal: metric.ratioWidth(20)
}

export const GREEN_TEXT: TextStyle = {
    ...SUB_TITLE,
    color: color.palette.green
}

export const EXPIRE_TEXT: TextStyle = {
    ...TEXT,
    ...DESCRIPTION,
    fontSize: RFValue(10),
    letterSpacing: metric.ratioWidth(1.03),
    color: color.palette.darkGrey,
    marginLeft: metric.ratioWidth(0),
}

export const ICON: ImageStyle = {
    width: metric.ratioWidth(36.34),
    height: metric.ratioHeight(40.46),
    marginLeft: metric.ratioWidth(33)
}

export const EDIT_BUTTON_PROPS: ButtonProps = {
    preset: "none",
    isAnimated: false,
    isSolid: true,
    imageSource: images.edit,
    imageStyle: {
        width: metric.ratioWidth(39),
        height: metric.ratioWidth(39),
    },
    containerStyle: {
        position: 'absolute',
        right: 0,
        bottom: 0,
        width: metric.ratioWidth(52),
        height: '100%',
        justifyContent: 'flex-end',
        paddingBottom: metric.ratioHeight(10.5)
    },
}