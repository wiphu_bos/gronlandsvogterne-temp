// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { Text } from "../../../../../../../components"
import { images } from "../../../../../../../theme/images"
import FastImage from "react-native-fast-image"
import { View } from "react-native"
import { UpdateProfileWarningProps } from "./update-profile-warning.props"

// MARK: Style Import

import * as Styles from "./update-profile-warning.styles"

export const UserWarningComponents: React.FunctionComponent<UpdateProfileWarningProps> = observer((props) => {
    return (<View shouldRasterizeIOS style={Styles.USER_DETAILS_CONTAINNER}>
        <View shouldRasterizeIOS style={Styles.SUB_USER_DETAILS_CONTAINNER}>
            <FastImage source={images.infomation} style={Styles.IMAGE_INFO} />
            <Text text={props?.text} style={Styles.SMALL_TITLE} />
        </View>
    </View>)
})
