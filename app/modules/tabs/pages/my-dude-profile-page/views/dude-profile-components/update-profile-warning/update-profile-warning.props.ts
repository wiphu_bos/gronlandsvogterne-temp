import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { TextStyle } from 'react-native'

export interface UpdateProfileWarningProps extends NavigationContainerProps<ParamListBase> {
    text?: string,
    textStyle?: TextStyle
}