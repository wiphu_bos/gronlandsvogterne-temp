// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { Button } from "../../../../../../../components"
import { View } from "react-native"
import { DudeMenuProps } from "./dude-menu.props"

// MARK: Style Import

import * as Styles from "./dude-menu.styles"

export const UserMenuComponents: React.FunctionComponent<DudeMenuProps> = observer((props) => {
    return (<View shouldRasterizeIOS style={Styles.USER_DETAILS_CONTAINNER}>
        <Button preset="none" text="DELETE" textStyle={Styles.USER_MENU_BUTTON_TEXT} containerStyle={Styles.USER_MENU_CONTAINER_TEXT} />
    </View>)
})