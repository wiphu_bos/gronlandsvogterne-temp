import { ViewStyle, TextStyle } from "react-native"
import * as metric from "../../../../../../../theme"
import { RFValue } from "react-native-responsive-fontsize"
import { color } from "../../../../../../../theme/color"

const ROW_CENTER: ViewStyle = {
    alignItems: 'center',
    flexDirection: 'row'
}

export const USER_DETAILS_CONTAINNER: ViewStyle = {
    ...ROW_CENTER,
}


export const USER_MENU_CONTAINER_TEXT: ViewStyle = {
    marginLeft: -metric.ratioWidth(10.5)
}
const TEXT: TextStyle = {
    color: color.palette.darkGrey,
    fontFamily: "Montserrat",
}
const SUB_TITLE: TextStyle = {
    ...TEXT,
    lineHeight: metric.ratioHeight(24),
    fontSize: RFValue(12),
    fontWeight: '300',

    //Android
    fontFamily: "Montserrat-Light",
}
export const DESCRIPTION: TextStyle = {
    ...TEXT,
    ...SUB_TITLE,
    lineHeight: metric.ratioHeight(27),
    fontSize: RFValue(12),
    color: color.palette.darkBrown,
    marginLeft: metric.ratioWidth(9),
    letterSpacing: metric.ratioWidth(1.13)
}

export const USER_MENU_BUTTON_TEXT: TextStyle = {
    ...TEXT,
    ...DESCRIPTION,
    fontSize: RFValue(11),
    letterSpacing: metric.ratioWidth(0.24),
    color: color.palette.red,
}