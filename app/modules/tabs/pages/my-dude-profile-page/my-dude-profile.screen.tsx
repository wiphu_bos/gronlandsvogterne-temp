// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { MyDudeProfileProps } from "./my-dude-profile.props"
import MyDudeProfileController from "./my-dude-profile.controllers"
import { HighlightDudeProfileComponents } from "./views/dude-profile-cards/highlight-dude-card/hilghight-dude-card"
import LinearGradient from "react-native-linear-gradient"
import { NormalDudeProfileComponents } from "./views/dude-profile-cards/normal-dude-card/normal-dude-card"
import { View, RefreshControl } from "react-native"
import { FlatList } from "react-native-gesture-handler"
import { LoadMoreIndicator } from "../../../../components/loadmore-indicator/loadmore-indicator"
import { LoadingListComponent, ConfirmModal } from "../../../../components"
import { useConfigurate } from "../../../../custom-hooks/use-configure-controller"
import { DudeStatus } from "../../../../constants/app.constant"

// MARK: Style Import

import * as Styles from "./my-dude-profile.styles"


const MyDudeProfileCardViewController: React.FunctionComponent<MyDudeProfileProps> = observer((props) => {
    const isHighlight = props?.dudeProfile$.is_highlight
    const isPublished = props?.dudeProfile$.status === DudeStatus.Published
    const isExpired = props?.dudeProfile$?.expiryDateCount <= 0
    return isHighlight &&
        isPublished &&
        !isExpired ?
        < HighlightDudeProfileComponents
            key={props?.dudeProfile$.uid}
            dudeProfile$={props?.dudeProfile$}
            navigation={props?.navigation}
            onPress={() => props?.controller?.onDudeCardPressed(props?.dudeProfile$)} /> :
        < NormalDudeProfileComponents
            key={props?.dudeProfile$.uid}
            dudeProfile$={props?.dudeProfile$}
            navigation={props?.navigation}
            onPress={() => props?.controller?.onDudeCardPressed(props?.dudeProfile$)}
            onDeletePress={() => props?.controller?.onDeleteDudePress(props?.dudeProfile$)} />
})



export const MyDudeProfileScreen: React.FunctionComponent<MyDudeProfileProps> = observer((props) => {

    const controller = useConfigurate(MyDudeProfileController, props) as MyDudeProfileController
    // MARK: Render
    return (
        <LinearGradient
            shouldRasterizeIOS
            style={Styles.CONTAINER}
            {...Styles.TAB_PAGE_BACKGROUND}>
            {MyDudeProfileController.viewModel?.getIsFirstLoadDone ?
                <FlatList
                    // {...resourcesViewModel?.getTestIDCommentList()}
                    refreshControl={
                        <RefreshControl
                            style={Styles.REFRESH_CONTROL}
                            refreshing={false}
                            onRefresh={controller.onRefresh}
                        />
                    }
                    contentContainerStyle={Styles.LIST_VIEW}
                    showsVerticalScrollIndicator={false}
                    ItemSeparatorComponent={({ highlighted }) => (
                        <View shouldRasterizeIOS style={Styles.SEPARATOR_VIEW} />
                    )}
                    extraData={MyDudeProfileController.viewModel?.getMyDudeList?.slice()}
                    data={MyDudeProfileController.viewModel?.getMyDudeList}
                    renderItem={({ item, index, separators }) => (
                        <MyDudeProfileCardViewController dudeProfile$={item} controller={controller} />
                    )}
                    ListFooterComponent={!MyDudeProfileController.viewModel?.getIsLoadDone && <LoadMoreIndicator />}
                    onEndReachedThreshold={0.4}
                    onEndReached={controller.onLoadMore}
                />
                : <LoadingListComponent />}
            <ConfirmModal
                {...MyDudeProfileController.resourcesViewModel?.getTestIDConfirmModal()}
                okButtonText={MyDudeProfileController.resourcesViewModel?.getResourceModalButtonOKTitle()}
                clearButtonText={MyDudeProfileController.resourcesViewModel?.getResourceModalButtonCancelTitle()}
                message={MyDudeProfileController.resourcesViewModel?.getResourceModalConfirmDeleteDude()}
                isVisible={MyDudeProfileController.viewModel?.getIsModalShowing || false}
                onCancel={controller.onCancel}
                onOK={controller.onConfirmDeleteDudePress}
                onModalClosed={controller.onModalClosed}
            />
        </LinearGradient>

    )
})
