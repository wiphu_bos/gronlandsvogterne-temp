// MARK: Import
import { NavigationKey, DudeStatus } from "../../../../constants/app.constant"
import { MyDudeProfileProps } from "./my-dude-profile.props"
import { MyDudeProfileResourcesStoreModel, MyDudeProfileStoreModel } from "./storemodels/my-dude-profile.store"
import { MyDudeProfileResourcesStore, MyDudeProfileStore } from "./storemodels/my-dude-profile.types"
import { RootStore, INavigationRoute } from "../../../../models"
import { PopoverMenuActionType } from "../../../../components/popover-menu/popover-menu.props"
import { StackActions } from "@react-navigation/core"
import { SnapshotChangeType, Property } from "../../../../constants/firebase/fire-store.constant"
import { FirebaseFirestoreTypes } from '@react-native-firebase/firestore'
import { IUser, UserStore, UserModel } from "../../../../models/user-store"
import FirebaseFireStore from "../../../../utils/firebase/fire-store/fire-store"
import { IMyDudeListListener } from "../../../../utils/firebase/fire-store/fire-store.types"
import * as DataUtils from "../../../../utils/data.utils"
import { BaseController } from "../../../base.controller"
import { onSnapshot, unprotect } from "mobx-state-tree"
import { ExtractCSTWithSTN } from "mobx-state-tree/dist/core/type/type"

class MyDudeProfileController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static viewModel: MyDudeProfileStore
    public static resourcesViewModel: MyDudeProfileResourcesStore
    private _dudeProfileSelected: ExtractCSTWithSTN<typeof UserModel>
    private static _disposer: () => void

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, myProps?: MyDudeProfileProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, myProps, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(myProps)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        MyDudeProfileController.resourcesViewModel = MyDudeProfileResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.MyDudeProfile)
        MyDudeProfileController.viewModel = localStore && MyDudeProfileStoreModel.create({ ...localStore }) || MyDudeProfileStoreModel.create({})
        unprotect(MyDudeProfileController.viewModel)
    }
    private _setupSnapShot = () => {
        onSnapshot(MyDudeProfileController.viewModel, (snap: MyDudeProfileStore) => {
            this._setInitializedToPropsParams()
        })
    }

    /*
       Mark Data
   */

    private _loadCacheData = () => MyDudeProfileController.viewModel?.setMyDudeList(this._rootStore)

    private _fetchMyDudeList = async () => await MyDudeProfileController.viewModel?.fetchMyDudeList(this._rootStore)

    /*
       Mark Event
   */

    private _onMyDudeListListener = () => {
        const onNext: (snapshot: FirebaseFirestoreTypes.QuerySnapshot) => void = (snap => {
            if (!MyDudeProfileController.viewModel?.getIsFirstLoadDone || snap.metadata.hasPendingWrites) return
            snap.docChanges().forEach(change => {
                const dudeDetails: IUser = change.doc.data()
                const expiryDateForHighlightDude = dudeDetails[Property.EXPIRY_DATE]
                const dudeDetailsWithPurchaseDetails = DataUtils.addPurchaseToUserProfileObject(dudeDetails, expiryDateForHighlightDude)
                if (change.type === SnapshotChangeType.MODIFIED) {
                    if (dudeDetailsWithPurchaseDetails) MyDudeProfileController.viewModel?.updateDudeInList(this._rootStore, dudeDetailsWithPurchaseDetails)
                }
                else if (change.type === SnapshotChangeType.REMOVED) {
                    if (dudeDetailsWithPurchaseDetails) MyDudeProfileController.viewModel?.removeDudeInList(this._rootStore, dudeDetailsWithPurchaseDetails)
                }
            })
        })

        const onError: (e: Error) => void = (e => {
        })

        const onComplete: () => void = () => {
        }

        const params: IMyDudeListListener = {
            userId: this._rootStore?.getUserStore?.getUserId,
            onNext,
            onError,
            onComplete
        }

        MyDudeProfileController._disposer = FirebaseFireStore.onMyDudeListListener(params)
    }

    private _isLoading = (value: boolean) => MyDudeProfileController.viewModel?.setIsPullRefreshing(value)

    public onDudeCardPressed = async (dudeProfile$: ExtractCSTWithSTN<typeof UserModel>) => {
        const dudeProfileObject: UserStore = DataUtils.getUserProfileObject(this._rootStore, null, dudeProfile$) //dis-observe here
        if (dudeProfileObject) {
            let navigationKey: string
            let params: any
            const status = dudeProfileObject.status
            if (status === DudeStatus.Deleted) return
            if (status === DudeStatus.WaitingForPublishing) {
                navigationKey = NavigationKey.WaitingDudePublishProfile
            } else if (status === DudeStatus.WaitingForApproval) {
                params = {
                    title: MyDudeProfileController.resourcesViewModel?.getResourceSendEmailSuccessTitle(),
                    subTitle: MyDudeProfileController.resourcesViewModel?.getResourceSendEmailSuccessSubTitle(),
                    description: MyDudeProfileController.resourcesViewModel?.getResourceSendEmailSuccessDescription(),
                    firstContent: dudeProfileObject?.email,
                    secondContent: dudeProfileObject?.full_name,
                    screenState: NavigationKey.WaitingDudeApprove
                }
                navigationKey = NavigationKey.EmailSendSuccess
            } else {
                navigationKey = NavigationKey.CreateEditProfileTab
                params = { dudeProfileObject: dudeProfileObject }
            }
            navigationKey && this._myProps?.navigation?.dispatch(StackActions.push(navigationKey, params) as any)
        }
    }

    private _onOpenModal = () => MyDudeProfileController.viewModel?.setIsModalShowing(true)
    private _onCloseModal = () => MyDudeProfileController.viewModel?.setIsModalShowing(false)

    public onDeleteDudePress = (dudeProfile$: ExtractCSTWithSTN<typeof UserModel>) => {
        this._dudeProfileSelected = dudeProfile$
        this._onOpenModal()
    }

    public onCancel = () => {
        this._dudeProfileSelected = null
        this._onCloseModal()
    }

    public onModalClosed = async () => {
        if (this._dudeProfileSelected) {
            const dudeProfile = this._dudeProfileSelected
            this._isGlobalLoading(true)
            await MyDudeProfileController.viewModel?.deleteDude(this._rootStore, dudeProfile.uid)
            this._isGlobalLoading(false)
            this._dudeProfileSelected = null
        }
    }
    public onConfirmDeleteDudePress = () => this._onCloseModal()

    public onRefresh = async () => {
        const isConnected = this._rootStore?.getSharedStore?.getIsConnected
        if (!isConnected) return
        this._isLoading(true)
        MyDudeProfileController.viewModel?.resetFetching(this._rootStore)
        await this._fetchMyDudeList()
        this._isLoading(false)
    }

    public onLoadMore = async () => {
        if (!MyDudeProfileController.viewModel?.getShouldLoadmore) return
        MyDudeProfileController.viewModel?.setIsLoadMore(true)
        await this._fetchMyDudeList()
    }

    public onPopoverMenuItemPressed = (type?: PopoverMenuActionType) => {
    }

    //@override
    backProcess = () => {
    }

    /*
       Mark Life cycle
    */

    //@override
    viewDidAppearOnce = async () => {
        this._onMyDudeListListener()
        this._loadCacheData()
    }

    //@override

    viewDidAppearAfterFocus = async () => {
        super.viewDidAppearAfterFocus && super.viewDidAppearAfterFocus()
        const isConnected = this._rootStore?.getSharedStore?.getIsConnected
        if (isConnected) {
            if (MyDudeProfileController.viewModel?.getMyDudeList?.length === 0) {
                await this.onRefresh()
            } else if (!MyDudeProfileController.viewModel?.getIsFirstLoadDone) {
                await this._fetchMyDudeList()
            }
        } else {
            MyDudeProfileController.viewModel?.setIsFirstLoadDone(true)
        }
    }
    //@override
    deInit = () => {
        super.deInit && super.deInit()
        MyDudeProfileController._disposer && MyDudeProfileController._disposer()
        MyDudeProfileController.viewModel?.invalidateAllTimerIfNeeded()
    }

    /*
       Mark Helper
   */
}

export default MyDudeProfileController