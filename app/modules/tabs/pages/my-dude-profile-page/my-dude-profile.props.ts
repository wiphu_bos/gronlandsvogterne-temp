import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { UserModel } from '../../../../models/user-store'
import { ExtractCSTWithSTN } from 'mobx-state-tree/dist/core/type/type'
import MyDudeProfileController from './my-dude-profile.controllers'

export interface MyDudeProfileCardProps extends NavigationContainerProps<ParamListBase> {
    dudeProfile$?: ExtractCSTWithSTN<typeof UserModel>
}
export interface MyDudeProfileProps extends NavigationContainerProps<ParamListBase>, MyDudeProfileCardProps {
    controller?: MyDudeProfileController
    onPress?: (dudeProfile$: ExtractCSTWithSTN<typeof UserModel>) => void
}