import { types } from "mobx-state-tree"
import { MyDudeProfilePropsModel } from "./my-dude-profile.models"
import { GeneralResources } from "../../../../../constants/firebase/remote-config"
import { TestIDResources } from "../../../../../constants/test-key/test.constant"
import * as Utils from "../../../../../utils"
import { GeneralResourcesStoreModel } from "../../../../../models/general-resources-store"
import * as DataUtils from "../../../../../utils/data.utils"
import { NavigationKey } from "../../../../../constants/app.constant"

export const MyDudeProfileViews = types.model(MyDudeProfilePropsModel)
    .views(self => ({
        get getMyDudeList() {
            return self.myDudeList$
        },
        get getLastId(): string {
            return self.last_id
        },
        get getIsPullRefreshing(): boolean {
            return self.isPullRefreshing
        },
        get getIsLoadMore(): boolean {
            return self.isLoadMore
        },
        get getIsFirstLoadDone(): boolean {
            return self.isFirstLoadDone
        },
        get getIsLoadDone(): boolean {
            return self.isLoadDone
        },
        get getShouldLoadmore(): boolean {
            return !self.isLoadDone && !self.isLoadMore && self.myDudeList$?.length > 0 && self.isFirstLoadDone
        },
        get getIsModalShowing(): boolean {
            return self.isModalShowing
        }
    }))

export const MyDudeProfileResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State

    const { MyDudeProfileScreen, WaitingDudeApproveScreen } = GeneralResources
    const { hightLightStatusTitle,
        updateDudeProfileTitle,
        deleteTitle,
        expireInTitle,
        modalButtonOKTitle,
        modalButtonCancelTitle,
        modalConfirmDeleteDude
    } = MyDudeProfileScreen

    const {
        successTitle,
        successSubTitle,
        successDescription
    } = WaitingDudeApproveScreen

    //MARK: Views
    const getResourcesWaitingDudeApprove = (key: string) => self.getValues(NavigationKey.WaitingDudeApprove, key)
    const getShareResources = (key: string) => self.getValues(key, true)
    const getResourceSendEmailSuccessTitle = () => getResourcesWaitingDudeApprove(successTitle)
    const getResourceSendEmailSuccessSubTitle = () => getResourcesWaitingDudeApprove(successSubTitle)
    const getResourceSendEmailSuccessDescription = () => getResourcesWaitingDudeApprove(successDescription)
    const getResourceHightLightStatusTitle = () => getShareResources(hightLightStatusTitle)
    const getResourceUpdateDudeProfileTitle = () => getShareResources(updateDudeProfileTitle)
    const getResourceDeleteTitle = () => getShareResources(deleteTitle)
    const getResourceExpireInTitle = () => getShareResources(expireInTitle)
    const getResourceUserStatusTitle = (status: string) => DataUtils.getUserStatusByLocaleKey(self, status)
    const getResourceModalButtonOKTitle = () => getShareResources(modalButtonOKTitle)
    const getResourceModalButtonCancelTitle = () => getShareResources(modalButtonCancelTitle)
    const getResourceModalConfirmDeleteDude = () => getShareResources(modalConfirmDeleteDude)
    return {
        getResourceSendEmailSuccessTitle,
        getResourceSendEmailSuccessSubTitle,
        getResourceSendEmailSuccessDescription,
        getResourceHightLightStatusTitle,
        getResourceUpdateDudeProfileTitle,
        getResourceDeleteTitle,
        getResourceExpireInTitle,
        getResourceUserStatusTitle,
        getResourceModalButtonOKTitle,
        getResourceModalButtonCancelTitle,
        getResourceModalConfirmDeleteDude
    }
})
    .views(self => {

        //MARK: Volatile State

        //MARK: Views
        const getTestIDConfirmModal = () => Utils.getTestIDObject(TestIDResources.MyDudeProfileScreen.confirmModal)
        const getTestIDImageProfile = () => Utils.getTestIDObject(TestIDResources.MyDudeProfileScreen.imageProfile)
        const getTestIDFullNameTitle = () => Utils.getTestIDObject(TestIDResources.MyDudeProfileScreen.fullName)
        const getTestIDHighLightStatusTitle = () => Utils.getTestIDObject(TestIDResources.MyDudeProfileScreen.highlightStatus)
        const getTestIDUpdateDudeProfileTitle = () => Utils.getTestIDObject(TestIDResources.MyDudeProfileScreen.updateDudeProfile)
        const getTestIDExpireInTitle = () => Utils.getTestIDObject(TestIDResources.MyDudeProfileScreen.expireIn)
        const getTestIDButtonDeleteTitle = () => Utils.getTestIDObject(TestIDResources.MyDudeProfileScreen.buttonDelete)
        const getTestIDButtonEditProfileTitle = () => Utils.getTestIDObject(TestIDResources.MyDudeProfileScreen.buttonEditProfile)
        const getTestIDCommentCountTitle = () => Utils.getTestIDObject(TestIDResources.MyDudeProfileScreen.commentCountTitle)
        const getTestIDUnreadCommentCountTitle = () => Utils.getTestIDObject(TestIDResources.MyDudeProfileScreen.unreadCommentCountTitle)
        const getTestIDViewCountTitle = () => Utils.getTestIDObject(TestIDResources.MyDudeProfileScreen.viewCountTitle)
        const getTestIDUserStatusTitle = (status: string) => Utils.getTestIDObject(status)
        return {
            getTestIDConfirmModal,
            getTestIDImageProfile,
            getTestIDFullNameTitle,
            getTestIDHighLightStatusTitle,
            getTestIDUpdateDudeProfileTitle,
            getTestIDExpireInTitle,
            getTestIDUserStatusTitle,
            getTestIDButtonDeleteTitle,
            getTestIDButtonEditProfileTitle,
            getTestIDCommentCountTitle,
            getTestIDUnreadCommentCountTitle,
            getTestIDViewCountTitle
        }
    })
