import { types, unprotect } from "mobx-state-tree"
import { MyDudeProfilePropsModel } from "./my-dude-profile.models"
import { IUser, UserStoreModel } from "../../../../../models/user-store"
import { RootStore } from "../../../../../models"
import * as DataUtils from "../../../../../utils/data.utils"
import { UserType } from "../../../../../constants/app.constant"

export const MyDudeProfileActions = types.model(MyDudeProfilePropsModel).actions(self => {
    const findExistingItemIndex = (dude: IUser): number => {
        const tempMyDudeList = self.myDudeList$
        const existingList = tempMyDudeList?.slice() //Slice to disobserve property
        const foundItemIndex = existingList?.findIndex(v => v.uid === dude?.uid)
        return foundItemIndex
    }
    const setMyDudeList = (rootStore: RootStore) => {
        rootStore.getUserStore?.getMyDudeList?.forEach((e: IUser) => {
            const dudeProfileObject = DataUtils.getUserProfileObject(rootStore, null, e)
            const childViewModel = UserStoreModel.create(dudeProfileObject)
            childViewModel.setIsTimerSet(rootStore, true)
            unprotect(childViewModel)
            self.myDudeList$.push(childViewModel)
        })
        self.isPullRefreshing = false
        self.isLoadMore = false
    }
    const updateDudeInList = (rootStore: RootStore, dude: IUser) => {
        const foundItemIndex = findExistingItemIndex(dude)
        if (foundItemIndex === -1) return
        const dudeProfileObject = DataUtils.getUserProfileObject(rootStore, null, dude)
        self.myDudeList$[foundItemIndex].bindingDataFromObject(dudeProfileObject, UserType.Dude)
        self.myDudeList$[foundItemIndex].setIsTimerSet(rootStore, dude?.is_highlight)
        rootStore?.getUserStore?.saveMyDudeToList(rootStore, dude)
    }
    const removeDudeInList = (rootStore: RootStore, dude: IUser) => {
        const foundItemIndex = findExistingItemIndex(dude)
        if (foundItemIndex === -1) return
        self.myDudeList$[foundItemIndex].setIsTimerSet(null, false)
        self.myDudeList$.splice(foundItemIndex, 1)
        rootStore?.getUserStore?.removeMyDudeFromList(dude)
    }
    const invalidateAllTimerIfNeeded = () => {
        self.myDudeList$.forEach(e => {
            e.setIsTimerSet(null, false)
        })
    }
    const setIsPullRefreshing = (value: boolean) => self.isPullRefreshing = value
    const setIsLoadMore = (value: boolean) => self.isLoadMore = value
    const setIsFirstLoadDone = (value: boolean) => self.isFirstLoadDone = value
    const setIsLoadDone = (value: boolean) => self.isLoadDone = value
    const resetFetching = (rootStore: RootStore) => {
        invalidateAllTimerIfNeeded()
        rootStore?.getUserStore?.setMyDudeList([])
        self.myDudeList$.clear()
        self.isFirstLoadDone = false
        self.isLoadDone = false
        self.isLoadMore = false
    }
    const setIsPopoverMenuVisible = (value: boolean) => self.isPopoverMenuVisible = value
    const setIsModalShowing = (value: boolean) => self.isModalShowing = value
    return {
        setIsModalShowing,
        setIsPopoverMenuVisible,
        updateDudeInList,
        setIsPullRefreshing,
        resetFetching,
        setIsLoadMore,
        removeDudeInList,
        setMyDudeList,
        setIsLoadDone,
        setIsFirstLoadDone,
        invalidateAllTimerIfNeeded
    }
})