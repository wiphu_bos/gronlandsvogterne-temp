import { types } from "mobx-state-tree"
import { UserStoreModel } from "../../../../../models/user-store"

export const MyDudeProfilePropsModel = {
    isPopoverMenuVisible: types.optional(types.boolean, false),
    myDudeList$: types.optional(types.array(UserStoreModel), []),
    last_id: types.maybeNull(types.string),
    isPullRefreshing: types.optional(types.boolean, false),
    isFirstLoadDone: types.optional(types.boolean, false),
    isLoadDone: types.optional(types.boolean, false),
    isLoadMore: types.optional(types.boolean, false),
    isModalShowing: types.optional(types.boolean, false)
} 