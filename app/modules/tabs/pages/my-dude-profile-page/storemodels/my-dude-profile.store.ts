import { types } from "mobx-state-tree"
import { StoreName } from "../../../../../constants/app.constant"
import { MyDudeProfilePropsModel } from "../viewmodels/my-dude-profile.models"
import { MyDudeProfileActions } from "../viewmodels/my-dude-profile.actions"
import { MyDudeProfileViews, MyDudeProfileResourcesViews } from "../viewmodels/my-dude-profile.views"
import { GeneralResourcesStoreModel } from "../../../../../models/general-resources-store"
import { MyDudeProfileServiceActions } from "../services/my-dude-profile.services"

const DudeProfileModel = types.model(StoreName.MyDudeProfile, MyDudeProfilePropsModel)
export const MyDudeProfileStoreModel = types.compose(DudeProfileModel, MyDudeProfileViews, MyDudeProfileActions, MyDudeProfileServiceActions)
export const MyDudeProfileResourcesStoreModel = types.compose(GeneralResourcesStoreModel, MyDudeProfileResourcesViews)

