import { Instance, SnapshotOut } from "mobx-state-tree"
import { MyDudeProfileStoreModel, MyDudeProfileResourcesStoreModel } from "./my-dude-profile.store"

export type MyDudeProfileStore = Instance<typeof MyDudeProfileStoreModel>
export type MyDudeProfileStoreSnapshot = SnapshotOut<typeof MyDudeProfileStoreModel>

export type MyDudeProfileResourcesStore = Instance<typeof MyDudeProfileResourcesStoreModel>
export type MyDudeProfileResourcesStoreSnapshot = SnapshotOut<typeof MyDudeProfileResourcesStoreModel>
