
import { MyDudeProfilePropsModel } from "../viewmodels"
import { types, flow, unprotect } from "mobx-state-tree"
import { APIResponse, APISuccessResponse } from "../../../../../constants/service.types"
import { RootStore } from "../../../../../models"
import { IMyDudeListResponse } from "../../../../../models/user-store/user.types"
import { UserStoreModel } from "../../../../../models/user-store/user.store"
import * as DataUtils from "../../../../../utils/data.utils"
import { Property } from "../../../../../constants/firebase/fire-store.constant"

export const MyDudeProfileServiceActions = types.model(MyDudeProfilePropsModel).actions(self => {
    const fetchMyDudeList = flow(function* (rootStore?: RootStore) {
        const last_id = self.isFirstLoadDone ? self.last_id : null
        try {
            self.isLoadDone = self.myDudeList$.length > 0
            const result: APIResponse = yield rootStore?.getUserStore.fetchMyDudeList(last_id)
            let success = (result as APISuccessResponse)?.data
            const response: IMyDudeListResponse = success?.data
            const myDudeList = response?.my_dude_list
            const pagingObj = response?.page
            if (myDudeList?.length > 0) {
                if (!self.isLoadMore) {
                    rootStore?.getUserStore?.setMyDudeList([])
                    self.myDudeList$.forEach(e => e.setIsTimerSet(null, false))
                    self.myDudeList$.clear()
                }
                myDudeList?.forEach(e => {
                    const expiryDate = e[Property.EXPIRY_DATE] //already cast to date from backend.
                    const dudeDetailsWithPaymentDetails = DataUtils.addPurchaseToUserProfileObject(e, expiryDate, false)
                    const myDudeObject = DataUtils.getUserProfileObject(rootStore, null, dudeDetailsWithPaymentDetails)
                    const childViewModel = UserStoreModel.create(myDudeObject)
                    childViewModel.setIsTimerSet(rootStore, true)
                    unprotect(childViewModel)
                    self.myDudeList$.push(childViewModel)
                })
                rootStore?.getUserStore?.setMyDudeList(myDudeList)
            }
            self.last_id = pagingObj?.last_id
            self.isLoadDone = pagingObj?.is_last_page
            return result
        } catch (e) {
            return e
        } finally {
            self.isFirstLoadDone = true
            self.isPullRefreshing = false
            self.isLoadMore = false
        }
    })

    const deleteDude = flow(function* (rootStore?: RootStore, dudeId?: string) {
        try {
            return yield rootStore?.getUserStore?.deleteDude(dudeId)
        }
        catch (e) {
            return e
        }
    })
    return { fetchMyDudeList, deleteDude }
})