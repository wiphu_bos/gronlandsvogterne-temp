// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { FavouriteDudetteListProps } from "./favourite-dudette-list.props"
import LinearGradient from "react-native-linear-gradient"
import { View, RefreshControl } from "react-native"
import { FlatList } from "react-native-gesture-handler"
import { LoadMoreIndicator } from "../../../../components/loadmore-indicator/loadmore-indicator"
import { LoadingListComponent } from "../../../../components"
import { useConfigurate } from "../../../../custom-hooks/use-configure-controller"
import { FavouriteCardComponents } from "../../../../components/favourite-card/favourite-card"
import FavouriteDudetteListController from "./favourite-dudette-list.controllers"
import { UserType } from "../../../../constants/app.constant"

// MARK: Style Import

import * as Styles from "./favourite-dudette-list.styles"

export const FavouriteDudetteListScreen: React.FunctionComponent<FavouriteDudetteListProps> = observer((props) => {

    const controller = useConfigurate(FavouriteDudetteListController, props) as FavouriteDudetteListController

    // MARK: Render
    return (
        <LinearGradient
            shouldRasterizeIOS
            style={Styles.CONTAINER}
            {...Styles.TAB_PAGE_BACKGROUND}>
            {FavouriteDudetteListController.viewModel?.getIsFirstLoadDone ?
                <FlatList
                    // {...resourcesViewModel?.getTestIDFavList()}
                    refreshControl={
                        <RefreshControl
                            style={Styles.REFRESH_CONTROL}
                            refreshing={false}
                            onRefresh={controller.onRefresh}
                        />
                    }
                    contentContainerStyle={Styles.LIST_VIEW}
                    showsVerticalScrollIndicator={false}
                    ItemSeparatorComponent={({ highlighted }) => (
                        <View shouldRasterizeIOS style={Styles.SEPARATOR_VIEW} />
                    )}
                    extraData={FavouriteDudetteListController.viewModel?.getFavouriteDudetteList?.slice()}
                    data={FavouriteDudetteListController.viewModel?.getFavouriteDudetteList}
                    renderItem={({ item, index, separators }) => (
                        <FavouriteCardComponents
                            userType={UserType.Dudette}
                            primaryCount={item.comment_count}
                            unreadPrimaryCount={item.unread_comment_count}
                            primaryCountTestID={FavouriteDudetteListController.resourcesViewModel?.getTestIDCommentCountTitle()}
                            fullnameTestID={FavouriteDudetteListController.resourcesViewModel?.getTestIDFullNameTitle()}
                            imageProfileTestID={FavouriteDudetteListController.resourcesViewModel?.getTestIDImageProfile()}
                            unreadPrimaryCountTestID={FavouriteDudetteListController.resourcesViewModel?.getTestIDUnreadCommentCountTitle()}
                            viewCountTestID={FavouriteDudetteListController.resourcesViewModel?.getTestIDViewCountTitle()}
                            viewCount={item.view_count}
                            favouriteDetails$={item}
                            navigation={props?.navigation}
                            isHighlight={false}
                            onPress={(item) => controller.onDudetteCardPressed(item)} />
                    )}
                    ListFooterComponent={!FavouriteDudetteListController.viewModel?.getIsLoadDone && <LoadMoreIndicator />}
                    onEndReachedThreshold={0.4}
                    onEndReached={controller.onLoadMore}
                />
                : <LoadingListComponent />}
        </LinearGradient>

    )
})
