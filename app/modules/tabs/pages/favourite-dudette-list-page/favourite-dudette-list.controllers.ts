// MARK: Import
import { NavigationKey, SearchListType } from "../../../../constants/app.constant"
import { FavouriteDudetteListProps } from "./favourite-dudette-list.props"
import { FavouriteDudetteListResourcesStoreModel, FavouriteDudetteListStoreModel } from "./storemodels/favourite-dudette-list.store"
import { FavouriteDudetteListResourcesStore, FavouriteDudetteListStore } from "./storemodels/favourite-dudette-list.types"
import { RootStore, INavigationRoute } from "../../../../models"
import { StackActions } from "@react-navigation/core"
import { IUser, UserStore, UserModel } from "../../../../models/user-store"
import * as DataUtils from "../../../../utils/data.utils"
import { BaseController } from "../../../base.controller"
import { onSnapshot } from "mobx-state-tree"
import { SnapshotChangeType } from "../../../../constants/firebase/fire-store.constant"
import { FirebaseFirestoreTypes } from "@react-native-firebase/firestore"
import FirebaseFireStore from "../../../../utils/firebase/fire-store/fire-store"
import { IFavouriteListListener } from "../../../../utils/firebase/fire-store/fire-store.types"
import { ExtractCSTWithSTN, unprotect } from "mobx-state-tree/dist/internal"

class FavouriteDudetteListController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static viewModel: FavouriteDudetteListStore
    public static resourcesViewModel: FavouriteDudetteListResourcesStore
    private static _disposer: () => void
    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, myProps?: FavouriteDudetteListProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, myProps, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(myProps)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        FavouriteDudetteListController.resourcesViewModel = FavouriteDudetteListResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.FavouriteDudetteList)
        FavouriteDudetteListController.viewModel = localStore && FavouriteDudetteListStoreModel.create({ ...localStore }) || FavouriteDudetteListStoreModel.create({})
        // unprotect(FavouriteDudetteListController.viewModel)
    }
    private _setupSnapShot = () => {
        onSnapshot(FavouriteDudetteListController.viewModel, (snap: FavouriteDudetteListStore) => {
            this._setInitializedToPropsParams()
        })
    }

    /*
       Mark Data
   */

    private _loadCacheData = () => FavouriteDudetteListController.viewModel?.setFavouriteDudetteList(this._rootStore, this._rootStore?.getUserStore?.getFavDudetteList)

    private _fetchFavouriteDudetteList = async () => await FavouriteDudetteListController.viewModel?.fetchFavouriteDudetteList(this._rootStore)


    /*
       Mark Event
   */


    private _onFavouriteDudetteListListener = () => {
        const onNext: (snapshot: FirebaseFirestoreTypes.QuerySnapshot) => void = (snap => {
            if (!FavouriteDudetteListController.viewModel?.getIsFirstLoadDone || snap.metadata.hasPendingWrites) return
            snap.docChanges().forEach(change => {
                const dudette: IUser = change.doc.data()
                if (change.type === SnapshotChangeType.ADDED) {
                    if (dudette) FavouriteDudetteListController.viewModel?.addDudetteInList(this._rootStore, dudette)
                }
                if (change.type === SnapshotChangeType.MODIFIED) {
                    if (dudette) FavouriteDudetteListController.viewModel?.updateDudetteInList(this._rootStore, dudette)
                }
                else if (change.type === SnapshotChangeType.REMOVED) {
                    if (dudette) FavouriteDudetteListController.viewModel?.removeDudetteInList(this._rootStore, dudette)
                }
            })
        })

        const onError: (e: Error) => void = (e => {
        })

        const onComplete: () => void = () => {
        }

        const params: IFavouriteListListener = {
            userId: this._rootStore?.getUserStore?.getUserId,
            type: SearchListType.Dudette,
            onNext,
            onError,
            onComplete
        }

        FavouriteDudetteListController._disposer = FirebaseFireStore.onFavouriteListener(params)
    }

    public onRefresh = async () => {
        const isConnected = this._rootStore?.getSharedStore?.getIsConnected
        if (!isConnected) return
        this._isLoading(true)
        FavouriteDudetteListController.viewModel?.resetFetching(this._rootStore)
        await this._fetchFavouriteDudetteList()
        this._isLoading(false)
    }

    public onLoadMore = async () => {
        if (!FavouriteDudetteListController.viewModel?.getShouldLoadmore) return
        FavouriteDudetteListController.viewModel?.setIsLoadMore(true)
        await this._fetchFavouriteDudetteList()
    }

    private _isLoading = (value: boolean) => FavouriteDudetteListController.viewModel?.setIsPullRefreshing(value)

    public onDudetteCardPressed = async (favouriteDetails$: ExtractCSTWithSTN<typeof UserModel>) => {
        this._isGlobalLoading(true)
        const dudetteProfile = await FavouriteDudetteListController.viewModel?.fetchDudetteProfileDetail(this._rootStore, favouriteDetails$.dudette_id)
        this._isGlobalLoading(false)
        const dudetteProfileObject: UserStore = DataUtils.getUserProfileObject(this._rootStore, null, dudetteProfile) //dis-observe here
        if (dudetteProfileObject) {
            this._myProps?.navigation?.dispatch(StackActions.push(NavigationKey.DudetteProfileDetails, { dudetteProfileObject: dudetteProfileObject }) as any)
        }
    }

    //@override
    backProcess = () => {
    }

    /*
       Mark Life cycle
   */

    //@override
    viewDidAppearOnce = async () => {
        this._onFavouriteDudetteListListener()
        this._loadCacheData()
    }

    //@override

    viewDidAppearAfterFocus = async () => {
        super.viewDidAppearAfterFocus && super.viewDidAppearAfterFocus()
        const isConnected = this._rootStore?.getSharedStore?.getIsConnected
        if (isConnected) {
            if (FavouriteDudetteListController.viewModel?.getFavouriteDudetteList?.length === 0) {
                await this.onRefresh()
            } else if (!FavouriteDudetteListController.viewModel?.getIsFirstLoadDone) {
                await this._fetchFavouriteDudetteList()
            }
        } else {
            FavouriteDudetteListController.viewModel?.setIsFirstLoadDone(true)
        }
    }

    //@override
    deInit = () => {
        super.deInit && super.deInit()
        FavouriteDudetteListController._disposer && FavouriteDudetteListController._disposer()
    }

    /*
       Mark Helper
   */
}

export default FavouriteDudetteListController