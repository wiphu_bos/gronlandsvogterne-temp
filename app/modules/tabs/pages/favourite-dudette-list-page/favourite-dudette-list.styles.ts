import { ViewStyle } from "react-native"
import { color } from "../../../../theme"
import * as metric from "../../../../theme"

export const FULL: ViewStyle = {
    flex: 1
}

export const TAB_PAGE_BACKGROUND: any = { colors: color.tabPageBackground }

export const CONTAINER: ViewStyle = {
    ...FULL
}

export const LIST_VIEW: ViewStyle = {
    marginTop: metric.ratioHeight(21),
    paddingHorizontal: metric.ratioWidth(8),
    paddingBottom: metric.ratioHeight(32),
    alignItems: 'center'
}

export const SEPARATOR_VIEW: ViewStyle = {
    height: metric.ratioHeight(10)
}
export const REFRESH_CONTROL: any = { tintColor: color.brightDim }
