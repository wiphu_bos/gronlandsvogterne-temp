
import { FavouriteDudetteListPropsModel } from "../viewmodels"
import { types, flow, unprotect } from "mobx-state-tree"
import { APIResponse, APISuccessResponse } from "../../../../../constants/service.types"
import { RootStore } from "../../../../../models"
import { UserType } from "../../../../../constants/app.constant"
import { IFavouriteListResponse, IUser } from "../../../../../models/user-store/user.types"
import { UserStoreModel } from "../../../../../models/user-store/user.store"
import * as DataUtils from "../../../../../utils/data.utils"

export const FavouriteDudetteListServiceActions = types.model(FavouriteDudetteListPropsModel).actions(self => {



    const fetchFavouriteDudetteList = flow(function* (rootStore: RootStore) {
       const last_id = self.isFirstLoadDone ? self.last_id : null
        try {
            self.isLoadDone = self.favouriteDudetteList$.length > 0
            const result: APIResponse = yield rootStore?.getUserStore.fetchFavouriteList(last_id, UserType.Dudette)
            let success = (result as APISuccessResponse)?.data
            const response: IFavouriteListResponse = success?.data
            const favouriteDudetteList = response?.favourite_list
            const pagingObj = response?.page
            if (favouriteDudetteList?.length > 0) {
                if (!self.isLoadMore) {
                    rootStore?.getUserStore?.setFavDudetteList([])
                    self.favouriteDudetteList$.clear()
                }

                favouriteDudetteList?.forEach(e => {
                    const favouriteDudetteObject = DataUtils.getUserProfileObject(rootStore, null, e)
                    const childViewModel = UserStoreModel.create(favouriteDudetteObject)
                    unprotect(childViewModel)
                    self.favouriteDudetteList$.push(childViewModel)
                })
                rootStore?.getUserStore?.setFavDudetteList(favouriteDudetteList)
            }
            self.last_id = pagingObj?.last_id
            self.isLoadDone = pagingObj?.is_last_page
            return result
        } catch (e) {
            return e
        } finally {
            self.isFirstLoadDone = true
            self.isPullRefreshing = false
            self.isLoadMore = false
        }
    })

    const fetchDudetteProfileDetail = flow(function* (rootStore?: RootStore, uid?: string) {
        try {
            let result: APISuccessResponse = yield rootStore?.getUserStore?.fetchDudetteProfileDetail(uid)
            const data: APISuccessResponse = result?.data
            const profileDetail: IUser = data?.data
            if (!profileDetail) throw data
            return profileDetail
        } catch (e) {
            return e
        }
    })

    return { fetchFavouriteDudetteList, fetchDudetteProfileDetail }
})