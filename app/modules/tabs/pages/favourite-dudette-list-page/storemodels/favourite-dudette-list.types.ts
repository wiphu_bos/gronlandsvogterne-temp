import { Instance, SnapshotOut } from "mobx-state-tree"
import { FavouriteDudetteListStoreModel, FavouriteDudetteListResourcesStoreModel } from "./favourite-dudette-list.store"

export type FavouriteDudetteListStore = Instance<typeof FavouriteDudetteListStoreModel>
export type FavouriteDudetteListStoreSnapshot = SnapshotOut<typeof FavouriteDudetteListStoreModel>

export type FavouriteDudetteListResourcesStore = Instance<typeof FavouriteDudetteListResourcesStoreModel>
export type FavouriteDudetteListResourcesStoreSnapshot = SnapshotOut<typeof FavouriteDudetteListResourcesStoreModel>
