import { types } from "mobx-state-tree"
import { StoreName } from "../../../../../constants/app.constant"
import { FavouriteDudetteListPropsModel } from "../viewmodels/favourite-dudette-list.models"
import { FavouriteDudetteListActions } from "../viewmodels/favourite-dudette-list.actions"
import { FavouriteDudetteListViews, FavouriteDudetteListResourcesViews } from "../viewmodels/favourite-dudette-list.views"
import { GeneralResourcesStoreModel } from "../../../../../models/general-resources-store"
import { FavouriteDudetteListServiceActions } from "../services/favourite-dudette-list.services"

const DudeProfileModel = types.model(StoreName.FavouriteDudetteList, FavouriteDudetteListPropsModel)
export const FavouriteDudetteListStoreModel = types.compose(DudeProfileModel, FavouriteDudetteListViews, FavouriteDudetteListActions, FavouriteDudetteListServiceActions)
export const FavouriteDudetteListResourcesStoreModel = types.compose(GeneralResourcesStoreModel, FavouriteDudetteListResourcesViews)

