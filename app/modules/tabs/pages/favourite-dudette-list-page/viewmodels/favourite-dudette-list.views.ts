import { types } from "mobx-state-tree"
import { FavouriteDudetteListPropsModel } from "./favourite-dudette-list.models"
import { TestIDResources } from "../../../../../constants/test-key/test.constant"
import * as Utils from "../../../../../utils"
import { GeneralResourcesStoreModel } from "../../../../../models/general-resources-store"

export const FavouriteDudetteListViews = types.model(FavouriteDudetteListPropsModel)
    .views(self => ({
        get getFavouriteDudetteList() {
            return self.favouriteDudetteList$
        },
        get getLastId(): string {
            return self.last_id
        },
        get getIsPullRefreshing(): boolean {
            return self.isPullRefreshing
        },
        get getIsLoadMore(): boolean {
            return self.isLoadMore
        },
        get getIsFirstLoadDone(): boolean {
            return self.isFirstLoadDone
        },
        get getIsLoadDone(): boolean {
            return self.isLoadDone
        },
        get getShouldLoadmore(): boolean {
            return !self.isLoadDone && !self.isLoadMore && self.favouriteDudetteList$?.length > 0 && self.isFirstLoadDone
        }
    }))

export const FavouriteDudetteListResourcesViews = GeneralResourcesStoreModel
    .views(self => {

        //MARK: Volatile State

        //MARK: Views
        const getTestIDImageProfile = () => Utils.getTestIDObject(TestIDResources.FavouriteListScreen.imageProfile)
        const getTestIDFullNameTitle = () => Utils.getTestIDObject(TestIDResources.FavouriteListScreen.fullName)
        const getTestIDCommentCountTitle = () => Utils.getTestIDObject(TestIDResources.FavouriteListScreen.commentCountTitle)
        const getTestIDUnreadCommentCountTitle = () => Utils.getTestIDObject(TestIDResources.FavouriteListScreen.unreadCommentCountTitle)
        const getTestIDViewCountTitle = () => Utils.getTestIDObject(TestIDResources.FavouriteListScreen.viewCountTitle)
        return {
            getTestIDImageProfile,
            getTestIDFullNameTitle,
            getTestIDCommentCountTitle,
            getTestIDUnreadCommentCountTitle,
            getTestIDViewCountTitle
        }
    })
