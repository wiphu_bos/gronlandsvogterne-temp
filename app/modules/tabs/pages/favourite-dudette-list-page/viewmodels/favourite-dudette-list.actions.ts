import { types, unprotect } from "mobx-state-tree"
import { FavouriteDudetteListPropsModel } from "./favourite-dudette-list.models"
import { IUser, UserStoreModel } from "../../../../../models/user-store"
import * as DataUtils from "../../../../../utils/data.utils"
import { RootStore } from "../../../../../models/root-store/root.types"

export const FavouriteDudetteListActions = types.model(FavouriteDudetteListPropsModel).actions(self => {
    const setFavouriteDudetteList = (rootStore: RootStore, list: IUser[]) => {
        list.forEach(e => {
            const dudeProfileObject = DataUtils.getUserProfileObject(rootStore, null, e)
            const childViewModel = UserStoreModel.create(dudeProfileObject)
            unprotect(childViewModel)
            self.favouriteDudetteList$.push(childViewModel)
        })
        self.isPullRefreshing = false
        self.isLoadMore = false
    }
    const findExistingItemIndex = (dudette: IUser): number => {
        const tempFavDudeList = self.favouriteDudetteList$
        const existingList = tempFavDudeList?.slice() //Slice to disobserve property
        const foundItemIndex = existingList?.findIndex(v => v.dudette_id === dudette?.dudette_id)
        return foundItemIndex
    }
    const addDudetteInList = (rootStore: RootStore, dudette: IUser) => {
        const foundItemIndex = findExistingItemIndex(dudette)
        if (foundItemIndex !== -1) return
        const tempFavDudeList = self.favouriteDudetteList$
        const dudeProfileObject = DataUtils.getUserProfileObject(rootStore, null, dudette)
        const childViewModel = UserStoreModel.create(dudeProfileObject)
        unprotect(childViewModel)
        self.favouriteDudetteList$ = [].concat.apply(childViewModel, tempFavDudeList)
    }
    const updateDudetteInList = (rootStore: RootStore, dudette: IUser) => {
        const foundItemIndex = findExistingItemIndex(dudette)
        if (foundItemIndex === -1) return
        const dudetteProfileObject = DataUtils.getUserProfileObject(rootStore, null, dudette)
        self.favouriteDudetteList$[foundItemIndex].bindingDataFromObject(dudetteProfileObject)
        rootStore?.getUserStore?.saveFavDudetteToList(dudette)
    }
    const removeDudetteInList = (rootStore: RootStore, dudette: IUser) => {
        const foundItemIndex = findExistingItemIndex(dudette)
        if (foundItemIndex === -1) return
        self.favouriteDudetteList$.splice(foundItemIndex, 1)
        rootStore?.getUserStore?.removeFavDudetteFromList(dudette)
    }
    const setIsPullRefreshing = (value: boolean) => self.isPullRefreshing = value
    const setIsLoadMore = (value: boolean) => self.isLoadMore = value
    const setIsFirstLoadDone = (value: boolean) => self.isFirstLoadDone = value
    const setIsLoadDone = (value: boolean) => self.isLoadDone = value
    const resetFetching = (rootStore: RootStore) => {
        rootStore?.getUserStore?.setFavDudetteList([])
        self.favouriteDudetteList$.clear()
        self.isFirstLoadDone = false
        self.isLoadDone = false
        self.isLoadMore = false
    }
    return {
        addDudetteInList,
        updateDudetteInList,
        removeDudetteInList,
        setIsPullRefreshing,
        resetFetching,
        setIsLoadMore,
        setFavouriteDudetteList,
        setIsLoadDone,
        setIsFirstLoadDone
    }
})