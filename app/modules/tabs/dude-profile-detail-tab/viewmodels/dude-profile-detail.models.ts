import { types } from "mobx-state-tree"

export const DudeProfileDetailPropsModel = {
    isFocusedInformationScreen: types.optional(types.boolean, true),
    isFocusedCommentScreen: types.optional(types.boolean, false),
    maxTabCanGo: types.optional(types.number, 0),
    isShowedImageProfile: types.optional(types.boolean, false),
    currentTab: types.optional(types.number, 0),
} 