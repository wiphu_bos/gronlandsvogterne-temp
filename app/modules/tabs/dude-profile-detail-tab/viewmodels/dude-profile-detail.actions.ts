import { types } from "mobx-state-tree"
import { DudeProfileDetailPropsModel } from "./dude-profile-detail.models"
export const DudeProfileDetailActions = types.model(DudeProfileDetailPropsModel).actions(self => {

    //MARK: Volatile State

    //MARK: Actions
    const setMaxTabCanGo = (value: number, isFavourite: boolean, reset: boolean = false) => {
        if (self.maxTabCanGo <= value || reset)
            self.maxTabCanGo = value
        if (value >= 2 || isFavourite) {
            self.isShowedImageProfile = true
        } else {
            self.isShowedImageProfile = false
        }
    }
    const setIsFocusedCommentScreen = (value: boolean) => {
        self.isFocusedCommentScreen = value
    }
    const setIsFocusedInformationScreen = (value: boolean) => {
        self.isFocusedInformationScreen = value
    }
    const setCurrentTab = (value: number) => {
        self.currentTab = value
    }
    const setIsShowedImageProfile = (value: boolean) => {
        self.isShowedImageProfile = value
    }

    return {
        setMaxTabCanGo,
        setIsFocusedCommentScreen,
        setIsShowedImageProfile,
        setIsFocusedInformationScreen,
        setCurrentTab
    }
})