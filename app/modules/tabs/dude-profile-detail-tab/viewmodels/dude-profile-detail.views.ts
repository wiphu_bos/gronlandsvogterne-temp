import { types } from "mobx-state-tree"
import { DudeProfileDetailPropsModel } from "./dude-profile-detail.models"
import { GeneralResourcesStoreModel } from "../../../../models/general-resources-store/general-resources.store"
import { TestIDResources } from "../../../../constants/test-key/test.constant"
import * as Utils from "../../../../utils"
import { GeneralResources } from "../../../../constants/firebase/remote-config"
export const DudeProfileDetailViews = types.model(DudeProfileDetailPropsModel)
    .views(self => ({
        get getMaxTabCanGo() {
            return self.maxTabCanGo
        },
        get getIsFocusedCommentScreen() {
            return self.isFocusedCommentScreen
        },
        get getIsFocusedInformationScreen() {
            return self.isFocusedInformationScreen
        },
        get getIsShowedImageProfile() {
            return self.isShowedImageProfile
        },
        get getCurrentTab() {
            return self.currentTab
        }
    }))


export const DudeProfileDetailResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State

    //MARK: Views
    const { DudeProfileDetailScreen } = GeneralResources
    const { enterTextPlaceholder, thumbsUpTitle, thumbsDownTitle } = DudeProfileDetailScreen
    const getShareResources = (key: string) => self.getValues(key, true)
    const getResourcesEnterTextPlaceholder = () => getShareResources(enterTextPlaceholder)
    const getResourcesThumbsUpTitle = () => getShareResources(thumbsUpTitle)
    const getResourcesThumbsDownTitle = () => getShareResources(thumbsDownTitle)
    return {
        getResourcesEnterTextPlaceholder,
        getResourcesThumbsUpTitle,
        getResourcesThumbsDownTitle
    }
})

    .views(self => {

        //MARK: Volatile State

        //MARK: Views

        const getTestIDFullNameAndAgeTitle = () => Utils.getTestIDObject(TestIDResources.DudeProfileTabScreen.fullNameWithAgeTitle)
        const getTestIDCityTitle = () => Utils.getTestIDObject(TestIDResources.DudeProfileTabScreen.cityTitle)
        const getTestIDProfileImage = () => Utils.getTestIDObject(TestIDResources.DudeProfileTabScreen.imgProfile)
        const getTestIDButtonSubmitTitle = () => Utils.getTestIDObject(TestIDResources.DudeProfileTabScreen.buttonSubmit)
        const getTestIDInputMessage = () => Utils.getTestIDObject(TestIDResources.DudeProfileTabScreen.buttonThumbsDown)
        const getTestIDButtonThumbsDown = () => Utils.getTestIDObject(TestIDResources.DudeProfileTabScreen.buttonThumbsDown)
        const getTestIDButtonThumbsUp = () => Utils.getTestIDObject(TestIDResources.DudeProfileTabScreen.buttonThumbsUp)
        const getTestIDThumbsDownTitle = () => Utils.getTestIDObject(TestIDResources.DudeProfileTabScreen.thumbsDownTitle)
        const getTestIDThumbsUpTitle = () => Utils.getTestIDObject(TestIDResources.DudeProfileTabScreen.thumbsUpTitle)

        return {
            getTestIDFullNameAndAgeTitle,
            getTestIDCityTitle,
            getTestIDProfileImage,
            getTestIDButtonSubmitTitle,
            getTestIDInputMessage,
            getTestIDButtonThumbsDown,
            getTestIDButtonThumbsUp,
            getTestIDThumbsDownTitle,
            getTestIDThumbsUpTitle
        }
    })