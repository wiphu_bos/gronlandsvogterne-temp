// MARK: Import

import React, { useRef } from "react"
import { observer } from "mobx-react-lite"
import { KeyboardAvoidingView } from "react-native"
import { Wallpaper, Header, AnimatedScrollView } from "../../../components"
import { DudeProfileProps } from "./dude-profile-detail.props"
import DudeProfileDetailController from "./dude-profile-detail.controllers"
import { SafeAreaView } from "react-navigation"
import { useConfigurate } from "../../../custom-hooks/use-configure-controller"
import { DudeProfileDetailTapComponents } from "../../../navigation/tabs/dude-profile-detail-tab/dude-profile-detail-tab"
import { INavigationRoute } from "../../../models/navigation-store/navigation.types"
import { images } from "../../../theme/images"
import * as metric from "../../../theme"
import { ProfileTitleComponents } from "../../../components/profile-title/profile-title"
import { DudeProfileFooterComponent } from "./views/dude-profile-footer"
import { ChatInputComponents } from "../../../components/chat-input/chat-input"

// MARK: Style Import

import * as Styles from "./dude-profile-detail.styles"

export const DudeProfileDetailScreen: React.FunctionComponent<DudeProfileProps & Partial<INavigationRoute>> = observer((props) => {

    const controller = useConfigurate(DudeProfileDetailController, props) as DudeProfileDetailController
    const scrollViewRef = useRef(null)
    const textInputRef = useRef(null)

    // MARK: Render

    return (
        <Wallpaper >
            <SafeAreaView shouldRasterizeIOS style={Styles.SAFE_AREA_VIEW}>
                <Header {...Styles.HEADER_PROPS} {...controller.getHeaderLeftOptions}
                    rightIconSource={DudeProfileDetailController.viewModel?.getIsFavourite ? images.starFav : images.starUnFav}
                    rightIconStyle={Styles.MENU_ICON_STYLE}
                    onRightPress={controller.onFavouritePress} />
                <KeyboardAvoidingView style={Styles.AVOID_KEYBOARD_VIEW} behavior={metric.keyboardBehavior} keyboardVerticalOffset={metric.keyboardVerticalOffSet}>
                    <AnimatedScrollView
                        setRef={scrollViewRef}
                        keyboardDismissMode="on-drag"
                        contentContainerStyle={Styles.SCROLL_VIEW}
                        {...metric.solidScrollView}>

                        <ProfileTitleComponents
                            containerStyle={Styles.PROFILE_TITLE_CONTAINER}
                            isDude={true}
                            displayName={[DudeProfileDetailController.viewModel?.getFullName, DudeProfileDetailController.viewModel?.getAge?.value].join(', ')}
                            displayNameTestID={DudeProfileDetailController.resourcesViewModel?.getTestIDFullNameAndAgeTitle()}
                            displayCity={DudeProfileDetailController.viewModel?.getCity?.value}
                            displayCityTestID={DudeProfileDetailController.resourcesViewModel?.getTestIDCityTitle()}
                            isShowProfileImage={DudeProfileDetailController.viewModel?.getIsShowedImageProfile}
                            profileImageTestID={DudeProfileDetailController.resourcesViewModel?.getTestIDProfileImage}
                            profileImage={(DudeProfileDetailController.viewModel?.getProfileImage[0]) as any}
                        />

                        <DudeProfileDetailTapComponents tabController={controller} numberOfTabsEnabled={DudeProfileDetailController.viewModel?.getMaxTabCanGo}/>

                        {DudeProfileDetailController.viewModel?.getIsFocusedCommentScreen &&
                            <ChatInputComponents
                                textInputTestID={DudeProfileDetailController.resourcesViewModel?.getTestIDInputMessage()}
                                buttonSubmitTestID={DudeProfileDetailController.resourcesViewModel?.getTestIDButtonSubmitTitle()}
                                placeholder={DudeProfileDetailController.resourcesViewModel?.getResourcesEnterTextPlaceholder()}
                                textInputRef={textInputRef}
                                onFocus={() => controller.onFocused(scrollViewRef)}
                                onChangeText={controller.onMessageChangeText}
                                onBlur={() => controller.onBlur(scrollViewRef)}
                                onSubmit={() => controller.onSubmitComment(textInputRef)}
                                buttonSubmitIconSource={images.chatCirclePink}
                            />}
                        {DudeProfileDetailController.viewModel?.getIsFocusedInformationScreen &&
                            <DudeProfileFooterComponent
                                onThumbsUpPress={controller.onThumbsUpPress}
                                thumbsDownText={DudeProfileDetailController.resourcesViewModel?.getResourcesThumbsDownTitle()}
                                thumbsDownButtonTestID={DudeProfileDetailController.resourcesViewModel?.getTestIDButtonThumbsDown()}
                                thumbsUpButtonTestID={DudeProfileDetailController.resourcesViewModel?.getTestIDButtonThumbsUp()}
                                thumbsDownTextTestID={DudeProfileDetailController.resourcesViewModel?.getTestIDThumbsDownTitle()}
                                thumbsUpTextTestID={DudeProfileDetailController.resourcesViewModel?.getTestIDThumbsUpTitle()}
                                onThumbsDownPress={controller.onThumbsDownPress}
                                thumbsUpText={DudeProfileDetailController.resourcesViewModel?.getResourcesThumbsUpTitle()} />}
                    </AnimatedScrollView>

                </KeyboardAvoidingView >

            </SafeAreaView>
        </Wallpaper >
    )

})

