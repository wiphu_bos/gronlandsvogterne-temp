import { ViewStyle, ImageStyle, TextStyle } from "react-native"
import * as metric from "../../../../../theme"
import { color } from "../../../../../theme"
import { RFValue } from "react-native-responsive-fontsize"
export const CONTAINNER: ViewStyle = {
    paddingHorizontal: metric.ratioWidth(33),
    width: '100%',
    position: 'absolute',
    bottom: 0,
    height: metric.ratioHeight(80),
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: metric.ratioHeight(60),
}

export const CONTAINNER_BUTTON: ViewStyle = {
    alignItems: "center"
}

export const THUMB: ImageStyle = {
    width: metric.ratioWidth(80),
    height: metric.ratioWidth(80),
}

export const TEXT: TextStyle = {
    color: color.palette.white,
    fontSize: RFValue(14),
    fontWeight: '300',

    //Android
    fontFamily: "Montserrat-Light",
}