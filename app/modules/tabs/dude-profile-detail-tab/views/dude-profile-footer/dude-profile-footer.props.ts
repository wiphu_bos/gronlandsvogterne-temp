import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"

export interface DudeProfileFooterProps extends NavigationContainerProps<ParamListBase> {
    containerTestID?: any,
    onThumbsDownPress?: () => void,
    onThumbsUpPress?: () => void,
    thumbsUpText?: string,
    thumbsDownText?: string,
    thumbsDownTextTestID?: any,
    thumbsUpTextTestID?: any,
    thumbsDownButtonTestID?: any,
    thumbsUpButtonTestID?: any
}