// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { DudeProfileFooterProps } from "./dude-profile-footer.props"
import * as Styles from "./dude-profile-footer.styles"
import { View } from "react-native"
import { images } from "../../../../../theme/images"
import { Button, Text } from "../../../../../components"

export const DudeProfileFooterComponent: React.FunctionComponent<DudeProfileFooterProps> = observer((props) => {

    // MARK: Render

    return (
        <View shouldRasterizeIOS style={Styles.CONTAINNER} {...props?.containerTestID}>
            <View style={Styles.CONTAINNER_BUTTON}>
                <Button imageSource={images.thumbsDown} imageStyle={Styles.THUMB} preset="none" onPress={props?.onThumbsDownPress} {...props?.thumbsDownButtonTestID}/>
                <Text text={props?.thumbsDownText} style={Styles.TEXT} {...props?.thumbsDownTextTestID} />
            </View>
            <View style={Styles.CONTAINNER_BUTTON}>
                <Button imageSource={images.thumbsUp} imageStyle={Styles.THUMB} preset="none" onPress={props?.onThumbsUpPress} {...props?.thumbsUpButtonTestID}/>
                <Text text={props?.thumbsUpText} style={Styles.TEXT} {...props?.thumbsUpTextTestID} />
            </View>
        </View>
    )
})