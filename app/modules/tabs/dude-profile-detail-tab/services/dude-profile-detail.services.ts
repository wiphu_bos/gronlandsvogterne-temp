import { types } from "mobx-state-tree"
import { DudeProfileDetailPropsModel } from "../viewmodels"
import { UserStoreModel } from "../../../../models/user-store"
import DudeServices from "../../../../services/api-domain/dude.services"
import * as FireStoreConstant from "../../../../constants/firebase/fire-store.constant"

export const DudeProfileDetailServiceActions = types.compose(types.model(DudeProfileDetailPropsModel), UserStoreModel).actions(self => {

    /*

        No need to use `flow` and `yield` because we don't care about any responses.

   */

    const doFavouriteDude = (dudeId: string) => {
        const params = {
            uid: dudeId
        }
        DudeServices.doFavouriteDude(params)
    }

    const increaseViewCountDude = (dudeId: string) => {
        const params = {
            uid: dudeId,
            field: FireStoreConstant.Property.VIEW_COUNT
        }
        DudeServices.increaseViewCount(params)
    }

    return { doFavouriteDude, increaseViewCountDude }
})