import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { UserStore } from '../../../models/user-store/user.types'

export interface DudeProfileProps extends NavigationContainerProps<ParamListBase> {
    dudeProfileObject: UserStore,
    shouldGoToCommentTab: boolean
}