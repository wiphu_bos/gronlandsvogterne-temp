import { Instance, SnapshotOut } from "mobx-state-tree"
import { DudeProfileStoreModel } from "./storemodels"

export type DudeProfileStore = Instance<typeof DudeProfileStoreModel>
export type DudeProfileStoreSnapshot = SnapshotOut<typeof DudeProfileStoreModel>