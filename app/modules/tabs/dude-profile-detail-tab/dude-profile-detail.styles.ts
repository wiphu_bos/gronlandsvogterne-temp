import { ViewStyle, ImageStyle } from "react-native"
import * as metric from "../../../theme"
import { images } from "../../../theme/images"
import { HeaderProps } from "../../../components/header/header.props"

export const FULL: ViewStyle = {
    flex: 1
}

export const SAFE_AREA_VIEW: ViewStyle = {
    ...FULL
}

export const MENU_ICON_STYLE: ImageStyle = {
    width: metric.ratioWidth(25),
    height: metric.ratioHeight(25),
    resizeMode: null
}

export const PROFILE_TITLE_CONTAINER: ViewStyle = {
    marginHorizontal: metric.ratioWidth(30),
}

export const HEADER_PROPS: HeaderProps = {
    leftIconSource: images.back,
    leftIconStyle: MENU_ICON_STYLE,
    isLeftIconAnimated: false,
    isRightIconAnimated: false,
    containerStyle: {
        height: metric.ratioHeight(59)
    }
}

export const AVOID_KEYBOARD_VIEW: ViewStyle = {
    ...FULL
}

export const SCROLL_VIEW: ViewStyle = {
    flexGrow: 1,
    height: metric.screenHeight * 0.85,
    justifyContent: "center"
}