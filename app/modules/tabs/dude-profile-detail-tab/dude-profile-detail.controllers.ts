// MARK: Import
import { Keyboard } from "react-native"
import { NavigationKey, SearchListType } from "../../../constants/app.constant"
import { DudeProfileProps } from "./dude-profile-detail.props"
import { DudeProfileStoreModel, DudeProfileResourcesStoreModel } from "./storemodels/dude-profile-detail.store"
import { RootStore, INavigationRoute, IRoute } from "../../../models"
import { onSnapshot } from "mobx-state-tree"
import { DudeProfileStore } from "./dude-profile-detail.types"
import { HeaderProps } from "../../../components/header/header.props"
import { DudeProfileResourcesStore } from "./storemodels/dude-profile-detail.types"
import DudeProfileFourthPageController from "../pages/dude-profile-page/page-4/dude-profile-fourth-page.controllers"
import { FirebaseFirestoreTypes } from "@react-native-firebase/firestore"
import { IDudeProfileListener, IFavouriteListListener } from "../../../utils/firebase/fire-store/fire-store.types"
import FirebaseFireStore from "../../../utils/firebase/fire-store/fire-store"
import * as DataUtils from "../../../utils/data.utils"
import { IUser, UserStore, IFavouriteResponse } from "../../../models/user-store"
import { SnapshotChangeType } from "../../../constants/firebase/fire-store.constant"
import { TabController } from "../../tab.controller"
import { DrawerActions } from "@react-navigation/native"

class DudeProfileDetailController extends TabController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static resourcesViewModel: DudeProfileResourcesStore
    public static viewModel: DudeProfileStore
    public static myProps: DudeProfileProps & Partial<INavigationRoute>
    private static _favouriteDisposer: () => void
    private static _dudeProfileDisposer: () => void
    private _message: string
    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, props?: DudeProfileProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, props, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(props)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        DudeProfileDetailController.resourcesViewModel = DudeProfileResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        if (!DudeProfileDetailController.myProps?.dudeProfileObject) return
        DudeProfileDetailController.viewModel = DudeProfileStoreModel.create(DudeProfileDetailController.myProps?.dudeProfileObject)
    }

    private _setupSnapShot = () => {
        onSnapshot(DudeProfileDetailController.viewModel, (snap: DudeProfileStore) => {
            this._setInitializedToPropsParams()
        })
    }

    private _initTab = () => {
        DudeProfileDetailController.viewModel?.getIsFavourite ? this._enableAllTab() : this._resetTab()
    }

    //@override
    _setupProps = (myProps: DudeProfileProps & Partial<INavigationRoute>) => {
        super._setupProps && super._setupProps(myProps)
        DudeProfileDetailController.myProps = {
            ...myProps,
            ...myProps?.route?.params
        }
    }

    /*
       Mark Data
   */

    /*
       Mark Event
   */

    //@override
    onPress = (route: any, index: number) => {
        Keyboard.dismiss()
        const currentState = this._tabState?.index
        const isFocused = this._tabProps?.state?.index === index
        const event = this._tabProps?.navigation?.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true
        })
        if (index <= currentState || index <= DudeProfileDetailController.viewModel?.getMaxTabCanGo) {
            if (!isFocused && !event.defaultPrevented) {
                let routeParam: object
                const isFourthStack = route?.name === NavigationKey.DudeProfileDetailFourthStack
                DudeProfileDetailController.viewModel?.setIsFocusedCommentScreen(isFourthStack)
                DudeProfileDetailController.viewModel?.setIsFocusedInformationScreen(!isFourthStack)
                if (isFourthStack) {
                    routeParam = {
                        screen: NavigationKey.DudeProfileDetailFourth
                    }
                }
                this._tabProps?.navigation?.navigate(route?.name, routeParam)
            }
            DudeProfileDetailController.viewModel?.setCurrentTab(index)
            DudeProfileDetailController.viewModel?.setMaxTabCanGo(index, DudeProfileDetailController.viewModel.getIsFavourite)
        }
    }

    public onThumbsUpPress = () => {
        const routes: IRoute[] = this._tabState?.routes
        const currentIndex = this._tabState?.index
        const next = currentIndex + 1
        if (next > routes?.length)
            return null
        DudeProfileDetailController.viewModel?.setMaxTabCanGo(next, DudeProfileDetailController.viewModel.getIsFavourite)
        this.onPress(routes[next], next)
    }

    public onThumbsDownPress = () => {
        this.backProcess()
    }

    private _enableAllTab = () => {
        DudeProfileDetailController.viewModel?.setMaxTabCanGo(this._tabState?.routes?.length, DudeProfileDetailController.viewModel.getIsFavourite)
    }

    private _resetTab = async () => {
        const current = DudeProfileDetailController.viewModel.getCurrentTab
        DudeProfileDetailController.viewModel?.setMaxTabCanGo(current, DudeProfileDetailController.viewModel.getIsFavourite, true)
    }

    public getHeaderLeftOptions: HeaderProps = {
        onLeftPress: this.backProcess
    }

    public onFavouritePress = () => {
        if (!DudeProfileDetailController.viewModel?.getIsFavourite) {
            DudeProfileDetailController.viewModel?.setIsFavourite(true)
            this._enableAllTab()
        } else {
            DudeProfileDetailController.viewModel?.setIsFavourite(false)
            this._resetTab()
        }
        DudeProfileDetailController.viewModel?.doFavouriteDude(DudeProfileDetailController.viewModel?.getUserId)
    }
    private _onFavouriteListener = () => {
        const onNext: (snapshot: FirebaseFirestoreTypes.QuerySnapshot) => void = (snap => {
            if (snap.metadata.hasPendingWrites) return
            snap.docChanges().forEach(change => {
                const favData: IFavouriteResponse = change.doc?.data()
                if (favData?.dude_id === DudeProfileDetailController.viewModel?.getUserId) {
                    if (change.type === SnapshotChangeType.ADDED) {
                        if (DudeProfileDetailController.viewModel?.setIsFavourite) {
                            this._enableAllTab()
                            DudeProfileDetailController.viewModel?.setIsFavourite(true)
                        }
                    } else if (change.type === SnapshotChangeType.REMOVED) {
                        if (!DudeProfileDetailController.viewModel?.setIsFavourite) {
                            this._resetTab()
                            DudeProfileDetailController.viewModel?.setIsFavourite(false)
                        }
                    }
                }
            })
        })

        const onError: (e: Error) => void = (e => {
        })

        const onComplete: () => void = () => {
        }

        const params: IFavouriteListListener = {
            userId: this._rootStore?.getUserStore?.getUserId,
            type: SearchListType.Dude,
            onNext,
            onError,
            onComplete
        }

        DudeProfileDetailController._favouriteDisposer = FirebaseFireStore.onFavouriteListener(params)
    }


    private _onDudeProfileListener = () => {
        const onNext: (snapshot: FirebaseFirestoreTypes.QueryDocumentSnapshot) => void = (snap => {
            if (snap.metadata.hasPendingWrites) return
            const dudeProfile: Partial<IUser> = snap.data()
            if (!dudeProfile) return
            const dudeProfileObject: UserStore = DataUtils.getUserProfileObject(this._rootStore, DudeProfileDetailController.viewModel, dudeProfile)
            DudeProfileDetailController.viewModel?.setFullName(dudeProfileObject?.full_name)
            DudeProfileDetailController.viewModel?.setAge(dudeProfileObject?.age)
            DudeProfileDetailController.viewModel?.setCity(dudeProfileObject?.city)
        })

        const onError: (e: Error) => void = (e => {
        })

        const onComplete: () => void = () => {
        }

        const params: IDudeProfileListener = {
            creatorId: DudeProfileDetailController.viewModel?.getCreatorId,
            dudeId: DudeProfileDetailController.viewModel?.getUserId,
            onNext,
            onError,
            onComplete
        }

        DudeProfileDetailController._dudeProfileDisposer = FirebaseFireStore.onDudeProfileListener(params)
    }
    public onFocused = (scrollViewRef?: React.MutableRefObject<any>) => {
        scrollViewRef?.current?.getNode().scrollTo({ x: 0, y: 750, animated: true })
    }

    public onBlur = (scrollViewRef?: React.MutableRefObject<any>) => {
        scrollViewRef?.current?.getNode().scrollTo({ x: 0, y: 0, animated: true })
    }

    public onMessageChangeText = (value: string) => this._message = value

    public onSubmitComment = async (textInputRef?: React.MutableRefObject<any>) => {
        if (!this._message) return
        textInputRef?.current?.clear()
        await DudeProfileFourthPageController.onSubmitComment(this._message)
        this._message = null
        Keyboard.dismiss()
    }

    private _goToCommentTab = () => {
        const routes: IRoute[] = this._tabState?.routes
        const currentIndex = this._tabState?.index
        const next = currentIndex + 3
        const validNext = next > 3 ? 3 : next
        if (next > routes?.length)
            return null
        this._enableAllTab()
        this._delayExecutor(() => this.onPress(routes[validNext], validNext), 1000)
    }

    //@override
    backProcess = () => {
        if (this._rootStore?.getNavigationStore?.getPreviousPageName === NavigationKey.SearchListScreen) {
            const drawerActions: any = DrawerActions.jumpTo(NavigationKey.SearchListScreen, {
                isShowFilter: false,
                isFetchData: false
            })
            Promise.all([this._myProps?.navigation?.goBack(), this._myProps?.navigation?.dispatch(drawerActions)])
        } else {
            this._myProps?.navigation?.goBack()
        }
    }

    /*
       Mark Life cycle
   */

    //@override
    viewDidAppearOnce = async () => {
        this._onDudeProfileListener()
        this._onFavouriteListener()
        this._initTab()
    }
    public viewDidAppearAfterFocus = async (newProps?: DudeProfileProps & Partial<INavigationRoute>) => {
        const shouldGoToCommentTab = DudeProfileDetailController.myProps?.shouldGoToCommentTab
        if (shouldGoToCommentTab) this._goToCommentTab()
        DudeProfileDetailController.myProps = {
            ...DudeProfileDetailController.myProps,
            shouldGoToCommentTab: false
        }
        super.viewDidAppearAfterFocus && super.viewDidAppearAfterFocus()
    }

    //@override
    deInit = () => {
        super.deInit && super.deInit()
        DudeProfileDetailController._dudeProfileDisposer && DudeProfileDetailController._dudeProfileDisposer()
        DudeProfileDetailController._favouriteDisposer && DudeProfileDetailController._favouriteDisposer()
    }
}


export default DudeProfileDetailController