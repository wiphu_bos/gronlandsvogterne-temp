import { types } from "mobx-state-tree"
import { DudeProfileDetailViews, DudeProfileDetailActions, DudeProfileDetailResourcesViews } from "../viewmodels"
import { DudeProfileDetailPropsModel } from "../viewmodels"
import { DudeProfileDetailServiceActions } from "../services/dude-profile-detail.services"
import { StoreName } from "../../../../constants/app.constant"
import { GeneralResourcesStoreModel } from "../../../../models/general-resources-store/general-resources.store"
import { UserStoreModel } from "../../../../models/user-store/user.store"
import { ValidationStoreModel } from "../../../../models/validation-store/validation-store.store"

const DudeProfileDetailModel = types.model(StoreName.DudeProfileDetailTab, DudeProfileDetailPropsModel)

export const DudeProfileStoreModel = types.compose(
    UserStoreModel,
    ValidationStoreModel,
    DudeProfileDetailModel,
    DudeProfileDetailViews,
    DudeProfileDetailActions,
    DudeProfileDetailServiceActions)

    export const DudeProfileResourcesStoreModel = types.compose(GeneralResourcesStoreModel, DudeProfileDetailResourcesViews)

