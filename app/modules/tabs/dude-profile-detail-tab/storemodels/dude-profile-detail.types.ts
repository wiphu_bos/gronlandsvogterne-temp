import { Instance, SnapshotOut } from "mobx-state-tree"
import { DudeProfileStoreModel, DudeProfileResourcesStoreModel } from "./dude-profile-detail.store"

export type DudeProfileStore = Instance<typeof DudeProfileStoreModel>
export type DudeProfileStoreSnapshot = SnapshotOut<typeof DudeProfileStoreModel>

export type DudeProfileResourcesStore = Instance<typeof DudeProfileResourcesStoreModel>
export type DudeProfileResourcesStoreSnapshot = SnapshotOut<typeof DudeProfileResourcesStoreModel>