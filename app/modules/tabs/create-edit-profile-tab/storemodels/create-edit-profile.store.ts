import { types } from "mobx-state-tree"
import { CreateEditProfileViews, CreateEditProfileActions, CreateEditProfileResourcesViews } from "../viewmodels"
import { CreateEditProfilePropsModel } from "../viewmodels"
import { CreateEditProfileServiceActions } from "../services/create-edit-profile.services"
import { StoreName } from "../../../../constants/app.constant"
import { GeneralResourcesStoreModel } from "../../../../models/general-resources-store/general-resources.store"
import { UserStoreModel } from "../../../../models/user-store/user.store"
import { ValidationStoreModel } from "../../../../models/validation-store/validation-store.store"

const CreateEditProfileModel = types.model(StoreName.CreateEditProfileTab, CreateEditProfilePropsModel)

export const CreateEditProfileStoreModel = types.compose(
    UserStoreModel,
    ValidationStoreModel,
    CreateEditProfileModel,
    CreateEditProfileViews,
    CreateEditProfileActions,
    CreateEditProfileServiceActions)

    export const CreateEditProfileResourcesStoreModel = types.compose(GeneralResourcesStoreModel, CreateEditProfileResourcesViews)

