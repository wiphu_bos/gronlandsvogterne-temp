import { Instance, SnapshotOut } from "mobx-state-tree"
import { CreateEditProfileStoreModel, CreateEditProfileResourcesStoreModel } from "./create-edit-profile.store"

export type CreateEditProfileStore = Instance<typeof CreateEditProfileStoreModel>
export type CreateEditProfileStoreSnapshot = SnapshotOut<typeof CreateEditProfileStoreModel>

export type CreateEditProfileResourcesStore = Instance<typeof CreateEditProfileResourcesStoreModel>
export type CreateEditProfileResourcesStoreSnapshot = SnapshotOut<typeof CreateEditProfileResourcesStoreModel>