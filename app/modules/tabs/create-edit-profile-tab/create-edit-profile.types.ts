import { Instance, SnapshotOut } from "mobx-state-tree"
import { CreateEditProfileStoreModel } from "./storemodels"

export type CreateEditProfileStore = Instance<typeof CreateEditProfileStoreModel>
export type CreateEditProfileStoreSnapshot = SnapshotOut<typeof CreateEditProfileStoreModel>