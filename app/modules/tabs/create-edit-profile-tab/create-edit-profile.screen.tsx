// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { Wallpaper, Header } from "../../../components"
import { CreateEditProfileProps } from "./create-edit-profile.props"
import CreateEditProfileController from "./create-edit-profile.controllers"
import { SafeAreaView } from "react-navigation"
import { useConfigurate } from "../../../custom-hooks/use-configure-controller"
import { CreateEditProfileTapComponents } from "../../../navigation/tabs/create-edit-profile-tab/create-edit-profile-tab"
import { INavigationRoute } from "../../../models/navigation-store/navigation.types"
import * as metric from "../../../theme"

// MARK: Style Import

import * as Styles from "./create-edit-profile.styles"

export const CreateEditProfileScreen: React.FunctionComponent<CreateEditProfileProps & Partial<INavigationRoute>> = observer((props) => {

    const controller = useConfigurate(CreateEditProfileController, props) as CreateEditProfileController

    // MARK: Render
    return (
        <Wallpaper >
            <SafeAreaView shouldRasterizeIOS style={Styles.SAFE_AREA_VIEW} {...metric.safeAreaViewProps}>
                <Header {...Styles.HEADER_PROPS} {...controller.getHeaderLeftOptions} />
                <CreateEditProfileTapComponents tabController={controller} />
            </SafeAreaView>
        </Wallpaper>
    )

})
