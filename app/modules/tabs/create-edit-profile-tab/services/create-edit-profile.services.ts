import { types, flow } from "mobx-state-tree"
import { CreateEditProfilePropsModel } from "../viewmodels"
import { UserStoreModel, IUser, IPaymentResponse, IPublishDudeWithPayment } from "../../../../models/user-store"
import UserServices from "../../../../services/api-domain/user.services"
import { IUploadFileParams, IUploadFileData } from "../../../../utils/firebase/fire-storage/fire-storage.types"
import { RootStore } from "../../../../models"
import * as FirebaseUtils from "../../../../utils/firebase/fire-storage"
import { FileUploadType } from "../../../../constants/app.constant"
import { APISuccessResponse, APIResponse } from "../../../../constants/service.types"
import { GeneralResources } from "../../../../constants/firebase/remote-config/remote-config.constant"
import { FirebaseAuthErrorResponse } from "../../../../constants/firebase/auth/auth-error-response.constant"
import * as StringUtils from "../../../../utils/string.utils"
import { Property } from "../../../../constants/firebase/fire-store.constant"

export const CreateEditProfileServiceActions = types.compose(types.model(CreateEditProfilePropsModel), UserStoreModel).actions(self => {

    const purchaseHighlightDudeIAP = flow(function* (rootStore?: RootStore) {
        try {
            return yield rootStore?.getUserStore?.purchaseHighlightDudeIAP()
        } catch (e) {
            return e
        }
    })

    const _askForPublishDudeProcess = flow(function* (payment?: IPaymentResponse) {
        let params: Partial<IUser> & Partial<IPaymentResponse> = {
            [Property.UID]: self.getUserId,
            [Property.FULL_NAME]: self.getFullName,
            [Property.AGE]: self.getAge?.value,
            [Property.CITY]: self.getCity?.key,
            [Property.HAVD_SOGER_HAN_LIST]: self.getHavdSogerHanList?.map(e => e.key),
            [Property.INTEREST_LIST]: self.getInterestList?.map(e => e.key),
            [Property.TAG_LIST]: self.getTagList?.map(e => e.key),
            [Property.DESCRIPTION]: StringUtils.newLineToBr(self.getDescription),
            [Property.PROFILE_IMAGE]: self.getProfileImageToUpdateDetails,
            [Property.GALLERY_IMAGE_LIST]: self.getGalleryImageToUpdateDetails,
            [Property.IS_HIGHLIGHT]: self.is_highlight
        }

        if (payment) {
            params = {
                ...params,
                [Property.TRANSACTION_ID]: payment?.transactionId,
                [Property.TRANSACTION_RECEIPT]: payment?.transactionReceipt ? payment?.transactionReceipt : payment?.purchaseToken,
                [Property.SKU_ID]: payment?.productId
            }
        }

        return yield UserServices.askForPublishDude(params)
    })
    const _uploadImages = flow(function* (path: string) {
        const galleryImagesList = self.getShouldUploadGalleryImages
        const profileImage = self?.getShouldUploadProfileImage
        const uploadList: any[] = galleryImagesList?.concat(profileImage)
        if (uploadList?.length > 0) {
            const params: IUploadFileParams = {
                fileList: uploadList,
                uid: path
            }
            const uploadedImageList: IUploadFileData[] = yield FirebaseUtils.uploadfiles(params)
            const profileImageList = uploadedImageList?.filter(e => e.type === FileUploadType.ProfileImage)
            const gallertImageList = uploadedImageList?.filter(e => e.type === FileUploadType.GalleryImage)
            if (profileImage.length > 0) self?.setProfileImage(profileImageList)
            if (gallertImageList.length > 0) self?.setGalleryImages(gallertImageList)
        }
    })

    const _deleteImages = flow(function* (path: string) {
        const galleryImagesToRemoveList = self?.getGalleryImagesToRemove
        const profileImageToRemove = self?.getProfileImagesToRemove
        if (galleryImagesToRemoveList.length > 0 || profileImageToRemove.length > 0) {
            const params: IUploadFileParams = {
                fileList: galleryImagesToRemoveList?.concat(profileImageToRemove) as any,
                uid: path
            }
            yield FirebaseUtils.deletefiles(params)
        }
    })

    const askForPublishDude = flow(function* (rootStore: RootStore, path: string, payment?: IPaymentResponse) {
        const isPublishWithPayment: boolean = payment?.productId && payment?.transactionId ? true : false
        const isConnected = rootStore?.getSharedStore?.getIsConnected
        if (!isConnected) {
            const firebaseErrorMessageObject = rootStore?.getGeneralResourcesStore(GeneralResources.SharedKeys.firebaseErrorMessage, true)
            const internetErrorMessage = firebaseErrorMessageObject[FirebaseAuthErrorResponse.NetworkRequestFailed]
            return internetErrorMessage
        }
        try {
            yield _deleteImages(path)
            yield _uploadImages(path)
            const result: APIResponse = yield _askForPublishDudeProcess(payment)
            const success = (result as APISuccessResponse)?.data
            const data: IUser | IPublishDudeWithPayment = success?.data
            if (isPublishWithPayment && !(data as IPublishDudeWithPayment)?.dude_details) throw result
            if (!(data as IUser)) throw result
            return success
        } catch (e) {
            return (e)
        }
    })

    return {
        purchaseHighlightDudeIAP,
        askForPublishDude
    }
})