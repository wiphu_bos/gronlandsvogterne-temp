// MARK: Import

import { NavigationKey, UserType } from "../../../constants/app.constant"
import { CreateEditProfileProps } from "./create-edit-profile.props"
import { CreateEditProfileStoreModel, CreateEditProfileResourcesStoreModel } from "./storemodels/create-edit-profile.store"
import { RootStore, INavigationRoute } from "../../../models"
import { onSnapshot } from "mobx-state-tree"
import { CreateEditProfileStore } from "./create-edit-profile.types"
import { StackActions } from "@react-navigation/core"
import { HeaderProps } from "../../../components/header/header.props"
import { CreateEditProfileResourcesStore } from "./storemodels/create-edit-profile.types"
import * as DataUtils from "../../../utils/data.utils"
import { TabController } from "../../tab.controller"

class CreateEditProfileController extends TabController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static resourcesViewModel: CreateEditProfileResourcesStore
    public static viewModel: CreateEditProfileStore
    public static myProps: CreateEditProfileProps & Partial<INavigationRoute>

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, props?: CreateEditProfileProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, props, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(props)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        CreateEditProfileController.resourcesViewModel = CreateEditProfileResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.CreateEditProfileTab)
        CreateEditProfileController.viewModel = localStore && CreateEditProfileStoreModel.create({ ...localStore }) || CreateEditProfileStoreModel.create({})
    }

    private _setupSnapShot = () => {
        onSnapshot(CreateEditProfileController.viewModel, (snap: CreateEditProfileStore) => {
            this._setInitializedToPropsParams()
        })
    }

    //@override
    _setupProps = (myProps: CreateEditProfileProps & Partial<INavigationRoute>) => {
        super._setupProps && super._setupProps(myProps)
        CreateEditProfileController.myProps = {
            ...myProps,
            ...myProps?.route?.params
        }
    }

    /*
       Mark Data
   */

    private _bindingDudeProfile = () => {
        let dudeProfileObject = CreateEditProfileController.myProps?.dudeProfileObject
        const localDudeProfile = this._rootStore?.getUserStore?.getMyDudeListByUID(dudeProfileObject?.uid)
        if (localDudeProfile) {
            dudeProfileObject = DataUtils.getUserProfileObject(this._rootStore, null, localDudeProfile)
        }
        if (dudeProfileObject) {
            CreateEditProfileController.viewModel?.bindingDataFromObject(dudeProfileObject, UserType.Dude)
        }
    }

    /*
       Mark Event
   */

    public getHeaderLeftOptions: HeaderProps = {
        onLeftPress: this.backProcess
    }

    public onCreateDudeButtonPress = () => {
        this._myProps?.navigation?.dispatch(StackActions.push(NavigationKey.CreateDude) as any)
    }

    /*
       Mark Life cycle
   */

    //@override
    viewDidAppearOnce = async () => {
        this._bindingDudeProfile()
    }

}

export default CreateEditProfileController