import { types } from "mobx-state-tree"
import { CreateEditProfilePropsModel } from "./create-edit-profile.models"
import { GeneralResourcesStoreModel } from "../../../../models/general-resources-store/general-resources.store"
import { UserStoreModel } from "../../../../models/user-store"
import { GeneralResources } from "../../../../constants/firebase/remote-config/remote-config.constant"

export const CreateEditProfileViews = types.compose(types.model(CreateEditProfilePropsModel), UserStoreModel)
    .views(self => ({
        get getIsFirstPageValid() {
            return self.getEmail &&
                self.getAge?.value &&
                self.getCity?.value &&
                self.getFullName &&
                self.getHavdSogerHanList?.length > 0 &&
                self.getInterestList?.length > 0 &&
                self.getTagList?.length >= 3 ? true : false
        },
        get getIsSecondPageValid() {
            return self.getDescription
        },
        get getIsThirdPageValid() {
            return self.getGalleryImages?.length > 0 && self.getProfileImage?.length > 0
        },
        get getIsAllPagesValid() {
            return this.getIsFirstPageValid &&
                this.getIsSecondPageValid &&
                this.getIsThirdPageValid
        }
    }))


export const CreateEditProfileResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State

    const { CreateEditProfileScreen } = GeneralResources
    const {
        alertValidateUserTitle,
        alertValidateUserMessage } = CreateEditProfileScreen

    //MARK: Views
    const getsharedResources = (key: string) => self.getValues(key, true)
    const getResourceAlertValidateUserTitle = () => getsharedResources(alertValidateUserTitle)
    const getResourceAlertValidateUserMessage = () => getsharedResources(alertValidateUserMessage)
    return {
        getResourceAlertValidateUserTitle,
        getResourceAlertValidateUserMessage
    }
})
