import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { UserStore } from '../../../models/user-store/user.types'

export interface CreateEditProfileProps extends NavigationContainerProps<ParamListBase> {
    dudeProfileObject: UserStore
}