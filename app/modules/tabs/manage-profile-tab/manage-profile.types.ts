import { Instance, SnapshotOut } from "mobx-state-tree"
import { ManageProfileStoreModel } from "./storemodels"

export type ManageProfileStore = Instance<typeof ManageProfileStoreModel>
export type ManageProfileStoreSnapshot = SnapshotOut<typeof ManageProfileStoreModel>