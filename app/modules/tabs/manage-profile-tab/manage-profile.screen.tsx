// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { Wallpaper, Header } from "../../../components"
import { ManageProfileProps } from "./manage-profile.props"
import ManageProfileController from "./manage-profile.controllers"
import { SafeAreaView } from "react-navigation"
import { useConfigurate } from "../../../custom-hooks/use-configure-controller"
import * as metric from "../../../theme"
import { ManageProfileTapComponents } from "../../../navigation/tabs/manage-profile-tab/manage-profile-tab"

// MARK: Style Import

import * as Styles from "./manage-profile.styles"

export const ManageProfileScreen: React.FunctionComponent<ManageProfileProps> = observer((props) => {

    const controller = useConfigurate(ManageProfileController, props) as ManageProfileController
    const headerOptions = ManageProfileController.viewModel?.getShouldShowAddDudeMenu && { ...controller.getHeaderRightOptions } 
    // MARK: Render
    return (
        <Wallpaper >
            <SafeAreaView
                {...metric.safeAreaViewProps}
                shouldRasterizeIOS
                style={Styles.SAFE_AREA_VIEW}>
                <Header {...Styles.HEADER_PROPS} {...controller.getHeaderLeftOptions} {...headerOptions}/>
                <ManageProfileTapComponents tabController={controller} />
            </SafeAreaView>
        </Wallpaper>
    )
})
