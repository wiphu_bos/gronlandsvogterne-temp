// MARK: Import

import { NavigationKey } from "../../../constants/app.constant"
import { ManageProfileProps } from "./manage-profile.props"
import { ManageProfileStoreModel, ManageProfileResourcesStoreModel } from "./storemodels/manage-profile.store"
import { RootStore, INavigationRoute } from "../../../models"
import { onSnapshot } from "mobx-state-tree"
import { ManageProfileStore } from "./manage-profile.types"
import { StackActions, DrawerActions } from "@react-navigation/core"
import { HeaderProps } from "../../../components/header/header.props"
import { ManageProfileResourcesStore } from "./storemodels/manage-profile.types"
import { TabController } from "../../tab.controller"

class ManageProfileController extends TabController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static resourcesViewModel: ManageProfileResourcesStore
    public static viewModel: ManageProfileStore
    public static myProps: ManageProfileProps

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, props?: ManageProfileProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, props, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(props)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        ManageProfileController.resourcesViewModel = ManageProfileResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.ManageProfileTab)
        ManageProfileController.viewModel = localStore && ManageProfileStoreModel.create({ ...localStore }) || ManageProfileStoreModel.create({})
    }

    private _setupSnapShot = () => {
        onSnapshot(ManageProfileController.viewModel, (snap: ManageProfileStore) => {
            this._setInitializedToPropsParams()
        })
    }

    //@override
    _setupProps = (props: ManageProfileProps & Partial<INavigationRoute>) => {
        ManageProfileController.myProps = {
            ...props as object,
            ...(props as INavigationRoute)?.route?.params
        }
    }


    /*
       Mark Data
   */

    /*
       Mark Event
   */

    public getHeaderRightOptions:HeaderProps = {
        rightText: ManageProfileController.resourcesViewModel?.getResourceButtonAddTitle(),
        isRightIconAnimated: false,
        onRightPress: () => this.onCreateDudeButtonPress()
    }

    public getHeaderLeftOptions: HeaderProps = {
        onLeftPress: () => this.onSideMenuPress()
    }

    //@override
    _onTabPageChange = () => {
        ManageProfileController.viewModel?.setShouldShowAddDudeMenu(this._isInMyDudeTab())
    }

    public onCreateDudeButtonPress = () => {
        this._myProps?.navigation?.dispatch(StackActions.push(NavigationKey.CreateDude) as any)
    }

    public onSideMenuPress = () => {
        this._myProps?.navigation?.dispatch(DrawerActions.openDrawer() as any)
    }

    /*
       Mark Life cycle
   */
    public viewDidAppearAfterFocus = async (newProps?: ManageProfileProps & Partial<INavigationRoute>) => {
        const shouldShowAddDudeMenu = ManageProfileController.myProps?.shouldShowAddDudeMenu
        if(this._isInMyDudeTab()) ManageProfileController.viewModel?.setShouldShowAddDudeMenu(shouldShowAddDudeMenu)
        super.viewDidAppearAfterFocus && super.viewDidAppearAfterFocus()
    }

    /*
        Helper
    */

    private _isInMyDudeTab = (): boolean => {
        const index = this._tabState?.index
        const routeNames = this._tabState?.routeNames
        return routeNames && index === 1 || false
    }

}

export default ManageProfileController