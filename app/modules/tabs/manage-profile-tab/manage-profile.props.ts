import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
export interface ManageProfileProps extends NavigationContainerProps<ParamListBase> {
    shouldShowAddDudeMenu?: boolean
}