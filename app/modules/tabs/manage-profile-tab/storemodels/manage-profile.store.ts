import { types } from "mobx-state-tree"
import { ManageProfileViews, ManageProfileActions, ManageProfileResourcesViews } from "../viewmodels"
import { ManageProfilePropsModel } from "../viewmodels"
import { ManageProfileServiceActions } from "../services/manage-profile.services"
import { StoreName } from "../../../../constants/app.constant"
import { GeneralResourcesStoreModel } from "../../../../models/general-resources-store/general-resources.store"

const ManageProfileModel = types.model(StoreName.ManageProfileTab, ManageProfilePropsModel)

export const ManageProfileStoreModel = types.compose(
    ManageProfileModel,
    ManageProfileViews,
    ManageProfileActions,
    ManageProfileServiceActions)

    export const ManageProfileResourcesStoreModel = types.compose(GeneralResourcesStoreModel, ManageProfileResourcesViews)

