import { Instance, SnapshotOut } from "mobx-state-tree"
import { ManageProfileStoreModel, ManageProfileResourcesStoreModel } from "./manage-profile.store"

export type ManageProfileStore = Instance<typeof ManageProfileStoreModel>
export type ManageProfileStoreSnapshot = SnapshotOut<typeof ManageProfileStoreModel>

export type ManageProfileResourcesStore = Instance<typeof ManageProfileResourcesStoreModel>
export type ManageProfileResourcesStoreSnapshot = SnapshotOut<typeof ManageProfileResourcesStoreModel>