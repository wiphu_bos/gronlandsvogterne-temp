import { ViewStyle, ImageStyle, TextStyle } from "react-native"
import { color } from "../../../theme"
import * as metric from "../../../theme"
import { images } from "../../../theme/images"
import { HeaderProps } from "../../../components/header/header.props"
import { RFValue } from "react-native-responsive-fontsize"

export const APP_SLOGAN: ViewStyle = {
}

export const TEXT: TextStyle = {
    color: color.palette.white,
    fontFamily: "Montserrat",
}

export const TITLE: TextStyle = {
    ...TEXT,
    lineHeight: metric.ratioHeight(24),
    fontSize: RFValue(19),
    fontWeight: '600',

    //Android
    fontFamily: "Montserrat-SemiBold",
}

export const FULL: ViewStyle = {
    flex: 1
}

export const SAFE_AREA_VIEW: ViewStyle = {
    ...FULL
}

export const CONTAINER_LIST_VIEW: ViewStyle = {
    marginTop: metric.ratioHeight(52)
}
export const TITLE_CONTAINNER: ViewStyle = {
    paddingLeft: metric.ratioWidth(24),
}
export const LIST_VIEW: ViewStyle = {
    paddingHorizontal: metric.ratioWidth(24)
}

export const CONTAINER_MENU_VIEW: ViewStyle = {
    marginHorizontal: metric.ratioWidth(24),
    marginTop: metric.ratioHeight(49),
}

export const MENU_VIEW: ViewStyle = {
    flexDirection: 'row',
    justifyContent: 'space-between'
}

export const SEPARATOR_VIEW: ViewStyle = {
    width: metric.ratioWidth(20)
}

export const BOTTOM_MARGIN: ViewStyle = {
    marginTop: metric.ratioHeight(31)
}

export const BACKGROUND_STYLE = color.pinkBlueGradient

export const ACTIVE_BACKGROUND_STYLE = color.redFleshGradient

export const MENU_ICON_STYLE: ImageStyle = {
    width: metric.ratioWidth(25),
    height: metric.ratioHeight(25),
    resizeMode: null
}

export const HEADER_PROPS: HeaderProps = {
    leftIconSource: images.hamMenu,
    leftIconStyle: MENU_ICON_STYLE,
    isLeftIconAnimated: false,
    containerStyle: {
        height: metric.ratioHeight(42)
    }
}

export const REFRESH_CONTROL: any = { tintColor: color.brightDim }

export const ICON_PORTRAIT: ImageStyle = {
    width: metric.ratioWidth(36.34),
    height: metric.ratioHeight(40.46)
}
export const ICON_SQUARE: ImageStyle = {
    width: metric.ratioWidth(40.46),
    height: metric.ratioHeight(40.46)
}
