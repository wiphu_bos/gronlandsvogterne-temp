import { types } from "mobx-state-tree"
import { ManageProfilePropsModel } from "./manage-profile.models"
export const ManageProfileActions = types.model(ManageProfilePropsModel).actions(self => {
    const setShouldShowAddDudeMenu = (value: boolean) => self.shouldShowAddDudeMenu = value
    return {
        setShouldShowAddDudeMenu
    }
})