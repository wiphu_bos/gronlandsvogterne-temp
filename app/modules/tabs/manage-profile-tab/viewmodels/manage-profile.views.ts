import { types } from "mobx-state-tree"
import { ManageProfilePropsModel } from "./manage-profile.models"
import { GeneralResourcesStoreModel } from "../../../../models/general-resources-store/general-resources.store"
import { GeneralResources } from "../../../../constants/firebase/remote-config/remote-config.constant"
import { NavigationKey } from "../../../../constants/app.constant"
import { TestIDResources } from "../../../../constants/test-key/test.constant"
import * as Utils from "../../../../utils"
export const ManageProfileViews = types.model(ManageProfilePropsModel)
    .views(self => ({
        get getShouldShowAddDudeMenu() {
            return self.shouldShowAddDudeMenu
        }
    }))


export const ManageProfileResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State

    const { ManageProfileTabScreen } = GeneralResources
    const { myProfileTabMenuTitle,
        myDudeProfileTabMenuTitle,
        buttonAddTitle } = ManageProfileTabScreen
    const { ManageProfileTab } = NavigationKey

    //MARK: Views
    const getResources = (key: string, childKeyOrShareKey: string | boolean = false) => self.getValues(childKeyOrShareKey ? key : ManageProfileTab, childKeyOrShareKey ? true : key)
    const getResourceMyProfileTabMenuTitle = () => getResources(myProfileTabMenuTitle)
    const getResourceMyDudeProfileTabMenuTitle = () => getResources(myDudeProfileTabMenuTitle)
    const getResourceButtonAddTitle = () => getResources(buttonAddTitle, true)
    return {
        getResourceMyProfileTabMenuTitle,
        getResourceMyDudeProfileTabMenuTitle,
        getResourceButtonAddTitle
    }
})
    .views(self => {

        //MARK: Volatile State

        //MARK: Views

        const getTestIDMyProfileTabMenuTitle = () => Utils.getTestIDObject(TestIDResources.ManageProfileTabScreen.myProfileTabMenu)
        const getTestIDDudeProfileTabMenuTitle = () => Utils.getTestIDObject(TestIDResources.ManageProfileTabScreen.myDudeProfileTabMenu)
        return {
            getTestIDMyProfileTabMenuTitle,
            getTestIDDudeProfileTabMenuTitle
        }
    })
