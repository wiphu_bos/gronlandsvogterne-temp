import { types, flow } from "mobx-state-tree"
import { DudetteDetailPropsModel } from "../viewmodels"
import { UserStoreModel } from "../../../models/user-store"
import UserServices from "../../../services/api-domain/user.services"
import * as FireStoreConstant from "../../../constants/firebase/fire-store.constant"
import { RootStore } from "../../../models"
import { APIResponse, APISuccessResponse } from "../../../constants/service.types"
import { IChat } from "../../../models/chat-store"

export const DudetteDetailServiceActions = types.compose(types.model(DudetteDetailPropsModel), UserStoreModel).actions(self => {

    /*

        Not using `flow` and `yield` because we don't care about response

    */
    const doFavouriteDudette = (dudetteId: string) => {
        const params = {
            uid: dudetteId
        }
        try {
            UserServices.doFavouriteDudette(params)
            return true
        } catch (e) {
            return e
        }
    }


    const increaseViewCountDudette = flow(function* (dudetteId: string) {
        const params = {
            uid: dudetteId,
            field: FireStoreConstant.Property.VIEW_COUNT
        }
        try {
            UserServices.increaseViewCount(params)
            return true
        } catch (e) {
            return e
        }
    })

    const createChatRoom = flow(function* (rootStore?: RootStore, creatorId?: string) {
        try {
            const result: APIResponse = yield rootStore?.getChatStore?.createChatRoomById(creatorId)
            let success = (result as APISuccessResponse)?.data
            const data: IChat = success?.data
            if (!data) throw result
            return success
        } catch (e) {
            return e
        }
    })

    return { doFavouriteDudette, increaseViewCountDudette, createChatRoom }
})