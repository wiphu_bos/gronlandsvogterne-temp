// MARK: Import

import React, { useRef } from "react"
import { observer } from "mobx-react-lite"
import { View } from "react-native"
import { Wallpaper, Text, Header, AnimatedScrollView, UserTagsComponent, ProfileTitleComponents } from "../../components"
import { DudetteDetailProps } from "./dudette-detail.props"
import DudetteDetailController from "./dudette-detail.controllers"
import { SafeAreaView } from "react-navigation"
import { useConfigurate } from "../../custom-hooks/use-configure-controller"
import { INavigationRoute } from "../../models/navigation-store/navigation.types"
import { images } from "../../theme/images"
import * as metric from "../../theme"
import * as StringUtils from "../../utils/string.utils"
import { SquareImage } from "../../components/square-image/square-image"

// MARK: Style Import

import * as Styles from "./dudette-detail.styles"

export const DudetteProfileDetailScreen: React.FunctionComponent<DudetteDetailProps & Partial<INavigationRoute>> = observer((props) => {

    const controller = useConfigurate(DudetteDetailController, props) as DudetteDetailController

    const scrollViewRef = useRef(null)
    // MARK: Render
    return (
        <Wallpaper >
            <SafeAreaView
                {...metric.safeAreaViewProps}
                shouldRasterizeIOS
                style={Styles.SAFE_AREA_VIEW}>
                <Header {...Styles.HEADER_PROPS} {...controller.getHeaderLeftOptions}
                    rightIconSource={DudetteDetailController.viewModel?.getIsFavourite ? images.starFav : images.starUnFav}
                    rightIconStyle={Styles.MENU_ICON_STYLE}
                    onRightPress={controller.onFavouritePress} />

                <ProfileTitleComponents
                    containerStyle={Styles.PROFILE_TITLE_CONTAINER}
                    displayName={[DudetteDetailController.viewModel?.getFullName, DudetteDetailController.viewModel?.getAge?.value].join(', ')}
                    displayNameTestID={DudetteDetailController.resourcesViewModel?.getTestIDFullNameAndAgeTitle()}
                    displayCity={DudetteDetailController.viewModel?.getCity?.value}
                    displayCityTestID={DudetteDetailController.resourcesViewModel?.getTestIDCityTitle()}
                    isShowProfileImage={DudetteDetailController.viewModel?.generateVerifyCode ? true : false}
                    profileImageTestID={DudetteDetailController.resourcesViewModel?.getTestIDProfileImage}
                    profileImage={(DudetteDetailController.viewModel?.getProfileImage[0]) as any}
                    isShowButton={true}
                    onButtonClick={controller.onDirectMessageToCreatorPressed}
                    buttonTestID={DudetteDetailController.resourcesViewModel?.getTestIDContactThisDudette()}
                    buttonText={DudetteDetailController.resourcesViewModel?.getResourceContactThisDudetteTitle()}
                    buttonTextStyle={Styles.BUTTON_TEXT}
                />
                <AnimatedScrollView
                    setRef={scrollViewRef}
                    keyboardDismissMode="on-drag"
                    contentContainerStyle={Styles.SCROLL_VIEW}
                    {...metric.solidScrollView}>
                    <View shouldRasterizeIOS style={Styles.CONTENT_CONTAINNER}>
                        <Text style={Styles.TITLE}
                            {...DudetteDetailController.resourcesViewModel?.getTestIDHavdSogerHanTitle()}
                            text={DudetteDetailController.resourcesViewModel?.getResourceHavdSogerHanTitle()} />
                        <Text style={Styles.CONTENT} {...DudetteDetailController.resourcesViewModel?.getTestIDHavdSogerHanContent()}>
                            {DudetteDetailController.viewModel?.getHavdSogerHanList.map(e => e.value).join(', ')}
                        </Text>

                        <Text style={Styles.TITLE}
                            {...DudetteDetailController.resourcesViewModel?.getTestIDInterestTitle()}
                            text={DudetteDetailController.resourcesViewModel?.getResourceInterestTitle()} />
                        <Text style={Styles.CONTENT} {...DudetteDetailController.resourcesViewModel?.getTestIDInterestContent()}>
                            {DudetteDetailController.viewModel?.getInterestList.map(e => e.value).join(', ')}
                        </Text>

                        <Text style={Styles.TITLE}
                            {...DudetteDetailController.resourcesViewModel?.getTestIDDescriptionTitle()}
                            text={DudetteDetailController.resourcesViewModel?.getResourceDetailTitle()} />
                        <Text style={Styles.CONTENT} {...DudetteDetailController.resourcesViewModel?.getTestIDDescriptionContent()}>
                            {StringUtils.brToNewLine(DudetteDetailController.viewModel?.getDescription)}
                        </Text>

                        {DudetteDetailController.viewModel?.getGalleryImages?.length != 0 && <View shouldRasterizeIOS style={Styles.IMAGE_CONTAINER}>
                            <View shouldRasterizeIOS style={Styles.GRID}>
                                    {DudetteDetailController.viewModel?.getGalleryImages?.map((item, index) => 
                                        <View shouldRasterizeIOS style={Styles.GRID_IMAGE}>
                                            <SquareImage
                                                imageSource={item?.url}
                                                showDelete={false}
                                                index={index} />
                                        </View>
                                    )}
                            </View>
                        </View>}
                        
                        <Text style={Styles.TITLE}
                            {...DudetteDetailController.resourcesViewModel?.getTestIDTagTitle()}
                            text={DudetteDetailController.resourcesViewModel?.getResourceTagTitle()} />
                        <UserTagsComponent
                            isDisabled
                            tagList={DudetteDetailController.viewModel?.getTagList}
                            customHeaderView={<View />}
                            contentStyle={{ marginTop: 0 }} />
                    </View>

                </AnimatedScrollView>
            </SafeAreaView>

        </Wallpaper >
    )

})

