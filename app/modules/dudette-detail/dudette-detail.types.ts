import { Instance, SnapshotOut } from "mobx-state-tree"
import { DudeProfileStoreModel } from "../tabs/dude-profile-detail-tab/storemodels/dude-profile-detail.store"

export type DudeProfileStore = Instance<typeof DudeProfileStoreModel>
export type DudeProfileStoreSnapshot = SnapshotOut<typeof DudeProfileStoreModel>