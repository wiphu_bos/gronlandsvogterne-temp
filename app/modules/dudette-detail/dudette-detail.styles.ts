import { ViewStyle, ImageStyle, TextStyle } from "react-native"
import * as metric from "../../theme"
import { images } from "../../theme/images"
import { HeaderProps } from "../../components/header/header.props"
import { RFValue } from "react-native-responsive-fontsize"
import { color } from "../../theme"

export const FULL: ViewStyle = {
    flex: 1
}

export const SAFE_AREA_VIEW: ViewStyle = {
    ...FULL
}

export const MENU_ICON_STYLE: ImageStyle = {
    width: metric.ratioWidth(25),
    height: metric.ratioHeight(25),
    resizeMode: null
}

export const HEADER_PROPS: HeaderProps = {
    leftIconSource: images.back,
    leftIconStyle: MENU_ICON_STYLE,
    isLeftIconAnimated: false,
    isRightIconAnimated: false,
    containerStyle: {
        height: metric.ratioHeight(59)
    }
}

export const PROFILE_TITLE_CONTAINER: ViewStyle = {
    paddingTop: metric.ratioHeight(30),
    paddingHorizontal: metric.ratioWidth(30)
}

export const SCROLL_VIEW: ViewStyle = {
    flexGrow: 1,
    justifyContent: "center",
    backgroundColor: color.brightDim,
    paddingBottom: metric.ratioHeight(24)
}


export const TITLE: TextStyle = {
    color: color.palette.lightPink,
    fontSize: RFValue(19),
    marginTop: metric.ratioHeight(34),
    fontWeight: '500',

    //Android
    fontFamily: "Montserrat-Medium",
}

export const CONTENT_CONTAINNER: ViewStyle = {
    ...FULL,
    marginHorizontal: metric.ratioWidth(30),
}

export const CONTENT: TextStyle = {
    color: color.palette.white,
    fontSize: RFValue(16),
    fontWeight: '400',
    marginTop: metric.ratioHeight(11),

    //Android
    fontFamily: "Montserrat-Regular",
}

export const IMAGE_CONTAINER: ViewStyle = {
    ...FULL,
    marginTop: metric.ratioHeight(20),
    minHeight: metric.ratioHeight(135),
}

export const GRID: ViewStyle = {
    ...FULL,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'flex-start',
}

export const GRID_IMAGE: ViewStyle = {
    marginVertical: metric.ratioHeight(2),
    width: '33.33%',
    alignItems: 'center'
}

export const BUTTON_TEXT: TextStyle = {
    fontSize: RFValue(12),
    fontWeight: '500',

    //Android
    fontFamily: "Montserrat-SemiBold",
}