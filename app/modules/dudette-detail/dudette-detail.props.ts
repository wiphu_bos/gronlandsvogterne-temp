import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { UserStore } from '../../models/user-store/user.types'

export interface DudetteDetailProps extends NavigationContainerProps<ParamListBase> {
    dudetteProfileObject: UserStore
}