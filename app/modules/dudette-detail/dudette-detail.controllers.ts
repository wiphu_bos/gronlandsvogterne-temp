// MARK: Import

import { NavigationKey, SearchListType } from "../../constants/app.constant"
import { DudetteDetailProps } from "./dudette-detail.props"
import { DudetteDetailStoreModel, DudetteDetailResourcesStoreModel } from "./storemodels/dudette-detail.store"
import { RootStore, INavigationRoute } from "../../models"
import { onSnapshot } from "mobx-state-tree"
import { HeaderProps } from "../../components/header/header.props"
import { DudetteDetailResourcesStore, DudetteDetailStore } from "./storemodels/dudette-detail.types"
import { DrawerActions, CommonActions, StackActions } from "@react-navigation/native"
import { IFavouriteResponse } from "../../models/user-store"
import { FirebaseFirestoreTypes } from "@react-native-firebase/firestore"
import { SnapshotChangeType } from "../../constants/firebase/fire-store.constant"
import { IFavouriteListListener } from "../../utils/firebase/fire-store/fire-store.types"
import FirebaseFireStore from "../../utils/firebase/fire-store/fire-store"
import { BaseController } from "../base.controller"
import { ValidationServices } from "../../services/api-domain/validate.services"
import { IChat } from "../../models/chat-store"
import * as DataUtils from "../../utils/data.utils"

class DudetteDetailController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static viewModel: DudetteDetailStore
    public static resourcesViewModel: DudetteDetailResourcesStore
    public static myProps: DudetteDetailProps & Partial<INavigationRoute>
    private static _disposer: () => void

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, myProps?: DudetteDetailProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, myProps, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(myProps)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        DudetteDetailController.resourcesViewModel = DudetteDetailResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        if (!DudetteDetailController.myProps?.dudetteProfileObject) return
        DudetteDetailController.viewModel = DudetteDetailStoreModel.create(DudetteDetailController.myProps?.dudetteProfileObject)
    }

    private _setupSnapShot = () => {
        onSnapshot(DudetteDetailController.viewModel, (snap: DudetteDetailStore) => {
            this._setInitializedToPropsParams()
        })
    }

    //@override
    _setupProps = (myProps: DudetteDetailProps & Partial<INavigationRoute>) => {
        super._setupProps && super._setupProps(myProps)
        DudetteDetailController.myProps = {
            ...myProps as object,
            ...myProps?.route?.params
        }
    }

    /*
       Mark Data
   */


    public getHeaderLeftOptions: HeaderProps = {
        onLeftPress: this.backProcess
    }

    /*
       Mark Event
   */


    public onFavouritePress = () => {
        if (!DudetteDetailController.viewModel?.getIsFavourite) {
            DudetteDetailController.viewModel?.setIsFavourite(true)
        } else {
            DudetteDetailController.viewModel?.setIsFavourite(false)
        }
        DudetteDetailController.viewModel?.doFavouriteDudette(DudetteDetailController.viewModel?.getUserId)
    }


    private _onFavouriteListener = () => {
        const onNext: (snapshot: FirebaseFirestoreTypes.QuerySnapshot) => void = (snap => {
            if (snap.metadata.hasPendingWrites) return
            snap.docChanges().forEach(change => {
                const favData: IFavouriteResponse = change.doc?.data()
                if (favData?.user_id === DudetteDetailController.viewModel?.getUserId) {
                    if (change.type === SnapshotChangeType.ADDED) {
                        DudetteDetailController.viewModel?.setIsFavourite(true)
                    } else if (change.type === SnapshotChangeType.REMOVED) {
                        DudetteDetailController.viewModel?.setIsFavourite(false)
                    }
                }
            })
        })
        const onError: (e: Error) => void = (e => {
        })

        const onComplete: () => void = () => {
        }

        const params: IFavouriteListListener = {
            userId: this._rootStore?.getUserStore?.getUserId,
            type: SearchListType.Dudette,
            onNext,
            onError,
            onComplete
        }

        DudetteDetailController._disposer = FirebaseFireStore.onFavouriteListener(params)
    }

    //@override
    backProcess = () => {
        const { navigation } = this._myProps
        const drawerActions: any = DrawerActions.jumpTo(NavigationKey.SearchListScreen, {
            isShowFilter: false,
            isFetchData: false
        })
        Promise.all([navigation?.goBack(), this._myProps?.navigation?.dispatch(drawerActions)])
    }

    public onDirectMessageToCreatorPressed = async () => {
        const creatorId: string = DudetteDetailController.viewModel?.getUserId
        this._isGlobalLoading(true)
        const result = await DudetteDetailController.viewModel?.createChatRoom(this._rootStore, creatorId)
        this._isGlobalLoading(false)
        const chatRoom: IChat = result?.data
        if (!chatRoom) {
            const params = {
                rootStore: this._rootStore,
                error: result
            }
            const [isError, message] = new ValidationServices(params).premiumAccount()
            if (isError) this._delayExecutor(() => this._myProps?.navigation?.dispatch(CommonActions.navigate(NavigationKey.SubscriptionConfirmation,
                { onSuccess: () => this._goToChatRoom(chatRoom) }) as any))
        } else {
            this._goToChatRoom(chatRoom)
        }
    }

    private _goToChatRoom = (chatRoom: IChat) => {
        const chatRoomObject = DataUtils.getChatMessageObject(this._rootStore, chatRoom)
        this._myProps?.navigation?.dispatch(StackActions.push(NavigationKey.ChatRoom, { chatRoomObject: chatRoomObject, shouldDelayFetching: false }) as any)
    }


    /*
       Mark Life cycle
   */

    //@override
    viewDidAppearOnce = async () => {
        this._onFavouriteListener()
        DudetteDetailController.viewModel?.increaseViewCountDudette(DudetteDetailController.viewModel?.getUserId)
    }

    //@override
    deInit = () => {
        super.deInit && super.deInit()
        DudetteDetailController._disposer && DudetteDetailController._disposer()
    }

    /*
       Mark Helper
   */
}

export default DudetteDetailController