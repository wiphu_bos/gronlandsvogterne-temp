import { types } from "mobx-state-tree"
import { DudetteDetailPropsModel } from "./dudette-detail.models"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store/general-resources.store"
import { TestIDResources } from "../../../constants/test-key/test.constant"
import * as Utils from "../../../utils"
import { GeneralResources } from "../../../constants/firebase/remote-config"
import { NavigationKey } from "../../../constants/app.constant"
export const DudetteDetailViews = types.model(DudetteDetailPropsModel)
    .views(self => ({
        get getEx() {
            return
        },

    }))


export const DudetteDetailResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State

    //MARK: Views
    const { DudetteProfileDetailScreen } = GeneralResources
    const { interestTitle,
        havdSogerHanTitle,
        detailTitle,
        contactThisDudetteTitle,
        tagTitle
    } = DudetteProfileDetailScreen
    const getResources = (key: string, childKeyOrShareKey: string | boolean = false) => self.getValues(childKeyOrShareKey ? key : NavigationKey.DudetteProfileDetails, childKeyOrShareKey ? true : key)
    const getResourceInterestTitle = () => getResources(interestTitle)
    const getResourceHavdSogerHanTitle = () => getResources(havdSogerHanTitle)
    const getResourceDetailTitle = () => getResources(detailTitle)
    const getResourceContactThisDudetteTitle = () => getResources(contactThisDudetteTitle, true)
    const getResourceTagTitle = () => getResources(tagTitle)

    return {
        getResourceInterestTitle,
        getResourceHavdSogerHanTitle,
        getResourceDetailTitle,
        getResourceContactThisDudetteTitle,
        getResourceTagTitle
    }
})

    .views(self => {

        //MARK: Volatile State

        //MARK: Views

        const getTestIDFullNameAndAgeTitle = () => Utils.getTestIDObject(TestIDResources.DudetteProfileDetailScreen.fullNameWithAgeTitle)
        const getTestIDCityTitle = () => Utils.getTestIDObject(TestIDResources.DudetteProfileDetailScreen.cityTitle)
        const getTestIDProfileImage = () => Utils.getTestIDObject(TestIDResources.DudetteProfileDetailScreen.imgProfile)
        const getTestIDInterestTitle = () => Utils.getTestIDObject(TestIDResources.DudetteProfileDetailScreen.interestTitle)
        const getTestIDHavdSogerHanTitle = () => Utils.getTestIDObject(TestIDResources.DudetteProfileDetailScreen.havdSogerHanTitle)
        const getTestIDDescriptionTitle = () => Utils.getTestIDObject(TestIDResources.DudetteProfileDetailScreen.descriptionTitle)
        const getTestIDInterestContent = () => Utils.getTestIDObject(TestIDResources.DudetteProfileDetailScreen.interestContent)
        const getTestIDHavdSogerHanContent = () => Utils.getTestIDObject(TestIDResources.DudetteProfileDetailScreen.descriptionTitle)
        const getTestIDDescriptionContent = () => Utils.getTestIDObject(TestIDResources.DudetteProfileDetailScreen.descriptionContent)
        const getTestIDContactThisDudette = () => Utils.getTestIDObject(TestIDResources.DudetteProfileDetailScreen.contactThisDudette)
        const getTestIDTagTitle = () => Utils.getTestIDObject(TestIDResources.DudetteProfileDetailScreen.tagTitle)

        return {
            getTestIDFullNameAndAgeTitle,
            getTestIDCityTitle,
            getTestIDProfileImage,
            getTestIDInterestTitle,
            getTestIDHavdSogerHanTitle,
            getTestIDDescriptionTitle,
            getTestIDInterestContent,
            getTestIDHavdSogerHanContent,
            getTestIDDescriptionContent,
            getTestIDContactThisDudette,
            getTestIDTagTitle
        }
    })