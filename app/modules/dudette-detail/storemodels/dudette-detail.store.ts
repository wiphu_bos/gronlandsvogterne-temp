import { types } from "mobx-state-tree"
import { DudetteDetailViews, DudetteDetailActions, DudetteDetailResourcesViews } from "../viewmodels"
import { DudetteDetailPropsModel } from "../viewmodels"
import { DudetteDetailServiceActions } from "../services/dudette-detail.services"
import { StoreName } from "../../../constants/app.constant"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store/general-resources.store"
import { UserStoreModel } from "../../../models/user-store/user.store"

const DudetteDetailModel = types.model(StoreName.DudetteDetailStore, DudetteDetailPropsModel)

export const DudetteDetailStoreModel = types.compose(
    UserStoreModel,
    DudetteDetailModel,
    DudetteDetailViews,
    DudetteDetailActions,
    DudetteDetailServiceActions)

    export const DudetteDetailResourcesStoreModel = types.compose(GeneralResourcesStoreModel, DudetteDetailResourcesViews)

