import { Instance, SnapshotOut } from "mobx-state-tree"
import { DudetteDetailStoreModel, DudetteDetailResourcesStoreModel } from "./dudette-detail.store"

export type DudetteDetailStore = Instance<typeof DudetteDetailStoreModel>
export type DudetteDetailStoreSnapshot = SnapshotOut<typeof DudetteDetailStoreModel>

export type DudetteDetailResourcesStore = Instance<typeof DudetteDetailResourcesStoreModel>
export type DudetteDetailResourcesStoreSnapshot = SnapshotOut<typeof DudetteDetailResourcesStoreModel>