// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { ChatRoomProps } from "./chat-room.props"
import { View } from "react-native"
import { Header, Text, Wallpaper, ConfirmModal } from "../../components"
import { useConfigurate } from "../../custom-hooks/use-configure-controller"
import ChatListController from "./chat-room.controllers"
import { SafeAreaView } from "react-navigation"
import * as metric from "../../theme"
import { GiftedChat, Send, IMessage, Composer, Bubble } from "../../libs/gifted-chat"
import ChatRoomController from "./chat-room.controllers"
import { ChatSubmitButtonComponents } from "../../components/chat-input/chat-input"
import * as Animatable from 'react-native-animatable'
import { images } from "../../theme/images"
import * as LocaleUtils from "../../utils/locale"
import { TimeCompoenent } from "./views/time"
import { Property } from "../../constants/firebase/fire-store.constant"
import { useActionSheet, ActionSheetProvider } from "@expo/react-native-action-sheet"

// MARK: Style Import

import * as Styles from "./chat-room.styles"

export const ChatRoomComponent: React.FunctionComponent<ChatRoomProps> = observer((props) => {

    const controller = useConfigurate(ChatListController, props) as ChatListController
    const { showActionSheetWithOptions } = useActionSheet()

    const renderSend = (props: Send['props']) => (
            <Send {...props}>
                <ChatSubmitButtonComponents
                    buttonSubmitTestID={ChatRoomController.resourcesViewModel?.getTestIDButtonSubmitTitle()}
                    buttonSubmitIconSource={images.sendMessage}
                    onSubmit={() => props.text && props.onSend({ 
                        [Property.TEXT]: props.text,
                        [Property.CHAT_ROOM_ID]: ChatRoomController.myProps?.chatRoomObject.chat_room_id
                
                } as Partial<IMessage>, true)} />
            </Send>
        )

    const renderComposer = (props: Composer['props']) => (
        <Composer {...props}  {...ChatRoomController.resourcesViewModel?.getTestIDInputMessage()} />
    )

    const renderBubble = (props: Bubble['props']) => {
        return (
            <Animatable.View animation={'bounceInUp'} duration={400}>
                {/*@ts-ignore*/}
                <Bubble {...props} />
            </Animatable.View>
        )
    }

    // MARK: Render
    return (
            <SafeAreaView
                {...metric.safeAreaViewProps}
                shouldRasterizeIOS
                style={Styles.SAFE_AREA_VIEW}>
                <Header {...Styles.HEADER_PROPS} {...controller.getHeaderLeftOptions} />
                <View shouldRasterizeIOS style={Styles.HEADER_CONTAINER}>
                    <Text text={ChatRoomController.myProps?.chatRoomObject?.user?.full_name} style={Styles.TITLE}
                        {...ChatListController.resourcesViewModel?.getTestIDTitle()} />
                </View>
                <GiftedChat
                    onLongPress={(context, CurrentMessage) => controller.showActionSheetWithOptions(showActionSheetWithOptions, CurrentMessage)}
                    bottomOffset={metric.ratioHeight(30)}
                    renderTime={(props) => <TimeCompoenent {...props} />}
                    extraData={ChatRoomController.viewModel?.getMessageList?.slice()}
                    messages={ChatRoomController.viewModel?.getMessageList}
                    shouldUpdateMessage={() => {return true}}
                    user$={ChatRoomController.rootStore?.getUserStore}
                    locale={LocaleUtils.getLocaleDevice()}
                    renderBubble={renderBubble}
                    showUserAvatar
                    inverted
                    isLoadingEarlier
                    scrollToBottom
                    infiniteScroll
                    alwaysShowSend
                    placeholder={ChatRoomController.resourcesViewModel?.getResourceEnterTextPlaceholder()}
                    renderComposer={renderComposer}
                    renderSend={renderSend}
                    onSend={controller._onChat}
                />
            <ConfirmModal
                {...ChatListController.resourcesViewModel?.getTestIDConfirmModal()}
                okButtonText={ChatListController.resourcesViewModel?.getResourceModalButtonOKTitle()}
                clearButtonText={ChatListController.resourcesViewModel?.getResourceButtonCancelTitle()}
                message={ChatListController.resourcesViewModel?.getResourceModalConfirmDeleteMessage()}
                isVisible={ChatListController.viewModel?.getIsModalShowing || false}
                onCancel={controller.onCancelModal}
                onModalClosed={controller.onModalClosed}
                onOK={controller.onDeleteChatThread} />
            </SafeAreaView>
    )
})

export const ChatRoomScreen: React.FunctionComponent<ChatRoomProps> = observer((props) => {
    return (
        <Wallpaper
            showBubble
            shouldRasterizeIOS
            style={Styles.CONTAINER}
            {...Styles.TAB_PAGE_BACKGROUND}>
            <ActionSheetProvider>
                <ChatRoomComponent {...props} />
            </ActionSheetProvider>
        </Wallpaper>
    )
})
