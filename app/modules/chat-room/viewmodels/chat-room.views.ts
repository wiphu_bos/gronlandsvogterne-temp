import { types } from "mobx-state-tree"
import { ChatRoomPropsModel } from "./chat-room.models"
import { TestIDResources } from "../../../constants/test-key/test.constant"
import * as Utils from "../../../utils"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store"
import { GeneralResources } from "../../../constants/firebase/remote-config/remote-config.constant"

export const ChatRoomViews = types.model(ChatRoomPropsModel)
    .views(self => ({
        get getMessageList() {
            return self.messageList$
        },
        get getLastId(): string {
            return self.last_id
        },
        get getIsLoadMore(): boolean {
            return self.isLoadMore
        },
        get getIsFirstLoadDone(): boolean {
            return self.isFirstLoadDone
        },
        get getIsLoadDone(): boolean {
            return self.isLoadDone
        },
        get getShouldLoadmore(): boolean {
            return !self.isLoadDone && !self.isLoadMore && self.messageList$?.length > 0 && self.isFirstLoadDone
        },
        get getIsModalShowing(): boolean {
            return self.isModalShowing
        }
    }))

export const ChatRoomResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State

    const { ChatRoomScreen } = GeneralResources
    const { enterTextPlaceholder,
        buttonCancelTitle,
        buttonDeleteTitle,
        modalButtonOKTitle,
        modalConfirmDeleteMessage } = ChatRoomScreen
    //MARK: Views
    const getsharedResources = (key: string) => self.getValues(key, true)
    const getResourceEnterTextPlaceholder = () => getsharedResources(enterTextPlaceholder)
    const getResourceButtonCancelTitle = () => getsharedResources(buttonCancelTitle)
    const getResourceModalButtonOKTitle = () => getsharedResources(modalButtonOKTitle)
    const getResourceButtonDeleteTitle = () => getsharedResources(buttonDeleteTitle)
    const getResourceModalConfirmDeleteMessage = () => getsharedResources(modalConfirmDeleteMessage)
    return {
        getResourceEnterTextPlaceholder,
        getResourceButtonCancelTitle,
        getResourceModalButtonOKTitle,
        getResourceButtonDeleteTitle,
        getResourceModalConfirmDeleteMessage
    }
})
    .views(self => {
        //MARK: Volatile State

        //MARK: Views
        const getTestIDConfirmModal = () => Utils.getTestIDObject(TestIDResources.ChatListScreen.confirmModal)
        const getTestIDTitle = () => Utils.getTestIDObject(TestIDResources.ChatRoomScreen.title)
        const getTestIDButtonSubmitTitle = () => Utils.getTestIDObject(TestIDResources.ChatRoomScreen.buttonSubmit)
        const getTestIDInputMessage = () => Utils.getTestIDObject(TestIDResources.ChatRoomScreen.inputMessage)
        return {
            getTestIDTitle,
            getTestIDButtonSubmitTitle,
            getTestIDInputMessage,
            getTestIDConfirmModal
        }
    })
