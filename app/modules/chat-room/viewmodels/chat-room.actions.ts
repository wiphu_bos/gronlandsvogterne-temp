import { types, unprotect, flow } from "mobx-state-tree"
import { ChatRoomPropsModel } from "./chat-room.models"
import { RootStore } from "../../../models/root-store/root.types"
import { IMessage } from "../../../libs/gifted-chat/Models"
import { MessageStoreModel } from "../../../models/message-store/message-store.store"

export const ChatRoomActions = types.model(ChatRoomPropsModel).actions(self => {
    const setMessageList = (rootStore: RootStore, list: IMessage[]) => {
        // self.messageList$ = list
        // self.isLoadMore = false
    }
    const foundItemIndex = (message: IMessage) => {
        return self.messageList$.slice().findIndex(e => e?.message_id === message?.message_id)
    }
    const addMessageIntoList = flow(function* (message: IMessage, isSender:boolean) {
        const index = foundItemIndex(message)
        if(index === -1) {
            const transformedMessage = {
                ...message,
                sender: { ...message.sender }, //dis-observer from userStore
            } as IMessage
            const tempChatList = self.messageList$
            const childViewModel = MessageStoreModel.create(transformedMessage)
            unprotect(childViewModel)
            self.messageList$ = [].concat.apply(childViewModel, tempChatList)
            if(isSender) yield childViewModel.createMessage()
        }
    })

    const removeMessageFromList = (message: IMessage) => {
        const index = foundItemIndex(message)
        if(index === -1) return
        self.messageList$.splice(index, 1)
    }

    const setIsLoadMore = (value: boolean) => self.isLoadMore = value
    const setIsFirstLoadDone = (value: boolean) => self.isFirstLoadDone = value
    const setIsLoadDone = (value: boolean) => self.isLoadDone = value
    const setIsModalShowing = (value: boolean) => self.isModalShowing = value
    return {
        setMessageList,
        addMessageIntoList,
        setIsLoadMore,
        setIsLoadDone,
        setIsFirstLoadDone,
        setIsModalShowing,
        removeMessageFromList
    }
})