import { types } from "mobx-state-tree"
import { MessageStoreModel } from "../../../models/message-store/message-store.store"
export const ChatRoomPropsModel = {
    messageList$: types.optional(types.array(MessageStoreModel), []),
    last_id: types.maybeNull(types.string),
    isFirstLoadDone: types.optional(types.boolean, false),
    isLoadDone: types.optional(types.boolean, false),
    isLoadMore: types.optional(types.boolean, false),
    isModalShowing: types.optional(types.boolean, false)
} 