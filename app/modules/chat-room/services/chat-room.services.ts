
import { ChatRoomPropsModel } from "../viewmodels"
import { types, flow, unprotect } from "mobx-state-tree"
import { APIResponse, APISuccessResponse } from "../../../constants/service.types"
import { RootStore } from "../../../models"
import * as DataUtils from "../../../utils/data.utils"
import { IMessage } from "../../../libs/gifted-chat"
import { IMessageListResponse } from "../../../models/message-store/message-store.types"
import { MessageStoreModel } from "../../../models/message-store/message-store.store"
import firestore from '@react-native-firebase/firestore'
export const ChatRoomServiceActions = types.model(ChatRoomPropsModel).actions(self => {
    const fetchMessageList = flow(function* (rootStore: RootStore, chatRoomId: string) {
        const last_id = self.isFirstLoadDone ? self.last_id : null
        try {
            self.isLoadDone = self.messageList$.length > 0
            const result: APIResponse = yield rootStore?.getChatStore.fetchMessageList(chatRoomId, last_id)
            let success = (result as APISuccessResponse)?.data
            const response: IMessageListResponse = success?.data
            const messageList = response?.message_list.map(e => DataUtils.getMessageObject(rootStore, e, false))
            const pagingObj = response?.page
            if (messageList?.length > 0) {
                if (!self.isLoadMore) {
                    self.messageList$.clear()
                }
                messageList?.forEach(e => {
                    const childViewModel = MessageStoreModel.create(e)
                    unprotect(childViewModel)
                    self.messageList$.push(childViewModel)
                })
            }
            self.last_id = pagingObj?.last_id
            self.isLoadDone = pagingObj?.is_last_page
            return result
        } catch (e) {
            yield firestore().collection("errors").add(e)
            return e
        } finally {
            self.isFirstLoadDone = true
            self.isLoadMore = false
        }
    })

    const deleteMessageById = flow(function* (rootStore?: RootStore, message_id?: string) {
        try {
            let result: APISuccessResponse = yield rootStore?.getChatStore?.deleteMessageById(message_id)
            const data: APISuccessResponse = result?.data
            const message: IMessage = data?.data
            if (!message) throw data
            return message
        } catch (e) {
            return e
        }
    })

    return { fetchMessageList, deleteMessageById }
})