// MARK: Import
import { NavigationKey } from "../../constants/app.constant"
import { ChatRoomProps } from "./chat-room.props"
import { ChatRoomResourcesStoreModel, ChatRoomStoreModel } from "./storemodels/chat-room.store"
import { ChatRoomResourcesStore, ChatRoomStore } from "./storemodels/chat-room.types"
import { RootStore, INavigationRoute } from "../../models"
import { BaseController } from "../base.controller"
import { onSnapshot, unprotect } from "mobx-state-tree"
import { SnapshotChangeType } from "../../constants/firebase/fire-store.constant"
import { FirebaseFirestoreTypes } from "@react-native-firebase/firestore"
import FirebaseFireStore from "../../utils/firebase/fire-store/fire-store"
import { IChatRoomListener } from "../../utils/firebase/fire-store/fire-store.types"
import { ExtractCSTWithSTN } from "mobx-state-tree/dist/core/type/type"
import { HeaderProps } from "../../components/header/header.props"
import { IMessage } from "../../libs/gifted-chat"
import * as DataUtils from "../../utils/data.utils"
import { ActionSheetOptions } from "@expo/react-native-action-sheet"
import { MessageStoreModel } from "../../models/message-store"

class ChatRoomController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static viewModel: ChatRoomStore
    public static resourcesViewModel: ChatRoomResourcesStore
    private static _disposer: () => void
    public static myProps: ChatRoomProps & Partial<INavigationRoute>
    private static _selectedItem: ExtractCSTWithSTN<typeof MessageStoreModel>

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, myProps?: ChatRoomProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, myProps, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(myProps)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        ChatRoomController.resourcesViewModel = ChatRoomResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.ChatRoom)
        ChatRoomController.viewModel = localStore && ChatRoomStoreModel.create({ ...localStore }) || ChatRoomStoreModel.create({})
        unprotect(ChatRoomController.viewModel)
    }

    //@override
    _setupProps = (props: ChatRoomProps & Partial<INavigationRoute>) => {
        ChatRoomController.myProps = {
            ...props as object,
            ...(props as INavigationRoute)?.route?.params
        }
    }

    private _setupSnapShot = () => {
        onSnapshot(ChatRoomController.viewModel, (snap: ChatRoomStore) => {
            this._setInitializedToPropsParams()
        })
    }

    /*
       Mark Data
   */

    private _fetchMessageList = async () => {
        this._isGlobalLoading(true)
        ChatRoomController.myProps?.shouldDelayFetching ? await new Promise(resolve => this._delayExecutor(async () => {
            await this._fetchMessage()
            resolve()
        }, 3000)) : await this._fetchMessage()
        this._isGlobalLoading(false)
    }

    private _fetchMessage = async () => {
        await ChatRoomController.viewModel?.fetchMessageList(this._rootStore, ChatRoomController.myProps.chatRoomObject?.chat_room_id)
    }

    /*
       Mark Event
   */
    private _markReadChatRoom = () => {
        const chatRoomId = ChatRoomController.myProps?.chatRoomObject?.chat_room_id
        this._rootStore?.getChatStore?.markReadChatById(chatRoomId)
    }


    public onDeleteChatThread = () => {
        ChatRoomController.viewModel?.setIsModalShowing(false)
    }
    public onModalClosed = async () => {
        if (ChatRoomController._selectedItem) {
            const messageId = ChatRoomController._selectedItem?.message_id
            this._isGlobalLoading(true)
            await ChatRoomController.viewModel?.deleteMessageById(this._rootStore, messageId)
            this._isGlobalLoading(false)
        }
        ChatRoomController._selectedItem = undefined
    }

    public onCancelModal = () => {
        ChatRoomController._selectedItem = undefined
        ChatRoomController.viewModel?.setIsModalShowing(false)
    }

    public showActionSheetWithOptions = (
        config: (options: ActionSheetOptions, callback: (i: number) => void) => void,
        selectedItem: ExtractCSTWithSTN<typeof MessageStoreModel>) => {
        const buttonDeleteText = ChatRoomController.resourcesViewModel?.getResourceButtonDeleteTitle()
        const buttonCancelText = ChatRoomController.resourcesViewModel?.getResourceButtonCancelTitle()
        const options = [buttonDeleteText, buttonCancelText]
        const destructiveButtonIndex = 0
        const cancelButtonIndex = 2
        config({
            options,
            cancelButtonIndex,
            destructiveButtonIndex,
        }, buttonIndex => {
            switch (buttonIndex) {
                case destructiveButtonIndex:
                    ChatRoomController._selectedItem = selectedItem
                    ChatRoomController.viewModel?.setIsModalShowing(true)
                    break
                default:
                    ChatRoomController._selectedItem = undefined
                    break
            }
        })
    }

    public getHeaderLeftOptions: HeaderProps = {
        onLeftPress: this.backProcess
    }

    public _onChat = async (messages: IMessage[]) => {
        if (messages?.length === 0)
            return
        const message: IMessage = messages[0]
        ChatRoomController.viewModel?.addMessageIntoList(message, true)
    }

    private _onChatRoomListener = () => {
        const onNext: (snapshot: FirebaseFirestoreTypes.QuerySnapshot) => void = (snap => {
            if (!ChatRoomController.viewModel?.getIsFirstLoadDone || snap.metadata.hasPendingWrites) return
            snap.docChanges().forEach(change => {
                const message: IMessage = DataUtils.getMessageObject(this._rootStore, change.doc.data(), true)
                if (change.type === SnapshotChangeType.ADDED) {
                    if (message) {
                        const senderId = message?.sender?.uid
                        ChatRoomController.viewModel?.addMessageIntoList(message, false)
                        if (senderId !== this._rootStore?.getUserStore?.getUserId) {
                            this._markReadChatRoom()
                        }
                    }
                } else if (change.type === SnapshotChangeType.REMOVED) {
                    if (message) ChatRoomController.viewModel?.removeMessageFromList(message)
                }
            })
        })

        const onError: (e: Error) => void = (e => {
        })

        const onComplete: () => void = () => {
        }

        const params: IChatRoomListener = {
            chatRoomId: ChatRoomController.myProps?.chatRoomObject?.chat_room_id,
            onNext,
            onError,
            onComplete
        }

        ChatRoomController._disposer = FirebaseFireStore.onChatRoomListener(params)
    }
    public onLoadMore = async () => {
        if (!ChatRoomController.viewModel?.getShouldLoadmore) return
        ChatRoomController.viewModel?.setIsLoadMore(true)
        await this._fetchMessageList()
    }

    //@override
    backProcess = () => {
    }

    /*
       Mark Life cycle
   */

    //@override
    viewDidAppearOnce = async () => {
        this._onChatRoomListener()
    }

    //@override

    viewDidAppearAfterFocus = async () => {
        super.viewDidAppearAfterFocus && super.viewDidAppearAfterFocus()
        const isConnected = this._rootStore?.getSharedStore?.getIsConnected
        if (isConnected) {
            if (ChatRoomController.viewModel?.getMessageList?.length === 0) {
                await this._fetchMessageList()
                this._markReadChatRoom()
            }
        } else {
            ChatRoomController.viewModel?.setIsFirstLoadDone(true)
        }
    }

    //@override
    deInit = () => {
        super.deInit && super.deInit()
        ChatRoomController._disposer && ChatRoomController._disposer()
    }

    /*
       Mark Helper
   */
}

export default ChatRoomController