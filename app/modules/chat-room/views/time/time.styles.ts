import { ViewStyle, TextStyle } from "react-native"
import * as metric from "../../../../theme"
import { color } from "../../../../theme"
import { RFValue } from "react-native-responsive-fontsize"



const WRAPPER: ViewStyle = {
    marginTop: metric.ratioHeight(6)
}

const TEXT: TextStyle = {
    fontSize: RFValue(8),
    backgroundColor: 'transparent',
    textAlign: 'right',
    fontWeight: '300',

    //Android
    fontFamily: "Montserrat-Light"
}

export const TIME_TEXT = (position: any): TextStyle => {
    let textStyle: TextStyle
    if (position === 'left') {
        textStyle = {
            color: color.palette.white,
            ...TEXT,
        }
    } else if (position === 'right') {
        textStyle = {
            color: color.palette.white,
            ...TEXT,
        }
    }
    return textStyle
}

export const TIME_CONTAINER = (position: any): ViewStyle => {
    let viewStyle: ViewStyle
    if (position === 'left') {
        viewStyle = {
            ...WRAPPER,
            alignItems: 'flex-start'
        }
    } else if (position === 'right') {
        viewStyle = {
            ...WRAPPER,
            alignItems: 'flex-end'
        }
    }
    return viewStyle
}