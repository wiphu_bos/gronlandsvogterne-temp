// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import * as Styles from "./time.styles"
import { View, TextStyle } from "react-native"
import { Text } from "../../../../components/text"
import dayjs from 'dayjs'
import * as LocaleUtils from "../../../../utils/locale"
import { TimeProps, IMessage } from "../../../../libs/gifted-chat/Models"

export const TimeCompoenent: React.FunctionComponent<TimeProps<IMessage>> = observer((props) => {

    const {
        position,
        containerStyle,
        currentMessage,
        timeFormat,
        timeTextStyle,
    } = props

    const createdDate = () => {
        if (currentMessage.created_date) {
            return dayjs(currentMessage.created_date)
                .locale(LocaleUtils.getLocaleDevice())
                .format(timeFormat)
        }
        return 'sending...'
    }

    if (!!currentMessage) {
        return (
            <View
                style={[
                    Styles.TIME_CONTAINER(position),
                    containerStyle && containerStyle[position],
                ]}
            >
                <Text
                    style={
                        [
                            Styles.TIME_TEXT(position),
                            timeTextStyle && timeTextStyle[position] as TextStyle,
                        ]
                    }
                >
                    {createdDate()}
                </Text>
            </View>
        )
    }
    return null
})

