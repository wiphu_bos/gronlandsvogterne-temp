import { ViewStyle, ImageStyle, TextStyle } from "react-native"
import { color } from "../../theme"
import * as metric from "../../theme"
import { HeaderProps } from "../../components/header/header.props"
import { images } from "../../theme/images"
import { RFValue } from "react-native-responsive-fontsize"

export const FULL: ViewStyle = {
    flex: 1
}
export const AVOID_KEYBOARD_VIEW: ViewStyle = {
    ...FULL
}

export const TAB_PAGE_BACKGROUND: any = { colors: color.tabPageBackground }

export const CONTAINER: ViewStyle = {
    ...FULL
}

export const SAFE_AREA_VIEW: ViewStyle = {
    ...FULL
}

const MENU_ICON_STYLE: ImageStyle = {
    width: metric.ratioWidth(24),
    height: metric.ratioHeight(24)
}

export const HEADER_CONTAINER: ViewStyle = {
    marginBottom: metric.ratioHeight(22),
    alignItems: 'center'
}

export const HEADER_PROPS: HeaderProps = {
    leftIconSource: images.back,
    leftIconStyle: MENU_ICON_STYLE,
    isLeftIconAnimated: false,
}
const TEXT: TextStyle = {
    color: color.palette.white,
    fontFamily: "Montserrat",
}
export const TITLE: TextStyle = {
    ...TEXT,
    lineHeight: metric.ratioHeight(36),
    fontSize: RFValue(24),
    fontWeight: '400',

    //Android
    fontFamily: "Montserrat-Regular",
}