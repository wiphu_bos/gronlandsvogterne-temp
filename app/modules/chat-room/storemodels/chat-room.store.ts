import { types } from "mobx-state-tree"
import { StoreName } from "../../../constants/app.constant"
import { ChatRoomPropsModel } from "../viewmodels/chat-room.models"
import { ChatRoomActions } from "../viewmodels/chat-room.actions"
import { ChatRoomViews, ChatRoomResourcesViews } from "../viewmodels/chat-room.views"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store"
import { ChatRoomServiceActions } from "../services/chat-room.services"

const ChatRoomModel = types.model(StoreName.ChatRoom, ChatRoomPropsModel)
export const ChatRoomStoreModel = types.compose(ChatRoomModel, ChatRoomViews, ChatRoomActions, ChatRoomServiceActions)
export const ChatRoomResourcesStoreModel = types.compose(GeneralResourcesStoreModel, ChatRoomResourcesViews)

