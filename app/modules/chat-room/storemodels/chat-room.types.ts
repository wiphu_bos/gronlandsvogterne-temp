import { Instance, SnapshotOut } from "mobx-state-tree"
import { ChatRoomStoreModel, ChatRoomResourcesStoreModel } from "./chat-room.store"

export type ChatRoomStore = Instance<typeof ChatRoomStoreModel>
export type ChatRoomStoreSnapshot = SnapshotOut<typeof ChatRoomStoreModel>

export type ChatRoomResourcesStore = Instance<typeof ChatRoomResourcesStoreModel>
export type ChatRoomResourcesStoreSnapshot = SnapshotOut<typeof ChatRoomResourcesStoreModel>