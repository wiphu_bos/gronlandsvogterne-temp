import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { IChat } from '../../models/chat-store'

export interface ChatRoomProps extends NavigationContainerProps<ParamListBase> {
    chatRoomObject?: IChat,
    shouldDelayFetching?: boolean
}