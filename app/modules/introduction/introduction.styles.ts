import { ViewStyle } from "react-native"
import { color } from "../../theme"

export const FULL: ViewStyle = {
    flex: 1
}

export const BackgroundStyle = color.pinkBlueGradient
