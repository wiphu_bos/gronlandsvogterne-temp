import { types } from "mobx-state-tree"
import { ViewPagerContainerType } from "../views/pages-container/pages-container.props"

export const IntroductionPropsModel = {
    keyViewPages: types.maybeNull(types.frozen<Partial<ViewPagerContainerType[]>>()),
    pageNumber: types.optional(types.number, 0),
    currentPage: types.optional(types.number, 0),
    previousPage: types.optional(types.number, 0)
}