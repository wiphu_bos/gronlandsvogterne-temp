import { types } from "mobx-state-tree"
import { IntroductionPropsModel } from "./introduction.models"
import * as Utils from "../../../utils"
import { TestIDResources } from "../../../constants/test-key/test.constant"
import { NavigationKey } from "../../../constants/app.constant"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store"
import { ViewPagerContainerType } from "../views/pages-container/pages-container.props"

export const IntroductionViews = types.model(IntroductionPropsModel)
    .views(self => ({
        get getKeyViewPages(): ViewPagerContainerType[] {
            return self.keyViewPages
        },
        get getCurrentPage(): number {
            return self.currentPage
        },
        get getPreviousPage(): number {
            return self.previousPage
        },
        get getPageNumber(): number {
            return self.pageNumber
        }
    }))

export const IntroductionResourcesViews = types.compose(IntroductionViews,GeneralResourcesStoreModel)
    .views(self => {

        //MARK: Volatile State

        const { Introduction } = NavigationKey

        //MARK: Views

        const getResourceIntroduction = () => self.getIntroductions(Introduction)

        return {
            getResourceIntroduction
        }
    })
    .views(self => {
        //MARK: Volatile State

        //MARK: Views

        const getTestIDImage = () => Utils.getTestIDObject(TestIDResources.IntroductionScreen.image)
        const getTestIDTitle = () => Utils.getTestIDObject(TestIDResources.IntroductionScreen.title)
        const getTestIDDescription = () => Utils.getTestIDObject(TestIDResources.IntroductionScreen.description)
        return {
            getTestIDImage,
            getTestIDTitle,
            getTestIDDescription
        }
    })