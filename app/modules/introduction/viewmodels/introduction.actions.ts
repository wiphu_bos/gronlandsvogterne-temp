import { types } from "mobx-state-tree"
import { IntroductionPropsModel } from "./introduction.models"
import { ViewPagerContainerType } from "../views/pages-container/pages-container.props"
export const IntroductionActions = types.model(IntroductionPropsModel).actions(self => {
    //MARK: Volatile State

    let prevPage: number = 0

    //MARK: Actions
    const setKeyViewPages = (value: Partial<ViewPagerContainerType[]>) => {
        self.keyViewPages = value
    }

    const setPageNumber = (value: number) => {
        self.pageNumber = value
    }
    const setPreviousPage = (value: number) => {
        self.previousPage = value
    }
    const setCurrentPage = (value: number) => {
        if (prevPage !== value) setPreviousPage(prevPage)
        prevPage = value
        self.currentPage = value
    }
    return {
        setKeyViewPages,
        setPageNumber,
        setCurrentPage
    }
})