// MARK: Import

import React from "react"
import { SafeAreaView } from "react-navigation"
import IntroductionController from "./introduction.controllers"
import { IntroductionProps } from "./introduction.props"
import { PageControlComponent } from "./views/page-control/page-control"
import { ViewPagerContainer } from "./views/pages-container/pages-container"
import { useConfigurate } from "../../custom-hooks/use-configure-controller"
import { Wallpaper } from "../../components"
import { observer } from "mobx-react-lite"

// MARK: Style Import

import * as Styles from './introduction.styles'

export const IntroductionScreen: React.FunctionComponent<IntroductionProps> = observer((props) => {

    const controller = useConfigurate(IntroductionController, props) as IntroductionController

    // MARK: Render

    return (
        <Wallpaper >
            <SafeAreaView shouldRasterizeIOS style={Styles.FULL}>
                <ViewPagerContainer
                    navigation={props?.navigation}
                    keyViewPages={IntroductionController.viewModel?.getKeyViewPages}
                    onPageScroll={controller.onPageScroll}
                    onPageSelected={controller.onPageSelected}
                    onPageScrollStateChanged={controller.onPageScrollStateChanged}
                />
                <PageControlComponent observeViewModel={IntroductionController.viewModel} />
            </SafeAreaView>
        </Wallpaper>
    )
})