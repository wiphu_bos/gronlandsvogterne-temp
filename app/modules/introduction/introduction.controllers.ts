// MARK: Import
import { IntroductionResourcesStoreModel, IntroductionResourcesStore, IntroductionStore, IntroductionStoreModel } from "./storemodels"
import { RootStore, INavigationRoute } from "../../models"
import { NativeSyntheticEvent } from "react-native"
import { NavigationKey } from "../../constants/app.constant"
import { IntroductionProps } from "./introduction.props"
import { PageScrollStateChangedEvent, ViewPagerOnPageSelectedEventData, ViewPagerOnPageScrollEventData } from '@react-native-community/viewpager'
import { ViewPagerContainerType, ViewPagerContentType } from "./views/pages-container/pages-container.props"
import { onSnapshot } from "mobx-state-tree"
import { BaseController } from "../base.controller"


class IntroductionController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static viewModel: IntroductionStore
    public static resourcesViewModel: IntroductionResourcesStore

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, myProps?: IntroductionProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, myProps, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(myProps)
        this._setupViewModel()
        this._setupSnapShot()
        this._setupPageView()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        IntroductionController.resourcesViewModel = IntroductionResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.Introduction)
        IntroductionController.viewModel = localStore && IntroductionStoreModel.create({ ...localStore }) || IntroductionStoreModel.create({})
    }

    private _setupSnapShot = () => {
        onSnapshot(IntroductionController.viewModel, (snap: IntroductionStore) => {
            this._setInitializedToPropsParams()
            this._rootStore?.setModuleStore({ [NavigationKey.Introduction]: snap })
        })
    }

    private _setupPageView = () => {
        const pageObjects = IntroductionController.resourcesViewModel?.getResourceIntroduction()
        this.setupPageView(pageObjects)
        this.setupPageNumber(pageObjects?.length)
    }

    /*
       Mark Data
   */


    public setupPageView = (pageObjects: any[]) => {
        const keyViewPages = []
        for (const [i, v] of pageObjects?.entries()) {
            let viewPagerContainer: ViewPagerContainerType = {
                id: i,
                contents: {
                    image: v?.image,
                    title: v?.title,
                    description: v?.description
                } as ViewPagerContentType
            } as ViewPagerContainerType
            keyViewPages.push(viewPagerContainer)
        }
        IntroductionController.viewModel?.setKeyViewPages(keyViewPages)
    }

    public setupPageNumber = (value: number) => {
        IntroductionController.viewModel?.setPageNumber(value)
    }

    public setCurrentPage = (value: number) => {
        IntroductionController.viewModel?.setCurrentPage(value)
    }

    /*
       Mark Event
   */

    public onPageScroll = (e: NativeSyntheticEvent<ViewPagerOnPageScrollEventData>) => {
    }

    public onPageSelected = (e: NativeSyntheticEvent<ViewPagerOnPageSelectedEventData>) => {
        this.setCurrentPage(e.nativeEvent.position)
    }
    public onPageScrollStateChanged = (e: NativeSyntheticEvent<PageScrollStateChangedEvent>) => {
    }

    /*
       Mark Life cycle
   */

    /*
       Mark Helper
   */
}

export default IntroductionController