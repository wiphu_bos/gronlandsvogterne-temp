import { types } from "mobx-state-tree"
import { IntroductionViews, IntroductionActions, IntroductionResourcesViews } from "../viewmodels"
import { IntroductionPropsModel } from "../viewmodels"
import { IntroductionServiceActions } from "../services/introduction.services"
import { StoreName } from "../../../constants/app.constant"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store"


const IntroductionModel = types.model(StoreName.Introduction, IntroductionPropsModel)

export const IntroductionStoreModel = types.compose(
    IntroductionModel,
    IntroductionViews,
    IntroductionActions,
    IntroductionServiceActions)

export const IntroductionResourcesStoreModel = types.compose(GeneralResourcesStoreModel, IntroductionResourcesViews)