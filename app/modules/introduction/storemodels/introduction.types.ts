import { Instance, SnapshotOut } from "mobx-state-tree"
import { IntroductionStoreModel, IntroductionResourcesStoreModel } from "./introduction.store"

export type IntroductionStore = Instance<typeof IntroductionStoreModel>
export type IntroductionStoreSnapshot = SnapshotOut<typeof IntroductionStoreModel>

export type IntroductionResourcesStore = Instance<typeof IntroductionResourcesStoreModel>
export type IntroductionResourcesStoreSnapshot = SnapshotOut<typeof IntroductionResourcesStoreModel>