// MARK: Import

import React from "react"
import { View } from "react-native"
import { IntroductionPageProps } from "./pages.props"
import { Text } from "../../../../components"
import FastImage from "react-native-fast-image"
import * as Utils from "../../../../utils"
import * as StringUtils from "../../../../utils/string.utils"
// MARK: Style Import
import * as Styles from './pages.styles'
import IntroductionController from "../../introduction.controllers"


export const PageScreen: React.FunctionComponent<IntroductionPageProps> = (props) => {

    // MARK: Global Variables

    // MARK: Local Variables
    const { id, contents } = props
    const image = contents?.image
    // MARK: Lifecycle

    // MARK: Render
    return (
        <View shouldRasterizeIOS style={Styles.FULL}
            key={id}>
            <View shouldRasterizeIOS style={Styles.IMAGE_CONTAINER}>
                {Utils.isValidURL(image) && <FastImage {...IntroductionController.resourcesViewModel?.getTestIDImage()} style={Styles.IMAGE} source={{ uri: image }} />}
            </View>
            <View shouldRasterizeIOS style={Styles.TEXT_CONTAINNER} >
                <Text {...IntroductionController.resourcesViewModel?.getTestIDTitle()} style={Styles.TITLE} text={contents.title} />
                <Text {...IntroductionController.resourcesViewModel?.getTestIDDescription()} style={Styles.DESCRIPTION} >
                    {StringUtils.brToNewLine(contents.description)}
                </Text>
            </View>
        </View>
    )
}
