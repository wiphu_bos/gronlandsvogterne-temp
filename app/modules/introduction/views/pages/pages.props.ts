import { ViewPagerContentType } from "../pages-container/pages-container.props";

export interface IntroductionPageProps {
    id: number,
    contents: ViewPagerContentType
}