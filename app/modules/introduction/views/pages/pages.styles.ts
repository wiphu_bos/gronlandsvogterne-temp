import { ViewStyle, TextStyle, ImageStyle } from "react-native"
import { color } from "../../../../theme"
import * as metric from "../../../../theme"
import { RFValue } from "react-native-responsive-fontsize"

export const FULL: ViewStyle = {
    height: '100%',
    //Android If view is not set in flex, must set bgColor.
    backgroundColor: color.clear
}
const CONTAINNER: ViewStyle = {
    flex: 1,
    alignItems: 'center',
}

export const BackgroundStyle = color.pinkBlueGradient

export const IMAGE: ImageStyle = {
    width: metric.ratioWidth(251),
    height: metric.ratioWidth(251),
    resizeMode: "cover",
    backgroundColor: color.superWhiteDim,
    borderRadius: metric.ratioWidth(251 / 2)
}
export const IMAGE_CONTAINER: ViewStyle = {
    ...CONTAINNER,
    justifyContent: "center",
    marginTop: metric.ratioHeight(44),
}

const BASE_TEXT: TextStyle = {
    fontFamily: "Montserrat",
    color: color.palette.white,
    textAlign: 'center',
}

export const BOLD: TextStyle = { fontWeight: "bold" }

export const TEXT_CONTAINNER: TextStyle = {
    ...CONTAINNER,
    marginTop: metric.ratioHeight(47)
}

export const TITLE: TextStyle = {
    ...BASE_TEXT,
    fontWeight: '400',
    fontSize: RFValue(22),
    width: metric.ratioWidth(238),
    lineHeight: metric.ratioHeight(36),

    //Android 
    fontFamily: 'Montserrat-Regular'
}

export const DESCRIPTION: TextStyle = {
    ...BASE_TEXT,
    fontWeight: '300',
    fontSize: RFValue(16),
    marginTop: metric.ratioHeight(47),
    width: metric.ratioWidth(318),
    lineHeight: metric.ratioHeight(30),

    //Android 
    fontFamily: 'Montserrat-Light'
}