// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { Button } from "../../../../components"
import { IntroductionHeaderProps } from "./footer.props"
import { NavigationKey } from "../../../../constants/app.constant"
import { StackActions } from "@react-navigation/native"
import IntroductionController from "../../introduction.controllers"


export const FooterComponent: React.FunctionComponent<IntroductionHeaderProps> = observer((props) => {

    const currentPage = IntroductionController?.viewModel?.getCurrentPage
    const pageNumber = IntroductionController?.viewModel?.getPageNumber

    const nextButtonDidTouch = () => {
        if (currentPage === pageNumber - 1) {
            props?.navigation?.dispatch(StackActions.push(NavigationKey.CreateAccount) as any)
        }
    }

    return (currentPage === pageNumber - 1 ? <Button
        isAnimated={false}
        testID="btnNext"
        accessibilityLabel="btnNext"
        preset="bottomRightMenu"
        imagePreset="next"
        onPress={() => nextButtonDidTouch()} /> : null)
})