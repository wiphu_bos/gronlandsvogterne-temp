import { MutableRefObject } from 'react'
import { NavigationContainerProps } from 'react-navigation'
import { ParamListBase } from '@react-navigation/native'

export interface IntroductionHeaderProps extends NavigationContainerProps<ParamListBase>  {
    pager: MutableRefObject<undefined>
}