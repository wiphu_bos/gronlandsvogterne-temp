// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import PageControl from 'react-native-page-control'
// MARK: Style Import
import * as Styles from './page-control.styles'
import { PageControlProps } from "./page-control.props"

export const PageControlComponent: React.FunctionComponent<PageControlProps> = observer((props) => {

    // MARK: Global Variables

    // MARK: Local Variables

    // MARK: Lifecycle

    return (<PageControl
        numberOfPages={props?.observeViewModel?.getPageNumber}
        currentPage={props?.observeViewModel?.getCurrentPage}
        hidesForSinglePage
        pageIndicatorTintColor={Styles.INDICATOR_DESELECTED.backgroundColor}
        currentPageIndicatorTintColor={Styles.INDICATOR_SELECTED.backgroundColor}
        indicatorStyle={Styles.INDICATOR_DESELECTED}
        currentIndicatorStyle={Styles.INDICATOR_SELECTED}
    />)
})