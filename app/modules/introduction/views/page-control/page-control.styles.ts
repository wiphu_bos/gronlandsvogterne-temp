import { ViewStyle } from "react-native"
import { color } from "../../../../theme"
import * as metric from "../../../../theme"

export const INDICATOR: ViewStyle = {
    width: metric.ratioWidth(7),
    height: metric.ratioWidth(7),
    marginHorizontal: metric.ratioWidth(5),
    marginBottom: metric.ratioHeight(40)
}
export const INDICATOR_SELECTED: ViewStyle = {
    ...INDICATOR,
    backgroundColor: color.palette.white
}
export const INDICATOR_DESELECTED: ViewStyle = {
    ...INDICATOR,
    backgroundColor: color.whiteDim
}