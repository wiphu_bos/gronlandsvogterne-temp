import { IntroductionStore } from "../../storemodels";

export interface PageControlProps {
    observeViewModel?: IntroductionStore
}