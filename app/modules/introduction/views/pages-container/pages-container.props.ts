import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { NativeSyntheticEvent } from 'react-native'
import { ViewPagerOnPageSelectedEventData, PageScrollStateChangedEvent } from '@react-native-community/viewpager'

export type ViewPagerContainerType = {
    id: number,
    contents: ViewPagerContentType
}

export type ViewPagerContentType = {
    image: string,
    title: string,
    description: string,
}

export interface ViewPagerContainerProps extends NavigationContainerProps<ParamListBase> {
    keyViewPages?:ViewPagerContainerType[],
    onPageScroll?: (e: NativeSyntheticEvent<ViewPagerOnPageSelectedEventData>) => void,
    onPageSelected?: (e: NativeSyntheticEvent<ViewPagerOnPageSelectedEventData>) => void,
    onPageScrollStateChanged?: (e: NativeSyntheticEvent<PageScrollStateChangedEvent>) => void
}