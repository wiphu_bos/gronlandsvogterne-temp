// MARK: Import

import React, { useRef } from "react"
import ViewPager from '@react-native-community/viewpager'
import { View } from "react-native"
import { HeaderComponent } from "../../../../components/custom-header/custom-header"
import { FooterComponent } from "../footer/footer"
import { ViewPagerContainerProps, ViewPagerContainerType } from "./pages-container.props"
import { observer } from "mobx-react-lite"
import { PageScreen } from "../pages/pages"
import IntroductionController from "../../introduction.controllers"

// MARK: Style Import

import * as Styles from './pages-container.styles'


export const ViewPagerContainer: React.FunctionComponent<ViewPagerContainerProps> = observer((props) => {

    const viewPagerRef = useRef(null)

    const renderPage = (value: ViewPagerContainerType) => {
        return (<PageScreen key={value.id} id={value.id} contents={value.contents} />)
    }

    const backButtonDidTouch = (ref: React.MutableRefObject<undefined>) => {
        const currentPage = IntroductionController.viewModel?.getCurrentPage
        if (currentPage === 0) {
            props?.navigation?.goBack()
        } else {
            const pager = ref?.current as ViewPager
            const prevPage = currentPage && currentPage - 1 || 0
            pager?.setPage(prevPage)
        }
    }

    return (
        <View shouldRasterizeIOS style={Styles.FULL}>
            <HeaderComponent onBackButtonPress={() => backButtonDidTouch(viewPagerRef)} />
            <ViewPager style={Styles.FULL}
                ref={viewPagerRef}
                initialPage={0}
                onPageScroll={props?.onPageScroll}
                onPageSelected={props?.onPageSelected}
                onPageScrollStateChanged={props?.onPageScrollStateChanged}>
                {props?.keyViewPages?.map(k => renderPage(k))}
            </ViewPager>
            <FooterComponent pager={viewPagerRef} navigation={props.navigation} />
        </View>)
})