import { types } from "mobx-state-tree"
import { WelcomePropsModel } from "./welcome.models"
import { GeneralResources } from "../../../constants/firebase/remote-config"
import { NavigationKey } from "../../../constants/app.constant"
import { TestIDResources } from "../../../constants/test-key/test.constant"
import * as Utils from "../../../utils"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store"

export const WelcomeViews = types.model(WelcomePropsModel)
    .views(self => ({
        get getExample() {
            return
        }
    }))

export const WelcomeResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State

    const { WelcomeScreen } = GeneralResources
    const { buttonCreateAccountTitle, buttonLoginTitle, contactSupportTitle } = WelcomeScreen
    const { Welcome } = NavigationKey

    //MARK: Views
    const getResources = (key: string, childKeyOrShareKey: string | boolean = false) => self.getValues(childKeyOrShareKey ? key : Welcome, childKeyOrShareKey ? true : key)
    const getResourceButtonCreateAccountTitle = () => getResources(buttonCreateAccountTitle)
    const getResourceButtonLoginTitle = () => getResources(buttonLoginTitle)
    const getResourceContactSupportTitle = () => getResources(contactSupportTitle, true)


    return {
        getResourceButtonCreateAccountTitle,
        getResourceButtonLoginTitle,
        getResourceContactSupportTitle
    }
})
    .views(self => {

        //MARK: Volatile State

        //MARK: Views

        const getTestIDButtonCreateAccountTitle = () => Utils.getTestIDObject(TestIDResources.WelcomeScreen.buttonCreateAccount)
        const getTestIDButtonLoginTitle = () => Utils.getTestIDObject(TestIDResources.WelcomeScreen.buttonLogin)
        const getTestIDButtonContactSupportTitle = () => Utils.getTestIDObject(TestIDResources.WelcomeScreen.buttonContactSupport)
        return {
            getTestIDButtonCreateAccountTitle,
            getTestIDButtonLoginTitle,
            getTestIDButtonContactSupportTitle
        }
    })
