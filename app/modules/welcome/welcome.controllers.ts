// MARK: Import
import { NavigationKey } from "../../constants/app.constant"
import { WelcomeProps } from "./welcome.props"
import { WelcomeResourcesStoreModel } from "./storemodels/welcome.store"
import { WelcomeResourcesStore, WelcomeStore } from "./storemodels/welcome.types"
import { RootStore, INavigationRoute } from "../../models"
import { StackActions } from "@react-navigation/native"
import { BaseController } from "../base.controller"
import { Linking, Alert } from 'react-native'
import { EMAIL_CONTACT, MAIL_TO } from "../../constants/emil.comstant"

class WelcomeController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static viewModel: WelcomeStore
    public static resourcesViewModel: WelcomeResourcesStore

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, myProps?: WelcomeProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, myProps, isNestedNavigation)
        this._setupResourcesViewModel()
        this._setupProps(myProps)
        this._setupNavigation()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        WelcomeController.resourcesViewModel = WelcomeResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }

    /*
       Mark Data
   */


    /*
       Mark Event
   */

    public goToIntroductionPage = () => {
        const { navigation } = this._myProps
        navigation.dispatch(StackActions.push(NavigationKey.Introduction) as any)
    }

    public goToLoginPage = () => {
        const { navigation } = this._myProps

        console.log('jon');

        navigation.dispatch(StackActions.push(NavigationKey.Login) as any)
    }

    public goToLoginPagex = () => {
        const { navigation } = this._myProps
        navigation.dispatch(StackActions.push(NavigationKey.Login) as any)
    }

    public openEmailContact = () => {
        Linking.openURL(MAIL_TO.concat(EMAIL_CONTACT))
    }


   

    /*
       Mark Helper
   */
}

export default WelcomeController

