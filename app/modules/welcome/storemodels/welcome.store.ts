import { types } from "mobx-state-tree"
import { StoreName } from "../../../constants/app.constant"
import { WelcomePropsModel } from "../viewmodels/welcome.models"
import { WelcomeActions } from "../viewmodels/welcome.actions"
import { WelcomeViews, WelcomeResourcesViews } from "../viewmodels/welcome.views"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store"

const WelcomeModel = types.model(StoreName.Welcome, WelcomePropsModel)
export const WelcomeStoreModel = types.compose(WelcomeModel,WelcomeViews,WelcomeActions)
export const WelcomeResourcesStoreModel = types.compose(GeneralResourcesStoreModel, WelcomeResourcesViews)

