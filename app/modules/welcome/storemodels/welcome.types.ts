import { Instance, SnapshotOut } from "mobx-state-tree"
import { WelcomeStoreModel, WelcomeResourcesStoreModel } from "./welcome.store"

export type WelcomeStore = Instance<typeof WelcomeStoreModel>
export type WelcomeStoreSnapshot = SnapshotOut<typeof WelcomeStoreModel>

export type WelcomeResourcesStore = Instance<typeof WelcomeResourcesStoreModel>
export type WelcomeResourcesStoreSnapshot = SnapshotOut<typeof WelcomeResourcesStoreModel>