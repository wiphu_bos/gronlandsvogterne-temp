// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { AppNameSlogan, Button, Wallpaper } from "../../components"
import { View } from "react-native"
import { WelcomeProps } from "./welcome.props"
import WelcomeController from "./welcome.controllers"
import { GradientButton } from "../../components/gradient-button/button"
import { textPresets } from "../../components/button/button.presets"
import { SafeAreaView } from "react-navigation"
import { useConfigurate } from "../../custom-hooks/use-configure-controller"

// MARK: Style Import

import * as Styles from "./welcome.styles"

export const WelcomeScreen2: React.FunctionComponent<WelcomeProps> = observer((props) => {

    const controller = useConfigurate(WelcomeController, props) as WelcomeController

    // MARK: Render

    return (
        <Wallpaper showBubble>
            <SafeAreaView shouldRasterizeIOS style={Styles.SAFE_AREA_VIEW}>
                <View shouldRasterizeIOS style={Styles.TOP_VIEW}>
                    <AppNameSlogan />
                </View>
                {/* <View shouldRasterizeIOS style={Styles.BODY_VIEW}>
                    <View shouldRasterizeIOS style={Styles.CONTAINER_VIEW}>
                        <Button text={WelcomeController.resourcesViewModel?.getResourceButtonCreateAccountTitle()}
                            {...WelcomeController.resourcesViewModel?.getTestIDButtonCreateAccountTitle()}
                            preset="transparent"
                            onPress={controller.goToIntroductionPage} />
                        <GradientButton
                            text={WelcomeController.resourcesViewModel?.getResourceButtonLoginTitle()}
                            {...WelcomeController.resourcesViewModel?.getTestIDButtonLoginTitle()}
                            containerStyle={Styles.BOTTOM_MARGIN}
                            onPress={controller.goToLoginPage} />
                    </View>
                </View>
                    <Button
                        style={Styles.BOTTOM_VIEW}
                        isAnimated={false}
                        {...WelcomeController.resourcesViewModel?.getTestIDButtonContactSupportTitle()}
                        preset="link" onPress={controller.openEmailContact} 
                        text={WelcomeController.resourcesViewModel?.getResourceContactSupportTitle()}
                        textStyle={[textPresets.regularNormal,Styles.CONTACT_TEXT]} /> */}
            </SafeAreaView>
        </Wallpaper>
    )
})
