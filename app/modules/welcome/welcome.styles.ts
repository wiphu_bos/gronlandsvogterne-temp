import { ViewStyle, TextStyle } from "react-native"
import { color } from "../../theme"
import * as metric from "../../theme"

export const FULL: ViewStyle = {
    flex: 1
}

export const TOP_VIEW: ViewStyle = {
    ...FULL
}

export const SAFE_AREA_VIEW: ViewStyle = {
    ...FULL,
    alignItems: 'center'
}

export const BODY_VIEW: ViewStyle = {
    height: metric.ratioHeight(250),
}
export const BOTTOM_VIEW: ViewStyle = {
    height: metric.ratioHeight(46),
    justifyContent: 'center'
}
export const CONTAINER_VIEW: ViewStyle = {
    justifyContent: 'space-between',
}

export const BOTTOM_MARGIN: ViewStyle = {
    marginTop: metric.ratioHeight(15)
}

export const CONTACT_TEXT: TextStyle = {
    marginBottom: metric.ratioHeight(23)
}

export const BACKGROUND_STYLE = color.pinkBlueGradient

export const ACTIVE_BACKGROUND_STYLE = color.redFleshGradient