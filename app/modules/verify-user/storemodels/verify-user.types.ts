import { Instance, SnapshotOut } from "mobx-state-tree"
import { VerifyUserStoreModel, VerifyUserResourcesStoreModel } from "./verify-user.store"

export type VerifyUserStore = Instance<typeof VerifyUserStoreModel>
export type VerifyUserStoreSnapshot = SnapshotOut<typeof VerifyUserStoreModel>

export type VerifyUserResourcesStore = Instance<typeof VerifyUserResourcesStoreModel>
export type VerifyUserResourcesStoreSnapshot = SnapshotOut<typeof VerifyUserResourcesStoreModel>