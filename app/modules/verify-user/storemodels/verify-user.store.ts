import { types } from "mobx-state-tree"
import { StoreName } from "../../../constants/app.constant"
import { VerifyUserServiceActions } from "../services/verify-user.services"
import { VerifyUserPropsModel } from "../viewmodels/verify-user.models"
import { VerifyUserActions } from "../viewmodels/verify-user.actions"
import { VerifyUserViews, VerifyUserResourcesViews } from "../viewmodels/verify-user.views"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store"

const VerifyUserModel = types.model(StoreName.VerifyUser, VerifyUserPropsModel)

export const VerifyUserStoreModel = types.compose(VerifyUserModel, VerifyUserViews, VerifyUserActions, VerifyUserServiceActions)
export const VerifyUserResourcesStoreModel = types.compose(GeneralResourcesStoreModel, VerifyUserResourcesViews)