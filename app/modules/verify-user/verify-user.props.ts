import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"

export interface VerifyUserProps extends NavigationContainerProps<ParamListBase> {
    email?:string,
    shouldSendVerifyCode?:boolean
}