import { types } from "mobx-state-tree"

export const VerifyUserPropsModel = {
    code: types.maybeNull(types.string),
    codeErrorMessage: types.maybeNull(types.string),
    isValid: types.optional(types.boolean, false),
} 