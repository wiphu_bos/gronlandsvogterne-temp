import { types } from "mobx-state-tree"
import { VerifyUserPropsModel } from "./verify-user.models"

export const VerifyUserActions = types.model(VerifyUserPropsModel).actions(self => {

    //MARK: Volatile State

    //MARK: Actions
    
    const setCode = (value: string) => self.code = value
    const setCodeErrorMessage = (value: string) => self.codeErrorMessage = value
    const setIsValid = (value: boolean) => self.isValid = value

    return { setCode, setIsValid, setCodeErrorMessage }
})