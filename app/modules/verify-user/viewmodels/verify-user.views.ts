import { types } from "mobx-state-tree"
import { VerifyUserPropsModel } from "./verify-user.models"
import { GeneralResources } from "../../../constants/firebase/remote-config"
import { NavigationKey } from "../../../constants/app.constant"
import { TestIDResources } from "../../../constants/test-key/test.constant"
import * as Utils from "../../../utils"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store"

export const VerifyUserViews = types.model(VerifyUserPropsModel)
    .views(self => ({
        get getCode() {
            return self.code
        },
        get getCodeErrorMessage() {
            return self.codeErrorMessage
        },
        get getIsValid() {
            return self.isValid
        }
    }))

export const VerifyUserResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State

    const { VerifyUserScreen } = GeneralResources
    const { title, subTitle, verifyCodePlaceholder, buttonSubmitCodeTitle, buttonResendVerifyCodeTitle } = VerifyUserScreen
    const { VerifyUser } = NavigationKey

    //MARK: Views
    const getResources = (key: string, childKeyOrShareKey: string | boolean = false) => self.getValues(childKeyOrShareKey ? key : VerifyUser, childKeyOrShareKey ? true : key)
    const getResourceTitle = () => getResources(title)
    const getResourceSubTitle = () => getResources(subTitle)
    const getResourceVerifyCodePlaceholder = () => getResources(verifyCodePlaceholder)
    const getResourceButtonSubmitCodeTitle = () => getResources(buttonSubmitCodeTitle, true)
    const getResourceButtonResendVerifyCodeTitle = () => getResources(buttonResendVerifyCodeTitle, true)


    return {
        getResourceTitle,
        getResourceSubTitle,
        getResourceVerifyCodePlaceholder,
        getResourceButtonSubmitCodeTitle,
        getResourceButtonResendVerifyCodeTitle
    }
})
    .views(self => {

        //MARK: Volatile State

        //MARK: Views

        const getTestIDTitle = () => Utils.getTestIDObject(TestIDResources.VerifyUserScreen.title)
        const getTestIDSubTitle = () => Utils.getTestIDObject(TestIDResources.VerifyUserScreen.subTitle)
        const getTestIDVerifyCodePlaceholder = () => Utils.getTestIDObject(TestIDResources.VerifyUserScreen.verifyCodePlaceholder)
        const getTestIDEmailExampleTitle = () => Utils.getTestIDObject(TestIDResources.VerifyUserScreen.emailExample)
        const getTestIDButtonSubmitCodeTitle = () => Utils.getTestIDObject(TestIDResources.VerifyUserScreen.buttonSubmitCode)
        const getTestIDErrorMessage = () => Utils.getTestIDObject(TestIDResources.VerifyUserScreen.errorMessage)
        const getTestIDButtonResendCodeTitle = () => Utils.getTestIDObject(TestIDResources.VerifyUserScreen.buttonResendCode)
        return {
            getTestIDTitle,
            getTestIDSubTitle,
            getTestIDVerifyCodePlaceholder,
            getTestIDEmailExampleTitle,
            getTestIDButtonSubmitCodeTitle,
            getTestIDErrorMessage,
            getTestIDButtonResendCodeTitle
        }
    })
