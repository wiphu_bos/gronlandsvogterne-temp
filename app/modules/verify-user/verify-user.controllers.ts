// MARK: Import
import { Keyboard } from "react-native"
import { NavigationKey } from "../../constants/app.constant"
import { RootStore, INavigationRoute } from "../../models"
import { onSnapshot } from "mobx-state-tree"
import { VerifyUserProps } from "./verify-user.props"
import { VerifyUserStore, VerifyUserResourcesStore } from "./storemodels/verify-user.types"
import { VerifyUserStoreModel, VerifyUserResourcesStoreModel } from "./storemodels/verify-user.store"
import { APIResponse, APISuccessResponse, APIErrorResponse } from "../../constants/service.types"
import * as Validate from "../../utils/local-validate"
import * as metric from "../../theme"
import { GeneralResources } from "../../constants/firebase/remote-config"
import { BaseController, IAlertParams } from "../base.controller"
import { ValidationServices } from "../../services/api-domain/validate.services"

class VerifyUserController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static viewModel: VerifyUserStore
    public static resourcesViewModel: VerifyUserResourcesStore
    public static myProps: VerifyUserProps & Partial<INavigationRoute>

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, myProps?: VerifyUserProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, myProps, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(myProps)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        VerifyUserController.resourcesViewModel = VerifyUserResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.VerifyUser)
        VerifyUserController.viewModel = localStore && VerifyUserStoreModel.create({ ...localStore }) || VerifyUserStoreModel.create({})
    }

    private _setupSnapShot = () => {
        onSnapshot(VerifyUserController.viewModel, (snap: VerifyUserStore) => {
            this._setInitializedToPropsParams()
        })
    }

    //@Override
    _setupProps = (myProps: VerifyUserProps & Partial<INavigationRoute>) => {
        super._setupProps && super._setupProps(myProps)
        VerifyUserController.myProps = {
            ...myProps as object,
            email: myProps?.route?.params?.email,
            shouldSendVerifyCode: myProps?.route?.params?.shouldSendVerifyCode
        }
    }

    /*
       Mark Data
   */


    /*
       Mark Event
   */

    private _getVerifyCodeIfNeeded = () => VerifyUserController.myProps?.shouldSendVerifyCode && this._getVerifyCode()

    private _getVerifyCode = async () => await VerifyUserController.viewModel?.generateVerifyCode(this._rootStore)

    private _verifyingProcess = async () => {
        this._isGlobalLoading(true)
        const verifyCodeResult: APIResponse = await VerifyUserController.viewModel?.sendCodeToVerify(this._rootStore)
        const verifyCodeSuccess = verifyCodeResult as APISuccessResponse
        let firebaseAuthResult: APIResponse
        if (verifyCodeSuccess?.data) {
            firebaseAuthResult = await VerifyUserController.viewModel?.createFirebaseAuth(this._rootStore)
            this._isGlobalLoading(false)
            const firebaseAuthSuccess = firebaseAuthResult as APISuccessResponse
            if (firebaseAuthSuccess?.data) {
                if (this._rootStore.getNavigationStore.getPreviousPageName === NavigationKey.Login) this.backProcess()
                this._isGlobalLoading(true)
                await VerifyUserController.viewModel?.signInFirebaseAuth(this._rootStore)
                this._isGlobalLoading(false)

            } else {
                this._verifyingFailed(firebaseAuthResult)
            }
        } else {
            this._verifyingFailed(verifyCodeResult)
        }
    }

    public resendVerifyCode = async () => {
        this._isGlobalLoading(true)
        const verifyCodeResult: APIResponse = await VerifyUserController.viewModel?.resendVerifyCode(this._rootStore)
        this._isGlobalLoading(false)
        const verifyCodeSuccess = verifyCodeResult as APISuccessResponse
        const alertParams: IAlertParams = {
            title: VerifyUserController.resourcesViewModel?.getResourceTitle()
        }
        const { ErrorMessage, VerifyUserScreen } = GeneralResources
        if (verifyCodeSuccess?.data) {
            alertParams.message = this._rootStore.getGeneralResourcesStore(VerifyUserScreen.verifyCodeSendToEmail, true)
        } else {
            alertParams.message = this._rootStore.getGeneralResourcesStore(ErrorMessage.somethingWentWrong, true)
        }
        VerifyUserController.viewModel?.setCode("")
        VerifyUserController.viewModel?.setCodeErrorMessage("")
        this._generalAlertOS(alertParams)
    }

    private _verifyingFailed = (error: APIResponse) => {
        this._isGlobalLoading(false)
        const params = {
            error: error as APIErrorResponse,
            localViewModel: VerifyUserController.viewModel as any,
            rootStore: this._rootStore
        }
        new ValidationServices(params).verifyCode()
    }

    public onChangeCode = (value: string) => {
        VerifyUserController.viewModel?.setCode(value)
        VerifyUserController.viewModel?.setCodeErrorMessage(null)
    }

    public onBlurCodeTextField = () => {
        const code = VerifyUserController.viewModel?.getCode
        const params = {
            value: code,
            localViewModel: VerifyUserController.viewModel as any,
            rootStore: this._rootStore
        }
        Validate.onBlueVerifyCodeTextField(params)
    }

    public onSubmitButtonDidTouch = async () => {
        Keyboard.dismiss()
        this._checkEmptyData()
        this.onBlurCodeTextField()
        if (!VerifyUserController.viewModel?.getIsValid) return
        await this._verifyingProcess()
    }

    /*
       Mark Life cycle
   */

    //@override
    viewDidAppearOnce = async () => {
        this._getVerifyCodeIfNeeded()
    }

    /*
       Mark Helper
   */

    private _checkEmptyData = () => {
        const { getCode } = VerifyUserController.viewModel
        const isValid = getCode && getCode.length == metric.maxCharVerifyCode
        VerifyUserController.viewModel?.setIsValid(isValid || false)
    }
}

export default VerifyUserController