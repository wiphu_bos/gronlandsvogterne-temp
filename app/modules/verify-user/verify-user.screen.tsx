// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import VerifyUserController from "./verify-user.controllers"
import { View, KeyboardAvoidingView } from "react-native"
import { Text, Wallpaper, AnimatedScrollView, Button } from "../../components"
import * as metric from "../../theme"
import { GradientButton } from "../../components/gradient-button/button"
import FloatLabelTextInput from '../../libs/float-label-textfield/float-label-text-field'
import { SafeAreaView } from "react-navigation"
import { VerifyUserProps } from "./verify-user.props"
import { useConfigurate } from "../../custom-hooks/use-configure-controller"
import { INavigationRoute } from "../../models"
import { HeaderComponent } from "../../components/custom-header/custom-header"
import { textPresets } from "../../components/button/button.presets"

// MARK: Style Import

import * as Styles from "./verify-user.styles"


export const VerifyUserScreen: React.FunctionComponent<VerifyUserProps & Partial<INavigationRoute>> = observer((props) => {

    const controller = useConfigurate(VerifyUserController, props) as VerifyUserController

    return (
        <Wallpaper showBubble>
            <SafeAreaView shouldRasterizeIOS style={Styles.SAFE_AREA_VIEW}>
                <HeaderComponent onBackButtonPress={controller.backProcess} />
                <KeyboardAvoidingView style={Styles.AVOID_KEYBOARD_VIEW} behavior={metric.keyboardBehavior} >
                    <AnimatedScrollView
                        contentContainerStyle={Styles.SCROLL_VIEW}
                        {...metric.solidScrollView} >
                        <View shouldRasterizeIOS style={Styles.CONTAINER_VIEW}>
                            <View shouldRasterizeIOS style={Styles.TEXT_CONTAINER}>
                                <Text style={Styles.TITLE} {...VerifyUserController.resourcesViewModel?.getTestIDTitle()}
                                    text={VerifyUserController.resourcesViewModel?.getResourceTitle()} />
                                <View shouldRasterizeIOS style={Styles.SUBTITLE_VIEW}>
                                    <Text style={Styles.SUB_TITLE} {...VerifyUserController.resourcesViewModel?.getTestIDSubTitle()}
                                        text={VerifyUserController.resourcesViewModel?.getResourceSubTitle()} />
                                </View>
                                <View shouldRasterizeIOS style={Styles.EMAIL_VIEW}>
                                    <Text style={Styles.EMAIL_TITLE}
                                        {...VerifyUserController.resourcesViewModel?.getTestIDEmailExampleTitle()}
                                        text={VerifyUserController.myProps?.email} />
                                </View>
                            </View>
                            <FloatLabelTextInput
                                {...VerifyUserController.resourcesViewModel?.getTestIDVerifyCodePlaceholder()}
                                errorMessage={VerifyUserController.viewModel?.getCodeErrorMessage}
                                testIDErrorMessage={VerifyUserController.resourcesViewModel?.getTestIDErrorMessage()}
                                placeholder={VerifyUserController.resourcesViewModel?.getResourceVerifyCodePlaceholder()}
                                value={VerifyUserController.viewModel?.getCode}
                                onChangeTextValue={controller.onChangeCode}
                                {...metric.verifyCodeTextFieldOptions}
                                keyboardType={'phone-pad'}
                                // letterSpacing={metric.ratioWidth(10)}
                                onBlur={controller.onBlurCodeTextField}
                                noBorder
                                returnKeyType="send"
                                onSubmitEditing={() => { controller.onSubmitButtonDidTouch() }}
                            />
                            <GradientButton
                                /*{localViewModel?.getIsValid ? "primary" : "disabled"}*/
                                text={VerifyUserController.resourcesViewModel?.getResourceButtonSubmitCodeTitle()}
                                {...VerifyUserController.resourcesViewModel?.getResourceButtonSubmitCodeTitle()}
                                style={Styles.BUTTON_MARGIN} onPress={controller.onSubmitButtonDidTouch} />

                            <Button
                                isAnimated={false}
                                preset="link"
                                text={VerifyUserController.resourcesViewModel?.getResourceButtonResendVerifyCodeTitle()}
                                textStyle={textPresets.regularMedium} style={Styles.BUTTON_TEXT_MARGIN}
                                {...VerifyUserController.resourcesViewModel?.getTestIDButtonResendCodeTitle()}
                                onPress={controller.resendVerifyCode} />
                        </View>
                    </AnimatedScrollView>
                </KeyboardAvoidingView>
            </SafeAreaView>
        </Wallpaper>
    )
})
