import { ViewStyle, TextStyle } from "react-native"
import { color } from "../../theme"
import * as metric from "../../theme"
import { TEXT } from "../../components/appname-slogan/appname-slogan.styles"
import { RFValue } from "react-native-responsive-fontsize"

export const FULL: ViewStyle = {
    flex: 1,
}
export const SAFE_AREA_VIEW: ViewStyle = {
    ...FULL,
}

export const AVOID_KEYBOARD_VIEW: ViewStyle = {
    ...FULL
}

export const SCROLL_VIEW: ViewStyle = {
    paddingBottom: metric.ratioHeight(10),
    alignItems: "center"
}

export const CONTAINER_VIEW: ViewStyle = {
    alignItems: 'center'
}

export const CONTENT_VIEW: ViewStyle = {
    marginTop: metric.ratioHeight(100)
}

export const BUTTON_MARGIN: ViewStyle = {
    marginTop: metric.ratioHeight(50)
}

export const SUBTITLE_VIEW: ViewStyle = {
    marginTop: metric.ratioHeight(56),
    marginHorizontal: metric.ratioWidth(40)
}

export const EMAIL_VIEW: ViewStyle = {
    marginTop: metric.ratioHeight(46),
}

export const TEXT_CONTAINER: ViewStyle = {
    marginTop: metric.ratioHeight(133),
    justifyContent: 'flex-start',
    marginBottom: metric.ratioHeight(56),
}

export const FONT_REGULAR: TextStyle = {
    //Android
    fontFamily: "Montserrat-Regular",
}

export const TITLE: TextStyle = {
    ...TEXT,
    ...FONT_REGULAR,
    fontSize: RFValue(26),
    fontWeight: '400',
}

export const SUB_TITLE: TextStyle = {
    ...TEXT,
    ...FONT_REGULAR,
    fontSize: RFValue(18),
    fontWeight: '400',
    lineHeight: metric.ratioHeight(30),
}

export const EMAIL_TITLE: TextStyle = {
    ...TEXT,
    ...FONT_REGULAR,
    fontSize: RFValue(16),
    fontWeight: '400',
    lineHeight: metric.ratioHeight(30),
}

export const BUTTON_TEXT_MARGIN: ViewStyle = {
    marginTop: metric.ratioHeight(45)
}





export const BACKGROUND_STYLE = color.pinkBlueGradient