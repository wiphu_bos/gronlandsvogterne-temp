import { types, flow } from "mobx-state-tree"
import { VerifyUserPropsModel } from "../viewmodels"
import { APIResponse } from "../../../constants/service.types"
import { RootStore } from "../../../models"

export const VerifyUserServiceActions = types.model(VerifyUserPropsModel).actions(self => {
    const generateVerifyCode = flow(function* (rootStore: RootStore) {
        try {
            return yield rootStore.getUserStore?.generateVerifyCode()
        } catch (e) {
            return (e)
        }
    })

    const resendVerifyCode = flow(function* (rootStore: RootStore) {
        try {
            return yield rootStore.getUserStore?.resendVerifyCode()
        } catch (e) {
            return (e)
        }
    })

    const sendCodeToVerify = flow(function* (rootStore: RootStore) {
        try {
            const code = self.code
            return yield rootStore.getUserStore?.sendCodeToVerify(code)
        } catch (e) {
            return (e)
        }
    })
    const createFirebaseAuth = flow(function* (rootStore: RootStore) {
        try {
            const result: APIResponse = yield rootStore.getUserStore?.createFirebaseAuth()
            return result
        } catch (e) {
            return (e)
        }
    })

    const signInFirebaseAuth = flow(function* (rootStore: RootStore) {
        try {
            const result = yield rootStore.getUserStore?.signInWithEmailAndPassword()
            return result
        } catch (e) {
            return (e)
        }
    })
    return {
        generateVerifyCode,
        sendCodeToVerify,
        createFirebaseAuth,
        signInFirebaseAuth,
        resendVerifyCode
    }
})