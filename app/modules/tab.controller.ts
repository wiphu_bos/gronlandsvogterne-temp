import { RootStore } from "../models/root-store/root.types";
import { INavigationState } from "../models/navigation-store/navigation.types";
import { BaseController, BaseProps, IAlertParams } from "./base.controller";
import { MaterialTopTabBarProps } from "@react-navigation/material-top-tabs";
import { MaterialTopTabDescriptorMap } from "@react-navigation/material-top-tabs/lib/typescript/src/types";
import { TextStyle, Alert } from "react-native";

export class TabController extends BaseController {

    /*
       Mark Inherited Variable & Declaration
   */
    protected _tabState: INavigationState
    protected _tabProps: MaterialTopTabBarProps
    /*
       Mark Setup
   */

    constructor(rootStore?: RootStore, myProps?: BaseProps, isNestedNavigation?: boolean) {
        super(rootStore, myProps, isNestedNavigation)
    }
    protected _setupTabPageState = (state?: INavigationState) => {
        this._tabState = state
        this._onTabPageChange()
    }

    public setupChildProps = (props: MaterialTopTabBarProps) => {
        this._tabProps = props
        this._setupTabPageState((props as any)?.state)
    }

    /*
       Mark Data
   */

    public tapTitleStyle: TextStyle = {

    }

    public getTapTitle = (descriptors: MaterialTopTabDescriptorMap, routeKey: string, route: any) => {
        const { options } = descriptors[routeKey]
        const title =
            options.tabBarLabel !== undefined
                ? options.tabBarLabel
                : options.title !== undefined
                    ? options.title
                    : route.name
        return title
    }

    /*
       Mark Event
   */

    protected _onTabPageChange = () => {
        const index = this._tabState?.index
        const routeNames = this._tabState?.routeNames
    }

    public onPress = (route: any, index: number) => {
        const isFocused = this._tabProps?.state.index === index
        const event = this._tabProps?.navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true
        })

        if (!isFocused && !event.defaultPrevented) {
            this._tabProps?.navigation.navigate(route.name)
        }
    }

    public onLongPress = (route: any) => {
        this._tabProps?.navigation.emit({
            type: 'tabLongPress',
            target: route.key,
        })
    }

    /*
        Mark Utils
    */

    protected static _generalAlertOS = (params: IAlertParams) => {
        TabController._delayExecutor(() => Alert.alert(params.title, params.message), params?.delay || 1000)
    }
}