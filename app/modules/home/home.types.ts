import { Instance, SnapshotOut } from "mobx-state-tree"
import { HomeStoreModel } from "./storemodels"

export type HomeStore = Instance<typeof HomeStoreModel>
export type HomeStoreSnapshot = SnapshotOut<typeof HomeStoreModel>