import { ViewStyle, ImageStyle, TextStyle } from "react-native";
import * as metric from "../../../../theme"
import { color } from "../../../../theme"
import { RFValue } from "react-native-responsive-fontsize";

export const ROOT_CONTAINER_STYLE: ViewStyle = {
    backgroundColor: color.palette.white,
    marginTop: metric.ratioHeight(20),
    borderRadius: metric.ratioWidth(16),
    width: metric.ratioWidth(250),
    height: metric.ratioHeight(216)
}
export const CONTAINNER_STYLE: ViewStyle = {
    backgroundColor: color.palette.white,
    marginTop: metric.ratioHeight(20),
    borderRadius: metric.ratioWidth(16),
    width: metric.ratioWidth(250),
    height: metric.ratioHeight(216)
}

export const CONTENT_CONTAINNER_STYLE: ViewStyle = {
    borderTopLeftRadius: metric.ratioWidth(16),
    borderTopRightRadius: metric.ratioWidth(16),
    width: metric.ratioWidth(250),
    height: metric.ratioHeight(135)
}

export const BUTTON_MENU_CONFIG: any = {
    colors: color.whiteClearGradient
}
export const FOOTER_CONTAINNER_STYLE: ViewStyle = {
    backgroundColor: color.palette.white,
    width: metric.ratioWidth(250),
    height: metric.ratioHeight(82),
    borderBottomLeftRadius: metric.ratioWidth(16),
    borderBottomRightRadius: metric.ratioWidth(16),
    marginTop: -metric.ratioHeight(1)
}

export const IMAGE: ImageStyle = {
    borderRadius: metric.ratioWidth(16),
    left: 0,
    top: 0,
    right: 0,
    bottom: 0,
    position: 'absolute'
}

export const IMAGE_OVERLAY: ViewStyle = {
    flex: 1
}

export const TEXT: TextStyle = {
    color: color.palette.darkBlue,
    fontFamily: "Montserrat",
}

export const TITLE: TextStyle = {
    ...TEXT,
    lineHeight: metric.ratioHeight(24),
    fontSize: RFValue(12),
    fontWeight: '500',

    //Android
    fontFamily: "Montserrat-Medium",
}
export const DESCRIPTION: TextStyle = {
    ...TEXT,
    lineHeight: metric.ratioHeight(24),
    fontSize: RFValue(12),
    fontWeight: '400',

    //Android
    fontFamily: "Montserrat-Regular",
    color: color.palette.metalicGrey
}
export const CONTENT_VIEW: ViewStyle = {
    marginTop: metric.ratioHeight(127),
    marginHorizontal: metric.ratioWidth(19),
    position: 'absolute'
}

export const TITLE_OPTIONS: any = { numberOfLines: 1, ellipsizeMode: 'tail' }
export const DESCRIPTION_OPTIONS: any = { numberOfLines: 2, ellipsizeMode: 'tail' }