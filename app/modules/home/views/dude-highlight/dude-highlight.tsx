// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { DudeHighlightProps } from "./dude-highlight-props"
import * as Styles from "./dude-highlight-styles"
import { Button, Text } from "../../../../components"
import LinearGradient from "react-native-linear-gradient"
import { View } from "react-native"
import FastImage from "react-native-fast-image"
import * as Utils from "../../../../utils"

export const DudeHighlightComponent: React.FunctionComponent<DudeHighlightProps> = observer((props) => {

    // MARK: Render

    const dudeInfo = props?.dudeProfile
    const image = dudeInfo?.profile_image?.url

    return (
        <Button isSolid isAnimated={false} preset="shadow" style={Styles.ROOT_CONTAINER_STYLE} onPress={() => props.onPress(dudeInfo)}>
            {Utils.isValidURL(image) && <FastImage source={{ uri: image }} style={Styles.IMAGE} />}
            <View shouldRasterizeIOS style={Styles.IMAGE_OVERLAY}>
                <LinearGradient shouldRasterizeIOS style={Styles.CONTENT_CONTAINNER_STYLE} {...Styles.BUTTON_MENU_CONFIG} />
                <View shouldRasterizeIOS style={Styles.FOOTER_CONTAINNER_STYLE} />
            </View>
            <View shouldRasterizeIOS style={Styles.CONTENT_VIEW}>
                <Text text={dudeInfo?.full_name?.toUpperCase()} style={Styles.TITLE} {...Styles.TITLE_OPTIONS} />
                <Text text={dudeInfo?.email} style={Styles.DESCRIPTION} {...Styles.DESCRIPTION_OPTIONS} />
            </View>
        </Button>

    )
})