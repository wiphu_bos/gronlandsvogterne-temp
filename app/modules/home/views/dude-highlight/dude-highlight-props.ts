import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { IUser } from '../../../../models/user-store'

export interface DudeHighlightProps extends NavigationContainerProps<ParamListBase> {
    dudeProfile: IUser
    onPress?: (dudeProfile: IUser) => void
}