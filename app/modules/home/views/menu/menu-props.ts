import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { ImageStyle } from "react-native"

export interface MenuProps extends NavigationContainerProps<ParamListBase> {
    iconSource?:string,
    iconStyle?:ImageStyle
    title?:string,
    onPress?: () => void;
}