import { ViewStyle, TextStyle } from "react-native";
import * as metric from "../../../../theme"
import { color } from "../../../../theme"
import { RFValue } from "react-native-responsive-fontsize";

export const CONTAINNER_STYLE: ViewStyle = {
    marginTop: metric.ratioHeight(20),
    borderRadius: metric.ratioWidth(16),
    backgroundColor: color.menuWhiteDim,
    width: metric.ratioWidth(176),
    height: metric.ratioHeight(120),
}

export const ICON_CONTAINNER: ViewStyle = {
    marginTop: metric.ratioHeight(8)
}

export const CONTENT_CONTAINNER: ViewStyle = {
    flex:1,
    alignItems:'center',
    justifyContent:'space-evenly'
}

const TEXT: TextStyle = {
    color: color.palette.white,
    fontFamily: "Montserrat"
}

export const TITLE: TextStyle = {
    ...TEXT,
    lineHeight: metric.ratioHeight(22),
    fontSize: RFValue(15),
    fontWeight: '400',

    //Android
    fontFamily: "Montserrat-Regular",
    color: color.palette.darkBlue,
    textAlign:'center',
}

export const PIN: ViewStyle = {
    width: metric.ratioWidth(30),
    height: metric.ratioHeight(4),
    backgroundColor: color.palette.pink,
    borderRadius: metric.ratioHeight(4) / 2,
    position:'absolute',
    bottom: -metric.ratioHeight(2),
    alignSelf:'center'
}