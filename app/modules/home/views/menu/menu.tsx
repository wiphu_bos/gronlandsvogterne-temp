// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { MenuProps } from "./menu-props"
import * as Styles from "./menu-styles"
import { Icon, Button, Text } from "../../../../components"
import { View } from "react-native"

export const MenuComponent: React.FunctionComponent<MenuProps> = observer((props) => {

    // MARK: Render

    return (
        <Button preset="shadow" style={Styles.CONTAINNER_STYLE} onPress={props.onPress}>
           <View shouldRasterizeIOS style={Styles.CONTENT_CONTAINNER}>
           <Icon source={props.iconSource} style={props.iconStyle} containerStyle={Styles.ICON_CONTAINNER} />
                <Text minimumFontScale={0.1} text={props.title} style={Styles.TITLE} />
            </View>
           <View shouldRasterizeIOS style={Styles.PIN} />
        </Button>
    )
})