// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import * as Styles from "./loading-card-styles"
import { Button } from "../../../../components"
import { images } from "../../../../theme/images"
import FastImage from "react-native-fast-image"

export const LoadingCardComponent: React.FunctionComponent = observer((props) => {

    // MARK: Render
    return (
        <Button isSolid isAnimated={false} preset="none" style={Styles.ROOT_CONTAINER_STYLE}>
            <FastImage source={images.splashLogo} style={Styles.INDICATOR_VIEW} resizeMode={FastImage.resizeMode.contain} />
        </Button>

    )
})