import { ImageStyle, ViewStyle } from "react-native";
import * as metric from "../../../../theme"
import { color } from "../../../../theme"
const iconWidth = metric.ratioWidth(60)
const iconHeight = metric.ratioHeight(62)
export const INDICATOR_VIEW: ImageStyle = {
    alignSelf:'center',
    width: iconWidth,
    height: iconHeight,
    opacity:0.5
}
export const ROOT_CONTAINER_STYLE: ViewStyle = {
    justifyContent:'center',
    alignSelf:'center',
    backgroundColor: color.superWhiteDim,
    marginTop: metric.ratioHeight(20),
    borderRadius: metric.ratioWidth(16),
    width: metric.ratioWidth(320),
    height: metric.ratioHeight(216)
}