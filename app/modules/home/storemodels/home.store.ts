import { types } from "mobx-state-tree"
import { HomeViews, HomeActions, HomeResourcesViews } from "../viewmodels"
import { HomePropsModel } from "../viewmodels"
import { HomeServiceActions } from "../services/home.services"
import { StoreName } from "../../../constants/app.constant"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store/general-resources.store"

const HomeModel = types.model(StoreName.Home, HomePropsModel)

export const HomeStoreModel = types.compose(
    HomeModel,
    HomeViews,
    HomeActions,
    HomeServiceActions)

export const HomeResourcesStoreModel = types.compose(GeneralResourcesStoreModel, HomeResourcesViews)

