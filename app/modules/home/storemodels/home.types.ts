import { Instance, SnapshotOut } from "mobx-state-tree"
import { HomeStoreModel, HomeResourcesStoreModel } from "./home.store"
import { IFireStorage } from "../../../models/user-store/user.types"
import { Property } from "../../../constants/firebase/fire-store.constant"

export type HomeStore = Instance<typeof HomeStoreModel>
export type HomeStoreSnapshot = SnapshotOut<typeof HomeStoreModel>

export type HomeResourcesStore = Instance<typeof HomeResourcesStoreModel>
export type HomeResourcesStoreSnapshot = SnapshotOut<typeof HomeResourcesStoreModel>

export type HighlightDude = {
    [Property.EMAIL]: string,
    [Property.IS_HIGHLIGHT]: boolean,
    [Property.UID]: string,
    [Property.PROFILE_IMAGE]: IFireStorage,
    [Property.FULL_NAME]: string,
}

export type IHomeResponse = {
    highlight_dude_list: HighlightDude[],
    notification_count: number,
    [Property.UNREAD_CHAT_COUNT]: number
}