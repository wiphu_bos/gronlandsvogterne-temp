import { types } from "mobx-state-tree"
import { HomePropsModel } from "./home.models"
export const HomeActions = types.model(HomePropsModel).actions(self => {
    const setIsPullRefreshing = (value: boolean) => self.isPullRefreshing = value
    const resetFetching = () => {
        self.isLoadDone = false
    }
    const setNotificationCount = (value: number) => {
        self.notificationCount = value
    }
    return {
        setIsPullRefreshing,
        resetFetching,
        setNotificationCount
    }
})