import { types } from "mobx-state-tree"
import { HomePropsModel } from "./home.models"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store/general-resources.store"
import { GeneralResources } from "../../../constants/firebase/remote-config/remote-config.constant"
import { NavigationKey } from "../../../constants/app.constant"
import { TestIDResources } from "../../../constants/test-key/test.constant"
import * as Utils from "../../../utils"
import { HighlightDude } from "../storemodels/home.types"
export const HomeViews = types.model(HomePropsModel)
    .views(self => ({
        get getHighlightDudeList(): HighlightDude[] {
            return self.highlighDudeList
        },
        get getNotificationCount(): number {
            return self.notificationCount
        },
        get getUnReadChatCount(): number {
            return self.unreadChatCount
        },
        get getIsPullRefreshing(): boolean {
            return self.isPullRefreshing
        },
        get getIsLoadDone(): boolean {
            return self.isLoadDone
        }
    }))

export const HomeResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State

    const { HomeScreen } = GeneralResources
    const { highlightTitle, homeMenuTitle, searchDudeMenuTitle, createDudeMenuTitle, recommendedMySelfMenuTitle, helpDudetteMenuTitle } = HomeScreen
    const { Home } = NavigationKey

    //MARK: Views
    const getResources = (key: string, childKeyOrShareKey: string | boolean = false, isChildDotNotation: boolean = false) => self.getValues(childKeyOrShareKey ? key : Home, childKeyOrShareKey ? true : key, isChildDotNotation)
    const getResourceHighlightTitle = () => getResources(highlightTitle)
    const getResourceMenuTitle = () => getResources(homeMenuTitle)
    const getResourceButtonCreateDudeTitle = () => getResources(createDudeMenuTitle, false, true)
    const getResourceButtonSearchDudeTitle = () => getResources(searchDudeMenuTitle, false, true)
    const getResourceButtonRecommendedMySelfTitle = () => getResources(recommendedMySelfMenuTitle, false, true)
    const getResourceButtonHelpDudetteTitle = () => getResources(helpDudetteMenuTitle, false, true)

    return {
        getResourceHighlightTitle,
        getResourceMenuTitle,
        getResourceButtonCreateDudeTitle,
        getResourceButtonSearchDudeTitle,
        getResourceButtonRecommendedMySelfTitle,
        getResourceButtonHelpDudetteTitle
    }
})
    .views(self => {

        //MARK: Volatile State

        //MARK: Views

        const getTestIDHighlightTitle = () => Utils.getTestIDObject(TestIDResources.Home.highlightTitle)
        const getTestIDHighlightDudeList = () => Utils.getTestIDObject(TestIDResources.Home.highlightDudeList)
        const getTestIDHomeMenuTitle = () => Utils.getTestIDObject(TestIDResources.Home.homeMenuTitle)
        const getTestIDButtonCreateDudeTitle = () => Utils.getTestIDObject(TestIDResources.Home.bottonCreateDude)
        const getTestIDButtonSearchDudeTitle = () => Utils.getTestIDObject(TestIDResources.Home.bottonSearchDude)
        const getTestIDButtonRecommendedMySelfTitle = () => Utils.getTestIDObject(TestIDResources.Home.bottonRecommendedMySelf)
        const getTestIDButtonHelpDudetteTitle = () => Utils.getTestIDObject(TestIDResources.Home.bottonHelpDudette)
        return {
            getTestIDHighlightTitle,
            getTestIDHighlightDudeList,
            getTestIDHomeMenuTitle,
            getTestIDButtonCreateDudeTitle,
            getTestIDButtonSearchDudeTitle,
            getTestIDButtonRecommendedMySelfTitle,
            getTestIDButtonHelpDudetteTitle
        }
    })
