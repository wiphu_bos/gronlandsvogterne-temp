import { types } from "mobx-state-tree"
import { HighlightDude } from "../storemodels/home.types"

export const HomePropsModel = {
    highlighDudeList: types.optional(types.frozen<HighlightDude[]>(), []),
    unreadChatCount: types.optional(types.number, 0),
    notificationCount: types.optional(types.number, 0),
    isPullRefreshing: types.optional(types.boolean, false),
    isLoadDone: types.optional(types.boolean, false),
} 