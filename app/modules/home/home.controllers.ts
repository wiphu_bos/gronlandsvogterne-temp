// MARK: Import

import { NavigationKey, SearchListType, UserType } from "../../constants/app.constant"
import { HomeProps } from "./home.props"
import { HomeResourcesStoreModel, HomeStoreModel } from "./storemodels/home.store"
import { HomeResourcesStore } from "./storemodels/home.types"
import { RootStore, INavigationRoute } from "../../models"
import { onSnapshot } from "mobx-state-tree"
import { HomeStore } from "./home.types"
import { StackActions, DrawerActions } from "@react-navigation/core"
import { FilterStoreModel } from "../../models/filter-store/filter-store.store"
import { FirebaseFirestoreTypes } from "@react-native-firebase/firestore"
import { IUser, IPaymentResponse } from "../../models/user-store/user.types"
import * as DataUtils from "../../utils/data.utils"
import FirebaseFireStore from "../../utils/firebase/fire-store/fire-store"
import { IUserListener } from "../../utils/firebase/fire-store/fire-store.types"
import { BaseController } from "../base.controller"
import moment from "moment"
import { GeneralResources } from "../../constants/firebase/remote-config/remote-config.constant"
import PushNotificationIOS from "@react-native-community/push-notification-ios"
import PushNotification from "react-native-push-notification"
import * as metric from "../../theme"

class HomeController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static viewModel: HomeStore
    public static resourcesViewModel: HomeResourcesStore
    private static _myProfileDisposer: () => void
    private static _mySubscriptionDisposer: () => void
    private static _myUnReadNotificationDisposer: () => void

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, myProps?: HomeProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, myProps, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupNavigation()
        this._setupProps(myProps)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        HomeController.resourcesViewModel = HomeResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.Home)
        HomeController.viewModel = localStore && HomeStoreModel.create({ ...localStore }) || HomeStoreModel.create({})

    }

    private _setupSnapShot = () => {
        onSnapshot(HomeController.viewModel, (snap: HomeStore) => {
            this._setInitializedToPropsParams()
            this._rootStore?.setModuleStore({ [NavigationKey.Home]: snap })
        })
    }

    /*
       Mark Data
   */

    private _refreshHomeContent = async () => {
        HomeController.viewModel?.setIsPullRefreshing(true)
        await this._fetchHomeContent()
    }
    private _fetchHomeContent = async () => await HomeController.viewModel?.fetchHomeContent()


    /*
       Mark Event
   */

    public onHighLightDudeCardPressed = async (item: IUser) => {
        if (!item?.uid) return
        this._isGlobalLoading(true)
        const dudeProfile: IUser = await HomeController.viewModel?.fetchDudeProfileDetails(this._rootStore, item?.uid)
        this._isGlobalLoading(false)
        if (!dudeProfile) return
        const dudeProfileObject = DataUtils.getUserProfileObject(this._rootStore, null, dudeProfile)
        this._myProps?.navigation?.dispatch(StackActions.push(NavigationKey.DudeProfileDetailTab, { dudeProfileObject: dudeProfileObject }) as any)

    }

    private _onMyProfileListener = () => {
        const onNext: (snapshot: FirebaseFirestoreTypes.QueryDocumentSnapshot) => void = (snap => {
            if (snap.metadata.hasPendingWrites) return
            const userData: Partial<IUser> = snap.data()
            const userDataWithSubscriptionDetails = DataUtils.addSubscriptionToUserProfileObject(this._rootStore, userData)
            if (userData) {
                const userProfile = DataUtils.getUserProfileObject(this._rootStore,
                    this._rootStore?.getUserStore,
                    userDataWithSubscriptionDetails)
                this._rootStore?.getUserStore?.bindingDataFromObject(userProfile, UserType.Me)
            } else {
                this._rootStore?.getUserStore?.forceLogoutWithoutAuth(this._rootStore)
            }
        })

        const onError: (e: Error) => void = (e => {
        })

        const onComplete: () => void = () => {
        }

        const params: IUserListener = {
            userId: this._rootStore?.getUserStore?.getUserId,
            onNext,
            onError,
            onComplete
        }

        HomeController._myProfileDisposer = FirebaseFireStore.onMyProfileListener(params)
    }

    private _onMySubscriptionListener = () => {
        const onNext: (snapshot: FirebaseFirestoreTypes.QueryDocumentSnapshot) => void = (snap => {
            const mySubscriptionData: Partial<IPaymentResponse> = snap.data()
            const expiryDate = moment(mySubscriptionData?.expiry_date?.toDate())
            const isSubscribed = mySubscriptionData?.transaction_id === undefined ? false : true
            this._rootStore?.getUserStore?.setExpiryDate(expiryDate)
            this._rootStore?.getUserStore?.setIsSubscription(isSubscribed)
        })

        const onError: (e: Error) => void = (e => {
        })

        const onComplete: () => void = () => {
        }

        const params: IUserListener = {
            userId: this._rootStore?.getUserStore?.getUserId,
            onNext,
            onError,
            onComplete
        }

        HomeController._mySubscriptionDisposer = FirebaseFireStore.onMySubscriptionListener(params)
    }

    private _onUnReadNotificationListener = () => {
        const onNext: (snapshot: FirebaseFirestoreTypes.QuerySnapshot) => void = (snap => {
            const unreadCount = snap?.size || 0
            HomeController.viewModel?.setNotificationCount(unreadCount)
            if (metric.isIPhone) PushNotificationIOS.setApplicationIconBadgeNumber(unreadCount)
            PushNotification.setApplicationIconBadgeNumber(unreadCount)
        })

        const onError: (e: Error) => void = (e => {
        })

        const onComplete: () => void = () => {
        }

        const params: IUserListener = {
            userId: this._rootStore?.getUserStore?.getUserId,
            onNext,
            onError,
            onComplete
        }

        HomeController._myUnReadNotificationDisposer = FirebaseFireStore.onUnReadNotificationListener(params)
    }

    public onHomeMenuButtonPressed = (identifier: any) => {
        let actions: any
        switch (identifier) {
            case SearchListType.Dude:
            case SearchListType.Dudette:
                const routeKey: string = NavigationKey.SearchListScreen
                this._rootStore?.setFilterStore(FilterStoreModel.create({}), this._rootStore)
                actions = DrawerActions.jumpTo(routeKey, {
                    type: identifier === SearchListType.Dude ? SearchListType.Dude : SearchListType.Dudette,
                    isShowFilter: true,
                    isFetchData: true
                })
                break
            case NavigationKey.CreateDude:
                actions = StackActions.push(identifier)
                break
             case NavigationKey.Indberetning:
                console.log('jwb activity')
                actions = StackActions.push(identifier)
            break
            case NavigationKey.SOS:
                console.log('jwb activity')
                actions = StackActions.push(identifier)
            break
            case NavigationKey.Menu:
                console.log('jwb activity')
                actions = StackActions.push(identifier)
            break
            case NavigationKey.Feed :
                    console.log('jwb activity')
                    actions = StackActions.push(identifier)
                    case NavigationKey.Indberetning :
                        console.log('jwb activity')
                        actions = StackActions.push(identifier)
           break
            case NavigationKey.ManageProfileTab:
            actions = DrawerActions.jumpTo(NavigationKey.ManageProfileTab, {
                screen: this._rootStore?.getGeneralResourcesStore(NavigationKey.ManageProfileTab, GeneralResources.ManageProfileTabScreen.myProfileTabMenuTitle),
            })
            break
            default:
                actions = StackActions.push(identifier)
                break
        }

        this._myProps?.navigation?.dispatch(actions)
    }

    public onRefresh = async () => {
        HomeController.viewModel?.resetFetching()
        const isConnected = this._rootStore?.getSharedStore?.getIsConnected
        if (isConnected) {
            await this._refreshHomeContent()
        }
    }

    //@override
    backProcess = () => {

    }

    public onSideMenuPress = () => {
        this._myProps?.navigation?.dispatch(DrawerActions.openDrawer() as any)
    }

    public onNotificationPress = () => {
        this._myProps?.navigation?.dispatch(StackActions.push(NavigationKey.NotificationList) as any)
    }

    /*
           Mark Life cycle
    */
    viewDidAppearOnce = async () => {
        this._onMyProfileListener()
        this._onMySubscriptionListener()
        this._onUnReadNotificationListener()
        this.onRefresh()
    }


    //@override
    public deInit = () => {
        super.deInit && super.deInit()
        HomeController._myProfileDisposer && HomeController._myProfileDisposer()
        HomeController._mySubscriptionDisposer && HomeController._mySubscriptionDisposer()
        HomeController._myUnReadNotificationDisposer && HomeController._myUnReadNotificationDisposer()
    }

}

export default HomeController