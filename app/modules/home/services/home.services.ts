import { types, flow } from "mobx-state-tree"
import { HomePropsModel } from "../viewmodels"
import { request, router } from "../../../services/api"
import { ServiceType } from "../../../constants/service.constant"
import { IHomeResponse } from "../storemodels/home.types"
import { APIResponse, APISuccessResponse } from "../../../constants/service.types"
import { RootStore } from "../../../models/root-store"
import { IUser } from "../../../models/user-store/user.types"

export const HomeServiceActions = types.model(HomePropsModel).actions(self => {

    //MARK: Volatile State

    //MARK: Service Actions

    const fetchDudeProfileDetails = flow(function* (rootStore?: RootStore, dudeId?: string) {
        try {
            let result: APISuccessResponse = yield rootStore?.getUserStore?.fetchDudeProfileDetail(dudeId)
            const data: APISuccessResponse = result?.data
            const profileDetail: IUser = data?.data
            if (!profileDetail) throw data
            return profileDetail
        } catch (e) {
            return e
        }
    })

    const fetchHomeContent = flow(function* () {
        if (self.isPullRefreshing) {
            self.highlighDudeList = []
        }
        try {
            const result: APIResponse = yield request(router(ServiceType.GetHomeContent))
            let success = (result as APISuccessResponse)?.data
            let data: IHomeResponse = success?.data
            if (!data) throw result
            self.highlighDudeList = data?.highlight_dude_list
            self.notificationCount = data?.notification_count
            self.unreadChatCount = data?.unread_chat_count
            return data
        } catch (e) {
            return (e)
        } finally {
            self.isPullRefreshing = false
            self.isLoadDone = true
        }
    })

    return {
        fetchHomeContent,
        fetchDudeProfileDetails
    }
})