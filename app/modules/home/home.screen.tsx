// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { AppNameSlogan, Wallpaper, Header, Text, AnimatedScrollView, Button } from "../../components"
import { View, RefreshControl } from "react-native"
import { HomeProps } from "./home.props"
import { SafeAreaView } from "react-navigation"
import { useConfigurate } from "../../custom-hooks/use-configure-controller"
import { FlatList } from "react-native-gesture-handler"
import { DudeHighlightComponent } from "./views/dude-highlight/dude-highlight"
import { MenuComponent } from "./views/menu/menu"
import { images } from "../../theme/images"
import * as Animatable from 'react-native-animatable'
import * as metric from "../../theme"
import { LoadingCardComponent } from "./views/loading-card/loading-card"
import { SearchListType, NavigationKey } from "../../constants/app.constant"
import HomeController from "./home.controllers"
import { useStores } from "../../models/root-store"

// MARK: Style Import

import * as Styles from "./home.styles"

export const HomeScreen: React.FunctionComponent<HomeProps> = observer((props) => {



    const rootStore = useStores()


    const controller = useConfigurate(HomeController, props) as HomeController

    // MARK: Render


    const renderDudeHighlighSection = () => {
        if (HomeController.viewModel?.getHighlightDudeList?.length == 0 && HomeController.viewModel?.isLoadDone) return null
        return <Animatable.View
            {...metric.animatedViewOptions}
            style={Styles.CONTAINER_LIST_VIEW} >
            <View shouldRasterizeIOS style={Styles.TITLE_CONTAINNER}>
                <Text text={HomeController.resourcesViewModel?.getResourceHighlightTitle()}
                    style={Styles.TITLE}
                    {...HomeController.resourcesViewModel?.getTestIDHighlightTitle()} />
            </View>
            {
                HomeController.viewModel?.getHighlightDudeList?.length > 0 ?
                    <FlatList
                        {...HomeController.resourcesViewModel?.getTestIDHighlightDudeList()}
                        contentContainerStyle={Styles.LIST_VIEW}
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        ItemSeparatorComponent={({ highlighted }) => (
                            <View shouldRasterizeIOS style={Styles.SEPARATOR_VIEW} />
                        )}
                        extraData={HomeController.viewModel?.getHighlightDudeList}
                        data={HomeController.viewModel?.getHighlightDudeList}
                        renderItem={({ item, index, separators }) => (
                            <DudeHighlightComponent navigation={props?.navigation} dudeProfile={item} key={index} onPress={controller.onHighLightDudeCardPressed} />
                        )}
                    />
                    : <LoadingCardComponent />
            }
        </Animatable.View >
    }

    const changeFlag = () => {
        const flag = rootStore.AuthStore.getLoggedIn
        rootStore.AuthStore.setIsLoggedIn(!flag)
    }





    return (
        <Wallpaper showBubble>

            <SafeAreaView
                {...metric.safeAreaViewProps}
                shouldRasterizeIOS
                style={Styles.SAFE_AREA_VIEW}>
                <Header {...Styles.HEADER_PROPS} onLeftPress={controller.onSideMenuPress}
                    rightIconBadgeNumber={HomeController.viewModel?.getNotificationCount}
                    onRightPress={controller.onNotificationPress} />
                <AnimatedScrollView
                    contentContainerStyle={Styles.SCROLL_VIEW}
                    showsVerticalScrollIndicator={false}
                    refreshControl={
                        <RefreshControl
                            style={Styles.REFRESH_CONTROL}
                            refreshing={false}
                            onRefresh={controller.onRefresh}
                        />
                    }>
                    <AppNameSlogan style={Styles.APP_SLOGAN} />
                    {renderDudeHighlighSection()}
                    <View shouldRasterizeIOS style={Styles.CONTAINER_MENU_VIEW}>
                        <Text {...HomeController.resourcesViewModel?.getTestIDHomeMenuTitle()} text={HomeController.resourcesViewModel?.getResourceMenuTitle()} style={Styles.TITLE} />
                        <View shouldRasterizeIOS style={Styles.MENU_VIEW}>
                            <MenuComponent iconSource={images.dude} iconStyle={Styles.ICON_PORTRAIT}
                                {...HomeController.resourcesViewModel?.getTestIDButtonSearchDudeTitle()}
                                title={HomeController.resourcesViewModel?.getResourceButtonSearchDudeTitle()}
                                onPress={() => controller.onHomeMenuButtonPressed(SearchListType.Dude)}
                            />
                            <MenuComponent iconSource={images.addDude} iconStyle={Styles.ICON_SQUARE}
                                {...HomeController.resourcesViewModel?.getTestIDButtonCreateDudeTitle()}
                                title={HomeController.resourcesViewModel?.getResourceButtonCreateDudeTitle()}
                                onPress={() => controller.onHomeMenuButtonPressed(NavigationKey.CreateDude)} />
                        </View>
                        <View shouldRasterizeIOS style={{ ...Styles.MENU_VIEW, ...Styles.ADDITIONAL_MENU_MARGIN_BUTTON }}>
                            <MenuComponent iconSource={images.recommendedMySelf} iconStyle={Styles.ICON_PORTRAIT}
                                {...HomeController.resourcesViewModel?.getTestIDButtonRecommendedMySelfTitle()}
                                title={HomeController.resourcesViewModel?.getResourceButtonRecommendedMySelfTitle()}
                                onPress={() => controller.onHomeMenuButtonPressed(NavigationKey.ManageProfileTab)}
                            />
                            <MenuComponent iconSource={images.dudette} iconStyle={Styles.ICON_PORTRAIT}
                                {...HomeController.resourcesViewModel?.getTestIDButtonHelpDudetteTitle()}
                                title={HomeController.resourcesViewModel?.getResourceButtonHelpDudetteTitle()}
                                onPress={() => controller.onHomeMenuButtonPressed(SearchListType.Dudette)} />




                        {/* <Button
                        isAnimated={false}
                        text='hello'
                        /> */}

                        </View>
                    </View>
                </AnimatedScrollView>
            </SafeAreaView>
        </Wallpaper>
    )
})
