import { Instance, SnapshotOut } from "mobx-state-tree"
// import { IndberetningStoreModel, IndberetningResourcesStoreModel } from "./atest-list.store"
import { IndberetningStoreModel, IndberetningResourcesStoreModel } from "./indberetning.store"
import { INotification } from "../../../models/notification-store"

export type IndberetningStore = Instance<typeof IndberetningStoreModel>
export type IndberetningStoreSnapshot = SnapshotOut<typeof IndberetningStoreModel>

export type IndberetningResourcesStore = Instance<typeof IndberetningResourcesStoreModel>
export type IndberetningResourcesStoreSnapshot = SnapshotOut<typeof IndberetningResourcesStoreModel>


export type INotificationPage = {
    page_size?: number,
    is_last_page?: boolean,
    last_id?: string,
}

export type INotificationResponse = {
    notification_list?: INotification[],
    page?: INotificationPage
}