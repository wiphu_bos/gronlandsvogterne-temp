import { types } from "mobx-state-tree"
import { StoreName } from "../../../constants/app.constant"
import { IndberetningPropsModel } from "../viewmodels/indberetning.models"
import { IndberetningActions } from "../viewmodels/indberetning.actions"
import { IndberetningViews, IndberetningResourcesViews } from "../viewmodels/indberetning.views"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store"
import { IndberetningServiceActions } from "../services/indberetning.services"

const IndberetningModel = types.model(StoreName.NotificationList, IndberetningPropsModel)
export const IndberetningStoreModel = types.compose(IndberetningModel, IndberetningViews, IndberetningActions, IndberetningServiceActions)
export const IndberetningResourcesStoreModel = types.compose(GeneralResourcesStoreModel, IndberetningResourcesViews)

