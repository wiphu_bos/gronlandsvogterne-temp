import { types, unprotect } from "mobx-state-tree"
import { IndberetningPropsModel } from "./indberetning.models"
import { INotification, NotificationStoreModel } from "../../../models/notification-store"
import { RootStore } from "../../../models"

export const IndberetningActions = types.model(IndberetningPropsModel).actions(self => {

    const findExistingItemIndex = (notification: INotification): number => {
        const tempChatList = self.notificationList$
        const existingList = tempChatList?.slice() //Slice to disobserve property
        const foundItemIndex = existingList?.findIndex(v => v.notification_id === notification.notification_id)
        return foundItemIndex
    }
    const setNotificationList = (rootStore: RootStore, list: INotification[]) => {
        list.forEach(e => {
            const childViewModel = NotificationStoreModel.create(e)
            childViewModel.setIsTimerSet(rootStore, true)
            unprotect(childViewModel)
            self.notificationList$.push(childViewModel)
        })
        self.isPullRefreshing = false
        self.isLoadMore = false
    }

    const addNotificationIntoList = (rootStore: RootStore, notification: INotification) => {
        const foundItemIndex = findExistingItemIndex(notification)
        if (foundItemIndex !== -1) return
        const tempList = self.notificationList$
        const childViewModel = NotificationStoreModel.create(notification)
        childViewModel.setIsTimerSet(rootStore, true)
        unprotect(childViewModel)
        self.notificationList$ = [].concat.apply(childViewModel, tempList)
    }

    const invalidateAllTimerIfNeeded = () => {
        self.notificationList$.forEach(e => {
            e.setIsTimerSet(null, false)
        })
    }

    const setIsPullRefreshing = (value: boolean) => self.isPullRefreshing = value
    const setIsLoadMore = (value: boolean) => self.isLoadMore = value
    const setIsFirstLoadDone = (value: boolean) => self.isFirstLoadDone = value
    const resetFetching = () => {
        invalidateAllTimerIfNeeded()
        self.notificationList$.clear()
        self.isLoadDone = false
        self.isLoadMore = false
        self.isFirstLoadDone = false
    }
    return {
        setNotificationList,
        addNotificationIntoList,
        invalidateAllTimerIfNeeded,
        setIsPullRefreshing,
        resetFetching,
        setIsLoadMore,
        setIsFirstLoadDone
    }
})