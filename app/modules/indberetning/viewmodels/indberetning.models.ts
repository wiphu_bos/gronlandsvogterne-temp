import { types } from "mobx-state-tree"
import { NotificationStoreModel } from "../../../models/notification-store/notification-store.store"

export const IndberetningPropsModel = {
    last_id: types.maybeNull(types.string),
    notificationList$: types.optional(types.array(NotificationStoreModel), []),
    isPullRefreshing: types.optional(types.boolean, false),
    isLoadDone: types.optional(types.boolean, false),
    isLoadMore: types.optional(types.boolean, false),
    isFirstLoadDone: types.optional(types.boolean, false)
} 