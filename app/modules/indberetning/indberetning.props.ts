import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"
import { ExtractCSTWithSTN } from 'mobx-state-tree/dist/core/type/type'
import { NotificationStoreModel } from '../../models/notification-store'

export interface IndberetningProps extends NavigationContainerProps<ParamListBase> {
    animation?: any,
    timeAgoTestID?: any,
    notification$?: ExtractCSTWithSTN<typeof NotificationStoreModel>,
    onPress?: (favouriteDetails$?: ExtractCSTWithSTN<typeof NotificationStoreModel>) => void
}