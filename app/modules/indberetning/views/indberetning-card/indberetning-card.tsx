// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { Text, Button } from "../../../../components"
import { View, Alert, TouchableWithoutFeedback, StyleSheet, Image } from "react-native"
import { IndberetningProps } from "../../indberetning.props"
import { images } from "../../../../theme/images"
import { Icon } from "../../../../components"

// MARK: Style Import


export const IndberetningComponents: React.FunctionComponent<IndberetningProps> = observer((props) => {

    // const title = ATestListController.resourcesViewModel?.getResourceNotificationTitle(props?.notification$?.title)
    // const body = ATestListController.resourcesViewModel?.getResourceNotificationTitle(props?.notification$?.body)
    const title =  props?.notification$?.title
    const body =  props?.notification$?.body

   return (

         <TouchableWithoutFeedback >
             <View style={flatListStyle.cell}>
                    <View style={{ width: '30%', justifyContent: 'center', alignItems: 'center',}}>
                      <Icon source={images.infomation} style={flatListStyle.imageView} />
                    </View>
                    <View style={{width: '70%', justifyContent: 'center'}}>
                      <Text style={flatListStyle.textView} >{title}</Text>
                    </View>
                  </View>
        </TouchableWithoutFeedback>
    )
 })

const flatListStyle = StyleSheet.create({
    flatListView: {
      width: '100%',
      height: '50%' ,
      padding:15,
      backgroundColor:'#880000',
    },
  
    cell :{
      flex:1, margin: 5,
      backgroundColor: '#880000', padding : 5,
      height: 60,
      flexDirection:"row",
    },
  
    imageView: {
      width: 40,
      height: 40 ,
      margin: 0,
      padding:0,
      borderRadius : 1000,
      overflow: 'hidden',
    },
  
    textView: {
      width:'90%',
      height: '35%' ,
      textAlign:'center',
      margin: 5,
      padding:0,
      color:'#000000', backgroundColor:'#ffffff',
    },
  
    textBottomImage: {
      width:'90%',
      height: '40%' ,
      textAlign:'center',
      margin: 10,
      padding:0,
      color:'#000000', backgroundColor:'#ffffff',
      fontSize: 8,
    }
  });