// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { IndberetningProps } from "./indberetning.props"
import IndberetningController from "./indberetning.controllers"
import { useConfigurate } from "../../custom-hooks/use-configure-controller"
import { Wallpaper, Header, Text } from "../../components"
import { View, RefreshControl, StyleSheet, TouchableOpacity, Image } from "react-native"
import { FlatList } from "react-native-gesture-handler"
import { IndberetningComponents } from "./views/indberetning-card/indberetning-card"
import { SafeAreaView } from "react-navigation"
import * as metric from "../../theme"
import { images } from "../../theme/images"

// MARK: Style Import

import * as Styles from "./indberetning.styles"

export const IndberetningScreen: React.FunctionComponent<IndberetningProps> = observer((props) => {

    const controller = useConfigurate(IndberetningController, props) as IndberetningController

    // MARK: Render
    return (
        <Wallpaper showBubble>
            <SafeAreaView
                {...metric.safeAreaViewProps}
                shouldRasterizeIOS
                style={Styles.SAFE_AREA_VIEW}>
                <View shouldRasterizeIOS style={Styles.BODY_CONTAINER}>
                    <FlatList  style={flatListStyle.flatListView}
                        contentInsetAdjustmentBehavior="automatic"

                        ItemSeparatorComponent={({ highlighted }) => (
                            <View shouldRasterizeIOS style={Styles.SEPARATOR_VIEW} />
                        )}
                        data={IndberetningController.viewModel?.getNotificationList}
                        numColumns={1}

                        renderItem={({ item, index, separators }) => (

                            <IndberetningComponents
                                animation={controller.flatlistResultAnimationConfig}
                                key={index}
                                notification$={item}
                                onPress={() => controller.onNotificationItemPress(item)} />
            
                        )}
                    />

                  <View style={footerStyle.footerView}> 
                        <TouchableOpacity
                                  style={footerStyle.touchableStyle}
                                  onPress={this.gotoNative}
                                  underlayColor='#fff'>
                                <View style={{flexDirection:"row",justifyContent: 'center'}}>
                                    <View style={{ width: '12%'}}>
                                      {/* <Image source = {require("./profile.png")} style={footerStyle.footerImage}/> */}
                                      <Image source = {images.whitePin} style={footerStyle.footerImage}/>

                                    </View>
                                    <View style={{width: '55%'}}>
                                      <Text style={footerStyle.buttonStyle}>OPRET INDBERETNIG</Text>
                                    </View>
                                </View>
                        </TouchableOpacity>
                    </View>

                </View>
            </SafeAreaView>
        </Wallpaper>

    )
})

const styles = StyleSheet.create({
    MainContainer :{
        backgroundColor: '#440000',
        padding: 0,
        justifyContent: 'center',
        flex:1,
        margin: 0,
    },
  });
  
  const flatListStyle = StyleSheet.create({
    flatListView: {
      width: '100%',
      height: '50%' ,
      padding:15,
  
      backgroundColor:'#ffffff',
    },
  
    cell :{
      flex:1, margin: 5,
      backgroundColor: '#000000', padding : 5,
      height: 100,
      flexDirection:"row",
    },
  
    imageView: {
      width: 30,
      height: 30 ,
      margin: 0,
      padding:0,
      borderRadius : 1000,
      overflow: 'hidden',
    },
  
    textView: {
      width:'90%',
      height: '15%' ,
      textAlign:'center',
      margin: 5,
      padding:0,
      color:'#000000', backgroundColor:'#ffffff',
    },
  
    textBottomImage: {
      width:'90%',
      height: '40%' ,
      textAlign:'center',
      margin: 10,
      padding:0,
      color:'#000000', backgroundColor:'#ffffff',
      fontSize: 8,
    }
  });
  
  const footerStyle = StyleSheet.create({
    footerView: {
      flex: 0.3,
      width: '100%',
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor : "#ff0000",
  
    },
    touchableStyle:{
      height: '60%',
      width: '60%',
      marginRight:40,
      marginLeft:40,
      marginTop:10,
      paddingTop:10,
      paddingBottom:10,
      backgroundColor:'#1E6738',
      borderRadius:10,
      borderWidth: 2,
      borderColor: '#ff0000',
      justifyContent: 'center',
    },
  
    footerImage: {
      flex: 1,
      width: null,
      height: null,
      resizeMode: 'contain',
      // backgroundColor:'#ffffff',
    },
  
    buttonStyle:{
        color:'#fff',
        textAlign:'left',
        paddingLeft : 0,
        paddingRight : 10,
        fontSize: 10,
    },
  });
  
  
  
  
  
  
  