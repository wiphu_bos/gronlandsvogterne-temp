import { Instance, SnapshotOut } from "mobx-state-tree"
import { LoginStoreModel, LoginResourceStoreModel } from "./login.store"

export type LoginStore = Instance<typeof LoginStoreModel>
export type LoginStoreSnapshot = SnapshotOut<typeof LoginStoreModel>

export type LoginResourcesStore = Instance<typeof LoginResourceStoreModel>
export type LoginResourcesStoreSnapshot = SnapshotOut<typeof LoginResourceStoreModel>