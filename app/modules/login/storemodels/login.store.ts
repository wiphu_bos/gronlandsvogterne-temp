import { types } from "mobx-state-tree"
import { LoginViews, LoginActions, LoginResourcesViews } from "../viewmodels"
import { LoginPropsModel } from "../viewmodels"
import { LoginServiceActions } from "../services/login.services"
import { StoreName } from "../../../constants/app.constant"
import { ValidationStoreModel } from "../../../models/validation-store"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store"
import { UserStoreModel } from "../../../models/user-store/user.store"

const LoginModel = types.model(StoreName.Login, LoginPropsModel)

export const LoginStoreModel = types.compose(
    LoginModel,
    LoginViews,
    LoginActions,
    LoginServiceActions,
    UserStoreModel,
    ValidationStoreModel)
export const LoginResourceStoreModel = types.compose(GeneralResourcesStoreModel, LoginResourcesViews)
