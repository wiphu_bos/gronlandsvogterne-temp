// MARK: Import

import { NavigationKey } from "../../constants/app.constant"
import { LoginProps } from "./login.props"
import { RootStore, INavigationRoute } from "../../models"
import { onSnapshot } from "mobx-state-tree"
import { StackActions } from "@react-navigation/native"
import { LoginStore, LoginStoreModel, LoginResourcesStore, LoginResourceStoreModel } from "./storemodels"
import * as Validate from "../../utils/local-validate"
import { BaseController } from "../base.controller"
import { Keyboard } from "react-native"
import { ValidationServices } from "../../services/api-domain/validate.services"

class LoginController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static viewModel: LoginStore
    public static resourcesViewModel: LoginResourcesStore

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, myProps?: LoginProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, myProps, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(myProps)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        LoginController.resourcesViewModel = LoginResourceStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.Login)
        LoginController.viewModel = localStore && LoginStoreModel.create({ ...localStore }) || LoginStoreModel.create({})
    }

    private _setupSnapShot = () => {
        onSnapshot(LoginController.viewModel, (snap: LoginStore) => {
            this._setInitializedToPropsParams()
        })
    }

    /*
       Mark Data
   */


    /*
       Mark Event
   */

    private _isLoading = (value: boolean) => this._rootStore?.getSharedStore?.setIsLoading(value)

    public onSubmitButtonPressed = async () => {
        /* jwb button pressed */
        console.log('jwb : onSubmitButtonPressed')

        Keyboard.dismiss()
        this._checkEmptyData()
        this._validateData()
        if (!LoginController.viewModel?.getIsValid) return
        this._isLoading(true)
        const result = await LoginController.viewModel?.signInFirebaseAuth(this._rootStore)
        this._isLoading(false)
        const userId = result?.uid
        if (!userId) {
            const params = {
                rootStore: this._rootStore,
                localViewModel: LoginController.viewModel as any,
                error: result
            }
            const alertMessage = new ValidationServices(params).generalAPIResponseValidation()
            if (alertMessage) this._generalAlertOS({ message: alertMessage })
        }
    }

    public onEmailChangeText = (value: string): void => {
        const params = {
            value: value,
            localViewModel: LoginController.viewModel as any,
            globalViewModel: this._rootStore?.getUserStore
        }
        Validate.onEmailChangeText(params)
    }

    public onPasswordChangeText = (value: string): void => {
        const params = {
            value: value,
            localViewModel: LoginController.viewModel as any,
            globalViewModel: this._rootStore?.getUserStore
        }
        Validate.onPasswordChangeText(params)
    }

    public onBlurEmailTextField = (): void => {
        const email = LoginController.viewModel?.getEmail
        const params = {
            value: email,
            localViewModel: LoginController.viewModel as any,
            rootStore: this._rootStore
        }
        Validate.onBlurEmailTextField(params)
    }

    public onBlurPasswordField = (): void => {
        const password = LoginController.viewModel?.getPassword
        const params = {
            value: password,
            localViewModel: LoginController.viewModel as any,
            rootStore: this._rootStore
        }
        Validate.onBlurPasswordField(params)
    }

    public goToWelcomeScreen = () => {
        this._myProps?.navigation?.dispatch(StackActions.popToTop() as any)
    }

    public goToForgotPassword = () => {
        this._myProps?.navigation?.dispatch(StackActions.push(NavigationKey.ForgotPassword) as any)
    }

    //@override
    backProcess = () => {

    }

    /*
       Mark Helper
   */

    private _validateData = (): void => {
        this.onBlurEmailTextField()
        this.onBlurPasswordField()
    }
    private _checkEmptyData = () => {
        const { getEmail, getPassword } = LoginController.viewModel
        LoginController.viewModel?.setIsValid(getEmail && getPassword ? true : false)
    }
}

export default LoginController