import { types, flow } from "mobx-state-tree"
import { LoginPropsModel } from "../viewmodels"
import { RootStore } from "../../../models/root-store"

export const LoginServiceActions = types.model(LoginPropsModel).actions(self => {
    const signInFirebaseAuth = flow(function* (rootStore: RootStore) {
        try {
            console.log('jwb : signInFirebaseAuth')

            return yield rootStore.getUserStore?.signInWithEmailAndPassword()
        } catch (e) {
            return (e)
        }
    })
    return { signInFirebaseAuth }
})