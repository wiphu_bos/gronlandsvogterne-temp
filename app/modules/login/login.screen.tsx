// MARK: Import

import React, { useRef } from "react"
import { observer } from "mobx-react-lite"
import { View, KeyboardAvoidingView } from "react-native"
import { LoginProps } from "./login.props"
import LoginController from "./login.controllers"
import { Button, AppNameSlogan, Wallpaper, AnimatedScrollView } from "../../components"
import { SafeAreaView } from "react-navigation"
import FloatLabelTextInput from '../../libs/float-label-textfield/float-label-text-field'
import * as metric from "../../theme"
import { useConfigurate } from "../../custom-hooks/use-configure-controller"
import { GradientButton } from "../../components/gradient-button/button"
import { textPresets } from "../../components/button/button.presets"

// MARK: Style Import

import * as Styles from "./login.styles"


export const LoginScreen: React.FunctionComponent<LoginProps> = observer((props) => {

    const controller = useConfigurate(LoginController, props) as LoginController

    const passwordTextInput = useRef(null)

    // MARK: Render
    return (
        <Wallpaper showBubble>
            <SafeAreaView shouldRasterizeIOS style={Styles.SAFE_AREA_VIEW}>
                <KeyboardAvoidingView enabled style={Styles.AVOID_KEYBOARD_VIEW} behavior={metric.keyboardBehavior} >
                    <AnimatedScrollView
                        contentContainerStyle={Styles.SCROLL_VIEW}
                        {...metric.solidScrollView} >
                        <AppNameSlogan style={Styles.APPNAME_SLOGAN_VIEW} />
                        <View shouldRasterizeIOS style={Styles.CONTENT_VIEW}>
                            <FloatLabelTextInput
                                {...LoginController.resourcesViewModel?.getTestIDEmailPlaceholder()}
                                // placeholder={LoginController.resourcesViewModel?.getResourceEmailPlaceholder()}
                                placeholder='kt.manaosoftware@gmail.com'

                                onChangeTextValue={controller.onEmailChangeText}
                                noBorder
                                returnKeyType="next"
                                keyboardType="email-address"
                                onSubmitEditing={() => { passwordTextInput.current.focus() }}
                                errorMessage={LoginController.viewModel?.getEmailErrorMessage}
                                testIDErrorMessage={LoginController.resourcesViewModel?.getTestIDErrorMessage()}
                                onBlur={controller.onBlurEmailTextField}
                            />

                            <FloatLabelTextInput
                                ref={passwordTextInput}
                                {...LoginController.resourcesViewModel?.getTestIDPasswordPlaceholder()}
                                {...metric.secureTextField}
                                returnKeyType="send"
                                onSubmitEditing={() => { controller.onSubmitButtonPressed() }}
                                // placeholder={LoginController.resourcesViewModel?.getResourcePasswordPlaceholder()}
                                placeholder='Manao100%'
                                onChangeTextValue={controller.onPasswordChangeText}
                                errorMessage={LoginController.viewModel?.getPasswordErrorMessage}
                                testIDErrorMessage={LoginController.resourcesViewModel?.getTestIDErrorMessage()}
                                onBlur={controller.onBlurPasswordField}
                                noBorder
                            />

                            <GradientButton
                                {...LoginController.resourcesViewModel?.getTestIDButtonLoginTitle()}
                                text={LoginController.resourcesViewModel?.getResourceButtonLoginTitle()}
                                containerStyle={Styles.BUTTON_MARGIN}
                                onPress={controller.onSubmitButtonPressed} />
                            <Button
                                isAnimated={false}
                                preset="none"
                                text={LoginController.resourcesViewModel?.getResourceForgotPasswordTitle()}
                                textStyle={textPresets.regularMedium} style={Styles.FORGOT_PW_BUTTON}
                                {...LoginController.resourcesViewModel?.getTestIDForgotPasswordTitle()}
                                onPress={controller.goToForgotPassword} />
                        </View>
                    </AnimatedScrollView>
                </KeyboardAvoidingView>
                    <Button
                        style={Styles.BOTTOM_VIEW}
                        isAnimated={false}
                        preset="none"
                        text={LoginController.resourcesViewModel?.getResourceACreateNewAccountTitle()}
                        {...LoginController.resourcesViewModel?.getTestIDCreateNewAccountTitle()}
                        textStyle={textPresets.regularNormal}
                        onPress={controller.goToWelcomeScreen}
                    />
            </SafeAreaView>
        </Wallpaper>
    )
})
