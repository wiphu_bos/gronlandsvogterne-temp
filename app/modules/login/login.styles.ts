import { ViewStyle } from "react-native"
import { color } from "../../theme"
import * as metric from "../../theme"

export const FULL: ViewStyle = {
    flex: 1,
}

export const SAFE_AREA_VIEW: ViewStyle = {
    ...FULL,
}

export const APPNAME_SLOGAN_VIEW: ViewStyle = {
    marginTop: metric.ratioHeight(140)
}

export const AVOID_KEYBOARD_VIEW: ViewStyle = {
    ...FULL
}

export const SCROLL_VIEW: ViewStyle = {
    paddingBottom: metric.ratioHeight(24),
    alignItems: "center"
}

export const CONTENT_VIEW: ViewStyle = {
    ...FULL,
    marginTop: metric.ratioHeight(131)
}

export const BUTTON_MARGIN: ViewStyle = {
    marginTop: metric.ratioHeight(18),
}

export const FORGOT_PW_BUTTON: ViewStyle = {
    marginTop: metric.ratioHeight(36),
    alignItems: 'center'
}

export const BOTTOM_VIEW: ViewStyle = {
    alignItems: 'center',
    height: metric.ratioHeight(47),
    justifyContent: 'flex-start',
    marginHorizontal: metric.ratioWidth(45)
}

export const BACKGROUND_STYLE = color.pinkBlueGradient