import { types } from "mobx-state-tree"
import { LoginPropsModel } from "./login.models"
import { GeneralResources } from "../../../constants/firebase/remote-config"
import { NavigationKey } from "../../../constants/app.constant"
import * as Utils from "../../../utils"
import { TestIDResources } from "../../../constants/test-key/test.constant"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store"

export const LoginViews = types.model(LoginPropsModel).views(self => ({
    get getExample() {
        return
    }
}))

export const LoginResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State

    const { LoginScreen } = GeneralResources
    const {
        emailPlaceholder,
        passwordPlaceholder,
        buttonLoginTitle,
        forgotPasswordTitle,
        createNewAccountTitle,
        keyboardButtonNextTitle,
        keyboardButtonSubmitTitle } = LoginScreen
    const { Login } = NavigationKey

    //MARK: Views

    const getResources = (key: string, childKeyOrShareKey: string | boolean = false) => self.getValues(childKeyOrShareKey ? key : Login, childKeyOrShareKey ? true : key)
    const getResourceEmailPlaceholder = () => getResources(emailPlaceholder)
    const getResourcePasswordPlaceholder = () => getResources(passwordPlaceholder)
    const getResourceButtonLoginTitle = () => getResources(buttonLoginTitle)
    const getResourceForgotPasswordTitle = () => getResources(forgotPasswordTitle)
    const getResourceACreateNewAccountTitle = () => getResources(createNewAccountTitle)
    const getResourceKeyboardButtonNextTitle = () => getResources(keyboardButtonNextTitle, true)
    const getResourceKeyboardButtonSubmitTitle = () => getResources(keyboardButtonSubmitTitle, true)


    return {
        getResourceEmailPlaceholder,
        getResourceButtonLoginTitle,
        getResourceForgotPasswordTitle,
        getResourcePasswordPlaceholder,
        getResourceACreateNewAccountTitle,
        getResourceKeyboardButtonNextTitle,
        getResourceKeyboardButtonSubmitTitle
    }
})

    .views(self => {
        //MARK: Volatile State

        //MARK: Views

        const getTestIDTitle = () => Utils.getTestIDObject(TestIDResources.LoginScreen.title)
        const getTestIDSubTitle = () => Utils.getTestIDObject(TestIDResources.LoginScreen.subTitle)
        const getTestIDEmailPlaceholder = () => Utils.getTestIDObject(TestIDResources.LoginScreen.emailPlaceholder)
        const getTestIDPasswordPlaceholder = () => Utils.getTestIDObject(TestIDResources.LoginScreen.passwordPlaceholder)
        const getTestIDButtonLoginTitle = () => Utils.getTestIDObject(TestIDResources.LoginScreen.buttonLogin)
        const getTestIDForgotPasswordTitle = () => Utils.getTestIDObject(TestIDResources.LoginScreen.forgotPassword)
        const getTestIDCreateNewAccountTitle = () => Utils.getTestIDObject(TestIDResources.LoginScreen.createNewAccount)
        const getTestIDErrorMessage = () => Utils.getTestIDObject(TestIDResources.LoginScreen.errorMessage)
        return {
            getTestIDTitle,
            getTestIDSubTitle,
            getTestIDEmailPlaceholder,
            getTestIDPasswordPlaceholder,
            getTestIDButtonLoginTitle,
            getTestIDForgotPasswordTitle,
            getTestIDCreateNewAccountTitle,
            getTestIDErrorMessage
        }
    })