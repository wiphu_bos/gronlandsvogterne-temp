import { types } from "mobx-state-tree"
import { WaitingDudePublishProfilePropsModel } from "./waiting-dude-approve-publish-profile.models"
import { GeneralResources } from "../../../constants/firebase/remote-config"
import { NavigationKey } from "../../../constants/app.constant"
import * as Utils from "../../../utils"
import { TestIDResources } from "../../../constants/test-key/test.constant"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store"

export const WaitingDudePublishProfileViews = types.model(WaitingDudePublishProfilePropsModel)
    .views(self => ({
        get getEx() {
            return
        }
    }))


export const WaitingDudePublishProfileResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State

    const { WaitingDudePublishProfileScreen } = GeneralResources
    const { title,
        description,
        buttonGotoManageProfile
    } = WaitingDudePublishProfileScreen
    const { WaitingDudePublishProfile } = NavigationKey

    //MARK: Views
    const getResources = (key: string, childKeyOrShareKey: string | boolean = false) => self.getValues(childKeyOrShareKey ? key : WaitingDudePublishProfile, childKeyOrShareKey ? true : key)
    const getShareResources = (key: string) => self.getValues(key, true)
    const getResourceTitle = () => getResources(title)
    const getResourceDescription = () => getResources(description)
    const getResourceManageProfile = () => getShareResources(buttonGotoManageProfile)

    return {
        getResourceTitle,
        getResourceDescription,
        getResourceManageProfile
    }
})
    .views(self => {

        //MARK: Volatile State

        //MARK: Views

        const getTestIDTitle = () => Utils.getTestIDObject(TestIDResources.WaitingDudeApproveScreen.title)
        const getTestIDDescription = () => Utils.getTestIDObject(TestIDResources.WaitingDudeApproveScreen.description)
        const getTestIDGotoManageProfile = () => Utils.getTestIDObject(TestIDResources.WaitingDudeApproveScreen.gotoManageProfile)

        return {
            getTestIDTitle,
            getTestIDDescription,
            getTestIDGotoManageProfile
        }
    })
