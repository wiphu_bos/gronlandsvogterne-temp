// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { View, KeyboardAvoidingView } from "react-native"
import * as metric from "../../theme"
import { SafeAreaView } from "react-navigation"
import { HeaderComponent } from "../../components/custom-header/custom-header"
import { Wallpaper, Text, AnimatedScrollView } from "../../components"
import { useConfigurate } from "../../custom-hooks/use-configure-controller"
import { images } from "../../theme/images"
import { INavigationRoute } from "../../models"
import FastImage from "react-native-fast-image"
import { GradientButton } from "../../components/gradient-button/button"
import { WaitingDudePublishProfileProps } from "./waiting-dude-approve-publish-profile.props"
import WaitingDudePublishProfileController from "./waiting-dude-approve-publish-profile.controllers"

// MARK: Style Import

import * as Styles from "./waiting-dude-approve-publish-profile.styles"


export const WaitingDudePublishProfileScreen: React.FunctionComponent<WaitingDudePublishProfileProps & Partial<INavigationRoute>> = observer((props) => {

    const controller = useConfigurate(WaitingDudePublishProfileController, props) as WaitingDudePublishProfileController

    // MARK: Render

    return (
        <Wallpaper showBubble>
            <SafeAreaView shouldRasterizeIOS style={Styles.SAFE_AREA_VIEW}>
                <HeaderComponent onBackButtonPress={controller.backProcess} />
                <KeyboardAvoidingView behavior={metric.keyboardBehavior} style={Styles.AVOID_KEYBOARD_VIEW}>
                    <AnimatedScrollView
                        contentContainerStyle={Styles.SCROLL_VIEW}
                        {...metric.solidScrollView} >
                        <View shouldRasterizeIOS style={Styles.TEXT_CONTAINER}>
                            <FastImage source={images.waitDudeApproveProfilePublishTitle} style={Styles.IMAGE_TITLE} />
                            <Text style={Styles.TITLE}
                                {...WaitingDudePublishProfileController.resourcesViewModel?.getTestIDTitle()} text={WaitingDudePublishProfileController.resourcesViewModel?.getResourceTitle()} />
                        </View>
                        <Text style={Styles.DESCRIPTION}
                            {...WaitingDudePublishProfileController.resourcesViewModel?.getTestIDDescription()} text={WaitingDudePublishProfileController.resourcesViewModel?.getResourceDescription()} />
                        <GradientButton
                            /*{localViewModel?.getIsValid ? "shadow" : "disabled"}*/
                            text={WaitingDudePublishProfileController.resourcesViewModel?.getResourceManageProfile()}
                            {...WaitingDudePublishProfileController.resourcesViewModel?.getTestIDGotoManageProfile()}
                            style={Styles.BUTTON_MARGIN} onPress={controller.onSubmitButtonDidTouch} />
                    </AnimatedScrollView>
                </KeyboardAvoidingView>
            </SafeAreaView>
        </Wallpaper>
    )
})
