// MARK: Import

import { NavigationKey } from "../../constants/app.constant"
import { RootStore, INavigationRoute } from "../../models"
import { onSnapshot } from "mobx-state-tree"
import { WaitingDudePublishProfileProps } from "./waiting-dude-approve-publish-profile.props"
import { WaitingDudePublishProfileStore, WaitingDudePublishProfileResourcesStore } from "./storemodels/waiting-dude-approve-publish-profile.types"
import { WaitingDudePublishProfileStoreModel, WaitingDudePublishProfileResourcesStoreModel } from "./storemodels/waiting-dude-approve-publish-profile.store"

import { StackActions } from "@react-navigation/native"
import { DrawerActions } from "@react-navigation/native"
import { GeneralResources } from "../../constants/firebase/remote-config/remote-config.constant"
import { BaseController } from "../base.controller"


class WaitingDudePublishProfileController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static viewModel: WaitingDudePublishProfileStore
    public static resourcesViewModel: WaitingDudePublishProfileResourcesStore
    public static myProps: WaitingDudePublishProfileProps & Partial<INavigationRoute>

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, myProps?: WaitingDudePublishProfileProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, myProps, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(myProps)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        WaitingDudePublishProfileController.resourcesViewModel = WaitingDudePublishProfileResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.WaitingDudePublishProfile)
        WaitingDudePublishProfileController.viewModel = localStore && WaitingDudePublishProfileStoreModel.create({ ...localStore }) || WaitingDudePublishProfileStoreModel.create({})
    }

    private _setupSnapShot = () => {
        onSnapshot(WaitingDudePublishProfileController.viewModel, (snap: WaitingDudePublishProfileStore) => {
            this._setInitializedToPropsParams()
        })
    }

    /*
       Mark Data
   */


    /*
       Mark Event
   */

    public onSubmitButtonDidTouch = () => {
        const { navigation } = this._myProps
        Promise.all([(navigation as any)?.dispatch(StackActions.popToTop()), (navigation as any)?.dispatch(DrawerActions.jumpTo(NavigationKey.ManageProfileTab, {
            screen: this._rootStore?.getGeneralResourcesStore(NavigationKey.ManageProfileTab, GeneralResources.ManageProfileTabScreen.myDudeProfileTabMenuTitle),
        }) as any)])
    }

    //@override
    backProcess = () => {
        const { navigation } = this._myProps
        navigation?.dispatch(StackActions.popToTop() as any)
    }

    /*
       Mark Life cycle
   */

    /*
       Mark Helper
   */
}

export default WaitingDudePublishProfileController