import { Instance, SnapshotOut } from "mobx-state-tree"
import { WaitingDudePublishProfileResourcesStoreModel, WaitingDudePublishProfileStoreModel } from "./waiting-dude-approve-publish-profile.store"

export type WaitingDudePublishProfileStore = Instance<typeof WaitingDudePublishProfileStoreModel>
export type WaitingDudePublishProfileStoreSnapshot = SnapshotOut<typeof WaitingDudePublishProfileStoreModel>

export type WaitingDudePublishProfileResourcesStore = Instance<typeof WaitingDudePublishProfileResourcesStoreModel>
export type WaitingDudePublishProfileResourcesStoreSnapshot = SnapshotOut<typeof WaitingDudePublishProfileResourcesStoreModel>
