import { types } from "mobx-state-tree"
import { StoreName } from "../../../constants/app.constant"
import { WaitingDudePublishProfileServiceActions } from "../services/waiting-dude-approve-publish-profile.services"
import { WaitingDudePublishProfilePropsModel } from "../viewmodels/waiting-dude-approve-publish-profile.models"
import { WaitingDudePublishProfileActions } from "../viewmodels/waiting-dude-approve-publish-profile.actions"
import { WaitingDudePublishProfileViews, WaitingDudePublishProfileResourcesViews } from "../viewmodels/waiting-dude-approve-publish-profile.views"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store"


const WaitingDudePublishProfileModel = types.model(StoreName.WaitingDudePublishProfile, WaitingDudePublishProfilePropsModel)

export const WaitingDudePublishProfileStoreModel = types.compose(
    WaitingDudePublishProfileModel,
    WaitingDudePublishProfileViews,
    WaitingDudePublishProfileActions,
    WaitingDudePublishProfileServiceActions)
export const WaitingDudePublishProfileResourcesStoreModel = types.compose(GeneralResourcesStoreModel, WaitingDudePublishProfileResourcesViews)