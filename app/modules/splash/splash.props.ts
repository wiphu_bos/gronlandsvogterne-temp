import { NavigationState, NavigationProp } from "@react-navigation/native"

export interface SplashProps extends NavigationProp<Record<string, object>, string, NavigationState, {}, {}> {
}