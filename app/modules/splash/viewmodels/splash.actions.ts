import { types } from "mobx-state-tree"
import { SplashPropsModel } from "./splash.models"
export const SplashActions = types.model(SplashPropsModel).actions(self => {
    const setContentObject = (value: any) => self.contentObject = value
    const setContent = (value: string) => self.content = value
    const setIsLoading = (value: boolean) => self.isLoading = value
    const setIsRefreshing = (value: boolean) => self.isRefreshing = value
    return {
        setContent,
        setContentObject,
        setIsLoading,
        setIsRefreshing
    }
})