import { types } from "mobx-state-tree"

export const ContentObejct = types.model({
    title: types.maybeNull(types.string),
    detail: types.maybeNull(types.string)
})

export const SplashPropsModel = {
    content: types.maybeNull(types.string),
    contentObject: types.maybeNull(ContentObejct),
    isLoading: types.optional(types.boolean, true),
    isRefreshing: types.optional(types.boolean, false)
} 