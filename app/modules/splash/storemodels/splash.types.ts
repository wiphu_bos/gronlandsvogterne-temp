import { Instance, SnapshotOut } from "mobx-state-tree"
import { SplashStoreModel } from "./splash.store"

export type SplashStore = Instance<typeof SplashStoreModel>
export type SplashStoreSnapshot = SnapshotOut<typeof SplashStoreModel>