import { ViewStyle, TextStyle, ImageStyle } from "react-native"
import { color, spacing } from "../../theme"


export const FULL: ViewStyle = { flex: 1 }
export const CONTAINER: ViewStyle = {
    backgroundColor: color.mainBackground
}

export const BackgroundStyle = color.grayPinkGradient

export const APPLOGO: ImageStyle = {
    width: '100%',
    height: 124,
}
export const APPLOGO_CONTAINER: ViewStyle = {
    flex: 1,
    justifyContent: "center",
    marginBottom: 14
}

export const TEXT: TextStyle = {
    color: color.palette.white,
    fontFamily: "Prompt",
}
export const BOLD: TextStyle = { fontWeight: "bold" }

export const TITLE_WRAPPER: TextStyle = {
    ...TEXT,
    textAlign: "center",
}
export const TITLE: TextStyle = {
    ...TEXT,
    ...BOLD,
    fontSize: 28,
    lineHeight: 38,
    textAlign: "center",
}

export const DETAIL: TextStyle = {
    ...TEXT,
    color: "#BAB6C8",
    fontSize: 15,
    lineHeight: 22,
    marginBottom: spacing[5],
}