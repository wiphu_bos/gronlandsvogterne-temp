// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import SplashController from "./splash.controllers"
import SplashScreen from 'react-native-splash-screen'
import LinearGradient from 'react-native-linear-gradient'
import { View, StatusBar } from "react-native"
import { images } from "../../theme/images"
import { useConfigurate } from "../../custom-hooks/use-configure-controller"
import FastImage from "react-native-fast-image"

// MARK: Style Import
import * as Styles from "./splash.styles"

export const Splashscreen: React.FunctionComponent<{}> = observer(() => {
    SplashScreen.hide()

    useConfigurate(SplashController) as SplashController

    return (
        <LinearGradient shouldRasterizeIOS style={Styles.FULL} colors={Styles.BackgroundStyle}>
            <StatusBar hidden animated />
            <View shouldRasterizeIOS style={Styles.APPLOGO_CONTAINER} >
                <FastImage style={Styles.APPLOGO} source={images.splashLogo} resizeMode={FastImage.resizeMode.contain} />
            </View>
        </LinearGradient>
    )
})
