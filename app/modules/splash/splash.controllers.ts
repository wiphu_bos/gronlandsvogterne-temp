// MARK: Import
import { RootStore, GeneralResourcesStore } from "../../models"
import { onSnapshot } from "mobx-state-tree"
import { BaseController } from "../base.controller"
import { FirebaseUserInfo } from "../../utils/firebase/authentication/auth.types"

// MARK: Vaiable Declaration

class SplashController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static viewModel: GeneralResourcesStore

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore) {
        super(rootStore)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
    }
    private _setupViewModel = () => {
        SplashController.viewModel = this._rootStore?.getAllGeneralResourcesStore
    }

    private _setupSnapShot = () => {
        onSnapshot(SplashController.viewModel, (snap: GeneralResourcesStore) => {
            this._setInitializedToPropsParams()
        })
    }

    /*
       Mark Data
   */
    private _getLocalizedString = async () => {
        await SplashController.viewModel?.fetchLocalizedGeneralResources()
    }

    private _getGeneralResources = async () => {
        await SplashController.viewModel?.fetchRemoteConfig()
    }

    private _initSharedInstanceProps = () => {
        SplashController.viewModel?.initSharedInstanceProps(this._rootStore)
    }

    private _fetchCurrentUser = () => {
        const user: FirebaseUserInfo = SplashController.viewModel?.fetchCurrentUser()
        if (user && user?.emailVerified === true) {
            this._rootStore?.getAuthStore?.setFirebaseUserInfo(user)
            this._rootStore?.getAuthStore?.setIsLoggedIn(true)
            this._rootStore?.getUserStore?.setIsVerified(true)
            this._rootStore?.getUserStore?.setUserId(user?.uid)
        } else {
            this._rootStore?.getUserStore?.forceLogoutWithoutAuth(this._rootStore)
        }
    }

    // MARK: Event handling

    //@override
    backProcess = () => {

    }

    /*
       Mark Life cycle
    */

    //@override
    viewDidAppearOnce = async () => {
        await this._getLocalizedString()
        await this._getGeneralResources()
        this._initSharedInstanceProps()
        this._fetchCurrentUser()
        this._rootStore?.setIsFetchingResources(false)
    }
}

export default SplashController
