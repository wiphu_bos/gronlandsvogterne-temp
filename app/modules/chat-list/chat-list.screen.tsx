// MARK: Import

import React from "react"
import { observer } from "mobx-react-lite"
import { ChatListProps } from "./chat-list.props"
import { View, RefreshControl } from "react-native"
import { FlatList } from "react-native-gesture-handler"
import { Header, Text, Wallpaper, ConfirmModal } from "../../components"
import { useConfigurate } from "../../custom-hooks/use-configure-controller"
import ChatListController from "./chat-list.controllers"
import { ChatListCardComponents } from "../../components/chat-list-card/chat-list-card"
import { SafeAreaView } from "react-navigation"
import * as metric from "../../theme"
import { useActionSheet, ActionSheetProvider } from "@expo/react-native-action-sheet"

// MARK: Style Import

import * as Styles from "./chat-list.styles"


const ChatListComponents: React.FunctionComponent<ChatListProps> = observer((props) => {
    const controller = useConfigurate(ChatListController, props) as ChatListController
    const { showActionSheetWithOptions } = useActionSheet()

    return (
        <SafeAreaView
            {...metric.safeAreaViewProps}
            shouldRasterizeIOS
            style={Styles.SAFE_AREA_VIEW}>
            <Header {...Styles.HEADER_PROPS} {...controller.getHeaderLeftOptions} />
            <View shouldRasterizeIOS style={Styles.HEADER_CONTAINER}>
                <Text text={ChatListController.resourcesViewModel?.getResourceScreenTitle()}
                    style={Styles.TITLE}
                    {...ChatListController.resourcesViewModel?.getTestIDTitle()} />
            </View>
            <View shouldRasterizeIOS style={Styles.BODY_CONTAINER}>
                <FlatList
                    contentContainerStyle={Styles.LIST_VIEW}
                    refreshControl={
                        <RefreshControl
                            style={Styles.REFRESH_CONTROL}
                            refreshing={ChatListController.viewModel?.getIsPullRefreshing}
                            onRefresh={controller.onRefresh}
                        />
                    }
                    showsVerticalScrollIndicator={false}
                    ItemSeparatorComponent={({ highlighted }) => (
                        <View shouldRasterizeIOS style={Styles.SEPARATOR_VIEW} />
                    )}
                    extraData={ChatListController.viewModel?.getChatList?.slice()}
                    data={ChatListController.viewModel?.getChatList}
                    renderItem={({ item, index, separators }) => (
                        <ChatListCardComponents
                            onLongPress={() => controller.showActionSheetWithOptions(showActionSheetWithOptions, item)}
                            animation={controller.flatlistResultAnimationConfig}
                            fullnameTestID={ChatListController.resourcesViewModel?.getTestIDFullNameTitle()}
                            imageProfileTestID={ChatListController.resourcesViewModel?.getTestIDImageProfile()}
                            timeAgoTestID={ChatListController.resourcesViewModel?.getTestIDTimeAgo()}
                            chat$={item}
                            navigation={props?.navigation}
                            onPress={(item) => controller.onChatCardPressed(item)} />
                    )}
                    onEndReachedThreshold={0.4}
                    onEndReached={controller.onLoadMore}
                />
            </View>
            <ConfirmModal
                {...ChatListController.resourcesViewModel?.getTestIDConfirmModal()}
                okButtonText={ChatListController.resourcesViewModel?.getResourceModalButtonOKTitle()}
                clearButtonText={ChatListController.resourcesViewModel?.getResourceButtonCancelTitle()}
                message={ChatListController.resourcesViewModel?.getResourceModalConfirmDeleteChatThread()}
                isVisible={ChatListController.viewModel?.getIsModalShowing || false}
                onCancel={controller.onCancelModal}
                onModalClosed={controller.onModalClosed}
                onOK={controller.onDeleteChatThread} />
        </SafeAreaView>
    )
})


export const ChatListScreen: React.FunctionComponent<ChatListProps> = observer((props) => {
    return (
        <Wallpaper showBubble>
            <ActionSheetProvider>
                <ChatListComponents {...props} />
            </ActionSheetProvider>
        </Wallpaper>
    )
})
