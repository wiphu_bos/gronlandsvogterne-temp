import { ParamListBase } from '@react-navigation/native'
import { NavigationContainerProps } from "react-navigation"

export interface ChatListProps extends NavigationContainerProps<ParamListBase> {
}