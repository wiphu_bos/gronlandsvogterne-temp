import { Instance, SnapshotOut } from "mobx-state-tree"
import { ChatListStoreModel, ChatListResourcesStoreModel } from "./chat-list.store"

export type ChatListStore = Instance<typeof ChatListStoreModel>
export type ChatListStoreSnapshot = SnapshotOut<typeof ChatListStoreModel>

export type ChatListResourcesStore = Instance<typeof ChatListResourcesStoreModel>
export type ChatListResourcesStoreSnapshot = SnapshotOut<typeof ChatListResourcesStoreModel>
