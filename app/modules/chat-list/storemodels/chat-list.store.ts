import { types } from "mobx-state-tree"
import { StoreName } from "../../../constants/app.constant"
import { ChatListPropsModel } from "../viewmodels/chat-list.models"
import { ChatListActions } from "../viewmodels/chat-list.actions"
import { ChatListViews, ChatListResourcesViews } from "../viewmodels/chat-list.views"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store"
import { ChatListServiceActions } from "../services/chat-list.services"

const ChatListModel = types.model(StoreName.ChatList, ChatListPropsModel)
export const ChatListStoreModel = types.compose(ChatListModel, ChatListViews, ChatListActions, ChatListServiceActions)
export const ChatListResourcesStoreModel = types.compose(GeneralResourcesStoreModel, ChatListResourcesViews)

