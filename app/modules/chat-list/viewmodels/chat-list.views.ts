import { types } from "mobx-state-tree"
import { ChatListPropsModel } from "./chat-list.models"
import { TestIDResources } from "../../../constants/test-key/test.constant"
import * as Utils from "../../../utils"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store"
import { GeneralResources } from "../../../constants/firebase/remote-config/remote-config.constant"
import { NavigationKey } from "../../../constants/app.constant"

export const ChatListViews = types.model(ChatListPropsModel)
    .views(self => ({
        get getChatList() {
            return self.chatList$
        },
        get getLastId(): string {
            return self.last_id
        },
        get getIsPullRefreshing(): boolean {
            return self.isPullRefreshing
        },
        get getIsLoadMore(): boolean {
            return self.isLoadMore
        },
        get getIsFirstLoadDone(): boolean {
            return self.isFirstLoadDone
        },
        get getIsLoadDone(): boolean {
            return self.isLoadDone
        },
        get getShouldLoadmore(): boolean {
            return !self.isLoadDone && !self.isLoadMore && self.chatList$?.length > 0 && self.isFirstLoadDone
        },
        get getIsModalShowing(): boolean {
            return self.isModalShowing
        }
    }))

export const ChatListResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State

    const { ChatListScreen } = GeneralResources
    const { title,
        buttonCancelTitle,
        modalButtonOKTitle,
        buttonDeleteTitle,
        modalConfirmDeleteChatThread } = ChatListScreen
    const { ChatList } = NavigationKey
    //MARK: Views
    const getResources = (key: string) => self.getValues(ChatList, key)
    const getsharedResources = (key: string) => self.getValues(key, true)
    const getResourceScreenTitle = () => getResources(title)
    const getResourceButtonCancelTitle = () => getsharedResources(buttonCancelTitle)
    const getResourceModalButtonOKTitle = () => getsharedResources(modalButtonOKTitle)
    const getResourceButtonDeleteTitle = () => getsharedResources(buttonDeleteTitle)
    const getResourceModalConfirmDeleteChatThread = () => getsharedResources(modalConfirmDeleteChatThread)
    return {
        getResourceScreenTitle,
        getResourceButtonCancelTitle,
        getResourceModalButtonOKTitle,
        getResourceButtonDeleteTitle,
        getResourceModalConfirmDeleteChatThread
    }
})
    .views(self => {
        //MARK: Volatile State

        //MARK: Views
        const getTestIDConfirmModal = () => Utils.getTestIDObject(TestIDResources.ChatListScreen.confirmModal)
        const getTestIDTitle = () => Utils.getTestIDObject(TestIDResources.ChatListScreen.title)
        const getTestIDImageProfile = () => Utils.getTestIDObject(TestIDResources.ChatListScreen.title)
        const getTestIDFullNameTitle = () => Utils.getTestIDObject(TestIDResources.ChatListScreen.fullName)
        const getTestIDLastTextMessage = () => Utils.getTestIDObject(TestIDResources.ChatListScreen.lastTextMessage)
        const getTestIDLastTextMessageTime = () => Utils.getTestIDObject(TestIDResources.ChatListScreen.lastTextMessageTime)
        const getTestIDTimeAgo = () => Utils.getTestIDObject(TestIDResources.ChatListScreen.timeAgo)
        return {
            getTestIDConfirmModal,
            getTestIDTitle,
            getTestIDImageProfile,
            getTestIDFullNameTitle,
            getTestIDLastTextMessage,
            getTestIDLastTextMessageTime,
            getTestIDTimeAgo
        }
    })
