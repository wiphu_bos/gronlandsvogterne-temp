import { types, unprotect } from "mobx-state-tree"
import { ChatListPropsModel } from "./chat-list.models"
import { IChat, ChatStoreModel } from "../../../models/chat-store"
import { RootStore } from "../../../models"

export const ChatListActions = types.model(ChatListPropsModel).actions(self => {

    const findExistingItemIndex = (chat: IChat): number => {
        const tempChatList = self.chatList$
        const existingList = tempChatList?.slice() //Slice to disobserve property
        const foundItemIndex = existingList?.findIndex(v => v.chat_room_id === chat?.chat_room_id)
        return foundItemIndex
    }

    const setChatList = (rootStore: RootStore, list: IChat[]) => {
        list.forEach(e => {
            const childViewModel = ChatStoreModel.create(e)
            childViewModel.setIsTimerSet(rootStore, true)
            unprotect(childViewModel)
            self.chatList$.push(childViewModel)
        })
        self.isPullRefreshing = false
        self.isLoadMore = false
    }
    const addChatIntoList = (rootStore: RootStore, chat: IChat) => {
        const foundItemIndex = findExistingItemIndex(chat)
        if (foundItemIndex !== -1) return
        const tempChatList = self.chatList$
        const childViewModel = ChatStoreModel.create(chat)
        childViewModel.setIsTimerSet(rootStore, true) //Must be called before bindingData
        childViewModel.bindingDataFromObject(chat)
        unprotect(childViewModel)
        self.chatList$ = [].concat.apply(childViewModel, tempChatList)
    }
    const updateChatInList = (rootStore: RootStore, chat: IChat) => {
        const foundItemIndex = findExistingItemIndex(chat)
        if (foundItemIndex !== -1) removeChatInList(chat)
        addChatIntoList(rootStore, chat)
    }
    const removeChatInList = (chat: IChat) => {
        const foundItemIndex = findExistingItemIndex(chat)
        if (foundItemIndex === -1) return
        self.chatList$[foundItemIndex].setIsTimerSet(null, false)
        self.chatList$.splice(foundItemIndex, 1)
    }

    const invalidateAllTimerIfNeeded = () => {
        self.chatList$.forEach(e => {
            e.setIsTimerSet(null, false)
        })
    }

    const setIsPullRefreshing = (value: boolean) => self.isPullRefreshing = value
    const setIsLoadMore = (value: boolean) => self.isLoadMore = value
    const setIsFirstLoadDone = (value: boolean) => self.isFirstLoadDone = value
    const setIsLoadDone = (value: boolean) => self.isLoadDone = value
    const setIsModalShowing = (value: boolean) => self.isModalShowing = value
    const resetFetching = () => {
        invalidateAllTimerIfNeeded()
        self.chatList$.clear()
        self.isFirstLoadDone = false
        self.isLoadDone = false
        self.isLoadMore = false
    }
    return {
        addChatIntoList,
        updateChatInList,
        removeChatInList,
        setIsPullRefreshing,
        resetFetching,
        setIsLoadMore,
        setChatList,
        setIsLoadDone,
        setIsFirstLoadDone,
        invalidateAllTimerIfNeeded,
        setIsModalShowing
    }
})