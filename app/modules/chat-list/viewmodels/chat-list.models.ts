import { types } from "mobx-state-tree"
import { ChatStoreModel } from "../../../models/chat-store/chat-store.store"

export const ChatListPropsModel = {
    chatList$: types.optional(types.array(ChatStoreModel), []),
    last_id: types.maybeNull(types.string),
    isPullRefreshing: types.optional(types.boolean, false),
    isFirstLoadDone: types.optional(types.boolean, false),
    isLoadDone: types.optional(types.boolean, false),
    isLoadMore: types.optional(types.boolean, false),
    isModalShowing: types.optional(types.boolean, false)
} 