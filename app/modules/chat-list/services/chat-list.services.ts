
import { ChatListPropsModel } from "../viewmodels"
import { types, flow, unprotect } from "mobx-state-tree"
import { APIResponse, APISuccessResponse } from "../../../constants/service.types"
import { RootStore } from "../../../models"
import { IChat, ChatStoreModel, IChatListResponse } from "../../../models/chat-store"
import * as DataUtils from '../../../utils/data.utils'

export const ChatListServiceActions = types.model(ChatListPropsModel).actions(self => {

    const fetchChatList = flow(function* (rootStore: RootStore) {
        const last_id = self.isFirstLoadDone ? self.last_id : null
        try {
            self.isLoadDone = self.chatList$.length > 0
            const result: APIResponse = yield rootStore?.getChatStore.fetchChatList(last_id)
            const success = (result as APISuccessResponse)?.data
            const response: IChatListResponse = success?.data
            const chatList = response?.chat_list.map(e => DataUtils.getChatMessageObject(rootStore, e, false))
            const pagingObj = response?.page
            if (chatList?.length > 0) {
                if (!self.isLoadMore) {
                    self.chatList$.forEach(e => e.setIsTimerSet(null, false))
                    self.chatList$.clear()
                }
                chatList?.forEach(e => {
                    const childViewModel = ChatStoreModel.create(e)
                    childViewModel.setIsTimerSet(rootStore, true)
                    unprotect(childViewModel)
                    self.chatList$.push(childViewModel)
                })
            }
            self.last_id = pagingObj?.last_id
            self.isLoadDone = pagingObj?.is_last_page
            return result
        } catch (e) {
            return e
        } finally {
            self.isFirstLoadDone = true
            self.isPullRefreshing = false
            self.isLoadMore = false
        }
    })

    const markReadChatById = flow(function* (rootStore?: RootStore, chatRoomId?: string) {
        try {
            let result: APIResponse = yield rootStore?.getChatStore?.markReadChatById(chatRoomId)
            const success = (result as APISuccessResponse)?.data
            const chat: IChat = success?.data
            if (!chat) throw success
            return chat
        } catch (e) {
            return e
        }
    })

    const createChatRoomById = flow(function* (rootStore?: RootStore, uid?: string) {
        try {
            let result: APIResponse = yield rootStore?.getChatStore?.createChatRoomById(uid)
            const success = (result as APISuccessResponse)?.data
            const chat: IChat = success?.data
            if (!chat) throw success
            return chat
        } catch (e) {
            return e
        }
    })

    const deleteChatRoomById = flow(function* (rootStore?: RootStore, chatroomId?: string) {
        try {
            let result: APIResponse = yield rootStore?.getChatStore?.deleteChatRoomById(chatroomId)
            const success = (result as APISuccessResponse)?.data
            const chat: IChat = success?.data
            if (!chat) throw success
            return chat
        } catch (e) {
            return e
        }
    })

    return { fetchChatList, markReadChatById, createChatRoomById, deleteChatRoomById }
})