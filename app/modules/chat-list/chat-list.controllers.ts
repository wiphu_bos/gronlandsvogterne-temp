// MARK: Import
import { NavigationKey } from "../../constants/app.constant"
import { ChatListProps } from "./chat-list.props"
import { ChatListResourcesStoreModel, ChatListStoreModel } from "./storemodels/chat-list.store"
import { ChatListResourcesStore, ChatListStore } from "./storemodels/chat-list.types"
import { RootStore, INavigationRoute, ISharedTimer } from "../../models"
import { BaseController } from "../base.controller"
import { onSnapshot, unprotect } from "mobx-state-tree"
import { SnapshotChangeType, Property } from "../../constants/firebase/fire-store.constant"
import { FirebaseFirestoreTypes } from "@react-native-firebase/firestore"
import FirebaseFireStore from "../../utils/firebase/fire-store/fire-store"
import { IUserListener } from "../../utils/firebase/fire-store/fire-store.types"
import { ExtractCSTWithSTN } from "mobx-state-tree/dist/core/type/type"
import { IChat, ChatStoreModel } from "../../models/chat-store"
import { HeaderProps } from "../../components/header/header.props"
import { DrawerActions, StackActions } from "@react-navigation/native"
import * as DataUtils from "../../utils/data.utils"
import * as metric from "../../theme"
import { ActionSheetOptions } from "@expo/react-native-action-sheet"

class ChatListController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static viewModel: ChatListStore
    public static resourcesViewModel: ChatListResourcesStore
    private static _selectedItem: ExtractCSTWithSTN<typeof ChatStoreModel>
    private static _disposer: () => void
    private static _isVisited: boolean
    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, myProps?: ChatListProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, myProps, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(myProps)
        this._setupViewModel()
        this._setupSnapShot()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        ChatListController.resourcesViewModel = ChatListResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.ChatList)
        ChatListController.viewModel = localStore && ChatListStoreModel.create({ ...localStore }) || ChatListStoreModel.create({})
        unprotect(ChatListController.viewModel)
    }
    private _setupSnapShot = () => {
        onSnapshot(ChatListController.viewModel, (snap: ChatListStore) => {
            this._setInitializedToPropsParams()
        })
    }

    /*
       Mark Data
   */

    private _fetchChatList = async () => await ChatListController.viewModel?.fetchChatList(this._rootStore)

    public getHeaderLeftOptions: HeaderProps = {
        onLeftPress: () => this.onSideMenuPress()
    }

    public flatlistResultAnimationConfig = ChatListController._isVisited ? null : metric.flatlistResultAnimationConfig

    /*
       Mark Event
   */

    public onDeleteChatThread = () => {
        ChatListController.viewModel?.setIsModalShowing(false)
    }
    public onModalClosed = async () => {
        if (ChatListController._selectedItem) {
            const chatRoomId = ChatListController._selectedItem?.chat_room_id
            this._isGlobalLoading(true)
            await ChatListController.viewModel?.deleteChatRoomById(this._rootStore, chatRoomId)
            this._isGlobalLoading(false)
        }
        ChatListController._selectedItem = undefined
    }

    public onCancelModal = () => {
        ChatListController._selectedItem = undefined
        ChatListController.viewModel?.setIsModalShowing(false)
    }

    public onSideMenuPress = () => {
        this._myProps?.navigation?.dispatch(DrawerActions.openDrawer() as any)
    }
    public showActionSheetWithOptions = (
        config: (options: ActionSheetOptions, callback: (i: number) => void) => void,
        selectedItem: ExtractCSTWithSTN<typeof ChatStoreModel>) => {
        const buttonDeleteText = ChatListController.resourcesViewModel?.getResourceButtonDeleteTitle()
        const buttonCancelText = ChatListController.resourcesViewModel?.getResourceButtonCancelTitle()
        const options = [buttonDeleteText, buttonCancelText]
        const destructiveButtonIndex = 0
        const cancelButtonIndex = 2
        config({
            options,
            cancelButtonIndex,
            destructiveButtonIndex,
        }, buttonIndex => {
            switch (buttonIndex) {
                case destructiveButtonIndex:
                    ChatListController._selectedItem = selectedItem
                    ChatListController.viewModel?.setIsModalShowing(true)
                    break
                default:
                    ChatListController._selectedItem = undefined
                    break
            }
        })
    }

    private _onChatListListener = () => {

        const removeChatInList = (chat: any) => ChatListController.viewModel?.removeChatInList(chat)

        const onNext: (snapshot: FirebaseFirestoreTypes.QuerySnapshot) => void = (snap => {
            if (!ChatListController.viewModel?.getIsFirstLoadDone || snap.metadata.hasPendingWrites) return
            snap.docChanges().forEach(change => {
                const shouldConvertTime: boolean = true
                const extractChatUserFromFirebase: boolean = true
                const chat: IChat & ISharedTimer = DataUtils.getChatMessageObject(
                    this._rootStore,
                    change.doc.data(),
                    shouldConvertTime,
                    extractChatUserFromFirebase)

                if (change.type === SnapshotChangeType.ADDED) {
                    if (chat) ChatListController.viewModel?.addChatIntoList(this._rootStore, chat)
                } else if (change.type === SnapshotChangeType.MODIFIED) {
                    if (chat) {
                        const uid = this._rootStore?.getUserStore?.getUserId
                        const interlocutorIds: string[] = chat[Property.INTERLOCUTOR_IDS]
                        const isDeleted = !interlocutorIds?.includes(uid)
                        if (isDeleted) {
                            removeChatInList(chat)
                        } else {
                            ChatListController.viewModel?.updateChatInList(this._rootStore, chat)
                        }
                    }
                }
                else if (change.type === SnapshotChangeType.REMOVED) {
                    if (chat) removeChatInList(chat)
                }
            })
        })

        const onError: (e: Error) => void = (e => {
        })

        const onComplete: () => void = () => {
        }

        const params: IUserListener = {
            userId: this._rootStore?.getUserStore?.getUserId,
            onNext,
            onError,
            onComplete
        }

        ChatListController._disposer = FirebaseFireStore.onChatListListener(params)
    }

    public onRefresh = async () => {
        const isConnected = this._rootStore?.getSharedStore?.getIsConnected
        if (!isConnected) return
        this._isLoading(true)
        ChatListController.viewModel?.resetFetching()
        await this._fetchChatList()
        this._isLoading(false)
    }

    public onLoadMore = async () => {
        if (!ChatListController.viewModel?.getShouldLoadmore) return
        ChatListController.viewModel?.setIsLoadMore(true)
        await this._fetchChatList()
    }

    private _isLoading = (value: boolean) => ChatListController.viewModel?.setIsPullRefreshing(value)

    public onChatCardPressed = async (chatDetails$: ExtractCSTWithSTN<typeof ChatStoreModel>) => {
        //No need to transform to object with `DataUtils.getChatMessageObject()`, because it's already been object.
        const chatRoomObject = { ...chatDetails$ } as IChat //spread to IChat to dis-observe
        if (!chatRoomObject) return
        this._myProps?.navigation?.dispatch(StackActions.push(NavigationKey.ChatRoom, { chatRoomObject: chatRoomObject, shouldDelayFetching: false }) as any)
    }

    //@override
    backProcess = () => {
    }

    /*
       Mark Life cycle
    */

    //@override
    viewDidAppearOnce = async () => {
        this._onChatListListener()
    }

    //@override

    viewDidAppearAfterFocus = async () => {
        super.viewDidAppearAfterFocus && super.viewDidAppearAfterFocus()
        const isConnected = this._rootStore?.getSharedStore?.getIsConnected
        if (isConnected) {
            if (ChatListController.viewModel?.getChatList?.length === 0) {
                await this.onRefresh()
            } else if (!ChatListController.viewModel?.getIsFirstLoadDone) {
                await this._fetchChatList()
            }
        } else {
            ChatListController.viewModel?.setIsFirstLoadDone(true)
        }
        if (!ChatListController._isVisited) {
            ChatListController._isVisited = true
        }
    }

    //@override
    deInit = () => {
        super.deInit && super.deInit()
        ChatListController._disposer && ChatListController._disposer()
        ChatListController.viewModel?.invalidateAllTimerIfNeeded()
    }

    /*
       Mark Helper
   */
}

export default ChatListController