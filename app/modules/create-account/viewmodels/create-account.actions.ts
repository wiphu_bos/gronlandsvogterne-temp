import { CreateAccountPropsModel } from "./create-account.models"
import { types } from "mobx-state-tree"

export const CreateAccountActions = types.model(CreateAccountPropsModel).actions(self => {
    //MARK: Volatile State

    //MARK: Actions
    const setIsModalShowing = (value: boolean) => {
        self.isModalShowing = value
    }

    return {
        setIsModalShowing
    }
})