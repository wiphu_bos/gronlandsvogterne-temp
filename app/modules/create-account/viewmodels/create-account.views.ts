import { types } from "mobx-state-tree"
import { CreateAccountPropsModel } from "./create-account.models"
import { GeneralResources } from "../../../constants/firebase/remote-config"
import { NavigationKey } from "../../../constants/app.constant"
import * as Utils from "../../../utils"
import { TestIDResources } from "../../../constants/test-key/test.constant"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store"


export const CreateAccountViews = types.model(CreateAccountPropsModel).views(self => ({
    get getIsModalShowing(): boolean {
        return self.isModalShowing
    }
}))

export const CreateAccountResourcesViews = GeneralResourcesStoreModel.views(self => {

    //MARK: Volatile State

    const { CreateAccountScreen } = GeneralResources
    const { title,
        fullNamePlaceholder,
        emailPlaceholder,
        countryPlaceholder,
        agePlaceholder,
        passwordPlaceholder,
        passwordDescription,
        confirmPasswordPlaceholder,
        alreadyHaveAnAccountTitle,
        buttonNextTitle,
        modalAgePlaceholder,
        modalCountryPlaceholder,
        modalButtonCancelTitle,
        keyboardButtonSubmitTitle,
        keyboardButtonNextTitle
    } = CreateAccountScreen

    const { CreateAccount } = NavigationKey

    //MARK: Views
    const getResources = (key: string, childKeyOrShareKey: string | boolean = false) => self.getValues(childKeyOrShareKey ? key : CreateAccount, childKeyOrShareKey ? true : key)
    const getResourceTitle = () => getResources(title)
    const getResourceFullNamePlaceholder = () => getResources(fullNamePlaceholder)
    const getResourceEmailPlaceholder = () => getResources(emailPlaceholder)
    const getResourceCountryPlaceholder = () => getResources(countryPlaceholder)
    const getResourceAgePlaceholder = () => getResources(agePlaceholder)
    const getResourcePasswordPlaceholder = () => getResources(passwordPlaceholder)
    const getResourcePasswordDescription = () => getResources(passwordDescription)
    const getResourceConfirmPasswordPlaceholder = () => getResources(confirmPasswordPlaceholder)
    const getResourceAlreadyHaveAnAccountTitle = () => getResources(alreadyHaveAnAccountTitle)
    const getResourceButtonNextTitle = () => getResources(buttonNextTitle, true)
    const getResourceModalCountryPlaceholder = () => getResources(modalCountryPlaceholder, true)
    const getResourceModalAgePlaceholder = () => getResources(modalAgePlaceholder, true)
    const getResourceModalButtonCancelTitle = () => getResources(modalButtonCancelTitle, true)
    const getResourceKeyboardButtonNextTitle = () => getResources(keyboardButtonNextTitle, true)
    const getResourceKeyboardButtonSubmitTitle = () => getResources(keyboardButtonSubmitTitle, true)

    return {
        getResourceTitle,
        getResourceFullNamePlaceholder,
        getResourceEmailPlaceholder,
        getResourceCountryPlaceholder,
        getResourceAgePlaceholder,
        getResourcePasswordPlaceholder,
        getResourcePasswordDescription,
        getResourceConfirmPasswordPlaceholder,
        getResourceAlreadyHaveAnAccountTitle,
        getResourceButtonNextTitle,
        getResourceModalCountryPlaceholder,
        getResourceModalAgePlaceholder,
        getResourceModalButtonCancelTitle,
        getResourceKeyboardButtonNextTitle,
        getResourceKeyboardButtonSubmitTitle,
    }
})

    .views(self => {
        //MARK: Volatile State

        //MARK: Views

        const getTestIDTitle = () => Utils.getTestIDObject(TestIDResources.CreateAccountScreen.title)
        const getTestIDFullNamePlaceholder = () => Utils.getTestIDObject(TestIDResources.CreateAccountScreen.fullNamePlaceholder)
        const getTestIDEmailPlaceholder = () => Utils.getTestIDObject(TestIDResources.CreateAccountScreen.emailPlaceholder)
        const getTestIDCountryPlaceholder = () => Utils.getTestIDObject(TestIDResources.CreateAccountScreen.countryPlaceholder)
        const getTestIDAgePlaceholder = () => Utils.getTestIDObject(TestIDResources.CreateAccountScreen.agePlaceholder)
        const getTestIDPasswordPlaceholder = () => Utils.getTestIDObject(TestIDResources.CreateAccountScreen.passwordPlaceholder)
        const getTestIDPasswordDescription = () => Utils.getTestIDObject(TestIDResources.CreateAccountScreen.passwordDescription)
        const getTestIDConfirmPasswordPlaceholder = () => Utils.getTestIDObject(TestIDResources.CreateAccountScreen.confirmPasswordPlaceholder)
        const getTestIDAlreadyHaveAnAccountTitle = () => Utils.getTestIDObject(TestIDResources.CreateAccountScreen.alreadyHaveAnAccount)
        const getTestIDButtonNextTitle = () => Utils.getTestIDObject(TestIDResources.CreateAccountScreen.buttonNext)
        const getTestIDModalPicker = () => Utils.getTestIDObject(TestIDResources.CreateAccountScreen.modalPicker)
        const getTestIDDescription = () => Utils.getTestIDObject(TestIDResources.CreateAccountScreen.description)
        const getTestIDErrorMessage = () => Utils.getTestIDObject(TestIDResources.CreateAccountScreen.errorMessage)

        return {
            getTestIDTitle,
            getTestIDFullNamePlaceholder,
            getTestIDEmailPlaceholder,
            getTestIDCountryPlaceholder,
            getTestIDAgePlaceholder,
            getTestIDPasswordPlaceholder,
            getTestIDPasswordDescription,
            getTestIDConfirmPasswordPlaceholder,
            getTestIDAlreadyHaveAnAccountTitle,
            getTestIDButtonNextTitle,
            getTestIDModalPicker,
            getTestIDDescription,
            getTestIDErrorMessage
        }
    })