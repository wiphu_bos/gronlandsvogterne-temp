import { types } from "mobx-state-tree"

export const CreateAccountPropsModel = {
    isModalShowing: types.optional(types.boolean, false),
} 