// MARK: Import
import { Keyboard, Alert } from "react-native"
import { NavigationKey } from "../../constants/app.constant"
import { CreateAccountProps } from "./create-account.props"
import { ISelect } from "../../models/user-store"
import { RootStore, INavigationRoute } from "../../models"
import { onSnapshot } from "mobx-state-tree"
import { StackActions } from "@react-navigation/native"
import { CreateAccountStore, CreateAccountResourcesStore, CreateAccountResourcesStoreModel, CreateAccountStoreModel } from "./storemodels"
import { SelectionType } from "../../models/validation-store"
import * as DataUtils from "../../utils/data.utils"
import { BaseController } from "../base.controller"
import * as Validate from "../../utils/local-validate"

class CreateAccountController extends BaseController {

    /*
        Mark Injectable Variable & Declaration
    */

    public static resourcesViewModel: CreateAccountResourcesStore
    public static viewModel: CreateAccountStore

    private static _ageContentList: ISelect[]
    private static _ageSelected: string
    private static _ageSelectedIndex: number
    private static _countryContentList: ISelect[]
    private static _countrySelected: string
    private static _countrySelectedIndex: number
    private static _selectionTypeSelected: SelectionType

    /*
        Mark Constructor
    */

    constructor(rootStore?: RootStore, props?: CreateAccountProps & Partial<INavigationRoute>, isNestedNavigation?: boolean) {
        super(rootStore, props, isNestedNavigation)
        if (this._myProps?.route?.params?.isInitialized) return
        this._setupResourcesViewModel()
        this._setupProps(props)
        this._setupViewModel()
        this._setupSnapShot()
        this._getDropdownContents()
    }

    /*
       Mark Setup
   */

    private _setupResourcesViewModel = () => {
        CreateAccountController.resourcesViewModel = CreateAccountResourcesStoreModel.create({ ...this._rootStore?.getAllGeneralResourcesStore })
    }
    private _setupViewModel = () => {
        const localStore = this._rootStore?.getModuleStore(NavigationKey.CreateAccount)
        CreateAccountController.viewModel = localStore && CreateAccountStoreModel.create({ ...localStore }) || CreateAccountStoreModel.create({})
    }

    private _setupSnapShot = () => {
        onSnapshot(CreateAccountController.viewModel, (snap: CreateAccountStore) => {
            this._setInitializedToPropsParams()
        })
    }

    /*
       Mark Data
   */

    private _getDropdownContents = () => {
        this._getCountryContent()
        this._getAgeContent()
    }

    private _getCountryContent = () => {
        CreateAccountController._countryContentList = this._rootStore?.getSharedStore?.getCountries || []
        /*
        Fixed To Denmark country For now
        */
        CreateAccountController._countrySelected = CreateAccountController._countryContentList[0].key
        CreateAccountController._countrySelectedIndex = 0
        this._setMockData({ key: CreateAccountController._countryContentList[0].key, value: CreateAccountController._countryContentList[0].label })
        /*
        Fixed To Denmark country For now
        */

        //Original Code
        // countrySelected = viewModel?.getCountry?.key
        // countrySelectedIndex = countryContentList?.findIndex((e) => {
        //     if (!countrySelected) return true
        //     return e.key === countrySelected
        // })
    }

    private _getAgeContent = () => {
        CreateAccountController._ageContentList = this._rootStore?.getSharedStore?.getAge || []
        CreateAccountController._ageSelected = CreateAccountController.viewModel?.getAge?.key
        CreateAccountController._ageSelectedIndex = DataUtils.getAgeSelectedIndex(CreateAccountController._ageSelected, CreateAccountController._ageContentList)
    }

    public getSelectOptions = () => {
        const countryOptions = {
            flatListViewProps: { selectedIndex: CreateAccountController._countrySelectedIndex },
            selectedOption: CreateAccountController._countrySelected,
            placeholderText: CreateAccountController.resourcesViewModel?.getResourceModalCountryPlaceholder(),
            options: CreateAccountController._countryContentList
        }
        const ageOptions = {
            flatListViewProps: { selectedIndex: CreateAccountController._ageSelectedIndex },
            keyboardType: 'numeric',
            selectedOption: CreateAccountController._ageSelected,
            placeholderText: CreateAccountController.resourcesViewModel?.getResourceModalAgePlaceholder(),
            options: CreateAccountController._ageContentList
        }
        return CreateAccountController._selectionTypeSelected == SelectionType.COUNTRY ? countryOptions : ageOptions
    }

    private _setMockData = (item: any) => {
        this._rootStore?.getUserStore?.setCountry(item)
        CreateAccountController.viewModel?.setCountry(item)
        CreateAccountController.viewModel?.setCountryErrorMessage(null)
    }

    /*

        Mark Event

    */

    public onTouchContryTextField = () => {
        CreateAccountController._selectionTypeSelected = SelectionType.COUNTRY
        CreateAccountController.viewModel?.setIsModalShowing(true)
    }

    public onTouchAgeTextField = () => {
        Keyboard.dismiss()
        CreateAccountController._selectionTypeSelected = SelectionType.AGE
        CreateAccountController.viewModel?.setIsModalShowing(true)
    }

    public onSelectItem = (item: any): void => {
        const { key, label } = item
        const selectedItem: ISelect = { key: key, value: label }
        if (CreateAccountController._selectionTypeSelected == SelectionType.COUNTRY) {
            this._rootStore?.getUserStore?.setCountry(selectedItem)
            CreateAccountController.viewModel?.setCountry(selectedItem)
        } else {
            this._rootStore?.getUserStore?.setAge(selectedItem)
            CreateAccountController.viewModel?.setAge(selectedItem)
            CreateAccountController._ageSelected = key
            CreateAccountController._ageSelectedIndex = DataUtils.getAgeSelectedIndex(CreateAccountController._ageSelected, CreateAccountController._ageContentList)
        }
        this.onCloseModal()
    }

    public onCloseModal = (): void => {
        const { getCountry, getAge } = this._rootStore?.getUserStore
        const params = {
            localViewModel: CreateAccountController.viewModel as any,
            rootStore: this._rootStore
        }
        CreateAccountController.viewModel?.setIsModalShowing(false)
        if (CreateAccountController._selectionTypeSelected == SelectionType.COUNTRY) {
            const isInvalid = { isInvalid: !(getCountry?.key) }
            const validateParams = { ...params, ...isInvalid }
            Validate.onErrorMessageCountryField(validateParams)
        } else if (CreateAccountController._selectionTypeSelected == SelectionType.AGE) {
            const isInvalid = { isInvalid: !(getAge?.key) }
            const validateParams = { ...params, ...isInvalid }
            Validate.onErrorMessageAgeField(validateParams)
        }
        CreateAccountController._selectionTypeSelected = null
    }

    public onFullNameChangeText = (value: string): void => {
        const params = {
            value: value,
            localViewModel: CreateAccountController.viewModel as any,
            globalViewModel: this._rootStore?.getUserStore
        }
        Validate.onFullNameChangeText(params)
    }

    public onEmailChangeText = (value: string): void => {
        const params = {
            value: value,
            localViewModel: CreateAccountController.viewModel as any,
            globalViewModel: this._rootStore?.getUserStore
        }
        Validate.onEmailChangeText(params)
    }

    public onPasswordChangeText = (value: string): void => {
        const params = {
            value: value,
            localViewModel: CreateAccountController.viewModel as any,
            globalViewModel: this._rootStore?.getUserStore
        }
        Validate.onPasswordChangeText(params)
    }

    public onConfirmPasswordChangeText = (value: string): void => {
        CreateAccountController.viewModel.setConfirmPasswordErrorMessage(null)
        CreateAccountController.viewModel.setConfirmPassword(value)
        this._rootStore?.getUserStore?.setConfirmPassword(value)
    }

    public onBlurFullNameTextField = (): void => {
        const fullName = CreateAccountController.viewModel?.getFullName
        const params = {
            value: fullName,
            localViewModel: CreateAccountController.viewModel as any,
            rootStore: this._rootStore
        }
        Validate.onBlurFullNameTextField(params)
    }

    public onBlurEmailTextField = (): void => {
        const email = CreateAccountController.viewModel?.getEmail
        const params = {
            value: email,
            localViewModel: CreateAccountController.viewModel as any,
            rootStore: this._rootStore
        }
        Validate.onBlurEmailTextField(params)
    }

    public onBlurPasswordField = (): void => {
        const { getPassword, getConfirmPassword } = CreateAccountController.viewModel
        const params = {
            value: getPassword,
            localViewModel: CreateAccountController.viewModel as any,
            rootStore: this._rootStore
        }
        Validate.onBlurPasswordField(params)
        this.onBlurConfirmPasswordField()
        if (getConfirmPassword) this.onBlurConfirmPasswordField()
    }

    public onBlurConfirmPasswordField = (): void => {
        const { getPassword, getConfirmPassword } = CreateAccountController.viewModel
        if (!getConfirmPassword || !getPassword) return
        const params = {
            primaryValue: getPassword,
            value: getConfirmPassword,
            localViewModel: CreateAccountController.viewModel as any,
            rootStore: this._rootStore
        }
        Validate.onBlurConfirmPasswordField(params)
    }

    public onSubmitButtonDidTouch = (): void => {
        alert(  CreateAccountController.viewModel.getFullName)
        // alert(  CreateAccountController.viewModel.getAge)
        // alert(  CreateAccountController.viewModel.age )
        // alert(  CreateAccountController.viewModel.email )

        Keyboard.dismiss()
        this._checkValidData()
        this._validateData()
        if (!CreateAccountController.viewModel?.getIsValid) return

        
        console.log( 'CreateAccountController' )
        console.log( CreateAccountController.viewModel.email )

        // const params = {
        //     full_name: self.full_name,
        //     country: self.country.key,
        //     age: self.age.key,
        //     email: self.email
        // }
        // return yield AuthServices.createUser(params)

        
        //this.goToTermsAndConditions()
    }

    public goToTermsAndConditions = (): void => {
        const { navigation } = this._myProps
        navigation?.dispatch(StackActions.push(NavigationKey.TermsAndConditions) as any)
    }

    public backToLogin = (): void => {
        const { navigation } = this._myProps
        navigation?.dispatch(StackActions.popToTop() as any)
    }

    private _validateDropdownData = (): void => {
        const { getCountry, getAge } = CreateAccountController.viewModel
        if (!(getCountry?.key)) Validate.onErrorMessageCountryField({
            isInvalid: true,
            localViewModel: CreateAccountController.viewModel as any,
            rootStore: this._rootStore
        })
        if (!(getAge?.key)) Validate.onErrorMessageAgeField({
            isInvalid: true,
            localViewModel: CreateAccountController.viewModel as any,
            rootStore: this._rootStore
        })
    }

    private _validateData = (): void => {
        this.onBlurFullNameTextField()
        this.onBlurEmailTextField()
        this.onBlurPasswordField()
        this.onBlurConfirmPasswordField()
        this._validateDropdownData()
    }

    /*
       Mark Life cycle
   */

    /*
       Mark Helper
   */
    private _checkValidData = () => {
        const { getFullName, getEmail, getPassword, getConfirmPassword, getCountry, getAge } = CreateAccountController.viewModel
        CreateAccountController.viewModel?.setIsValid(getFullName && getEmail && getPassword && getConfirmPassword && getCountry?.key && getAge?.key ? true : false)
    }
}

export default CreateAccountController