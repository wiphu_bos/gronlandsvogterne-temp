import { ViewStyle, TextStyle } from "react-native"
import { color } from "../../theme"
import * as metric from "../../theme"
import { RFValue } from "react-native-responsive-fontsize"

export const FULL: ViewStyle = {
    flex: 1,
}

export const SAFE_AREA_VIEW: ViewStyle = {
    ...FULL,
}

export const AVOID_KEYBOARD_VIEW: ViewStyle = {
    ...FULL
}
export const SCROLL_VIEW: ViewStyle = {
    paddingBottom: metric.ratioHeight(30),
    justifyContent: "center"
}
export const CONTAINER_VIEW: ViewStyle = {
    marginHorizontal: metric.ratioWidth(57)
}

export const CONTENT_VIEW: ViewStyle = {
    marginTop: metric.ratioHeight(6)
}

export const TITLE_VIEW: ViewStyle = {
    marginTop: metric.ratioHeight(66)
}

const BASE_TEXT: TextStyle = {
    fontFamily: "Montserrat",
    color: color.palette.white,
    textAlign: 'center',
}

export const TITLE: TextStyle = {
    ...BASE_TEXT,
    fontSize: RFValue(25),
    fontWeight: '400',

    //Android
    fontFamily: 'Montserrat-Regular'
}
export const BUTTON_MARGIN: ViewStyle = {
    marginTop: metric.ratioHeight(20)
}
export const FOOTER: ViewStyle = {
    marginTop: metric.ratioHeight(20),
    height:metric.ratioHeight(metric.isNewIPhone ? 44 : 80),
    justifyContent:'flex-end'
}
export const BOTTOM_TEXT: TextStyle = {
    alignSelf:'center',
}

export const BACKGROUND_STYLE = color.pinkBlueGradient