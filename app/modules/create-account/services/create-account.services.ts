//import { types } from "mobx-state-tree"
import { types, flow } from "mobx-state-tree"

import { CreateAccountPropsModel } from "../viewmodels"
import AuthServices from "../../../services/api-domain/auth.services"
// import { types, flow } from "mobx-state-tree"

export const CreateAccountServiceActions = types.model(CreateAccountPropsModel).actions(self => {

    const createUser = flow(function* () {
        const params = {
            full_name: 'full_name',
            country: 'full_name',
            age: 'full_name',
            email: 'full_name'
        }
        return yield AuthServices.createUser(params)
    })

    //MARK: Service Actions
    return {
        createUser,
    }
})


// export const UserRegisterServiceActions = types.model(UserPropsModel).actions(self => {

//     const createUser = flow(function* () {
//         const params = {
//             full_name: self.full_name,
//             country: self.country.key,
//             age: self.age.key,
//             email: self.email
//         }
//         return yield AuthServices.createUser(params)
//     })


//     const generateVerifyCode = flow(function* () {
//         const params = {
//             email: self.email,
//             uid: self.uid
//         }
//         return yield AuthServices.generateVerifyCode(params)
//     })

//     const resendVerifyCode = flow(function* () {
//         const params = {
//             email: self.email
//         }
//         return yield AuthServices.resendVerifyCode(params)
//     })

//     const sendCodeToVerify = flow(function* (code: string) {
//         const params = {
//             email: self.email,
//             uid: self.uid,
//             code: parseInt(code)
//         }
//         const result: APIResponse = yield AuthServices.sendCodeToVerify(params)
//         const success = result as APISuccessResponse
//         if (success) {
//             self.is_verified = true
//         } else {
//             throw result
//         }
//         return result
//     })

//     const createFirebaseAuth = flow(function* () {
//         const params = {
//             uid: self.uid,
//             email: self.email,
//             password: self.password
//         }
//         return yield AuthServices.createFirebaseAuth(params)
//     })

//     const signInWithEmailAndPassword = flow(function* () {
//         const params = { email: self.email, password: self.password }
//         return yield AuthServices.signInWithEmailAndPassword(params)
//     })

//     const sendPasswordResetEmail = flow(function* () {
//         return yield AuthServices.sendPasswordResetEmail({ email: self.email })
//     })
//     return {
//         createUser,
//         generateVerifyCode,
//         sendCodeToVerify,
//         createFirebaseAuth,
//         signInWithEmailAndPassword,
//         sendPasswordResetEmail,
//         resendVerifyCode
//     }
// })