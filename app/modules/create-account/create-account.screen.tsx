// MARK: Import

import React, { useRef } from "react"
import { observer } from "mobx-react-lite"
import { View, KeyboardAvoidingView } from "react-native"
import { CreateAccountProps } from "./create-account.props"
import CreateAccountController from "./create-account.controllers"
import { Text, Button, Wallpaper, AnimatedScrollView } from "../../components"
import { SafeAreaView } from "react-navigation"
import FloatLabelTextInput from '../../libs/float-label-textfield/float-label-text-field'
import ModalFilterPicker from '../../libs/modal-filter-picker/modal-filter-picker'
import * as metric from "../../theme"
import { GradientButton } from "../../components/gradient-button/button"
import { useConfigurate } from "../../custom-hooks/use-configure-controller"
import { textPresets } from "../../components/button/button.presets"

// MARK: Style Import

import * as Styles from "./create-account.styles"

export const CreateAccountScreen: React.FunctionComponent<CreateAccountProps> = observer((props) => {

    const controller = useConfigurate(CreateAccountController, props) as CreateAccountController

    const emailTextInput = useRef(null)
    const ageTextInput = useRef(null)
    const passwordTextInput = useRef(null)
    const confirmPasswordTextInput = useRef(null)
    // MARK: Render
    return (
        <Wallpaper showBubble>
            <SafeAreaView
                {...metric.safeAreaViewProps}
                shouldRasterizeIOS
                style={Styles.SAFE_AREA_VIEW}>
                <KeyboardAvoidingView style={Styles.AVOID_KEYBOARD_VIEW} behavior={metric.keyboardBehavior} keyboardVerticalOffset={metric.keyboardVerticalOffSet}>
                    <AnimatedScrollView
                        contentContainerStyle={Styles.SCROLL_VIEW}
                        {...metric.solidScrollView}>
                        <View shouldRasterizeIOS style={Styles.CONTAINER_VIEW}>
                            <View shouldRasterizeIOS style={Styles.TITLE_VIEW}>
                                <Text text={CreateAccountController.resourcesViewModel.getResourceTitle()} style={Styles.TITLE} {...CreateAccountController.resourcesViewModel.getTestIDTitle()} />
                            </View>
                            <FloatLabelTextInput
                                {...CreateAccountController.resourcesViewModel.getTestIDFullNamePlaceholder()}
                                containerStyle={Styles.CONTENT_VIEW}
                                placeholder={CreateAccountController.resourcesViewModel.getResourceFullNamePlaceholder()}
                                onChangeTextValue={controller.onFullNameChangeText}
                                noBorder
                                returnKeyType="next"
                                onSubmitEditing={() => { emailTextInput.current.focus() }}
                                errorMessage={CreateAccountController.viewModel?.fullNameErrorMessage}
                                testIDErrorMessage={CreateAccountController.resourcesViewModel.getTestIDErrorMessage()}
                                onBlur={controller.onBlurFullNameTextField}
                            />
                            <FloatLabelTextInput
                                ref={emailTextInput}
                                returnKeyType="next"
                                onSubmitEditing={() => { ageTextInput.current.focus() }}
                                {...CreateAccountController.resourcesViewModel.getTestIDEmailPlaceholder()}
                                placeholder={CreateAccountController.resourcesViewModel.getResourceEmailPlaceholder()}
                                onChangeTextValue={controller.onEmailChangeText}
                                errorMessage={CreateAccountController.viewModel?.getEmailErrorMessage}
                                testIDErrorMessage={CreateAccountController.resourcesViewModel.getTestIDErrorMessage()}
                                noBorder
                                keyboardType="email-address"
                                onBlur={controller.onBlurEmailTextField}
                            />
                            <FloatLabelTextInput
                                {...CreateAccountController.resourcesViewModel.getTestIDCountryPlaceholder()}
                                editable={false}
                                value={CreateAccountController.viewModel?.getCountry?.value /*|| (CreateAccountController.viewModel?.getResourceCountryPlaceholder())*/}
                                noBorder
                                //isDropDown
                                // onTouchTextField={controller.onTouchContryTextField}
                                errorMessage={CreateAccountController.viewModel?.getCountryErrorMessage}
                                testIDErrorMessage={CreateAccountController.resourcesViewModel.getTestIDErrorMessage()}
                            />
                            <FloatLabelTextInput
                                ref={ageTextInput}
                                {...CreateAccountController.resourcesViewModel.getTestIDAgePlaceholder()}
                                editable={false}
                                value={CreateAccountController.viewModel?.getAge?.value || (CreateAccountController.resourcesViewModel.getResourceAgePlaceholder())}
                                noBorder
                                isDropDown
                                onTouchTextField={controller.onTouchAgeTextField}
                                errorMessage={CreateAccountController.viewModel?.getAgeErrorMessage}
                                testIDErrorMessage={CreateAccountController.resourcesViewModel.getTestIDErrorMessage()}
                                returnKeyType="next"
                                onSubmitEditing={() => { passwordTextInput.current.focus() }}
                            //                 onFocus={@myFocusFunction}
                            //   onBlur={@onBlurFunction}
                            />
                            <FloatLabelTextInput
                                ref={passwordTextInput}
                                {...CreateAccountController.resourcesViewModel.getTestIDPasswordPlaceholder()}
                                {...metric.secureTextField}
                                placeholder={CreateAccountController.resourcesViewModel.getResourcePasswordPlaceholder()}
                                onChangeTextValue={controller.onPasswordChangeText}
                                noBorder
                                errorMessage={CreateAccountController.viewModel?.getPasswordErrorMessage}
                                testIDErrorMessage={CreateAccountController.resourcesViewModel.getTestIDErrorMessage()}
                                testIDDescription={CreateAccountController.resourcesViewModel.getTestIDDescription()}
                                description={CreateAccountController.resourcesViewModel.getResourcePasswordDescription()}
                                onBlur={controller.onBlurPasswordField}
                                returnKeyType="next"
                                onSubmitEditing={() => { confirmPasswordTextInput.current.focus() }}
                            />
                            <FloatLabelTextInput
                                ref={confirmPasswordTextInput}
                                {...CreateAccountController.resourcesViewModel.getTestIDConfirmPasswordPlaceholder()}
                                {...metric.secureTextField}
                                placeholder={CreateAccountController.resourcesViewModel.getResourceConfirmPasswordPlaceholder()}
                                onChangeTextValue={controller.onConfirmPasswordChangeText}
                                noBorder
                                errorMessage={CreateAccountController.viewModel?.getConfirmPasswordErrorMessage}
                                testIDErrorMessage={CreateAccountController.resourcesViewModel.getTestIDErrorMessage()}
                                onBlur={controller.onBlurConfirmPasswordField}
                                returnKeyType="send"
                                onSubmitEditing={() => { controller.onSubmitButtonDidTouch() }}
                            />
                            <GradientButton
                                text={CreateAccountController.resourcesViewModel.getResourceButtonNextTitle()}
                                {...CreateAccountController.resourcesViewModel.getTestIDButtonNextTitle()}
                                containerStyle={Styles.BUTTON_MARGIN}
                                onPress={controller.onSubmitButtonDidTouch} />
                            <Button
                                preset="none"
                                isAnimated={false}
                                {...CreateAccountController.resourcesViewModel.getTestIDAlreadyHaveAnAccountTitle()}
                                text={CreateAccountController.resourcesViewModel.getResourceAlreadyHaveAnAccountTitle()} 
                                textStyle={[textPresets.regularNormal,Styles.BOTTOM_TEXT]}
                                 style={Styles.FOOTER}
                                 onPress={controller.backToLogin} />
                        </View>

                    </AnimatedScrollView>
                </KeyboardAvoidingView>
            </SafeAreaView>
            <ModalFilterPicker
                {...CreateAccountController.resourcesViewModel.getTestIDModalPicker()}
                cancelButtonText={CreateAccountController.resourcesViewModel.getResourceModalButtonCancelTitle()}
                visible={CreateAccountController.viewModel?.getIsModalShowing || false}
                onSelect={controller.onSelectItem}
                onCancel={controller.onCloseModal}
                {...controller.getSelectOptions()}
            />
        </Wallpaper >

    )
})
