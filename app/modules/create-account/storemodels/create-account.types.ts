import { Instance, SnapshotOut } from "mobx-state-tree"
import { CreateAccountStoreModel, CreateAccountResourcesStoreModel } from "./create-account.store"

export type CreateAccountStore = Instance<typeof CreateAccountStoreModel>
export type CreateAccountStoreSnapshot = SnapshotOut<typeof CreateAccountStoreModel>

export type CreateAccountResourcesStore = Instance<typeof CreateAccountResourcesStoreModel>
export type CreateAccountResourcesStoreSnapshot = SnapshotOut<typeof CreateAccountResourcesStoreModel>