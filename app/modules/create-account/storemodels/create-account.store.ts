import { types } from "mobx-state-tree"
import { UserStoreModel } from "../../../models/user-store"
import { ValidationStoreModel } from "../../../models/validation-store"
import { CreateAccountActions, CreateAccountViews, CreateAccountPropsModel, CreateAccountResourcesViews} from "../viewmodels"
import { StoreName } from "../../../constants/app.constant"
import { GeneralResourcesStoreModel } from "../../../models/general-resources-store"

const CreateAccountModel = types.model(StoreName.CreateAccount, CreateAccountPropsModel)
const CreateAccountLocalStore = types.compose(CreateAccountModel, CreateAccountActions, CreateAccountViews)
const CreateAccountBaseStore = types.compose(UserStoreModel, ValidationStoreModel)
export const CreateAccountStoreModel = types.compose(CreateAccountBaseStore, CreateAccountLocalStore)
export const CreateAccountResourcesStoreModel = types.compose(GeneralResourcesStoreModel, CreateAccountResourcesViews)