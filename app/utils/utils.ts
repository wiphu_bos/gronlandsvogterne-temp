import { NavigationState } from "../models/navigation-store"
export type StringToBooleanMap = { [key: string]: boolean }

export const arrayToDictionary = (arr?: Array<string>): StringToBooleanMap => {
    if (!arr) return {}
    return arr.reduce((dict: StringToBooleanMap, elem) => {
        dict[elem] = true
        return dict
    }, {})
}
export const getActiveRouteName = (state: typeof NavigationState) => {
    const routes = state?.routes
    if (!routes) return null

    const route = routes[state.index]
    if (route.state) return getActiveRouteName(route.state)
    return route.name
}

export function timeout(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms))
}

var tester = /^[-!#$%&'*+\/0-9=?A-Z^_a-z`{|}~](\.?[-!#$%&'*+\/0-9=?A-Z^_a-z`{|}~])*@[a-zA-Z0-9](-*\.?[a-zA-Z0-9])*\.[a-zA-Z](-?[a-zA-Z0-9])+$/
export const isEmail = (email: string): boolean => {
    if (!email) return false

    if (email.length > 256) return false;

    if (!tester.test(email)) return false;

    // Further checking of some things regex can't handle
    var [account, address] = email.split('@')
    if (account.length > 64) return false;

    var domainParts = address.split('.')
    if (domainParts.some(function (part) {
        return part.length > 63
    })) return false

    return true
}

export const isValidPassword = (value: string): boolean => {
    return (hasNumber(value) && hasUppercase(value) && hasProperLength(value))
}
const hasProperLength = (value: string): boolean => {
    return value.length >= 8
}
const hasNumber = (value: string): boolean => {
    return new RegExp(/[0-9]/).test(value)
}
const hasUppercase = (value: string): boolean => {
    return new RegExp(/[A-Z]/).test(value)
}

export const isEqual = (val1: any, val2: any): boolean => {
    return val1.localeCompare(val2) === 0
}

export const refDotNotation = (obj: any, str: string): any => {
    if (!obj || !str) return null
    return str.split(".").reduce(function (o, x) { return o[x] }, obj)
}

export function* range(start: number, end: number) {
    yield start;
    if (start === end) return;
    yield* range(start + 1, end)
}

export const getTestIDObject = (value: string) => {
    return {
        testID: value,
        accessibilityLabel: value
    }
}

export const getBadgeNumber = (value: number) => {
    if (value > 99) {
        return "99+"
    }
    return value.toString()
}
export const isEmptyObject = (object: any): boolean => Object.keys(object).length === 0

export const isValidURL = (value?: string) => {
    var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
    return value && regexp.test(value) || false
}