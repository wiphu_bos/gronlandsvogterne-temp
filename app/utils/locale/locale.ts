import { NativeModules } from "react-native"
import * as AppConstant from "../../constants/app.constant"
import * as RMCFConstant from "../../constants/firebase/remote-config"
import * as metric from "../../theme"

export const getLocaleDevice = (): string => {
    let defaultLocale = AppConstant.Locale.Default
    let locale = metric.isIPhone
        ? NativeModules.SettingsManager.settings.AppleLocale
        : NativeModules.I18nManager.localeIdentifier
    locale = locale?.slice(0,2)
    if (!locale) locale = NativeModules.SettingsManager.settings.AppleLanguages[0]
    return locale || defaultLocale
}

export const getLocalizedGeneralResources = async () => {
    //Refactor later >> can't use require('./' + locale + '/general-resources.json')
    const { GeneralResources, Introductions, Cities, Regions, RegionsCities, Interests, HavdSogerHans, Tags, Countries, Notifications } = RMCFConstant.RemoteConfigRootKey
    let locale = getLocaleDevice()
    const generalResources = locale === AppConstant.Locale.Denmark ?
        await require('./dk/general-resources.json') : await require('./en/general-resources.json')
    const introductions = locale === AppConstant.Locale.Denmark ?
        await require('./dk/introductions.json') : await require('./en/introductions.json')
    const interests = locale === AppConstant.Locale.Denmark ?
        await require('./dk/interests.json') : await require('./en/interests.json')
    const tags = locale === AppConstant.Locale.Denmark ?
        await require('./dk/tags.json') : await require('./en/tags.json')
    const cities = locale === AppConstant.Locale.Denmark ?
        await require('./dk/cities.json') : await require('./en/cities.json')
    const regions = locale === AppConstant.Locale.Denmark ?
        await require('./dk/regions.json') : await require('./en/regions.json')
    const regionsCities = locale === AppConstant.Locale.Denmark ?
        await require('./dk/regions-cities.json') : await require('./en/regions-cities.json')
    const countries = locale === AppConstant.Locale.Denmark ?
        await require('./dk/countries.json') : await require('./en/countries.json')
    const havdSogerHands = locale === AppConstant.Locale.Denmark ?
        await require('./dk/havd-soger-hans.json') : await require('./en/havd-soger-hans.json')
    const notifications = locale === AppConstant.Locale.Denmark ?
        await require('./dk/notifications.json') : await require('./en/notifications.json')
    return {
        ...{ [GeneralResources]: generalResources },
        ...{ [Introductions]: introductions },
        ...{ [Interests]: interests },
        ...{ [Tags]: tags },
        ...{ [Cities]: cities },
        ...{ [RegionsCities]: regionsCities },
        ...{ [Regions]: regions },
        ...{ [Countries]: countries },
        ...{ [HavdSogerHans]: havdSogerHands },
        ...{ [Notifications]: notifications }
    }
}