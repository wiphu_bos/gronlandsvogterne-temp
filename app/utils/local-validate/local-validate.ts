import * as Utils from "../"
import { Validate } from "./local-validate.types"
import { GeneralResources } from "../../constants/firebase/remote-config"
import * as metric from "../../theme"

export const onBlurFullNameTextField = (params: Validate): void => {
    const { ErrorMessage } = GeneralResources
    if (!params.value) {
        const fullnameEmptyTitle = params.rootStore?.getGeneralResourcesStore(ErrorMessage.fullnameEmptyTitle, true)
        params.localViewModel?.setFullNameErrorMessage(fullnameEmptyTitle)
    }
}

export const onBlurEmailTextField = (params: Validate): void => {
    const { ErrorMessage } = GeneralResources
    if (!params.value) {
        const emailEmptyTitle = params.rootStore?.getGeneralResourcesStore(ErrorMessage.emailEmptyTitle, true)
        params.localViewModel?.setEmailErrorMessage(emailEmptyTitle)
    } else if (!Utils.isEmail(params.value)) {
        const emailInvalidTitle = params.rootStore?.getGeneralResourcesStore(ErrorMessage.emailInvalidTitle, true)
        params.localViewModel?.setEmailErrorMessage(emailInvalidTitle)
    }
}

export const onBlurPasswordField = (params: Validate): void => {
    const { ErrorMessage } = GeneralResources
    if (!params.value) {
        const passwordEmptyTitle = params.rootStore?.getGeneralResourcesStore(ErrorMessage.passwordEmptyTitle, true)
        params.localViewModel?.setPasswordErrorMessage(passwordEmptyTitle)
    } else {
        const passwordInvalidTitle = params.rootStore?.getGeneralResourcesStore(ErrorMessage.passwordInvalidTitle, true)
        if (!Utils.isValidPassword(params.value)) params.localViewModel?.setPasswordErrorMessage(passwordInvalidTitle)
    }
}

export const onBlurConfirmPasswordField = (params: Validate): void => {
    const { ErrorMessage } = GeneralResources
    if (!Utils.isEqual(params.primaryValue, params.value)) {
        const confirmPasswordInvalidTitle = params.rootStore?.getGeneralResourcesStore(ErrorMessage.confirmPasswordInvalidTitle, true)
        params.localViewModel?.setConfirmPasswordErrorMessage(confirmPasswordInvalidTitle)
    } else {
        params.localViewModel?.setConfirmPasswordErrorMessage(null)
    }
}

export const onErrorMessageAgeField = (params: Validate): void => {
    const { ErrorMessage } = GeneralResources
    const ageEmptyTitle = params.rootStore?.getGeneralResourcesStore(ErrorMessage.ageEmptyTitle, true)
    params.localViewModel?.setAgeErrorMessage(params.isInvalid ? ageEmptyTitle : null)
}

export const onErrorMessageCountryField = (params: Validate): void => {
    const { ErrorMessage } = GeneralResources
    const countryEmptyTitle = params.rootStore?.getGeneralResourcesStore(ErrorMessage.countryEmptyTitle, true)
    params.localViewModel?.setCountryErrorMessage(params.isInvalid ? countryEmptyTitle : null)
}

export const onErrorMessageCityField = (params: Validate): void => {
    const { ErrorMessage } = GeneralResources
    const cityEmptyTitle = params.rootStore?.getGeneralResourcesStore(ErrorMessage.cityEmptyTitle, true)
    params.localViewModel?.setCityErrorMessage(params.isInvalid ? cityEmptyTitle : null)
}

export const onErrorMessageTagListSelection = (params: Validate): void => {
    const { ErrorMessage } = GeneralResources
    const tagListInvalidTitle = params.rootStore?.getGeneralResourcesStore(ErrorMessage.tagListInvalidTitle, true)
    params.localViewModel?.setTagListErrorMessage(params.isInvalid ? tagListInvalidTitle : null)
}

export const onErrorMessageInterestField = (params: Validate): void => {
    const { ErrorMessage } = GeneralResources
    const interestListEmptyTitle = params.rootStore?.getGeneralResourcesStore(ErrorMessage.interestListEmptyTitle, true)
    params.localViewModel?.setInterestListErrorMessage(params.isInvalid ? interestListEmptyTitle : null)
}

export const onErrorMessageHavdSogerHanField = (params: Validate): void => {
    const { ErrorMessage } = GeneralResources
    const havdSogerHanEmptyTitle = params.rootStore?.getGeneralResourcesStore(ErrorMessage.havdSogerHanEmptyTitle, true)
    params.localViewModel?.setHavdSogerHanListErrorMessage(params.isInvalid ? havdSogerHanEmptyTitle : null)
}

export const onErrorMessageProfileImageSelection = (params: Validate): void => {
    const { ErrorMessage } = GeneralResources
    const profileImageEmptyTitle = params.rootStore?.getGeneralResourcesStore(ErrorMessage.profileImageEmptyTitle, true)
    params.localViewModel?.setProfileImageErrorMessage(params.isInvalid ? profileImageEmptyTitle : null)
}

export const onErrorMessageGalleryImagesSelection = (params: Validate): void => {
    const { ErrorMessage } = GeneralResources
    const galleryImageEmptyTitle = params.rootStore?.getGeneralResourcesStore(ErrorMessage.galleryImageEmptyTitle, true)
    params.localViewModel?.setGalleryImageErrorMessage(params.isInvalid ? galleryImageEmptyTitle : null)
}

export const onFullNameChangeText = (params: Validate): void => {
    params.localViewModel?.setFullName(params.value)
    params.localViewModel?.setFullNameErrorMessage(null)
    params.globalViewModel?.setFullName(params.value)
}
export const onEmailChangeText = (params: Validate): void => {
    params.localViewModel?.setEmail(params.value)
    params.localViewModel?.setEmailErrorMessage(null)
    params.globalViewModel?.setEmail(params.value)
}

export const onPasswordChangeText = (params: Validate): void => {
    params.localViewModel?.setPasswordErrorMessage(null)
    params.localViewModel?.setPassword(params.value)
    params.globalViewModel?.setPassword(params.value)
}

export const onBlueVerifyCodeTextField = (params: Validate): void => {
    const { ErrorMessage } = GeneralResources
    let error: string
    if (!params.value) {
        error = params.rootStore?.getGeneralResourcesStore(ErrorMessage.verifyCodeEmptyTitle, true)
    } else if (params.value.length < metric.maxCharVerifyCode) {
        error = params.rootStore?.getGeneralResourcesStore(ErrorMessage.verifyCodeInvalidTitle, true)
    }
    params.localViewModel?.setCodeErrorMessage(error)
}