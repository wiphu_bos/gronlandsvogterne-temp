import { UserStore } from "../../models/user-store"
import { ValidationStore } from "../../models/validation-store/validation-store.types"
import { RootStore } from "../../models/root-store/root.types"
import { VerifyUserStore } from "../../modules/verify-user/storemodels/verify-user.types"
import { APIErrorResponse } from "../../constants/service.types"

export type ValidateStore =
    UserStore
    & ValidationStore
    & VerifyUserStore

export type Validate = {
    isInvalid?: boolean,
    primaryValue?: string,
    value?: string,
    error?: APIErrorResponse,
    localViewModel?: ValidateStore,
    rootStore?: RootStore,
    globalViewModel?: UserStore
}
