import dynamicLinks, { firebase } from '@react-native-firebase/dynamic-links'
import { NavigationKey } from "../../../constants/app.constant"
import { RootStore } from '../../../models/root-store'
import { useEffect } from 'react'
import { DynamicLink } from './dynamic-links.types'
import { StackActions } from '@react-navigation/core'

const navigateByLayout = (rootStore?: RootStore, link?: DynamicLink) => {
  const { VerifyUser } = NavigationKey
  if (link?.url?.includes(VerifyUser) &&
    rootStore?.getUserStore?.getUserId &&
    rootStore?.getUserStore?.getEmail &&
    rootStore?.getUserStore?.getIsVerified === false &&
    rootStore?.getNavigationStore?.getCurrentPageName !== VerifyUser) {
    (rootStore?.getNavigationStore?.getNavigation as any)?.dispatch(StackActions.push(VerifyUser, { email: rootStore?.getUserStore?.getEmail, shouldSendVerifyCode: false }))
  }

  return null
}

export const useDynamicLinks = (rootStore?: RootStore) => {
  useEffect(() => {
    const unsubscribe = firebase.dynamicLinks().onLink((link: DynamicLink) => navigateByLayout(rootStore, link))
    return () => unsubscribe()
  }, [])

  return null
}

export const useInitialDynamicLinks = (rootStore?: RootStore) => {
  let initialLink: DynamicLink
  const getInitialLink = async () => {
    initialLink = await dynamicLinks().getInitialLink()
    navigateByLayout(rootStore, initialLink)
  }
  getInitialLink()

}