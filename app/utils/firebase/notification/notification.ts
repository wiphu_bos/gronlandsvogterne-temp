import messaging, { FirebaseMessagingTypes } from '@react-native-firebase/messaging'
import PushNotification from 'react-native-push-notification';

class NotificationServices {
    public getToken = async () => await messaging().getToken()
    public requestPermission = async () => await messaging().requestPermission()
    public setBackgroundMessageHandler = (message: (message: FirebaseMessagingTypes.RemoteMessage) => Promise<any>) => messaging().setBackgroundMessageHandler(message)
    public isDeviceRegisteredForRemoteMessages = messaging().isDeviceRegisteredForRemoteMessages
    public registerDeviceForRemoteMessages = async () => await messaging().registerDeviceForRemoteMessages()
    public deleteToken = async () => await messaging().deleteToken()
    public foregroundStateMessage = (listener: (message: FirebaseMessagingTypes.RemoteMessage) => Promise<any>) => messaging().onMessage(listener)
    public onNotificationOpenedApp = (message: (message: FirebaseMessagingTypes.RemoteMessage) => Promise<any>) => messaging().onNotificationOpenedApp(message)
    public onTokenRefresh = (callback: (token: string) => any) => messaging().onTokenRefresh(callback)

    public createLocalNotification = (remoteMessage: FirebaseMessagingTypes.RemoteMessage) => {
        PushNotification.localNotification({
            vibration: 300, // vibration length in milliseconds, ignored if vibrate=false, default: 1000

            /* iOS only properties */
            category: remoteMessage?.category, // (optional) default: empty string
            userInfo: remoteMessage?.notification, // (optional) default: {} (using null throws a JSON value '<null>' error)

            /* iOS and Android properties */
            title: remoteMessage?.notification?.title, // (optional)
            message: remoteMessage?.notification?.body, // (required)
        })
    }
}

export default new NotificationServices()
