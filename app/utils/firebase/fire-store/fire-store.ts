import * as FireStoreConstant from "../../../constants/firebase/fire-store.constant"
import firestore from '@react-native-firebase/firestore'
import { ICommentListListener, INotificationListListener, IFirestoreListener, IMyDudeListListener, IDudeProfileListener, IFavouriteListListener, IUserListener, IChatRoomListener } from "./fire-store.types"
import { Collection } from "../../../constants/firebase/fire-store.constant"
import { SearchListType } from "../../../constants/app.constant"

class FirebaseFireStore {

    private onFirestoreListener = (params: IFirestoreListener): () => void => {
        const { onNext, onError, onComplete } = params
        return (params.query as any)
            .onSnapshot(
                {
                    next: onNext && onNext,
                    error: onError && onError,
                    complete: onComplete && onComplete
                }
            )
    }
    public fetchDudetteProfileDetails = async (uid?: string) => {
        const result = await firestore()
            .collection(FireStoreConstant.Collection.USERS)
            .doc(uid)
            .get()
        return result?.data()
    }

    public fetchDudeProfileDetails = async (dudeId?: string) => {
        const result = await firestore()
            .collectionGroup(FireStoreConstant.Collection.DUDES)
            .where(FireStoreConstant.Property.UID, '==', dudeId)
            .get()
        return result?.docs?.length > 0 && result?.docs[0]?.data()
    }

    public onFavouriteListener = (params: IFavouriteListListener): () => void => {
        const { userId, type, ...callbacks } = params
        const collection = type === SearchListType.Dude ? Collection.FAVOURITE_DUDE : Collection.FAVOURITE_DUDETTE
        const query = firestore().collection(Collection.USERS)
            .doc(userId)
            .collection(collection)
        const firestoreParams: IFirestoreListener = {
            query,
            ...callbacks
        }
        return this.onFirestoreListener(firestoreParams)
    }


    public onDudeProfileListener = (params: IDudeProfileListener): () => void => {
        const { dudeId, creatorId, ...callbacks } = params
        const query = firestore().collection(Collection.USERS)
            .doc(creatorId)
            .collection(Collection.DUDES)
            .doc(dudeId)
        const firestoreParams: IFirestoreListener = {
            query,
            ...callbacks
        }
        return this.onFirestoreListener(firestoreParams)
    }

    public onCommentListener = (params: ICommentListListener): () => void => {
        const { creatorId, dudeId, size, ...callbacks } = params
        const query = firestore().collection(Collection.USERS)
            .doc(creatorId)
            .collection(Collection.DUDES)
            .doc(dudeId)
            .collection(Collection.COMMENTS)
            .orderBy(FireStoreConstant.Property.CREATED_DATE, 'desc')
            .where(FireStoreConstant.Property.STATUS, '==', 'Active')
            .limit(size)
        const firestoreParams: IFirestoreListener = {
            query,
            ...callbacks
        }
        return this.onFirestoreListener(firestoreParams)
    }

    public onNotificationListener = (params: INotificationListListener): () => void => {
        const { userId, ...callbacks } = params
        const query = firestore().collection(Collection.USERS)
            .doc(userId)
            .collection(Collection.NOTIFICATIONS)
            .orderBy(FireStoreConstant.Property.CREATED_DATE, 'desc')
        const firestoreParams: IFirestoreListener = {
            query,
            ...callbacks
        }
        return this.onFirestoreListener(firestoreParams)
    }

    public onMyDudeListListener = (params: IMyDudeListListener): () => void => {
        const { userId, ...callbacks } = params
        const query = firestore().collection(Collection.USERS)
            .doc(userId)
            .collection(Collection.DUDES)
            .orderBy(FireStoreConstant.Property.IS_HIGHLIGHT, 'desc')
            .orderBy(FireStoreConstant.Property.CREATED_DATE, 'desc')
        const firestoreParams: IFirestoreListener = {
            query,
            ...callbacks
        }
        return this.onFirestoreListener(firestoreParams)
    }

    public onMySubscriptionListener = (params: IUserListener): () => void => {
        const { userId, ...callbacks } = params
        const query = firestore().collection(Collection.SUBSCRIPTIONS)
            .doc(userId)
        const firestoreParams: IFirestoreListener = {
            query,
            ...callbacks
        }
        return this.onFirestoreListener(firestoreParams)
    }

    public onUnReadNotificationListener = (params: IUserListener): () => void => {
        const { userId, ...callbacks } = params
        const query = firestore().collection(Collection.USERS)
            .doc(userId)
            .collection(Collection.NOTIFICATIONS)
            .where(`${FireStoreConstant.Property.MISC}.${FireStoreConstant.Property.IS_READ}`, '==', false)
        const firestoreParams: IFirestoreListener = {
            query,
            ...callbacks
        }
        return this.onFirestoreListener(firestoreParams)
    }

    public onMyProfileListener = (params: IUserListener): () => void => {
        const { userId, ...callbacks } = params
        const query = firestore().collection(Collection.USERS)
            .doc(userId)
        const firestoreParams: IFirestoreListener = {
            query,
            ...callbacks
        }
        return this.onFirestoreListener(firestoreParams)
    }

    public onChatListListener = (params: IUserListener): () => void => {
        const { userId, ...callbacks } = params
        const query = firestore().collection(Collection.CHATS)
            .where(FireStoreConstant.Property.INTERLOCUTOR_IDS, 'array-contains', userId)
            .where(`${FireStoreConstant.Property.LAST_MESSAGE}.${FireStoreConstant.Property.IS_REPLIED}`, '==', true)
            .orderBy(`${FireStoreConstant.Property.UPDATED_DATE}`, 'desc')
        const firestoreParams: IFirestoreListener = {
            query,
            ...callbacks
        }
        return this.onFirestoreListener(firestoreParams)
    }
    public onChatRoomListener = (params: IChatRoomListener): () => void => {
        const { chatRoomId, ...callbacks } = params
        const query = firestore().collection(Collection.CHATS)
            .doc(chatRoomId)
            .collection(Collection.MESSAGES)
        const firestoreParams: IFirestoreListener = {
            query,
            ...callbacks
        }
        return this.onFirestoreListener(firestoreParams)
    }
}
export default new FirebaseFireStore()