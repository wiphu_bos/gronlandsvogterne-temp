import { FirebaseFirestoreTypes } from "@react-native-firebase/firestore"
import { SearchListType } from "../../../constants/app.constant"

type IListenerSnapshot = {
    onNext?: (snapshot: FirebaseFirestoreTypes.QuerySnapshot | FirebaseFirestoreTypes.QueryDocumentSnapshot) => void,
    onError?: (error: Error) => void,
    onComplete?: () => void
}

export interface IUserListener extends IListenerSnapshot {
    userId: string
}
interface IDudeListener extends IListenerSnapshot {
    dudeId: string,
    creatorId: string
}

export interface IFirestoreListener extends IListenerSnapshot {
    query: FirebaseFirestoreTypes.Query | FirebaseFirestoreTypes.DocumentReference
}

export interface ICommentListListener extends IListenerSnapshot, IDudeListener {
    size?: number
}

export interface IDudeProfileListener extends IDudeListener {
}
export interface INotificationListListener extends IUserListener {
}

export interface IMyDudeListListener extends IUserListener {
}
export interface IChatRoomListener extends IListenerSnapshot {
    chatRoomId: string
}
export interface IFavouriteListListener extends IUserListener {
    type: SearchListType
}