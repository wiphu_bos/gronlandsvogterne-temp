import storage, { FirebaseStorageTypes } from '@react-native-firebase/storage'
import { IUploadFileData, IUploadFileParams } from './fire-storage.types'

export const uploadfiles = async (params: IUploadFileParams): Promise<IUploadFileData[]> => {
    const promiseTask = await Promise.all(
        params.fileList?.map((file, index) => new Promise(function (resolve, reject) {
            let filename = file.file_path.split('/')?.pop()?.split('.')[0]
            filename = filename.concat(index.toString()).concat('_').concat(new Date().getTime().toString())
            const storageRef = storage().ref(`${file.storage_path}/${params.uid}/${filename}`)
            var uploadTask = storageRef.putFile(file.file_path)
            let taskSnapShot: FirebaseStorageTypes.TaskSnapshot
            uploadTask.on(storage.TaskEvent.STATE_CHANGED,
                function (snapshot) {
                    var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100
                    switch (snapshot.state) {
                        case storage.TaskState.PAUSED:
                            break
                        case storage.TaskState.RUNNING:
                            break
                        case storage.TaskState.SUCCESS:
                            taskSnapShot = snapshot
                            break
                    }
                }, function (error) {
                    reject(error)
                    switch (error.code) {
                        case 'storage/unauthorized':
                            break
                        case 'storage/canceled':
                            break
                        case 'storage/unknown':
                            break
                    }
                }, function () {
                    taskSnapShot.ref.getDownloadURL().then(function (downloadURL: string) {
                        const result: IUploadFileData = {
                            ...file,
                            file_name: filename,
                            url: downloadURL
                        }
                        resolve(result)
                    })
                })
        })
        ))
    return promiseTask as any
}

export const deletefiles = async (params: IUploadFileParams): Promise<IUploadFileData[]> => {
    const promiseTask = await Promise.all(
        params.fileList?.map((file, index) => new Promise(function (resolve, reject) {
            storage()
                .ref(`${file.storage_path}/${params.uid}/${file.file_name}`)
                .delete()
                .then(function () {
                    const result: IUploadFileData = {
                        ...file,
                        file_name: file.file_name,
                        url: file.url
                    }
                    resolve(result)
                }).catch(function (error) {
                    resolve(error)
                });
        })
        ))
    return promiseTask as any
}
