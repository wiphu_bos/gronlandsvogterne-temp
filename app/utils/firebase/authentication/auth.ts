import auth from '@react-native-firebase/auth'

class FireBaseServices {
    public fetchCurrentUser = () => {
        return auth().currentUser
    }
    public updateAuthPassword = async (password: string) => {
        return await auth().currentUser?.updatePassword(password)
    }

    public fetchSignInMethodsForEmail = async (email: string) => {
        return await auth().fetchSignInMethodsForEmail(email)
    }

    public registerEmailAndPassword = async (email: string, password: string) => {
        return await auth().createUserWithEmailAndPassword(email, password)
    }

    public signInEmailAndPassword = async (email: string, password: string) => {
        return await auth().signInWithEmailAndPassword(email, password)
    }

    public sendPasswordResetEmail = async (email: string) => {
        return await auth().sendPasswordResetEmail(email)
    }

    public getFirebaseToken = async (): Promise<string> => {
        return await auth().currentUser?.getIdToken(true)
    }

    public signOut = async (): Promise<any> => {
        return await auth().signOut()
    }
}
export default new FireBaseServices()
