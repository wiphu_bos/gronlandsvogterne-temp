import { useState, useEffect } from 'react'
import auth from '@react-native-firebase/auth'
import { FirebaseUserInfo } from './auth.types'
import { RootStore } from '../../../models'

export const useAuthentication = (rootStore: RootStore) => {

    // MARK: Global Variables

    const [initializing, setInitializing] = useState<boolean>(true)
    const [user, setUser] = useState()

    const onAuthStateChanged = async (user: FirebaseUserInfo) => {
        if (user && user?.emailVerified === true) {
            rootStore.getAuthStore?.setFirebaseUserInfo(user)
            rootStore.getAuthStore?.setIsLoggedIn(true)
            rootStore.getUserStore?.setIsVerified(true)
            rootStore.getUserStore?.setUserId(user?.uid)
            setUser(user as any)
        } else {
            rootStore?.getUserStore?.forceLogoutWithoutAuth(rootStore)
        }
        if (initializing) setInitializing(false)
    }

    useEffect(() => {
        const subscriber = auth().onAuthStateChanged(onAuthStateChanged)
        return subscriber
    }, [])

    if (initializing) return null
}
