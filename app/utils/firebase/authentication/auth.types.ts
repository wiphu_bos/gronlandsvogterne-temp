import { RootStore } from "../../../models"
import { ValidateStore } from "../../local-validate/local-validate.types"

type AdditionalUserInfo = {
    username?: string,
    providerId?: string,
    isNewUser?: boolean,
    profile?: any
}

type UserProviderData = {
    providerId?: string,
    uid?: string,
    email?: string
}
type UserMetadata = {
    creationTime?: number,
    lastSignInTime?: number
}
export type FirebaseUserInfo = {
    displayName?: string,
    email?: string,
    emailVerified?: boolean,
    isAnonymous?: boolean,
    metaData?: UserMetadata,
    providerData?: UserProviderData[],
    providerId?: string,
    photoURL?: string,
    phoneNumber?: string,
    uid?: string
}
export type UserCredential = {
    additionalUserInfo?: AdditionalUserInfo,
    user?: FirebaseUserInfo
}

export type FirebaseErrorResponseValidation = {
    rootStore?: RootStore,
    localViewModel?: ValidateStore,
    error?: any,
    goBackHandler?: () => void

}
