import { ISelect, IUser } from "../models/user-store/user.types"
import { RootStore } from "../models/root-store/root.types"
import { toJS } from "mobx"
import { INotification } from "../models/notification-store"
import { FirebaseMessagingTypes } from '@react-native-firebase/messaging'
import moment, { Moment } from "moment"
import 'moment/locale/da'
import { IComment, IUserComment } from "../modules/tabs/pages/dude-profile-page/page-4/storemodels"
import { GeneralResources, RemoteConfigGroupedKey } from "../constants/firebase/remote-config"
import { GeneralResourcesStore, ISharedTimer } from "../models"
import { FileUploadType } from "../constants/app.constant"
import { IUploadFileData } from "./firebase/fire-storage/fire-storage.types"
import { Property, MobileProperty } from "../constants/firebase/fire-store.constant"
import { IChat } from "../models/chat-store"
import { IMessage } from "../libs/gifted-chat"
import * as LocaleUtils from "./locale"
import { UserModel } from "../models/user-store/user.store"
import { ExtractCSTWithSTN } from "mobx-state-tree/dist/core/type/type"

export const getAgeSelectedIndex = (age?: string, referenceArray?: ISelect[]): number => {
    return referenceArray?.findIndex((e) => {
        if (!age) return true
        return e.key === age
    })
}

export const getSelectObject = (objects: any[]): ISelect[] => {
    let options: ISelect
    return objects?.map((v: {}) => {
        options = {
            key: Object.keys(v)[0],
            value: Object.values(v)[0] as any
        }
        return options
    })
}

export const transformNotificationData = (notification: FirebaseMessagingTypes.RemoteMessage): INotification => {
    const payload = notification?.data
    const data = payload?.data && JSON.parse(payload?.data)
    const navigation = payload?.navigation && (typeof payload?.navigation === 'string' ? JSON.parse(payload?.navigation) : undefined)
    const misc = payload?.misc && (typeof payload?.misc === 'string' ? JSON.parse(payload?.misc) : undefined)
    return {
        [Property.NOTIFICATION_ID]: payload[Property.NOTIFICATION_ID] && payload[Property.NOTIFICATION_ID],
        [Property.DATA]: data,
        [Property.NAVIGATION]: navigation,
        [Property.MISC]: misc,
        [Property.TOPIC]: payload?.topic,
        ...notification?.notification
    }
}

export const getModalContentList = (data: any[], isSelectType: boolean = false) => {
    const objects = data as any[]
    return objects?.map(v => {
        let options = {
            key: isSelectType ? v.key : Object.keys(v)[0],
            label: isSelectType ? v.value : Object.values(v)[0]
        }
        return options
    })
}

export const getCommentObject = (commentData?: Partial<IComment>): Partial<IComment> => {
    const comment: IComment = commentData
    return comment
}

export const getUserCommentObject = (data?: IUserComment) => {
    const comment: IComment = data?.comment
    const dudeProfile: IUser = data?.dudeProfile
    return {
        comment,
        dudeProfile
    }
}

export const getNotificationObject = (
    notificationData?: Partial<INotification>,
    convertDate?: boolean): Partial<INotification & ISharedTimer> => {
    let data: INotification & ISharedTimer = notificationData
    const responseDate = data?.misc?.date
    const date = convertDate ? moment(responseDate.toDate()) : moment(responseDate)
    data = {
        ...data,

        //to cast to shareStore timer.
        [MobileProperty.DATE_TIME]: date,
    }
    return data
}

export const getChatMessageObject = (rootStore: RootStore,
    chatData?: Partial<IChat>,
    convertDate?: boolean,
    fromFirebase: boolean = false): Partial<IChat & ISharedTimer> => {

    const chatUserFromFirebase = chatData?.interlocutor_profiles?.filter(v => v.uid != rootStore?.getUserStore?.getUserId)
    let data: IChat & ISharedTimer = chatData

    const responseUpdatedDate = data?.updated_date
    const updatedDate = convertDate ? moment(responseUpdatedDate.toDate()) : moment(responseUpdatedDate)
    data = {
        ...data,
        [Property.UPDATED_DATE]: updatedDate,
        [Property.USER]: getUserProfileObject(rootStore, null, fromFirebase ? chatUserFromFirebase?.length > 0 &&
            chatUserFromFirebase[0] : data?.user),
        [Property.LAST_MESSAGE]: getMessageObject(rootStore, data?.last_message),

        //to cast to shareStore timer.
        [MobileProperty.DATE_TIME]: updatedDate,
    }
    return data
}

export const getMessageObject = (rootStore: RootStore, messageData?: Partial<IMessage>, convertDate?: boolean): Partial<IMessage> => {
    const responseCreatedDate = messageData?.created_date
    const createdDate = convertDate ? moment(responseCreatedDate.toDate()) : moment(responseCreatedDate)
    return {
        ...messageData,
        [Property.SENDER]: getUserProfileObject(rootStore, null, messageData?.sender),
        [Property.CREATED_DATE]: createdDate
    }
}

export const getUserProfileObject = (rootStore: RootStore, data?: any, userData?: Partial<IUser> | ExtractCSTWithSTN<typeof UserModel>) => {
    const cities = rootStore?.getAllGeneralResourcesStore?.getCities
    const countries = rootStore?.getAllGeneralResourcesStore?.getCountries
    const seekings = getSelectObject(rootStore?.getAllGeneralResourcesStore?.getHavdSogerHans)
    const interests = rootStore?.getAllGeneralResourcesStore?.getInterests
    const tags = rootStore?.getAllGeneralResourcesStore?.getTags
    const existingData = data && toJS(data)
    const transformPrimativeType = (value?: any, referenceArray?: ISelect[]) => {
        if (!value) return
        const isPrimative: boolean = typeof value === "number" || typeof value === "string"
        if (!isPrimative) return value
        if (referenceArray) {
            const mappedValue = referenceArray?.find(e => e.key === value)?.value
            if (mappedValue?.length === 0) return referenceArray
            return {
                key: value,
                value: mappedValue
            }
        }
        return {
            key: value.toString(),
            value: value.toString()
        } as ISelect
    }

    const profileImage = (): IUploadFileData[] => {
        const profileImage = userData?.profile_image as IUploadFileData
        if (!profileImage) return []
        const obj: any = profileImage
        const url = profileImage?.url || obj && obj[0]?.file_path
        const baseProfileImage = {
            [MobileProperty.FILE_PATH]: url,
            [MobileProperty.TYPE]: FileUploadType.ProfileImage
        }
        if (Array.isArray(obj)) return [{ ...(obj[0]) }]
        return [{
            ...profileImage,
            ...baseProfileImage
        }]
    }
    const galleryImageList = userData?.gallery_image_list as IUploadFileData[]
    const galleryImages: IUploadFileData[] = galleryImageList &&
        galleryImageList?.map((obj) => {
            const url = obj?.url || (obj as any)?.file_path
            return {
                ...obj,
                [MobileProperty.FILE_PATH]: url,
                [MobileProperty.TYPE]: FileUploadType.GalleryImage,
            }
        })

    const transformObject = (primaryArray: ISelect[], secondArray: any): ISelect[] => {
        if (!secondArray || secondArray?.length === 0) return []
        if (secondArray[0]?.key) return secondArray
        return primaryArray?.filter((array: { key: string }) =>
            secondArray?.some((filter: any) => {
                return filter?.key ? filter?.key?.toLocaleLowerCase() : filter.toLocaleLowerCase() === array.key.toLocaleLowerCase()
            })
        )
    }
    const params = {
        ...userData,
        ...{
            [Property.AGE]: transformPrimativeType(userData?.age)
        },
        ...{
            [Property.COUNTRY]: transformPrimativeType(userData?.country, countries)
        },
        ...{
            [Property.CITY]: transformPrimativeType(userData?.city, cities)
        },
        ...{
            [Property.HAVD_SOGER_HAN_LIST]: transformObject(seekings, userData?.havd_soger_han_list)
        },
        ...{
            [Property.INTEREST_LIST]: transformObject(interests, userData?.interest_list)
        },
        ...{
            [Property.TAG_LIST]: transformObject(tags, userData?.tag_list)
        },
        ...{
            [Property.GALLERY_IMAGE_LIST]: galleryImages
        },
        ...{
            [MobileProperty.COMPARE_GALLERY_IMAGE_LIST]: galleryImages
        },
        ...{
            [Property.PROFILE_IMAGE]: profileImage()
        },
        ...{
            [MobileProperty.COMPARE_PROFILE_IMAGE]: profileImage()
        }
    }
    if (existingData) return { ...existingData, ...params }
    return params
}

export const getAllCitiesByRegions = (rootStore?: RootStore, selectedRegions?: ISelect[]): string => {
    const citiesRegions = rootStore?.getAllGeneralResourcesStore?.getCitiesRegions
    const cityListValuesSelected = citiesRegions?.filter((array: { key: string }) => selectedRegions?.some(filter => filter.key === array.key)).map(e => e.value)
    const cityListSelected: string[] = [].concat.apply([], cityListValuesSelected)
    return cityListSelected.join(',')
}

export const getDateTimeFormatted = (date?: any): string => {
    let dateMoment: Moment
    let dateTime = new Date(date)
    if (dateTime instanceof Date && !isNaN(dateTime.valueOf())) {
        dateMoment = moment(dateTime)
    } else {
        const firebaseTime = date?._seconds * 1000
        dateMoment = moment(new Date(firebaseTime))
    }
    return dateMoment?.locale(LocaleUtils.getLocaleDevice().toLowerCase())?.format("MMMM, D") || null
}

export const getUserStatusByLocaleKey = (store: GeneralResourcesStore, status: string): string => {
    const keys = Object.values(GeneralResources.UserStatus)
    const translatedValue: string[] = keys.filter(e => e.includes(status))
    return translatedValue?.length > 0 && store.getValues(translatedValue[0], true)
}

export const getNotificationByLocaleKey = (store: GeneralResourcesStore, value: string, type: RemoteConfigGroupedKey): string | undefined => {
    const keys = Object.values(GeneralResources.Notification)
    const translatedValue: string[] = keys.filter(e => e.includes(value))
    return translatedValue?.length > 0 && store.getNotification(type + "." + translatedValue[0])
}

export const getSeekingGuyStatusByLocaleKey = (store: GeneralResourcesStore, isSeeking: boolean): string => {
    const keys = Object.values(GeneralResources.SeekingGuyStatus)
    const statusText = store.getValues(GeneralResources.SharedKeys.statusTitle, true)
    const statusValue = !isSeeking ? store.getValues(keys[0], true) : store.getValues(keys[1], true)
    return statusText.concat(" ").concat(statusValue)
}

export const addSubscriptionToUserProfileObject = (rootStore: RootStore, userData: IUser) => {
    let combinedObj = userData
    const dateNow = moment(new Date())
    const serverTime = rootStore?.getSharedStore?.getServerTime
    const serverDate = serverTime && moment(serverTime) || dateNow
    const expiryDateForSubscription = userData?.expiry_date
    if (serverDate && expiryDateForSubscription) {
        const momentExpiryDate = moment(expiryDateForSubscription?.toDate())
        combinedObj = {
            ...userData,
            [Property.EXPIRY_DATE]: momentExpiryDate,
            [Property.IS_SUBSCRIPTION]: !serverDate.isSameOrAfter(momentExpiryDate)
        }
    }
    return combinedObj
}

export const addPurchaseToUserProfileObject = (userData: IUser, firebaseExpiryDate?: any, shouldCastToDate: boolean = true) => {
    let combinedObj = userData
    if (firebaseExpiryDate) {
        const expiryDate = shouldCastToDate ? moment(firebaseExpiryDate?.toDate()) : moment(firebaseExpiryDate)
        combinedObj = {
            ...userData,
            [Property.EXPIRY_DATE]: expiryDate
        }
    }
    return combinedObj
}

export var isEqual = function (value, other) {

    // Get the value type
    var type = Object.prototype.toString.call(value);

    // If the two objects are not the same type, return false
    if (type !== Object.prototype.toString.call(other)) return false;

    // If items are not an object or array, return false
    if (['[object Array]', '[object Object]'].indexOf(type) < 0) return false;

    // Compare the length of the length of the two items
    var valueLen = type === '[object Array]' ? value.length : Object.keys(value).length;
    var otherLen = type === '[object Array]' ? other.length : Object.keys(other).length;
    if (valueLen !== otherLen) return false;

    // Compare two items
    var compare = function (item1, item2) {

        // Get the object type
        var itemType = Object.prototype.toString.call(item1);

        // If an object or array, compare recursively
        if (['[object Array]', '[object Object]'].indexOf(itemType) >= 0) {
            if (!isEqual(item1, item2)) return false;
        }

        // Otherwise, do a simple comparison
        else {

            // If the two items are not the same type, return false
            if (itemType !== Object.prototype.toString.call(item2)) return false;

            // Else if it's a function, convert to a string and compare
            // Otherwise, just compare
            if (itemType === '[object Function]') {
                if (item1.toString() !== item2.toString()) return false;
            } else {
                if (item1 !== item2) return false;
            }

        }
    };

    // Compare properties
    if (type === '[object Array]') {
        for (var i = 0; i < valueLen; i++) {
            if (compare(value[i], other[i]) === false) return false;
        }
    } else {
        for (var key in value) {
            if (value.hasOwnProperty(key)) {
                if (compare(value[key], other[key]) === false) return false;
            }
        }
    }

    // If nothing failed, return true
    return true;

};