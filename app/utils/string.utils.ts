export const newLineToBr = (inputString: string) => {
    return inputString?.replace(/\n/g, '<br/>').replace(/\s/g, '&nbsp;')
}

export const brToNewLine= (inputString: string) => {
    return inputString?.replace(/(<br>|<\/br>|<br \/>)/mgi, '\n').replace(/&nbsp;/g, ' ')
}