export const writeFile = async (fileName: string, data: any) => {
    var RNFS = require('react-native-fs')
    var path = RNFS.DocumentDirectoryPath + '/' + fileName
    try {
        await RNFS.writeFile(path, data, 'utf8')
    }
    catch (e) {
    }
}

export const readFile = async (fileName: string): Promise<any> => {
    var RNFS = require('react-native-fs')
    var path = RNFS.DocumentDirectoryPath + '/' + fileName

    try {
        return await RNFS.readFile(path, 'utf8')
    } catch (e) {
        return null
    }
}